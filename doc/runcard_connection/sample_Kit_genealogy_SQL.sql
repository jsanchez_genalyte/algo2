SELECT mb_con1.serial AS kit_serial,
       
mb_con1.partnum       AS matchbox_part,   #00446
       
mb_con1.partrev       AS matchbox_rev,  
       
mb_con1.inv_serial    AS matchbox_serial,
       
mb_con2.partnum       AS spot_chip_part,  #00445
       
mb_con2.partrev       AS spot_chip_rev,
       
mb_con2.inv_serial    AS spot_chip_serial,
       
mb_con3.partnum       AS carrier_part,  #00058
       
mb_con3.partrev       AS carrier_rev,
       
mb_con3.inv_serial    AS carrier_serial 

FROM      wo_consumption AS mb_con1
  
LEFT JOIN wo_consumption AS mb_con2
  ON mb_con2.serial = mb_con1.inv_serial
    AND mb_con2.partnum = '00445'

LEFT JOIN wo_consumption AS mb_con3
  ON mb_con3.serial = mb_con1.inv_serial
    AND mb_con3.partnum = '00058'

WHERE mb_con1.partnum = '00446';

The query is great, and almost what we need.
The key column to add to the output       would be:  spot_chip_lot_number  
The key column to add to the where clause would be:  and spot_chip_lot_number = 'AB150I' (as an example).

