function [ a_table ] = table_add_table_ndx( a_table )
%TABLE_ADD_TABLE_NDX adds a column called: table_ndx to a given table. The column contents is the series: 1:1:height(a_table)
%   The pupose here is to save the original order of the table records so when a table is subset, with the table_ndx it can be restored the rest of the row.
%   Assumption: the table does not have already a table_ndx column. If it does: the function does nothing.
% sample call:  [ a_table ] = table_add_table_ndx( a_table )

[column_ndx_found_flag,~]= is_element_in_cell(a_table.Properties.VariableNames,'table_ndx');
if (column_ndx_found_flag)
    % COLUMN: table_ndx ALREADY EXIST IN TABLE. Do nothing
    return;
end
% COLUMN: table_ndx DOES NOT  EXIST IN TABLE. Add it.
table_height    = height(a_table);
table_ndx_new   = table([1:1:table_height]');
table_ndx_new.Properties.VariableNames{end} = 'table_ndx';
a_table         = [ a_table table_ndx_new ];

end % fn table_add_table_ndx

