function [ change_flag, ui_state_struct  ] = mpopup_menu_upd_list_item_slected( ui_state_struct,cur_gui_ndx )  % list_ndx_filt_ori_dat  ui_state_struct,db_metad_set_struct   )
%MPOPUP_MENU_UPD_LIST_ITEM_SLECTED updates the list_item_slected after a user selects an item from any of the mpopup_menus
%   Returns list_item_slected with one or zeros to reflect the selection of the menu in ori_dat coordinates
%   Handles the special cases:  (jas_here: parse the policy for mpopup_menu
%   'All','Swap','Search..'. Future: handle the custom or search cases.   % CURRENT: version:   in use (note: ORI version is  niu.)
%   jas_tbd: 'Search...' (use the routines from the experiment tab with regexpr
%   Currently handling  multiple selections future: using custom or multi checkmarks on pullups if possible from matlab GUI
%   source: ui_upd_list_ndx_filt_dir
%   mpopup_menu_policy documentation: at the end of this file

% pipeline:   Sensogram_Analysis_App_10 -> popupmenu_probe_Callback --> mpopup_menu_upd_list_item_slected
% see:        sa_ui_create_popup_struc


%list_item_names    = ui_state_struct_cat.list_item_names;
list_item_slected   = ui_state_struct.list_item_slected;
%list_item_enabled   = ui_state_struct.list_item_enabled; not used in this FUNCTION

change_flag           = true;
list_item_slected_ori = list_item_slected;


if( cur_gui_ndx ==1 )
    % ALL SELECTED: swap 1 and make all others match the all selection: either 1 or zero.
    %
    list_item_slected(1)        = ~list_item_slected(1);    % swap 1
    list_item_slected(4:end)    =  list_item_slected(1);    % match singles with all: all zeros or all ones
    % Code common to ALL and SWAP: ...
    ui_state_struct.cur_gui_ndx = cur_gui_ndx;
    sel_cnt                     = sum(list_item_slected(4:end));
    first_sel_ndx               = find(list_item_slected(4:end) ==1,1);
    
    if (length(list_item_slected(4:end)) == 1)
        % ONLY ONE ITEM: Never let it get unselected and do not display ALL
        ui_state_struct.cur_gui_ndx = 4; % forces to display the single item.
        list_item_slected(1)        = 1;
        list_item_slected(2)        = 0; % swap: is not selected
        list_item_slected(3)        = 0; % search: is not selected
        list_item_slected(4)        = 1; % forces to turn on the single item
    else
        % MORE THAN ONE ITEMS ON THE LIST
        if (sel_cnt == length(list_item_slected(4:end)))
            % ALL IS SELECTED NOW:
            list_item_slected(1)        = 1;
            ui_state_struct.cur_gui_ndx = 1;
        end
        if ((sel_cnt < length(list_item_slected(4:end))) && (sel_cnt >0))
            % 1 OR MORE SELECTED BUT NOT ALL: set it to the first found
            ui_state_struct.cur_gui_ndx =  first_sel_ndx + 3; % account for the first_3
        end
        if (sel_cnt == 0)
            % NOTHING IS SELECTED AMONG THE SINGLES AFTER THE ALL: set the all to zero to be consistent
            list_item_slected(1)        = 0; % reset the all (so it can be turned on later).
            % Now force something on the screen: The first item.
            ui_state_struct.cur_gui_ndx = 4;
            list_item_slected(4)        = 1;
        end
        if (sel_cnt == 1)
            % this case SHOULD NEVER HAPPEN BECAUSE ALL sets eveithing to zero or 1 
            % ONE IS SELECTED AMONG THE SINGLES AFTER THE SWAP: set the all to zero to be consistent
            list_item_slected(1)        = 0; % reset the all (so it can be turned on later).
            list_item_slected(2)        = 0; % reset the swap
            ui_state_struct.cur_gui_ndx = first_sel_ndx + 3;
            display(                'Error_in_llogic: all should set eveithing to zeros or ones');
            %list_item_slected(first_sel_ndx) = 1; % this is redundant!!!
        end
    end % more than one on the list
end % ALL Selected

if( cur_gui_ndx ==2 )
    % SWAP SELECTED:
    list_item_slected(4:end)        = ~list_item_slected(4:end) ; % swap singles
    % Code common to ALL and SWAP: ...
    ui_state_struct.cur_gui_ndx     = cur_gui_ndx;
    sel_cnt                         = sum(list_item_slected(4:end));
    first_sel_ndx                   = find(list_item_slected(4:end) ==1,1);
    
    if (length(list_item_slected(4:end)) == 1)
        % ONLY ONE ITEM: Never let it get unselected and do not display ALL
        ui_state_struct.cur_gui_ndx = 4; % forces to display the single item.
        list_item_slected(1)        = 1;
        list_item_slected(2)        = 0; % swap: is not selected
        list_item_slected(3)        = 0; % search: is not selected
        list_item_slected(4)        = 1; % forces to turn on the single item
    else
        % MORE THAN ONE ITEMS ON THE LIST
        if (sel_cnt == length(list_item_slected(4:end)))
            % ALL IS SELECTED NOW:
            list_item_slected(1)        = 1;
            ui_state_struct.cur_gui_ndx = 1;
        end
        if ((sel_cnt < length(list_item_slected(4:end))) && (sel_cnt >0))
            % 1 OR MORE SELECTED BUT NOT ALL: set it to the first found
            ui_state_struct.cur_gui_ndx =  first_sel_ndx + 3; % account for the first_3
        end
        if (sel_cnt == 0)
            % NOTHING IS SELECTED AMONG THE SINGLES AFTER THE SWAP: set the all to zero to be consistent
            list_item_slected(1)        = 0; % reset the all (so it can be turned on later).
            % Now force something on the screen: The first item.
            ui_state_struct.cur_gui_ndx = 4;
            list_item_slected(4)        = 1;
        end
        if (sel_cnt == 1)
            % ONE IS SELECTED AMONG THE SINGLES AFTER THE SWAP: set the all to zero to be consistent
            list_item_slected(1)        = 0; % reset the all (so it can be turned on later).
            list_item_slected(2)        = 0; % reset the swap
            ui_state_struct.cur_gui_ndx = first_sel_ndx + 3;
            %list_item_slected(first_sel_ndx) = 1; % this is redundant!!!
        end
    end % more than one on the list
end % SWAP selected
if( cur_gui_ndx ==3 )
    % SEARCH SELECTED: jas_tbd
    % list_item_slected(4:end) = ~list_item_slected(4:end) ; % swap singles
    ui_state_struct.cur_gui_ndx = cur_gui_ndx;
end

if( (cur_gui_ndx>3))  % DO NOT CHECK FOR ENABLED!!! && ( list_item_enabled(cur_gui_ndx)  ))
    % SINGLE ITEM AND VALID SELECTION: Swap single
    if (length(list_item_slected(4:end)) == 1)
        % ONLY ONE ITEM: Never let it get unselected and do not display ALL
        ui_state_struct.cur_gui_ndx = 4; % forces to display the single item.
        list_item_slected(1)        = 1;
        list_item_slected(2)        = 0; % swap: is not selected
        list_item_slected(3)        = 0; % search: is not selected
        list_item_slected(4)        = 1; % forces to turn on the single item
    else
        % MORE THAN ONE ITEMS ON THE LIST
        list_item_slected(cur_gui_ndx)      = ~list_item_slected(cur_gui_ndx); % swap selection
        if (list_item_slected(cur_gui_ndx) )
            % ITEM IS SELECTED NOW: ndx is the one for this index: the last selected
            ui_state_struct.cur_gui_ndx     = cur_gui_ndx;
        else
            % ITEM IS UN-SELECTED NOW: Find the first ndx that is selected
            % but first: unselect 'ALL' (because at least one is not selected)
            list_item_slected(1)            = 0;
            first_sel                       = find(list_item_slected(4:end) == 1,1);
            if (isempty(first_sel))
                % FOUND NOTHING SELECTED: Rownd robin: Start all over by selecting all:
                list_item_slected(4:end)    = 1;
                list_item_slected(1)        = 1;
                ui_state_struct.cur_gui_ndx = 1;
            else
                % FOUND SOMETHING SELECTED: make it the current selection
                ui_state_struct.cur_gui_ndx = first_sel + 3;  %+3 to offset all/swap/search
            end
        end
    end % more than 1 item in list
end
ui_state_struct.list_item_slected   = list_item_slected; % save back selection
if (( ui_state_struct.cur_gui_ndx == cur_gui_ndx) ...
        && (sum( list_item_slected_ori == list_item_slected  ) == length(list_item_slected_ori) ) )
    % NOTHING HAS CHANGED: Set the flag to false so nothing will change in the UI
    change_flag           = flase;
    display('No changes where detected in list_item_slected and cur_gui_ndx');
end

end % fn mpopup_menu_upd_list_item_slected
% % 
% % DATA_FILTER_ALGORITHM:  mpopup_menu_policy file: mpopup_menu_upd_list_item_slected
% % 
% % special individual cases: date,rata,amplif (because: only single selection, no all, and no inversion makes sense)
% % 
% % bold/plain    == selected/not selected  independent of other selections   stored in user data    bold==bold          plain== non-bold 
% % dark/gray     == availabe/not available   dependent of other selections   stored in user data    dark==normal black  gray== italic pale  
% % 
% % arrays:
% % 
% % item_selected_array logical array: 1 = selected: current or before (i.e this is the memory of selections to accumulate or remove from selected set
% % item_availabe_array logical array: 1 = availabe: current: determined as last spet after re-calculating all data that is available as resulting of last selection.
% % 
% % mpopup_menu:   is a multi selection popup menu (called mpopup) that allows the following:
% %                1 - select all.
% % 			   2 - swap selection. (if all selected: --> becomes: select none. This would be a shortcut to start all over or go from all to single).
% % 			       selected singles become unselected and viceversa. 
% % 			   3 - select individual items: either  accumulate to cur selection list: if single is currently un-selected) (change to bold)
% % 			                                or      remove   from cur selection list: if single is currently    selected) (change to plain
% % 											the state of all is: bold: if all items are selected (indep of other selections) otherwhise plain                             
% % 											                     dark: if any item is availabe(from other selections) otherwhise: gray (ie all unavailable).
% %                                                                  											
% % 			   4 - mpopup list is constant. what changes is either bold/plain (selected/not) or black/gray (availabe/not available)
% % 			   5 - order of selection is irrelevant when accumulating/removing selections.
% % 			   6 - selection of not available items: will make them selected but i gues nothing will change in the data (think about this jas_tbd)
% %                7 - bold/plain (selected/not)     is function exclusivelly from the menu items.
% % 			   8 - black/gray (availabe/not available) is function of: current selection,available data and new selection.
% % 			   9 - select all,swap and search selections are ALWAYS dark: availabe (never gray) and 
% % 			   10- When ALL is selected: (indep of other selections)
% % 			       swap (1) and make list(4:end) = list(1)  i.e. if all goes on: every_item goes 1  and viceversa. 
% % 			       cases: single item:  turn on the single item, never let single item be turned off. swap=0,search=0,all=1
% % 				          multi  item:  if sel_cnt is < all and sel_cnt >0 
% %                                            set first item for the gui
% %                                         if sel_cnt ==0
% %                                            set(1) = 0
% %                                            set(4) = 1; turn on first item and gui = 4;
% %                                         										   
% % 			   11- When SWAP is selected:
% % 			       - swap (4:end) i.e. make list(4:end) = ~list(4:end)  
% % 				   - Same logic as for 'All', excep that there is one extra case:
% % 				     if (some are selected: ie >0 and <all)
% % 					    set the gui to the first turned on element on the list. 
% %                
% %                12- When singles are selected: 
% % 			       cases: single item:  turn on the single and never let it be turned off (to avoid empty list) and make (1) =1  swap=0 search=0 				   						  
% % 				          multi  item: 	swap (cur_gui) i.e. make list(cur_gui) = ~list(cur_gui)
% % 						                if item on:  make gui == cur_gui (to display new selection)
% % 						                if item off: make gui first on element in the list (if none is found: make gui = element 4 and make it on)
% % 
% % 						
% % 			   13- reevaluate available data using new selection list (ignore previous availability)
% % 			   14- update menus using these rules:
% % 				   selected/unselected:     bold/plain          font.
% % 				   available/un-available:  black/gray-itiailic fond.
% % 				 
% % 				   jas_tbd_think about this: dark  when at least one item is dark (after reevaluating others) or pale when all items are pale  
% % 				   
% % 			   15- select swap state: plain always (non-state selection: never selected) make all items swap bold/plain (indep of other selections). then reevaluate
% % 				                      dark  always (dark: always: always availabe swap (never gray)
% % 			   
% % 1 - Initalization:                              initial state:
% %     create menus with:  1 - all                 selected(bold) available(plain)   for single items: it should be   selected(bold)/  available(plain)
% % 	                    2 - swap                selected(bold) available(plain)   for single items: it should be unselected(plain)/unavailable(italic)
% % 						3 - search ...          selected(bold) available(plain)   for single items: it should be unselected(plain)/unavailable(italic)
% % 						4 - n individual list
% %                         i.e.: no filter
% % 2 - when a mpopup menu is selected:
% %     define bold/plain:  item 1 selected (all:  make all items bold).
% %     	                item 2 selected (swap selection:  swap bold/plain for all singles)
% % 						
% % 						