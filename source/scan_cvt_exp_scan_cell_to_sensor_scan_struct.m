function [ sensor_scan_struct ] = scan_cvt_exp_scan_cell_to_sensor_scan_struct( exp_scan_cell )
%SCAN_CVT_EXP_SCAN_CELL_TO_SENSOR_SCAN_STRUCT converts the scan cells returned by the API into a struct to be saved in the HD
%   sensor_scan_struct has 3 integers and the matrix with the scan data for all the sensors.

scan_max_col_cnt_cfg    = 42; % jas_config: hardcoded limit in number of scan cols. anything above it gets trimmed out. jas_tbd.

sensor_cnt              = size(exp_scan_cell,1);
scan_len_max            = 0;
scan_col_max            = 0;
for cur_sensor_ndx      = 1:1:sensor_cnt
    cur_sensor_struct   = exp_scan_cell{cur_sensor_ndx};
    if( (size(cur_sensor_struct.sensor_data,1)) > scan_len_max)
        scan_len_max    = size(cur_sensor_struct.sensor_data,1);
    end
    if( (size(cur_sensor_struct.sensor_data,2)) > scan_col_max)
        scan_col_max    = size(cur_sensor_struct.sensor_data,2);
    end    
    
end

% Preallocate matrix for real scan data: to write into the .mat file
sensor_data_dim_1    = scan_len_max*sensor_cnt;
%sensor_data_dim_2    = scan_col_max;
sensor_data_dim_2    = min(scan_col_max,scan_max_col_cnt_cfg); %jas_temp: poor mans way to fix jervis problem: scan data is not always 42 cols: at times: 44


sensor_data_mat      = nan(sensor_data_dim_1,sensor_data_dim_2); % extra col is for the  no need:  sensor number implicit in row.

cur_start_row=1;
for cur_sensor_ndx   = 1:1:sensor_cnt
    %cur_sensor_ndx
    % Store in the sensor_data_mat matrix the data this way: col_1 is . col_1 to end is real scan data   (sensor_number is implicit)
    cur_sensor_struct= exp_scan_cell{cur_sensor_ndx};
    if (isfield(cur_sensor_struct,'number'))
        cur_number       = cur_sensor_struct.number;
        if (cur_number ~= cur_sensor_ndx)
            % report missing scan data.
            sprintf('\nWarning: sensor data does not come sorted %-4d %-4d',cur_number,cur_sensor_ndx);
            continue;
        end
    else
        continue;
    end
    if (~isempty(cur_sensor_struct.sensor_data))
        cur_sensor_data  = cur_sensor_struct.sensor_data(:,1:sensor_data_dim_2);
        cur_end_row      = cur_start_row+size(cur_sensor_data,1) -1;
        sensor_data_mat(cur_start_row:cur_end_row,1:size(cur_sensor_data,2)) = cur_sensor_data;
    else
                sprintf('\nWarning: Empty sensor data %-4d ',cur_sensor_ndx);
    end
    cur_start_row 	 =   cur_start_row + scan_len_max; % (vs: cur_end_row + 1
%     if ((cur_end_row + 1) ~= cur_start_row)
%         % FOUND A CASE WHERE SCAN DATA IS SHORT: jas_tbd_review: is this a issue or not?
%          fprintf('\nFor sensor: %4d  nominal scan length = %5d and curr scan len = %5d\n'...
%              ,size(cur_sensor_data,1),scan_len_max,scan_len_max);
%     end
end

% No need to trim data because using scan_len_max: This would damage the RESHAPE!!!!! 
% % % % % % % if ( cur_end_row < sensor_data_dim_1 )
% % % % % % %     % NEED TO TRIM ROWS:
% % % % % % %     sensor_data_mat = sensor_data_mat(1:cur_end_row,:);
% % % % % % % end

% Pack struct to return: 
sensor_scan_struct = struct;
sensor_scan_struct.sensor_cnt   = sensor_cnt;
sensor_scan_struct.scan_len_max = scan_len_max;
sensor_scan_struct.scan_col_max = scan_col_max;
sensor_scan_struct.sensor_data_mat = sensor_data_mat;

end % fn scan_cvt_exp_scan_cell_to_sensor_scan_struct

