function [ sensor_table] = probe_get_sensor_names_and_numbers( probe_map_table, sensor_cnt ) % old: get_sensor_names_and_numbers_for_probe
%PROBE_GET_SENSOR_NAMES_AND_NUMBERS retireves the sensor names and sensor number associated for each sensor name.
%   Detailed explanation goes here   NOT NIU yet NIU (old style: data from the instrument)
% sample call: [  name_list, sensor_list] = probe_get_sensor_names_and_numbers( probe_map_table, 1 );

 % old return: name_list, sensor_list  new return: a table: sensor_table

% get wanted channel and only analysis (category ==1))  and tr (category ==3) . Note: leak detectors get excluded here: not of interest.
% ndx = (probe_map_table.channel ==channel_number) & ( (probe_map_table.category ==1) | (probe_map_table.category ==3) );
% probe_map_table_wanted = probe_map_table(ndx,:);
% probe_map_table_size   = height(probe_map_table_wanted);
% size_est        = 136; % sum(ndx)*4*2;
sensor_number   = zeros(sensor_cnt,1);
sensor_type     = zeros(sensor_cnt,1);
sensor_name     = cell( sensor_cnt,1);
cluster         = zeros(sensor_cnt,1);
channel         = zeros(sensor_cnt,1);
probe_map_size  = height(probe_map_table);
for cur_probe_ndx=1:1:probe_map_size
    % for each probe grab the details and build the list of sensors. (128 size) 
    cur_sensor_list = probe_map_table.sensors(cur_probe_ndx);
    cur_sensor_name = probe_map_table.name(cur_probe_ndx);   
    cur_sensor_type = probe_map_table.category(cur_probe_ndx);     
    cur_channel     = probe_map_table.channel(cur_probe_ndx);       
    cur_sensor_list = cur_sensor_list{1};
    for cur_sensor_ndx=1:1:size(cur_sensor_list,1)
        cur_sensor = cur_sensor_list(cur_sensor_ndx);
        sensor_number(cur_sensor)   = cur_sensor;
        sensor_type  (cur_sensor)   = cur_sensor_type;
        sensor_name  (cur_sensor)   = cur_sensor_name;  
        cluster      (cur_sensor)   = cur_sensor_ndx;       % 1 to 4 or 1 to 2   
        channel      (cur_sensor)   = cur_channel;       % 1 to 4 or 1 to 2          
    end
end
sensor_table = table(sensor_number,sensor_type,sensor_name,cluster,channel);

% if (channel_number == 1)
%     antr_list   = table2array(probe_map_table(1:18,1));    
%     name_list   = table2array(probe_map_table(1:18,2));
%     sensor_list = table2array(probe_map_table(1:18,3));
% end
% if (channel_number == 2)
%     antr_list   = table2array(probe_map_table(19:36,1));      
%     name_list   = table2array(probe_map_table(19:36,3));
%     sensor_list = table2array(probe_map_table(19:36,3));
% end

end % fn probe_get_sensor_names_and_numbers

