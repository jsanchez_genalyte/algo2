function [ handles ] = sa_do_edit_req_expe( hObject, eventdata, handles )
%SA_DO_EDIT_REQ_EXPE  gets the string from the end-user to request a set of experiments. by name
%   If the API does not handle wild characters: this function request all experiments and 
%   calls the matlab filter that processes wild chars from a list
%  Sample experiment: Jervis ANA Validation

% Hints: get(hObject,'String') returns contents of edit_req_spot as text
%        str2double(get(hObject,'String')) returns contents of edit_req_spot as a double
% Steps:
% S1 get all experiments: API indexTests

% S2
edit_req_expe_str                       = get(hObject,'String');
% HAVE ONE OR MORE EXPERIMENT STRINGS (May have wild characters)
delimiter                               = {','}; % COMMA IS THE ONLY VALID SEPARATOR BETWEEN exp names. spaces are ok in exp names, dashes too: '     ','    ','   ','  ',' ', ,'-'}
edit_req_expe_str                       = split(edit_req_expe_str,delimiter);
handles.ui_state_struct.req_expe_list   = edit_req_expe_str;
if ( strcmp(edit_req_expe_str,''))
      status_message = sprintf('Not using Experiment Name filter ' );
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
else
    status_message = strcat('Experiment Filter:__        ',edit_req_expe_str);
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
      
end

% [ protocol_list_cel, expe_list_cel, error_message ]  = sa_rest_protocol_get_all( )
% sample experiment details: 
% Real API returned fields: elements of a row of the 100 x 9 cell: 
%     ''28ec02fe-7bd4-4f54-be32-5f751c1287ef'	% 1  id:				string (guid)		% 
% 	'complete'             					% 2  state:			string				% 
% 	[1]                    					% 3  control:		string (boolean)    % 
% 	'4cdbc37c-3325-443c-a903-b55d70cbdbdd'  % 4  protocol_id:	string (guid)       % 
% 	'M53'                  					% 5  device_id:		string (guid)       % 
% 	{2�1 cell              					% 6  specimen_id
%     [1�1 struct]           				% 7  consumable	   123456789 123456789 123456789 
% 	'2017-07-21T05:57:45.994-07:00'			% 8  created_at   '2017-07-21T05:57:45.994-07:00'	date: 1:10 time: 12:16
% 	'2017-07-21T06:00:27.163-07:00' 		% 9  updated_id					
% 	2017-07-21 5:57 to 2017-07-21 7:00		% sample_date
%   2017-07-21 5:57 to 2017-07-21 7:00

% Documented API (2 swapped fields) (Ignored until confirming with John)
% id:				string (guid)		% 1 '28ec02fe-7bd4-4f54-be32-5f751c1287ef'
% state:			string				% 2 'complete'
% control:          string (boolean)    % 3 true
% protocol_id:      string (guid)       % 4 '4cdbc37c-3325-443c-a903-b55d70cbdbdd'
% device_id:		string (guid)       % 5 'M53'
% created_at:		string (datetime)   % 6 2x1 cell
% updated_id:		string (datetime)   % 7 1x1 struct
% specimen_id:	[ Identifier of the Specimen or Sample, if only 1 string is passed it is used for both channels.
% 				string
% 				]
% consumable:		[
% 				{
% 				experiment_name:		string (float)
% 				carrier_serial_number:	string
% 				spotting_lot_number:	string
% 				lot_number:				string
% 				}
% 				]


% 
% 'Jervis ANA Validation'
% 'CLIA Rerun 6'
% 'hay-test-080917-4'
% 'CLIA Rerun 1'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'Test-AB'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'CLIA Rerun 14'
% 'Jervis ANA Validation'
% 'hay-test-080917-3'
% 'Jervis ANA Validation'
% 'CLIA Rerun 38'
% 'hay-data-072717-1'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% []
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'CLIA Rerun 47'
% 'Jervis ANA Validation'
% 'Random Dropped Probe 1'
% 'hay-data-072717-6'
% 'Jervis ANA Validation'
% 'CLIA Rerun 44'
% 'hay-test-073117-15'
% 'Random Dropped Probe 7'
% 'hay-test-073117-7'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'hay-jervis15-validation-1'
% 'CLIA Rerun 33'
% 'CLIA Rerun 9'
% 'hay-test073117-13'
% 'hay-test-100'
% 'hay-test-073117-37'
% 'hay-test-073117-9'
% 'CLIA Rerun 11'
% 'Cleaning Test'
% 'hay-test-073117-22'
% 'hay-test-08102017-1'
% 'CLIA Rerun 18'
% []
% 'Testing-17'
% 'CLIA Rerun 4'
% 'Testing-17'
% 'hay-test-08142017-7'
% 'hay-test-081517-11'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'CLIA Rerun 2'
% 'Random Dropped Probe 3'
% 'CLIA Rerun 15'
% 'Random Dropped Probe 2'
% 'Jervis ANA Validation'
% 'CLIA Rerun 8'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'Jervis ANA Validation'
% 'CLIA Rerun 20'
% 'CLIA Rerun 1'
% 'CLIA Rerun 16'
% 'Jervis ANA Validation'
% 'CLIA Rerun 45'
% 'Jervis ANA Validation'
% 'CLIA Rerun 21'
% 'CLIA Rerun 12'
% 'Jervis ANA Validation'
% 'Random Dropped Probe 8'
% 'Jervis ANA Validation'
% 'CLIA Rerun 34'
% []
% 'CLIA Rerun 17'
% 'hay-test-073117-33'
% 'Jervis ANA Validation'
% 'hay-test-073117-1'
% 'CLIA Rerun 39'
% 'PortalTest'
% 'Jervis ANA Validation'
% 'hay-test-073117-16'
% 'Jervis ANA Validation'
% 'hay-data-072717-2'
% 'Alpha Jervis Testing'
% 'hay-test-073117-8'
% 'hay-test-072817-1'
% 'hay-test-080917-5'
% 'hay-test-073117-23'
% 'hay-test-073117-17'
% 'hay-test-09-28-17_1'
% 'hay-test-081617-27'
% 'CLIA Rerun 2'
% 'hay-test-073117-41'
% []
% 'Testing-17'
% 'Testin-17'



