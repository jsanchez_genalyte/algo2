function [ all_sensor_maps_table ] =  probe_get_all_sensor_all_maps( probe_map_struct )   % old: get_channel_number_for_sensor_map
%PROBE_GET_ALL_SENSOR_ALL_MAPS calculates all them  poas map for a probe_map_struct.  IU (new style: fixed 2018)
%   This is a one time thing, that returns an 136 array with the sensor_to_channel_map
%   input: probe_map_struct an array of 36 structs retrieved from the protocol 
%   sample call: [ channel_number ] = probe_get_all_sensor_all_maps( probe_map_struct); (Step 1 of 2). Step 2: next line: 
%                 So you can do: cur_channel = sensor_to_channel_map(cur_sensor_ndx); i.e. for a given sensor produces the corresponding channel.
%                 So: sensor_to_channel_map(40) = 1  should return channel 1
%                 So you can do: cur_analyte_ndx = analyte_ndx(cur_sensor_ndx); i.e. for a given sensor produces the corresponding channel.
%                 So: sensor_to_channel_map(40) = 1  should return analyte xxx where analyte xxx is the ndx of 

analyte_ndx      	= nan(136,1);
probe_sensor_number	= nan(136,1);

for cur_well_ndx = 1:36 %  ch_1_start_analyte_pos_ndx:1:ch_1_final_analyte_pos_ndx
    % get the scan_data for the current sensor
    %cur_scan_data = table2array(scan_table(cur_sensor_ndx,2));cur_scan_data = cur_scan_data{1};
    %gru = scan(1,cur_sensor_ndx).sensor_data(:,2);
    %a = = table2array(scan_table(cur_sensor_ndx,1));cur_scan_data = cur_scan_data{1};
    sensor_list     = probe_map_struct(cur_well_ndx).sensors;
    sensor_list_len = length(sensor_list);
    cur_channel     = probe_map_struct(cur_well_ndx).channel;
    for cur_sensor_ndx  =1:1:sensor_list_len
        cur_sensor                       	= sensor_list(cur_sensor_ndx);
        %sensor_to_channel_map(cur_sensor) 	= cur_channel;        
        analyte_ndx(cur_sensor)          	= cur_well_ndx; % using numbers for 1 to 36 for clarity: later: will have to filter out non-analytes 
        probe_sensor_number(cur_sensor)     = cur_sensor_ndx;         
    end
    
    cur_category   = probe_map_struct(cur_well_ndx).category;
    if  ( cur_category ==1 || cur_category == 4)
        % THIS IS A TRUE ANALYTE: store it 
        
    end
    
sensor_all_maps     = table(analyte_ndx   probe_sensor_number];
end

% S1. Create  list of analyte names
% S2. Create matrix  to store analyte gru.
% S3. Populate matrix
 
probe_map_table         = struct2table(probe_map_struct);
category                = probe_map_table.category;
category_analyte_ndx    = find(category == 1 | category == 4);  % jas_hard_coded: question cat 1 and 4 is what we want?
name_all                = probe_map_table.name;
name_analyte            = name_all(category_analyte_ndx);    
analyte_cnt             = size(category_analyte_ndx,1);
exp_sensor_excel        = double(8,analyte_cnt+2); % first 2 are channel (1 or 2) and sensor_numb (1 to 4)
for cur_ana_ndx = 1:1:analyte_cnt
     cur_channel = exp_sensor_table(cur_sensor_ndx).channel;
     cur_col     = exp_sensor_table(cur_sensor_ndx).analyte_ndx;
     if (cur_channel == 1)
         % FIRST BLOCK:
         cur_row = 1;
     end
     if (cur_channel == 2)
         % SECOND BLOCK:
         cur_row = 5;                  
     end     
    exp_sensor_excel(cur_row,cur_col) =  exp_sensor_table.results.normalized;
end % for each  cur_ana_ndx


end % fn probe_get_all_sensor_all_maps

