function [ req_date_struct,error_message ] = parse_date_request( date_request_str )
%parse_date_request converts a user request date of any format into 2 possible formats: day and second resolution
%   jerivs resolution format is yy to day  (used to make requests using the REST api)
%   second resolution format is yy to sec  (This is the way the data is stored in the db)
%   End user  may request in either format:
%             day resolution:     mm/dd/yyyy
%             second resolution:  mm/dd/yyyy HH:MM:SS AM
%   end user request may be:
%   1 - a single date: in day resolution it will be taken as a request for the whole day.
%   2 - a single date: in sec resolution it will be taken as a request for experiments at an exact time
%   3 - a single date: preceded by the keyword FROM: Meaning from that date to current time
%   4 - a single date: followed by the keyword TO:   Meaning all experiments up to the given date.
%   5 - 2 dates: FROM first_date TO sencond_date:    Meaning specific range
%   6 - No dates, but any of the following keywords, relative to the current date-time.

error_message                               = '';
req_date_struct                             = struct;
req_date_struct.start_day_resol_date_str    = '';
req_date_struct.end_day_resol_date_str      = '';
req_date_struct.start_sec_resol_date_str    = '';
req_date_struct.end_sec_resol_date_str      = '';

date_str_to_day_format               = 'mm/dd/yyyy';
date_str_to_sec_format               = 'mm/dd/yyyy HH:MM:SS PM';

% rule: when a number is     IN  a kewyword means: exact   amount of time: hours,days,weeks etc.
% rule: when a number is NOT IN  a kewyword means: partial amount of time: fraction of hours,days,weeks etc.
%               P        E
keywords_1 = { 'TODAY', 'YESTERDAY' };  % TODAY == 'LAST DAY'   'YESTERDAY' =='LAST 2 DAYS' (partial days)
%               P            P            E            P             E              P              E
keywords_2 = { 'LAST HOUR', 'THIS WEEK', 'LAST WEEK', 'THIS MONTH', 'LAST MONTH',  'THIS YEAR' ,  'LAST YEAR' };
%               E               E                E              E               E                E               E               E             E              E            E             E              E
keywords_3=  {'PREVIOUS 1 HOUR', 'PREVIOUS 2 HOURS', 'LAST 1 HOUR',  'LAST 2 HOURS',  'LAST 3 HOURS','LAST 6 HOURS', 'LAST 8 HOURS',  'LAST 12 HOURS','LAST 24 HOURS','LAST 2 DAYS','LAST 3 DAYS','LAST 4 DAYS','LAST 5 DAYS','LAST 6 DAYS','LAST 7 DAYS','LAST 30 DAYS','LAST 1 YEAR' };
%              'PREVIOUS 1 HOUR', : cur time: 10:23 --> 8:00 _ 9:00 (exact 1 hour within hour boundaries) 
% keywords_1_cat = categorical(keywords_1); % { 'TODAY', 'YESTERDAY' };
% keywords_2_cat = categorical(keywords_2); % { LAST HOUR', 'THIS WEEK', 'LAST WEEK', 'THIS MONTH', 'LAST MONTH',  'THIS YEAR' ,  'LAST YEAR' };
% keywords_3_cat = categorical(keywords_3); % { 'LAST 7 DAYS','LAST 30 DAYS' };

% Assumptions: 2 date-strings separated by either  blank or 'to'
% or:          1 date-string: in this case it is taken as a given day.
% date_str_day_resol_format = 'mm/dd/yyyy'; % jas_hard_coded from Dinakar: no datetime resolution.
% clean up the sepparator: replace it with blank.

% HAVE ONE OR more CONTIGUOS VALID STRINGS.

req_date_str_ori = upper(date_request_str);
req_date_str_ori = strrep(req_date_str_ori,'''','');  % get rid of quotes: if any
req_date_str_ori = strrep(req_date_str_ori,'  ',' '); % get rid of multiple spaces: if any
req_date_str_ori = strrep(req_date_str_ori,'  ',' '); % get rid of multiple spaces: if any

token_delimiter = {' ','\f','\n','\r','\t','\v'};
token_cel = strsplit(req_date_str_ori, token_delimiter) ;
token_cnt = size(token_cel,2);

% HAVE A CLEAN DATE RANG REQUEST STRING TO WORK WITH:

% Determine which kewords are present:
[ from_found_flag, from_ndx ] = is_element_in_cell(token_cel,'FROM');
[ to_found_flag,     to_ndx ] = is_element_in_cell(token_cel,'TO');
[ kw1_found_flag,  kw_1_ndx ] = is_element_in_cell(keywords_1,req_date_str_ori);
[ kw2_found_flag,  kw_2_ndx ] = is_element_in_cell(keywords_2,req_date_str_ori);
[ kw3_found_flag,  kw_3_ndx ] = is_element_in_cell(keywords_3,req_date_str_ori);

if (~( from_found_flag || to_found_flag ||isdatetime(req_date_str_ori) || kw1_found_flag || kw2_found_flag || kw3_found_flag))
    % NEITHER: FROM nor TO nor SINGLE_DATETIME nor KEYWORD: Try: a single date:
    try
        date_request_num = datenum(req_date_str_ori,'mm/dd/yyyy'); % 1/25/2018
        req_date_str_ori =datestr(date_request_num,'mm/dd/yyyy');
        
        req_date_struct.start_day_resol_date_str    = req_date_str_ori;
        req_date_struct.end_day_resol_date_str      = req_date_str_ori;
        req_date_struct.start_sec_resol_date_str    = datestr(date_request_num,date_str_to_sec_format); 
        if (~(isempty(req_date_str_ori)))
            req_date_struct.end_sec_resol_date_str      = strcat(req_date_str_ori,' ',' 11:59:59 PM');
        else
            req_date_struct.end_sec_resol_date_str  = req_date_str_ori;
        end
        return;
    catch
        fprintf('\n_error_: exception_thrown_function_ parse_date_request. NEITHER: FROM nor TO\n');        
        error('Incorrect Format')
        error_message = sprintf('Invalid date request: %s. See help for all the valid date  and date range formats',req_date_str_ori);
        return;
    end    
end
% HAVE A VALID DATE OR A VALID RANGE:
% Check simplest case first: singledate
if (isdatetime(req_date_str_ori) )
    % HAVE A VALID DATE:
    date_request_num                            = datenum(req_date_str_ori);
    req_date_struct.start_day_resol_date_str    = datestr(date_request_num,date_str_to_day_format);
    req_date_struct.end_day_resol_date_str      = datestr(date_request_num,date_str_to_day_format);
    req_date_struct.start_sec_resol_date_str    = datestr(date_request_num,date_str_to_sec_format);
    req_date_struct.end_sec_resol_date_str      = datestr(date_request_num,date_str_to_sec_format);
    return;
end

if ( (token_cnt > 1) && (isempty(token_cel{1})) )
    % EMPTY TOKEN AT THE BEGINNING: Get rid of it. and decrease the indexes for consistency
    token_cel = token_cel(2:end);
    %from_ndx = from_ndx-1;
    %to_ndx   = to_ndx-1;
    kw_1_ndx = kw_1_ndx-1;
    kw_2_ndx = kw_1_ndx-2;
    kw_3_ndx = kw_1_ndx-3;
end

if ( (token_cnt > 1) && (isempty(token_cel{end})) )
    % EMPTY TOKEN AT THE END: Get rid of it.
    token_cel = token_cel(1:end-1);
end

% READY TO PROCESS INDIVIDUAL CASES:

if ( from_found_flag && to_found_flag)
    % FROM and TO: expect 2 dates after each keyword
    from_str_ndx = strfind(req_date_str_ori,'FROM');
    to_str_ndx   = strfind(req_date_str_ori,'TO');
    from_date_str   = req_date_str_ori(from_str_ndx+5:to_str_ndx-1);
    to_date_str     = req_date_str_ori(to_str_ndx  +2:end     );
    try
        date_from_request_num                       = datenum(from_date_str);
        date_to_request_num                         = datenum(to_date_str);
        req_date_struct.start_day_resol_date_str    = datestr(date_from_request_num,date_str_to_day_format);
        req_date_struct.end_day_resol_date_str      = datestr(date_to_request_num,  date_str_to_day_format);
        req_date_struct.start_sec_resol_date_str    = datestr(date_from_request_num,date_str_to_sec_format);
        if (~(isdatetime(to_date_str)))
            % TO is a DATE (ie to day) not a datetime: (i.e. to SS)
            date_to_request_num                     = addtodate(date_to_request_num, +1, 'd');
            req_date_struct.end_sec_resol_date_str  = datestr(date_to_request_num,  date_str_to_sec_format);
        end
        return;
    catch
        lprintf_log('\n_error_: exception_thrown_function_ parse_date_request. FROM and TO:\n');                 
        error_message = sprintf('Invalid date request: FROM %s TO %s See help for all the valid date formats',from_date_str,to_date_str);
        return;
    end
end


if ( from_found_flag && (~to_found_flag))
    % FROM and ~TO: expect 1 dates after FROM keyword
    
    from_str_ndx    = strfind(req_date_str_ori,'FROM');
    from_date_str   = req_date_str_ori(from_str_ndx+5:end);
    try
        date_from_request_num                       = datenum(from_date_str);
        req_date_struct.start_day_resol_date_str   = datestr(date_from_request_num,date_str_to_day_format);
        req_date_struct.end_day_resol_date_str      = '';
        req_date_struct.start_sec_resol_date_str    = datestr(date_from_request_num,date_str_to_sec_format);
        req_date_struct.end_sec_resol_date_str      = '';
        return;
    catch
        error_message = sprintf('Invalid date request: FROM %s  See help for all the valid date formats',from_date_str);
        return;
    end
end


if ( (~(from_found_flag) && to_found_flag))
    % ~FROM and TO: expect 1 date after TO keyword
    to_str_ndx    = strfind(req_date_str_ori,'TO');
    to_date_str   = req_date_str_ori(to_str_ndx+3:end);
    try
        date_to_request_num                       = datenum(to_date_str);
        req_date_struct.end_day_resol_date_str    = datestr(date_to_request_num,date_str_to_day_format);
        req_date_struct.end_sec_resol_date_str    = datestr(date_to_request_num,date_str_to_sec_format);
        req_date_struct.start_day_resol_date_str  = ''; % the api will return the first experiment found        
        req_date_struct.start_sec_resol_date_str  = ''; % it is unknown when the fist exp it is acutally!.
        return;
    catch
        lprintf_log('\n_error_: exception_thrown_function_ parse_date_request. ~FROM and TO :\n');                         
        error_message = sprintf('Invalid date request: TO %s  See help for all the valid date formats',to_date_str);
        return;
    end
end

if ( kw1_found_flag)
    %  KEYWORD_1
    [ start_to_day_date_str, end_to_day_date_str,start_to_sec_date_str, end_to_sec_date_str] = parse_date_keyword_1(req_date_str_ori); % 'TODAY');
    req_date_struct.start_day_resol_date_str    = start_to_day_date_str;
    req_date_struct.end_day_resol_date_str      = end_to_day_date_str;
    req_date_struct.start_sec_resol_date_str    = start_to_sec_date_str;
    req_date_struct.end_sec_resol_date_str      = end_to_sec_date_str;
    return;
end
if ( kw2_found_flag)
    %  KEYWORD_2
    [ start_to_day_date_str, end_to_day_date_str,start_to_sec_date_str, end_to_sec_date_str] = parse_date_keyword_2(req_date_str_ori); % 'LAST WEEK');
    req_date_struct.start_day_resol_date_str    = start_to_day_date_str;
    req_date_struct.end_day_resol_date_str      = end_to_day_date_str;
    req_date_struct.start_sec_resol_date_str    = start_to_sec_date_str;
    req_date_struct.end_sec_resol_date_str      = end_to_sec_date_str;
    return;
end
if ( kw3_found_flag)
    %  KEYWORD_3
    [ start_to_day_date_str, end_to_day_date_str,start_to_sec_date_str, end_to_sec_date_str] = parse_date_keyword_3(req_date_str_ori); % 'LAST 2 HOURS');
    req_date_struct.start_day_resol_date_str    = start_to_day_date_str;
    req_date_struct.end_day_resol_date_str      = end_to_day_date_str;
    req_date_struct.start_sec_resol_date_str    = start_to_sec_date_str;
    req_date_struct.end_sec_resol_date_str      = end_to_sec_date_str;
    return;
end

error_message = sprintf('Invalid date request: %s  See help for all the valid date formats',req_date_str_ori);

end % fn parse_date_request

