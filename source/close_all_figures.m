% ref: to close all figure windows:
% source: https://stackoverflow.com/questions/38973332/how-to-close-all-graphs-in-gui-without-closing-the-gui-itself


fh=findall(0,'Type','Figure');
close(setxor(fh,Sensogram_Analysis_App_10));
