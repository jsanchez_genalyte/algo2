% LOCATE_FEATURES_TEST script to test locate_features function.
% LOCATE_FEATURES locates features from a TIFF gray scale image with sparse features
% Cleans the image noise while preserving  contents:  
% INPUT ARGUMENTS

im_tiff_gray_file_path = 'C:\Users\jsanchez\Documents\algo2\ml_test_algo\2018_test_files\test_1.jpg'; % input  file path

%min_rect_side_px
%min_contrast
[success_fg] = locate_features( im_tiff_gray_file_path); 

