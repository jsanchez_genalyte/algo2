function [aggr_struct_1,error_message] = do_index_table( a_table,column_aggr_1_ndx )
%DO_INDEX_TABLE creates indices for 2 aggregation columns in a table
%   Input: the table and 2 indices into the columns of the table (the ones to be aggregated)
% output:aggr_struct_1 with these fields: see the end for desc.
%   1 -  2 lists of unique values from the table.
%   2 -  2 tables: the col_i_table_ndx: as many cols as unique vals. As many cols as the orig table: a_table.

%   [aggr_struct_1,error_message] = do_index_table( assay_sum_table_qc,9 ); % col_9 == chan
error_message = '';
aggr_struct_1 = {};
% ,aggr_struct_2, aggr_struct_3,
% ,column_aggr_2_ndx,column_aggr_3_ndx
% aggr_struct_2 = {};
% aggr_struct_3 = {};
all_data_size = height(a_table);

a_table_col     = a_table(:,column_aggr_1_ndx);

% % % if (iscell(a_table_col(:,1))) %(strcmp(char(col_values_table.Properties.VariableNames),'qc_rack_number'))
% % %     % SPECIAL CASE: cells.
% % %     col_values_1_cat	= categorical(table2cell(a_table_col(:,1)));
% % % else
% % %     % REGULAR CASE: strings.
% % %     col_values_1_cat  	= categorical(table2array(a_table_col(:,1)));
% % % end


col_cat         = categorical(cellstr(num2str(cell2mat(table2cell(a_table_col))))); %  .chann))));
col_cat_names   = unique(col_cat);

col_cat_names_cel =  cellstr(col_cat_names);
% Make sure nan is not a category:
[nan_found_flag,nan_found_ndx]= is_element_in_cell(col_cat_names_cel,'NaN');
if (nan_found_flag)
    % NAN FOUND: Get rid of it, It is not a valid category !
    if (nan_found_ndx ==1)
        xcol_cat_names_cel = col_cat_names_cel(2:end);
    else
        if (nan_found_ndx ==size(col_cat_names_cel,1))
            xcol_cat_names_cel = col_cat_names_cel(1:end-1);
        else
            xcol_cat_names_cel = [ col_cat_names_cel(1:nan_found_ndx-1) ;  col_cat_names_cel(nan_found_ndx+1:end) ];
        end
    end
    xcol_cat_names   = categorical(xcol_cat_names_cel);
    col_cat_names    = xcol_cat_names;
end

col_cat_size    = size(col_cat_names,1);
col_ndx_mat     = nan(all_data_size,col_cat_size+1); % first col is all. cols 2 to n+1 are singles.

% build index for each unique value of the aggregation column
for cur_ndx = 1:1:col_cat_size
    %
    cur_value                  = col_cat_names(cur_ndx);
    col_ndx_mat(:,cur_ndx+1)   = col_cat  == cur_value;
end

% build index for "all" or both aggregation column:
if (col_cat_size>1)
    % MORE THAN ONE UNIQUE VALUE: calc ndx for "both" or "all" and store it on the first column.
    % as the "OR" of any of the values: As all indices are zero or 1: then nanmax does it.
    temp            = col_ndx_mat(:,2:(col_cat_size+1));
    col_ndx_mat(:,1)= nanmax(temp,[],2) ; %
end
if (col_cat_size == 1)
    % ONE UNIQUE VALUE: calc ndx for "both" or "all" as a clone of the unique index
    col_ndx_mat(:,1)= col_ndx_mat(:,2); %
end
col_cat_size_1                      = col_cat_size+1;           % num of unique values plus 1: for all of them
aggr_struct_1.col_cat_size          = col_cat_size_1;               
aggr_struct_1.col_cat_names = cell(col_cat_size_1,1);
aggr_struct_1.col_cat_names{1} = 'ALL____';
aggr_struct_1.col_cat_names(2:end) = cellstr(char(col_cat_names));% list of unique names for these values
aggr_struct_1.col_ndx_mat           = logical(col_ndx_mat);         % matrix of indices: one col per unique val.

end % fn do_index_table

