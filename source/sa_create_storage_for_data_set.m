function [ data_set_struct_list  ] = ...
    sa_create_storage_for_data_set( db_input_struct )
%SA_CREATE_STORAGE_FOR_DATA_SET   creates a list structs to start brand new loaded data_set.
%   Splits the excel data into x structs:  one per analyte.  and initializes them with loaded excel data from the db.
%   
% pipeline: do_open_db_local_file --> create_storage_for_data_set
sa_declare_globals; 

probe_found_cnt           = length(db_input_struct);

for cur_probe = 1:1:probe_found_cnt
    material_name           = db_input_struct{cur_probe}.material_name;
    g_db_set                = nan(g_rows_db_cnt,(db_cols_cnt + ui_total_fg_cnt));
    g_db_set                = db_input_struct{cur_probe}.material_type;
    con_dilution_name       = db_input_struct{cur_probe}.con_dilution_name;
    con_dilution_array      = db_input_struct{cur_probe}.con_dilution_array;
    candidate_names         = db_input_struct{cur_probe}.candidate_names;
    candidate_matrix        = db_input_struct{cur_probe}.candidate_matrix;
    
 	probe_dates             =  nan(g_rows_db_cnt,1);                              % dates 
   
    
    data_set_struct_list{cur_probe} = cell(material_type_ref_cnt,1);
end %  
end %fn sa_create_storage_for_data_set