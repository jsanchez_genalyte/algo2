function [ exp_list_acc_cel ] = exp_list_cel_flatten_specimen_id( exp_list_acc_cel )
%EXP_LIST_CEL_FLATTEN_SPECIMEN_ID flattens a cell array of structs with one or 2 specimen_ids into 2 fileds: specimen_id and specimen_id_2
%   If there is only one specimen_id: both get the same value.
%   If there are 2: the first get the first and the second gets the second but as a new fields
%   The only purpose of this function is being able to cve the cell array of strucs into a table
%   without having matlabe beaching about not being able to handle it: struct2table

if (isempty(exp_list_acc_cel{1}))
    return; % nothing to concatente
end

if (size(exp_list_acc_cel,1) < size(exp_list_acc_cel,2))
    % ROW ORIENTED: Force it to be columnwhise. Trans pose it.
    exp_list_acc_cel = exp_list_acc_cel';
end

exp_set_cnt = size(exp_list_acc_cel,1);
for cur_exp_set_ndx =1:1:exp_set_cnt
    cur_exp_struct = exp_list_acc_cel{cur_exp_set_ndx};
    cur_exp_struct.specimen_id_2 = {};
    if (~(isempty(cur_exp_struct.specimen_id)))
        if (size(cur_exp_struct.specimen_id,1) == 2)
            % DUAL SPECIMEN: copy each on corresponding field.
            cur_exp_struct.specimen_id_2 = {cur_exp_struct.specimen_id{2}}; % flatten fields
            cur_exp_struct.specimen_id   = {cur_exp_struct.specimen_id{1}};
        else
            % SINGLE SPECIMEN: copy it on both fields
            cur_exp_struct.specimen_id_2 = cur_exp_struct.specimen_id ; % {cur_exp_struct.specimen_id{1}}; % flatten fields
            %cur_exp_struct.specimen_id  = {cur_exp_struct.specimen_id{1}};            
        end
    end
    % STRUCT HAS NOW 2 SPECIMEN FIELDS:
    % save it back the struct with 2 clean specimen fields: specimen_id and specimen_id_2
    exp_list_acc_cel{cur_exp_set_ndx} = cur_exp_struct;
end

end % exp_list_cel_flatten_specimen_id

