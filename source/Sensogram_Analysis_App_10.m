function varargout = Sensogram_Analysis_App_10(varargin)
% SENSOGRAM_ANALYSIS_APP_10 MATLAB code for Sensogram_Analysis_App_10.fig
%      SENSOGRAM_ANALYSIS_APP_10, by itself, creates a new SENSOGRAM_ANALYSIS_APP_10 or raises the existing
%      singleton*.   jas_here: stopping point to do marketing (jan-31): sa_do_push_top_sens 
%       h = matlab.desktop.editor.getAll; h.close
%       delete(findall(0));   %deletes all open figures even the SensogramAnal GUI 
%       % Delete all Figures with an empty FileName property
%         delete(findall(groot, 'Type', 'figure', 'FileName', []));
% fake_data_location: excel_file_set_dir_01 = 'C:\Users\jsanchez\Documents\data_2\luis\SP\SP-1\TraceData\CL160505-12-Array-SP-01\';
% fake_data_protocol: ANA Multiplex (12 Chip) 1.1 QC-Prod   12_12_2017_luis 4.4  5.4 7.7 8.2
% sc_fill_in_recipe_and_analysis_time.m:    analysis_time_array(time_analysis_bl_beg_mea_ndx      ) = 5.1; % 4; % ndx of analysis tbdone jas_hard_coded_temp (_aaron_correction)
% sc_fill_in_recipe_and_analysis_time.m:    analysis_time_array(time_analysis_bl_end_mea_ndx      ) = 5.6; % 5; % ndx of

% fake_data_load:   fake_input_data.m 
%      H = SENSOGRAM_ANALYSIS_APP_10 returns the handle to a new SENSOGRAM_ANALYSIS_APP_10 or the handle to      the existing singleton*.
%      cd 'C:\Users\jsanchez\Documents\algo2\source'
%      cd 'C:\Users\jsanchez\Documents\fcc\source'
%      SENSOGRAM_ANALYSIS_APP_10('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SENSOGRAM_ANALYSIS_APP_10.M with the given input arguments.
%
%      SENSOGRAM_ANALYSIS_APP_10('Property','Value',...) creates a new SENSOGRAM_ANALYSIS_APP_10 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Sensogram_Analysis_App_10_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Sensogram_Analysis_App_10_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
% see: DATA_FILTER_ALGORITHM:  mpopup_menu_policy file: mpopup_menu_upd_list_item_slected
% Edit the above text to modify the response to help Sensogram_Analysis_App_10

% Last Modified by GUIDE v2.5 30-Jan-2018 13:50:58
% jas_code:
%sa_declare_globals;                        % *** ALL APP GLOBALS ARE declared HERE ******
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Sensogram_Analysis_App_10_OpeningFcn, ...
    'gui_OutputFcn',  @Sensogram_Analysis_App_10_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Sensogram_Analysis_App_10 is made visible.
function Sensogram_Analysis_App_10_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Sensogram_Analysis_App_10 (see VARARGIN)

%jas_temp
% sa_declare_globals;
% sa_init_globals;                      % __________ init globals ____________
% % Define Processing Input Arguments
% global_figure_start_no          = 1;      % starting number of figure: All figures will be number after this value.
% tr_delta_step_threshold         = 2.5;    % Maximum admisible difference between the 2 TR values in a channel for which TR values are consider sane (usable)
% wanted_inflexion_point_count    = 2;      % Want to find the 2 largest inflexion points: IE the ones followed by the largest going up values
% tr_enable_normalize_flag        = true;   % true; % == Do TR normalize; % false; == Do not normalize
%
% main_init_globals;

%display(varargin)  jas_deploy
% Initialize "non-gui" app:
% jas_temp
set(0,'DefaultUIControlFontSize',12);       % jas_experiment back to font 8 if you do not like it
%start_up_source_code_dir = pwd();      	% get source_code's real dir
% Define input Parameters:
if isempty(varargin)
    % CFG FILE WAS not PASSED AS ARGUMENT: Use this hardcoded one:
    cfg_parameters_file_name =  'sa_cfg';   %   strcat(pwd,std_curve_cfg); %
else
    % CFG FILE WAS PASSED AS ARGUMENT:      Use to initialize configuration: (file_name without path)
    cfg_parameters_file_name    = varargin{1};
end
[ input_parameters_struct ]                             = sa_read_cfg_parameters_file( cfg_parameters_file_name );
[ db_data_set_struct,db_data_exp_struct,ui_state_struct]= sa_init_cells_n_structs(  );
handles.ui_state_struct                                 = ui_state_struct;
[ handles ]                                             = sa_init_env_app( handles,input_parameters_struct );
[ handles ]                                             = do_clear_axes_all_plots( handles );

handles.db_data_set_struct                              = db_data_set_struct;% Future: this may have to move out of the handles (big data)
handles.db_data_exp_struct                              = db_data_exp_struct;% Future: this may have to move out of the handles (big data)


% create TOE
[toe_cell,toe_col_struct]	= toe_create(100); %  %jas_hardcoded. future.   sum(handles.ui_state_struct.db_cur_exp_net_ndx); twin of db_cur_fit_net_ndx
handles.toe_col_struct      = toe_col_struct;
handles.toe_cell            = toe_cell;        % tos_cell has the full_db_exp_set (empty to start with!)

%
% Choose default command line output for Sensogram_Analysis_App_10
handles.output = hObject;
%
% % warning off % jas_deploy: remove_comment for this line.
% % Update handles structure
guidata(hObject, handles);
%
% DO: ONE TIME Initialize of app DURING OPENING: _______________________________
% handles                         = sa_opening(hObject,handles); %jas_temp

guidata(hObject, handles);

% UIWAIT makes Sensogram_Analysis_App_10 wait for user response (see UIRESUME)
% uiwait(handles.figure_sens_Anal_app_10);


% --- Outputs from this function are returned to the command line.
function varargout = Sensogram_Analysis_App_10_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox_update_good_and_anom_sens.
function checkbox_update_good_and_anom_sens_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_update_good_and_anom_sens (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of checkbox_update_good_and_anom_sens



function edit_sens_anom_per_plot_Callback(hObject, eventdata, handles)
% hObject    handle to edit_sens_anom_per_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: get(hObject,'String') returns contents of edit_sens_anom_per_plot as text
%        str2double(get(hObject,'String')) returns contents of edit_sens_anom_per_plot as a double


% --- Executes during object creation, after setting all properties.
function edit_sens_anom_per_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sens_anom_per_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_anom_sens_env.
function checkbox_anom_sens_env_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_anom_sens_env (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of checkbox_anom_sens_env


% --- Executes on button press in checkbox_anom_sens_avg.
function checkbox_anom_sens_avg_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_anom_sens_avg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of checkbox_anom_sens_avg


% --- Executes on button press in checkbox_anom_sens_med.
function checkbox_anom_sens_med_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_anom_sens_med (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of checkbox_anom_sens_med


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_


% --- Executes during object creation, after setting all properties.
function edit_sens_good_per_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sens_good_per_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_good_sens_env.
function checkbox_good_sens_env_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_good_sens_env (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of checkbox_good_sens_env
handles.ui_state_struct.checkbox_good_sens_env  = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in checkbox_good_sens_med.
function checkbox_good_sens_med_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_good_sens_med (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of checkbox_good_sens_med
handles.ui_state_struct.checkbox_good_sens_med  = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --- Executes on selection change in popupmenu12.
function popupmenu12_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu12 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu12


% --- Executes during object creation, after setting all properties.
function popupmenu12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu9.
function popupmenu9_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu9 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu9
% sa_declare_globals ; % out_xx_

% --- Executes during object creation, after setting all properties.
function popupmenu9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu10.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu10 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu10

% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu11.
function popupmenu11_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu11 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu11


% --- Executes during object creation, after setting all properties.
function popupmenu11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% sa_declare_globals ; % out_xx_

% --- Executes on selection change in popupmenu13.
function popupmenu13_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu13 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu13


% --- Executes during object creation, after setting all properties.
function popupmenu13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu14.
function popupmenu14_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu14 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu14


% --- Executes during object creation, after setting all properties.
function popupmenu14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton11.
function radiobutton11_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of radiobutton11


% --- Executes on button press in radiobutton_10.
function radiobutton_10_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of radiobutton_10


% --- Executes on selection change in popupmenu_chipl.
function popupmenu_chipl_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_chipl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index                 = get(handles.popupmenu_chipl,   'Value');
% For the new current chiplot update the rest of the popup  menu with the corresponding quantities
ui_chipl_state_struct               = handles.ui_state_struct.chipl;
[ change_flag,ui_chipl_state_struct]= mpopup_menu_upd_list_item_slected( ui_chipl_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.chipl       = { ui_chipl_state_struct };
if (ui_chipl_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end

if (change_flag)
    % USER SELECTED A NEW CHIPLOT.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.chipl{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                             = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                              = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]             = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                               = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles); 
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end



% --- Executes during object creation, after setting all properties.
function popupmenu_chipl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_chipl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_exper.
function popupmenu_exper_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_exper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index                 = get(handles.popupmenu_exper,   'Value');
% For the new current analyte update the rest of the popup  menu with the corresponding quantities
ui_exper_state_struct               = handles.ui_state_struct.exper;
[ change_flag,ui_exper_state_struct]= mpopup_menu_upd_list_item_slected( ui_exper_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.exper       = { ui_exper_state_struct };
if (ui_exper_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW EXPERIMENT.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.exper{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles); 
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function popupmenu_exper_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_exper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_instr.
function popupmenu_instr_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_instr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index              	= get(handles.popupmenu_instr,   'Value');
% For the new current analyte update the rest of the popup  menu with the corresponding quantities
ui_instr_state_struct             	= handles.ui_state_struct.instr;
[ change_flag,ui_instr_state_struct]= mpopup_menu_upd_list_item_slected( ui_instr_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.instr     	= { ui_instr_state_struct };
if (ui_instr_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW INSTRUMENT.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.instr{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status );
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function popupmenu_instr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_instr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_carri.
function popupmenu_carri_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_carri (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index              	= get(handles.popupmenu_carri,   'Value');
% For the new current analyte update the rest of the popup  menu with the corresponding quantities
ui_carri_state_struct            	= handles.ui_state_struct.carri;
[ change_flag,ui_carri_state_struct]= mpopup_menu_upd_list_item_slected( ui_carri_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.carri    	= { ui_carri_state_struct };
if (ui_carri_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW CARRIER.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.carri{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function popupmenu_carri_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_carri (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_chann.
function popupmenu_chann_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_chann (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index                 = get(handles.popupmenu_chann,   'Value');
% For the new current channel update the rest of the popup  menu with the corresponding quantities
ui_chann_state_struct               = handles.ui_state_struct.chann;
[change_flag,ui_chann_state_struct] = mpopup_menu_upd_list_item_slected( ui_chann_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.chann       = { ui_chann_state_struct };
if (ui_chann_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW CHANNEL.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.chann{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function popupmenu_chann_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_chann (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_clust.
function popupmenu_clust_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_clust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index                 = get(handles.popupmenu_clust,   'Value');
% For the new current cluster  update the rest of the popup  menu with the corresponding quantities
ui_clust_state_struct               = handles.ui_state_struct.clust;
[change_flag,ui_clust_state_struct] = mpopup_menu_upd_list_item_slected( ui_clust_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.clust       = { ui_clust_state_struct };
if (ui_clust_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW CLUSTER.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.clust{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end   

% --- Executes during object creation, after setting all properties.
function popupmenu_clust_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_clust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_datat.
function popupmenu_datat_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_datat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_datat contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_datat


% --- Executes during object creation, after setting all properties.
function popupmenu_datat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_datat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_ampli.
function popupmenu_ampli_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_ampli (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_ampli contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_ampli


% --- Executes during object creation, after setting all properties.
function popupmenu_ampli_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_ampli (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_probe.
function popupmenu_probe_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_probe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_probe contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_probe
% BEHAVIOR: When an analyte is selected, the rest of the popup menus shoud be updated: consistency
% the candidate popup candidates menu items MUST match the corresponding analyte.
% Action:   Update filters: first for probe and then for each other popup so they get filterout accordingly (same as excel filter)
% Build list of candidates for the selected analyte and update the gui for the candidate popup menu.
popupmenu_sel_index             	= get(handles.popupmenu_probe,   'Value');
% For the new current analyte update the rest of the popup  menu with the corresponding quantities: ie protocol x/y
ui_probe_state_struct            	= handles.ui_state_struct.probe;
[change_flag,ui_probe_state_struct] = mpopup_menu_upd_list_item_slected( ui_probe_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.probe       = { ui_probe_state_struct }; 
if (ui_probe_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW ANALYTE.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    

    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices(handles.ui_state_struct.db_cur_fit_net_ndx...
        ,handles.db_data_set_struct.db_metad_set_struct.clasi,handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end


% --- Executes during object creation, after setting all properties.
function popupmenu_probe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_probe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_dates.
function popupmenu_dates_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_dates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_dates contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_dates
handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe

% --- Executes during object creation, after setting all properties.
function popupmenu_dates_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_dates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_proto.
function popupmenu_proto_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_proto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index                 = get(handles.popupmenu_proto,   'Value');
% For the new current protocol update the rest of the popup  menu with the corresponding quantities
ui_proto_state_struct               = handles.ui_state_struct.proto;

temp = ui_proto_state_struct{1};
if (length(temp.list_item_slected) == 1)
    display ('wrong size');
end

[change_flag,ui_proto_state_struct] = mpopup_menu_upd_list_item_slected( ui_proto_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.proto       = { ui_proto_state_struct };
if (ui_proto_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW PROTOCOL.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    % handles.ui_state_struct.proto{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function popupmenu_proto_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_proto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_sampl.
function popupmenu_sampl_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_sampl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
popupmenu_sel_index                 = get(handles.popupmenu_sampl,   'Value');
% For the new current sampl update the rest of the popup  menu with the corresponding quantities
ui_sampl_state_struct               = handles.ui_state_struct.sampl;
[ change_flag,ui_sampl_state_struct]= mpopup_menu_upd_list_item_slected( ui_sampl_state_struct{1},popupmenu_sel_index );
handles.ui_state_struct.sampl       = { ui_sampl_state_struct }; 
if (ui_sampl_state_struct.list_item_slected(1))
    [ handles ] = mpopup_menu_reset_all_to_all( handles );
end
if (change_flag)
    % USER SELECTED A NEW SAMPLE.
    handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after changing filters : play safe
    %handles.ui_state_struct.sampl{1}.list_ndx_filt_ori_dat = new_list_ndx_filt_dat;
    % update all the popupmenus after popup change:
    [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );
    handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                      = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % build indices based on classification and ui to be used by plots/tos/stats:
    [ handles.ui_state_struct ]     = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]                       = ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function popupmenu_sampl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_sampl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% sa_declare_globals ; % out_xx_
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton_local_file.
function radiobutton_local_file_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_local_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of radiobutton_local_file


% --- Executes on button press in radiobutton_db.
function radiobutton_db_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_db (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Hint: get(hObject,'Value') returns toggle state of radiobutton_db


% --- Executes on button press in pushbutton_get_data.
function pushbutton_get_data_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_get_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%jas_temp
% sa_declare_globals ; % out_xx_
% [ db_input_struct ] = fake_input_data(handles.input_parameters_struct);
if (0)
    % [handles] = fake_input_data(handles);           % main_algo_init_gb,db_data_set_struct,tos_cell,ui_state_struct)
    handles.ui_state_struct.db_session_opened   = 1;     % flag to tell that have a loaded/opened session.
    handles.ui_state_struct.cur_session_name    = {''};
    handles.ui_state_struct.local_input_dir     = {handles.input_parameters_struct.local_input_dir}; % use the excel dir to start with (poor man's option)
    [handles]                                   = sa_ui_set_env_after_get_data( handles );
    
    guidata(hObject, handles); % Update handles structure  FDBK to end-user: cnts of loaded data.
    % do dlg: if  ( max(senso_curves_perp_cnt+anomn_curves_per_plot_cnt) > 20 ) do dlg: Classiff Senso ... Wait ..
    
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                  = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % create TOS
    handles.ui_state_struct.ui_request_flag = 'g_n_a'; % by default start with good and anomalous.
    [tos_cell,tos_col_struct]	= tos_create(handles.db_data_set_struct.rows_db_cnt{1}); % future.   sum(handles.ui_state_struct.db_cur_fit_net_ndx);
    handles.tos_col_struct      = tos_col_struct;
    handles.tos_cell            = tos_cell; % tos_cell has the full_db_data_set
    
    % Save the virigin full tos_cell (untochable!)
    [handles]                   = ui_do_fill_tos_sens_set(handles);
    
    % build indices to be used by plot and tos
    % 3 cases: G only A only or G&A visible on the screen.
    if (handles.ui_state_struct.checkbox_hide_anom) % hide anom is on
        handles.ui_state_struct.ui_request_flag = 'g_only';
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_full_pos );
        set(handles.axes_anom_sens,'Visible','off' );
        cla(handles.axes_anom_sens);
        guidata(hObject, handles);
        set(handles.axes_good_sens,'Visible','on' );
        
    end
    if (handles.ui_state_struct.checkbox_hide_good) % hide good is on
        handles.ui_state_struct.ui_request_flag = 'a_only';
        set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_full_pos );
        set(handles.axes_good_sens,'Visible','off' );
        cla(handles.axes_good_sens);
        guidata(hObject, handles);
        set(handles.axes_anom_sens,'Visible','on' );
    end
    if ( (~(handles.ui_state_struct.checkbox_hide_good)) && (~(handles.ui_state_struct.checkbox_hide_anom) )) % hide anom is off and  hide good is off
        handles.ui_state_struct.ui_request_flag = 'g_n_a';
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_half_pos );
        set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_half_pos );
        set(handles.axes_good_sens,'Visible','on' );
        guidata(hObject, handles);
        set(handles.axes_anom_sens,'Visible','on' );
    end
    handles.ui_state_struct	= ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]              	= ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
end % if (0)
guidata(hObject, handles); % Update handles structure
%[handles ] = sc_classify_sensograms(handles); %       noi: move_ahead_doing_classification
%guidata(hObject, handles); % Update handles structure

% --------------------------------------------------------------------
function file_menu_Callback(hObject, eventdata, handles)
% hObject    handle to file_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function edit_menu_Callback(hObject, eventdata, handles)
% hObject    handle to edit_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_menu_Callback(hObject, eventdata, handles)
% hObject    handle to view_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function analysis_menu_Callback(hObject, eventdata, handles)
% hObject    handle to analysis_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function thresholds_menu_Callback(hObject, eventdata, handles)
% hObject    handle to thresholds_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function help_menu_Callback(hObject, eventdata, handles)
% hObject    handle to help_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function help_about_sc_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to help_about_sc_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function help_help_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to help_help_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function threshold_1_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_1_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function threshold_2_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_2_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function threshold_3_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_3_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function analysis_norm_jervis_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to analysis_norm_jervis_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function analysis_norm_matlab_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to analysis_norm_matlab_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function analysis_classi_senso_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to analysis_classi_senso_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_auto_scale_good_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_auto_scale_good_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_manual_scale_good_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_manual_scale_good_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_auto_scale_anom_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_auto_scale_anom_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_manual_scale_anom_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_manual_scale_anom_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_plot_tr_good_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_plot_tr_good_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_plot_tr_anom_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_plot_tr_anom_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_plot_meas_reg_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_plot_meas_reg_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_plot_grid_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_plot_grid_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_plot_bline_circles_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_plot_bline_circles_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function view_plot_ampli_circles_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to view_plot_ampli_circles_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function edit_select_all_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to edit_select_all_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% jas_vac: code from std_curve to ve cloned:
ui_state_struct         = handles.ui_state_struct;
if (handles.db_data_set_struct.rows_db_cnt{1})
    % THERE IS ALREADY DATA FROM DB or FILE:
    % check if a session needs to be saved: I.E. If the table is not empty:
    handles = do_select_all_rows_menu_item(handles);
end % any data from db
guidata(hObject, handles);

% --------------------------------------------------------------------
function edit_sel_inv_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to edit_sel_inv_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function edit_copy_sel_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to edit_copy_sel_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
ui_state_struct         = handles.ui_state_struct;
%indices                 = eventdata.Indices;
%row_selected            = indices(:,1);
if (handles.db_data_set_struct.rows_db_cnt{1})
    % THERE IS ROOM: So the session is not empty: so selected row can be cut
    handles = do_copy_rows(handles); % row_selected
end % Excel file already oppened.
guidata(hObject, handles);   % Update handles structure: Jorge's addition to update ui

% --------------------------------------------------------------------
function edit_copy_window_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to edit_copy_window_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% CONCLUSION: Either use the snipping tool or try on my old laptop to see
% if it is my graphics drivers. 
print -dmeta                                % _ugly  wrong font size: font at wrong scale for all of them.
%print('-clipboard','-dbitmap' )  ;          % _ugly clipboardformat
%print   -opengl -r0  -dmeta     ;          % _ugly  -bestfit %  % copy window to the clipboard. -dpdf
 saveas(gcf,'SensogramCurFigure','jpeg')    % _ugly
% --------------------------------------------------------------------
function file_open_local_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to file_open_local_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
% Opens a local file: The whole session gets (optionally saved first) deleted
%                     and a new session gets loaded
%input_parameters_struct = handles.input_parameters_struct;
ui_state_struct         = handles.ui_state_struct;

if (ui_state_struct.db_session_opened)
    % THERE IS ALREADY A SESSION FILE OPENED:
    % check if a session needs to be saved: I.E. If the table is not empty:
    [ cancel_flag,handles ] = sa_do_save_session_dlg(handles,ui_state_struct.cur_session_name );
    if (cancel_flag)
        % User decided to cancel.
        return;
    end
end % Session file already oppened.
cd(ui_state_struct.local_input_dir{1});

[input_file_name,local_input_dir] = uigetfile('*.mat','Select a SENSOGRAM SESSION DATA FILE to open. (MATLAB format) ');
if (~(input_file_name))
    % USER CANCELED OR CLOSED THE DLG: get out
    return;
end

if (~isempty(local_input_dir))
    cd(local_input_dir);
end

% HAVE AN INPUT  SESSION FILE TO WORK WITH: Open it.
filename_matlab                     = strcat(local_input_dir,input_file_name);
% Check that the file exist_exist
matlab_session_file_found           = dir(input_file_name);
if (isempty(matlab_session_file_found))
    % LET USER KNOW THAT FILE IS DOES NOT EXIST:
    warning_msg = sprintf('SESSION FILE NOT FOUND. File name: %s.',input_file_name);
    warndlg(warning_msg,' LOADING SESSION FILE  ...');
    return;
end
ui_state_struct.input_file_name     = { input_file_name};
ui_state_struct.input_data_type     = {'matlab_file' };
ui_state_struct.local_input_dir     = { local_input_dir };
% Load Session File but first clean up current stuff:
delete(handles.figure_sens_Anal_app_10)         % delete current figure                       ****** whipes out everything
%guidata(hObject, handles);     % Update handles structure
load (filename_matlab,'-mat');  % reload new handles (from the saved session) ****** reloads everything.
%guidata(hObject, handles);     % can not do any statement because context was lost. But is is ok
% --------------------------------------------------------------------
function file_save_db_local_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to file_save_db_local_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
[ cancel_flag,handles ] = sa_do_save_session_dlg(handles, handles.ui_state_struct.cur_session_name);
if (cancel_flag)
    % User decided to cancel.
    return;
end
guidata(hObject, handles); 
% --------------------------------------------------------------------
function file_append_sel_local_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to file_append_sel_local_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_  jas_tbd think about meaning of append:
% does the user needs it: Really? 

% --------------------------------------------------------------------
function file_new_local_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to file_new_local_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function file_exit_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to file_exit_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_
ui_state_struct         = handles.ui_state_struct;
if (ui_state_struct.db_session_opened)
    % THERE IS ALREADY A SESSION FILE OPENED:
    % check if a session needs to be saved: I.E. If the table is not empty:
    [ cancel_flag,handles ] = sa_do_save_session_dlg(handles,ui_state_struct.cur_session_name );
    if (cancel_flag)
        % User decided to cancel.
        return;
    end
    warning on; % jas_deploy    
end % Session file already oppened.
cd(ui_state_struct.local_input_dir{1});
% USER CONFIRMED EXIT: EITHER SAVING OR NOT SAVING... BUT IT DID NOT CANCEL:
% Get out of here: Final Clean up.
guidata(hObject, handles);   % Update handles structure: May not be needed ...
delete(handles.figure_sens_Anal_app_10)      % fn exit_menu

% --------------------------------------------------------------------
function ui_toggle_tool_auto_manual_axis_OffCallback(hObject, eventdata, handles)
% hObject    handle to ui_toggle_tool_auto_manual_axis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function ui_toggle_tool_auto_manual_axis_OnCallback(hObject, eventdata, handles)
% hObject    handle to ui_toggle_tool_auto_manual_axis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function ui_toggle_tool_legend_OffCallback(hObject, eventdata, handles)
% hObject    handle to ui_toggle_tool_legend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function ui_toggle_tool_legend_OnCallback(hObject, eventdata, handles)
% hObject    handle to ui_toggle_tool_legend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_

% --------------------------------------------------------------------
function edit_cut_sel_menu_item_Callback(hObject, eventdata, handles)
% hObject    handle to edit_cut_sel_menu_item (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% sa_declare_globals ; % out_xx_


% --- Executes when selected cell(s) is changed in ui_table_tos.
function ui_table_tos_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to ui_table_tos (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when entered data in editable cell(s) in ui_table_tos.
function ui_table_tos_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to ui_table_tos (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)



function edit_senso_per_plot_Callback(hObject, eventdata, handles)
% hObject    handle to edit_senso_per_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit_senso_per_plot as text
%        str2double(get(hObject,'String')) returns contents of edit_senso_per_plot as a double
senso_cnt_pplot_edit_str = get(hObject,'String');
if ( (strcmpi('All',senso_cnt_pplot_edit_str)) || (strcmpi('0',senso_cnt_pplot_edit_str)) )
    % user requested ALL plots: accounting from available filters)
    handles.ui_state_struct.senso_start_ndx = 1;
    handles.ui_state_struct.senso_curves_perp_cnt =  sum(handles.ui_state_struct.db_cur_fit_net_ndx);
else
    if (~(isnan(str2double(senso_cnt_pplot_edit_str))))
        new_value = str2double(senso_cnt_pplot_edit_str);
        handles.ui_state_struct.senso_start_ndx = 1;       % ALWAYS RESET TO 1 after editing cpp : play safe
        if (new_value == handles.ui_state_struct.senso_curves_perp_cnt)
            % USER ENTERED THE SAME VALUE: Hint to restart
            handles.ui_state_struct.senso_start_ndx = 1;
        end
        % MAKE SURE THE VALUE IS NOT TOO LARGE:
        if (new_value > sum(handles.ui_state_struct.db_cur_fit_net_ndx))
            % VALUE TOO LARGE: Give warning to user: use status line and trim it.
            set(handles.text_status,'string',sprintf('Due to the current Data Filters, There are only  %-4d sensograms available. '...
                ,sum(handles.ui_state_struct.db_cur_fit_net_ndx)),'ForegroundColor',handles.ui_state_struct.color_orange_warning,'Fontsize',12);
            handles.ui_state_struct.senso_curves_perp_cnt = sum(handles.ui_state_struct.db_cur_fit_net_ndx);
        else
            % OK INPUT:
            handles.ui_state_struct.senso_curves_perp_cnt = new_value;
        end
    else
        % MEANINGLESS INPUT: Ignore it!. do not change anthing. Just give warning
        set(handles.text_status,'string',sprintf('Invalid Input:   %s   ignored. Enter an integer between 1 and %-4d '...
            ,senso_cnt_pplot_edit_str,sum(handles.ui_state_struct.db_cur_fit_net_ndx)),'ForegroundColor','red','Fontsize',12);
    end
end
% set back final value:
set(hObject,'String',handles.ui_state_struct.senso_curves_perp_cnt);
% and do the first or next plot:
% ui_state_struct.ui_request_flag= ,'g_n_a'; think about this.
% Classify sensograms to display on the screen according to which plots are up: good/anom or both
[handles ]                  = sc_classify_sensograms(handles); % classify only what is needed to display in screen
[handles.ui_state_struct ]  = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
    , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status );
[handles]                   = ui_do_plot_sens_cur(hObject,handles);
% Populate TOS on the UI just with what is selected:
[handles ] = sa_ui_update_tos(handles);
% move the ndx to the next bock
handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
if (isnan(handles.ui_state_struct.senso_start_ndx_next))
    % GOT TO THE END: roll over to the first one
    handles.ui_state_struct.senso_start_ndx = 1;
else
    handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
end
guidata(hObject, handles); % Update handles structure

% --- Executes during object creation, after setting all properties.
function edit_senso_per_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_senso_per_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% set back final value:
%set(hObject,'String',handles.ui_state_struct.senso_curves_perp_cnt);
guidata(hObject, handles); % Update handles structure

% --- Executes on button press in checkbox_good_sens_mea.
function checkbox_good_sens_mea_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_good_sens_mea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox_good_sens_mea
handles.ui_state_struct.checkbox_good_sens_mea  = get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in checkbox_good_sens_med.
function checkbox_good_sens_med2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_good_sens_med (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_good_sens_med


% --- Executes on button press in checkbox_good_sens_env.
function checkbox_good_sens_env2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_good_sens_env (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox_good_sens_env


% --- Executes on button press in checkbox_hide_good.
function checkbox_hide_good_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_hide_good (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox_hide_good
handles.ui_state_struct.checkbox_hide_good  = get(hObject,'Value');
if (handles.ui_state_struct.checkbox_hide_good)
    % USER HIDED GOOD PLOTS:
    handles.ui_state_struct.ui_request_flag = 'a_only';
    %  cla(handles.axes_good_sens);
    %  title_str                               = strcat(' GOOD SENSOGRAMS:   HIDEN');
    %  set(handles.axes_good_sens,'FontSize',12);
    %  title(handles.axes_good_sens,title_str,'Color',handles.ui_state_struct.color_green_nice);
    % HERE:jas_tbd: resize big anom plots.
    % Steps: good_invisible, anom full_position anom_visible
    set(handles.axes_good_sens,'Visible','off' );
    cla(handles.axes_good_sens);    
    guidata(hObject, handles);
    set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_full_pos );
    set(handles.axes_anom_sens,'Visible','on'  );
else
    % USER SHOWED ANOM PLOTS:
    if (~(handles.ui_state_struct.checkbox_hide_anom))
        % GOOD ON AND ANOM ON
        handles.ui_state_struct.ui_request_flag     = 'g_n_a';
        % HERE:jas_tbd: resize small good plots.    small anom plots.
        % show the SAME plot:        
        % show the SAME plot:  BETTER: reset to first plot:
        handles.ui_state_struct.senso_start_ndx = 1; % ALWAYS RESET TO 1 after changing filters : play safe
        handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
        % Classify sensograms to display on the screen according to which plots are up: good/anom or both
        [handles ]                              = sc_classify_sensograms(handles); % classify only what is needed to display in screen
        % build indices based on classification and ui to be used by plots/tos/stats:
        [ handles.ui_state_struct ]             = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
            , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
         set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_half_pos );
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_half_pos );
        set(handles.axes_anom_sens,'Visible','on' );
        guidata(hObject, handles);
        set(handles.axes_good_sens,'Visible','on' );
        [handles]                               = ui_do_plot_sens_cur(hObject,handles);
        % Populate TOS on the UI just with what is selected:
        [handles ] = sa_ui_update_tos(handles);              
        %set(handles.ui_table_tos,'Data',handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,:),'Visible','on','Enable','on');  % save_to_handles.ui_table_tos: Data
        % move the ndx to the next bock
        handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
        if (isnan(handles.ui_state_struct.senso_start_ndx_next))
            % GOT TO THE END: roll over to the first one
            handles.ui_state_struct.senso_start_ndx = 1;
        else
            handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
        end
        guidata(hObject, handles);                             
    else
        % GOOD ON AND ANOM OFF
        handles.ui_state_struct.ui_request_flag = 'g_only';
        % HERE:jas_tbd: resize big good plots. No need to redraw
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_full_pos );
        set(handles.axes_anom_sens,'Visible','off' );
        cla(handles.axes_anom_sens);
        guidata(hObject, handles);
        set(handles.axes_good_sens,'Visible','on' );
        % Populate TOS on the UI just with what is selected:
        [handles ] = sa_ui_update_tos(handles);
    end
end
guidata(hObject, handles);

% --- Executes on button press in checkbox_hide_anom.
function checkbox_hide_anom_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_hide_anom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox_hide_anom
handles.ui_state_struct.checkbox_hide_anom  = get(hObject,'Value');
if (handles.ui_state_struct.checkbox_hide_anom)
    % USER HIDED ANOM PLOTS:
    handles.ui_state_struct.ui_request_flag = 'g_only';
    %  cla(handles.axes_anom_sens);
    %  title_str                               = strcat(' ANOM SENSOGRAMS:   HIDEN');
    %  set(handles.axes_anom_sens,'FontSize',12);
    %  title(handles.axes_anom_sens,title_str,'Color','red');
    % HERE:jas_tbd: resize big good plots.
    % Steps: anom_invisible, good full_position good_visible
    set(handles.axes_anom_sens,'Visible','off' );
    cla(handles.axes_anom_sens);
    set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_full_pos );
    guidata(hObject, handles);
    set(handles.axes_good_sens,'Visible','on'  );
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
else
    % USER SHOWED ANOM PLOTS:
    if (~(handles.ui_state_struct.checkbox_hide_good))
        % GOOD ON AND ANOM ON
        handles.ui_state_struct.ui_request_flag     = 'g_n_a';
        % HERE:jas_tbd: resize small good plots.     small anom plots.
        % show the SAME plot:  BETTER: reset to first plot:
        handles.ui_state_struct.senso_start_ndx = 1; % ALWAYS RESET TO 1 after changing filters : play safe
        handles.ui_state_struct.senso_start_ndx = 1; % reset start_ndx
        % Classify sensograms to display on the screen according to which plots are up: good/anom or both
        [handles ]                              = sc_classify_sensograms(handles); % classify only what is needed to display in screen
        % build indices based on classification and ui to be used by plots/tos/stats:
        [ handles.ui_state_struct ]             = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
            , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
        set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_half_pos );
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_half_pos );
        guidata(hObject, handles);
        set(handles.axes_anom_sens,'Visible','on' );
        set(handles.axes_good_sens,'Visible','on' );
        [handles]                               = ui_do_plot_sens_cur(hObject,handles);
        % Populate TOS on the UI just with what is selected:
        [handles ] = sa_ui_update_tos(handles); 
        % move the ndx to the next bock
        handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
        if (isnan(handles.ui_state_struct.senso_start_ndx_next))
            % GOT TO THE END: roll over to the first one
            handles.ui_state_struct.senso_start_ndx = 1;
        else
            handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
        end
        guidata(hObject, handles);       
    else
        % GOOD OFF AND ANOM ON
        handles.ui_state_struct.ui_request_flag = 'a_only';
        % HERE:jas_tbd: resize big anom plots.
        set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_full_pos );
        set(handles.axes_good_sens,'Visible','off' );
        cla(handles.axes_good_sens);
        guidata(hObject, handles);        
        set(handles.axes_anom_sens,'Visible','on' );
        % Populate TOS on the UI just with what is selected:
        [handles ] = sa_ui_update_tos(handles); 
    end
end
guidata(hObject, handles);

% --- Executes on button press in checkbox_anom_sens_mea.
function checkbox_anom_sens_mea_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_anom_sens_mea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox_anom_sens_mea
handles.ui_state_struct.checkbox_anom_sens_mea  = get(hObject,'Value');
guidata(hObject, handles);

% --- Executes on button press in pushbutton_senso_next.
function pushbutton_senso_next_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_senso_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% build indices to be used by plot and tos
% Classify sensograms to display on the screen according to which plots are up: good/anom or both
[handles ] = sc_classify_sensograms(handles); % classify only what is needed to display in screen

[handles.ui_state_struct ]                  = ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
    , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
[handles]                               	= ui_do_plot_sens_cur(hObject,handles);
% Populate TOS on the UI just with what is selected: % added: 12_11_2017
[handles ] = sa_ui_update_tos(handles); 
% move the ndx to the next bock
handles.ui_state_struct.senso_start_ndx_save= handles.ui_state_struct.senso_start_ndx;
if (isnan(handles.ui_state_struct.senso_start_ndx_next))
    handles.ui_state_struct.senso_start_ndx	= 1;
else
    handles.ui_state_struct.senso_start_ndx	= handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
end


    

guidata(hObject, handles); % Update handles structure

% --- Executes on button press in checkbox_combine.
function checkbox_combine_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_combine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.ui_state_struct.checkbox_combine  = get(hObject,'Value');  %jas_temp niu jas_futre jas_tbd
% Hint: get(hObject,'Value') returns toggle state of checkbox_combine
guidata(hObject, handles);


% --- Executes on button press in pushbutton_top_expe.
function pushbutton_top_expe_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_top_expe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_top_expe(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes on button press in pushbutton_top_sens.
function pushbutton_top_sens_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_top_sens (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_top_sens(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes on button press in pushbutton_top_algo.
function pushbutton_top_algo_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_top_algo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_top_algo(hObject, eventdata, handles);
guidata(hObject, handles);


function edit_req_carr_Callback(hObject, eventdata, handles)
% hObject    handle to edit_req_carr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit_req_carr as text
%        str2double(get(hObject,'String')) returns contents of edit_req_carr as a double
handles = sa_do_edit_req_carr(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_req_carr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_req_carr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in text_req_spot.
function pushbutton_req_carr_Callback(hObject, eventdata, handles)
% hObject    handle to text_req_spot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_req_carr(hObject, eventdata, handles);
guidata(hObject, handles);

function edit_req_spot_Callback(hObject, eventdata, handles)
% hObject    handle to edit_req_spot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit_req_spot as text
%        str2double(get(hObject,'String')) returns contents of edit_req_spot as a double
handles = sa_do_edit_req_spot(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_req_spot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_req_spot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in text_req_carr.
function pushbutton_req_spot_Callback(hObject, eventdata, handles)
% hObject    handle to text_req_carr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_req_spot(hObject, eventdata, handles);
guidata(hObject, handles);


function edit_req_kitl_Callback(hObject, eventdata, handles)
% hObject    handle to edit_req_kitl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit_req_kitl as text
%        str2double(get(hObject,'String')) returns contents of edit_req_kitl as a double
handles = sa_do_edit_req_kitl(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_req_kitl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_req_kitl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in text_req_kitl.
function pushbutton_req_kitl_Callback(hObject, eventdata, handles)
% hObject    handle to text_req_kitl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_req_kitl(hObject, eventdata, handles);
guidata(hObject, handles);


function edit_req_expe_Callback(hObject, eventdata, handles)
% hObject    handle to edit_req_expe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit_req_expe as text
%        str2double(get(hObject,'String')) returns contents of edit_req_expe as a double
handles = sa_do_edit_req_expe(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_req_expe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_req_expe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_req_expe.
function pushbutton_req_expe_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_req_expe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_req_expe(hObject, handles); % hObject, eventdata,
guidata(hObject, handles);


function edit_req_prot_Callback(hObject, eventdata, handles)
% hObject    handle to edit_req_prot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_req_prot as text
%        str2double(get(hObject,'String')) returns contents of edit_req_prot as a double
handles = sa_do_edit_req_prot(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_req_prot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_req_prot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in text_req_prot.
function pushbutton_req_prot_Callback(hObject, eventdata, handles)
% hObject    handle to text_req_prot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_req_prot(hObject, eventdata, handles);
guidata(hObject, handles);


function edit_req_date_Callback(hObject, eventdata, handles)
% hObject    handle to edit_req_date (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_req_date as text
%        str2double(get(hObject,'String')) returns contents of edit_req_date as a double
handles = sa_do_edit_req_date(hObject, eventdata, handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_req_date_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_req_date (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_req_date.
function pushbutton_req_date_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_req_date (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = sa_do_push_req_date(hObject, eventdata, handles);
guidata(hObject, handles);
