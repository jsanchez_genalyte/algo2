function [ ui_pop_up_struct ] = sa_ui_create_popup_struc( list_str_cel_cur_dat,pop_up_name)
%SA_UI_CREATE_POPUP_STRUC Creates a standard struct for popup menus.
%   The created struct has cur_gui_ndx same as default. 
%   Assumption: default is 'All' at position 1 then ndxs are ones. User wants with all items on the list. (true items). 
%   pipeline: sa_ui_set_env_after_get_data -->sa_ui_create_popup_struc
%             sa_create_storage_for_ui.m   -->sa_ui_create_popup_struc
% Update list of indices that reflect direct and indirect filtering:
% Need to recalculate quantities.
% pre_mpopupmenu
list_len_cur_dat                  	= length(list_str_cel_cur_dat);
% % list_len_cur_gui                    = list_len_cur_dat + 2;
% % 
% % list_ndx_filt_ori_dat             	= ones(list_len_cur_dat,1);                 % default is all always.
% % list_str_cel_cur_gui              	= cell(list_len_cur_gui,1);
% % list_str_cel_cur_gui{1}          	= 'All';                             	% 'All'        in position 1: default is all.
% % list_str_cel_cur_gui{end }        	= 'Search...';                          % 'Search ...' the end : 
% % list_str_cel_cur_gui((2:end-1),1) 	= list_str_cel_cur_dat;             	% same as dat

%     create menus with:    1 - all        These 3 are standard for all the mpopupmenus.
%                           2 - swap 
%                           3 - search ...
% item_slected_array logical array: 1 = selected: current or before (i.e this is the memory of selections to accumulate or remove from selected set
% bold/plain    == selected/not selected  independent of other selections   stored in user data    bold==bold          plain== non-bold 

% item_enabled_array logical array: 1 = enabled:  current: determined as last spet after re-calculating all data that is available as resulting of last selection.
% dark/gray     == enabled/disabled         dependent of other selections   stored in user data    dark==normal black  gray== italic pale  



list_start_std_item_cnt                 = 3;     % 3 standard items for all the menus. 
allsel_ndx                              = 1;
swapse_ndx                              = 2;
search_ndx                              = 3;
start_dat_ndx                         	= list_start_std_item_cnt +1;                      % real data menu items start at ndx 4
list_len_cur_names                   	= list_len_cur_dat + list_start_std_item_cnt;
list_item_names                         = cell(list_len_cur_names,1);

list_item_names{allsel_ndx}             = 'All'; 
list_item_names{swapse_ndx}             = 'Swap'; 
list_item_names{search_ndx}             = 'Search ...';
list_item_names((start_dat_ndx:end),1)  = list_str_cel_cur_dat;             	% same as dat
list_item_slected                    	= true(size(list_item_names)); 
list_item_slected(swapse_ndx)       	= 0; % swap   is not selected by default, until user chooses it: ie plain
list_item_slected(search_ndx)       	= 0; % search is not selected by default, until user chooses it: ie plain
list_item_enabled                    	= true(size(list_item_names));% default: all enabled because we start with no filters.


if (strcmpi(pop_up_name,'PROBE'))
    % PROBE: Overwrite 'Search ...' with 'Tr'
    %_tr_ list_str_cel_cur_gui{end }    	= 'Tr';
end

%  Build standard struct:  
% NOTE: Matlab requires brackets to create the struct, so list_str_cel_cur_dat      will be a cel    not a cel of cells  !!!!!!
% NOTE: Matlab requires brackets to create the struct, so list_ndx_filt_ori_dat will be a array  not a cel of double !!!!!!
ui_pop_up_struct = struct(                      ...
    'pop_up_name'           ,{pop_up_name}      ...
    ,'list_item_names'      ,{list_item_names}  ... % spopupmenu_new
    ,'list_item_slected'    ,{list_item_slected}... % spopupmenu_new
    ,'list_item_enabled'    ,{list_item_enabled}... % spopupmenu_new
    ,'cur_gui_ndx',         1                   ...  % in pop coordinates: always changes: 1 means all:  need to substract 1 to address list_str_cel_cur_dat    
    );
    
% %     ,'list_str_cel_ori_dat', { list_str_cel_cur_dat } ... %cin dat coordinates: never  changes
% %     ,'list_str_cel_cur_dat', { list_str_cel_cur_dat } ... % in dat coordinates: always changes: CURRENT changes according to the current selection (this popup or other popups)
% %     ,'list_str_cel_ori_gui', { list_str_cel_cur_gui } ... %cin pop coordinates: never  changes
% %     ,'list_str_cel_cur_gui', { list_str_cel_cur_gui } ... % in pop coordinates: always changes: CURRENT changes according to the current selection (this popup or other popups)
% %     ,'list_ndx_filt_ori_dat',{ list_ndx_filt_ori_dat} ... % in dat ori coordin:  CURRENT explicit user selected  (goes with list_str_cel_ori)
% %     ,'cur_dat_ndx',         0                        ...  % in dat coordinates: always changes: 1 means all:  need to substract 1 to address list_str_cel_cur_dat
% %     ,'def_gui_ndx',         1                        ...  % in pop coordinates: never  changes: 1 means all:  need to substract 1 to address list_str_cel_cur_dat
end % fn sa_ui_create_pop_up_struc
%     %,'list_ndx_filt_ind',{list_ndx_filt_ind     }...

