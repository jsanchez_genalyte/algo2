%%                            QC REPORT: LOT TO LOT COMPARISON
%%  Lot to lot comparison for each analyte.
%  Green   scatter dots:  channel  1:  Squares
%  Blue    scatter dots:  channel  2:  Triangle
%  Source: Assay Dev Summary Report (From Jervis API)

close_all_true_figures;

% file:///C:/Users/jsanchez/Documents/algo2/source/html/sa_do_plot_ana_lot_2_lot_url.html#1
color_code = { rgb('Green')  rgb('Blue') rgb('Red') rgb('Orange') };
shape_code = {      's'          '^'         's'        's' };

fig_btm             = 50;
fig_left            = 50;
fig_width           = 1600;
fig_height          = 900;
height_delta        = 0.15; % .2 units will be taller the box than the tex
load('qc_lot_2_lot_struct.mat','qc_lot_2_lot_struct')
ana_cnt             = size(qc_lot_2_lot_struct,2);
sub_plot_hor_cnt    = 1;           % change it to 2 or to 4 to see bigger or smaller plots jas_hard_coded
num_sub_plots       = ana_cnt*2;   % ceil(spotl_cnt / sub_plot_hor_cnt );
cur_sub_plot_num    = 1;
sub_plot_ver_cnt    = 2; % ceil(num_sub_plots /sub_plot_hor_cnt) ;

for cur_ana_ndx=1:1:ana_cnt
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both
    cur_sub_plot_num = cur_ana_ndx;
    hFig                = figure('Name',sprintf('  Lot to lot variability  ( Cluster mean ) %s',qc_lot_2_lot_struct(cur_ana_ndx).name_ana));
    set(hFig, 'Position', [fig_left fig_btm fig_width fig_height]);
    fig_left = fig_left+0;
    
    subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,1); % top part: plot. Btm part: table.
    hold all;
    ana_lot_struct      = qc_lot_2_lot_struct(cur_ana_ndx);
    data_aggr_mat       = ana_lot_struct.data_aggr_mat(:,:,3);
    col_chann_both = size(ana_lot_struct.data_aggr_mat,3);       % both oe
    chann_cnt      = col_chann_both -1;
    data_aggr_o_mat     = ana_lot_struct.data_aggr_mat(:,:,1);   % scatter odd
    data_aggr_e_mat     = ana_lot_struct.data_aggr_mat(:,:,2);   % scatter even
    name_cur_ana        = ana_lot_struct.name_ana;
    col_values_cat_u  	= ana_lot_struct.col_values_cat_u;
    spotl_cnt           = length(col_values_cat_u);
    
    % Produce Box Plots for all available lots for each analyte
    % display(name_cur_spotl) position: [left bottom width height]
    ax_box             = gca;
    set(ax_box,'Units','normalized')
    ax_box_position_ori     = ax_box.Position;
    ax_box_position_new     = ax_box_position_ori;
    ax_box_position_new(2)  = ax_box_position_ori(2) - height_delta; % lower the btm of the box
    ax_box_position_new(4)  = ax_box_position_ori(4) + height_delta; % make taller      the box
    ax_box.Position         = ax_box_position_new;
    
    boxplot(data_aggr_mat,'notch','on',...
        'labels',{ char(col_values_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))
    set(ax_box,'FontSize',9);
    title(ax_box,  strcat(strrep(name_cur_ana,'_','-'),'   Positive Cluster Mean') );
    ylabel(ax_box, strcat(strrep(name_cur_ana,'_','-'),' - GRU') );
    
    % draw the individual dots: oddd and even
    % Do scatter plots to plot individual dots ___________________________________________
    x_axis_qc_plot  = get(ax_box,'XAxis');
    x_limits_qc_plot= x_axis_qc_plot.Limits;
    x_min           = x_limits_qc_plot(1);
    x_max           = x_limits_qc_plot(2);
    x_range         = x_max - x_min;
    x_inc           = x_range / ((2*spotl_cnt));
    cur_start_x     = x_min + (x_inc/2);
    data_odd_plot_x = zeros(size(data_aggr_o_mat,1), spotl_cnt);
    data_even_plot_x= zeros(size(data_aggr_e_mat,1),spotl_cnt);
    for cur_lot_ndx = 1:1:spotl_cnt
        data_odd_plot_x( :,cur_lot_ndx)     = cur_start_x ;
        data_even_plot_x(:,cur_lot_ndx)     = cur_start_x + x_inc; % + (x_inc/3);        
        cur_start_x                         = cur_start_x + (2*x_inc);
        scatter(ax_box,data_odd_plot_x(:,cur_lot_ndx),data_aggr_o_mat(:,cur_lot_ndx)...
            ,'MarkerEdgeColor',color_code{1}...              
            ,'MarkerFaceColor',color_code{1}...          
            ,'LineWidth'      ,0.2...
            ,'Marker'         ,shape_code{1}...
            ,'SizeData'       ,9);
        %            ,'*g','LineWidth',0.2); % ,'MarkerFaceColor',dark_green_color);
        scatter(ax_box,data_even_plot_x(:,cur_lot_ndx),data_aggr_e_mat(:,cur_lot_ndx)...
            ,'MarkerEdgeColor',color_code{2}...              
            ,'MarkerFaceColor',color_code{2}...
            ,'LineWidth'      ,0.2...
            ,'Marker'         ,shape_code{2}...
            ,'SizeData'       ,9);
        
        % ,'*b','LineWidth',0.2); % ,'MarkerFaceColor',dark_blue_color);
    end
    
    set(ax_box,'XGrid','on');
    set(ax_box,'YGrid','off');
    grid on;
    
    data_sum_stats          = qc_lot_2_lot_struct(cur_ana_ndx).data_sum_stats;
    data_sum_stats_table    = array2table(data_sum_stats);
    Spotting_lot            = col_values_cat_u;
    data_sum_stats_table    = [ table(Spotting_lot) data_sum_stats_table ];
    data_sum_stats_header   = {'Spotting_Lot', '   N  ','MEAN ','STD ','CV'};
    data_sum_stats_table.Properties.VariableNames= data_sum_stats_header;
    
    %fprintf(' ASSAY: %-s',name_cur_ana);
    %disp(data_sum_stats_table);
    % Summary statistics:
    subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,2); % top part: plot. Btm part: table.
    hold all;
    ax_tex                  = gca;
    set(ax_tex,'Units','normalized')
    ax_tex_position_ori     = ax_tex.Position;
    ax_tex_position_new     = ax_tex_position_ori;
    ax_tex_position_new(4)  = ax_tex_position_ori(4) - (height_delta/3); % make shorter     the text (ony 5 racks
    ax_tex.Position         = ax_tex_position_new;
    ax_tex.Visible          = 'off';
    sum_title_cell          = { sprintf(' STATSISTICS FOR ANALYTE: %-s \n',name_cur_ana) };
    spotl_cel               = cellstr(char(data_sum_stats_table.Spotting_Lot));
    data_sum_stats_table.Spotting_Lot = spotl_cel;
    
    col_head                = data_sum_stats_header;
    col_head                = strrep(col_head,'_','-');
    col_head{1}             = 'Spotting Lot';
    col_head                = col_head(2:end);                                      % first_elem_out
    wid                     = [  12,  20,  20,  20  ];                              % first_elem_out [6,
    fms                     = { '4.0f','15.1f','19.1f','19.2f'};                    % first_elem_out {'4.0f',
    row_head                = {}; %  '  ', '  ', '  ', '  ' };% , fid, colsep, rowending)% first_elem_out  {'',
    fid                     = 1;
    col_sep                 = ' ';
    row_end                 = '';
    text_cell               = displaytable_to_cell(data_sum_stats_table(:,2:end),col_head, wid,  fms,row_head,fid, col_sep, row_end);
    temp_by_str = strcat(name_cur_ana, ' by ');
    t = text(0.05,0.97-(0.0),{temp_by_str});
    t(1).Color = 'Blue';
    %t(1).FontSize = 14;
    for cur_lot_ndx        = 1:1:height(data_sum_stats_table)+1
        if (cur_lot_ndx == 1)
            row_cell = {['Spotting Lot  ' text_cell{cur_lot_ndx} ] };
        else
            row_cell = { [char(table2cell(data_sum_stats_table(cur_lot_ndx-1,1))) , '        ',    char(text_cell{cur_lot_ndx})] };
        end
        
        t = text(0.05,0.97-(0.17*cur_lot_ndx),row_cell);
        if (cur_lot_ndx == 1)
            t(1).Color = 'red';
        end
    end
    hold off;
end % for each analyte: : plotting each on it's own window.
%% Summary statistics For Individual Analyte  By Spotting Lot
% Summary statistics: Text version ________________________________________________________
for cur_ana_ndx=1:1:ana_cnt
    % PRODUCE SUM STATS FOR CURRENT ANALYTE:  both
    ana_lot_struct      = qc_lot_2_lot_struct(cur_ana_ndx);
    data_aggr_mat       = ana_lot_struct.data_aggr_mat(:,:,3);
    col_chann_both = size(ana_lot_struct.data_aggr_mat,3);       % both oe
    chann_cnt      = col_chann_both -1;
    data_aggr_o_mat     = ana_lot_struct.data_aggr_mat(:,:,1);   % scatter odd
    data_aggr_e_mat     = ana_lot_struct.data_aggr_mat(:,:,2);   % scatter even
    name_cur_ana        = ana_lot_struct.name_ana;
    col_values_cat_u  	= ana_lot_struct.col_values_cat_u;
    spotl_cnt           = length(col_values_cat_u);
    
    data_sum_stats          = ana_lot_struct.data_sum_stats;
    data_sum_stats_table    = array2table(data_sum_stats);
    Spotting_lot            = col_values_cat_u;
    data_sum_stats_table    = [ table(Spotting_lot) data_sum_stats_table ];
    data_sum_stats_header   = {'Spotting_Lot', '   N  ','MEAN ','STD ','CV'};
    data_sum_stats_table.Properties.VariableNames= data_sum_stats_header;
    
    % Summary statistics For Individual Analyte  By Spotting Lot
    spotl_cel               = cellstr(char(data_sum_stats_table.Spotting_Lot));
    data_sum_stats_table.Spotting_Lot = spotl_cel;
    
    col_head                = data_sum_stats_header;
    col_head                = strrep(col_head,'_','-');
    col_head{1}             = 'Spotting Lot';
    col_head                = col_head(2:end);                                      % first_elem_out
    wid                     = [  12,  20,  20,  20  ];                              % first_elem_out [6,
    fms                     = { '4.0f','15.1f','19.1f','19.2f'};                    % first_elem_out {'4.0f',
    row_head                = {}; %  '  ', '  ', '  ', '  ' };% , fid, colsep, rowending)% first_elem_out  {'',
    fid                     = 1;
    col_sep                 = ' ';
    row_end                 = '';
    text_cell               = displaytable_to_cell(data_sum_stats_table(:,2:end),col_head, wid,  fms,row_head,fid, col_sep, row_end);
    fprintf('\n%s By\n',name_cur_ana );
    %t(1).FontSize = 14;
    for cur_lot_ndx        = 1:1:height(data_sum_stats_table)+1
        if (cur_lot_ndx == 1)
            row_cell = {['Spotting Lot  ' text_cell{cur_lot_ndx} ] };
        else
            row_cell = { [char(table2cell(data_sum_stats_table(cur_lot_ndx-1,1))) , '        ',    char(text_cell{cur_lot_ndx})] };
        end
        fprintf('%s',char(row_cell));
    end
    fprintf('\n');
    %displaytable(table2array(data_sum_stats_table),col_head, wid,  fms,row_head,fid, col_sep, row_end);
end % for each analyte: : plotting each on it's own window.

%  file: sa_do_plot_ana_lot_2_lot_url


