function [ aggr_struct,errror_message ] = aggreate_column_for_table( a_table,a_col_aggr_1_name,a_col_data_name_list,a_col_aggr_2_name )
%AGGREATE_COLUMN_FOR_TABLE  agregates several data columns of a table given one or 2 columns used as aggregation criteria.
%   Generic function that uses any table with any column names and produces the aggreation, the indices and the stats.
%   Input:
%   1. a_table              data table
%   2. a_col_aggr_1_name    name of first    aggregation level: the column of the table to build the aggreations (i.e. spotl) (i.e. the pivot)
%   3.a_col_aggr_2_name     name of second   aggregation level: Ussually chann or sensor
%   4. a_col_data_name_list name of the data columns of the table to do the aggreations (i.e. a list of the 16 analytes).
%   Output:
%   1. aggr_struct: Array of structures (one element per name in the a_col_aggr_name_list) with:
%                   data_aggr_mat:       data for the given column  used to build the aggregations. last dim is individual scatter chan/sens. last val is both.
%                   name_ana:            clean name of the given analyte (clean enough to be able to be used in plot labels/titles)
%                   col_values_1_cat_u:    unique list of names for the given a_col_data_name
%                   data_sum_stats:      marix with 4 cols and as many rows as unique values: n,mean,std and cv ussing the corresponding data.
%   2. a index_set_table with:
%           1 - as many columns as unique values for the given column of the original table
%           2 - each element is a logical: 0/1 on whether the column value row matches the corresponding value for this column.
%   2. data_sum_stats

%   sample call:
% [ aggr_struct,errror_message ] = aggreate_column_for_table( assay_sum_table,'spotl',assay_ana_header )
% [ aggr_struct,errror_message ] = aggreate_column_for_table( assay_sum_table,'instr',assay_ana_header )

aggr_struct            	= struct;
errror_message       	= '';
% First check: find indices of wanted analytes:
wanted_data_size        = length(a_col_data_name_list);
ndx_data_col_list       = nan(wanted_data_size,1);
all_data_size           = height(a_table);

a_table_header          = a_table.Properties.VariableNames;
% Find if there is a chann or a sensor column in the data: That will dictate: oe or senso1,2,3,4
% find the column indexes  on the headers of the data table (usually sum or detail assay dev)
%a_table_header         =  'chann': Column 9   'sensor':  Column 12
% a_col_chann_name      = 'chann';  % now it is an argument
col_aggr_2_cat_size     = 0;
% largest_col_aggr_2_len 	= 0;

[column_aggr_2_found_flag,column_aggr_2_ndx]= is_element_in_cell(a_table_header,a_col_aggr_2_name);
% CHECK THAT THE COLUMN IS ONE THE DATA COLUMNS FOR THE TABLE
if (~(column_aggr_2_found_flag))
    % COLUMN DATA IS NOT ON THE TABLE: do not aggregate by channel
    info_message =sprintf('column aggr 1 name %s not found in data table. Not doing OE/sensor aggregations',a_col_aggr_2_name);
    sprintf('\n%s\n',info_message);
end %%% else
    % FOUND CHANNEL/SENSOR COLUMN: do oe/sensor aggregations: use this ndx: column_aggr_2_ndx
% % %     a_table_chann       = a_table(:,column_aggr_2_ndx);
% % %     col_aggr_2_cat      = categorical(cellstr(num2str(cell2mat(table2cell(a_table_chann))))); %  .chann))));
% % %     col_aggr_2_cat_u    = unique(col_aggr_2_cat);
% % %     col_aggr_2_cat_size = size(col_aggr_2_cat_u,1);     % a_table(:,column_aggr_2_ndx)=
% % %     % build index for each unique value of the chann column (i.e.: 1234
% % %     col_aggr_2_mat_ndx 	= nan(all_data_size,col_aggr_2_cat_size);
    
% % %     for cur_aggr_2_ndx = 1:1:col_aggr_2_cat_size
% % %         % 2 chann: 1,2 or sensor: 1,2,3,4,5
% % %         cur_value_2                      	= col_aggr_2_cat_u(cur_aggr_2_ndx);
% % %         col_aggr_2_mat_ndx(:,cur_aggr_2_ndx)= col_aggr_2_cat  ==  cur_value_2;
% % %         col_aggr_2_cnt_list                 = nansum(col_aggr_2_mat_ndx);
% % %         largest_col_aggr_2_len              = nanmax(col_aggr_2_cnt_list);
% % %     end
% % % end % done building chann indices

for cur_data_name_ndx = 1:1:wanted_data_size
    a_col_data_name = a_col_data_name_list{cur_data_name_ndx};
    [column_data_found_flag,a_column_data_ndx]= is_element_in_cell(a_table_header,a_col_data_name);
    % CHECK THAT THE COLUMN IS ONE THE DATA COLUMNS FOR THE TABLE
    if (~(column_data_found_flag))
        % COLUMN DATA IS NOT ON THE TABLE: get out
        errror_message =sprintf('Invalid data column name %s not found in data table',a_col_data_name);
        return;
    end
    ndx_data_col_list(cur_data_name_ndx) = a_column_data_ndx;
end

% Second  check: find index of aggregation 1 colum: Do the same for the aggregation colum: i.e. the spotl
[column_aggr_1_found_flag,column_aggr_1_ndx]= is_element_in_cell(a_table_header,a_col_aggr_1_name);
if (~(column_aggr_1_found_flag))
    % COLUMN IS NOT ON THE TABLE: get out
    errror_message =sprintf('Invalid aggregation 1 column name %s not found in data table',a_col_aggr_1_name);
    return;
end

% FOUND BOTH COLUMNS ON THE TABLE: the aggregation and all the data columns.
%col_values_table    = a_table(:,column_aggr_1_ndx);
% % % if (iscell(a_table(:,column_aggr_1_ndx))) %(strcmp(char(col_values_table.Properties.VariableNames),'qc_rack_number'))
% % %     % SPECIAL CASE: qc_rack numbers comes as cells.
% % %     col_values_1_cat	= categorical(table2cell(a_table(:,column_aggr_1_ndx)));
% % % else
% % %     % REGULAR CASE: analyte names: strings.
% % %     col_values_1_cat  	= categorical(table2array(a_table(:,column_aggr_1_ndx)));
% % % end
% % % 
% % % col_values_1_cat_u      = unique(col_values_1_cat);
% % % col_values_1_cnt        = length(col_values_1_cat_u);

% build index for each unique value of the aggregation column (i.e. spotl)
% % % col_aggr_1_mat_ndx                        = nan(all_data_size,col_values_1_cnt);
% % % for cur_value_1_ndx = 1:1:col_values_1_cnt
% % %     cur_value_1                      	= col_values_1_cat_u(cur_value_1_ndx);
% % %     col_aggr_1_mat_ndx(:,cur_value_1_ndx) = col_values_1_cat == cur_value_1;
% % %     col_cnt_1_list                        = nansum(col_aggr_1_mat_ndx);
% % % end

% DONE VERIFYING ALL COLUMN NAMES AND BUILDING INDICES FOR THEM: Do the real work: The aggregations.
% fill in the matrix: data_aggr_mat with data arranged this way:      iscell(data_cur_ana)
%                     each column:  one unique value for this data column. data_cur_ana = cell2mat(data_cur_ana);
%                     each tower:   one unique (aggr_2) channel/sensor   where the last one is for both chann or all sensors combined.
%largest_col_aggr_len            = all_data_size; % bug_fix: 06_19 vs: nanmax((largest_col_aggr_2_len),nanmax(col_cnt_1_list));
%col_2_both                      = col_aggr_2_cat_size+1;    % last column is for both channels or all sensors or all whatever aggr_2_name is!.

% BUILD INDICES OF THE 2 AGGREGATION columns
[aggr_struct_1,error_message]   = do_index_table( a_table,column_aggr_1_ndx );
if (~(isempty(error_message)))
    return;
end
col_aggr_1_mat_ndx              = aggr_struct_1.col_ndx_mat;
col_values_1_cat_u              = aggr_struct_1.col_cat_names;
col_values_1_cnt                = aggr_struct_1.col_cat_size;
col_cnt_1_list                   = nansum(col_aggr_1_mat_ndx);
col_values_2_cnt = 1;
if (column_aggr_2_found_flag)
    [aggr_struct_2,error_message]   = do_index_table( a_table,column_aggr_2_ndx );
    if (~(isempty(error_message)))
        return;
    end
    col_aggr_2_mat_ndx              = aggr_struct_2.col_ndx_mat;
    %col_values_2_cat_u              = aggr_struct_2.col_cat_names;
    col_values_2_cnt                = aggr_struct_2.col_cat_size;
end
for cur_ana_data_ndx            = 1:1:wanted_data_size % 16
    % CALCULATE DATA FOR AGGR_1 (LOT_2_LOT) FOR CURRENT ANALYTE:
    data_aggr_mat               = nan(all_data_size,col_values_1_cnt,col_values_2_cnt); %third dim is oe for chan or indiv sens for senso
    data_sum_stats              = nan(4,col_values_1_cnt); % value 4 is because there are 4 stats summary metrics: N,mean,std and CV.
    for cur_value_1_ndx         = 1:1:col_values_1_cnt
        % ONE WANTED COLUMN AT A TIME: I.E. data for the given analyte.
        %ndx_cur                = logical(col_aggr_1_mat_ndx(:,cur_value_1_ndx));   % col_aggr_1_mat_ndx(1:col_cnt_1_list(cur_value_1_ndx))';
        %temp_rows              = 1:col_cnt_1_list(cur_value_1_ndx);
        %temp_rows              = temp_rows';
        data_cur_ana          	= table2array(a_table(:,ndx_data_col_list(cur_ana_data_ndx)));
        if  (iscell(data_cur_ana))
            data_cur_ana = cell2mat(data_cur_ana);
        end
        all_cur_ndx             = col_aggr_1_mat_ndx(:,cur_value_1_ndx);
        %all_cur_ndx (~ndx_cur)  = 0;  % exclude outisders.
        if (sum(iscell(data_cur_ana(all_cur_ndx))))
            fprintf('found a cell\n')
        end
        if (column_aggr_2_found_flag)
            % AGGREGATING BY 2 COLUMNS: Combine the cur_value_1 with each of the cur_2_values of the second column                               
            for cur_value_2_ndx=1:1:col_values_2_cnt                
                % all, or scatter odd or scatter even (or indiv sensors when calling with: senso). and of 2 aggregations:
                cur_1_and_2_ndx   = logical(all_cur_ndx & col_aggr_2_mat_ndx(:,cur_value_2_ndx)); 
                data_aggr_mat(cur_1_and_2_ndx,cur_value_1_ndx,cur_value_2_ndx)= data_cur_ana(cur_1_and_2_ndx);
            end
        else
            % AGGREGATING BY ONLY ONE COLUMN:            
            data_aggr_mat(all_cur_ndx,cur_value_1_ndx,1) = data_cur_ana(all_cur_ndx);
        end
        % calculate summary stats for current analyte: col dim 3 / col 1 from data_aggr_mat holds for all.
        data_sum_stats(1,cur_value_1_ndx)           = sum(isfinite((data_aggr_mat(:,cur_value_1_ndx,1))));               % N
        data_sum_stats(2,cur_value_1_ndx)           = nanmean(      data_aggr_mat(:,cur_value_1_ndx,1));                 % mean
        data_sum_stats(3,cur_value_1_ndx)           = nanstd(       data_aggr_mat(:,cur_value_1_ndx,1));                 % std
        data_sum_stats(4,cur_value_1_ndx)           = 100 * data_sum_stats(3,cur_value_1_ndx) /data_sum_stats(2,cur_value_1_ndx);% CV
    end % for each column of data
    aggr_struct(cur_ana_data_ndx).name_ana          = strrep(a_col_data_name_list{cur_ana_data_ndx},'_','-') ;
    aggr_struct(cur_ana_data_ndx).col_values_cat_u  = col_values_1_cat_u;  %jas_tbreviewed: col_values_1_cat_u
    aggr_struct(cur_ana_data_ndx).data_aggr_mat     = data_aggr_mat;% store all results scatter one channel per last dimension  then both.
    aggr_struct(cur_ana_data_ndx).data_sum_stats    = data_sum_stats';
end % for each data analyte
end % fn aggreate_column_for_table
        % calculate both as the nanmax of the individuals: ok for chan,and sensors, but may not be true for data:    
        % Does not work: I need all not just the or of the values!!!!! 
%         if (col_2_both>1)
%             % DOING SINGLE AND BOTH:
%             temp = squeeze(data_aggr_mat(:,cur_value_1_ndx,1:(col_2_both-1)));
%             data_aggr_mat(: ,cur_value_1_ndx,col_2_both) = nanmax(temp,[],2) ; %            = data_cur_ana(logical(all_cur_ndx));
%         end     

%       data_aggr_mat(  temp_rows               ,cur_value_1_ndx,col_2_both)            = data_cur_ana(logical(all_cur_ndx)); % not bad idea:
