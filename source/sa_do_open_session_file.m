function [ handles ] = sa_do_open_session_file( handles,input_file_name,local_input_dir )
%DO_OPEN_SESSION_FILE given a path and a matlab-session input_file_name name opens the input_file_name and sets up ui. 

% sample call:  [ handles ] = do_open_session_file( handles,input_file_name,local_input_dir )

input_parameters_struct = handles.input_parameters_struct ; 


% [input_file_name,local_input_dir] = uigetfile('*.xlsx');
if ~isequal(input_file_name, 0)
    %cd(local_input_dir);
    input_parameters_struct.local_input_dir     = {local_input_dir };
    input_parameters_struct.input_file_names    = { input_file_name};    
    input_parameters_struct.input_data_type     = {'excel_file' };
    input_parameters_struct.excel_input_dir     = { local_input_dir };   
    % HAVE AN INPUT DATA EXCEL input_file_name TO WORK WITH: Open it.
    % open(input_file_name);
    filename_excel   = strcat(local_input_dir,'\',input_file_name);
    excel_tab_number = input_parameters_struct.excel_tab_number{1};
    [file_loaded_count, analyte_struct_list ] ...
        = std_curve_load_excel_data ( filename_excel,excel_tab_number); % *** LOAD EXCEL input_file_name ***
    if (file_loaded_count)
        %display 'OK done_loading_data from Excel input_file_name:try_1 directory '
        handles.env_struct_list.cur_ui_excel_file_opened= true;        
    else
        
        error_string = sprintf('Failed loading_data from Excel input_file_name: %s ',filename_excel);
        h = errordlg(error_string,' LOADING EXEL input_file_name WITH STARDARD CURVE DATA ...');
        return;
    end
    % When loading a new excel input_file_name: Set the first analyte and first candidate as the current oned to begin with:
    cur_ui_analyte_ndx   = 1; 
    cur_ui_candidate_ndx = 1;

    % Make the the following pull down menus visible/enabled then set the to the list of analytes read on the excel input_file_name: Done       
    [handles] = init_env_for_new_analyte(handles,analyte_struct_list,cur_ui_analyte_ndx,cur_ui_candidate_ndx);    
    % jas_here: tb_decided: When oppening a new analyte:
    % do I wipe out the existing table of fits? 
    
    %cd(input_parameters_struct.matlab_code_dir{1});

end % fn do_open_session_file

