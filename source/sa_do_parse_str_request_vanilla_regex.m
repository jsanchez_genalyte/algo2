function [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla_regex( a_request_vanila_str )
%SA_DO_PARSE_STR_REQUEST_VANILLA handles parsing a vanilla request: breaks down request into a cell of strings
%  criteria: commas (one or more) or blanks (one or more)
% sample call:  [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla_regex(   'AA16R1'    'AA17R2')

wild_array_flag =false;
[a_request_cell_str] = strsplit(a_request_vanila_str,{' ',','},'CollapseDelimiters',true);
a_request_cell_str = a_request_cell_str';
if (~isempty(a_request_cell_str))
    if isempty(a_request_cell_str{1})
        %2018_04_04: Bug fix: get rid on an empty first item. 
        if (length(a_request_cell_str) >1)
            % MORE THAN ONE ENTRY AN THE FIRST ONE IS EMPTY: shift the list
            a_request_cell_str = a_request_cell_str(2:end);
        else
            %           ONE ENTRY AN THE FIRST ONE IS EMPTY:
            a_request_cell_str ={};
        end
    end
end
end % fn sa_do_parse_str_request_vanilla


