function [ ] = mpopup_menu_set_items( a_popupmenu,list_item_names,list_item_slected,list_item_enabled,last_selected_ndx) % [ a_popupmenu ]
%MPOPUP_MENU_SET_ITEMS Sets the state of a given multi pop up menu: using 3 arrays: the names, the seclected flag and the enabled flag.
% Input:    a_popupmenu       handle to a popup menu to be set
%           list_item_names   str_cell with the list of names of the mpopup_menu: expected: { all, swap, search, item_1, ... item_n }
%           list_item_slected logical: 1 = selected: current or before (i.e this is the memory of selections to accumulate or remove from selected set
%           list_item_enabled logical: 1 = enabled:  current: determined as last spet after re-calculating all data that is available as resulting of last selection.
%           last_selected_ndx ndx of last selected item. (it is accumulative)

% rules; bold/plain    == selected/not selected  independent of other selections   stored in user data    bold==bold          plain== non-bold
%        dark/gray     == enabled/disabled         dependent of other selections   stored in user data    dark==normal black  gray== italic pale
%

for cur_item_ndx = 1:length(list_item_names)
    %jas_debug: display(list_item_names(cur_item_ndx));
    
    if (list_item_slected(cur_item_ndx))
        % ITEM IS SELECTED: Set it to bold
        list_item_names{cur_item_ndx} = str2html( list_item_names{cur_item_ndx},'bold',true  );
    else
        % ITEM IS NOT SELECTED: Set it to plain
        list_item_names{cur_item_ndx} = str2html( list_item_names{cur_item_ndx},'bold',true  );
    end
    if (list_item_enabled(cur_item_ndx))
        % ITEM IS ENABLED: Set it to black
        list_item_names{cur_item_ndx} = str2html( list_item_names{cur_item_ndx},'italic',false,'colour', 'black'  );
    else
        % ITEM IS DIS-ABLED: Set it to italic grey
        list_item_names{cur_item_ndx} = str2html( list_item_names{cur_item_ndx},'italic',true,'colour', '#A0A0A0' );
    end
end
set(a_popupmenu,'String',list_item_names,'Value',last_selected_ndx); % 1 == all is the selected item.
end % fn mpopup_menu_set_items

