function [ error_message ] = sa_do_plot_ana_lot_2_lot(qc_lot_2_lot_struct,cur_win_ana_title,cur_plot_title_list,plot_oe_flag)
%SA_DO_PLOT_ANA_LOT_2_LOT do the lot2lot for 1 analyte and writes it's summ table
%   xx
% sample call: [ error_message ] = sa_do_plot_one_ana_lot_2_lot(qc_lot_2_lot_struct);
% data_aggr_mat
%
% spotl_cat   = categorical(assay_sum_table.spotl);
% col_values_cat_u = unique(categorical(assay_sum_table.spotl));
% %spotl_cnt  = length(col_values_cat_u);
% spotl_cnt   = size(data_aggr_mat,2);
%
% a_mean = nanmean(data_aggr_mat);

% setup:
% dark_green_color                = [0.1  0.5  0.2];  % nice dark green.
% dark_blue_color                 = [0     0    1];    % nice dark blue
error_message ='';

for cur_ana_ndx=1:1:size(qc_lot_2_lot_struct,2) % 16
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both
    data_aggr_mat   = qc_lot_2_lot_struct(cur_ana_ndx).data_aggr_mat;
    if (plot_oe_flag)
        data_aggr_o_mat = qc_lot_2_lot_struct(cur_ana_ndx).data_aggr_o_mat;
        data_aggr_e_mat = qc_lot_2_lot_struct(cur_ana_ndx).data_aggr_e_mat;
    end
    name_cur_ana        = qc_lot_2_lot_struct(cur_ana_ndx).name_ana;
    col_values_cat_u    = qc_lot_2_lot_struct(cur_ana_ndx).col_values_cat_u;    
    spotl_cnt           = length(col_values_cat_u);    
    
    % from here down all plotting: for each analyte
    
    %data_ana = assay_sum_ana_mat(cur_ana_ndx+data_offset);
    % produce box plots for all lots and given analyte
    subplot(2,1,1) % top part: plot. Btm part: table.
    figure('Name','  Lot to Lot variability  ( Cluster mean )');
    fig_qc_lot_2_lot = gcf;
    hold all;
    boxplot(data_aggr_mat,'notch','on',...
        'labels',{ char(col_values_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))
    axes_qc_lot_2_lot               = gca;
    set(axes_qc_lot_2_lot,'FontSize',9);
    title(axes_qc_lot_2_lot, strcat(strrep(name_cur_ana,'_','-'),'   Positive Cluster Mean') );
    ylabel(axes_qc_lot_2_lot, strcat(strrep(name_cur_ana,'_','-'),' - GRU') );
    % draw the individual dots: oddd and even
    hold all;
    % Do scatter plots to plot individual dots ___________________________________________
    x_axis_qc_plot  = get(axes_qc_lot_2_lot,'XAxis');
    x_limits_qc_plot= x_axis_qc_plot.Limits;
    x_min           = x_limits_qc_plot(1);
    x_max           = x_limits_qc_plot(2);
    x_range         = x_max - x_min;
    x_inc           = x_range / ((2*spotl_cnt));
    cur_start_x     = x_min + (x_inc/2);
    if (plot_oe_flag)
        data_odd_plot_x = zeros(size(data_aggr_o_mat,1), spotl_cnt);
        data_even_plot_x= zeros(size(data_aggr_e_mat,1),spotl_cnt);
        for cur_lot_ndx = 1:1:spotl_cnt
            data_odd_plot_x( :,cur_lot_ndx)     = cur_start_x - (x_inc/3);
            data_even_plot_x(:,cur_lot_ndx)     = cur_start_x; % + (x_inc/3);
            data_odd_plot_x(1:end,  cur_lot_ndx)= cur_start_x + x_inc;
            cur_start_x                         = cur_start_x  + (2*x_inc);
            scatter(axes_qc_lot_2_lot,data_odd_plot_x(:,cur_lot_ndx),data_aggr_o_mat(:,cur_lot_ndx),'*g','LineWidth',0.2); % ,'MarkerFaceColor',dark_green_color);
            scatter(axes_qc_lot_2_lot,data_even_plot_x(:,cur_lot_ndx),data_aggr_e_mat(:,cur_lot_ndx),'*b','LineWidth',0.2); % ,'MarkerFaceColor',dark_blue_color);
        end
    end
    set(axes_qc_lot_2_lot,'XGrid','on');
    set(axes_qc_lot_2_lot,'YGrid','off');
    grid on;
    set(fig_qc_lot_2_lot,'Units','normalized')
    %fig_qc_lot_2_lot.Position %  [left bottom width height]
    fig_qc_lot_2_lot.Position =   [ 0.1   0.03   0.9    0.9];
    
    % Display summary stats:
    data_sum_stats = qc_lot_2_lot_struct(cur_ana_ndx).data_sum_stats;
%   qc_lot_2_lot_struct(2).data_sum_stats
%   165.0000   65.7180   33.2729   50.6298
%   190.0000   60.0814   30.2139   50.2883
%   175.0000   30.9963   21.8645   70.5390
%   180.0000   49.9533   40.7337   81.5436
    hold off;
    
    
end % for each analyte: : plotting each on it's own window.


end % fn sa_do_plot_ana_lot_2_lot

