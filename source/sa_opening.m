function [handles] = sa_opening(hObject,handles)
%SA_OPENING initializes env_struct_list, tos  and cells for app start up.  
% returns: handles.env_struct_list 
%  Set up globals before std_curve app starts.
% pipeline:   
% 1 - sa_opening     (hObject,handles)                      NO UI: cfg file reads and inits GUI needed structs 
% 2 - sa_init_env_app( handles,input_parameters_struct )	UI:    GUI elments initilization
% 3 - sa_init_env_for_new_data_set

% This will be used when exporting to EXCEL the TOS: (Column headers starting with classification flags)

tos_column_names=   ...
    {
    'Classification' ...  % 1 All flags get appened as in a single categorical array.
    ,'Name'      	...   % 2 Analyte Name (Aaron want multiple analytes)
    ,'Date'        	...   % 3
    ,'Protocol'     ...   % 4
    ,'Sample'     	...   % 5
    ,'Lot'          ...   % 6
    ,'Experiment'  	...   % 7
    ,'Instrument'   ...   % 8
    ,'Carrier'   	...   % 9
    ,'Channel'      ...   % 10
    ,'Cluster'      ...   % 11
    ,'Data Type'    ...   % 12
    ,'B-Line GRU' 	...   % 13
    ,'Ampli GRU'    ...   % 14
    ,'GRU Shift'  	...   % 15
    ,'An Flag1'     ...   % 16 Annalysis Flag
    ,'An Flag2'   	...   % 17
    ,'An Flag3'   	...   % 18
    };

tos_cur_selection     = [0 0];  % [0 0] means nothing selected: [start_row end_row]

% CREATE CELL WITH ALL THE CURRENT SETTING IN THE GUI:
env_struct_list = nan; % cell(8); % follows: 8 fields.

env_struct_list.cur_ui_local_file_opened        = false;
env_struct_list.local_file_name                = '';
env_struct_list.cur_ui_db_set_requested         = false;
env_struct_list.cur_ui_db_set_received          = false;
env_struct_list.ui_state_struct                 = nan;
env_struct_list.tos_column_names                = tos_column_names;
env_struct_list.tos_cur_selection               = tos_cur_selection;
handles.tos_row_cnt                             = 0;               % the table gets created with 0 rows
% Add to handles the environment
handles.env_struct_list                         = env_struct_list; % These are for book keeping.         

% MAKE INVISIBLE THE BUTTON AND THE LIST BOX: Not needed until after having a data_set from either local or db file.
% hide: All 3plots, all buttons and edit boxers for plots, tos, 
% % set(handles.listbox_excel_tab_selection,  'Visible','off','Enable','off');
% % set(handles.excel_sheet_selection_text,   'Visible','off'); 



% Save the change you made to the structure
guidata(hObject,handles); 

end % fn tse_edit_opening

