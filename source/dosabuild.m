% Ref: do_compile_senso.m script to compile sensogram app  to produce a senso_app executable that can be installed on another pc.
% Folder:
%   C:\Users\jsanchez\Documents\algo2\source
%       sa.exe                  % executable to be deployed
%   C:\Users\jsanchez\Documents\algo2\mcr_installer\*.*      % matlab runtime to be run in the  target machine
close all; % no_args version
clear;
clear obj;
clear all;
cache_dir                  = 'C:\Users\jsanchez\Documents\Temp\jsanchez\mcrCache9.1\';
% Check if the cfg folder exists:
if ( ( exist(cache_dir,'dir') == 7))
    % Folder Exist and it is a directory:  If dir exists: Remove all cache dirs
    cd  (cache_dir);
    % CMD for loop to recursivelly remove all dirs: To start clean and avoid compiling with the wrong runcard db.
    !for /d %G in ("dies*") do rd /s /q "%~G"
end

cd  ('C:\Users\jsanchez\Documents\algo2\source');
%!del sa.exe  % prevent infinite recursion.
disp('Sensogram_Analysis_App version 1_00  ...');
utils_save_all_editor_open_files;
% to compile:
mcc -m           Sensogram_Analysis_App_10              ...  % main .m file
    -a           ..\cfg_and_data                        ...  % additional dir with cfg icons etc.
    -o           sa                                     ...  % executable file (old: senso_analysis_100  
    -R logfile,'sa.log'   ...  % log file for debug    
     , 'Compiling Sensogram_Analysis_App version 1_00.0 ...' ...
     , 'Compiltation DONE '; 
disp('Compiling ... Sensogram_Analysis_App DONE'); 


% Deploy 
!copy sa.exe C:\Users\jsanchez\Documents\algo2\exe\
!move sa.exe \\genstore2\Users\Storage\jsanchez\Documents\sa\exe\

% move   senso_analysis_100.exe  ..\senso_deploy\senso_analysis_100.exe   
%!senso_analysis_100.exe   ..\senso_deploy\exes\                          
%!senso_analysis_100.exe   \\genstore2\Users\Storage\jsanchez\Documents\senso_analysis_rel_dec_01_2017\senso_deploy\exes\senso_analysis_100.exe

% cp 'C:\Program Files\MATLAB\R2016b\toolbox\compiler\deploy\win64\MCRInstaller.exe' '.\std_curve_deploy\mcr_installer\'

% help:

% mcrinstaller
% The WIN64 MATLAB Runtime Installer, version 9.0.0, is:
%     C:\Program Files\MATLAB\R2016a\toolbox\compiler\deploy\win64\MCRInstaller.exe
% mcrversion  % ans: 9