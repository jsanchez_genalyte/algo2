function [ cancel_flag,handles ] = sa_do_save_session_dlg(handles, cur_session_name_arg )
%DO_SAVE_SESSION_DLG prompts user regarding saving or not the current session
%   If user wants to save current session: It saves the session.

% Construct a questdlg with three options: Save, Save_as and Cancel
% Handles the 2 cases:
%    1 - There is     a current session (in this case uses it's name as default.
%    1 - There is NOT a current session (in this case no default for save is provided.

% Returns true: If either the user canceled the save opperation or the save failed.


if (isempty(cur_session_name_arg))
    cur_session_name ='';
else
    cur_session_name = cur_session_name_arg{1};
end

cancel_flag       = false;
saved_cwd         = pwd();
session_file_name = '';
if (~(strcmp(cur_session_name,'')))
    % THERE IS AN EXISTING SESSION GOING ON: Use it's data
    [path_str,session_file_name,~] = fileparts(cur_session_name);
    if (~(isempty(path_str)))
        cd(path_str);
    end
end
default_session_file=session_file_name;
dlg_title ='Save Session ';
if (strcmp(default_session_file,''))
    % THERE IS not CURRENT SESSION:
    user_question =  'Would you like to save current work on a session file?';
else
    % THERE IS     CURRENT SESSION:
    full_file = dir(handles.ui_state_struct.cur_session_name{1});
    file_display_name = strcat(full_file.folder,'\',full_file.name);
    user_question =  sprintf('Would you like to Save session: %s  ?',file_display_name);
end
if (strcmp(default_session_file,''))
    % NO   current session: NEED A NAME. Provide NO  default:
    choice = questdlg(user_question, ...
        'Save Current work as a Session',      ...
        'Save As ...','No'     ,'Cancel','Cancel');
    % Handle response
    switch choice
        case 'Save As ...'
            disp([choice 'Save Session As...'])
            [file_name, file_path] = uiputfile('*.mat',dlg_title);
            if ((isequal(file_name,0)) || isequal(file_path,0))
                cancel_flag = true;
                disp('User pressed cancel')
                return;
            end
            default_session_file = file_name;
            cd(file_path);
            % Save the session and all details
            [ handles ] = sa_do_save_session(handles, default_session_file );            
            return;
        case 'No'
            disp([choice 'Not Saving current session'])
            return;
        case 'Cancel'
            disp('Cancel');
            cancel_flag = true;
            % Cancel means: User changed his/her mind: So Return and do not do the open.
            cd(saved_cwd);
            return
    end % switch
else
    % YES  current session: Provide YES default:
    % [save_yes_not_flag] =   uisave(std_curve_fit_app_variables,dir(a_matlab_file_name)); % ,'FontSize','15.0');
    [ handles  ] = sa_do_save_session(handles, default_session_file );
    return;
end
cd(saved_cwd);
end % fn sa_do_save_session_dlg

