function [ qc_lot_2_lot_struct,qc_rack_2_rack_struct,error_message ] = sa_qc_calc_rack_2_rack( assay_sum_table_qc,assay_ana_header )
%SA_QC_CALC_RACK_2_RACK  calculates the qc_rack_2_rack_struct for the auto qc report.
%   inputs:   assay_sum_table_qc joined table between runcard and the summary table produced by gen_qc_rpt_data: assay_sum_table
%   outputs:  (note: here lot refers to spotting lot: ie spotl)
%   1. the data to produce the qc_rack_to_rack plots for each analyte and each spotl.
%      1 to 16: each analyte. and number 17 is:
%          data_aggr_mat     aggregated (by spotl) data matrix with odd(1),even(2),both(3) sensors
%          name_ana          name of analyte
%          col_values_cat_u  unique list of spotl for the given analyte
%          data_sum_stats    statistical summary for the given analyte (by spotl)
%   2. error message:        blank means no errors.

%   next function in pipeline: plot qc_rack_to_rack plot for each analyte and each lot: boxplot for each rack. only 1 to 5 racks
%   sample call:
%  [ qc_rack_2_rack_struct,error_message ] = sa_qc_calc_rack_2_rack( assay_sum_table,assay_ana_header );

assay_ana_header_ext                = assay_ana_header ;
assay_ana_header_ext{end+1}         = 'qc_rack_number';

[qc_lot_2_lot_struct,error_message]     = aggreate_column_for_table( assay_sum_table_qc,'spotl',assay_ana_header_ext,'chann'); % *** 1 aggreagte ****
if (~(isempty(error_message)))
    return;
end
ana_cnt                                 = size(qc_lot_2_lot_struct,2)-1;  % substract the last one: the qc_rack_number
for cur_ana_ndx=1:1:ana_cnt % 16 and 17 is rack.
    % PROCESS CURRENT ANALYTE:  both channels
    spotl_names_list        = qc_lot_2_lot_struct(cur_ana_ndx).col_values_cat_u;
    data_aggr_mat           = squeeze(qc_lot_2_lot_struct(cur_ana_ndx).data_aggr_mat(:,:,1)); % 1 is both lots.
    col_values_cat_u        = qc_lot_2_lot_struct(cur_ana_ndx).col_values_cat_u;
    ana_lot_aggr_table      = array2table(data_aggr_mat);
    spotl_cnt               = size(col_values_cat_u,1)  ;           % cnt of unique spotl categories
    lot_numbers_list_ori_str= char(spotl_names_list);               % cellstr(char(spotl_names_list));
    lot_numbers_list_ext_cel= cell(size(spotl_names_list));         % (lot_numbers_list_ori_str;
    
    % set the column names: name of the spotl: WARNING: when a lot number starts with a number: it is not a valid variable name
    % so: preapend the lot number with LOT_  so they become valid
    for cur_lot_name_ndx = 1:1:size(lot_numbers_list_ori_str,1)
        lot_numbers_list_ext_cel{cur_lot_name_ndx}              =  strcat('lot_',lot_numbers_list_ori_str(cur_lot_name_ndx,:));
    end
    ana_lot_aggr_table.Properties.VariableNames                 = lot_numbers_list_ext_cel;
    chan_cnt                                              = size(qc_lot_2_lot_struct(cur_ana_ndx).data_aggr_mat,3); % third dim is for both channels.
    
    for cur_spotl_ndx=1:1:spotl_cnt
        for cur_chan_ndx =1:1:chan_cnt
            % RETRIEVE THE DATA FOR EACH CHANNEL: break it down into each rack: % assume rack is last header col!!
            temp_rack_tabl                                  = squeeze(array2table(qc_lot_2_lot_struct(end).data_aggr_mat(        :,:,cur_chan_ndx)));
            data_aggr_mat                                   = squeeze(            qc_lot_2_lot_struct(cur_ana_ndx).data_aggr_mat(:,:,cur_chan_ndx));
            ana_lot_aggr_table                              = array2table(data_aggr_mat);
            ana_lot_aggr_table.Properties.VariableNames     = lot_numbers_list_ext_cel;
            ana_lot_aggr_table_ext                          = [ ana_lot_aggr_table(:,cur_spotl_ndx)  temp_rack_tabl(:,cur_spotl_ndx) ];
            
            ana_lot_aggr_table_ext.Properties.VariableNames{end}= 'qc_rack_number';
            cur_spotl_ext_name  = strcat('lot_',char(qc_lot_2_lot_struct(end).col_values_cat_u(cur_spotl_ndx)));
            %                                                                                                                    % *** 2 aggreagte ****
            [ ana_lot_rack_struct,error_message ]              = aggreate_column_for_table( ana_lot_aggr_table_ext,'qc_rack_number',{ cur_spotl_ext_name},'');
            if (~(isempty(error_message)))
                return;
            end
            % store results:
            qc_rack_2_rack_struct(cur_ana_ndx,cur_spotl_ndx,cur_chan_ndx).ana_lot_rack_struct  = ana_lot_rack_struct;
        end % for each chann
    end % for each spotln
end % for each analye: calc data to plot and summary
save('qc_rack_2_rack_struct.mat','qc_lot_2_lot_struct','qc_rack_2_rack_struct');

% Generate an HTML view of the saved MATLAB file.
% Published figure window appearance 'entireGUIWindow' (default) | 'print' | 'getframe' | 'entireFigureW

publish('sa_do_plot_ana_rack_2_rack_url.m'...
    , 'figureSnapMethod','getframe' ... %  'entireFigureWindow'...
    ,'useNewFigure',true...
    ,'showCode' ,false ...
    );
% The publish command executes the code for each cell in sa_do_plot_ana_rack_2_rack_url.m, and saves the file to /html/sa_do_plot_ana_lot_2_lot_url.html.
% View the HTML file.
web('html/sa_do_plot_ana_rack_2_rack_url.html');
end % fn sa_qc_calc_rack_2_rack