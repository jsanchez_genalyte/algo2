function [ handles ] = ui_do_plot_sens_set(hObject, handles )
%UI_DO_PLOT_SENS_SET plots a set of sensograms in either or both of the good/anoma axes.
%   uses ui_state_struct.senso_start_ndx.
%input_parameters_struct	= handles.input_parameters_struct;
ui_state_struct           	= handles.ui_state_struct;
db_cur_fit_net_ndx       	= ui_state_struct.db_cur_fit_net_ndx;

%popup_analyte_sel_ndx       = get(handles.popup_menu_reference_material,   'Value');
%popup_candidate_sel_ndx     = get(handles.popup_menu_reference_candidate,  'Value');
%popup_fit_type_sel_ndx      = get(handles.popup_menu_reference_fit,        'Value');

% determine which sensograms need to be: classified and then plotted: build index of:
%   ui_state_struct.cur_good_plot_ndx, and  ui_state_struct.cur_anom_plot_ndx.
% Start from the current db_data_set, use exiting filters( from the pulldown menus), and buttons etc.
rows_to_plot_cnt           	= sum(db_cur_fit_net_ndx); % handles.db_data_set_struct.rows_db_cnt{1};    % single float

senso_start_ndx          	= ui_state_struct.senso_start_ndx;
senso_start_ndx
senso_curves_perp_cnt   	= ui_state_struct.senso_curves_perp_cnt; %  4; % rows_to_plot_cnt ; %
senso_end_ndx                = min( (senso_start_ndx+ senso_curves_perp_cnt-1),rows_to_plot_cnt);

% Build the ndx to select data to plot: a chunk at a time. i.e. a block.
% plot current block of curves.
x_data                        = handles.db_data_set_struct.scan_raw_time{1}(db_cur_fit_net_ndx,:);
x_data                        = x_data(senso_start_ndx:senso_end_ndx,:);
y_data                        = handles.db_data_set_struct.scan_raw_gru{1}(db_cur_fit_net_ndx,:);
y_data                        = y_data( senso_start_ndx:senso_end_ndx,:);
title_str                     = strcat(' GOOD SENSOGRAMS: (',(strrep(sprintf('%-4d-%-4d / %5d)',senso_start_ndx,senso_end_ndx,rows_to_plot_cnt),' ','')));
legend_str                    = nan;
ui_create_plot_senso_block(handles.axes_good_sens,x_data,y_data...
    ,nan,nan,nan                                    ...  % y_data_enve,y_data_mean,y_data_median
    ,'Time '                                        ...  % x_label_str
    ,' GRU '                                        ...  % y_label_str
    ,title_str                                      ...
    ,legend_str);
guidata(hObject, handles); % Update handles structure

%senso_start_ndx

% for cur_sensor_ndx = senso_start_ndx:1:senso_end_ndx
%     % plot current curve: get the data
%     plot(handles.db_data_set_struct.scan_raw_time,handles.db_data_set_struct.scan_raw_gru);
% end

% call sc_* functions to classiy the sensograms of interest

% call ui_do_display_plot(axes_name,cur_good_plot_ndx, etc _*) and
% call ui_do_display_plot(axes_name,cur_anom_plot_ndx, etc _*) and

% upd counters for consistency. cur_start_plot_ndx_value, etc. for good and anom  (ie ui_state).

% save results in handles.
handles.ui_state_struct =ui_state_struct;
end % fn ui_do_plot_sens_set

