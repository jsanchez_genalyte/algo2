% script:  a_rest_get_exp
% ref:      Testing of JERVIS API 1.4 frohm John. 
% wed: build one function per API call.

% John_questions:
% 1 - for how long is the connection valid? expires_in: 86400 meaning?
% 2 - How do I distinguish between an internal error (try block?) vs a no experiment found? Try xxx: Should not return an error!).
% 3 - when using dates as filter: which column is used: created_at or updated_at
% 4 - createTest: what is the meaning of: required parameters in an output parameter? Body parameters Required: device_id,specimen_id

% Connection to rest service: works fine.....
url_login_str 	= 'http://jervis-staging.genalyte.com/api/auth/token';
token_str       = 'GL86C3700A12';
token_struct    = struct('authorization_token',token_str);
token_json      = jsonencode(token_struct);
options         = weboptions('MediaType','application/json');
response        = webwrite(url_login_str,token_json,options);

% API_1: getProtocol: get: /protocols/{id}   request to get details for a given protocol: Mistery?  _____________________________________________         API_1: getProtocol
%                     John:          works for a protocol that is     in the protocol list: see and execute:  sa_rest_protocol_get_details
%                     John: DOES NOT work  for a protocol that is     in the exp     list: '4cdbc37c-3325-443c-a903-b55d70cbdbdd'
options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )}); % jas_debug_timeout
options.Timeout         = 25; % jas_debug_timeout    
try
    error_msg           = '';
    test_protocol       = nan;
    test_protocol       = webread('http://jervis-staging.genalyte.com/api/protocols/4cdbc37c-3325-443c-a903-b55d70cbdbdd',options);   % does not work: in exp but not in protocol_list !!
catch
    % Protocol not found in protocol list
    error_msg        = 'Protocol not found in protocol list';
    sprintf(error_msg)
end

try
    error_msg           = '';
    test_protocol       = nan;
    test_protocol        = webread('http://jervis-staging.genalyte.com/api/protocols/6bd9420c-3cab-4c1d-b298-1005cd0fb858',options);   % works ok
catch
    % Protocol not found in protocol list
    error_msg        = 'Protocol not found in protocol list: 6bd9420c-3cab-4c1d-b298-1005cd0fb858';
    sprintf(error_msg)
end    

% API_2: listProtocols: get: /protocols/{id}   request a list of all protocols: OK  ______________________________________________________________        API_2: listProtocols
%                     John:          there are experiments with a protocol that is not in the protocol_list !!!!
options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )});
protocol_list_all   = webread('http://jervis-staging.genalyte.com/api/protocols/',options);   % OK: Returns the details of 30 protocols.

% API_3: showSensor: get: /sensors/{id}   request to gete scan_data for a given sensor: ok. 63�44   ___________________________________________________  API_3: showSensor
%                    John: is the sensor_id an unique id across tests? how come we do not pass the test_id to the query?
options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )});
test_sensors        = webread('http://jervis-staging.genalyte.com/api/sensors/18841',options);   % b5d306cb-70a4-462a-a651-d3878bddadc4

% returns:            test_sensors =  struct with fields:
%              id: 18841
%          number: 9
%        is_valid: 1
%     sensor_data: [63�44 double]  % This is the scan data  (needs to be retrieved one sensor at a time


% API_4: createTest: post: /tests   request to get an specific test: ___________________________________________________________________________________  API_4: createTest
%                    body parameter json object: Required: device_id,specimen_id
consumable_struct                          = struct;
consumable_struct.identifier               = ''; % string ? Identifier for Consumable (case sensitive ? )
consumable_struct.experiment_name          = ''; % string
consumable_struct.carrier_serial_number    = ''; % string
consumable_struct.spotting_lot_number      = ''; % string
consumable_struct.kit_name                 = ''; % string
consumable_struct.part_number              = ''; % string
consumable_struct.lot_number 			   = ''; % string
consumable_struct.serial_number 		   = ''; % string
consumable_struct.expiration_date          = ''; % string

device_id_string_inst                      = 'M53'; % string (guid) Identifier for Device
protocol_id_string_inst                    = '';    % string guid)  Identifier of the Protocol to be used

specimen_id_string_inst_1                  = 'PS12345601'; % string
specimen_id_string_inst_2                  = 'PS12345601'; % string
specimen_id_struct                          = {specimen_id_string_inst_1 specimen_id_string_inst_2}; % rather than a struct it is a cell of either 1 or 2 elements.
specimen_id_struct_inst                     = specimen_id_struct;
control_bool_inst                           = false;                    % boolean Indicates if this test should be treated as a control test
consumable_struct_inst                      = consumable_struct;
test_struct                                 = struct;
test_struct.consumable                      = consumable_struct_inst;
test_struct.device_id                       = device_id_string_inst;
test_struct.protocol_id                     = protocol_id_string_inst;
test_struct.specimen_id                     = specimen_id_struct_inst;
test_struct.control                         = control_bool_inst;

test_struct_inst                            = test_struct;
% Done creating body parameter:
test_struct_inst_json      = jsonencode(test_struct_inst);

url_login_str 	= 'http://jervis-staging.genalyte.com/api/auth/token';   % start Login ok: ___________________________
token_str       = 'GL86C3700A12';
token_struct    = struct('authorization_token',token_str);
token_json      = jsonencode(token_struct);
options         = weboptions('MediaType','application/json');
response        = webwrite(url_login_str,token_json,options);            % end Login ok: ___________________________

% John: What Post means??? why the output has: required fields: Required: id,protocol
% RESTful API
% RESTful APIs or interfaces provide standard HTTP methods such as GET, PUT, POST, or DELETE.
% Tips
% For functionality not supported by the RESTful web services functions, see the HTTP Interface.
% For HTTP POST requests, the webread function supports only the application/x-www-form-urlencoded media type.   ??  application/x-www-form-urlencoded media type ??? in webread doc
% To send a POST request with content of any other internet media type, use webwrite.

% John: Output is the output of IndexTests!!!! it is missing fields: probe_map and well_plate !!!!
options       	= weboptions('HeaderFields',{'Content-Type', 'application/json' ;'Authorization' sprintf(' Bearer %s', response.token ); 'RequestMethod', 'post';'ArrayFormat','json'});
%options        = weboptions('HeaderFields',{'Content-Type', 'json'             ;'Authorization' sprintf(' Bearer %s', response.token )}); % no difference
test_return_json= webread('http://jervis-staging.genalyte.com/api/tests','json',test_struct_inst_json,options);    % returns all tests: Not the requested one.


% API_5: indexTests: get: /tests    request to get an specific test: all tried cases fail ..... _________________________________________________         API_5: indexTests:
options             = weboptions('HeaderFields',{'Content-Type', 'application/json' ;'Authorization' sprintf(' Bearer %s', response.token )});                                                                                                                                                    % status 500 with message "Internal Server Error"
expe_list           = webread('http://jervis-staging.genalyte.com/api/tests',options);                                                              % ok returns all experiments. in the staging area

try
    error_msg     	= '';
    expe_list      	= webread('http://jervis-staging.genalyte.com/api/tests/','q','Jervis ANA Validation',options);                              % status 500 with message "Internal Server Error"
catch
    % experimet not found in protocol list
    error_msg   	= 'Experiment not found in Experiment list. Experiment name:  Jervis ANA Validation. No date range ';
    sprintf(error_msg)
end

try
    error_msg        = '';
    expe_list    	= webread('http://jervis-staging.genalyte.com/api/tests/','start_date','2017-07-20 5:00','end_date','2017-07-22 7:00','q','Jervis ANA Validation',options);
catch
    % experimet not found in protocol list
    error_msg        = 'Experiment not found in Experiment list:  Experiment name:  Jervis ANA Validation  and date range';
    sprintf(error_msg)
end

options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )});
expe_list           = webread('http://jervis-staging.genalyte.com/api/tests',options);                                                              % ok returns all experiments. in the staging area

try
    error_msg        = '';
    expe_list           = webread('http://jervis-staging.genalyte.com/api/tests/','q','xxxx',options);                                                  % returns internal error
catch
    % experimet not found in protocol list
    error_msg        = 'Experiment not found in Experiment list';
    sprintf(error_msg)
end

expe_list           = webread('http://jervis-staging.genalyte.com/api/tests/','start_date','2017-07-21 5:00','end_date','2017-07-21 6:00',options); % returns all: filer does not work. returns all experiments

% API_6: showTestMetadata: get: /tests/{id}/metadata    request to get an specific test: seems to be ok?. ______________________________________________  API_6: showTestMetadata
% Body parameters
% json
% status	% string
% errors  % integer

test_metad_struct = struct;
test_metad_struct.status = '';     % string     % John: who fills the status? is it input or output?
test_metad_struct.errors = [ 0 0 ]; % vect int  %       same for errors ?

test_metad_struct_inst 	= test_metad_struct;
% Done creating body parameter: id = 'b5d306cb-70a4-462a-a651-d3878bddadc4' for test(5)  if exits returns metadata for a test, otherwise errors out (try block)
test_metad_inst_json   	= jsonencode(test_metad_struct_inst);

options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )});
test_metad          = webread('http://jervis-staging.genalyte.com/api/tests/b5d306cb-70a4-462a-a651-d3878bddadc4/metadata','json',test_metad_inst_json,options);                                                              %


% API_7: showTestProbes: get: /tests/{id}/probes    request to get an specific test probe data: seems to be ok?. 36 probes _____________________________  API_7: showTestProbes
%                        usefull:

options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )});
test_probes         = webread('http://jervis-staging.genalyte.com/api/tests/b5d306cb-70a4-462a-a651-d3878bddadc4/probes',options);                                                              %

% API_8: showTestSensorMetadata: get: /tests/{id}/probes    request to get an specific test sensor meta data: seems to be ok?. 136 sensors _____________  API_8: showTestSensorMetadata
%
options             = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', response.token )});
test_sensors_metad  = webread('http://jervis-staging.genalyte.com/api/tests/b5d306cb-70a4-462a-a651-d3878bddadc4/sensors',options);
%                     john: this is not metadata but data: Should it be called:   showTestSensor    : remove Metadata
% returns:
% test_sensors_metad =   136�1 struct array with fields:
%     id                John: values around: 18769 and 18840 what are these sensor_ids?  ignore them?
%     number            ok: 1 to 136
%     channel           ok: 1 or 2
%     is_valid          ok: 1 or 0 meaning: John: (is_valid_flag on Jervis?)
%     sensor_errors     ok: blank or a string like: result 3999.xx has been flaged as an outlier
%     results           ok: struct:    test_sensors_metad(120).results
%                         struct with fields:
%                         value: 21.8890            	 == raw gru?
%                         normalized_value: 2.6033       == norm gru
%                         is_outlier: 0                  == outlier flag
%                         sensor_errors: []              ==  if not empty means it is not valid? reduntant with sensor sensor_errors?

% API_9 to 11 : updateTestSensor, updateTestLogs,updateTestSensor: Do not need them?


