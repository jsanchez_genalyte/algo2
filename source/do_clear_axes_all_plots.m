function [ handles ] = do_clear_axes_all_plots( handles )
%DO_CLEAR_AXES_ALL_PLOTS Clear axes for all plots to keep ui consistent
%   3 pairs (main and residuals) for: ref,gnm and std plus the axes_params.
legend(handles.axes_good_sens,'off')
axes(handles.axes_good_sens);
set(handles.axes_good_sens, 'Visible','off');
cla;
legend(handles.axes_anom_sens,'off')
axes(handles.axes_anom_sens);
set(handles.axes_anom_sens, 'Visible','off');

% The plot buttons,text and edit text and  should be clear too:

%set(handles.text_good_bline_region_yellow,   	'Visible','off'); % start invisible until a ref and gnm have a fit.
%set(handles.text_good_ampli_region_yellow,   	'Visible','off'); % start invisible until a ref and gnm have a fit.
%set(handles.text_anom_bline_region_yellow,   	'Visible','off'); % start invisible until a ref and gnm have a fit.
%set(handles.text_anom_ampli_region_yellow,   	'Visible','off'); % start invisible until a ref and gnm have a fit.

% good side
set(handles.checkbox_good_sens_env,             'Visible','off','Enable','off');
set(handles.checkbox_good_sens_med,             'Visible','off','Enable','off');
set(handles.checkbox_good_sens_mea,             'Visible','off','Enable','off');
set(handles.checkbox_hide_good,                 'Visible','off','Enable','off');

% middle side
set(handles.text_senso_per_plot,                'Visible','off');
set(handles.edit_senso_per_plot,                'Visible','off','Enable','off');
set(handles.pushbutton_senso_next,              'Visible','off','Enable','off');
set(handles.checkbox_combine,                   'Visible','off','Enable','off');

% anom side
set(handles.checkbox_anom_sens_med,             'Visible','off','Enable','off');
set(handles.checkbox_anom_sens_mea,             'Visible','off','Enable','off');
set(handles.checkbox_hide_anom,                 'Visible','off','Enable','off');

% text for plot related stuff:

% The Tos should be clear too:
set(handles.ui_table_tos,                        'Visible','off','Enable','off');

set(handles.radiobutton_local_file,             'Visible','on');                % may not have a file to work with 
set(handles.radiobutton_db,                 	'Visible','on','Enable','off'); % future
set(handles.pushbutton_get_data,             	'Visible','on','Enable','on' );  % 

% Force new fits:
%  handles.env_struct_list.ui_state_struct.ref.fit_done_flag = 0;
%  handles.env_struct_list.ui_state_struct.gnm.fit_done_flag = 0;
end

