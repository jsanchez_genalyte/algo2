function [ handles] = fake_input_data(handles)
% fake hardcoded data: 1 analyte with 4 sensors: and 4 raw and normalized scans.
% declare_globals;  % algo_1 jas_temp
% sa_declare_globals;
% see:   called_by      Sensogram_Analysis_App_10  --> fake_input_data --> fill_in_db_set_from_fake_data
input_parameters_struct	   = handles.input_parameters_struct;
db_data_set_struct         = handles.db_data_set_struct;

%ui_state_struct               = handles.ui_state_struct;

[algo_gb_struct ]          = main_algo_init_gb(input_parameters_struct);  % main_init_globals;  % algo_1  jas_temp     now_not global anymore.      % *** ALL APP GLOBALS ARE initialized HERE ******

% Define Input/Output types: ___________________________________________________________________________________________
cd 'C:\Users\jsanchez\Documents\algo2\source';
%input_data_type_list  = {'excel_file' , 'excel_dir' , 'mat_file' , 'mat_dir'};
%output_data_type_list = {'mat_file'   , 'mat_dir'};
% Define Input and output file locations: ______________________________________________________________________________
%' cd 'o:'
% cd 'Data Analytics/QC Tests/66060/CL160505/Raw Data/SP/SP-1/TraceData/CL160505-12-Array-SP-01/'

% Training Data from Luis: 5 * 24 files to build reference pattern:
% net drive: O:\Data Analytics\QC Tests\66060\CL160505\Raw Data\SP\
excel_file_set_dir_01 = 'C:\Users\jsanchez\Documents\data_2\luis\SP\SP-1\TraceData\CL160505-12-Array-SP-01\';
excel_file_set_dir_02 = 'C:\Users\jsanchez\Documents\data_2\luis\SP\SP-2\TraceData\CL160505-12-Array-SP-02\';
excel_file_set_dir_03 = 'C:\Users\jsanchez\Documents\data_2\luis\SP\SP-3\TraceData\CL160505-12-Array-SP-03\';
excel_file_set_dir_04 = 'C:\Users\jsanchez\Documents\data_2\luis\SP\SP-4\TraceData\CL160505-12-Array-SP-04\';
excel_file_set_dir_05 = 'C:\Users\jsanchez\Documents\data_2\luis\SP\SP-5\TraceData\CL160505-12-Array-SP-05\';
mat_file_set_dir_01   = 'C:\Users\jsanchez\Documents\data_66060_CL160505_mat\sp_01\';
mat_file_set_dir_02   = 'C:\Users\jsanchez\Documents\data_66060_CL160505_mat\sp_02\';
mat_file_set_dir_03   = 'C:\Users\jsanchez\Documents\data_66060_CL160505_mat\sp_03\';
mat_file_set_dir_04   = 'C:\Users\jsanchez\Documents\data_66060_CL160505_mat\sp_04\';
mat_file_set_dir_05   = 'C:\Users\jsanchez\Documents\data_66060_CL160505_mat\sp_05\';

% - Define Input Files _____________________

input_data_type      = 'excel_dir';
% 5 dirs: 1 to 5
input_excel_data_dir = { excel_file_set_dir_01}; %  , excel_file_set_dir_02, excel_file_set_dir_03  , excel_file_set_dir_04, excel_file_set_dir_05}; % , excel_file_set_dir_02, excel_file_set_dir_03 , excel_file_set_dir_04, excel_file_set_dir_05}; % }; %
% - Define Output Files _____________________
output_data_type     = 'mat_dir'; %  'mat_file';
output_mat_data_dir  = mat_file_set_dir_01;


[input_file_names, output_file_names ] = ...
    define_io_file_names(input_data_type,input_excel_data_dir,output_data_type,output_mat_data_dir);
input_parameters_struct.input_file_names = input_file_names;
% Load all files in one shot:  ___________________________________
display 'before_loading_data'
set(handles.text_status,'string','Loading Data Form Local File ....','ForegroundColor','blue','Fontsize',12); %'blue'
load_matlab= true;            % jas_wed_ori true    true;  false;   true: load_mat   false:load_excel.
if (load_matlab)
    filename = 'ws_sp_1.mat';
    load (filename,'file_loaded_count', 'raw_times_data', 'raw_shifts_data',  'raw_col_names_data', 'raw_ld_data' , 'raw_tr_data','meta_data_data')
    display 'done_loading_data from Matlab: ws_sp_1.mat from dir:'
    pwd    % show dir wher file sits
else
    [file_loaded_count, raw_times_data, raw_shifts_data,  raw_col_names_data, raw_ld_data , raw_tr_data,meta_data_data ] ...
        = load_test_data (input_data_type, input_file_names);
    display 'done_loading_data from Excel : sp_5 directory'
    display 'saving mat file: ws_sp_1.mat  in dir: '
    pwd    % show dir wher file gets saved
    filename = 'ws_sp_1.mat';
    save(filename, 'file_loaded_count', 'raw_times_data', 'raw_shifts_data',  'raw_col_names_data', 'raw_ld_data' , 'raw_tr_data','meta_data_data' );
end
display 'done_loading_fake_data'     
set(handles.text_status,'string','Normalizing Data             ....','ForegroundColor','green','Fontsize',12); %'blue'
                                                                     % fake_load

[db_data_set_struct ] = ...
    fill_in_db_set_from_fake_data(input_parameters_struct,algo_gb_struct...
    ,file_loaded_count, raw_times_data, raw_shifts_data,  raw_col_names_data , raw_tr_data, meta_data_data ...
    ,db_data_set_struct );

set(handles.text_status,'string','Normalizing Data             DONE','ForegroundColor','blue','Fontsize',12); %'blue'
handles.db_data_set_struct = db_data_set_struct;
%handles.ui_state_struct      = handles.ui_state_struct;
end  % fake_inpu_data

%size(date_str_list,1);
% handles.db_data_set_struct.clasi           = util_transpose_to_col_vector(handles.db_data_set_struct.clasi)
% leftovers
%
% date_str_list  = {'01/02/2003' ; '04/05/2006' ; '07/08/2009' ; '10/11/2012'};
% probe_date_struct = create_date_struct_for_date_str_list( date_str_list );
%
% db_input_struct.probe_date_struct = probe_date_struct;

