%%                            QC REPORT: CONTROL CHARS FOR EACH ANALYTE - SPOTTING LOT COMBINATION
%%  RACK TO RACK DETAIL cCONTROL for each Analyte - Lot combination
%  Green     dots:     sensor 1
%  Blue      dots:     sensor 2
%  Red       dots:     sensor 3
%  Orange    dots:     sensor 4
%  Squares   channel:  1
%  Triangles channel:  2
%  Source:   Assay Dev Detail Report (JERVIS API and RUNCARD).

%  To run: sa_do_plot_ana_control_charts_det_url
%  url:    
% color_sensor:     1             2           3          4
color_code = { rgb('Green')  rgb('Blue') rgb('Purple') rgb('Orange') };
shape_code = {     's'           's'          's'          's' };
% % % channel_1 square    
% % % channel_2 triangle   '^'	Upward-pointing triangle

fig_btm                 = 50;
fig_left                = 50;
fig_width               = 1600;
fig_height              = 900;
height_delta            = 0.15; % .2 units will be taller the box than the tex
load('qc_control_charts_det_struct.mat','qc_lot_2_lot_det_struct','qc_control_charts_det_struct')
ana_cnt                 = size(qc_lot_2_lot_det_struct,2)-1;
plot_lots_per_win       = 3;
for cur_ana_ndx=1:1:ana_cnt % jas_temp_debug:  ana_cnt % jas_temp_debug_ ana_cnt
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both
    spotl_names_list    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    name_ana            = qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana ;
    col_values_cat_u    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    spotl_cnt           = length(col_values_cat_u); %
    sub_plot_hor_cnt    = 1;	        % left: spotl1 right: spotl2 left. Change it from  2 to 3 or 4 to see more subplots horizontally: jas_hard_coded
    sub_plot_ver_cnt    = 3;            % top: plot btm: text for summary stats table
    num_sub_plots       = sub_plot_hor_cnt*sub_plot_ver_cnt;
    
    hFig                = figure('Name',sprintf('  CONTROL CHART. Analyte: %-s ',name_ana));
    set(hFig, 'Position', [fig_left fig_btm  fig_width fig_height]);
    fig_left            = fig_left+20;
    cur_sub_plot_num    = 0;
    col_senso_both      = size(qc_control_charts_det_struct,3);
    senso_cnt           = col_senso_both -1;
    for cur_spotl_ndx=1:1:spotl_cnt
        % PRODUCE CONTROL CHARS FOR CURRENT ANALYTE AND CURRENT SPOTL:  by sensor
        cur_sub_plot_num= cur_sub_plot_num +1;
        if (cur_sub_plot_num > sub_plot_ver_cnt)
            % DONE WITH THIS WINDOW: (ALL SUBPLOTS DONE) need to create a new window and reset cur_sub_plot_num
            cur_sub_plot_num = 1;
            hFig    = figure('Name',sprintf('  CONTROL CHART. Analyte: %-s ',name_ana));
            set(hFig, 'Position', [fig_left fig_btm fig_width fig_height]);
            fig_left = fig_left+0;
        end
        
        subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,cur_sub_plot_num); % top part: plot. Btm part: table.
        hold all;
        ana_lot_chan_struct     = qc_control_charts_det_struct(cur_ana_ndx,cur_spotl_ndx,col_senso_both).ana_lot_rack_struct;
        data_aggr_mat           = ana_lot_chan_struct.data_aggr_mat;
        name_cur_spotl          = ana_lot_chan_struct.name_ana;
        col_values_cat_u        = ana_lot_chan_struct.col_values_cat_u;
        chan_cnt                = length(col_values_cat_u);                   % rack_cnt is really channel_cnt
        
        clear data_aggr_single_mat;
        for cur_senso = 1:1:senso_cnt
            data_aggr_single_mat(cur_senso)    = qc_control_charts_det_struct(cur_ana_ndx,cur_spotl_ndx,cur_senso).ana_lot_rack_struct;
        end
        
        % Produce Control Chart Plots for all available lots for each analyte
        % display(name_cur_spotl) position: [left bottom width height]
        ax_box                  = gca;
        set(ax_box,'Units','normalized')
        %         ax_box_position_ori     = ax_box.Position;
        
%         plot(ax_box,data_aggr_mat...  % assay_ana_h_pret(1:2))  'labels',{ char(col_values_cat_u) }
%             ,'MarkerFaceColor',color_code{cur_senso}...
%             ,'MarkerEdgeColor',color_code{cur_senso}...
%             ,'LineWidth',0.2...
%             ,'Marker',shape_code{cur_senso});

        set(ax_box,'FontSize',9);
        str_title = strcat(strrep(name_cur_spotl,'_','-'),'.  Positive Cluster Mean  .','  ',qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana);
        title(ax_box,strrep(str_title,'.',' '),'FontSize',11);
        ylabel(ax_box, strcat(strrep(name_cur_spotl,'_','-'),' - GRU') );
        
        % plot the individual dots: oddd and even
        % Do scatter plots to plot individual dots ___________________________________________
        x_axis_qc_plot  = get(ax_box,'XAxis');
        x_limits_qc_plot= x_axis_qc_plot.Limits;
        x_min           = x_limits_qc_plot(1);
        x_max           = x_limits_qc_plot(2);
        x_range         = x_max - x_min;
        x_inc           = x_range / ((2*chan_cnt));
                 
        clear data_single_plot_x;
        for  cur_senso = 1:1:senso_cnt    % jas_temp vs both 
            temp = zeros(size((data_aggr_single_mat(cur_senso).data_aggr_mat),1),chan_cnt);
            data_single_plot_x{cur_senso} = 1:1:size(temp,1);
        end
        
        % Plot 8 curves per plot: 4_senso * 2 channels. 
        for cur_chan_ndx = 1:1:chan_cnt   % it used to be cur_rack_ndx
            for cur_senso = 1:1:senso_cnt
                temp_mat = data_aggr_single_mat(cur_senso).data_aggr_mat;
                cur_x_plot                  = zeros(size(temp_mat));
                cur_x_plot(:,cur_chan_ndx)  = 1:1:length(temp_mat(:,cur_chan_ndx));
                plot(ax_box,cur_x_plot(:,cur_chan_ndx),temp_mat(:,cur_chan_ndx)...
                    ,'MarkerFaceColor',color_code{cur_senso}...
                    ,'LineWidth',0.2...
                    ,'Marker',shape_code{cur_senso}); % ,'MarkerFaceColor',dark_green_color);                 
            end
            % move to the next boxplot
        end
        set(ax_box,'XGrid','on');
        set(ax_box,'YGrid','off');
        grid on;
        hold off;
    end % for each spotting lot
end % for each analyte: : plotting each on it's own window.
%end % fn sa_do_plot_ana_control_charts_det_url

