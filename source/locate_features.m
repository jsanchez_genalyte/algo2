function [success_fg] = locate_features( im_tiff_gray_file_path)
% CVT_GRAY_TO_BW Converts a TIFF gray scale file with sparse rectangles to Binary (Black/White)
% Cleans the image noise while preserving  contents: Rectangle(s) size
% INPUT ARGUMENTS
% 1 - im_tiff_gray_file_path: [ Required string ] Full path to input file: TIFF gray scale
% 2 - im_tiff_bw_file_path:   [ Required string ]Full path to input file: TIFF
% 3 - min_rect_side_px:       { Optional positive integer ] Minimum rectangle side (default 10 px)
% 4 - min_contrast:           [ Optional positive integer ] Minimum contrast (default 50 px)
% OUTPUT ARGUMENTS:
% 1 success_fg                Boolean: 1 == success.
%                                      0 == Errors on arguments or
%                                           Errors during file open/write.
% OUTPUT FILE:               TIFF Binary with noise removed.


% Parse arguments _________________________________________________

err_msg                   ='';  % No error msg to begin with.
success_fg                = 1;  % Assume things go fine... until a problem is found.
default_rect_min_side_px = 10;  % if no minimum restangle side is specified, 10 is used.
default_min_contrast      = 50; % if no minimum contrast is specified,   50 is used.
th_level                  = double(1.0);  % Threshold value make image binary.

err_msg                   ='';  % No error msg to begin with.
success_fg                = 1;  % Assume things go fine... until a problem is found.
default_rect_min_side_px = 10;  % if no minimum restangle side is specified, 10 is used.
default_min_contrast      = 50; % if no minimum contrast is specified,   50 is used.
th_level                  = double(1.0);  % Threshold value make image binary.
if nargin == 0
    % Missing input and output file
    % Use Default values f
    err_msg=strcat(err_msg,sprintf('\n ERROR: Unspecified Input and output TIFF file paths '));
    success_fg = 0;
end;

if nargin == 1
    % First ARGUMENT SPECIFIED: Input file name
    %
    if (~ischar(im_tiff_gray_file_path))
        err_msg=strcat(err_msg,sprintf(...
            '\n ERROR: Missing output TIFF file path. Only input file path specified %s:' ...
            ,im_tiff_gray_file_path));
        success_fg  = 0;
    end
end

% Real Processing ....
% DONE VERIFING ARGUMENTS _________________________________________________

fprintf('\n Processing file: %s ...\n',im_tiff_gray_file_path)

% MAIN CONVERT TO BINARY script.

if success_fg
    
    % ARGUMENTS ARE VALID: Proceed. _______________________________________
    
    % Open Input TIFF file and retrieve file details.
    
    im_tiff_gray   = imread(im_tiff_gray_file_path);
    h_im_tiff_gray = imshow(im_tiff_gray_file_path);
    imgmodel       = imagemodel(h_im_tiff_gray);
    
    height        = getImageHeight     (imgmodel);  % Return number of rows
    width         = getImageWidth      (imgmodel);  % Return number of cols
    
[Gmag, Gdir] = imgradient(im_tiff_gray(:,:,1),'prewitt');

[Gmag, Gdir] = imgradient(imgmodel,'prewitt');

figure; imshowpair(Gmag, Gdir, 'montage');
title('Gradient Magnitude, Gmag (left), and Gradient Direction, Gdir (right), using Prewitt method')
axis off;    

% try_2: smoothing img
sigma = 2;
smoothImage = imgaussfilt(im_tiff_gray(:,:,1),sigma);
smoothGradient = imgradient(smoothImage,'CentralDifference');

figure
imshow(smoothGradient,[])
title('Smoothed Gradient Magnitude')


end

end % fn locate_features


