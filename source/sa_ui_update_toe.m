function [handles ] = sa_ui_update_toe(handles)
%SA_UI_UPDATE_TOE Updates the toe to be displayed in the ui after requesting list of exper from db i.e. new selection is being done from the UI
%  updates the ndx:  ie using sa_ui_update_toe_ui_cur_exp_net_ndx 
% sample call: [handles ] = sa_ui_update_toe(handles);

%if (strcmpi(handles.ui_state_struct.ui_request_flag,'g_n_a'))
% jas_tbd: review filling  of toe

% JAS: jas_tbd: use new filtered data from db to recreate the toe   NOT READY TO RUN THIS FUNCTION. see: sa_ui_update_tos

% handles.toe_cell(handles.ui_state_struct.ui_cur_exp_net_ndx,handles.toe_col_struct.clasi)= ...
%     cellstr(handles.db_data_exp_struct.db_metad_exp_struct.clasi(handles.ui_state_struct.ui_cur_exp_net_ndx)) ; %  xxxx__first_of_smd __xxxx
% handles.toe_cell(handles.ui_state_struct.ui_cur_exp_net_ndx,handles.toe_col_struct.ndxdb)= ...
%     num2cell((handles.ui_state_struct.ui_cur_fit_net_ndx)) ;

% grab the first 10 cols for the  ui_table_exp table:
if (~(isempty(handles.ui_state_struct.db_cur_exp_net_ndx)))
    handles.toe_cell(handles.ui_state_struct.db_cur_exp_net_ndx,:)= ...
        table2cell( ...
        handles.db_data_exp_struct.expe_req_table(handles.ui_state_struct.db_cur_exp_net_ndx,1:10));
    % Update the toe in the screen:
    set(handles.ui_table_exp,'Data',handles.toe_cell(handles.ui_state_struct.db_cur_exp_net_ndx,:),'Visible','on','Enable','on');  % save_to_handles.ui_table_toe: Data

else
    handles.toe_cell = {};
end
% Update the toe in the screen:
set(handles.ui_table_exp,'Data',handles.toe_cell,'Visible','on','Enable','on');  % save_to_handles.ui_table_toe: Data
%end
end % fn sa_ui_update_toe
