function [ qc_rpt_table,error_message,error_flag ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params )
%GET_QC_RPT_DATA_FROM_RUNCARD_LOT_LIST  given a SPOTTING LOT LIST retrieves a table to be used by the qc_report (Automated version)
%     without knowning the QC WORK ORDER.
%     3 step

% NOTE:  clone of get_qc_rpt_data_from_runcard   
%        key difference: this one uses a list of sp lots. the other one uses a work order number

%Output:
% 1 - qc_rpt_table:  A table with the following columns:
%
%    sp_lot_number mb_work_order mb_lot_number  mb_serial  mb_part_number  carrier_serial  wafer_serial  qc_rack_number  qc_rack_index
%    _____________  _________  ______________  ______________  ____________  ______________  ______________
% 2 - error_message:   if empty: no errors.
% 3 - error_flag:      if 0 no errors. If 1 some errors, ie incomplete data may had been returned.

% Assumptions:
% 1 - The spot failures are logged in RunCard using the SP_FAIL APP. (Ana / Joshua)
% 2 - Sampling plan is performed in a single Work Order at a time.
% 3 - Multiple sampling plans can be performed in a single work order.
% 4 - When there are multiple sampling plans, they must be applied sequentially. SP1, then SP2 etc.
% 5 - The chips from the sampling plan get pick sequentially from the gel pack
% 6 - All the chips from the Sampling Plan go from the gel packs to another gel pack(s):
%     1_ QC gel_pack sp_1
%     2_LSC gel_pack sp_2
% 7 - Two work orders are  generated: QCP0023-QC (positive/negatives) and QCP0023-LSC (parametric: Known patient samples)
%     (The LSC is to be ignored by this function: It is not part of the QC report.Nick confirmed!)
%     sample WO number: WO EWO-0137
% 8 - Only QCP0023-QC  (SP1 I.E. Bin 2) get reported.
%
% 9 - The Virtual QC racks are filled sequentially from rack 1 to the last and each rack has up to 20 chips/ Matchboxes.
% 10- The count of chips of sampling plan 1 (bin 2) must match with the count of the chips in QC gel_packs.
%     This is the only verification (sanity check) that can/MUST be done to detect handling mistakes.
%
% sample call: see: do_get_qc_rpt_data_from_runcard_lot_number.m 
% [ qc_rpt_table,error_message,error_flag ] = get_qc_rpt_data_from_runcard_lot_number( input_params )

%error_message  = '';
error_flag      = 0;
qc_rpt_table    = table();
webservice_url  = input_params.webservice_url; % 'http://10.0.2.226/runcard/soap?wsdl';          % for real
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;

% Get inventory params:

inv_serial          = input_params.inv_serial           ;% '';         % the MB serials is unknown, so it will pull all of the 445 parts
inv_status          = input_params.inv_status           ;% 'CONSUMED';
inv_part_number     = input_params.inv_part_number      ;% '00445';
inv_part_revision   = input_params.inv_part_revision    ;% '01';
inv_work_order      = input_params.inv_work_order       ;% '';     % unknown. What is known is the spotting lot number.
inv_op_code         = input_params.inv_op_code          ;% '';     % jas_tbd. previous op_code ='S380'; 
inv_seq_number      = input_params.inv_seq_number       ;% '';     % jas_tbd.
inv_warehouse_loc   = input_params.inv_warehouse_loc    ;% 'Consumable Inventory';
inv_warehouse_bin   = input_params.inv_warehouse_bin    ;% 'INVENTORY';

qc_sp_lot_list      = input_params.qc_sp_lot_list       ; 
qc_part_bin         = input_params.qc_part_bin          ; 

qc_status           = input_params.qc_status            ;% = 'COMPLETE';
qc_part_number      = input_params.qc_part_number       ;% 'QCP0023-QC';
qc_part_revision    = input_params.qc_part_revision     ;% '01';
qc_work_order       = input_params.qc_work_order        ;% '';                      % 'EWO-0137'; only for dbg.
qc_op_code          = input_params.qc_op_code           ;% 'S380';                      
qc_seq_number       = input_params.qc_seq_number        ;% 30;
qc_warehouse_loc    = input_params.qc_warehouse_loc     ;% 'CLEAN ROOM: SV01.112';     % jas_tbd  or '	CLEAN ROOM: SV01.112'
qc_warehouse_bin    = input_params.qc_warehouse_bin     ;% 'INVENTORY';                % jas_tbd  either WIP or '2' 'INVENTORY'
display_flag        = input_params.display_flag         ;% 1;                             % 1 == display as process goes on. 0 == be quiet

% S1. Get from inventory  ONLY THE QC parts that we are looking for

[inv_response, error_message]   = fetchInventoryItems(obj, inv_serial, inv_status, inv_part_number, inv_part_revision...  % S1 all wafer/die
    , inv_work_order,inv_op_code ,  inv_seq_number, inv_warehouse_loc, inv_warehouse_bin);
if (~(isempty(error_message)))
    error_message =  strcat( error_message, '\nERROR_fn_get_qc_rpt_data_from_runcard_sp_lot_list fetchUnitGenealogy');
    return
end;
if (isempty(inv_response))
    % NO PARTS FOUND IN INVENTORY TO  APPLY FILTERS BY LOT NUMBER.
    error_message ='ERROR: no_parts_in_inventory_found';
    return;
end

% PARTS IN INVENTORY HAVE OR MORE  DIE: 
inv_response_table          = struct2table(inv_response);
%inv_lot_number_list         = char(unique(categorical(inv_response_table.lotnum)));
%inv_serial_cnt              = height(inv_response_table);
inv_response_table.lotnum   = categorical(inv_response_table.lotnum);
% Filter by part_bin and by lotnum: I.E. the list of lot numbers and the part_bin == 2: I.E. for qc
lotnum                      = qc_sp_lot_list;
sp_lot_table                = table(lotnum);
sp_lot_table.lotnum         = categorical(sp_lot_table.lotnum);

inv_response_table_sp       = innerjoin(inv_response_table,sp_lot_table);

qc_part_bin_num             = str2double(qc_part_bin);
inv_table_wafer             = inv_response_table_sp(cell2matdouble(inv_response_table_sp.part_bin) == qc_part_bin_num,:); % '2'
%inv_serial_cnt              = height(inv_table_wafer);

inv_table_wafer.serial     	= categorical(inv_table_wafer.serial); % wafer/die serial.  performance: will do lots of comparisons.
 
% HAVE THE TABLE WITH ONLY THE WANTED LOTS AND QC PARTS. and wafer serials: Pwafer_lot.wafer_number.die_inded
qc_serial ='';
[response, error_value, error_message]   = fetchInventoryItems(obj, qc_serial, qc_status, qc_part_number, qc_part_revision...          % S2 all qc
    , qc_work_order,qc_op_code ,  qc_seq_number, qc_warehouse_loc, qc_warehouse_bin);
if (~(isempty(error_message)))
    error_message =  strcat( error_message, '\nError_value = ',num2str(error_value));
    return
end;
if (isempty(response))
    % NO CARRIERS FOUND
    error_message ='ERROR: no_carriers_found';
    return;
end
% WORK ORDER HAVE 1 OR MORE  CARRIERS
response_table 	= struct2table(response);   % the lot number here is for the carrier or for the kit. I need the spotting_lot_number. (Ryan question)
%lot_number   	= char(unique(categorical(response_table.lotnum)));
serial_cnt      = height(response_table);

% Generate empty table with as many rows as carriers found in the WO.
qc_rpt_headers	= {'sp_lot_number','mb_work_order','mb_lot_number','mb_serial','mb_part_number','qc_rack_number','qc_rack_index','wafer_serial','wafer_lot','wafer_number','die_index'};
data          	= cell(serial_cnt,size(qc_rpt_headers,2));
qc_rpt_table  	= cell2table(data);
qc_rpt_table.Properties.VariableNames = qc_rpt_headers;

% Fill in the mb serial:
% not too quick:  qc_rpt_table.mb_serial = response_table.serial;
delimiter              = {'.'}; % DOT IS THE ONLY VALID SEPARATOR BETWEEN PARTS OF THE SERIAL.
qc_rack_size           = 20;    % jas_hardcoded qc_rac_size
cur_qc_rack_number     = 1;
cur_qc_rack_index      = 0;
included_mb_ndx        = 0;
response_table.serial  = categorical(response_table.serial);

for cur_mb_serial_ndx = 1:1:serial_cnt
    % HAVE MATCH BOX SERIAL: retrieve the wafer info to see if the wafer is one of the wanted P values that belong to one of the lots.            
    % Next lines to be confirmed by Ryan: Get the P for this mb_serial
    insert_chip_into_carrier_step = num2str(qc_seq_number - 10); % 30 -10 --> 20
    [unit_bom_items, error_value, error_message]   = getUnitBOMItems(obj,char(response_table.serial(cur_mb_serial_ndx)),qc_work_order,insert_chip_into_carrier_step);  % 'B160002'
    
    if (~(isempty(error_message)))
        error_message =  strcat(...
            'ERROR_FROM: getUnitBOMItems:', error_message, '\nError_value = ',num2str(error_value),'For serial: ',char(response_table.serial(cur_mb_serial_ndx)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
        continue
    end;
    if (isempty(unit_bom_items))
        % NO BOM FOUND FOR THIS SERIAL
        error_message = sprintf('ERROR_FROM: getUnitBOMItems no_items_found for serial %-s',char(response_table.serial(cur_mb_serial_ndx)));
        error_flag =1;
        if (display_flag)
            sprintf('\n%-s',error_message);
        end
        continue;
    end
    
    % Perform Sanity:
    if ( (isfield(unit_bom_items,'partnum')) && (strcmp(unit_bom_items.partnum, inv_part_number ) ) )  % '00445'
        % FOUND EXPECTED PART NUMBER: Determine if this Pserial is in the list of the wanted serials (it the ones that belong to the wanted lots:
        [wafer_ndx ] = ismember(inv_table_wafer.serial,unit_bom_items.inv_serial); % returns an array containing logical 1 (true) where the data in A is found in B. Elsewhere, the array contains logical 0 (false).
        
        if (~(any(wafer_ndx)))
            % THIS IS not A WANTED P serial: skip it
            continue
        end
        % HAVE A WANTED P VALUE: Store the P and the mb_serial:
         included_mb_ndx                            = included_mb_ndx+1;
         qc_rpt_table.sp_lot_number(included_mb_ndx)= { char(inv_table_wafer.lotnum( wafer_ndx )) }; % save spot lot number
        
        qc_lot_num_cur                              = response_table.lotnum(cur_mb_serial_ndx);      % save match box lot number
        qc_work_order_cur                           = response_table.workorder(cur_mb_serial_ndx);   % save qc work order

        % Get the wafer/die information
        cur_wafer_serial_str                        = split(unit_bom_items.inv_serial,delimiter);
        cur_wafer_lot                               = cur_wafer_serial_str(1);
        cur_wafer_number                            = double(cur_wafer_serial_str(2)); % the second is the wafer number
        cur_die_index                               = double(cur_wafer_serial_str(3)); % the last part (the 3rd) is now the die_index
        
        qc_rpt_table.wafer_serial(included_mb_ndx)  = {unit_bom_items.inv_serial};
        qc_rpt_table.wafer_lot(included_mb_ndx)     = {cur_wafer_lot            };
        qc_rpt_table.wafer_number(included_mb_ndx)  = {cur_wafer_number         };
        qc_rpt_table.die_index(included_mb_ndx)     = {cur_die_index            };
    else
        % NOT INTERESTED IN THIS ONE: Skip it.
        continue;
        %         error_message = sprintf('ERROR_FROM: getUnitBOMItems Empty unit_bom_items for serial %-s',char(response_table.serial(cur_mb_serial_ndx)));
        %         error_flag =1;
        %         if (display_flag)
        %             sprintf('\n%-s',error_message);
        %         end
    end
    
    % Determine rack number and index for current carrier: produces indices: 1 to 20 , 1 to 20 .. to the end
    cur_qc_rack_index     = cur_qc_rack_index + 1;
    if (cur_qc_rack_index > qc_rack_size)
        cur_qc_rack_number= cur_qc_rack_number+1;
        cur_qc_rack_index = 1;
    end
    qc_rpt_table.mb_work_order(included_mb_ndx)   = {qc_work_order_cur                        }; % this is the WO being processed
    qc_rpt_table.mb_lot_number(included_mb_ndx)   =  qc_lot_num_cur                           ; % the lot number  of the carrier becomes the lot  number of the mb
    qc_rpt_table.mb_serial(included_mb_ndx)       = {char(response_table.serial(cur_mb_serial_ndx))}; % the serial      of the carrier becomes the serial of the mb
    qc_rpt_table.mb_part_number(included_mb_ndx)  = {qc_part_number                           };  % the part number of the carrier becomes the part number of the mb
    qc_rpt_table.qc_rack_number(included_mb_ndx)  = {cur_qc_rack_number                       };
    qc_rpt_table.qc_rack_index(included_mb_ndx)   = {cur_qc_rack_index                        };
    
    % Additional info: If you select one carrier, let's say: B1600002
    % 1 - Step 20 / A320 shows the following data:  Within the BOM Item consumption record:
    %     Part number: 00445  Serial / Lot: P162143.08.0002 From inventory    
end % for each serial
if (included_mb_ndx)
    % HAVE SOMETHING: trim it to the real size
    qc_rpt_table = qc_rpt_table(1:included_mb_ndx,:);
else
    % HAVE NOTHING: return empty table     
    qc_rpt_table = table();
end
end % fn get_qc_rpt_data_from_runcard_lot_number

