% ref: get_qc_rpt_data_from_runcard_sp_lot_list_do  is a script to call: get_qc_rpt_data_from_runcard_sp_lot_list and  get_qc_rpt_data_from_runcard

% S1: to run: get_qc_rpt_data_from_runcard_sp_lot_list
%             fill in input paramteres with workorder and needed details: using list of spotting lots.
input_params                    = {};
input_params.webservice_url     = 'http://10.0.2.226/runcard_test/soap?wsdl';
input_params.sp_chip_part_number= '00445';            
input_params.qc_sp_lot_list     = { 'AB1602' , 'AB160J' }; % {'AB150I'  }; %   { 'AB1602' };             % spot lot number list 
input_params.qc_part_number     = 'QCP0023-QC'; % spotted lot part number.
input_params.display_flag       = 1;                        % 1 == display as process goes on. 0 == be quiet
% call: get_qc_rpt_data_from_runcard
[ qc_rpt_table,error_message ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params );

% This table is the one needed to automate the qc_reports.

disp('DONE RETRIEVING QC_RPT_DATA FROM RUNCARD')
 
% input_params.part_number   = '00445';
% input_params.op_code       = 'A320';                      
% input_params.seq_number    = 20;

% Francis qc:   experiment name:  NA-QC-AB1602
%               Spotting lot      AB1602


