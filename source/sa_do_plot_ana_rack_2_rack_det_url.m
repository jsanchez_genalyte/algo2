%%                            QC REPORT: RACK TO RACK SENSOR COMPARISON
%%  RACK TO RACK sensor comparison for each lot and for analyte. 
%  Green   scatter dots:  sensor 1
%  Blue    scatter dots:  sensor 2
%  Red     scatter dots:  sensor 3
%  Orange  scatter dots:  sensor 4
%  Source: Assay Dev Detail Report (JERVIS API and RUNCARD)

%  To run: sa_do_plot_ana_rack_2_rack_det_url
%  url:    file:///C:/Users/jsanchez/Documents/algo2/source/html/sa_do_plot_ana_rack_2_rack_det_url.html
% color_sensor:      1_fake             1_green      2_blue       3            4
color_shape_code = { rgb('Black')  rgb('Green')  rgb('Blue') rgb('Purple') rgb('Orange') };
shape_code       = { 's'                's'           '^'          's'         '^' };
% % % channel_1 square    
% % % channel_2 triangle   '^'	Upward-pointing triangle


close_all_true_figures;
fig_btm                 = 50;
fig_left                = 50;
fig_width               = 1600;
fig_height              = 900;
height_delta            = 0.15; % .2 units will be taller the box than the tex
load('qc_rack_2_rack_det_struct.mat','qc_lot_2_lot_det_struct','qc_rack_2_rack_det_struct')
ana_cnt                 = size(qc_lot_2_lot_det_struct,2)-1;
plot_lots_per_win       = 2;
for cur_ana_ndx=1:1:ana_cnt % jas_temp_debug:  ana_cnt % jas_temp_debug_ ana_cnt
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both
    spotl_names_list    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    name_ana            = qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana ;
    col_values_cat_u    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    spotl_cnt           = length(col_values_cat_u); %
    sub_plot_hor_cnt    = 2;	        % left: spotl1 right: spotl2 left. Change it from  2 to 3 or 4 to see more subplots horizontally: jas_hard_coded
    sub_plot_ver_cnt    = 2;            % top: plot btm: text for summary stats table
    num_sub_plots       = sub_plot_hor_cnt*sub_plot_ver_cnt;          	

    hFig                = figure('Name',sprintf('  RACK TO RACK SENSOR variability. Analyte: %-s ',name_ana));    
    set(hFig, 'Position', [fig_left fig_btm  fig_width fig_height]);
    fig_left            = fig_left+0;
    cur_sub_plot_num    = 0;    
    senso_cnt           = size(qc_lot_2_lot_det_struct(cur_ana_ndx).data_aggr_mat,3); % third dim is the senso
     
    
    for cur_spotl_ndx=2:1:spotl_cnt
        % PRODUCE BOX PLOTS FOR CURRENT ANALYTE AND CURRENT SPOTL:  by qc_rack_number
        cur_sub_plot_num= cur_sub_plot_num +1;
        if (cur_sub_plot_num > sub_plot_hor_cnt)
            % DONE WITH THIS WINDOW: (ALL SUBPLOTS DONE) need to create a new window and reset cur_sub_plot_num
            cur_sub_plot_num = 1;
            hFig    = figure('Name',sprintf('  RACK TO RACK sensor variability. Analyte: %-s ',name_ana));
            set(hFig, 'Position', [fig_left fig_btm fig_width fig_height]);
            fig_left = fig_left+0;
        end
        
        subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,cur_sub_plot_num); % top part: plot. Btm part: table.
        hold all;
        ana_lot_rack_struct     = qc_rack_2_rack_det_struct(cur_ana_ndx,cur_spotl_ndx,col_senso_both).ana_lot_rack_struct;
        data_aggr_mat           = ana_lot_rack_struct.data_aggr_mat; % buf_fix_06_18: both sensors  qc_lot_2_lot_det_struct
        name_cur_spotl          = ana_lot_rack_struct.name_ana;
        col_values_cat_u        = ana_lot_rack_struct.col_values_cat_u;
        rack_cnt                = length(col_values_cat_u);
          
        % Produce Box Plots for all available lots for each analyte
        % display(name_cur_spotl) position: [left bottom width height]
        ax_box                  = gca;
        set(ax_box,'Units','normalized')
        ax_box_position_ori     = ax_box.Position;
        ax_box_position_new     = ax_box_position_ori;
        ax_box_position_new(2)  = ax_box_position_ori(2) - height_delta; % lower the btm of the box
        ax_box_position_new(4)  = ax_box_position_ori(4) + height_delta; % make taller      the box
        ax_box.Position         = ax_box_position_new;        
        
        boxplot(ax_box,data_aggr_mat,'notch','on',...     %% buf_fix_06_18:  last dimension is sensor and last value is all sensors.
            'labels',{ char(col_values_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))
        set(ax_box,'FontSize',9);
        str_title = strcat(strrep(name_cur_spotl,'_','-'),'.  Positive Cluster Mean  .','  ',qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana);
        title(ax_box,strrep(str_title,'.',' '),'FontSize',11);
        ylabel(ax_box, strcat(strrep(name_cur_spotl,'_','-'),' - GRU') );
        
        % draw the individual dots: for each of the 4 sensors
        % Do scatter plots to plot individual dots ___________________________________________
        x_axis_qc_plot  = get(ax_box,'XAxis');
        x_limits_qc_plot= x_axis_qc_plot.Limits;
        x_min           = x_limits_qc_plot(1);
        x_max           = x_limits_qc_plot(2);
        x_range         = x_max - x_min;
        x_inc           = x_range / ((2*rack_cnt));
        cur_start_x     = x_min + (x_inc);
        for cur_rack_ndx = 1:1:rack_cnt
            for cur_senso_ndx = 2:1:senso_cnt
                data_aggr_mat 	= qc_rack_2_rack_det_struct(cur_ana_ndx,cur_spotl_ndx,cur_senso_ndx).ana_lot_rack_struct.data_aggr_mat;                
                cur_x_plot      = zeros(size(data_aggr_mat));
                %cur_x_plot(:,cur_rack_ndx) = cur_start_x;
                if (cur_senso_ndx == 2)
                    cur_x_plot(:,cur_rack_ndx) = cur_start_x- (x_inc/1.2);
                end
                if (cur_senso_ndx == 3)
                    cur_x_plot(:,cur_rack_ndx) = cur_start_x- (x_inc/1.5);
                end   
                if (cur_senso_ndx == 4)
                    cur_x_plot(:,cur_rack_ndx) = cur_start_x+ (x_inc/1.5);
                end
                if (cur_senso_ndx == 5)
                    cur_x_plot(:,cur_rack_ndx) = cur_start_x+ (x_inc/1.2);
                end                  
                
                scatter(ax_box,cur_x_plot(:,cur_rack_ndx),data_aggr_mat(:,cur_rack_ndx)...
                    ,'MarkerFaceColor',color_shape_code{cur_senso_ndx}...
                    ,'MarkerEdgeColor',color_shape_code{cur_senso_ndx}...                           
                    ,'Marker'         ,shape_code      {cur_senso_ndx}...       
                    ,'LineWidth'      ,0.2...                    
                    ,'SizeData'       ,9); % ,'MarkerFaceColor',dark_green_color);
            end
            cur_start_x         = cur_start_x  + (2*x_inc); % move to the next boxplot            
        end % cur_rack_ndx
        set(ax_box,'XGrid','on');
        set(ax_box,'YGrid','off');
        grid on;
        
        % Summary statistics: data
        data_sum_stats          = ana_lot_rack_struct.data_sum_stats;
        data_sum_stats_table    = array2table(data_sum_stats);
        rack_number             = col_values_cat_u;
        data_sum_stats_table    = [ table(rack_number) data_sum_stats_table ];
        data_sum_stats_header   = {'QC_RACK','N ','MEAN   ','STD    ','CV'};
        data_sum_stats_table.Properties.VariableNames= data_sum_stats_header;
        
        % Summary statistics: text on plot
        cur_sub_text_num        =  cur_sub_plot_num + sub_plot_hor_cnt;
        subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,cur_sub_text_num) % top part: plot. Btm part: table.
        hold all;
        ax_tex                  = gca;
        set(ax_tex,'Units','normalized')
        ax_tex_position_ori     = ax_tex.Position;
        ax_tex_position_new     = ax_tex_position_ori;
        ax_tex_position_new(4)  = ax_tex_position_ori(4) - (height_delta/3); % make shorter     the text (ony 5 racks
        ax_tex.Position         = ax_tex_position_new;
        ax_tex.Visible          = 'off';
        sum_title_cell          = { sprintf(' ANALYTE: %-s SPOTTING LOT: %-s\n',name_ana,name_cur_spotl) };
        
        data_sum_stats_table.QC_RACK(1) ={'0'};
        data_sum_stats_table.QC_RACK = cell2mat(data_sum_stats_table.QC_RACK);
        data_sum_stats_cel      = table2cell((data_sum_stats_table(:,1)));
        QC_RACK                 = cell2matdouble(data_sum_stats_cel(:,1));
        data_sum_stats_table    = [ table(QC_RACK) data_sum_stats_table(:,2:end) ];       
        col_head                = data_sum_stats_table.Properties.VariableNames;
        col_head                = strrep(col_head,'_','-');
        col_head{1}             = 'QC RACK';
        wid                     = [7,  12,  20,  20,  20  ];
        fms                     = {'4.0f','4.0f','15.1f','19.1f','19.2f'};
        row_head                = {}; %   {'', '  ', '  ', '  ', '  ' };% , fid, colsep, rowending)
        fid                     = 1;
        col_sep                 = ' ';
        row_end                 = '';
        text_cell               = displaytable_to_cell(data_sum_stats_table,col_head, wid,  fms,row_head,fid, col_sep, row_end);        
        temp_by_str = strcat(upper(name_ana), ' BY ');
        t = text(0.05, 0.97 - (0.0),{temp_by_str}); % 0.97- --> 0 
        t(1).Color = 'Blue';
        t(1).FontSize = 14;        
        for cur_rack_ndx = 1:1:height(data_sum_stats_table)+1
            if (cur_rack_ndx == 1)
                row_cell = {['' text_cell{cur_rack_ndx} ] }; %Rack 
            else             %num2str(cell2mat(table2cell(data_sum_stats_table(cur_rack_ndx-1,1)))) , 
                row_cell = { [ '        ',    char(text_cell{cur_rack_ndx})] };
            end            
            t = text(0.05, 0.80 -(0.17*(cur_rack_ndx-1)),row_cell); % 0.97- --> 0   0.17
            if (cur_rack_ndx == 1)
                t(1).Color = 'red';
            end
        end
        hold off;
    end % for each spotting lot
end % for each analyte: : plotting each on it's own window.

%% Summary statistics: For Individual Analyte - Spotting Lot. Text Tables Only 

for cur_ana_ndx=1:1:1 %jas_temp   ana_cnt % jas_temp_debug_ ana_cnt or 1
    % PRODUCE SUM STATS FOR CURRENT ANALYTE:  both
    for cur_spotl_ndx=1:1:spotl_cnt
        % PRODUCE SUM STATS FOR CURRENT ANALYTE AND CURRENT SPOTL:  by qc_rack_number
        %cur_sub_plot_num = cur_spotl_ndx;
        %subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,cur_sub_plot_num); % top part: plot. Btm part: table.
        %hold all;
        ana_lot_rack_struct = qc_rack_2_rack_det_struct(cur_ana_ndx,cur_spotl_ndx).ana_lot_rack_struct;
        data_aggr_mat       = ana_lot_rack_struct.data_aggr_mat;
        name_cur_spotl    	= ana_lot_rack_struct.name_ana;
        col_values_cat_u 	= ana_lot_rack_struct.col_values_cat_u;
        rack_cnt         	= length(col_values_cat_u);
        
        %fig_qc_rack_to_rack.Position %  [left bottom width height]
        data_sum_stats          = ana_lot_rack_struct.data_sum_stats;
        data_sum_stats_table    = array2table(data_sum_stats);
        rack_number             = col_values_cat_u;
        data_sum_stats_table    = [ table(rack_number) data_sum_stats_table ];
        data_sum_stats_header   = {'QC_RACK','N ','MEAN   ','STD    ','CV'};
        data_sum_stats_table.Properties.VariableNames= data_sum_stats_header;
        
        %% Summary statistics For Individual Analyte - Spotting Lot
        %sum_title_cell = { sprintf(' ANALYTE: %-s SPOTTING LOT: %-s\n',name_ana,name_cur_spotl) };
        data_sum_stats_table.QC_RACK(1) ={'0'};
        data_sum_stats_table.QC_RACK = cell2mat(data_sum_stats_table.QC_RACK);
        data_sum_stats_cel = table2cell((data_sum_stats_table(:,1)));
        QC_RACK = cell2matdouble(data_sum_stats_cel(:,1));
        data_sum_stats_table    = [ table(QC_RACK) data_sum_stats_table(:,2:end) ];        

        col_head    = data_sum_stats_table.Properties.VariableNames;
        col_head    = strrep(col_head,'_','-');
        col_head{1} = 'QC-RACK';
        wid         = [8,  12,  20,  20,  20  ];
        fms         = {'4.0f','4.0f','15.1f','19.1f','19.2f'};
        row_head    = {}; % '', '  ', '  ', '  ', '  ' };% , fid, colsep, rowending)
        fid         = 1;
        col_sep     = ' ';
        row_end     = '';
        fprintf('\n Analyte: %s   - \tspotting lot: %-s\n',qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana,name_cur_spotl);
        
        displaytable(table2array(data_sum_stats_table),col_head, wid,  fms,row_head,fid, col_sep, row_end);
        
     end % for each spotting lot
end % for each analyte: : plotting each on it's own window.

% profiles styleshee.xsl
% https://www.mathworks.com/matlabcentral/answers/98455-is-it-possible-to-change-the-size-of-my-fonts-and-background-color-when-i-generate-an-html-document

