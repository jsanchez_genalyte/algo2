function [ error_message ] = sa_rest_exp_save_scan(local_hd_struct,sensor_scan_struct,cur_expid)
%sa_rest_exp_save_scan saves the scan data for a whole experiment into a matlab file
%   The name of the matlab file is the exp_id. The contents is 3 integers and a matrix of doubles:
%   3 integers:   sensor_cnt,scan_len_max,scan_col_max,
%   1 matrix:     sensor_data_mat:    rows in blocks of: scan_len_max  total rows is scan_len_max * sensor_cnt
%                                     col_1 to scan_len_max is the real scan data. (the sensor number is implicit every scan_len_max rows)
%   1 vector:     sensor_type
%  1 string:      protocol_id
% see:                  sa_rest_exp_get_scan_from_hd
% pipeline:             sa_rest_exp_get_scan_from_db.m or sa_rest_exp_get_scan_from_hd.m --> sa_rest_exp_save_scan
% sample call:          [ error_message, sensor_data_mat ] = sa_rest_exp_save_scan(exp_scan_cell,'01101ef0-47bd-49f9-af6b-a5cc8087e64a'); % 'cur_expid)
error_message           = '';

% HAVE sensor_data_mat with exact dimentions: Save it:
% save (filename_matlab,'sensor_id_list_str');

local_hd_db_data_expr_scan_mat_dir  = local_hd_struct.local_hd_db_data_expr_scan_mat_dir;   % 'C:\Users\jsanchez\Documents\algo2\db_data_scan\';
db_data_expe_scan_file_name         = strcat(local_hd_db_data_expr_scan_mat_dir,cur_expid,'.mat');
sensor_cnt                          = sensor_scan_struct.sensor_cnt;
scan_len_max                        = sensor_scan_struct.scan_len_max;
scan_col_max                        = sensor_scan_struct.scan_col_max;
sensor_data_mat                     = sensor_scan_struct.sensor_data_mat;
sensor_type                         = sensor_scan_struct.sensor_type;
proid                               = sensor_scan_struct.proid;    % jas_here before sasi
save                                 (db_data_expe_scan_file_name,'sensor_cnt','scan_len_max','scan_col_max','sensor_data_mat','sensor_type','proid');
%sensor_data_mat = sensor_data_mat(:,1:3); % return just the 3 cols: sensor_id,time,raw_gru.
end % fn sa_rest_exp_save_scan
