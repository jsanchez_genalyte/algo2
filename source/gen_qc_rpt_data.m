function [ assay_sum_table, assay_det_table,error_message] = gen_qc_rpt_data( expe_ned_table,exp_full_struct,handles_text_exp_status,use_norm_flag,exc_outl_flag  )
%GEN_QC_RPT_DATA Uses the exp_full_data from a set of experiments. Goal: Automate the qc rpt (To replace Excel/Jervis Porta)
%   Produces the summay and detail tabs of the assay dev jervis excel file using the Jervis API

% pipeline:  sa_do_push_top_sens --> gen_qc_rpt_data
% S0. Retrieve data from Run-card: see: get_qc_rpt_data_from_runcard_sp_lot_list_do
% S1: to run: get_qc_rpt_data_from_runcard_sp_lot_list
%             fill in input paramteres with workorder and needed details: using using list of spotting lots

% pipeline: sa_do_push_top_sens --> gen_qc_rpt_data -->  aggreate_column_for_table 
%                                                   -->  (old way: sa_qc_calc_lot_2_lot)



assay_sum_table = table();
assay_det_table = table();


% call: get_qc_rpt_data_from_runcard
%jas_tbd_when_find_proper_test_with_mario_alex: [ qc_rpt_table,error_message ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params );

% Pre-Allocate arrays to store: summary and detail excel tabs info for all the selected experiments.
exp_request_cnt = height(expe_ned_table);
cur_sum_esti_cnt= exp_request_cnt * 2;  % 2 = channels per exper.
cur_det_esti_cnt= exp_request_cnt * 8;  % 8 = channels per exper * 4 sensors per probe.

%pretty version of the headers for rpt display
assay_com_h_pret = {'Experiment Name','Chip Spotting Lot Number','MB Carrier ','Run Date','Protocol Name','Instrument ID','Sample Count','Sample ID','Channel'};
assay_ana_h_pret = {'HSA','dsDNA','HSA3','Jo-1','Sm','HSA6','Scl-70','HSA8','Cenp B','HSA10','SS-A 60','HSA12','SS-B','RNP','Anti-IgG','Human IgG','Baseline Start','Baseline Duration','Amplified Start','Amplified Duration','GUID'};


assay_com_header = {'exper','spotl','carri','dates','proto','instr','samcnt','samid','chann'};
assay_ana_header = {'hsa','dsdna','hsa3','jo_1','sm','hsa6','scl_70','hsa8','cenp_b','hsa10','ss_a_60','hsa12','ss_b','rnp','anti_igg','human_igg'};
assay_tai_header = {'blines','blined','ampls','ampld','expid'};

assay_sum_header = [ assay_com_header             assay_ana_header  assay_tai_header];

assay_det_header = [ assay_com_header {'sensor'}  assay_ana_header assay_tai_header];

% sum
assay_sum_com_cell                          = cell(cur_sum_esti_cnt,size(assay_com_header,2));
assay_sum_com_table                         = cell2table(assay_sum_com_cell);
assay_sum_com_table.Properties.VariableNames= assay_com_header;

assay_sum_ana_cell                          = cell(cur_sum_esti_cnt,size(assay_ana_header,2));
assay_sum_ana_table                         = cell2table(assay_sum_ana_cell);
assay_sum_ana_table.Properties.VariableNames= assay_ana_header;
assay_sum_ana_mat                           =nan(size(assay_sum_ana_table));

assay_sum_tai_cell                          = cell(cur_sum_esti_cnt,size(assay_tai_header,2));
assay_sum_tai_table                         = cell2table(assay_sum_tai_cell);
assay_sum_tai_table.Properties.VariableNames= assay_tai_header;

%det:
assay_det_com_cell                          = cell(cur_det_esti_cnt,size(assay_com_header,2));
assay_det_com_table                         = cell2table(assay_det_com_cell);
assay_det_com_table.Properties.VariableNames= assay_com_header;

assay_det_ana_cell                          = cell(cur_det_esti_cnt,size(assay_ana_header,2));
assay_det_ana_table                         = cell2table(assay_det_ana_cell);
assay_det_ana_table.Properties.VariableNames= assay_ana_header;
assay_det_ana_mat                           =nan(size(assay_det_ana_table));

assay_det_tai_cell                          = cell(cur_det_esti_cnt,size(assay_tai_header,2));
assay_det_tai_table                         = cell2table(assay_det_tai_cell);
assay_det_tai_table.Properties.VariableNames= assay_tai_header;


% columns to fill so we have everything to populate the toa_sum and toa_det

for cur_exp_ndx_ok = 1:1:size(exp_full_struct,2)  %: jas_temp: account for failures: exp_request_cnt % ________________________________________________________________________________________________
    cur_exp_ndx    =exp_full_struct{cur_exp_ndx_ok}.ok_data_array_ndx;
    %msg = sprintf('cur_exp_ndx_ok = %-4d ,cur_exp_ndx = %-4d', cur_exp_ndx_ok,cur_exp_ndx);
    %fprintf('\n%s',msg);
%     cur_exp_ndx_ok
%     if (cur_exp_ndx_ok == 15)
%             fprintf('\n%s',msg);
%     end
    exp_ndx_odd = ((cur_exp_ndx_ok-1)*2)+1;
    exp_ndx_even= exp_ndx_odd+1;
    % S2. EXP_DETAILS PART: ________
    status_msg = sprintf('Processing ASSAY_DEV: for experiment  %5d  of  %5d ... Please wait .....',cur_exp_ndx_ok,exp_request_cnt);
    protocol_struct      = exp_full_struct{cur_exp_ndx_ok}.protocol_struct;
    exp_det_struct       = exp_full_struct{cur_exp_ndx_ok}.exp_det_struct;
    exp_probe_struct     = exp_full_struct{cur_exp_ndx_ok}.exp_probe_struct;
    exp_sensor_struct    = exp_full_struct{cur_exp_ndx_ok}.exp_sensor_struct;
    %scan_data_struct     = exp_full_struct{cur_exp_ndx}.scan_data_struct;
    if (~(mod(exp_request_cnt,20)))
        set(handles_text_exp_status,'string',status_msg,'ForegroundColor','blue','Fontsize',12); %'blue'
        %lprintf_log( '\n%s\n',status_msg);
        drawnow;
    end
    
    % Generate Assay Summary Tab for current experiment:
    exper_senso_table                           = struct2table(exp_sensor_struct);    
    probe_map                                   = protocol_struct.probe_map;
    probe_map_table                             = struct2table(probe_map);
    cur_baseline_start  = {''};
    if ( strcmp(protocol_struct.parameters(3).name, 'Baseline Interval Start'))
        cur_baseline_start  = {protocol_struct.parameters(3).value};
    end
    cur_baseline_duration  = {''};
    if ( strcmp(protocol_struct.parameters(4).name, 'Baseline Interval Duration'))
        cur_baseline_duration   = {protocol_struct.parameters(4).value};
    end
    cur_amplified_start  = {''};
    if ( strcmp(protocol_struct.parameters(5).name, 'Amplified Interval Start'))
        cur_amplified_start    = {protocol_struct.parameters(5).value};
    end
    cur_amplified_duration  = {''};
    if ( strcmp(protocol_struct.parameters(6).name, 'Amplified Interval Duration'))
        cur_amplified_duration = {protocol_struct.parameters(6).value};
    end
    
    cur_spotl = expe_ned_table.spotl{cur_exp_ndx};
    if (isfield(exp_det_struct,'consumable'))
        if (~(isempty(exp_det_struct.consumable)))
            %cur_carri = exp_det_struct.consumable.carrier_serial_number;
            %cur_spotl = exp_det_struct.consumable.spotting_lot_number;
            cur_chipl = exp_det_struct.consumable.lot_number;      %jas_tbd_is this correct? chip or kit lot number? or something else
            if  (isempty(cur_chipl))
                cur_chipl = cur_spotl;  %jas_tbd: to avoid empty chiplots: Use spot lot.
            end
        end
    end
    cur_sampl_cnt = 0;
    cur_sampl_cnt_1=0;
    cur_sampl_cnt_2=0;
    cur_sampl_1   ='';
    cur_sampl_2   ='';
    if (isfield(exp_det_struct,'specimen_id'))
        if (~(isempty(exp_det_struct.specimen_id)))
            cur_sampl_cnt = size(exp_det_struct.specimen_id,1);
            cur_sampl_1 = exp_det_struct.specimen_id{1};  % jas_tbd define where to store specimen
            if (cur_sampl_cnt == 2)
                cur_sampl_2 = exp_det_struct.specimen_id{2};
                % temp1 = exp_det_struct.specimen_id{2};% jas_tbd define what to do
                cur_sampl_cnt_1=1;
                cur_sampl_cnt_2=2;
            else
                cur_sampl_cnt_1=1;
                cur_sampl_cnt_2=1;
            end
        end
    end
    
    % ready to populate fields of the assay_summary_table
    
    assay_sum_com_table.exper{exp_ndx_odd}  = expe_ned_table.exper{cur_exp_ndx};
    assay_sum_com_table.exper{exp_ndx_even} = expe_ned_table.exper{cur_exp_ndx};
    assay_sum_com_table.spotl{exp_ndx_odd}  = expe_ned_table.spotl{cur_exp_ndx};
    assay_sum_com_table.spotl{exp_ndx_even} = expe_ned_table.spotl{cur_exp_ndx};
    assay_sum_com_table.carri{exp_ndx_odd}  = expe_ned_table.carri{cur_exp_ndx};
    assay_sum_com_table.carri{exp_ndx_even} = expe_ned_table.carri{cur_exp_ndx};
    assay_sum_com_table.dates{exp_ndx_odd}  = expe_ned_table.dates{cur_exp_ndx};
    assay_sum_com_table.dates{exp_ndx_even} = expe_ned_table.dates{cur_exp_ndx};
    assay_sum_com_table.proto{exp_ndx_odd}  = expe_ned_table.proto{cur_exp_ndx};
    assay_sum_com_table.proto{exp_ndx_even} = expe_ned_table.proto{cur_exp_ndx};
    assay_sum_com_table.instr{exp_ndx_odd}  = expe_ned_table.instr{cur_exp_ndx};
    assay_sum_com_table.instr{exp_ndx_even} = expe_ned_table.instr{cur_exp_ndx};
    assay_sum_com_table.samcnt{exp_ndx_odd} = cur_sampl_cnt_1;
    assay_sum_com_table.samcnt{exp_ndx_even}= cur_sampl_cnt_2;
    assay_sum_com_table.samid{exp_ndx_odd}  = cur_sampl_1;
    assay_sum_com_table.samid{exp_ndx_even} = cur_sampl_2;
    assay_sum_com_table.chann{exp_ndx_odd}  = 1;
    assay_sum_com_table.chann{exp_ndx_even} = 2;
    
    % Fill in the assay(ie analyte) deppendent part:
    %     ana_start_col = size(assay_com_header,2) + 1;
    %     ana_end_col   = size(assay_ana_header,2) + ana_start_col -1;
    
    assay_sum_tai_table.blines{exp_ndx_odd} = cur_baseline_start;
    assay_sum_tai_table.blines{exp_ndx_even}= cur_baseline_start;
    assay_sum_tai_table.blined{exp_ndx_odd} = cur_baseline_duration;
    assay_sum_tai_table.blined{exp_ndx_even}= cur_baseline_duration;
    assay_sum_tai_table.ampls{exp_ndx_odd}	= cur_amplified_start;
    assay_sum_tai_table.ampls{exp_ndx_even} = cur_amplified_start;
    assay_sum_tai_table.ampld{exp_ndx_odd}  = cur_amplified_duration;
    assay_sum_tai_table.ampld{exp_ndx_even} = cur_amplified_duration;
    assay_sum_tai_table.expid{exp_ndx_odd}  = expe_ned_table.expid{cur_exp_ndx};
    assay_sum_tai_table.expid{exp_ndx_even} = expe_ned_table.expid{cur_exp_ndx};
    
    % pack sensor data to do calculations
    assay_cnt = 16; % jas_hardcoded
    [assay_sens_struct,assay_sens_table,error_message]  = get_assay_sensor_data_from_sensor(exp_sensor_struct,assay_cnt);
    %niu:   cur_sensor_cnt                                      = size(exp_sensor_struct,1);
    %niu:  [ sensor_table ]                                    = probe_get_sensor_names_and_numbers( probe_map_table, cur_sensor_cnt );
    
    % calculations for the assay_summary_table
    % %     if (cur_exp_ndx ==  24)
    % %         cur_exp_ndx
    % %     end
    
    if (use_norm_flag)
        shift = cell(assay_sens_table.shifj);
    else
        shift = cell(assay_sens_table.shifr);
    end
    shift_mean_odd = nan(size(shift,1)/2,1);
    shift_mean_even= nan(size(shift,1)/2,1);
    ch     = 1;
    ndx_ch = 1;
    if (exc_outl_flag)
        % USER REQUESTED TO exclude OUTLIERS: filter for outliers
        outfg       = cell(assay_sens_table.outfg);
        for ndx = 1:1:size(shift,1)
%             if (ndx == 17)
%                 ndx
%             end
            shift_val       = nan(4,1); %  cell2mat(shift(ndx,:));
            outfg_cur       = zeros(size(shift_val));
            %outfg_cur      =  outfg(ndx,:);
            [ outfg_cur ] = logical(cell2mat_nan( outfg(ndx,:),outfg_cur,1  ));
            [ shift_val ] =         cell2mat_nan( shift(ndx,:),shift_val,nan );            
            
            % Handle missing data:
%             for cur_senso_out_ndx = 1:1:size(outfg_cur,2)
%                 if  ( (isempty(outfg_cur{cur_senso_out_ndx}))  )
%                     % *** TAKE MISSING OUTLIER FLAG AS OUTLIER:
%                     outfg_cur{cur_senso_out_ndx} =logical(1);
%                     shift_val(cur_senso_out_ndx) = nan;
%                 else
%                     shift_val(cur_senso_out_ndx) = cell2mat(shift(ndx,cur_senso_out_ndx));
%                 end
%             end
       
            outfg_val       = ~outfg_cur;                           % ~(cell2mat(outfg_cur));
            shift_val_use   = shift_val(outfg_val);
            if (ch==1)
                shift_mean_odd(ndx_ch) = nanmean(shift_val_use);    % 32 assay mean.
                ch=2;
            else
                shift_mean_even(ndx_ch) = nanmean(shift_val_use);   % 32 assay mean.
                ndx_ch=ndx_ch+1;
                ch=1;
            end
        end
    else
        % USER REQUESTED TO include OUTLIERS: Use all data.
        for ndx = 1:1:size(shift,1)
            if (ch==1)
                shift_mean_odd(ndx_ch) = nanmean(cell2mat(shift(ndx,:)));   % 32 assay mean.
                ch=2;
            else
                shift_mean_even(ndx_ch) = nanmean(cell2mat(shift(ndx,:)));   % 32 assay mean.
                ndx_ch=ndx_ch+1;
                ch=1;
            end
        end
    end
    % calc the means per assay:
    assay_sum_ana_mat(exp_ndx_odd,:)  = shift_mean_odd ;
    assay_sum_ana_mat(exp_ndx_even,:) = shift_mean_even; % just for fun: jas_tbd right using map.
    

    % DONE WITH ASSAY SUMM TABLE for current experiment
    % Generate Assay Detail Tab for current experiment:
    
    % ready to populate fields of the assay_summary_table  jas_tbr_all   shift: 128 = 32x4   excel 16x8 = 128

    for det_ndx_ref=1:1:8  % 8 recs per experiment:
        det_ndx   = (((cur_exp_ndx_ok-1)*8) +1) + det_ndx_ref - 1;
        
        assay_det_tai_table.blines{det_ndx} = cur_baseline_start;
        assay_det_tai_table.blined{det_ndx} = cur_baseline_duration;
        assay_det_tai_table.ampls{det_ndx}	= cur_amplified_start;
        assay_det_tai_table.ampld{det_ndx}  = cur_amplified_duration;
        assay_det_tai_table.expid{det_ndx}  = expe_ned_table.expid{cur_exp_ndx};
        
        assay_det_com_table.exper{det_ndx}  = expe_ned_table.exper{cur_exp_ndx};
        assay_det_com_table.spotl{det_ndx}  = expe_ned_table.spotl{cur_exp_ndx};
        assay_det_com_table.carri{det_ndx}  = expe_ned_table.carri{cur_exp_ndx};
        assay_det_com_table.dates{det_ndx}  = expe_ned_table.dates{cur_exp_ndx};
        assay_det_com_table.proto{det_ndx}  = expe_ned_table.proto{cur_exp_ndx};
        assay_det_com_table.instr{det_ndx}  = expe_ned_table.instr{cur_exp_ndx};
        if (det_ndx_ref <=4)
            assay_det_com_table.chann{det_ndx}  = 1;
            assay_det_com_table.sensor{det_ndx} = det_ndx_ref;            
            assay_det_com_table.samid{det_ndx}  = cur_sampl_1;
            assay_det_com_table.samcnt{det_ndx} = cur_sampl_cnt_1;
        else
            assay_det_com_table.chann{det_ndx}  = 2;
            assay_det_com_table.sensor{det_ndx} = det_ndx_ref-4;               
            assay_det_com_table.samid{det_ndx}  = cur_sampl_2;
            assay_det_com_table.samcnt{det_ndx} = cur_sampl_cnt_2;
        end
    end
    
    % Produce the real data for the assay detail tab
    [ senso_map_table ]   = sensor_build_all_maps( probe_map_table );    
    [ assay_det_ana_mat_cur ] = sensor_cvt_to_excel_format( probe_map_table,senso_map_table,exper_senso_table ); % source: dcrc_genxml
    % save part of the assay_det_ana_mat_cur: cols 3 to 18 and the 8 rows:
    assay_det_ana_mat(det_ndx-8+1:det_ndx,:) = assay_det_ana_mat_cur(:,3:end);
    
end % for each expid : % _________________________________________________________________________________________

% Pack tables to match assay dev sum and detail as in the excel files:
assay_sum_ana_mat   = assay_sum_ana_mat(1:exp_ndx_even(end),:);
aa_table            = array2table(assay_sum_ana_mat);
assay_sum_table     = [ assay_sum_com_table(1:exp_ndx_even(end),:)   aa_table    assay_sum_tai_table(1:exp_ndx_even(end),:)];
assay_sum_table.Properties.VariableNames= assay_sum_header;

assay_det_ana_mat   = assay_det_ana_mat(1:det_ndx,:);
aa_table_det        = array2table(assay_det_ana_mat(1:det_ndx,:));
assay_det_table     = [ assay_det_com_table(1:det_ndx,:)   aa_table_det assay_det_tai_table(1:det_ndx,:)];
assay_det_table.Properties.VariableNames= assay_det_header;

% old way: calc and plot in one shot and alll of them in one window with a 4x4 
% [ data_box_plot,data_odd_plot,data_even_plot ] = do_test( assay_sum_table,assay_sum_ana_mat );                    % old orig way.

% for test/compare only:
% [ qc_lot_2_lot_struct_old,error_message ]           = sa_qc_calc_lot_2_lot( assay_sum_table,assay_sum_ana_mat );
% [ error_message ]                                   = sa_do_plot_ana_lot_2_lot(qc_lot_2_lot_struct_old);

% new way: First calculations for qc_lot_2_lot with stats summary.
%          Second plotting: one window per plot (need to add summary table to the plot. (lower part of the window.
% [ qc_lot_2_lot_struct,error_message ]              = aggreate_column_for_table( assay_sum_table,'spotl',assay_ana_header );
% [ error_message ]                                   = sa_do_plot_ana_lot_2_lot(qc_lot_2_lot_struct);

% produce plots:
% week_1:  16 plots: one per analyte
%          2 series one for odd and one for even channel.
%          x_axei: lots
%          y_axis: mean shift 

end % fn gen_qc_rpt_data


% S3. PROBE PART:       ______________________________________________________________________

% S4. SENSOR PART:      ______________________________________________________________________

% S2.2 store experiment sensors:
%max_scan_len_all_exp                        = 0;  % just to tell how long are the scans for all the experiments.
% %     start_ndx                                   = cur_sen_ndx_use+1;

% %     cur_sen_ndx_use = 0;
% %     for cur_sensor_ndx = 1:1:cur_sensor_cnt % ___________________________________________________________ sensor_loop  jas_test: start with a real sensor: 7
% %         % STORE EACH SENSOR: If it is not a leak detector     e124 = dserror(124)  isempty(e124{1})
% %         % sensor_type_key:  _bad_  1 == leak detector, 2 == Temp Detect       3 == Analyte,      4== control)
% %         % sensor_type_key:  _okk_  1 == Analyte,       2 == leak detector,    3 == Temp Detect   4== control
% %         cur_sensor_struct             = exp_sensor_struct(       cur_sensor_ndx);
% %         if ( (sensor_table.sensor_type(cur_sensor_ndx) == 2)  || (sensor_table.sensor_type(cur_sensor_ndx) == 3))  %  %jas_temp_tr_norm: tbd:
% %             % LD OR TR: jas_temp: for the time being: do not process _tr_jas_tbd_tr_
% %             % scond_cange to align the data: DO NOT       continue;  % keep 136 and not 128
% %             continue;
% %         end
% %         cur_sen_ndx_use               = cur_sen_ndx_use + 1;
% %         probe(cur_sen_ndx_use)        = sensor_table.sensor_name(cur_sensor_ndx);
% %         clust(cur_sen_ndx_use)        = sensor_table.cluster(    cur_sensor_ndx);
% %     end % for each sensor ________________________________________________________________________________________________
% %


