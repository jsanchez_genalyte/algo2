function [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla_regex_old( a_request_vanila_str )
%SA_DO_PARSE_STR_REQUEST_VANILLA handles parsing a vanilla request using regular expresions (vs casi)
%   a vanilla request is either:                           NIU (replaced by a simpler version: sa_do_parse_str_request_vanilla_regex)
%   1 - a single string (with no wilde chars)
%   2 - a single string (with wilde chars)
%   3 - a comma separated list of single strings and a companion array with 0 or 1s for wild chars. 
% returns a cell array of strings
% sample call:  [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla_regex( 'exp_1, exp_2, exp  3333')
wild_array_flag     = [];
comma_pattern       = ','; % comma is the sepparator in a list of experiments,lots etc.
%semic_pattern       = ';'; %
wildc_pattern       = '*'; %
%a_request_vanila_str= 'exp_1, exp_2, exp_3333';

%comma_regex_pattern = '\w*,+\w*'; % not too good
comma_regex_pattern ='[^,]* ' ;    % better: anything that has no commas
a_request_cell_str   = regexp(a_request_vanila_str,comma_regex_pattern, 'match');
token_cnt = size(a_request_cell_str,2);
final_len = token_cnt;
wild_array_flag    = ones(token_cnt,1);
if ~(isempty(a_request_cell_str))
    % FOUND ONE OR MORE TOKENS: get rid of potential blanks only tokens
    cur_ndx =1;
    while (cur_ndx <= final_len)
        if (( isempty(replace(a_request_cell_str{cur_ndx},'  ',''))) || strcmp(a_request_cell_str{cur_ndx},' '))
            % BLANKS ONLY TOKEN: Flag it to remove it:
            % EMPTY ONE AND NOT AT THE END: move up:
            final_len = final_len-1;
            for cur_move_ndx=cur_ndx:length(wild_array_flag)-1
                wild_array_flag( cur_move_ndx)     = wild_array_flag(cur_move_ndx+1);
                a_request_cell_str( cur_move_ndx)  = a_request_cell_str(cur_move_ndx+1);
            end
            continue;
        end
        % check for wild chars:
        str_wild_ndx  = strfind(a_request_cell_str{cur_ndx},wildc_pattern);
        if (isempty(str_wild_ndx))
            wild_array_flag(cur_ndx,1) = 0;
        end
        
        % trim trailer blanks and starter blanks: jas_tbd
        a_request_cell_str{cur_ndx} = deblank(a_request_cell_str{cur_ndx});
        k = find(a_request_cell_str{cur_ndx} ~= ' ',1);
        if (~(isempty(k)) && (k >1))
            a_request_cell_str{cur_ndx} = a_request_cell_str{cur_ndx}(k:end);
        end
        cur_ndx = cur_ndx+1;
    end
    if (final_len < token_cnt )
        % NEED TO TRIM: Cut the empty ones
        wild_array_flag      = wild_array_flag(1:final_len);
        a_request_cell_str  = a_request_cell_str(1:final_len);
        a_request_cell_str  = a_request_cell_str';
    end
end
end % fn sa_do_parse_str_request_vanilla