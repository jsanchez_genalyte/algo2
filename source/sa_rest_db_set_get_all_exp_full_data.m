function [exp_full_struct,error_message  ] = sa_rest_db_set_get_all_exp_full_data(...
    local_hd_struct,expe_ned_table,rest_token,handles_text_exp_status)
%SA_REST_DB_SET_GET_ALL_EXP_FULL_DATA fills in a exp_full_struct from real api db or local_hd data
%   returns: exp_full_struct  with the exp_full data loaded corresponding to the experiments listed in: expe_ned_table
% clone of :     sa_rest_db_set_get_all_data
% pipeline_new:  Sensogram_Analysis_App_10 --> pushbutton_top_sens_Callback  --> sa_do_push_top_sens --> sa_rest_db_set_get_all_exp_full_data --> sa_rest_exp_get_exp_full_from_hd
%                                                                                                                                             --> sa_rest_exp_get_exp_full_from_db
%                                                                                                                                             --> sa_rest_exp_get_sensors
%                                                                                                                                             --> save_exp_full_mat
max_sensor_cnt_esti         = 136;  % jas_tbd_config_parameter    _max_sensor_cnt_
% this is the upper bound of how long I can manage scans: More than that it will show as lines to zero: From the end back to zero....
max_scan_len_esti           = 140;  % % jas_tbd_config_parameter  _max_scan_len_   to pre_allocate a reasonable size)  % tipically 63 for cilica 102
%                                   % jas_tbd_make it a configurable parameter: if zero lines at end of sensogram: means this value is too short.
error_message = '';
% Pre-Allocate arrays to store: exp/protocol/probe/sensor/scan data.
exp_request_cnt                     = height(expe_ned_table);
%ok_data_array                       = zeros(exp_request_cnt,1); % assume bad until ok data is found
exp_full_struct                     = {};
% exp_full_struct.protocol_struct     = struct;
% exp_full_struct.exp_det_struct      = struct;
% exp_full_struct.exp_probe_struct    = struct;
% exp_full_struct.exp_sensor_struct   = struct;
% exp_full_struct.scan_data_struct    = struct;
cur_exp_ndx_use     =0;
data_bad_api_cnt    =0;
data_bad_read_cnt   =0;
data_bad_write_cnt  =0;
for cur_exp_ndx= 1:1:exp_request_cnt % ________________________________________________________________________________________________
    if (~(mod(exp_request_cnt,10)))
        if (data_bad_api_cnt)
            status_msg = sprintf('Retrieving: experiment  %5d  of  %5d ... Please wait ..... Experiments with Bad Data From Jervis: %-4d',cur_exp_ndx,exp_request_cnt,data_bad_api_cnt);
        else
            status_msg = sprintf('Retrieving: experiment  %5d  of  %5d ... Please wait .....',cur_exp_ndx,exp_request_cnt);
        end
        set(handles_text_exp_status,'string',status_msg,'ForegroundColor','blue','Fontsize',12); %'blue'
        drawnow;
    end
    
    %lprintf_log( '\n%s\n',status_msg);
    cur_exper = char(expe_ned_table.exper(cur_exp_ndx));
    %cur_carri = char(expe_ned_table.carri(cur_exp_ndx));
    %date_str  = char(expe_ned_table.dates(cur_exp_ndx));
    cur_expid = char(expe_ned_table.expid(cur_exp_ndx));
%     date_str  = cellstr(date_str);
%     date_str  = strrep(date_str,'(UTC)','');
%     date_str  = strrep(date_str,' ','');
    %cur_datef = datenum(date_str);
    %cur_dates = date_str;  % cellstr(int2str(datenum(date_str)));
    %cur_proid = char(expe_ned_table.proid(cur_exp_ndx));
%     lprintf_log( '\n%s \t\t%-s \t\t%-s \t\t%-s \t\t%-s \n'...
%         ,status_msg,char(cur_dates),cur_carri,cur_expid,cur_exper);
    
    % Check if the exp file exists for this cur_expid:   cur_expid_exp.mat
    % if the file exists: load it from the hd. (usel local_hd info for the directory) then continue
    % else: continue with the api and then save the cur_expid_exp.mat file for next round.
    
    % Attempt to get data from the HD:
    [ error_message,  protocol_struct,exp_det_struct,exp_probe_struct,exp_sensor_struct ] = sa_rest_exp_get_exp_full_from_hd(local_hd_struct,cur_expid);
    if (~(isempty(error_message)))
        
        if (~(strcmp(error_message,'file exp_full does not exist')))
            % ERRORS LOADING THE MATLAB BINARY exp_full : Skip this experiment.
            % jas_tbd: Mark it as bad.    (cur_exp_ndx).data_flag = 1;
            lprintf_log( '\n  not skipping experiment %-s due to loading from HD issue: %s. Trying to get it from the API for a second_time',error_message,cur_exper);
            % Give it a second chance: Do not continue.
            data_bad_read_cnt = data_bad_read_cnt+1;
            %continue;
        end
        
        % FILE DOES NOT EXIST OR COULD NOT LOAD IT: Get it from the API
        % The next line: calls the api to retrieve the assay data, and save it locally.
        [ error_message,protocol_struct,exp_det_struct,exp_probe_struct,exp_sensor_struct ] = sa_rest_exp_get_exp_full_from_db(rest_token,expe_ned_table,cur_exp_ndx); % ***  BOTTLENECK SOLUTION ****
        if (~(isempty(error_message)))
            % ERRORS FROM THE API: Skip this experiment.
            % jas_tbd: Mark it as bad.    (cur_exp_ndx).data_flag = 1;
            lprintf_log( '\n  skipping experiment %-s due to data issue: %s',error_message,cur_exper);
            % jas_tbd_add here: status_log            
            data_bad_api_cnt = data_bad_api_cnt+1;
            continue;
        end
    else
        % FILE DOES yes EXIST: Got it from the HD
    end
    % HAVE THE DATA FROM EITHER SOURCE: API or HD
    %ok_data_array(cur_exp_ndx) = 1; % flag it as good.     
    cur_exp_ndx_use                                     = cur_exp_ndx_use+1;
    exp_full_struct{cur_exp_ndx_use}.protocol_struct    = protocol_struct;
    exp_full_struct{cur_exp_ndx_use}.exp_det_struct     = exp_det_struct;
    exp_full_struct{cur_exp_ndx_use}.exp_probe_struct   = exp_probe_struct;
    exp_full_struct{cur_exp_ndx_use}.exp_sensor_struct  = exp_sensor_struct;
    exp_full_struct{cur_exp_ndx_use}.ok_data_array_ndx  = cur_exp_ndx; % flag it as good.  
    %fprintf('Done retrieving assay data:  for the experiment: details/protocol/probes/sensor/ scan (future) ');
    if (local_hd_struct.local_hd_db_exp_full_save_flag)
        % USER REQUESTED TO GENERATE EXP_FULL MAT File: Write exp,probe and sensor (future:and scan data) in a mat file: one experiemnt at a time. excel_exp_save_assay
        [ local_hd_struct,error_message ]    = save_exp_full_mat(local_hd_struct,cur_expid,exp_full_struct{cur_exp_ndx_use});
        if (error_message)                   % clone: excel_exp_save_scan: excel_exp_save_assay(local_hd_struct,expe_ned_table,sensor_excel_table,scan_data_struct);
            % jas_tbd: decide how to handle write errors: show in status line in red?
            data_bad_write_cnt = data_bad_write_cnt+1;   
            continue;
        end
    end

    % ALL DATA FOR EXPERIMENT IS GOOD: Move on.
    % S0. SET UP:
    
    
% % %     date_str  = cellstr(date_str);
% % %     date_str  = strrep(date_str,'(UTC)','');
% % %     date_str  = strrep(date_str,' ','');
% % %     %cur_datef = datenum(date_str);
% % %     cur_dates = date_str;  % cellstr(int2str(datenum(date_str)));
% % %     cur_proid= char(expe_ned_table.proid(cur_exp_ndx));
% % %     lprintf_log( '\n%s \t\t%-s \t\t%-s \t\t%-s \t\t%-s \n'...
% % %         ,status_msg,char(cur_dates),cur_carri,cur_expid,cur_exper);
    
    % S1. PROTOCOLS PART:   ______________________________________________________________________
    % S2. EXP_DETAILS PART: ______________________________________________________________________
    % S3. PROBE PART:       ______________________________________________________________________
    
    %Retrieve scan data for all analysis  sensors: i.e. exclude the leak detectors
    % S7. Get 1 experiment SCAN data: 	API_3: showSensor: 136 sensors. (eliminate non_analytes but keep trs)
    %exp_scan_sensors:           sensor_cnt x 3        col_1 number, col_2 is_valid, col_3_scan_cnt
    %scan_col: exp_scan_data:    sensor_cnt x 70 x 2   scan_plane_time = 1  scan_plane_gru = 2    row is sensor and col is scan sequence. assumption: scan_cnt < 70.
    
    % manana ... 
% % % %     % Get scan data from either source: The db (either cpp or Matlab using the API), the Hard Drive (Using previoulsly stored files if available)
% % % %     [ error_message,scan_data_struct] = sa_rest_exp_get_scan(local_hd_struct,cur_expid,cur_proid,exp_sensor_struct,rest_token,sensor_table); % SCAN_OPTICAL_EXCEL:
% % % %     if (~(isempty(error_message)))
% % % %         continue; % try with other sensors
% % % %     end
% % % %     
% % % %     if (size(scan_data_struct.scan_data_time,2) >  max_scan_len_esti)
% % % %         lprintf_log('\nERROR: Scan way too long: %4d   Current Matrices are dimensioned for max scan length of %4d. \nChange configuration parameter max_scan_length to at least %4d ',...
% % % %             size(scan_data_struct.scan_data_time,2),max_scan_len_esti,size(scan_data_struct.scan_data_time,2));
% % % %     end
% % % %     
% % % %     if (size(scan_data_struct.scan_data_time,1) >  max_sensor_cnt_esti)
% % % %         lprintf_log('\nERROR: Scan way too long: %4d   Current Matrices are dimensioned for max scan length of %4d. \nChange configuration parameter max_sensor_cnt to at least %4d ',...
% % % %             size(scan_data_struct.scan_data_time,1),max_sensor_cnt_esti,size(scan_data_struct.scan_data_time,1));
% % % %     end
% % % %     exp_full_struct{cur_exp_ndx_use}.scan_data     = scan_data_struct;
% % % %     
end % for each expid : % _________________________________________________________________________________________

status_msg = sprintf('Experiment Retrieval Summary: Retireved Experiments    %5d  ',exp_request_cnt);
if (data_bad_read_cnt)
    status_msg = sprintf('Experiment Retrieval Summary: Experiments with local READ Errors  %5d  of  %5d ',data_bad_read_cnt,exp_request_cnt);
end
if (data_bad_write_cnt)
    status_msg = sprintf('Experiment Retrieval Summary: Experiments with local WRITE Errors  %5d  of  %5d ',data_bad_write_cnt,exp_request_cnt);
end

if (data_bad_api_cnt)
    status_msg = sprintf('Experiment Retrieval Summary: Experiments with API Errors  %5d  of  %5d ',data_bad_api_cnt,exp_request_cnt);
end
if (data_bad_read_cnt + data_bad_write_cnt + data_bad_api_cnt)
    set(handles_text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'
    drawnow;
end
lprintf_log( '\n%s\n',status_msg);


% AT THIS POINT I SHOULD HAVE: exp,probe and sensor data, independiently of source: local_hd or cloud API  stored in exp_full_struct with one row per experiment.
end % fn sa_rest_db_set_get_all_exp_full_data
