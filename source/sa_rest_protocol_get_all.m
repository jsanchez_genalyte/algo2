function  [ protocol_list_cel,  error_message ]  = sa_rest_protocol_get_all( rest_token)
%SA_REST_PROTOCOL_GET_ALL retrieves the list of ALL available protocols from Jervis Data Service  API_2: listProtocols
%  returns:
%   1 protocol_list:    a struct with a list of protocols availabe for quering.
%                       protocol_list(1) gives a struct with 3 fileds:
%                       id,name and maintenance flag
%   2 error_message:    either an empty string error message if everything ok, or some kind of diagnostic.
%    sample call:       [ protocol_list_cel,  error_message ]  = sa_rest_protocol_get_all( handles.rest_token);
%   Assumption:         you called first: [ error_message, handles ] = sa_rest_do_connect( handles,false );
%   see:                sa_rest_protocol_get_details.m  to get details of individual protocol
% Login first:          [ error_message,handles ] = sa_rest_do_connect( handles,true ); 
error_message       = '';
protocol_list_cel   = nan;  
% url_login_str       = 'http://jervis-staging.genalyte.com/api/auth/token';
% token_str           = 'GL86C3700A12'; % token for Staging.
% token_struct        = struct('authorization_token',token_str);
% token_json          = jsonencode(token_struct);
% options             = weboptions('MediaType','application/json');
% response            = webwrite(url_login_str,token_json,options);

%if ( (isfield(response,'created_at')) && (isfield(response,'expires_in')) && (isfield(response,'token')) )
% ASSUMED OK RESPONSE FROM THE WEB SERVICE
[ url_struct ]      = sa_set_production_vs_stagging_url_flag(); % prod_stag_flag );
url_protocols_str   = url_struct.url_protocols_str;
% For Protocols: Add a request header parameter with the following key and values
options =  weboptions('HeaderFields',{'Content-Type' 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token )}); % jas_debug_timeout
options.Timeout         = 25; % jas_debug_timeout    
% creates a weboptions object that contains two header fields: Content-Length with value 78 and Content-Type with value application/json.
%options = weboptions('Authorization', strcat('Bearer', ' ', rest_token ));   %  ,'ContentType','text'
try
    error_message  	= '';
    protocol_list 	= nan;
    protocol_list   = webread(url_protocols_str,options);
catch
    % COULD NOT READ PROTOCOL LISTS
    error_message	= fprintf('\nERROR_ WHILE READING Protocol list from API: WEBREAD error. Function: sa_rest_protocol_get_all)');
    display(error_message);
    return;
end

if ( (isfield(protocol_list,'id')) && (isfield(protocol_list,'name')) && (isfield(protocol_list,'maintenance')) )
    % OK RESPONSE FROM THE WEB SERVICE: API protocol list
else
    error_message = fprintf('\nERROR_ RETRIEVING THE PROTOCOL LIST FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.');
    return;
end
protocol_list_cel   =  struct2cell(protocol_list);
protocol_list_cel   = protocol_list_cel';
end    % end fn sa_rest_protocol_get_all


% cd /c/Users/jsanchez/Documents/algo2/source
% cd C:/Users/jsanchez/Documents/16b/algo2r/source     dont use: cd C:\Users\jsanchez\Documents\16b\algo2r\source
% qc_protocol 22 : close to: {'85112PRT ANA Panel Protocol','c893151d-67a9-4112-a063-4bee164d40c2'}

%     % get list of experiments:
%     url_expe_str        = 'http://jervis-staging.genalyte.com/api/tests';   % try and failed: /api/tests/indexTests';
%     id                  = 'Jervis ANA Validation';
%     id                  = 'b5d306cb-70a4-462a-a651-d3878bddadc4';  %control: '28ec02fe-7bd4-4f54-be32-5f751c1287ef'; %   'Jervis ANA Validation' ; %  '3013345c-0f44-4469-82f2-ad4217fc1cc1';
%     req_start_date_str  = '2017-07-21 5:00';
%     req_end_date_str    = '2017-07-21 7:00';
%
%     id                  = strcat('''','Jervis ANA Validation',''''); %  '3013345c-0f44-4469-82f2-ad4217fc1cc1';
% 	id                  = strcat('''','b5d306cb-70a4-462a-a651-d3878bddadc4','''');
%     req_start_date_str  = strcat('''','2017-07-21 5:00','''');
%     req_end_date_str    = strcat('''','2017-07-21 7:00','''');
%
%     url_expe_str2       = strcat(url_expe_str,',',req_start_date_str,',',req_end_date_str,',',id);  % 'q',',',id , ,')'
%     url_expe_str3       = strcat(',''start_date'',',req_start_date_str,',''end_date'',',req_end_date_str,',''q'',',id);
%     options             = weboptions('HeaderFields',{'Content-Type' 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token )});
%     expe_list           = webread(url_expe_str,url_expe_str3,options);
%
%
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','start_date','2017-07-20 5:00','end_date','2017-07-22 7:00','q','b5d306cb-70a4-462a-a651-d3878bddadc4',options);
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','start_date','2017-07-20 5:00','end_date','2017-07-22 7:00','q','Jervis ANA Validation',options);
%
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','q','b5d306cb-70a4-462a-a651-d3878bddadc4',options);
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','q','Jervis ANA Validation',options);
%
%     if ( (isfield(expe_list,'id')) && (isfield(expe_list,'state'))  )
%         % OK RESPONSE FROM THE WEB SERVICE: API test list
%     else
%         error_message = strcat(' ERROR RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: %s'...
%             ,char(expe_list));
%         return;
%     end
%
%
% else
%     error_message = strcat(' ERROR LOGGING INTO THE THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: %s'...
%         ,char(response));
%     return;
% end;


% expe_list_cel       =  struct2cell(expe_list);
% expe_list_cel       = expe_list_cel';





% protocol_id_list   = protocol_list_cel(1,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_1 is id
% protocol_name_list = protocol_list_cel(2,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_2 is name
% protocol_main_list = protocol_list_cel(2,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_3 is maintenenance flag



% sample call: [ protocol_list_cel, error_message ]  = sa_rest_protocol_get_all( )

% protocol_list_cel =   30�3 cell array
%     '86ba7f1a-e393-4e55-bcd0-1a558a30c869'    'Maverick Maintenance Kit'                 [1]
%     '6bd9420c-3cab-4c1d-b298-1005cd0fb858'    '00083 ANA Training'                       [0]
%     'eb0986b4-07ce-4282-96b4-2489a7905838'    'Celiac 2-Step 5_2'                        [0]
%     '666ad442-bb35-4a37-bb5a-c5b6d5c00220'    'LF Panel 2-Step Dilution B'               [0]
%     '00cc717a-ae5f-4f5a-83df-9bb92b2faf65'    'Celiac 2-Step 2'                          [0]
%     '52df9a69-e774-49b7-a27b-fe0d60cddc22'    'Celiac 2-Step 6'                          [0]
%     '40d23e81-a796-4f10-9c89-f3a6d02c04ad'    'Salt 1 Step - 5 Point Curve Protoco�'    [0]
%     '5ea2163c-9e06-422a-bca8-634e0e6d8742'    'Celiac Looks Like Lupus Imported'         [0]
%     'd8f2959e-4c71-4bfa-b200-1ad70932bb37'    'LF Panel SampleA'                         [0]
%     '697701c0-1156-4144-9bf5-d38d095fbdf0'    'The Protocol formerly known as LF P�'    [0]
%     '6bb73d13-d208-48db-be34-65c9eabbc620'    'LF Panel 85113PRT'                        [0]
%     '03da0d05-3f5a-4925-8fc3-88787556cf85'    'Cylance_Test'                             [1]
%     '00c80636-799c-41d4-87ca-70119a7ab639'    'Celiac Test KR'                           [0]
%     '2319cddf-261f-4f62-aa6e-581052fbf0d7'    'Test test'                                [0]
%     '54e183a3-9062-4fe3-b0c8-d48fab599fb7'    'Celiac 2 Test KR'                         [0]
%     'f3a26634-6fb0-41b8-8d53-b6d42bec3b5f'    'Celiac Test KR 2'                         [0]
%     'd34c8f81-f1dd-48c7-ba29-554e41a53cdf'    'RA Panel 14-2 detects'                    [0]
%     '5f641b15-ae68-48b8-82b4-c761c7283cbe'    'RA Panel 14-2 detects.1'                  [0]
%     'c4fe0628-7870-4715-ba37-4b2583195bae'    'Celiac 2-Step 3-AB'                       [0]
%     '446933a6-d80a-40ef-afaa-a700ff4d4687'    'Salt Steps'                               [0]
%     'b0594274-24bd-486c-bca0-01abdc9ffba9'    'Maverick Maintenance Kit_TEST4'           [1]
%     'c893151d-67a9-4112-a063-4bee164d40c2'    '85112PRT ANA Panel Protocol'              [0]
%     '4a7ee136-fdab-4463-ab87-6be8a6ba9152'    '00083 ANA Training - John Test'           [0]
%     '69e6f69f-2c15-4c76-8105-42a09695716f'    'Celiac Panel protocol'                    [0]
%     'cdb0ce4f-d4b7-4d9f-8ef9-b7000bfc4381'    'Celiac 2-Step 3-AB1'                      [0]
%     'e4dd14f3-f81b-4511-b886-fb38fd2c44c7'    'Celiac 2-Step-170397-3 Racks-2'           [0]
%     '248253ba-14f4-42a2-8bb9-3e776b53f4bb'    'RA Panel 14_long RB1'                     [0]
%     'ce3b0bcf-8fc1-4ffe-9cee-8fc843677dda'    'Celiac Test KR3'                          [0]
%     'b4925e84-9a63-4f6a-b26e-c0500222ee9a'    'Debug Test'                               [0]
%     '852244d0-1a0b-4c12-983b-4ef1d40fd816'    '00185PRT Celiac Panel protocol'           [0]
