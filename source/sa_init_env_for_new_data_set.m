function   [handles ]  ...
    = sa_init_env_for_new_data_set( handles,data_set_struct_list )   % data_set_struct_list: should be now global.
% SA_INIT_ENV_FOR_NEW_DATA_SET initalizes all the data and gui elements 
% this is to be called: once a new data_set is loaded in memory: either from the db or from a local file.
% availabe analytes: come from the returned dataset. 

% see file std_curve_opening  
% ASSUMPTION: If there is a current session going on, the user got prompted regarding saving or not the current data_set. 
% This function wipes out the current data_set.
% See files:  sa_init_env_app(GUI elments initilization) and sa_opening and init_env_for_new_data_set  

% called from many places: do_load_data_data_set_local_file, do_load_data_data_set_db ... 


sa_declare_globals;


env_struct_list                     = handles.env_struct_list;
input_parameters_struct             = handles.input_parameters_struct;
fit_type_valid                      = input_parameters_struct.fit_type_valid;

% Make visible: plots: good,anom,paretos, plot_buttons, tos
% classify the first x sensograms.
% populate plots, paretos and tos



end % fn  sa_init_env_for_new_data_set