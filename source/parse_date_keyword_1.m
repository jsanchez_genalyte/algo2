function [ start_to_day_date_str, end_to_day_date_str,start_to_sec_date_str, end_to_sec_date_str ] = parse_date_keyword_1(wanted_keyword)
%PARSE_DATE_KEYWORD_1 converts a wanted_keyword into concrete start and end dates:
%   Return start and end datetimes in 2 formats: to day and to second resoultions. 
% sample call:
% [ start_to_day_date_str, end_to_day_date_str,start_to_sec_date_str, end_to_sec_date_str] = parse_date_keyword_1('TODAY');

end_to_day_date_str              = '';
start_to_day_date_str            = '';
% sample:   02/23/2018  23:59:11 AM
%           123456789 123456789 012
beg_day_str =         ' 00:00:00 AM';
end_day_str =         ' 11:59:59 PM';

date_str_to_day_format               = 'mm/dd/yyyy';
date_str_to_sec_format               = 'mm/dd/yyyy HH:MM:SS PM';
if (strcmp(wanted_keyword,'TODAY'))
    start_to_day_date_str       = datestr(now,date_str_to_day_format);
    start_to_sec_date_str       = strcat(start_to_day_date_str,beg_day_str);
    
    end_to_day_date_str         = start_to_day_date_str;
    end_to_sec_date_str         = datestr(now,date_str_to_sec_format);
end

if (strcmp(wanted_keyword,'YESTERDAY'))
    start_to_day_date_str       = datestr((today - days(1)),date_str_to_day_format);
    start_to_sec_date_str       = strcat(start_to_day_date_str,beg_day_str);
    
    end_to_day_date_str         = start_to_day_date_str;
    end_to_sec_date_str         = strcat(end_to_day_date_str,end_day_str);
end

end % fn date_keyword_1

