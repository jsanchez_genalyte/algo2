function escaped_string = utils_xml_escape_string(in_string)
% function escaped_string = utils_xml_escape_string(in_string)
%   escapee_chars all special characters (ndx_e.g. \r\n, \t, etc.) in the string in_string
%   and returns a string containing the required escape sequences to
%   reproduce the string ndx_e.g. with sprintf(escaped_string).
%
%   Helpful for debug purposes. Note that this function is not optimized
%   for speed (yet).
%
%   parameters:
%       in_string        The string that should be escaped_chars
%       escaped_string   String with all special characters replaced with their escape sequences
% source: https://www.mathworks.com/matlabcentral/fileexchange/41512-escapestring--convert-special-characters-in-a-string-into-their-escape-sequences?focused=3785497&tab=function
% sample call: escaped_string = utils_xml_escape_string('aa bb cc dd')   escaped_string = utils_xml_escape_string('aa 'bb cc dd')
len_s = length(in_string);
escaped_string = '';

if (nargin < 1)
    return;
end

escapee_chars = {'\r','\n','\t','\v','\\','''','\a','\b','%%','\0'};
escaped_chars = {'\r','\n','\t','\v','\\','''','\a','\b','%%','\0'};

len_e = length(escapee_chars);

for (ndx_s=1:len_s)
    for (ndx_e=1:len_e)
        found = false;
        if ( strcmp(in_string(ndx_s), sprintf(escapee_chars{ndx_e})) )
            escaped_string = [escaped_string, escaped_chars{ndx_e}];
            found = true;
            break;
        end
    end
    if (~found)
        escaped_string = [escaped_string, in_string(ndx_s)];
    end
end
end % fn utils_xml_escape_string