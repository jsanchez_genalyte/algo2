v = [2 4 6];
P = perms(v);

P = perms(v;) % returns a matrix containing all permutations 
% of the elements of vector v in reverse lexicographic order. Each row of P contains a different permutation of the n elements in v. Matrix P has the same data type as v, and it has n! rows and n columns.
% 

% Rearrange dimensions of N-D array  
% Syntax
B = permute(A,order);

 
b = nchoosek(n,k);  % returns the binomial coefficient, 
%                   % defined as n!/((n�k)! k!). 
%                   % This is the number of combinations of n items taken k at a time.

 
C = nchoosek(v,k);  % returns a matrix containing all possible combinations of the 
%                   % elements of vector v taken k at a time. 
%                   % Matrix C has k columns and n!/((n�k)! k!) rows, where n is length(v).
 