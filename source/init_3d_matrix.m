function [ ...
    time_raw_matrix         ,time_normalized_matrix     ...
    ,shift_raw_matrix       ,shift_normalized_matrix    ...
    ,tr_matrix                                          ...
    ,feature_raw_matrix     ,feature_normalized_matrix  ...
    ,flag_raw_matrix        ,flag_normalized_matrix     ...
    ,envelope_raw_matrix    ,envelope_normalized_matrix ...
    ] = init_3d_matrix( curve_size_len_max,curve_size_cnt_max,file_loaded_count )
%INIT_3D_MATRIX  Initializes to nan each of the 3D matrices for this app with the appropiate dimentions 
%   Normalized and raw matrices have the same dimentions. tr_matrix is the exception.

declare_globals;

% Allocate memory for: raw_shift,time,envelope,flags and features matrices
time_raw_matrix           = nan(curve_size_len_max,     curve_size_cnt_max,file_loaded_count);
shift_raw_matrix          = nan(curve_size_len_max,     curve_size_cnt_max,file_loaded_count);
feature_raw_matrix        = nan(global_feature_row_max, curve_size_cnt_max,file_loaded_count);

tr_matrix                 = nan(curve_size_len_max,     tr_last_col,       file_loaded_count);
%flag_matrix              = nan(global_flag_row_max,    curve_size_cnt_max,file_loaded_count);
flag_raw_matrix           = nan(global_flag_row_max,    curve_size_cnt_max,file_loaded_count);
envelope_raw_matrix       = nan(curve_size_len_max,     curve_size_cnt_max,global_envelope_col_max);

% Normalized matrices have the same dimentions as the raw matrices. The only exception is the tr_matrix.

time_normalized_matrix    = time_raw_matrix;
shift_normalized_matrix   = shift_raw_matrix;
feature_normalized_matrix = feature_raw_matrix;
envelope_normalized_matrix= envelope_raw_matrix;
flag_normalized_matrix    = flag_raw_matrix;


return % fn init_3d_matrix



