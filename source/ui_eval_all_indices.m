function [ ui_state_struct ] = ui_eval_all_indices( db_cur_fit_net_ndx,clasi, ui_state_struct,handle_text_status  )
%UI_EVAL_ALL_INDICES Evaluates current indices to be used to display the tos and/or plots
%   Produces 3 indices: 
%     ui_cur_fit_net_ndx (union or single), 
%     ui_cur_fit_net_ndx_good (good only),
%     ui_cur_fit_net_ndx_anom (anom only)
%   Uses the ui_request_flag to decide how to build the ui indices:
% input:
% 1_ db_cur_fit_net_ndx: index of whatever data is selected (net of all filtering)
% 2_ clasi:              classification results: U==Unknown,G=good,A=Anomalous,etc.       	% db_metad_set_struct.clasi
% 3 _ ui_state_struct:   senso_start_ndx and senso_curves_perp_cnt: start & curves_perp_cnt for good and bad sensograms:
% 4_ ui_request_flag
%           'g_only' to produce indices of only good sensograms.
%           'a_only' to produce indices of only anom sensograms.
%           'g_n_a'  to produce indices of good and  anom sensograms.
%           'u_only' to produce indices of unknown sensograms: ie not_analyzed.
% output: ui_state_struct.
%               ui_cur_fit_net_ndx      ndx to be used to plot/and tos. logical of size 1536 +,ui_cur_fit_net_ndx_good +,ui_cur_fit_net_ndx_anom
%               senso_start_ndx_next  	ndx of the next start (for the next call.).
% sample call:  [ ui_state_struct_temp ] = ui_eval_all_indices( ...
%               handles.ui_state_struct.db_cur_fit_net_ndx, handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct ,'u_only'  )

% ASSUMPTION:  sc_clasify_sensograms was called BEFORE evaluating all indexes, becuase it uses the latest classification results.
%              future: make a classy and eval_indices wrapper to pack the 2 of them in one and guarantee the proper equence.

% NEW METHOD: sc_classify_sensograms stores the 3 arrays now. so the only thing this function does is calculate the next. and return

ui_request_flag                         = ui_state_struct.ui_request_flag;
% setup for the 3 cases:
senso_start_ndx                         = ui_state_struct.senso_start_ndx;        	% this ndx is relative to whatever is currently selected: db_ cur_fit_net_ndx
senso_curves_perp_cnt                  	= ui_state_struct.senso_curves_perp_cnt;  	%100; % sa_cfg_
included_indices                        = find(db_cur_fit_net_ndx);
included_len                            = length(included_indices);
% Define indices:  senso_end_ndx   and   senso_start_ndx_next
if ((senso_start_ndx + senso_curves_perp_cnt - 1)   <= included_len)
    % WITHIN INCLUDED LEN: keep going
    senso_end_ndx               = senso_start_ndx + senso_curves_perp_cnt - 1;
else
    % PASS INCLUDED LEN: Use only as many as it can. (User will get less than requested)
    senso_end_ndx           	= included_len;
end

if ((senso_start_ndx + senso_curves_perp_cnt)   <= included_len)
    % WITHIN INCLUDED LEN: keep going
    senso_start_ndx_next	= senso_start_ndx + senso_curves_perp_cnt;
else
    % PASS INCLUDED LEN: Use only as many as it can. (User will get less than requested)
    senso_start_ndx_next                    = nan;     % nan is the flag to roll back
    warning_msg = 'Reached the end. Rolling back to plot 1 ...';  %-4d sensogram(s) of the %4d  requested  '...
    set(handle_text_status,'string',warning_msg,'ForegroundColor','blue','Fontsize',12);
end

if (strcmpi(ui_request_flag,'g_only'))
    % GOOD
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx_good))
        % nothing to show
        ui_state_struct.senso_start_ndx_next_good    = nan;
    else
        ui_state_struct.senso_start_ndx_next_good  = length(ui_state_struct.ui_cur_fit_net_ndx_good)+1; % should not be nan
    end
end

if (strcmpi(ui_request_flag,'a_only'))
    % ANOM
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx_anom))
        % nothing to show
        ui_state_struct.senso_start_ndx_next_anom 	= nan;
    else
        ui_state_struct.senso_start_ndx_next_anom	= length(ui_state_struct.ui_cur_fit_net_ndx_anom)+1; % should not be nan
    end
end

if (strcmpi(ui_request_flag,'g_n_a'))
    % GOOD AND ANOM
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx))
        % nothing to show
        set(handle_text_status,'string','Empty Range in ui_eval_all_inidices' ,'ForegroundColor','red','Fontsize',12);
        ui_state_struct.senso_start_ndx_next        = nan;
    else
        ui_state_struct.senso_start_ndx_next      = senso_start_ndx_next; % should not be nan
    end
    
end
end % fn ui_eval_all_indices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
% ui_request_flag                         = ui_state_struct.ui_request_flag;
% ui_state_struct.ui_cur_fit_net_ndx      = [];
% ui_state_struct.ui_cur_fit_net_ndx_good = []; % to plot them simultaneously or separatedly
% ui_state_struct.ui_cur_fit_net_ndx_anom = []; % to plot them simultaneously or separatedly
% ui_cur_fit_net_ndx                      = [];
% ui_cur_fit_net_ndx_good                 = []; % to plot them simultaneously or separatedly
% ui_cur_fit_net_ndx_anom                 = []; % to plot them simultaneously or separatedly
% senso_start_ndx_next	             	= nan;
% senso_start_ndx_next_good               = nan;
% senso_start_ndx_next_anom               = nan;
%
% db_cur_fit_net_len                      = sum(db_cur_fit_net_ndx);
%
%
% % REAL PROCESSING: 3 Cases 'g_only', 'a_only' and 'g_n_a'  (future combined)
%
% if (senso_end_ndx < senso_start_ndx)
%     % EMPTY RANGE
%     ui_state_struct.ui_cur_fit_net_ndx      = []; % save the result to return.
%     ui_state_struct.senso_start_ndx_next    = nan;
%     set(handle_text_status,'string','Empty Range in ui_eval_all_inidices' ,'ForegroundColor','red','Fontsize',12);
%     %pause(1);
% else
%     % DATA TO WORK WITH:
%     %S1 _____________________________________________________________________________________________________________
%     if (  ( (strcmpi(ui_request_flag,'g_only')) || (strcmpi(ui_request_flag,'g_n_a')) ) && (senso_end_ndx >= senso_start_ndx) )
%         % REQUESTED GOOD_ONLY or both  AND SENSO RANGE IS NOT EMPTY
%         % determine ndx of the good ones:
%         ui_cur_fit_net_ndx_good_all     = find(db_cur_fit_net_ndx   & clasi =='G');
%         if (isempty(ui_cur_fit_net_ndx_good_all))
%             % FOUND no GOOD FOR THE SELECTED RANGE
%             ui_cur_fit_net_ndx_good	    = [];
%             senso_start_ndx_next_good   = 0;
%         else
%             % FOUND some GOOD FOR THE SELECTED RANGE:
%             % Check if the found ones are in the region that we are looking for. Between:  senso_start_ndx  and senso_end_ndx
%             found_good_ndx = ui_cur_fit_net_ndx_good_all >= senso_start_ndx & ui_cur_fit_net_ndx_good_all <= senso_end_ndx;
%             if isempty(found_good_ndx)
%                 % FOUND NO GOOD:
%                 ui_cur_fit_net_ndx_good =[];
%                 warning_msg ='Found no good';
%                 display(warning_msg);
%                 set(handle_text_status,'string',warning_msg,'ForegroundColor','blue','Fontsize',12);
%                 senso_start_ndx_next_good = senso_start_ndx_next;
%             else
%                 % FOUND SOME GOOD:
%                 ui_cur_fit_net_ndx_good	=  ui_cur_fit_net_ndx_good_all(found_good_ndx);
%                 senso_start_ndx_next_good = senso_start_ndx_next;
%             end
%             % evaluate senso_start_ndx_next_good:
%             if (senso_start_ndx_next_good == 0)
%                 senso_start_ndx_next_good = nan;
%             else
%                 if (senso_end_ndx < included_len)
%                     % STILL WITHIN: Move on to the next
%                     senso_start_ndx_next_good =senso_end_ndx +1; % nan is the flag to reset to 1
%                 else
%                     % ATTEMPING TO PASS WHAT IS AVAILABLE
%                     senso_start_ndx_next_good =nan; % nan is the flag to reset to 1
%                 end
%             end
%         end % no empty data
%     end % 'g_only' or 'g_n_a'
%     %S2 _____________________________________________________________________________________________________________
%     if (  ( (strcmpi(ui_request_flag,'a_only')) || (strcmpi(ui_request_flag,'g_n_a')) ) && (senso_end_ndx >= senso_start_ndx) )
%         % REQUESTED ANOM_ONLY or both  AND SENSO RANGE IS NOT EMPTY
%         % determine ndx of the anom ones:
%         ui_cur_fit_net_ndx_anom_all     = find(db_cur_fit_net_ndx   & clasi =='A');
%         if (isempty(ui_cur_fit_net_ndx_anom_all))
%             % FOUND no ANOM FOR THE SELECTED RANGE
%             senso_start_ndx_next_anom   = 0;
%         else
%             % FOUND some ANOM FOR THE SELECTED RANGE
%             if (length(ui_cur_fit_net_ndx_anom_all) >= senso_end_ndx)
%                 % FOUND TOO MANY: Trim to only what is needed
%                 ui_cur_fit_net_ndx_anom	= ui_cur_fit_net_ndx_anom_all(senso_start_ndx:senso_end_ndx);
%             else
%                 % FOUND LESS THAN WHAT IS WANTED: take them all
%                 if (length(ui_cur_fit_net_ndx_anom_all) >= senso_start_ndx)
%                     ui_cur_fit_net_ndx_anom	= ui_cur_fit_net_ndx_anom_all(senso_start_ndx:end);
%                 else
%                     % ERROR CONDITION BECAUSE: ui_cur_fit_net_ndx_anom_all is shorter than the start: senso_start_ndx
%                     error_msg ='Errror condition anom: shorter all than the start_ndx';
%                     display(error_msg);
%                     set(handle_text_status,'string',error_msg,'ForegroundColor','red','Fontsize',12);
%                     senso_start_ndx_next_anom = 0;
%                     ui_cur_fit_net_ndx_anom	= ui_cur_fit_net_ndx_anom_all;
%                 end
%             end
%             % evaluate senso_start_ndx_next_anom:
%             if (senso_start_ndx_next_anom == 0)
%                 senso_start_ndx_next_anom = nan;
%             else
%                 if (senso_end_ndx < included_len)
%                     % STILL WITHIN: Move on to the next
%                     senso_start_ndx_next_anom =senso_end_ndx +1; % nan is the flag to reset to 1
%                 else
%                     % ATTEMPING TO PASS WHAT IS AVAILABLE
%                     senso_start_ndx_next_anom =nan; % nan is the flag to reset to 1
%                 end
%             end
%         end
%     end % 'a_only' or 'g_n_a'
%
%     %S3 _____________________________________________________________________________________________________________
%     if (  (strcmpi(ui_request_flag,'g_n_a')) && (senso_end_ndx >= senso_start_ndx) ) % and not combined
%         % REQUESTED GOOD AND ANOM   AND SENSO RANGE IS NOT EMPTY
%         % Already have the determine ndx of the anom ones:   HAS_HERE_THURS_DEBUG_DIFFERENCE
%         % grab: all the good and anom that have an ndx less than this: In other words trim the 2 arrays (the union should match)
%         ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good(ui_cur_fit_net_ndx_good <= included_indices(senso_end_ndx));
%         ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom(ui_cur_fit_net_ndx_anom <= included_indices(senso_end_ndx));
%         % SANITY CHECK:
%         if ( (  length(ui_cur_fit_net_ndx_good) + length(ui_cur_fit_net_ndx_anom )) ~= senso_curves_perp_cnt)
%             err_msg = sprintf('DISPLAYNG LESS THAN REQUESTED: GOOD =%-4d  ANOM =%-4d  REQUESTED =%-4d '...
%                 , length(ui_cur_fit_net_ndx_good),length(ui_cur_fit_net_ndx_anom), senso_end_ndx);
%             display(err_msg);
%             set(handle_text_status,'string',err_msg,'ForegroundColor','red','Fontsize',12);
%             senso_start_ndx_next = nan;
%             %pause(1);
%         end
%     end
%     %S4 _____________________________________________________________________________________________________________
%     % Save good and anom
%     ui_state_struct.ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good; % to plot them simultaneously or separatedly
%     ui_state_struct.ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom; % to plot them simultaneously or separatedly
%     % Evaluate what you've got:
%     if  (   strcmpi(ui_request_flag,'g_only'))
%         % REQUESTED GOOD_ONLY
%         ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx_good;
%         senso_start_ndx_next    = senso_start_ndx_next_good;
%     elseif (strcmpi(ui_request_flag,'a_only'))
%         % REQUESTED ANOM_ONLY
%         ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx_anom;
%         senso_start_ndx_next    = senso_start_ndx_next_anom;
%     elseif (strcmpi(ui_request_flag,'g_n_a'))
%         %     REQUESTED GOOD_AND ANOM
%         ui_cur_fit_net_ndx = union(ui_cur_fit_net_ndx_good, ui_cur_fit_net_ndx_anom);  % **** UNION ****  (for combined and tos)
%         if (senso_end_ndx <  db_cur_fit_net_len )
%             senso_start_ndx_next    = senso_end_ndx+1;
%         else
%             senso_start_ndx_next    = nan;
%         end
%     end
% %     % Final check to avoid getting stock in the last plot.
% %     if ((senso_start_ndx == db_cur_fit_net_len) && (senso_start_ndx == senso_end_ndx) )
% %         % GOT TO THE END: Roll back
% %         senso_start_ndx_next =  nan;     % nan is the flag to reset to 1
% %     end
%
%     ui_state_struct.ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx; % save the result to return.
%     ui_state_struct.senso_start_ndx_next    = senso_start_ndx_next;
% end % non-empty sel range
% end % fn ui_eval_all_indices