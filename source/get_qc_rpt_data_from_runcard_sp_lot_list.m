function [ qc_rpt_table,error_message,error_flag ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params )
%GET_QC_RPT_DATA_FROM_RUNCARD_LOT_LIST  given a SPOTTING LOT LIST retrieves a table to be used by the qc_report (Automated version)
%     without knowning the QC WORK ORDER.
%     3 step

% NOTE:  clone of get_qc_rpt_data_from_runcard
%        key difference: this one uses a list of sp lots. the other one uses a work order number

%Output:
%Output:
% 1 - qc_rpt_table:  A table with the following columns:  (To later join with Jervis data using  mb_id
%    spot_ln    mb_wo       mb_ln       mb_id       mb_pn           qc_rack_number  qc_rack_index   chip_id             wafer_ln  	wafer_n     die_index         die_index
%   'AB1602'    'EWO-0137'  'AB180C'    'B160002'   'QCP0023-QC'    1               1               'P162143.08.0002'   "P162143"   8           2

% partial table: response_qc_table:
% serial        partnum         partrev lotnum      bom_partnum     bom_partrev     bom_serial          bom_lotnum  bom_qty workorder   seqnum  opcode  carrier_id  carrier_row carrier_col trans_date};
% 'B160002'     'QCP0023-QC',   '01',   'AB180C',   '00445',        '01',           'P162143.08.0002',  'AB1602',   1,      'EWO-0137', 20,     'A320', '',         NaN,        NaN,        '2018-05-03 17:32:39'}
% mb_id         mb_pn                   mb_ln       sp_chip                         chip_id             spot_ln         	mb_wo


%    _____________  _________  ______________  ______________  ____________  ______________  ______________
% 2 - error_message:   if empty: no errors.
% 3 - error_flag:      if 0 no errors. If 1 some errors, ie incomplete data may had been returned.

% Assumptions:
% 1 - The spot failures are logged in RunCard using the SP_FAIL APP. (Ana / Joshua)
% 2 - Sampling plan is performed in a single Work Order at a time.
% 3 - Multiple sampling plans can be performed in a single work order.
% 4 - When there are multiple sampling plans, they must be applied sequentially. SP1, then SP2 etc.
% 5 - The chips from the sampling plan get pick sequentially from the gel pack
% 6 - All the chips from the Sampling Plan go from the gel packs to another gel pack(s):
%     1_ QC gel_pack sp_1
%     2_LSC gel_pack sp_2
% 7 - Two work orders are  generated: QCP0023-QC (positive/negatives) and QCP0023-LSC (parametric: Known patient samples)
%     (The LSC is to be ignored by this function: It is not part of the QC report.Nick confirmed!)
%     sample WO number: WO EWO-0137
% 8 - Only QCP0023-QC  (SP1 I.E. Bin 2) get reported.
%
% 9 - The Virtual QC racks are filled sequentially from rack 1 to the last and each rack has up to 20 chips/ Matchboxes.
% 10- The count of chips of sampling plan 1 (bin 2) must match with the count of the chips in QC gel_packs.
%     This is the only verification (sanity check) that can/MUST be done to detect handling mistakes.
%
% sample call: see: do_get_qc_rpt_data_from_runcard_lot_number.m
% [ qc_rpt_table,error_message,error_flag ] = get_qc_rpt_data_from_runcard_lot_number( input_params )

%error_message      = '';
error_flag          = 0;
qc_rpt_table        = table();
response_qc_table   = table();
webservice_url      = input_params.webservice_url; % 'http://10.0.2.226/runcard/soap?wsdl';          % for real
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;

% Get inventory params:

sp_chip_part_number = input_params.sp_chip_part_number ; % '00445';
qc_sp_lot_list      = input_params.qc_sp_lot_list       ;
qc_part_number      = input_params.qc_part_number       ; % 'QCP0023-QC'; part number of the MB selected for QC
display_flag        = input_params.display_flag         ; % 1;        % 1 == display as process goes on. 0 == be quiet

% new method using genealogy from Ryan: Get all parent records of 0045 (ie chip spotted partnumber)
%methods(obj)
partrev             = '';
lotnum              = '';
serial              = '';
workorder           = '';
opcode              = '';
seqnum              = '';
whereUsed           = 1;    % parent records of the spotted chip: ie: all mb
[mb_all_list_response, error, error_message] = fetchUnitGenealogy(obj,sp_chip_part_number, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);
if error ~= 0
    error_message =  strcat( error_message, '\nERROR_fn_get_qc_rpt_data_from_runcard_sp_lot_list fetchUnitGenealogy');
    return
end;

mb_all_list_table       = struct2table(mb_all_list_response);

mb_partnum_cat          = categorical(mb_all_list_table.partnum);

% Keep only the QC spot lots: 'QCP0023-QC'
a_chips_ndx_list        = mb_partnum_cat == qc_part_number; % 'QCP0023-QC'; % '00446'; % ie spoted chips part of the mb.
%sum(a_chips_ndx_list)  % records 200   989
mb_sp_list_table        = mb_all_list_table(a_chips_ndx_list,:);

%
% Create table_ndx column: Original order ndx as retrieved from runcard: May not be correct jas_temp
% 
mb_sp_list_table = table_add_table_ndx(mb_sp_list_table);

spotl_cat               = categorical(mb_sp_list_table.bom_lotnum);
spotl_cnt               = length(qc_sp_lot_list);

for cur_spotl_ndx = 1:1:spotl_cnt
    % process current spotting lot
    a_chips_sl_lot_ndx_list = spotl_cat == (char(qc_sp_lot_list{cur_spotl_ndx})); % 'AB1602';  % % chips belong to sp lot number
    %sl_chip_cnt             = sum(a_chips_sl_lot_ndx_list); %  100 records %579
    qc_rpt_table_temp       = mb_sp_list_table(a_chips_sl_lot_ndx_list,:);
    response_qc_table       = [ response_qc_table ;qc_rpt_table_temp ];
end
%


disp('done');  % jas_here thursday eve.  tbd: fill in the
serial_cnt      = height(response_qc_table);
% Generate empty table with as many rows as carriers found in the WO.
qc_rpt_headers	= {'splot_ln','mb_wo','mb_ln','mb_id','mb_pn','qc_rack_number','qc_rack_index','chip_id','wafer_ln','wafer_number','die_index','table_ndx'};
data          	= cell(serial_cnt,size(qc_rpt_headers,2));
qc_rpt_table  	= cell2table(data);
qc_rpt_table.Properties.VariableNames = qc_rpt_headers;

% Fill in the mb serial:
% not too quick:  qc_rpt_table.mb_id = response_table.serial;
delimiter                   = {'.'}; % DOT IS THE ONLY VALID SEPARATOR BETWEEN PARTS OF THE SERIAL.
qc_rack_size                = 20;    % jas_hardcoded qc_rac_size
cur_qc_rack_number          = 1;
cur_qc_rack_index           = 0;


prev_spot_ln                                 = char(response_qc_table.bom_lotnum( 1 ));

for cur_mb_id_ndx = 1:1:serial_cnt
    % HAVE MATCH BOX SERIAL: retrieve the wafer info to see if the wafer is one of the wanted P values that belong to one of the lots.
    
    % HAVE A WANTED P VALUE: Store the P and the mb_id:
    cur_spot_ln                                 = char(response_qc_table.bom_lotnum( cur_mb_id_ndx )); % save spot lot number
    
    cur_mb_ln                                   = char(response_qc_table.lotnum(cur_mb_id_ndx));      % save match box lot number
    cur_mb_wo                                   = char(response_qc_table.workorder(cur_mb_id_ndx));   % save qc work order
    table_ndx                                   = double(response_qc_table.table_ndx(cur_mb_id_ndx));  
    % Get the wafer/die information: Store the P, the wafer lot, wafer number and die index.
    cur_chip_id_all_str                         = char(response_qc_table.bom_serial( cur_mb_id_ndx ));
    cur_chip_id_str                             = split(cur_chip_id_all_str,delimiter);
    cur_wafer_ln                                = cur_chip_id_str(1);
    cur_wafer_number                            = double(cur_chip_id_str(2)); % the second is the wafer number
    cur_die_index                               = double(cur_chip_id_str(3)); % the last part (the 3rd) is now the die_index
    
    cur_mb_id_str                               = char(response_qc_table.serial( cur_mb_id_ndx ));
    qc_rpt_table.chip_id(cur_mb_id_ndx)         = {cur_chip_id_all_str      };
    qc_rpt_table.wafer_ln(cur_mb_id_ndx)        = {cur_wafer_ln             };
    qc_rpt_table.wafer_number(cur_mb_id_ndx)    = {cur_wafer_number      	};
    qc_rpt_table.die_index(cur_mb_id_ndx)       = {cur_die_index            };
    
    % Determine rack number and index for current carrier: produces indices: 1 to 20 , 1 to 20 .. to the end
    if (strcmp(prev_spot_ln,cur_spot_ln))
        % SAME SPOTTING LOT:
        cur_qc_rack_index     = cur_qc_rack_index + 1;
        if (cur_qc_rack_index > qc_rack_size)
            cur_qc_rack_number= cur_qc_rack_number+1;
            cur_qc_rack_index = 1;
        end
    else
        % DIFFERENTE SPOTTING LOT: reset both
        prev_spot_ln = cur_spot_ln;
        cur_qc_rack_number= 1;
        cur_qc_rack_index = 1;
    end
    qc_rpt_table.splot_ln(cur_mb_id_ndx)        = {cur_spot_ln                                      }; % this is the spotting lot number
    qc_rpt_table.mb_wo(cur_mb_id_ndx)           = {cur_mb_wo                                        }; % this is the mb WO being processed
    qc_rpt_table.mb_ln(cur_mb_id_ndx)           = {cur_mb_ln                                        }; % the lot number  of the carrier becomes the lot  number of the mb
    qc_rpt_table.mb_id(cur_mb_id_ndx)           = {cur_mb_id_str                                    }; % the chip serial : I.e. 'P162143.08.0187'
    qc_rpt_table.mb_pn(cur_mb_id_ndx)           = {qc_part_number                                   };  % the part number of the carrier becomes the part number of the mb
    qc_rpt_table.qc_rack_number(cur_mb_id_ndx)  = {cur_qc_rack_number                               };
    qc_rpt_table.qc_rack_index(cur_mb_id_ndx)   = {cur_qc_rack_index                                };
    qc_rpt_table.table_ndx(cur_mb_id_ndx)       = {table_ndx                                        }; % to save the original order from rucard
    
    % Additional info: If you select one carrier, let's say: B1600002
    % 1 - Step 20 / A320 shows the following data:  Within the BOM Item consumption record:
    %     Part number: 00445  Serial / Lot: P162143.08.0002 From inventory
end % for each MB serial
if (display_flag)
    disp('DONE RETRIEVING QC_RPT_DATA FROM RUNCARD')
    disp(qc_rpt_table)
end
end % fn get_qc_rpt_data_from_runcard_lot_number

