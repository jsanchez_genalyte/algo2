function    [ start_to_day_date_str, end_to_day_date_str, start_to_sec_date_str,end_to_sec_date_str ] = parse_date_keyword_2(wanted_keyword)
%PARSE_DATE_KEYWORD_2 converts a wanted_keyword into concrete start and end dates:
% handles all cases of 2 word keywords. If none found, returns blank: start_to_day_date_str and end_to_day_date_str
% sample call:   [ start_to_day_date_str, end_to_day_date_str ] = date_keyword_2_hh_mm('THIS MONTH');

% keywords_2 = { 'THIS WEEK', 'LAST WEEK'} %, 'THIS MONTH', 'LAST MONTH',  'THIS YEAR' ,  'LAST YEAR' };
% keywords_2_cat = categorical(keywords_2);  start_to_day_date_str, end_to_day_date_str,start_to_sec_date_str, end_to_sec_date_str

% sample:      02/23/2018 23:59:11 AM
%              123456789 123456789 12
beg_day_str =           ' 00:00:00 AM'; % intentionally: leave blank at the beginning!
beg_year_str= '01/01/2017 00:00:00 AM'; % 7:10       beg_year_str(7:10) = 
end_year_str= '12/31/2017 11:59:59 PM'; % 7:10   
end_day_str =           ' 11:59:59 PM';
beg_hour_str =              '00:00'; % ndx: 15:19 
%end_hour_str =             '59:59'; % ndx: 15:19 

date_str_to_day_format      	= 'mm/dd/yyyy';
date_str_to_sec_format       	= 'mm/dd/yyyy HH:MM:SS PM';
%date_str_to_hou_format        	= 'mm/dd/yyyy HH PM';

end_to_day_date_str             = '';
start_to_day_date_str           = '';
time_now_dt                     = datetime('now');
%cur_year                       = year(time_now_dt);
%cur_month                      = month(time_now_dt);

% dsbug: datestr(sprintf('%s/%s/%s',num2str(cur_month), num2str(cur_day), num2str(cur_year)),date_str_to_day_format);

if (strcmp(wanted_keyword,'THIS WEEK')) % % partial week
    % Find the day of the week and go back to monday:                                   

    end_to_day_date_str          = datestr(now,date_str_to_day_format);    
    end_to_sec_date_str          = strcat(end_to_day_date_str,beg_day_str);      

    end_day_of_week_num          = weekday(datenum(time_now_dt));    
    start_date                   = time_now_dt-days(end_day_of_week_num-1);
    start_to_day_date_str        = datestr(start_date,date_str_to_day_format);
    start_to_sec_date_str        = strcat(start_to_day_date_str,beg_day_str);          
    return;
end

if (strcmp(wanted_keyword,'LAST WEEK'))
    % Find the day of the week and go back to monday: then substract 7 days.         	% OK
    end_day_of_week_num          = weekday(datenum(time_now_dt));
    end_date                     = time_now_dt-days(end_day_of_week_num-1);
    end_to_day_date_str          = datestr(end_date,date_str_to_day_format);    
    end_to_sec_date_str          = strcat(end_to_day_date_str,beg_day_str);       

    end_to_day_date_str          = datestr(end_date,date_str_to_day_format);
    start_date                   = end_date-days(7);
    start_to_day_date_str        = datestr(start_date,date_str_to_day_format);
    start_to_sec_date_str        = strcat(start_to_day_date_str,beg_day_str);         
    return;
end
if (strcmp(wanted_keyword,'LAST HOUR'))
    % partial hour:  This will at the most one hour. last hour means within the current hour up to the current minute.
    % see: date_keyword_2_hh_mm for:  last 1 hour and last 2 hours
    start_to_day_date_str        = datestr(time_now_dt,date_str_to_day_format);     
    start_to_sec_date_str        = datestr(time_now_dt,date_str_to_sec_format); 
    start_to_sec_date_str(15:19) = beg_hour_str;  '00:00'; % start of the hour
    
    end_to_day_date_str          = start_to_day_date_str;
    end_to_sec_date_str          = datestr(time_now_dt,date_str_to_sec_format); 
    return;    
end
if (strcmp(wanted_keyword,'THIS MONTH')) % % partial month
    % Find the day of the month and go back to the first:                                   
    end_to_day_date_str          = datestr(now,date_str_to_day_format);    
    end_to_sec_date_str          = strcat(end_to_day_date_str,beg_day_str);      

    end_day_of_month_num         = day(datenum(time_now_dt));    
    start_date                   = time_now_dt-days(end_day_of_month_num-1);
    start_to_day_date_str        = datestr(start_date,date_str_to_day_format);
    start_to_sec_date_str        = strcat(start_to_day_date_str,beg_day_str);          
    return;
end    

if (strcmp(wanted_keyword,'LAST MONTH'))  % full: exact: similar to previous month.
     % full month: today-1 month  
    temp_str_to_day_format  = 'MM/dd/yyyy'; % SPECIAL for this case: so it is not ambiguous mm month or mm minute:
    % Fist: substract one month from now. Then get start month and year. then use day 1 for start.
    start_date              = time_now_dt - calmonths(1);
    start_year              = year(start_date);
    start_month             = month(start_date);
    start_day               = 1;
    date_start_dt           = datetime(start_year,start_month,start_day,'format',temp_str_to_day_format);
    start_to_sec_date_str   = datestr(date_start_dt,'mm/dd/yyyy');  % IT DOES NOT WORK: ,'format','mm/dd/yyy'); % date_str_format); % date_start_num      = datenum(date_start_dt);
    start_to_sec_date_str     = strcat(start_to_sec_date_str,beg_day_str);
    % HAVE START DATE: Add 1 calendar month minus 1 da because jervis is inclusive in the end date.
    % date_start_num        = datetime(start_year,start_month,1,0,0,0);
    end_date_dt             = date_start_dt   +  calmonths(1);
    end_date_dt             = end_date_dt - caldays(1);
    end_to_day_date_str     = datestr(end_date_dt,'mm/dd/yyyy'); % IT DOES NOT WORK: , date_str_format);
    end_to_sec_date_str     = strcat(end_to_day_date_str,end_day_str);
    return;
end 

if (strcmp(wanted_keyword,'THIS YEAR')) % % partial year
    %                     
    start_year                 = int2str(year(time_now_dt));
    start_month                 = '01';
    start_day                   = '01';
    start_to_day_date_str       = strcat(start_month,'/',start_day,'/',start_year);
    end_to_day_date_str         = datestr(today,date_str_to_day_format);
    
    start_to_sec_date_str       = strcat(start_to_day_date_str,beg_day_str);
    end_to_sec_date_str         = datestr(now,date_str_to_sec_format);
    return;
end

if (strcmp(wanted_keyword,'LAST YEAR')) % % full year: == previous 
    %                      
    prev_year                  = int2str(year(time_now_dt)-1);  
    start_to_day_date_str       = beg_year_str(1:10);
    start_to_day_date_str(7:10) = prev_year  ;  
    
    end_to_day_date_str         = end_year_str(1:10);
    end_to_day_date_str(7:10)   = prev_year;      
    
    start_to_sec_date_str       = beg_year_str;    
    start_to_sec_date_str(7:10) = prev_year  ;     
    
    end_to_sec_date_str         = end_year_str;    
    end_to_sec_date_str(7:10)   = prev_year;  
    return;
end 


    
end % fn parse_date_keyword_2

%
% % left overs
%
%     start_day           = '01';
%     end_day             = '01';     % approximation: it should be 28,29,30 or 31
%     start_year          = int2str(year(time_now_dt));
%     end_year            = start_year;
%
%
%     datestr(sprintf('%s/%s/%s',num2str(cur_month), num2str(cur_day), num2str(cur_year)),date_str_to_day_format);
%
%
%     cur_month           = month(time_now_dt);
%     start_to_day_date_str  =
%
%     if (month(time_now_dt)==1)
%         % JANUARY
%         start_year      = int2str(year(time_now_dt)-1);
%         start_month     = '12';
%         end_month       = '01';
%     else
%         % NO NEED TO ADJUST MONTH/YEAR
%         if (month((time_now_dt))<=9)
%             % ONE DIGIT
%             start_month = sprintf('0%1d',month((time_now_dt-1)));
%         else
%             % TWO DIGIT
%             start_month = sprintf('%2d',month((time_now_dt-1)));
%         end
%         if (month((time_now_dt))<=9)
%             % ONE DIGIT
%             end_month = sprintf('0%1d',month((time_now_dt))); % approx
%         else
%             % TWO DIGIT
%             end_month = sprintf('%2d',month((time_now_dt)));  % approx
%         end
%     end
%     start_to_day_date_str  = strcat(start_month,'/',start_day,'/',start_year);
%     end_to_day_date_str    = strcat(end_month,'/',end_day,'/',end_year);
%

%
%     start_year          = int2str(year(time_now_dt));
%     if (month((time_now_dt))<=9)
%         % ONE DIGIT
%         start_month     = sprintf('0%1d',month((time_now_dt)));
%     else
%         % TWO DIGIT
%         start_month     = sprintf('%2d',month((time_now_dt)));
%     end
%     start_day           = '01';
%     start_to_day_date_str = strcat(start_month,'/',start_day,'/',start_year);



%     % last year: Fist: substract one month from now. Then get start month and year. then use day 1 for start.
%     start_date          = time_now_dt- calyears(1);
%     start_year          = year(start_date);
%     start_month         = '01';
%     start_day           = '01';
%     start_to_day_date_str  = datestr(sprintf('%s/%s/%s',num2str(start_month), num2str(start_day), num2str(start_year)),date_str_to_day_format);
%     % HAVE START DATE: Add 1 calendar month minus 1 da because jervis is inclusive in the end date.
%     end_month           = '12';
%     end_day             = '31';
%     end_to_day_date_str    = datestr(sprintf('%s/%s/%s',num2str(end_month), num2str(end_day), num2str(start_year)),date_str_to_day_format);
