% lots:   ANA-QC-AB1602     AB1602
% lots:   ANA-QC-AB160J     AB160J
% exp:    ANA-QC-
% lots:   AB1602   AB160J

spotl_cat = categorical(assay_sum_table.spotl);
spotl_cat_u = unique(categorical(assay_sum_table.spotl));
spotl_cnt   = length(spotl_cat_u);
spotl_oe_cnt= spotl_cnt*2;  
odd_ch_ndx  = 1:2:size(assay_sum_ana_mat,1);
even_ch_ndx = 2:2:size(assay_sum_ana_mat,1);

% Produce indices to travers odd and even data.
ndx_all                 = false(size(assay_sum_ana_mat,1),1);
ndx_odd                 = ndx_all;
ndx_even                = ndx_all;
ndx_odd(odd_ch_ndx)     = 1;
ndx_even(even_ch_ndx)   = 1;

data_offset = 9; % data starts in col 10

% build index of lot_numbers
spot_mat_ndx = nan(height(assay_sum_table),spotl_cnt);

for cur_lot_ndx = 1:1:spotl_cnt
    cur_lot     = spotl_cat_u(cur_lot_ndx);
    spot_mat_ndx(:,cur_lot_ndx)= spotl_cat ==  cur_lot;
    spot_cnt_list =   sum(spot_mat_ndx);
end

% fill in the matrix: data_box_plot with data arranged this way:
%                     each cil in one lot. 

largest_lot_data_len = max(sum(spot_mat_ndx));
for cur_ana_ndx=1:1:1 % 6
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both    
    data_box_plot  	= nan(largest_lot_data_len,spotl_cnt);
    data_odd_plot  	= nan(largest_lot_data_len,spotl_cnt);    
    data_even_plot  = nan(largest_lot_data_len,spotl_cnt);      
    for cur_lot_ndx = 1:1:spotl_cnt
        ndx_cur     = logical(spot_mat_ndx(:,cur_lot_ndx));   % spot_mat_ndx(1:spot_cnt_list(cur_lot_ndx))';
        ndx_cur_odd = ndx_cur;
        ndx_cur_odd(ndx_even) =0; % for the odd: reset the even
        
        ndx_cur_even= ndx_cur;
        ndx_cur_even(ndx_odd) =0; % for the even: reset the odd
        temp_rows = 1:spot_cnt_list(cur_lot_ndx);
        temp_rows = temp_rows';
        data_box_plot( temp_rows,                     cur_lot_ndx) = assay_sum_ana_mat(logical(spot_mat_ndx(ndx_cur,     cur_lot_ndx)),cur_ana_ndx);
        data_odd_plot( 1:sum(ndx_cur_odd),            cur_lot_ndx) = assay_sum_ana_mat(logical(spot_mat_ndx(ndx_cur_odd, cur_lot_ndx)),cur_ana_ndx);
        data_even_plot(1:sum(ndx_cur_even),           cur_lot_ndx) = assay_sum_ana_mat(logical(spot_mat_ndx(ndx_cur_even,cur_lot_ndx)),cur_ana_ndx);
    end
    data_ana = assay_sum_ana_mat(cur_ana_ndx+data_offset);
    name_ana = assay_sum_table.Properties.VariableNames{cur_ana_ndx+data_offset};
    figure;
    % produce box plots for all lots and given analyte
    
    boxplot(data_box_plot,'notch','on',...
        'labels',{ char(spotl_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))
    % draw the individual dots: oddd and even
    hold all;
    cur_start_x = 0.3;
    incr_x      = 0.2;
    data_odd_plot_x                     = zeros(size(data_odd_plot,1),spotl_cnt);
    for cur_lot_ndx = 1:1:spotl_cnt
        data_odd_plot_x(1:end,  cur_lot_ndx)  = cur_start_x + incr_x;
        cur_start_x = cur_start_x + incr_x;
    end
    %scatter(data_odd_plot_x,data_odd_plot);
end % for each analyte

figure
boxplot(assay_sum_ana_mat(odd_ch_ndx,1:spotl_cnt),'notch','on',...
        'labels',{ char(spotl_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))      

spotl_cat_u_oe       = cell(spotl_oe_cnt,1);
assay_sum_oe_ana_mat = nan(odd_ch_ndx,32);

for cur_spotl_ndx =1:1:spotl_oe_cnt
    
end
spotl_cat_u_oe = cell(size(spotl_cat_u,1)*2,1);

% 'Whisker' � Maximum whisker length
% 1.5 (default) | positive numeric value
% Maximum whisker length, specified as the comma-separated pair consisting of 'Whisker' and a positive numeric value.
% boxplot draws points as outliers if they are greater than q3 + w � (q3 � q1) or less than q1 � w � (q3 � q1). 
% q1 and q3 are the 25th and 75th percentiles of the sample data, respectively.
% The default value for 'Whisker' corresponds to approximately +/�2.7? and 99.3 percent coverage if the data are normally distributed. 
% The plotted whisker extends to the adjacent value, which is the most extreme data value that is not an outlier.

Specify 'Whisker' as 0 to give no whiskers and to make every point outside of q1 and q3 an outlier.    
    
% Box Plots
% 
% Box plots provide a visualization of summary statistics for sample data and contain the following features:
% 
% The tops and bottoms of each "box" are the 25th and 75th percentiles of the samples, respectively. The distances between the tops and bottoms are the interquartile ranges. You can compute the value of the interquartile range using iqr.
% The line in the middle of each box is the sample median. If the median is not centered in the box, it shows sample skewness. You can compute the value of the median using the median function.
% The whiskers are lines extending above and below each box. Whiskers are drawn from the ends of the interquartile ranges to the furthest observations within the whisker length (the adjacent values).
% Observations beyond the whisker length are marked as outliers. By default, an outlier is a value that is more than 1.5 times the interquartile range away from the top or bottom of the box, but this value can be adjusted with additional input arguments. Outliers are displayed with a red + sign.
% Notches display the variability of the median between samples. The width of a notch is computed so that box plots whose notches do not overlap (as above) have different medians at the 5% significance level. The significance level is based on a normal distribution assumption, but comparisons of medians are reasonably robust for other distributions. Comparing box-plot medians is like a visual hypothesis test, analogous to the t test used for means.


% Extract the data for each variety
setosa = meas(1:50, :);
virginica = meas(51:100, :);
versicolor = meas(101:150, :);

% Calculate the means and standard deviations for each variety
irisMeans = [mean(setosa); mean(virginica); mean(versicolor)];
irisSTDs =  [std(setosa); std(virginica); std(versicolor)];

% Draw error bar chart with means and standard deviations
figure
errorbar(irisMeans, irisSTDs, 's')

% Add title and axis labels
title('Comparison of three species in the Fisher Iris data')
xlabel('Species of Iris')
ylabel('Mean size in mm')
box on

% Change the labels for the tick marks on the x-axis
irisSpecies = {'Setosa', 'Virginica', 'Versicolor'};
set(gca, 'XTick', 1:3, 'XTickLabel', irisSpecies)

% Create labels for the legend
irisMeas = {'Sepal length', 'Sepal width', 'Petal length', 'Petal width'};
legend(irisMeas, 'Location', 'Northwest')