function [ handles ] = sa_ui_set_env_after_get_exp( handles )
%SA_SET_ENV_AFTER_GET_EXP sets the ui consistent with the experiments just loaded.
% Goest thru all the loaded data and updates all the  menu items to reflect the available data to work with:
% populates toes. 
% source: sa_ui_set_env_after_get_data (clone)

% input_parameters_struct           = handles.input_parameters_struct;
ui_state_struct                     = handles.ui_state_struct;

% get the number of loaded rows and unique values and counts for each pulldown
rows_db_cnt                         = handles.db_toe_set_struct.rows_db_cnt{1};     % single float
ui_state_struct.db_cur_toe_net_ndx  = ones(rows_db_cnt,1);                           % start with all rows selected
if (rows_db_cnt)
    % SOMETHING GOT LOADED: From db or local file: Set the ui to be consistent:
    % Enable button: sensograms: tab number 2. jas_tbd
    
    % E1.  Main Menus FILE/EDIT/VIEW/ANALYZE MENU:  _________	      set(handles.edit_menu,                             'Visible','on','Enable','on');
    set(handles.view_menu,                          'Visible','on','Enable','on');
    set(handles.file_save_db_local_menu_item,       'Visible','on','Enable','on');
    set(handles.file_append_sel_local_menu_item,    'Visible','on','Enable','on');
    set(handles.edit_copy_sel_menu_item,            'Visible','on','Enable','off');    % wait for selection
    set(handles.analysis_menu,                      'Visible','on','Enable','on');     % menu_4 analysis
    set(handles.analysis_norm_jervis_menu_item,   	'Visible','on','Enable','on');

    %datat_len= size(list_datat,1);      % noiu it does not make sense
    %     valfg_len = size(list_valfg,1);    jas_tbd_future
    %     outfg_len = size(list_outfg,1);
    %     error_len = size(list_error,1);

    % Update ui_state_struct indices with the new loaded data (to be consistent):

%     if (~isempty(list_dates)) % 2
%         ui_dates_state_struct                       = sa_ui_create_popup_struc( list_dates,'DATES');
%         ui_state_struct.dates                       = {ui_dates_state_struct};
%         set(handles.popupmenu_dates,'String',ui_dates_state_struct.list_item_names,'Value',1);
%     end
 
    % text_probe      	popupmenu_probe     1 Ana_1 ..Ana_16 , Tr, All (except  Tr)   no need for check marks
    % text_dates     	popupmenu_dates     2
    % text_proto      	popupmenu_proto     3
    % text_sampl       	popupmenu_sampl     4
    % text_chipl      	popupmenu_chipl     5
    % text_exper       	popupmenu_exper  	6
    % text_instr     	popupmenu_instr     7
    % text_carri        popupmenu_carri     8
    % text_chann        popupmenu_chann     9
    % text_clust        popupmenu_clust   	10
    % text_datat     	popupmenu_datat   	11
    % text_ampli        popupmenu_ampli     12
    

    ui_state_struct.ui_request_flag         = 'g_n_a'; % consistent with: 0 hide_good and 0: hide_anom 
    % The Tos should be visible:
    % has_tbd: poulate toe with all the db_data: 
    [toe_cell,toe_col_struct]	= toe_create(handles.db_toe_set_struct.rows_db_cnt{1}); % future.   sum(handles.ui_state_struct.db_cur_fit_net_ndx);
    handles.toe_col_struct      = toe_col_struct;
    handles.toe_cell            = toe_cell; % toe_cell has the full_db_data_set
    % Save the virigin full toe_cell (untochable!),
    [handles]                   = ui_do_fill_toe_sens_set(handles);
    
    
    
    set(handles.ui_table_exp,                       'Visible','on','Enable','on');
    set(handles.edit_select_all_menu_item,          'Visible','on','Enable','on');
    set(handles.edit_sel_inv_menu_item,             'Visible','on','Enable','on');
    
    handles.ui_state_struct = ui_state_struct;  % save it back.
        % MAKE SURE WE START WITH ALL SELECTED IN THE TOE
     %[ handles ] = mpopup_menu_reset_all_to_all_all( handles );

end % rows_db_cnt > 0
end % fn sa_set_env_after_get_data

