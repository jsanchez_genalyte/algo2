function [ error_message,exp_detail_struct ] = sa_rest_exp_get_details( a_exp_id,rest_token  ) % JOHN: Seems to work fine.  API_6: showTestMetadata
%SA_REST_EXP_GET_DETAILS retrieves the details of a given a_exp_id
%   assumption: None. If no experiment exist with this id: exp_detail_struct will be returned nan.
%   if it is  valid: the error_msg wil be empty
% returns: exp_detail_struct =   struct with fields:
%       device_id;  % 'M53'                 nstrument_id
%     protocol_id;  % '4cdbc37c-3325-4�'   protocol_guid
%         control;  % 0                     flag: 0 == no control: ie production   
%     specimen_id;  % {2�1 cell}            'PS12345601', 'PS12345601'  John? What is this? linkages?
%      consumable;  % [1�1 struct]           struct with fields:
%                                               experiment_name;        %  'Jervis ANA Validation'
%                                               carrier_serial_number;  %  'A27NFXP'  == MB Carrier # From EXCEL FILE. 
%                                               spotting_lot_number;    %  'AA17R2'
%                                               lot_number;             %  'AA17R2'  % John: what is this lot number?  
   

% sample call: [ error_message,exp_detail_struct ] = sa_rest_exp_get_details('b5d306cb-70a4-462a-a651-d3878bddadc4',handles.rest_token )
exp_detail_struct  = struct;
error_message       = '';
% ASSUME: OK RESPONSE FROM THE WEB SERVICE
test_metad_struct = struct;
test_metad_struct.status = '';     % string     % John: who fills the status? is it input or output?
test_metad_struct.errors = [ 0 0 ]; % vect int  %       same for errors ?

test_metad_struct_inst 	= test_metad_struct;
% Done creating body parameter: id = 'b5d306cb-70a4-462a-a651-d3878bddadc4' for test(5)  if exits returns metadata for a test, otherwise errors out (try block)
test_metad_inst_json   	= jsonencode(test_metad_struct_inst);

options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )});

[ url_struct ]          = sa_set_production_vs_stagging_url_flag(); % prod_stag_flag );
url_exp_det_str          = url_struct.url_exp_det_str;

url_exp_det_str_full  	= strcat(url_exp_det_str,sprintf('%s/metadata',char(a_exp_id)));

try
    error_message       = '';
    exp_detail_struct   = nan;
    exp_detail_struct 	= webread(url_exp_det_str_full,'json',test_metad_inst_json,options);
catch
    % COULD NOT READ EXP DETAILS
    error_message       = sprintf('\nERROR_ WHILE READING experiment details details from API: \nWEBREAD error. Function: sa_rest_exp_get_details for Experiment: %s NOT FOUND',a_exp_id);
    %display(error_message);
    return;
end
if ( (isfield(exp_detail_struct,'device_id')) && (isfield(exp_detail_struct,'protocol_id')) && (isfield(exp_detail_struct,'specimen_id')) )
    % OK RESPONSE FROM THE WEB SERVICE: API experiment details
else
    error_message = sprintf(...
        '\nERROR_ RETRIEVING THE EXPERIMENT DETAILS FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: ');
end
end % fn sa_rest_exp_get_details




