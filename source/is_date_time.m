function [ valid_date_format_flag ] = is_date_time( a_date_str,a_date_format_str )
%IS_DATE_TIME returns true or false deppending of valid datetime strings for a given date_format string.
%   Detailed explanation goes here
% sample call: [ valid_date_format_flag ] = is_date_time(  '11/22/2017',  'mm/dd/yyyy')  % returns true
% sample call: [ valid_date_format_flag ] = is_date_time(  '11/32/2017',  'mm/dd/yyyy')  % should return false but returns TRUE !!! see below
% sample call: [ valid_date_format_flag ] = is_date_time(  '11/xx/2017',  'mm/dd/yyyy')  % returns false as expected
valid_date_format_flag = 1;
try
    a_date_num = datenum(a_date_str,a_date_format_str);
catch
    valid_date_format_flag = 0;
    %fprintf('Invalid Date Format: %s',datestr(a_date_num));
    %error('Invalid Date Format')
    return
end

% NOT NEEDED: Do final check: currently tbd: user must enter: 04/02/2018 because the strcmp fails when user enter: 4/2/2018
% if ( (strcmp(  datestr( a_date_str,a_date_format_str) , a_date_str)) ...
%        || (datenum(a_date_str) == datenum(a_date_format_str) ))
%     %fprintf('   VALID Date Format: %s',datestr(a_date_num));
% else
%     valid_date_format_flag = 0;                    
%     fprintf('IN-VALID Date Format: \n Passed:  %-s \n VS. Get: %-s\n', a_date_str, datestr( a_date_str,a_date_format_str));
% end

end % fn isdatetime

% Invalid Return: 
% VALID Date Format: 02-Dec-2017
% valid_date_format_flag =  1

% a_date_format_str = 'mm/dd/yyyy'; 
% a_date_str        = '11/32/2017';
% if ( is_date_time( a_date_str,a_date_format_str )

%       strcmp(  datestr( '11/22/2017','dd/mm/yyyy') , '11/22/2017')
%       strcmp(  datestr( '11/23/2017','dd/mm/yyyy') , '11/32/2017')
%  if ( strcmp(  datestr( a_date_str,a_date_format_str) , a_date_str)