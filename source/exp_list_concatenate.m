function  [exp_list, error_message ]  = exp_list_concatenate(exp_list_acc_cel ) % FOLLOW UP: Debug it with John. It errors out.
%EXP_LIST_CONCATENATE concatenate as many exp_lists come on the exp_list_acc_cel cell and filters them out for completness.
%   handles concatenation of up to 1000 experiments and removes every experiment that is not complete.

% pipeline:  get_jervis_data_for_dcrc --> sa_rest_exp_get_all --> exp_list_concatenate
% ASSUMPTIONS: preceding loop:
% exp_set_cnt = 0;
% while (1)
%     exp_list                          = webread(url_experiments_str,'start_date',start_date,'end_date',end_date,options);
%     exp_set_cnt                       = exp_set_cnt +1;
%     exp_list_acc_cel{exp_set_cnt}     = exp_list ;
%     if ( size(exp_list{exp_set_cnt},1)< 100)
%         break;
%     end
%     % POSSIBLY MORE DATA
% end; % while not done
error_message ={};
exp_list = {};
if (isempty(exp_list_acc_cel{1}))
    return; % nothing to concatente
end

%jas_here_friday_: loop over each element of exp_list_acc_cel
%                  and flattein it inside the loop
%  goal: a list of homogeneous elements for all the exp_list_acc_cel{1} that can be cvted to table
% 
% jas_sp_stop: figure out how to build table: issue 
%  a55 =a5;
% a55.device_id = a4.device_id
% 
% a55 = 
% 
%         id          state      control    protocol_id    device_id     specimen_id      consumable     created_at     updated_at     specimen_id_2
%     ___________    ________    _______    ___________    _________    _____________    ____________    ___________    ___________    _____________
% 
%     [1x36 char]    complete    false      [1x36 char]    M66          'IFCCCRP-600'    [1x1 struct]    [1x29 char]    [1x29 char]    'IFCCCRP-300'
% 
% a = [ a55; a4 ]
% 
% a = 
% 
%         id          state      control    protocol_id    device_id     specimen_id      consumable     created_at     updated_at     specimen_id_2
%     ___________    ________    _______    ___________    _________    _____________    ____________    ___________    ___________    _____________
% 
%     [1x36 char]    complete    false      [1x36 char]    M66          'IFCCCRP-600'    [1x1 struct]    [1x29 char]    [1x29 char]    'IFCCCRP-300'
%     [1x36 char]    complete    false      [1x36 char]    M66          '0.085M EPPS'    [1x1 struct]    [1x29 char]    [1x29 char]    '0.085M EPPS'
% 
% a55.device_id = a5.device_id
% 
% a55 = 
% 
%         id          state      control    protocol_id    device_id     specimen_id      consumable     created_at     updated_at     specimen_id_2
%     ___________    ________    _______    ___________    _________    _____________    ____________    ___________    ___________    _____________
% 
%     [1x36 char]    complete    false      [1x36 char]    K2B-1        'IFCCCRP-600'    [1x1 struct]    [1x29 char]    [1x29 char]    'IFCCCRP-300'
% 
% a = [ a55; a4 ]
% Could not concatenate the table variable 'device_id' using VERTCAT.
% 
% Caused by:
%     Error using vertcat
%     Dimensions of matrices being concatenated are not consistent.
%  
% a = [ a4; a5 ]
% Could not concatenate the table variable 'device_id' using VERTCAT.

[ exp_list_acc_cel ] = exp_list_cel_flatten_specimen_id( exp_list_acc_cel ); % make sure struct2table is happy
exp_set_cnt = size(exp_list_acc_cel,1);
switch exp_set_cnt
    case 1
        exp_list_acc_cel{1}.specimen_id = exp_list_acc_cel{1}.specimen_id{1}; % get rid of second_specimen to be able to build table
        exp_list_tbl = struct2table(exp_list_acc_cel{1});
    case 2
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2})] ;
    case 3
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{3})] ;
    case 4
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{3}) ; struct2table(exp_list_acc_cel{4})] ;
    case 5
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{4}) ; struct2table(exp_list_acc_cel{5})];
    case 6
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{3}) ; struct2table(exp_list_acc_cel{4}) ; struct2table(exp_list_acc_cel{5}) ; struct2table(exp_list_acc_cel{6})] ;
    case 7
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{4}) ; struct2table(exp_list_acc_cel{5}) ; struct2table(exp_list_acc_cel{6}); struct2table(exp_list_acc_cel{7}) ];
    case 8
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{3}) ; struct2table(exp_list_acc_cel{4}) ; struct2table(exp_list_acc_cel{5}) ; struct2table(exp_list_acc_cel{6}); struct2table(exp_list_acc_cel{7}); struct2table(exp_list_acc_cel{8})] ;
    case 9
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{4}) ; struct2table(exp_list_acc_cel{5}); struct2table(exp_list_acc_cel{6}); struct2table(exp_list_acc_cel{7}); struct2table(exp_list_acc_cel{8}); struct2table(exp_list_acc_cel{9})] ;
    case 10
        exp_list_tbl = [ struct2table(exp_list_acc_cel{1}) ; struct2table(exp_list_acc_cel{2}) ; struct2table(exp_list_acc_cel{3}) ; struct2table(exp_list_acc_cel{4}) ; struct2table(exp_list_acc_cel{5}) ; struct2table(exp_list_acc_cel{6}); struct2table(exp_list_acc_cel{7}); struct2table(exp_list_acc_cel{8}); struct2table(exp_list_acc_cel{9}); truct2table(exp_list_acc_cel{10})];
    otherwise
        %error_message = 'More than 1000 experiments found. Try a smaller window';
        exp_list_tbl = table();
        for ndx=1:1:exp_set_cnt
            exp_list_tbl = [ exp_list_tbl ; struct2table(exp_list_acc_cel{ndx}) ] ;
        end
        %return;
end
state_temp          = categorical(cellstr(exp_list_tbl.state));
complete_ndx        = state_temp == 'complete';
if (sum(complete_ndx) < height(exp_list_tbl))
    % SOME EXPERIMENTS ARE NOT COMPLETE
    %exp_list_tbl = exp_list_tbl(complete_ndx,:);  % jas_here_fri_afternoor: work with exp_list_tbl and filter exp with no protocols
    exp_list        = table2struct(exp_list_tbl(complete_ndx,:));
else
    exp_list        = table2struct(exp_list_tbl);
end
end % fn exp_list_concatenate