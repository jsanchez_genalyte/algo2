% file:              sa_cfg.m
% ref:               configuration file for the sensogram analysis app.
% Instructions:      Modify this file with the default starting parameters for the senso_anal app.
% To load this file: sa_cfg_qc    see: read_cfg_parameters_file
% see:               sa_read_cfg_parameters_file
% file_locations______________________________________________________________________________________________________________________________________
% local_input_dir  = '\senso_anal\cfg_and_data\try_001_04_11_local_db\';
% input_file_name_01  = 'data_set_1.mat' ;

cfg_file_name       = 'sa_cfg';   
cur_pwd             = pwd;
ndx                 = strfind(cur_pwd,'Documents');
if ( (~(isempty(ndx)))  && (length(ndx) == 1) )
    base_dir = cur_pwd;% (1:ndx+8);  %   C:\Users\jsanchez\Documents\algo2\source
    local_input_dir = strcat(base_dir, '\');   %  source is the most convinient... '\cfg_and_data\data_db\'   assume there is a \senso_anal\cfg_and_data\ within the Documents folder.
    % Check if the folder exists:
    if ( ( exist(local_input_dir,'dir') == 7))
        % Folder Exist: We are almost done.
    else
        % Folder does not exist: Prompt for loc of input local_db file.
        local_input_dir = base_dir;  % If nothing found: Use this one to start with. This is the
                                     % default dir to look for local db matlab input files.
    end
end
input_file_name   	= 	 'local_data_set.mat' 	;  
% data related: ______________________________________________________________________________________________________________________________________
% analyte_names_for_analysis:    note: all for probe means all except tr.                           % enter: { 'all' }             to include everything on the local_db file. or:
% True_master: read_cfg file need to be updated to match this one!!! 
probe_valid	= { 'All','HSA1',	'dsDNA',	'Sm',	'Scl-70',	'HSA5',	'Jo-1',	'SS-A60',	'HSA8',	'SS-B',	'RNP',	'HSA11',	'CenpB',	'HSA13',	'HumanIgG',	'HSA15',	'Anti-IgG', 'Tr'};  % 'All (Except Tr)'
dates_valid	= { 'All','Last 7 days', 'Today', 'Yesterday', 'Last 30 days', 'This Month', 'Last Month',  'Search ...'};
proto_valid	= { 'All','ANA Multiplex (12 Chip) 1.1 QC-Prod','Search ...' }; % jas_tbd_hardcode only known protocol. add more from jason objs.
sampl_valid = { 'All','SAMPLE #1','SAMPLE #2','SAMPLE #3','SAMPLE #4','SAMPLE #5','Search...'}; % think jas_tbd jasd_here_Wednesday.
chipl_valid = { 'All','160505','Search ...'}; % think jas_tbd
exper_valid = { 'All','CL160505-12-Array-SP-01','CL160505-12-Array-SP-02','CL160505-12-Array-SP-03','CL160505-12-Array-SP-04','CL160505-12-Array-SP-05','Search...'}; % think jas_tbd
instr_valid = { 'All','10', '11','12','13','14','15','16','17','18','19','20','21','22','Search...'}; % think jas_tbd
carri_valid = { 'All','Search ...'}; % think jas_tbd
chann_valid = { 'All','1','2','Search ...'};
clust_valid = { 'All','1','2','3','4','Search ...'}; % here search sould allow only combinations of 1 to 4
datat_valid = { 'Raw','Normalized By Jervis','Normalized by Matlab ...'}; % note that this is the only case with no: 'All' !!!
ampli_valid = { 'All','Positives Only','Negative Only','Search ...'}; % think jas_tbd

global_figure_start_no          = 1;      % starting number of figure: All figures will be number after this value.
tr_delta_step_threshold         = 2.5;    % Maximum admisible difference between the 2 TR values in a channel for which TR values are consider sane (usable)
       
min_pos_delta_th_hor_pct        = 0.02;   % This is the theshold in percent of the range for which a value
%                                             is considered to be less than a previous one.
%                                           This is to avoid going back on almost horizonal fragment of the curve
min_pos_delta_th_amp_pct        = 0.1;    % This is the theshold in percent of the range for which an amplification value
%                                             is consider a real amplification vs just noise on a given curve.
%                                             % .1 == 10% (to start experimenting: any base line should produce an increment of at least
%                                             % 10% of the range of the curve.

wanted_inflexion_point_count    = 2;      % Want to find the 2 largest inflexion points: IE the ones followed by the largest going up values
tr_enable_normalize_flag        = true;   % true; % == Do TR normalize; % false; == Do not normalize

% gui_related:
senso_curves_perp_cnt          	=  4 ; % 1536; % 4 ; % the same variable will take different meaing according to: checkbox_visi_good_plots and checkbox_visi_anom_plots (either or boht) 
%good_curves_per_plot_cnt        = 12; % 1536; %12;     % sa_cfg_
%anom_curves_per_plot_cnt        =  4;     % 

color_orange_warning    = [ 255 165 79] ;
color_orange_warning    = color_orange_warning / 255 ;
color_green_nice    	= [ 27 79 53 ]; % rgb lite [ 59 113 86 ];  rgb med:  [  193 221 198 ]   rgb dark: [ 27 79 53  ]
color_green_nice    	= color_green_nice /255;  % normalized rgb


% jas_future: is it worthed to cfg so users can request only analytes in this list?

% leftovers: _______________________________________________________________________________________________________________________
% 04      % include here VALID analytes to be analyzed: default all found on data files.
% gru_range_for_output = [ 100 800 ];      % 05      % niu currently: AN_SP? Range of GRU values to use (values outside: cliped)                                                      
% con_range_for_output = [ 100 800 ];      % 06      % niu currently: AN_SP? Range of concntration values to use (values outside: cliped)                                                          
% synthetic_data_flag  = { 'false' };      % 07      % 'true' == synthetic data.	'false' == real instrument data. default==false
% fit_type_valid       = {  'exp1-cte' 'exp2' 'linear' 'poly3'  'power2'  'fourier2'};  % 08 similar to models_name_cell.

% additional fields: ______________________________________________________________________________________________________________________________________
% Add here fields as needed and update the read_cfg_parameters_file with the proper fields.
% field_name = field_value ....
