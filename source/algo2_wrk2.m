% file: algo2_wrk2.m (file_2)
tbd: algo2 before switch to sasi invert model; see: % file: algo2_wrk2.m (file_2) 2017_oct_05

1 - Clean up: 10:  
        - think about a cleaner way to do:
          start_next (maybe by comparing start_and start_next:
          if start_next is > start) then move start_to start_next 
        - clean calls to classify: within calc indices: check if any U:
          if U present: call classify. (so classify only in demand)
2 - Robust ui_eval_all_indices: 
            check for empty returns of each find or logical_subindices and handle gracefully
3 - Robust: plot: check for empty x,y and just clear the axis.

4 - complete criteria for good and anom 
            
4 - add logic to: / test: hide plots (without hiding good or anom)
5 - add logic to:  do combine      (without hiding good or anom)
6 - Auto / Manual axis: Dlg to get current,set to current, synch good and anom axes.      

latest files:  ui_eval_all_indices, Sensogram_Analysis_App_10 algo2_wrk2 ui_do_plot_sens_cur
future:  Menu: Data Filters / Reset al filters (Tos set all to ALL).



_______________________
middle
text_senso_per_plot     old: senso_flag_array  
edit_senso_per_plot     old: edit_sens_good_per_plot  
pushbutton_senso_next

senso_flag_array   old: pushbutton_sens_good_next
good
checkbox_good_sens_mea  old:  checkbox_good_sens_mea  
checkbox_good_sens_med  old:  checkbox_good_sens_med  
checkbox_good_sens_mea  old:  checkbox_good_sens_env  
anom
checkbox_anom_sens_mea  old:  checkbox_good_sens_mea  
checkbox_anom_sens_med  old:  checkbox_good_sens_med  

checkbox_hide_good      handles.ui_state_struct.checkbox_hide_good  = get(hObject,'Value')
checkbox_hide_anom      handles.ui_state_struct.checkbox_hide_anom  = get(hObject,'Value')
 
OLD TBD: pushbutton_sens_good_next AND 
 AXES: axes_anom_sens

function [ senso_flag_str ] = xxx( senso_flag_array )
%SC_PACK_SENSO_FLAGS Packs an array of sensogram flags(resulting from classification) into a single string
%   handles up to 10 flags,



end % fn sc_pack_senso_flags

replacements: 
    good_start_ndx              --> senso_start_ndx
    good_curves_per_plot_cnt   --> senso_curves_perp_cnt

handles.checkbox_visi_good_plots
handles.checkbox_visi_anom_plots
handles.checkbox_comb_allp_plots

% variables
% ui_state_struct. ui_visi_good_plots_fg
% ui_state_struct. ui_visi_anom_plots_fg

% uip_start_ndx               to be used when good is visi and anom is hiden    text: good_senso_per_plot
% uip_curves_per_plot_cnt 


% good_start_ndx               to be used when good is visi and anom is hiden    text: good_senso_per_plot
% senso_start_ndx      
% anom_start_ndx               to be used when good is hiden and anom is visi    text: good_anom_per_plot
% anom_curves_per_plot_cnt      

% default: split_cnt (can get less than asked for but both add up to asked cnt)
% allp_start_ndx               to be used when both visi    text: senso_per_plot        2nd_text: hiden. 2nd_edit: hiden
% allp_curves_per_plot_cnt

% allp_start_ndx               to be used when combi is on      text: senso_per_plot        2nd_text: hiden. 2nd_edit: hiden
% allp_curves_per_plot_cnt


% if (~(ui_visi_good_plots_fg))  && (~(ui_visi_anom_plots_fg))
%    % SHOWING BOTH GOOD and ANOM
%    Set title to: sensograms per plot (for the 2 text on both sides)
%    % so
%
%
%
%
% S1 do_visi_good_plots     
%  if (Not_already_hiden_good_plots)
%        % USER WANTS TO HIDE GOOD: 
%          % hide good stuff
%          set(handles. check_visi_good_plots,'check','on');
%          set(handles. text_good_senso_per_plots,'visible','off');
%          set(handles. edit_good_senso_per_plots,'visible','off');
%          set(handles. good_axes,'visible','off')         
%     if ( ui_state_struct.ui_visi_anom_plots_fg)
%        % USER WANTS TO HIDE GOOD: and ANOM is HIDEN: hide good and unhide ANOM
%          ui_state_struct.ui_visi_good_plots_fg = 1;
%        %  un-hide anom stuff
%          set(handles. check_visi_anom_plots,'check','on');
%          set(handles. text_anom_senso_per_plots,'visible','on');
%          set(handles. edit_anom_senso_per_plots,'visible','on');
%          set(handles. anom_axes,'visible','on')
%          % maximize anom plot
%     end
%     % GOOD IS HIDEN and ANOM is ON 
%     % behavior: use    anom_start_ndx if zero: init it with allp_start_ndx

% S2 do_visi_anom_plots    /     
%  if (Not_already_hiden_anom_plots)
%        % USER WANTS TO HIDE ANOM: 
%          % hide anom stuff
%          set(handles. check_visi_anom_plots,'check','on');
%          set(handles. text_anom_senso_per_plots,'visible','off');
%          set(handles. edit_anom_senso_per_plots,'visible','off');
%          set(handles. anom_axes,'visible','off')         
%     if ( ui_state_struct.ui_visi_good_plots_fg)
%        % USER WANTS TO HIDE ANOM: and GOOD is HIDEN: hide anom and unhide good
%          ui_state_struct.ui_visi_anom_plots_fg = 1;
%        %  un-hide good stuff
%          set(handles. check_visi_good_plots,'check','on');
%          set(handles. text_good_senso_per_plots,'visible','on');
%          set(handles. edit_good_senso_per_plots,'visible','on');
%          set(handles. good_axes,'visible','on')
%          % maximize good plot
%     end
%     % GOOD is ON AND ANOM IS HIDEN 
%     % behavior: use    good_start_ndx  if zero: init it with allp_start_ndx

% S3 do_visi_good_plots     
%  if (Not_already_visi_good_plots)
%          % visible good stuff
%          set(handles. check_visi_good_plots,'check','off');
%          set(handles. text_good_senso_per_plots,'visible','on');
%          set(handles. edit_good_senso_per_plots,'visible','on');
%          set(handles. good_axes,'visible','on')         
%     if ( ui_state_struct.ui_visi_anom_plots_fg)
%        % USER WANTS TO VISIBLE GOOD: and ANOM is HIDEN: hide good and unhide ANOM    INVALID STATE: Do nothing.
%          ui_state_struct.ui_visi_good_plots_fg = 1;
%        %  un-hide anom stuff
%          set(handles. check_visi_anom_plots,'check','on');
%          set(handles. text_anom_senso_per_plots,'visible','off');
%          set(handles. edit_anom_senso_per_plots,'visible','off');
%          set(handles. anom_axes,'visible','on')
%          % maximize anom plot
%     end
%     % GOOD IS HIDEN and ANOM is ON 
%     % behavior: use    anom_start_ndx if zero: init it with allp_start_ndx


% % do_visi_good_plots