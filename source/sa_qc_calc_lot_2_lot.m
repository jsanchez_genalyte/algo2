function [ qc_lot_2_lot_struct,error_message ] = sa_qc_calc_lot_2_lot( assay_sum_table,assay_ana_header )
%SA_QC_CALC_LOT_2_LOT  calculates the qc_lot_2_lot_struct for the auto qc report: lot_to_lot
%   outputs:  (note: here lot refers to spotting lot: ie spotl)
%   1. the data to produce the qc_lot_to_lot plots for each analyte and each spotl. 2d Matrix with rows and as many columns as spotl for each analyte
%      1 to 16: each analyte. and number 17 is:
%          data_aggr_mat     aggregated (by spotl) data matrix with odd and even sensors
%          third_dim:_1      odd channel
%          third_dim:_2      even channel
%          third_dim:_3      both channel
%          name_ana          name of analyte
%          col_values_cat_u  unique list of spotl for the given analyte
%          data_sum_stats    statistical summary for the given analyte (by spotl)
%   2. error message:        blank means no errors.

%   next function in pipeline: plot qc_rack_to_rack plot for each analyte and each lot: boxplot for each rack. only 1 to 5 racks
%   sample call:
%  [ qc_lot_2_lot_struct,error_message ] = sa_qc_calc_lot_2_lot( assay_sum_table,assay_ana_header );

% pipeline:   sa_do_push_top_sens.m --> sa_qc_calc_lot_2_lot.m --> sa_do_plot_ana_lot_2_lot_url

[ qc_lot_2_lot_struct,error_message ]              = aggreate_column_for_table( assay_sum_table,'spotl',assay_ana_header,'chann' );
if (~(strcmp(error_message,'')))
    return;
end
save('qc_lot_2_lot_struct.mat','qc_lot_2_lot_struct');
% Generate an HTML view of the MATLAB file.
publish('sa_do_plot_ana_lot_2_lot_url.m'...
    , 'figureSnapMethod', 'getframe'...   %   'entireFigureWindow'...
    ,'useNewFigure',true...
    ,'showCode' ,false ...
    );
% The publish command executes the code for each cell in sa_do_plot_ana_lot_2_lot_url.m, and saves the file to /html/sa_do_plot_ana_lot_2_lot_url.html.
% View the HTML file.
web('html/sa_do_plot_ana_lot_2_lot_url.html')
end % fn sa_qc_calc_lot_2_lot