function [handles ] = sa_ui_update_tos(handles)
%sa_ui_update_tos Updates the tos to be displayed in the ui after a classification is being done for 3 cases: good and anom:
%  for each case uses the proper ndx:  ie using sa_ui_update_tos_ui_cur_fit_net_ndx or sa_ui_update_tos_ui_cur_fit_net_ndx_good or sa_ui_update_tos_ui_cur_fit_net_ndx_anom
% jas_hardcoded: flags start at column 20 to col 29
% sample call: [handles ] = sa_ui_update_tos(handles);


% handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,cur_col_tos) = ...
%             cellstr(num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx,cur_col)));

        
flag_count      = 10;   %jas_temp: deppends on figure ui_table. 
flag_start_col  = 20;   %jas_temp: deppends on figure ui_table. 
if (strcmpi(handles.ui_state_struct.ui_request_flag,'g_n_a'))
    % UPDATING TOS FOR GOOD AND ANOM: First the overall flag.    
	handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,handles.tos_col_struct.clasi)= cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %  xxxx__first_of_smd __xxxx
    handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,handles.tos_col_struct.ndxdb)= ...
        num2cell((handles.ui_state_struct.ui_cur_fit_net_ndx)) ;
    % Update the 10 flags:  str2num str2double
    for cur_col=1:1:flag_count
        cur_col_tos=cur_col+flag_start_col;
        %handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,cur_col_tos) = cellstr(num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx,cur_col)));
        % feb-7-2018: to bypass classification).. quick and dirty
        if ( (~(isnumeric((handles.db_data_set_struct.sc_clasi_flag)))) && (~(isnan(handles.db_data_set_struct.sc_clasi_flag))))
            handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,cur_col_tos) = ...
                cellstr(strcat('__',num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx,cur_col)),'__'));
        end
    end
    % Update the tos in the screen:
    set(handles.ui_table_tos,'Data',handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,:),'Visible','on','Enable','on');  % save_to_handles.ui_table_tos: Data
end
if (strcmpi(handles.ui_state_struct.ui_request_flag,'g_only'))
    % UPDATING TOS FOR GOOD ondly
    handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_good,handles.tos_col_struct.clasi)= cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %  xxxx__first_of_smd __xxxx
    handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_good,handles.tos_col_struct.ndxdb)= ...
        num2cell(     handles.ui_state_struct.ui_cur_fit_net_ndx_good) ;
    % Update the 10 (flag_count) flags:  str2num str2double
    for cur_col=1:1:flag_count
        cur_col_tos=cur_col+flag_start_col;
        %handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_good,cur_col_tos) = cellstr(num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx_good,cur_col)));
        % feb-7-2018: to bypass classification).. quick and dirty
        if ( (~(isnumeric((handles.db_data_set_struct.sc_clasi_flag)))) && (~(isnan(handles.db_data_set_struct.sc_clasi_flag))))            handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_good,cur_col_tos) = ...
                cellstr(strcat('__',num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx_good,cur_col)),'__'));
        end
    end
    % Update the tos in the screen:
    set(handles.ui_table_tos,'Data',handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_good,:),'Visible','on','Enable','on');  % save_to_handles.ui_table_tos: Data
end
if (strcmpi(handles.ui_state_struct.ui_request_flag,'a_only'))
    % UPDATING TOS FOR GOOD ANOM only
	handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,handles.tos_col_struct.clasi)= cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %  xxxx__first_of_smd __xxxx    
    handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,handles.tos_col_struct.ndxdb)= ...
        num2cell(     handles.ui_state_struct.ui_cur_fit_net_ndx_anom) ;        
    % Update the 10 flags:  str2num str2double
    for cur_col=1:1:flag_count
        cur_col_tos=cur_col+flag_start_col;
        %handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,cur_col_tos) = cellstr(num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,cur_col)));
        handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,cur_col_tos) = ...
            cellstr(strcat('__',num2str(handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,cur_col)),'__'));
    end
    % Update the tos in the screen:
    % Update the tos in the screen:
    set(handles.ui_table_tos,'Data',handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx_anom,:),'Visible','on','Enable','on');  % save_to_handles.ui_table_tos: Data
end

end %fn sa_ui_update_tos

%  field1  = 'clasi';     value1  = { clasi } ;  handles.tos_col_struct.valfg
%  field2  = 'probe';     value2  = { probe } ;
%  field3  = 'dates';     value3  = { dates } ;
%  field4  = 'proto';     value4  = { proto } ;
%  field5  = 'sampl';     value5  = { sampl } ;
%  field6  = 'chipl';     value6  = { chipl } ;
%  field7  = 'exper';     value7  = { exper } ;
%  field8  = 'instr';     value8  = { instr } ;
%  field9  = 'carri';     value9  = { carri } ;
%  field10 = 'chann';     value10 = { chann } ;
%  field11 = 'clust';     value11 = { clust } ;
%  field12 = 'datat';     value12 = { datat } ;
%  field13 = 'valfg';     value13 = { valfg } ;
%  field14 = 'outfg';     value14 = { outfg } ;
%  field15 = 'error';     value15 = { error } ;
%  field16 = 'bligc';     value16 = { bligc } ;
%  field17 = 'amplc';     value17 = { ampgc } ;
%  field18 = 'shifc';     value18 = { shifc } ;
%  field19 = 'anfg1';     value19 = { anfg1 } ;

% classification flags and values: to interpret tos    good = 1
% sc_clasi_flag_struct=struct;
% sc_clasi_flag_struct.allre_ip_found_fg   =  FLAG_ 1;% 19  overall
% sc_clasi_flag_struct.swash_ip_found_fg   =  FLAG_ 2;% 20  sample wash  inflexion point found
% sc_clasi_flag_struct.ampli_ip_found_fg   =  FLAG_ 3;% 21  aplification inflexion point found

% sc_clasi_flag_struct.swash_ok_adrsq_fg   =  FLAG_ 4;% 22  sample wash  exppential_fit_ok
% sc_clasi_flag_struct.bline_ok_adrsq_fg   =  FLAG_ 5;% 23  base line    exppential_fit_ok
% sc_clasi_flag_struct.ampli_ok_adrsq_fg   =  FLAG_ 6;% 24  aplification exppential_fit_ok

% sc_clasi_flag_struct.bline_ok_mea_ra_fg  =  FLAG_ 7;% 25  base line    measurement range flag ok   -41
% sc_clasi_flag_struct.bline_ok_mea_sl_fg  =  FLAG_ 8;% 26  base line    measurement slope flag ok   -42
% sc_clasi_flag_struct.ampli_ok_mea_ra_fg  =  FLAG_ 9;% 27  aplification measurement range flag ok   -43
% sc_clasi_flag_struct.ampli_ok_mea_sl_fg  =  FLAG_10;% 28  aplification measurement slope flag ok   -44
%
%
%     need to be debuged: too messi
%     handles.tos_cell(handles.ui_state_struct.ui_cur_fit_net_ndx,1)  =  'U'; % set evertything to U. THen overwrite with G or A
%     anom_ndx = handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx,allre_ip_found_fg) == 0; % allre_ip_found_fg
%     handles.tos_cell(handles.ui_state_struct.anom_ndx,1)            =  'G';
%     anom_ndx = handles.db_data_set_struct.sc_clasi_flag(handles.ui_state_struct.ui_cur_fit_net_ndx,allre_ip_found_fg) == 1; % allre_ip_found_fg
%     handles.tos_cell(handles.ui_state_struct.anom_ndx,1)            =  'A';
