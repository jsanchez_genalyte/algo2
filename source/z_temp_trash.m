% USING SLASH AFTER tests
% using the guid for the q parameter _________________________________________________________________________________________________________
% without quotes
http://jervis-staging.genalyte.com/api/tests/start_date=2017-07-21 5:00,end_date=2017-07-21 7:00,q=b5d306cb-70a4-462a-a651-d3878bddadc4
% with quotes
http://jervis-staging.genalyte.com/api/tests/start_date='2017-07-21 5:00',end_date='2017-07-21 7:00',q='b5d306cb-70a4-462a-a651-d3878bddadc4'

% using the experiment name for the q parameter _____________________________________________________________________________________________
% without quotes
http://jervis-staging.genalyte.com/api/tests/start_date=2017-07-21 5:00,end_date=2017-07-21 7:00,q=Jervis ANA Validation
% with quotes
http://jervis-staging.genalyte.com/api/tests/start_date='2017-07-21 5:00',end_date='2017-07-21 7:00',q='Jervis ANA Validation'


% USING COMMA AFTER tests
% using the guid for the q parameter _________________________________________________________________________________________________________
% without quotes
http://jervis-staging.genalyte.com/api/tests,start_date=2017-07-21 5:00,end_date=2017-07-21 7:00,q=b5d306cb-70a4-462a-a651-d3878bddadc4
% with quotes
http://jervis-staging.genalyte.com/api/tests,start_date='2017-07-21 5:00',end_date='2017-07-21 7:00',q='b5d306cb-70a4-462a-a651-d3878bddadc4'

% using the experiment name for the q parameter _____________________________________________________________________________________________
% without quotes
http://jervis-staging.genalyte.com/api/tests,start_date=2017-07-21 5:00,end_date=2017-07-21 7:00,q=Jervis ANA Validation
% with quotes
http://jervis-staging.genalyte.com/api/tests,start_date='2017-07-21 5:00',end_date='2017-07-21 7:00',q='Jervis ANA Validation'
