function [ handles ] = ui_do_fill_tos_sens_set( handles )
%UI_DO_FILL_TOS_SENS_SET fills in the tos NOT: using the current ndx: db_cur_fit_net_ndx
%   Returns handles with the whole tos filled in.
%   see: tos_create_row

% tos_rows = handles.db_data_set_struct.rows_db_cnt{1};
% tos_col_struct = handles.tos_col_struct;

% ui_request_flag                         = handles.ui_state_struct.ui_request_flag;
% setup for the 3 cases:
% senso_start_ndx                         = handles.ui_state_struct.senso_start_ndx;        	% this ndx is relative to whatever is currently selected: db_cur_fit_net_ndx
% senso_curves_perp_cnt                   = handles.ui_state_struct.senso_curves_perp_cnt;  	%100; % sa_cfg_
% included_indices                        = find(db_cur_fit_net_ndx);
% included_len                            = length(included_indices);

handles.tos_cell(:,handles.tos_col_struct.clasi)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi); %  xxxx__first_of_smd __xxxx
handles.tos_cell(:,handles.tos_col_struct.probe)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.probe); %
handles.tos_cell(:,handles.tos_col_struct.dates)	= cellstr(datestr(str2num(char(handles.db_data_set_struct.db_metad_set_struct.dates)),'dd-mmm-yy HH:MM')); %  run date as a string.
                                                       
handles.tos_cell(:,handles.tos_col_struct.proto)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.proto); %
handles.tos_cell(:,handles.tos_col_struct.sampl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.sampl); %
handles.tos_cell(:,handles.tos_col_struct.chipl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chipl); %
handles.tos_cell(:,handles.tos_col_struct.exper)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.exper); %
handles.tos_cell(:,handles.tos_col_struct.instr)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.instr); %
handles.tos_cell(:,handles.tos_col_struct.carri)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.carri); %
handles.tos_cell(:,handles.tos_col_struct.chann)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chann); %
handles.tos_cell(:,handles.tos_col_struct.clust)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.clust); %
handles.tos_cell(:,handles.tos_col_struct.datat)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.datat); %
handles.tos_cell(:,handles.tos_col_struct.valfg)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.valfg); %
handles.tos_cell(:,handles.tos_col_struct.outfg)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.outfg); %
handles.tos_cell(:,handles.tos_col_struct.error)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.error); % xxxx__last_of_smd __xxxx
% use the current cuantities: they may be raw or either one of the normalized values.
handles.tos_cell(:,handles.tos_col_struct.bligc)    = cellstr(num2str(round(handles.db_data_set_struct.bligc))); %
handles.tos_cell(:,handles.tos_col_struct.ampgc)    = cellstr(num2str(round(handles.db_data_set_struct.ampgc))); %
handles.tos_cell(:,handles.tos_col_struct.shifc)    = cellstr(num2str(round(handles.db_data_set_struct.shifc)));  




% if (strcmpi(ui_request_flag,'g_n_a')) % str2num
%     % DOING BOTH:
%     handles.tos_cell(:,handles.tos_col_struct.clasi)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %  xxxx__first_of_smd __xxxx
%     handles.tos_cell(:,handles.tos_col_struct.probe)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.probe(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.dates)	= cellstr(datestr(str2double(char(handles.db_data_set_struct.db_metad_set_struct.dates(handles.ui_state_struct.ui_cur_fit_net_ndx))),'dd-mmm-yy HH:MM')) ; % run date as a string.
%     handles.tos_cell(:,handles.tos_col_struct.proto)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.proto(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.sampl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.sampl(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.chipl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chipl(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.exper)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.exper(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.instr)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.instr(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.carri)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.carri(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.chann)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chann(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.clust)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.clust(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.datat)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.datat(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.valfg)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.valfg(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.outfg)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.outfg(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.error)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.error(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; % xxxx__last_of_smd __xxxx
% end
% 
% if (strcmpi(ui_request_flag,'g_only'))
%     % DOING GOOD_ONLY
%     handles.tos_cell(:,handles.tos_col_struct.clasi)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %  xxxx__first_of_smd __xxxx
%     handles.tos_cell(:,handles.tos_col_struct.probe)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.probe(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.dates)	= cellstr(datestr(str2double(char(handles.db_data_set_struct.db_metad_set_struct.dates(handles.ui_state_struct.ui_cur_fit_net_ndx_good))),'dd-mmm-yy HH:MM')) ; % run date as a string.
%     handles.tos_cell(:,handles.tos_col_struct.proto)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.proto(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.sampl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.sampl(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.chipl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chipl(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.exper)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.exper(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.instr)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.instr(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.carri)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.carri(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.chann)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chann(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.clust)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.clust(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.datat)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.datat(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.valfg)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.valfg(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.outfg)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.outfg(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.error)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.error(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; % xxxx__last_of_smd __xxxx
% end
% 
% if (strcmpi(ui_request_flag,'s_only'))
%     % DOING ANOM ONLY:
%     handles.tos_cell(:,handles.tos_col_struct.clasi)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %  xxxx__first_of_smd __xxxx
%     handles.tos_cell(:,handles.tos_col_struct.probe)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.probe(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.dates)	= cellstr(datestr(str2double(char(handles.db_data_set_struct.db_metad_set_struct.dates(handles.ui_state_struct.ui_cur_fit_net_ndx_anom))),'dd-mmm-yy HH:MM')) ; % run date as a string.
%     handles.tos_cell(:,handles.tos_col_struct.proto)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.proto(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.sampl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.sampl(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.chipl)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chipl(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.exper)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.exper(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.instr)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.instr(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.carri)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.carri(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.chann)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.chann(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.clust)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.clust(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.datat)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.datat(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.valfg)	= cellstr(handles.db_data_set_struct.db_metad_set_struct.valfg(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.outfg)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.outfg(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.tos_cell(:,handles.tos_col_struct.error)    = cellstr(handles.db_data_set_struct.db_metad_set_struct.error(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; % xxxx__last_of_smd __xxxx
% end

end % fn ui_do_fill_tos_sens_set

