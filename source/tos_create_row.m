function [ tos_senso_row ] = tos_create_row()
%TABLE_OF_FITS_CREATE_ROW creates a structure with blanks values for a new row of a tos: table of sensograms.
% Output is the new empty row.
% Assumption: there are 28 columns on the tos. 
% Sample call:  [ tos_senso_row ] = toe_create_row()
% see:  tos_create, ui_do_fill_tos_sens_set  

 % tos_col_struct = handles.tos_col_struct; ui_table_of_fits   ui_table_tos
 
% Initial State values _______________________________________________________
 ndxdb = '';
 clasi = 'U';
 probe = '';
 dates = '';
 proto = '';
 sampl = '';
 chipl = '';
 exper = '';
 instr = '';
 carri = '';
 chann = '';
 clust = '';
 datat = 'Raw';
 valfg = '';
 outfg = '';
 error = '';
 bligc = nan;
 ampgc = nan;
 shifc = nan;
 anfg1 = '';
 anfg2 = '';
 anfg3 = '';
 anfg4 = '';
 
 anfg5 = '';
 anfg6 = '';
 anfg7 = '';
 anfg8 = '';
 anfg9 = '';
 anfg10= '';
 %expid = ''; % not visible.

 field0  = 'ndxdb';    value0  = { ndxdb } ;
 field1  = 'clasi';     value1  = { clasi } ;
 field2  = 'probe';     value2  = { probe } ;
 field3  = 'dates';     value3  = { dates } ; 
 field4  = 'proto';     value4  = { proto } ;
 field5  = 'sampl';     value5  = { sampl } ;
 field6  = 'chipl';     value6  = { chipl } ;
 field7  = 'exper';     value7  = { exper } ;
 field8  = 'instr';     value8  = { instr } ;
 field9  = 'carri';     value9  = { carri } ;
 field10 = 'chann';     value10 = { chann } ;
 field11 = 'clust';     value11 = { clust } ;
 field12 = 'datat';     value12 = { datat } ;
 field13 = 'valfg';     value13 = { valfg } ;
 field14 = 'outfg';     value14 = { outfg } ;
 field15 = 'error';     value15 = { error } ;
 field16 = 'bligc';     value16 = { bligc } ;
 field17 = 'amplc';     value17 = { ampgc } ;
 field18 = 'shifc';     value18 = { shifc } ;
 field19 = 'anfg1';     value19 = { anfg1 } ;
 field20 = 'anfg2';     value20 = { anfg2 } ;
 field21 = 'anfg3';     value21 = { anfg3 } ;
 field22 = 'anfg4';     value22 = { anfg4 } ;
 
 field23 = 'anfg5';     value23 = { anfg5 } ;
 field24 = 'anfg6';     value24 = { anfg6 } ;
 field25 = 'anfg7';     value25 = { anfg7 } ;
 field26 = 'anfg8';     value26 = { anfg8 } ;
 field27 = 'anfg9';     value27 = { anfg9 } ;
 field28 = 'anfg10';    value28 = { anfg10} ;
 
 %field29 = 'expid';     value29 = { expid} ; % this is the link with the toe vs tos:  maybe not here but in the dbdataset

          
 
 tos_senso_row = struct( field0  ,value0 ...
     ,field1  ,value1 ,field2 ,value2 ,field3 ,value3 ,field4 ,value4                    ...
     ,field5 ,value5 ,field6 ,value6 ,field7 ,value7 ,field8 ,value8                    ...
     ,field9 ,value9 ,field10,value10,field11,value11,field12,value12                   ...
     ,field13,value13,field14,value14,field15,value15,field16,value16,field17,value17   ...
     ,field18,value18,field19,value19,field20,value20,field21,value21,field22,value22   ...
	 ,field23,value23,field24,value24,field25,value25,field26,value26,field27,value27   ... 
     ,field28,value28);
end % fn table_of_fits_create_row

