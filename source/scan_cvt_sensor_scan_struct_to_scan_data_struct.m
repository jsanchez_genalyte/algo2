function [ scan_data_struct ] = scan_cvt_sensor_scan_struct_to_scan_data_struct( local_hd_struct,sensor_scan_struct )
%SCAN_CVT_SENSOR_SCAN_STRUCT_TO_SCAN_DATA_STRUCTT cvts into a struct easy to handle for tos
%   Uses only the first 2 cols: time and gru: Future: (present now using flag: save_qc_rpt_to_excel_req_flag)  optical data. cols 3 to 40
% notes:         this function is a clone of: sa_rest_exp_get_scan_from_hd   (need to keep the changes in sync)
% cvt: sensor_scan_struct into  fields of interest/ 

% scan_data_struct is an easy to swallow struct with 136 row arrays.
scan_data_struct = {};
scan_data_struct.sensor_cnt     = sensor_scan_struct.sensor_cnt;
scan_data_struct.scan_len_max   = sensor_scan_struct.scan_len_max;
scan_data_struct.scan_col_max   = sensor_scan_struct.scan_col_max; % (A,[sensor_scan_struct.scan_len_max ,sensor_scan_struct.scan_col_max])
if ( (sum(sum(isnan(sensor_scan_struct.sensor_data_mat))) == scan_data_struct.sensor_cnt) || ( sensor_scan_struct.scan_len_max < 3))
    % EITHER ALL NANS or NO SCAN DATA:
    scan_data_struct.scan_data_time = nan;
    scan_data_struct.scan_data_rgru = nan;
    scan_data_struct.scan_data_ngru = nan;    
else
    % DATA SEEMS TO BE OK:
    scan_data_struct.scan_data_time = reshape(sensor_scan_struct.sensor_data_mat(:,1),sensor_scan_struct.scan_len_max , []); % col 1 is time
    scan_data_struct.scan_data_rgru = reshape(sensor_scan_struct.sensor_data_mat(:,2),sensor_scan_struct.scan_len_max , []); % col 2 is rgru
    scan_data_struct.scan_data_ngru = reshape(sensor_scan_struct.sensor_data_mat(:,2),sensor_scan_struct.scan_len_max , []); % col 2 is ngru
    scan_data_struct.scan_data_time = scan_data_struct.scan_data_time'; % make it 136x64 or so ...
    scan_data_struct.scan_data_rgru = scan_data_struct.scan_data_rgru';
end
if (local_hd_struct.local_hd_save_scan_excel_flag)
   % SCAN_OPTICAL_EXCEL:  user requested saving this exp: store the whole scan matrix    
    scan_data_struct.sensor_data_mat= sensor_scan_struct.sensor_data_mat; %jas_tbd: Save FSR and other cols of interest same way as raw gru
end
% NIU: jas_dbg_garbage in data from jervis.
% % for cur_sensor_ndx =1:1:sensor_scan_struct.sensor_cnt
% %     if(         ( scan_data_struct.scan_data_time(cur_sensor_ndx,1) ==  scan_data_struct.scan_data_time(cur_sensor_ndx,scan_data_struct.scan_len_max)) ...
% %             &&  ( scan_data_struct.scan_data_rgru(cur_sensor_ndx,1) ==  scan_data_struct.scan_data_rgru(cur_sensor_ndx,scan_data_struct.scan_len_max)))
% %         fprintf('\nERROR IN LOGIC FIRST AND LAST TIME-GRU VALUES MATCH \n');
% %     end
% % end

end % fn scan_cvt_sensor_scan_struct_to_scan_data_structt

