function [clasi_res,sc_clasi_flag,sc_clasi_vals ] = sc_eval_flags( ...
    cur_senso_ndx,senso_flag_array_adrsq,clasi_res,sc_clasi_flag,sc_clasi_vals ...
    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt...
    ,sc_clasi_flag_struct,sc_clasi_vals_struct)
%SC_EVAL_FLAGS Evaluates flags and stores analog values after sensogram classi to make a final call: G (good),A(Anom),or 'F (algo Fail)
% jas_temp: using only 5 criteria: 2 ips and 3 rmsq: need to add: flatness for positives and all the logic for negatives.
% jas_temp: 12_12_2017 added criteria: flatness: 4 criterias: 2 for bl and 2 for ampli:  for each: range and slope.

% pipeline:    10.  sc_classify_sensograms -->  sc_classify_sensogram_process   --> sc_classify_sensogram_init
%                                                                               --> sc_find_going_up_ip_ndx_in_section, basic....
%                                          -->  sc_eval_flags
sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.allre_ip_found_fg)  = 2;    % 2 == unknown until proven good or anomalus. (in case or early return)
sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_cnt) =found_swash_ip_cnt;
switch found_swash_ip_cnt
    case 0
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx1) =0;
    case 1
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx1) =ip_sec_ndx_swash(1);
        sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.swash_ip_found_fg)  = 1;
    case 2
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx2) =ip_sec_ndx_swash(2);
        sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.swash_ip_found_fg)  = 1;
    case 3 %  way too many: call it bad
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx3) =ip_sec_ndx_swash(3);
    otherwise % >3 way too many: call it bad
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx1) = ip_sec_ndx_swash(1); %  way too many
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx2) = ip_sec_ndx_swash(2);
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ip_found_dx3) = ip_sec_ndx_swash(3);
end % case: found_swash_ip_cnt

sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_cnt) =found_amp_ip_cnt;
switch found_amp_ip_cnt
    case 0
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx1) =0;
    case 1
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx1) =ip_sec_ndx_amp(1);
        sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ip_found_fg)  = 1;
    case 2
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx2) =ip_sec_ndx_amp(2);
        sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ip_found_fg)  = 1;
    case 3  %  way too many: call it bad
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx3) =ip_sec_ndx_amp(3);
    otherwise % >3 way too many: call it bad
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx1) =ip_sec_ndx_amp(1); %  way too many
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx2) =ip_sec_ndx_amp(2);
        sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ip_found_dx3) =ip_sec_ndx_amp(3);
end % case: found_swash_ip_cnt

% switch found_ip_cnt
%     case 0
%         %
%     case 1
%         %
%     case 2
%         %
%     case 3
%         %
%     otherwise
%         % future handle >3 and 0 in different way
%         %
% end    % switch len_sf
%[ senso_flag_str ]  = sc_pack_senso_flags( senso_flag_array );

% jas_temp: include 5 crieria only: 2 amplif found and 3 adrsq ok.
% jas_temp: tobe added: flatness, negatives etc. ... this is just to show some
% jas_temp: basic casification of positive samples
if (senso_flag_array_adrsq(1) > 0)
    sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.swash_ok_adrsq_val) =senso_flag_array_adrsq(1);
    sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.swash_ok_adrsq_fg)  = 1;
end
if (senso_flag_array_adrsq(2) > 0)
    sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.bline_ok_adrsq_val) =senso_flag_array_adrsq(2);
    sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.bline_ok_adrsq_fg)  = 1;
end

if (senso_flag_array_adrsq(3) > 0)
    sc_clasi_vals(cur_senso_ndx,sc_clasi_vals_struct.ampli_ok_adrsq_val) =senso_flag_array_adrsq(3);
    sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ok_adrsq_fg)  = 1;
end

% 12_12_2017 Using 9 criteria to eval a sensogram: 2 ip found, 3 exp_adj_rsq ok, 4 flatness: 2 in bline and 2 in ampli
final_clasi =                                           ...
    sc_clasi_flag(  cur_senso_ndx,sc_clasi_flag_struct.swash_ip_found_fg)    ...
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ip_found_fg)    ...
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.swash_ok_adrsq_fg)    ...
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.bline_ok_adrsq_fg)    ...
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ok_adrsq_fg)    ... % next lines added: 12_12_2017: flatness
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.bline_ok_mea_ra_fg)   ... % range bline
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.bline_ok_mea_sl_fg)   ... % slope bline
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ok_mea_ra_fg)   ... % range ampli
    & sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.ampli_ok_mea_sl_fg)   ... % slope ampli
    ;
if (final_clasi)
    clasi_res = 'G'; % save clasification result.  GOOD: all flags must be a 1 to make a GOOD call. 
    sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.allre_ip_found_fg)  = 1;      % overall call: 1 is good 0 is anomalus. 2: unknown.
else
    clasi_res = 'A';
    sc_clasi_flag(cur_senso_ndx,sc_clasi_flag_struct.allre_ip_found_fg)  = 0;    
end

end % sc_eval_flags;