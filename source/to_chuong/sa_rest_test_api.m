% file: sa_rest_test_api
% Ref: Test for all the rest API calls using new methods:
% date format from Kabir:  01/20/2018   

%S1: Connect once
handles = struct;
[ error_message, handles ] = sa_rest_do_connect( handles,false );

while (isempty(error_message))
    % OK LOGIN TO REST SERIVCE
    % S1. Get all protocols:            API_2: listProtocols
    [ protocol_list_cel,  error_message ]  = sa_rest_protocol_get_all( handles.rest_token);
    if (~(isempty(error_message)))
        break;
    end
    % S2. Get one protocol details:     API_1: getProtocol:
    [ error_message,qc_protocol_struct ] = sa_rest_protocol_get_details('40d23e81-a796-4f10-9c89-f3a6d02c04ad',handles.rest_token); %
    if (~(isempty(error_message)))
        break;
    end
    % S3. Get all experiment list:      API_5: indexTests % FOLLOW UP: Debug it with John. It ERRORS_OUT. or error: exp not found       ***** (4 not needed) 
    [ exp_list_cel,  error_message ]  = sa_rest_exp_get_all('2017-07-20 5:00','2017-07-22 7:00','Jervis ANA Validation', handles.rest_token); %returns empty
    [ exp_list_cel,  error_message ]  = sa_rest_exp_get_all('2017-07-20 5:00','2017-07-22 7:00','',                      handles.rest_token); %returns 100 experiments.   
    
        [ exp_list_cel,  error_message ]  = sa_rest_exp_get_all('','','0.17M EPPS Run 4 K1A-2', handles.rest_token); %returns empty
        [ exp_list_cel,  error_message ]  = sa_rest_exp_get_all('','','hay-test-12-13-17-08', handles.rest_token); %returns empty
        
    
    if (~(isempty(error_message)))
        break;
    end
    % S4. Get 1 experiment details:     API_6: showTestMetadata
    [ error_message,exp_detail_struct ] = sa_rest_exp_get_details('b5d306cb-70a4-462a-a651-d3878bddadc4',handles.rest_token );
    if (~(isempty(error_message)))
        break;
    end
    
    % S5. Get 1 experiment probes:      API_7: showTestProbes: 36 probes with: mean,stdev,percent_cv,sensor_cnt (1 to 4)
    
    [ error_message,exp_probes_struct ] = sa_rest_exp_get_probes('b5d306cb-70a4-462a-a651-d3878bddadc4',handles.rest_token);
    if (~(isempty(error_message)))
        break;
    end
    
    % S6. Get 1 experiment sensors:     API_8: showTestSensorMetadata: 136 sensors  input: Experiment GUID
    %     returns: id(1876 to .. ?), number(1 to 136),   channel(1 or 2),  is_valid(0,1),  sensor_errors,    results( value(grue), normalized_value(norm_gru), errors etc.
    [ error_message,exp_sensor_struct ] = sa_rest_exp_get_sensors('b5d306cb-70a4-462a-a651-d3878bddadc4', handles.rest_token );
    if (~(isempty(error_message)))
        break;
    end
    
    % S7. Get 1 experiment SCAN data: 	API_3: showSensor:
    %           exp_scan_sensors:           sensor_cnt x 3        col_1 number, col_2 is_valid, col_3_scan_cnt
    %           scan_col: exp_scan_data:    sensor_cnt x 70 x 2   scan_plane_time = 1  scan_plane_gru = 2    row is sensor and col is scan sequence. assumption: scan_cnt < 70.
    
    [ error_message,exp_scan_sensors,exp_scan_data] = sa_rest_exp_get_scan(exp_sensor_struct,handles.rest_token);
    if (~(isempty(error_message)))
        break;
    end
    
    
    
    % ALL TESTS OK: Get out:
    break;
end % while no errors;
if (~(isempty(error_message)))
    fprintf(' ERROR FROM API: %s',error_message);
end