% Sample code to get the list of protocols and the details for the qc  protocol  
% see gca_jervis validation for functions to parse and handle the protocol.
[ protocol_list_cel, expe_list_cel, error_message ]  = sa_rest_get_protocol_list();
if (isempty(error_message))
 [ error_message,qc_protocol_struct ] = sa_rest_get_protocol_details(char(protocol_list_cel(22,1)));
end

% [ protocol_list, response, error_message ]  = get_protocol_list();
% protocol_list_cel =  struct2cell(protocol_list);
% protocol_name_list = protocol_list_cel(2,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_2 is name
% protocol_id_list   = protocol_list_cel(1,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_1 is id
% % qc_protocol 22: protocol_name_list(22)  
% % close to: {'85112PRT ANA PanelProtocol','c893151d-67a9-4112-a063-4bee164d40c2'} part_no 85112
% 
% 
% url_protocols_str_qc   = strcat(url_protocols_str,'/:','c893151d-67a9-4112-a063-4bee164d40c2');
% url_protocols_str_qc   = strcat(url_protocols_str,'/',protocol_id_list(22));
% qc_protocol_struct     = webread(url_protocols_str_qc{1},options);
