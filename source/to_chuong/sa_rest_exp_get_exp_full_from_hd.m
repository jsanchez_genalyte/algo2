function [ error_message,  protocol_struct,exp_det_struct,exp_probe_struct,exp_sensor_struct ] = sa_rest_exp_get_exp_full_from_hd(local_hd_struct, cur_expid)
%SA_REST_EXP_GET_EXP_FULL_FROM_HD retrieves from the hard drive the full_exp data for a single experiment (all sensors)
%   Loads a matrix of doubles:
%   col_1 is the sensor number
%   col_2 to end is the real scan data.

% see:      sa_rest_exp_save_scan  (twin function)
% pipeline: sa_rest_exp_get_scan --> sa_rest_exp_get_exp_full_from_hd
% sample call: [ error_message, sensor_data_mat ] = sa_rest_exp_get_exp_full_from_hd('01101ef0-47bd-49f9-af6b-a5cc8087e64a'); % 'cur_expid)
% notes:       this function is a clone of: scan_cvt_sensor_scan_struct_to_scan_data_structt sa_rest_exp_get_exp_full_from_hd   (need to keep the changes in sync)


error_message   	= ''; % delete('C:\Users\jsanchez\Documents\algo2\db_data_exp\*.mat')
protocol_struct     = struct;
exp_det_struct      = struct;
exp_probe_struct    = struct;
exp_sensor_struct   = struct;


db_data_exp_full_file_name = strcat(local_hd_struct.local_hd_db_exp_full_mat_dir,cur_expid,'_exp.mat');

if (~(exist(db_data_exp_full_file_name, 'file') == 2))
    % FILE DOES NOT EXIST: return
    error_message   = sprintf('file exp_full does not exist'); %  %s',db_data_exp_full_file_name);
    return;
end

% FILE ALREADY EXIST: Get scan data from the file
% old way: [ protocol_struct,exp_det_struct,exp_probe_struct, exp_sensor_struct]=load(db_data_exp_full_file_name,'-mat');
[ all_struct]=load(db_data_exp_full_file_name,'-mat');
if (isempty(all_struct) ...
        || (~(isfield(all_struct,'protocol_struct')))  ...
        || (~(isfield(all_struct,'exp_det_struct')))  ...
        || (~(isfield(all_struct,'exp_probe_struct'))) ...
        || (~(isfield(all_struct,'exp_sensor_struct'))) )
    error_message 	= sprintf('Could not load file: %s',db_data_exp_full_file_name);
    return;
else
    protocol_struct  = all_struct.protocol_struct;      %: [1�1 struct]
    exp_det_struct   = all_struct.exp_det_struct;       %: [1�1 struct]
    exp_probe_struct = all_struct.exp_probe_struct;    	%: [36�1 struct]
    exp_sensor_struct= all_struct.exp_sensor_struct;  	%: [136�1 struct]
end
%niu: sensor_cnt      = sensor_scan_struct.sensor_cnt;
% sensor_type     = sensor_scan_struct.sensor_type;            % 2 or 3 are analysis sensors: 2 is tr? and 3 is analytes?
% proid           = sensor_scan_struct.proid;
% cvt: sensor_scan_struct into 3 fields:

% scan_data_struct is an easy to swallow struct with 136 row arrays.

% exp_full_struct.protocol_struct     = protocol_struct;
% exp_full_struct.exp_det_struct      = exp_det_struct;
% exp_full_struct.exp_probe_struct    = exp_probe_struct;
% exp_full_struct.exp_sensor_struct   = exp_sensor_struct;
% exp_full_struct.scan_data_struct	= struct;

end % fn sa_rest_exp_get_exp_full_from_hd


%   % jas_tb_fix: changed 42 with 2   2018_05_01

% if (local_hd_struct.local_hd_save_scan_excel_flag)
%     % SCAN_OPTICAL_EXCEL:  user requested saving this exp: store the whole scan matrix
%     scan_data_struct.sensor_data_mat = sensor_scan_struct.sensor_data_mat; %jas_tbd: Save FSR and other cols of interest same way as raw gru
% end
%scan_data_struct.scan_data_ngru = scan_data_struct.scan_data_ngru'; % jas_temp_maybe this is not normalized gru!!!.
% niu: jas_dbg_only
% % for cur_sensor_ndx =1:1:sensor_scan_struct.sensor_cnt
% %     if(         ( scan_data_struct.scan_data_time(cur_sensor_ndx,1) ==  scan_data_struct.scan_data_time(cur_sensor_ndx,scan_data_struct.scan_len_max)) ...
% %             &&  ( scan_data_struct.scan_data_rgru(cur_sensor_ndx,1) ==  scan_data_struct.scan_data_rgru(cur_sensor_ndx,scan_data_struct.scan_len_max)))
% %         fprintf('\nERROR IN LOGIC FIRST AND LAST TIME-GRU VALUES MATCH \n');
% %     end
% % end
% exp_scan_sensors            = nan(sensor_cnt,scan_sensors_col_dim); niu

% niu: ? tbv
% scan_data_plane_time       = 1;
% scan_data_plane_gru        = 2;
% scan_data_plane_dim         = 2;
% exp_scan_data               = nan(sensor_cnt,max_scan_esti_len,scan_data_plane_dim );


%     sensor_data_mat(cur_start_row:cur_end_row,2:size(cur_sensor_data,2)+1) = cur_sensor_data;
%     sensor_data_time(cur_start_row:cur_end_row,1:size(cur_sensor_data,2))  = cur_sensor_data(:,1;

% structs:
%  stored in files:  3 integers: sensor_cnt,max_scan_len,max_scan_col,
%                    1 matrix:   scan_data_matrix   dim(sensor_cnt*max_scan_len,max_scan_col)
%  recovered    sensor_data_time   scan_data_matrix(:,1)
%  recovered    sensor_data_grur   scan_data_matrix(:,2)
%  build        sensor_data_numb   1:1:sensor_cnt          replicated
%  build        sensor_data_len    max_scan_len            replicated