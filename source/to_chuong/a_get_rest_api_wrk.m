% ref: parsing of 4 json files from john (nov-29-2017)
% con: 3 of them done. missing: sensor_details (it has tons of data that is not needed according to Chuong )
full_path_name ='C:\Users\jsanchez\Documents\algo2\from_john';

file_name_test_exp  ='tests';
file_name_test_sen  ='test_sensors';
file_name_sens_det  ='sensor_details';
file_name_test_pro  ='test_probes';

data_test_exp = loadjson([full_path_name,'\',file_name_test_exp,'.json']); % comments_john: ok. parsed and clear
data_sens_det = loadjson([full_path_name,'\',file_name_sens_det,'.json']); % comments_john: need only time and gru: need to remove most of the data in the file (lots of overhead). is this raw or normalized?
data_test_pro = loadjson([full_path_name,'\',file_name_test_pro,'.json']); % comments_john: ok. id is sensor number?. 
data_test_sen = loadjson([full_path_name,'\',file_name_test_sen,'.json']); % comments_john: ok. parsed some questions

% 1 - ok test_exp: (from John's file:)   7 of the 12 needed fields: expr_id,prot_id,inst_id,date_ch,expr_nm,carrier,chp_lt, ________________________________________
%              id %  : '3013345c-0f44-4469-82f2-ad4217fc1cc1'
%           state %  : 'complete'
%         control %  : 0
%     protocol_id %  : 'eb041d4e-627d-4b7b-b38c-2b1434a0dfd8'
%       device_id %  : 'M40'
%     specimen_id %  : {'GY00000005'  'GY00000005'} 1x2 cell
%      consumable %  : [1�1 struct]
%      created_at %  : '2017-10-11T16 %  :01 %  :10.220-07 %  :00'
%      updated_at %  : '2017-10-11T16 %  :01 %  :54.888-07 %  :00'
for k = 1:length(data_test_exp)
    P.test_exp(k).expr_id = data_test_exp{1,k}.id;
    P.test_exp(k).prot_id = data_test_exp{1,k}.protocol_id;
    P.test_exp(k).inst_id = data_test_exp{1,k}.device_id;
    P.test_exp(k).date_ch = data_test_exp{1,k}.created_at(1:10);
    P.test_exp(k).expr_nm = data_test_exp{1,k}.consumable.experiment_name;
    P.test_exp(k).carrier = data_test_exp{1,k}.consumable.carrier_serial_number;
    P.test_exp(k).chp_lt  = data_test_exp{1,k}.consumable.spotting_lot_number;
    P.test_exp(k).kit_lt  = data_test_exp{1,k}.consumable.lot_number;
end
test_exp_temp = struct2cell( P.test_exp);
test_exp = squeeze(test_exp_temp);
test_exp = test_exp';                    % cell wtih 22 experiments cols: 1 to 8: expr_id,prot_id,inst_id,date_ch,expr_nm,carrier,chp_lt,kit_lt
% 2 - ok test_sensors _______________________________________________________________________________________________________________________________
%         "id": 892,
%         "number": 12,
%         "channel": 2,
%         "is_valid": true,
%         "sensor_errors": [],
%         "results": [
%             {
%                 "value": 28.0989853,
%                 "normalized_value": 0.6646540875,
%                 "is_outlier": false,
%                 "sensor_errors": []
%             }
%         ]
%     },
% {
%         "id": 921,
%         "number": 73,
%         "channel": 2,
%         "is_valid": true, logical 0/1
%         "sensor_errors": [
%             "Result 93.1779768 has been flagged as an outlier"
%         ],
%         "results": [
%             {
%                 "value": 93.1779768,
%                 "normalized_value": 65.33867153,
%                 "is_outlier": true,                                 true_outlier_     
%                 "sensor_errors": [
%                     "Result 93.1779768 has been flagged as an outlier"
%                 ]
%             }
%         ]
%     },
for k = 1:length(data_test_sen) % 136
    P.test_sen(k).sens_id = data_test_sen{1,k}.id;                              % 1 ? meaning?
    P.test_sen(k).sens_nu = data_test_sen{1,k}.number;                          % 2   ok 1 to 136
    P.test_sen(k).channel = data_test_sen{1,k}.channel;                         % 3   ok 1 to 2
    P.test_sen(k).is_vali = data_test_sen{1,k}.is_valid;                        % 4   ok  true, or false
    P.test_sen(k).err_str = data_test_sen{1,k}.sensor_errors;                   % 5 ? empty or a cell?
    if (isempty(data_test_sen{1,k}.results ))
        P.test_sen(k).raw_gru = nan;
        P.test_sen(k).nor_gru = nan;
        P.test_sen(k).is_outl = nan;
    else
        P.test_sen(k).raw_gru = data_test_sen{1,k}.results{1}.value;            % 6   raw  gru
        P.test_sen(k).nor_gru = data_test_sen{1,k}.results{1}.normalized_value;	% 7   norm gru
        P.test_sen(k).is_outl = data_test_sen{1,k}.results{1}.is_outlier;       % 8   logical 0 /1 
    end
end
test_sen_temp       = struct2cell( P.test_sen);
test_sen            = squeeze(test_sen_temp);
test_sen            = test_sen';                    % cell with 136 x 8 fields. 
test_sen_cols       = {'sens_id','sens_nu','channel','is_vali','err_str','raw_gru','nor_gru','is_outl'};    
sens_col_pos_sens_id= 1;
sens_col_pos_sens_nu= 2;
sens_col_pos_channel= 3;
sens_col_pos_is_vali= 4;
sens_col_pos_err_str= 5;
sens_col_pos_raw_gru= 6;
sens_col_pos_nor_gru= 7;
sens_col_pos_is_outl= 8;

% 3 - ok test_probes _______________________________________________________________________________________________________________________________
%      sample:
%         "id": 220,
%         "name": "HSA 2",
%         "category": "analysis",
%         "results": [
%             {
%                 "mean": 28.7927248375,
%                 "standard_deviation": 0.38111877613313094,
%                 "percent_cv": 1.323663454167968,
%                 "sensor_count": 4
for k = 1:length(data_test_pro) % 36
    P.test_pro(k).sens_id  = data_test_pro{1,k}.id;                                 % 1 ? meaning?
    P.test_pro(k).name_ctr = data_test_pro{1,k}.name;                               % 2   ok probe name
    P.test_pro(k).category = data_test_pro{1,k}.category;                           % 3   ok I need to see how to use it.
    if (isempty(data_test_pro{1,k}.results ))
        P.test_pro(k).mea_val = nan;
        P.test_pro(k).std_val = nan;
        P.test_pro(k).prct_cv = nan;
        P.test_pro(k).sen_cnt = nan;       
    else
        P.test_pro(k).mea_val = data_test_pro{1,k}.results{1}.mean;                 % 6   mean for the probe
        P.test_pro(k).std_val = data_test_pro{1,k}.results{1}.standard_deviation;	% 7   std
        P.test_pro(k).prct_cv = data_test_pro{1,k}.results{1}.percent_cv;           % 8   cv
        P.test_pro(k).sen_cnt = data_test_pro{1,k}.results{1}.sensor_count;         % 8   sensor_cnt
    end
end
test_sen_temp           = struct2cell(P.test_pro);
test_pro                = squeeze(test_sen_temp);
test_pro                = test_pro';                    % cell with 36 probes x 7 fields 
prob_col_pos_sens_id    = 1;
prob_col_pos_name_ctr   = 2;
prob_col_pos_category   = 3;
prob_col_pos_mea_val    = 4;
prob_col_pos_std_val    = 5;
prob_col_pos_prct_cv    = 6;
prob_col_pos_sen_cnt    = 7;

