function  [error_message]  = sample_rest_get_exp(start_date,end_date,exp_name )
%SAMPLE_REST_GET_EXP retrieves the list of ALL available experiments from Jervis within the date range and a given exp_name Data Service Johns:  API_5: indexTests
% Input: start_date: if blank: means from first exp. Format: mm/dd/yyyy  DAY RESOLUTION and NO PROTOCOL NAMES!
%        end_date:   if blank: means up o current.   Format: mm/dd/yyyy
%        exp_name:   if blank: all experiments. In reallity exp_name could be a spotting lot number, or anything else....
%        

% Sample call: [error_message]  = sample_rest_get_exp('','','-ANA-QC-180' )   % ok
% Sample call: [error_message]  = sample_rest_get_exp('','','-ANA-QC-' )      % fail

% Handles 8 cases of combinations from the 3 input parameters.

% STEP_1. CONNECTION PART: _________________________________________________________________
error_message = '';
staging_flag = 1;

if (staging_flag)
    % STAGING
    url_login_str                       = 'http://jervis-staging.genalyte.com/api/auth/token';
    token_str                           = 'GL86C3700A12';                                      % token for Stagging.
    
else
    % PRODUCTION
    url_login_str                       = 'http://jervis.genalyte.com/api/auth/token';    	   % login for production.
    token_str                           = 'GL33D0C2AB50';                                      % token for production.
end


token_struct                        = struct('authorization_token',token_str);
token_json                          = jsonencode(token_struct);
options                             = weboptions('MediaType','application/json');
options.Timeout                     = 5; % default is 5 seconds
response                            = webwrite(url_login_str,token_json,options);
if ( (isfield(response,'created_at')) && (isfield(response,'expires_in')) && (isfield(response,'token')) )
    % OK RESPONSE FROM THE WEB SERVICE
    rest_token                  = response.token;
else
    % CONNECTION LOGGIN FAILED: Give up:
    error_message = sprintf('\nERROR_ WILE LOGGING TO THE REST SERVICE.');
    return;
end

% STEP_2. Request experiment data for one of 8 combinations of the input parameters:________________________________
if (staging_flag)
    % STAGING
url_experiments_str     = 'http://jervis-staging.genalyte.com/api/tests/';
else
    % PRODUCTION
url_experiments_str     = 'http://jervis.genalyte.com/api/tests/';
    
end
options                 = weboptions('HeaderFields',{'Content-Type', 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token ); 'RequestMethod', 'post';'ArrayFormat','json'});
sa_concat_size          = 10; % jas_hardcoded: Handling Up to 10x100 = 1K experiments.
api_page_size           = 100;% jas_hardcoded_rest_api_limit_from_john: each page brings 100 experiments at a time.

try
    error_message       = '';
    exp_list            = {};
    if ((~(isempty(start_date))) &&  (~(isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_1: PASSED THE 3 ARGUMENTS: Full query
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                         	= webread(url_experiments_str,'start_date',start_date,'end_date',end_date,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
    end  % case 1
    
    if ((~(isempty(start_date))) &&  ((isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_2: PASSED ONLY 2 ARGUMENTS: 1 0 1
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_experiments_str,'start_date',start_date,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
    end  % case 2
    
    if (((isempty(start_date))) &&  (~(isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_3: PASSED ONLY 2 ARGUMENTS: 0 1 1
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_experiments_str,'end_date',end_date,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
    end  % case 3
    
    if ((~(isempty(start_date))) &&  ((isempty(end_date))) &&  ((isempty(exp_name)))  )
        % CASE_4: PASSED ONLY 1 ARGUMENTS: 1 0 0
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_experiments_str,'start_date',start_date,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
    end  % case 4
    
    if (((isempty(start_date))) &&  (~(isempty(end_date))) &&  ((isempty(exp_name)))  )
        % CASE_5: PASSED ONLY 1 ARGUMENTS: 0 1 0
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_experiments_str,'end_date',end_date,options,'page',num2str(exp_page_cnt));
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
    end  % case 5
    
    if (((isempty(start_date))) &&  ((isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_6: PASSED ONLY 1 ARGUMENTS: 0 0 1 date and non-blank experiment
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_experiments_str,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
    end % case 6: empty start and end dates but not empty experiment name
    
    if (((isempty(start_date))) &&  ((isempty(end_date))) &&  ((isempty(exp_name)))  )
        % CASE_7: PASSED NO ARGUMENTS: set a warning: nothing will be returned
        error_message ='Missing filter. You need at least one. No filters would imply to request ALL experiments. Try again';
        sprintf('\n%s\n',error_message);
        return; % it will return an empty table
    end  % case 7
    
    if ((~(isempty(start_date))) &&  (~(isempty(end_date))) &&  ((isempty(exp_name)))  )
        %  CASE_8: PASSED ONLY 2 ARGUMENTS: 1 1 0 date and blank experiment
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_experiments_str,'start_date',start_date,'end_date',end_date,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
    end % case 8 Last case
catch
    % COULD NOT READ EXPERIMENTS: Failure of webread.
    fprintf('\n_error_: exception_thrown_function_ sa_rest_exp_get_all.\n');
    error_message	= sprintf('\nERROR_ RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE: during webread.  \n for start_date=%s, end_date=%s and exp_name=%s'...
        ,start_date,end_date,char(exp_name));
    return;
end

if ( (isfield(exp_list,'id')) && (isfield(exp_list,'state')) && (isfield(exp_list,'control')) )
    % OK RESPONSE FROM THE WEB SERVICE: API exp list
else
    error_message = fprintf('\nERROR_ RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE: webread.');
    return;
end

% HAVE A LIST OF EXPERIMENTS:  Get the details for each experiment, then protocol, then probe and then sensor data. ************************
if (isempty(error_message))
    fprintf(' NO-ERRORS. Retrieved: 100 exp pages:  %-4d  Last page size: % -4d',exp_page_cnt,size(exp_list,1));
    exp_list.id
end    % fn sample_rest_get_exp
