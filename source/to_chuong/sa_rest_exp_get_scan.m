function [ error_message,scan_data_struct ] = sa_rest_exp_get_scan( ...
    local_hd_struct,cur_expid,cur_proid,exp_sensor_struct,rest_token,sensor_table) % local_hd_try_first_flag,local_hd_save_scan_flag,local_hd_save_scan_excel_flag )
%SA_REST_EXP_GET_SCAN retrieves the scan details: raw scan data  of a given a_sensor_id_array for the  sensor_cnt sensors
%   assumption: None. If no experiment exist with this id: exp_scan_cell will be returned nan.
%   if it is  valid: the error_msg wil be empty
% returns:  exp_scan_sensors:  sensor_cnt x 3        col_1 number, col_2 is_valid, col_3_scan_cnt
% scan_col: gone exp_scan_data:     sensor_cnt x 70 x 2   scan_plane_time = 1  scan_plane_gru = 2    row is sensor and col is scan sequence. assumption: scan_cnt < 70.
% sample call: [ error_message,exp_scan_sensors,exp_scan_data]  = sa_rest_exp_get_scan(exp_sensor_struct,handles.rest_token);
% pipeline: sa_do_push_top_sens -->  sa_rest_db_set_get_all_data -->sa_rest_exp_get_scan

%scan_sensors_col_numbers   = 1;
%scan_sensors_col_is_vali   = 2;
%scan_sensors_col_scan_cnt  = 3;
% noiu: scan_sensors_col_dim= 3;
%scan_data_plane_time       = 1;
%scan_data_plane_gru        = 2;
% gone scan_data_plane_dim  = 2;
% jas_tbd_config: %jas_tbd: configuration_parameter: local dir to store .mat files with scan data
local_hd_try_first_flag    	= local_hd_struct.local_hd_try_first_flag;

%local_hd_struct.local_hd_db_data_expr_scan_mat_dir = 'C:\Users\jsanchez\Documents\algo2\db_data_scan\'; %jas_tbd: configuration_parameter: local dir to store .mat files with scan data

sensor_cnt                  = size(exp_sensor_struct,1);
% gone: exp_scan_sensors            = nan(sensor_cnt,scan_sensors_col_dim);
% gone: max_scan_esti_len   = 110; % to pre_allocate a reasonable size)
% gone: exp_scan_data       = nan(sensor_cnt,max_scan_esti_len,scan_data_plane_dim );
% sensor_type_key:  _bad_  1 == leak detector, 2 == Temp Detect       3 == Analyte,      4== control)
% sensor_type_key:  _okk_  1 == Analyte,       2 == leak detector,    3 == Temp Detect   4== control
% ndx_all_use               = (sensor_table.sensor_type ~= 3)  & (sensor_table.sensor_type ~= 2);  % i.e analysis sensors. ; % jas_temp_tr_tbd
ndx_all_use                 = (sensor_table.sensor_type == 1)  | (sensor_table.sensor_type == 4);  % i.e analysis sensors. ; % jas_temp_tr_tbd

% jas_temp_debug_data_scan_shift: INCLUDE_ALL TO KEEP ALIGNED WITH SCAN DATA:  test_alexa2   last: A13RZEW
ndx_all_use                 =   (sensor_table.sensor_type == 1) ...
    |                           (sensor_table.sensor_type == 4) ...
    |                           (sensor_table.sensor_type == 2) ...
    |                           (sensor_table.sensor_type == 3);  % i.e analysis sensors. ; % jas_temp_tr_tbd


% For each Sensor: API Returns: exp_scan_cell = exp_scan_cell =  struct with fields:
%                               id:          18841
%                               number:      9
%                               is_valid:    1
%                               sensor_data: [63�44 double] (I need only 2 cols: time and gru).
% gone: sensor_scan_struct      = {};
% niu: error_message           = '';
% niu: scan_data_struct        = {};

if (local_hd_try_first_flag)
    % CALLED REQUESTED TO GET DATA FROM HD IF IT IS AVAILABLE:
    [ error_message,scan_data_struct ] = sa_rest_exp_get_scan_from_hd(local_hd_struct,cur_expid); % SCAN_OPTICAL_EXCEL: % first .csv and in the future: .mat
    if (~(isempty(error_message)))
        % FILE DOES NOT EXIST: Get it from the API or the cpp app: But ...First build list of sensor ids: cpp  web_read_common_str ='http://jervis.genalyte.com/api/sensors/';
        
        % The next line: calls the cpp function to do multithread calls to the api to retrieve the scan data, and save it locally.
        [ error_message,scan_data_struct ] = sa_rest_exp_get_scan_from_db(local_hd_struct,sensor_cnt,rest_token,cur_expid,cur_proid,exp_sensor_struct,sensor_table.sensor_type); % SCAN_OPTICAL_EXCEL: % *** CPP Multithread: BOTTLENECK SOLUTION ****
        fprintf('Done retrieving scan data for the 136 sensors per experiment using cpp-mt');
    end
else
    % CALLED REQUESTED TO GET DATA FROM API: Independiently of whether it exists locally or not (usefull for dbg but very slow: 1 min).
    [ error_message,scan_data_struct ] = sa_rest_exp_get_scan_from_db(local_hd_struct, sensor_cnt,rest_token,cur_expid,cur_proid,exp_sensor_struct,sensor_table.sensor_type);% SCAN_OPTICAL_EXCEL: % *** CPP Multithread: BOTTLENECK SOLUTION ****
    
    % ASSUMED:  OK RESPONSE FROM THE WEB SERVICE
    
    
    % HAVE: scan_data_struct from the HD or from the API:  This is the struct to be returned
    
    % jas: here thursday eve: decide what to do with: scan_data_struct
    
    % cpp call: exp_scan_cell{cur_sensor_ndx}	= webread('http://jervis.genalyte.com/api/sensors/2793847'
    % Make sure we return only the correct size: use the largest scan as reference
    % % gone next lines:     scan_len_max = nanmax(exp_scan_sensors(:,3));
    % %     if(scan_len_max < max_scan_esti_len)
    % %         % PRE-ALLOCATE SIZE was too long
    % %         exp_scan_data = exp_scan_data(:,1:scan_len_max,:);
    % %     end
    
end % using API to get the scan data

%jas_optics: At this point we should have: .scan_data_struct.sensor_data_mat The matrix with the full scan.

% Independiently of how it was retrieved: Trim out to keep only analysis sensors:
% return only the rows for analysis:
%     exp_scan_data    = exp_scan_data(   ndx_all_use,:,:);

%exp_scan_sensors = exp_scan_sensors(ndx_all_use,:);
scan_data_struct.sensor_cnt     = sum(ndx_all_use);
if ( (isfield(scan_data_struct,'scan_data_time')) && ( ~(sum(sum(isnan(scan_data_struct.scan_data_time))))))
    scan_data_struct.scan_data_time = scan_data_struct.scan_data_time(ndx_all_use,:);
else
    scan_data_struct.scan_data_time = nan;
end
if ( (isfield(scan_data_struct,'scan_data_rgru')) && ( ~(sum(sum(isnan(scan_data_struct.scan_data_rgru))))))
    scan_data_struct.scan_data_rgru = scan_data_struct.scan_data_rgru(ndx_all_use,:);
else
    scan_data_struct.scan_data_rgru = nan;
end
if ( (isfield(scan_data_struct,'sensor_type')) && ( ~(sum(sum(isnan(scan_data_struct.sensor_type))))))
    scan_data_struct.sensor_type    = scan_data_struct.sensor_type(ndx_all_use);
else
    scan_data_struct.sensor_type    = nan;
end
end % fn sa_rest_exp_get_scan

%    for: Time required to retrieve scan data for the 136 sensors per experiment =  87.9 secs
% parfor: Time required to retrieve scan data for the 136 sensors per experiment =  44.5 secs


% ORIG CODE BEFORE PARFOR:  change parfor to for and you get the linear version:
%
% exp_scan_cell  = struct;
% error_message       = '';
% % ASSUMED:  OK RESPONSE FROM THE WEB SERVICE
%     options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )});
%     cur_sen_ndx_use = 0;
%     ndx_all_use = (sensor_table.sensor_type ~= 2) & (sensor_table.sensor_type ~= 3);
%     parfor cur_sensor_ndx = 1:1:sensor_cnt
%         if ( (sensor_table.sensor_type(cur_sensor_ndx) == 2) || (sensor_table.sensor_type(cur_sensor_ndx) == 3))
%             continue;
%         end
%         cur_sen_ndx_use         = cur_sen_ndx_use + 1;
%         url_str                 = sprintf('http://jervis-staging.genalyte.com/api/sensors/%-3d',exp_sensor_struct(cur_sensor_ndx).id    );
%         url_str                 = sprintf('http://jervis.genalyte.com/api/sensors/%-3d',exp_sensor_struct(cur_sensor_ndx).id    );
%         % without: try/catch: exp_scan_cell 	= webread(url_str,options);
%         try
%             error_message       = '';
%             exp_scan_cell     = nan;
%             exp_scan_cell 	= webread(url_str,options);   % *** BOTTLENECK ****
%         catch
%             % COULD NOT READ SCAN DATA FOR THIS EXPERIMENT
%             error_message       = sprintf('\nERROR_ WHILE READING experiment scan from API: \nWEBREAD error. Function: sa_rest_exp_get_scan for %s sensor: %d NOT FOUND',exp_sensor_struct(cur_sensor_ndx).id );
%             display(error_message);
%             return;
%         end
%         if ( (isfield(exp_scan_cell,'id')) && (isfield(exp_scan_cell,'number')) && (isfield(exp_scan_cell,'is_valid')) && (isfield(exp_scan_cell,'sensor_data')) )
%             % OK RESPONSE FROM THE WEB SERVICE: API sensors
%             exp_scan_sensors(cur_sensor_ndx,scan_sensors_col_numbers)   =  exp_scan_cell.number;
%             exp_scan_sensors(cur_sensor_ndx,scan_sensors_col_is_vali)   =  exp_scan_cell.is_valid;
%             scan_len                                                    =  size( exp_scan_cell.sensor_data,1);
%             exp_scan_sensors(cur_sensor_ndx,scan_sensors_col_scan_cnt)  =  scan_len;
%             if (scan_len)
%                 % jas_tbd: investigate what to do when scan_len is zero: ie no scan data?
%                 exp_scan_data(cur_sensor_ndx,1:scan_len,scan_data_plane_time)= exp_scan_cell.sensor_data(1:scan_len,1)'; % col_1 is time
%                 exp_scan_data(cur_sensor_ndx,1:scan_len,scan_data_plane_gru )= exp_scan_cell.sensor_data(1:scan_len,2)'; % col_2 is gru
%             end
%         else
%             error_message = sprintf(...
%                 '\nERROR_ RETRIEVING THE EXPERIMENT DETAILS FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.');
%         end
%     end % for each sensor

%2nd:                                                  'http://jervis.genalyte.com/api/sensors/2793854
% cpp_call:   exp_scan_cell{cur_sensor_ndx}= webread('http://jervis.genalyte.com/api/sensors/2793847',options);
% cpp_call:   weboptions with properties:
%
%       CharacterEncoding: 'auto'
%               UserAgent: 'MATLAB 9.1.0.441655 (R2016b)'
%                 Timeout: 5
%                Username: ''
%                Password: ''
%                 KeyName: ''
%                KeyValue: ''
%             ContentType: 'auto'
%           ContentReader: []
%               MediaType: 'application/x-www-form-urlencoded'
%           RequestMethod: 'auto'
%             ArrayFormat: 'csv'
%            HeaderFields: {2�2 cell}
%     CertificateFilename: 'C:\Program Files\MATLAB\R2016b\sys\certificates\ca\rootcerts.pem'


% https://undocumentedmatlab.com/blog/explicit-multi-threading-in-matlab-part3


% To create multi-threaded MEX, all it takes is:
% 1 - connect the thread-enabled C/C++ code into our mexFunction(),
% 2 -provide the relevant threading library to the mex linker and we�re done.


% monday_link_from_mathworks: THE BEST:  https://www.mathworks.com/matlabcentral/fileexchange/52848-matlab-support-for-mingw-w64-c-c++-compiler?s_tid=answers_rc2-2_p5_MLT
% downloaded: git-cloneed but it has to be built: git clone git://git.code.sf.net/p/mingw-w64/mingw-w64 mingw-w64-mingw-w64
% page:       https://sourceforge.net/p/mingw-w64/mingw-w64/ci/v4.x/tree/

% source for 9.2 downloaded from: https://sourceforge.net/projects/mingw-w64/files/Toolchain%20sources/Personal%20Builds/mingw-builds/4.9.2/
% but i need to figure out how to build it.





% mingw download: https://sourceforge.net/projects/mingw-w64/?source=typ_redirect

% MinGW 5.3 C/C++ (Distributor: mingw-w64) compatible with ml 2017

% to test mingw: https://en.wikibooks.org/wiki/C%2B%2B_Programming/Examples/Hello_world
% env: MW_MINGW64_LOC C:\mingw-w64\i686-7.2.0-posix-dwarf-rt_v5-rev1\mingw32\
% path:               C:\mingw-w64\i686-7.2.0-posix-dwarf-rt_v5-rev1\mingw32\

% mex tutorial:
% http://www.shawnlankton.com/2008/03/getting-started-with-mex-a-short-tutorial/

% % % conclusion:
% % % makefile issue:
% % % https://www.mathworks.com/matlabcentral/answers/163301-error-with-codegen-mex-error-status-code-2
% % % https://www.mathworks.com/matlabcentral/answers/163264-matlab-coder-and-mex-file-generation-problem-on-linux#answer_159475
% % %


% MATLAB Coder and MEX-file generation problem on windows  Linux

% C:\Program Files\MATLAB\R2016b\toolbox\coder\coder\mex\c\mex_mingw.mk
% mex -setup     % to see the setup
% mex -setup C++ % to set it to c++
% gcc.exe (i686-posix-dwarf-rev1, Built by MinGW-W64 project) 7.2.0

% for R2016b: Requires MinGW 4.9.2 (at least) will be replaced by a newer version in a future release

% From mathworks: MATLAB only supports specific versions of MinGW.
% From mathworks: R2017b supports MinGW 5.3, while
% From mathworks: R2017a and prior support MinGW 4.9.2.

% % Building with 'MinGW64 Compiler (C)'.
% % C:\mingw-w64\i686-7.2.0-posix-dwarf-rt_v5-rev1\mingw32\bin\gcc -c -DMX_COMPAT_32   -m64 -DMATLAB_MEX_FILE  -I"C:\Program Files\MATLAB\R2016b/extern/include" -I"C:\Program Files\MATLAB\R2016b/simulink/include" -I"C:\Program Files\MATLAB\R2016b/extern\lib\win64\mingw64" -fexceptions -fno-omit-frame-pointer -O -DNDEBUG C:\Users\jsanchez\Documents\algo2\mex_example\source\yprime.c -o C:\Users\jsanchez\AppData\Local\Temp\mex_1874865934792643_12600\yprime.obj
% % Error using mex
% % The command 'C:\mingw-w64\i686-7.2.0-posix-dwarf-rt_v5-rev1\mingw32\bin\gcc' exited with a return value '2'
% %

% Generated compiler/linker cmd:

% % C:\mingw-w64\i686-7.2.0-posix-dwarf-rt_v5-rev1\mingw32\bin\gcc
% % -c
% % -DMX_COMPAT_32
% % -m64
% % -DMATLAB_MEX_FILE
% % -I"C:\Program Files\MATLAB\R2016b/extern/include"
% % -I"C:\Program Files\MATLAB\R2016b/simulink/include"
% % -I"C:\Program Files\MATLAB\R2016b/extern\lib\win64\mingw64"
% % -fexceptions -fno-omit-frame-pointer
% % -O
% % -DNDEBUG C:\Users\jsanchez\Documents\algo2\mex_example\source\yprime.c
% % -o C:\Users\jsanchez\AppData\Local\Temp\mex_1874865934792643_12600\yprime.obj   % ok got created

% mex is ok with the mcr: https://www.mathworks.com/help/compiler/unsupported-functions.html


% mex tutorial nice:  http://www.shawnlankton.com/wp-content/uploads/files/mex_tutorial/demo.cpp

% mingw tutorial: http://www.mingw.org/wiki/mingw_for_first_time_users_howto



% ser_summary:
% Mingw   4.9.2 source:  https://github.com/mathworks/MinGW_492/tree/0fe14a9f4a3d47f16bc2cbb99d7cf274272e620d

