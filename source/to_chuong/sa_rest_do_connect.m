function [ error_message,handles ] = sa_rest_do_connect( handles,force_reconnect_flag )
%SA_REST_DO_CONNECT  connects to rest_service for the first time, and reconnects ONLY if time has expired or a forced reconnect is requested
%   Returns the handles.rest_token needed by every API connection. Hides the connection details from the rest of the API
%   Hardcoded_here: jas_hardcoded stagging vs production.
%   sample call: [ error_message,handles ] = sa_rest_do_connect( handles,true ); or  
%   sample call: [ error_message, handles ] = sa_rest_do_connect( handles,false );

% prod_flag     = 1; %  1 == connect to production API       0 == connect to staging API
[ url_struct ]  = sa_set_production_vs_stagging_url_flag();
url_login_str   = url_struct.url_login_str;
token_str       = url_struct.token_str; 
error_message   = ''; % blank == no_errors
cur_time        = datetime('now');
if (force_reconnect_flag || (~(isfield(handles,'rest_token'))))
    % FIRST TIME CALLED OR USER WANTS TO FORCE A RECONNECT: When something is wrong and connection got lost
    token_struct                        = struct('authorization_token',token_str);
    token_json                          = jsonencode(token_struct);
    options                             = weboptions('MediaType','application/json'); % jas_debug_timeout
    options.Timeout = 25; % default is 5 seconds  % jas_debug_timeout
    response                            = webwrite(url_login_str,token_json,options);    
    if ( (isfield(response,'created_at')) && (isfield(response,'expires_in')) && (isfield(response,'token')) )
        % OK RESPONSE FROM THE WEB SERVICE
        handles.rest_token                  = response.token;
        handles.connection_start_time_str   = cur_time;
        handles.connection_expire_time_str  = handles.connection_start_time_str + seconds(response.expires_in);
        
        %fprintf('Time required to connect = %s',connection_respond_time_msg);
    else
        % CONNECTION LOGGIN FAILED: Do a second try:
        response   = webwrite(url_login_str,token_json,options);
        if ( (isfield(response,'created_at')) && (isfield(response,'expires_in')) && (isfield(response,'token')) )
            % OK RESPONSE FROM THE WEB SERVICE
            handles.rest_token                  = response.token;
            handles.connection_start_time_str   = cur_time;
            handles.connection_expire_time_str  = handles.connection_start_time_str + seconds(response.expires_in);
        else
            % FAILED AGAIN: Give up:
            error_message = sprintf('\nERROR_ WILE LOGGING TO THE REST SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.');
        end
    end
end

if ((isfield(handles,'rest_token')) && (cur_time > handles.connection_expire_time_str))
    % ALRREAD LOGGED IN AND EXPIRED TIME: Log in again and reset variables:
    % Login first:
    %url_login_str       = 'http://jervis-staging.genalyte.com/api/auth/token';   % staging url. For % production   https://jervis.genalyte.com
    %token_str           = 'GL86C3700A12';                                        % token for Staging.
    token_struct        = struct('authorization_token',token_str);
    token_json          = jsonencode(token_struct);
    options             = weboptions('MediaType','application/json'); % jas_debug_timeout
    options.Timeout = 25; % default is 5 seconds  % jas_debug_timeout    
    response            = webwrite(url_login_str,token_json,options);    
    handles.connection_start_time_str  = datetime('now');
    handles.connection_expire_time_str = handles.connection_start_time_str + seconds(response.expires_in);    
    handles.rest_token  = response.token;
else    
    % LOGGED IN AND WITHIN TIME: Do nothing
%     fprintf('\nAlready logged and time has not expired: \nConnected at:   %s\nWill Expire at: %s\n'...
%     ,handles.connection_start_time_str,handles.connection_expire_time_str);
end
end % fn sa_rest_do_connect

