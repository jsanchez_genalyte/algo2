function [exp_full_struct,error_message  ] = sample_rest_get_all_data(exp_id)
%SAMPLE_REST_GET_ALL_DATA fills in a exp_full_struct from the api given the exp_id
%   returns: an error message and the exp_full_struct  for the experiment: experiment_details, protocol_details, probe and sensor data.

%  [exp_full_struct,error_message  ] = sample_rest_get_all_data('513d1210-9b58-487c-8f92-39a6c6210e04' )

exp_full_struct                         = {};
error_message                           = '';
staging_flag                            = 1;

if (staging_flag)
    % STAGING
    url_login_str                       = 'http://jervis-staging.genalyte.com/api/auth/token';
    token_str                           = 'GL86C3700A12';                                      % token for Stagging.
else
    % PRODUCTION
    url_login_str                       = 'http://jervis.genalyte.com/api/auth/token';    	   % login for production.
    token_str                           = 'GL33D0C2AB50';                                      % token for production.
end

% STEP_1. CONNECTION PART: _________________________________________________________________
token_struct                        = struct('authorization_token',token_str);
token_json                          = jsonencode(token_struct);
options                             = weboptions('MediaType','application/json');
options.Timeout                     = 5; % default is 5 seconds
response                            = webwrite(url_login_str,token_json,options);
if ( (isfield(response,'created_at')) && (isfield(response,'expires_in')) && (isfield(response,'token')) )
    % OK RESPONSE FROM THE WEB SERVICE
    rest_token                      = response.token;
else
    % CONNECTION LOGGIN FAILED: Give up:
    error_message = sprintf('\nERROR_ WILE LOGGING TO THE REST SERVICE.');
    return;
end


% STEP_2. EXP_DETAILS PART: _________________________________________________________________
if (staging_flag)
    url_str_exp_det                = sprintf('http://jervis-staging.genalyte.com/api/tests/%s/metadata',char(exp_id));
else
    url_str_exp_det                = sprintf('http://jervis.genalyte.com/api/tests/%s/metadata',char(exp_id));
end
% ASSUME: OK RESPONSE FROM THE WEB SERVICE
test_metad_struct    	= struct;
test_metad_struct.status= '';     % string     % John: who fills the status? is it input or output?
test_metad_struct.errors= [ 0 0 ]; % vect int  %       same for errors ?

test_metad_struct_inst 	= test_metad_struct;
% Done creating body parameter.
test_metad_inst_json   	= jsonencode(test_metad_struct_inst);
options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )});

try
    exp_det_struct  	= webread(url_str_exp_det,'json',test_metad_inst_json,options);
catch
    % COULD NOT READ EXP DETAILS
    error_message       = sprintf('\nERROR_ WHILE READING experiment details from API: \nWEBREAD error. for Experiment: %s NOT FOUND',exp_id);
    return;
end

% STEP_3. PROTOCOL PART: _________________________________________________________________

if (staging_flag)
    %STAGING
    url_protocols_str       = 'http://jervis-staging.genalyte.com/api/protocols';
else
    % PRODUCTION
    url_protocols_str       = 'http://jervis.genalyte.com/api/protocols';
end

options                 =  weboptions('HeaderFields',{'Content-Type' 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token )});

url_protocols_str_qc    = strcat(url_protocols_str,'/',exp_det_struct.protocol_id);
try
    protocol_struct     = webread(url_protocols_str_qc,options);
catch
    % COULD NOT READ PROTOCOL LISTS
    error_message       = fprintf('\nERROR_ WHILE READING Protocol details from API: WEBREAD error. Function: sa_rest_protocol_get_details for: %s',prot_id);
    display(error_message);
    return;
end
if ( ((isfield(protocol_struct,'id')) && (isfield(protocol_struct,'name')) && (isfield(protocol_struct,'part_number')) ) && (~(isempty(protocol_struct.name))) )
    % OK RESPONSE FROM THE WEB SERVICE: API protocol details
else
    % FAILED: Report error and get out
    error_message = fprintf(...
        '\nERROR_ RETRIEVING THE PROTOCOL DETAILS FROM THE WEB SERVICE.');
    return;
end

% STEP_4. PROBE PART:       _________________________________________________________________
test_metad_struct = struct;
test_metad_struct.status = '';     % string     % John: who fills the status? is it input or output?
test_metad_struct.errors = [ 0 0 ]; % vect int  %       same for errors ?

test_metad_struct_inst 	= test_metad_struct;
% Done creating body parameter: id = 'b5d306cb-70a4-462a-a651-d3878bddadc4' for test(5)  if exists it returns metadata for a test, otherwise errors out (try block)
test_metad_inst_json   	= jsonencode(test_metad_struct_inst);
options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )});

if (staging_flag)
    % STAGING
    url_str_probes                 = sprintf('http://jervis-staging.genalyte.com/api/tests/%s/probes',exp_id);
else
    % PRODUCTION
    url_str_probes                 = sprintf('http://jervis.genalyte.com/api/tests/%s/probes',exp_id);
end

try
    exp_probes_struct 	= webread(url_str_probes,'json',test_metad_inst_json,options);
catch
    % COULD NOT READ PROTOCOL LISTS
    error_message       = sprintf('\nERROR_ WHILE READING experiment probes from API: \nWEBREAD error. While getting  probes for Experiment: %s NOT FOUND',exp_id);
    return;
end
if ( (isfield(exp_probes_struct,'id')) && (isfield(exp_probes_struct,'name')) && (isfield(exp_probes_struct,'results')) )
    % OK RESPONSE FROM THE WEB SERVICE: API protocol details
else
    error_message = sprintf(...
        '\nERROR_ RETRIEVING THE EXPERIMENT DETAILS FROM THE WEB SERVICE. ');
    return;
end
% STEP_5. SENSOR PART:      _________________________________________________________________
%     Get cur experiment sensors:
options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )});

if (staging_flag)
    % STAGING
    url_str_sensors               = sprintf('http://jervis-staging.genalyte.com/api/tests/%s/sensors',exp_id);
else
    % PRODUCTION
    url_str_sensors                = sprintf('http://jervis.genalyte.com/api/tests/%s/sensors',exp_id);
end
 
try
    error_message       = '';
    exp_sensor_struct 	= webread(url_str_sensors,options);
catch
    % COULD NOT READ SENSORS FOR THIS EXPERIMENT
    error_message       = sprintf('\nERROR_ WHILE READING experiment sensors from API: \nWEBREAD error. While getting sensors for Experiment: %s NOT FOUND',exp_id);
    return;
end
if ( (isfield(exp_sensor_struct,'id')) && (isfield(exp_sensor_struct,'number')) && (isfield(exp_sensor_struct,'channel')) && (isfield(exp_sensor_struct,'results')) )
    % OK RESPONSE FROM THE WEB SERVICE: API sensors
else
    error_message = sprintf('\nERROR_ RETRIEVING THE SENSOR DETAILS FROM THE WEB SERVICE. \n');
end
if (~(isempty(error_message)))
    return;
end



% HAVE THE DATA FROM THE API
exp_full_struct.protocol_struct    = protocol_struct;
exp_full_struct.exp_det_struct     = exp_det_struct;
exp_full_struct.exp_probes_struct   = exp_probes_struct;
exp_full_struct.exp_sensor_struct  = exp_sensor_struct;

% ALL DATA FOR EXPERIMENT IS GOOD: Move on.
status_msg = sprintf('Experiment Retrieval data is fine ');
fprintf( '\n%s\n',status_msg);

% AT THIS POINT I SHOULD HAVE: exp,probe and sensor data, from the cloud API  stored in exp_full_struct
end % fn sample_rest_get_all_data
