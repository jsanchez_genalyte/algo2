function [ error_message,exp_probes_struct ] = sa_rest_exp_get_probes( a_exp_id,rest_token ) % JOHN: Seems to work fine.  API_7: showTestProbes
%SA_REST_EXP_GET_PROBES retrieves the probes of a given a_exp_id
%   assumption: None. If no experiment exist with this id: exp_probes_struct will be returned nan. 
%   assumption: prior call to: [ error_message,handles ] = sa_rest_do_connect( handles,true );
%   if it is  valid: the error_msg wil be empty      

% sample call: [ error_message,exp_probes_struct ] = sa_rest_exp_get_probes('b5d306cb-70a4-462a-a651-d3878bddadc4',handles.rest_token) 
% jas_dbg: probes_table =  struct2table(exp_probes_struct);


exp_probes_struct   = struct;
error_message       = '';
% [ handles ] = sa_rest_do_connect( handles,false );
    % ASSUMED: OK RESPONSE FROM THE WEB SERVICE
    test_metad_struct = struct;
    test_metad_struct.status = '';     % string     % John: who fills the status? is it input or output?
    test_metad_struct.errors = [ 0 0 ]; % vect int  %       same for errors ?
    
    test_metad_struct_inst 	= test_metad_struct;
    % Done creating body parameter: id = 'b5d306cb-70a4-462a-a651-d3878bddadc4' for test(5)  if exists it returns metadata for a test, otherwise errors out (try block)
    test_metad_inst_json   	= jsonencode(test_metad_struct_inst);
    
    options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )}); % jas_debug_timeout
    options.Timeout         = 25; % jas_debug_timeout        
    
    [ url_struct ]          = sa_set_production_vs_stagging_url_flag(); % prod_stag_flag );
    url_probes_str          = url_struct.url_probes_str;
    
%     url_probes_str      	= sprintf('h ttp://jervis-staging.genalyte.com/api/tests/%s/probes',a_exp_id);
%     url_probes_str        = sprintf('h ttp://jervis.genalyte.com/api/tests/%s/probes',a_exp_id);    

    url_probes_str_full 	= strcat(url_probes_str,sprintf('%s/probes',a_exp_id));
    
    try
        error_message       = '';
        exp_probes_struct   = nan;
        exp_probes_struct 	= webread(url_probes_str_full,'json',test_metad_inst_json,options);        
    catch
        % COULD NOT READ PROTOCOL LISTS
        error_message       = sprintf('\nERROR_ WHILE READING experiment probes from API: \nWEBREAD error. Function: sa_rest_exp_get_probes for Experiment: %s NOT FOUND',a_exp_id);
        %display(error_message);
        return;
    end
    if ( (isfield(exp_probes_struct,'id')) && (isfield(exp_probes_struct,'name')) && (isfield(exp_probes_struct,'results')) )
        % OK RESPONSE FROM THE WEB SERVICE: API protocol details
    else
        error_message = sprintf(...
            '\nERROR_ RETRIEVING THE EXPERIMENT DETAILS FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: ');
    end
end % fn sa_rest_exp_get_probes



