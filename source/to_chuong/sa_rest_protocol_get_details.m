function [ error_message,protocol_struct ] = sa_rest_protocol_get_details( a_protocol_id,rest_token )
%SA_REST_PROTOCOL_GET_DETAILS retrieves the details of a given protocol_id   API_1: getProtocol:
%   assumption: the protocol_id must be a string with a valid protocol.
%   it is not valid: an error message will be returned.
%   if it is  valid: the error_msg wil be empty      a_protocol_id = '40d23e81-a796-4f10-9c89-f3a6d02c04ad' % this protocol is in the list of protocols. OK 7: 'Salt 1 Step - 5 Point Curve Protocol (Fixed)'
% see: 
% sa_rest_exp_get_all.m           
% sa_rest_exp_get_details.m       
% sa_rest_exp_get_probes.m        
% sa_rest_exp_get_scan.m          
% sa_rest_exp_get_sensors.m       
% sa_rest_protocol_get_all.m        
% sa_rest_protocol_get_details.m  this one.

% sample call: [ error_message,protocol_struct ] = sa_rest_protocol_get_details( protocol_id_list(22)),rest_token;
% sample call: [ error_message,protocol_struct ] = sa_rest_protocol_get_details('40d23e81-a796-4f10-9c89-f3a6d02c04ad',handles.rest_token) % this one does  show in protocol_id list  !!!!!
% ana    call: [ error_message,protocol_struct ] = sa_rest_protocol_get_details(  protocol_list{23,1},handles.rest_token )

protocol_struct     = struct;
error_message       = '';
url_protocols_str 	= 'http://jervis-staging.genalyte.com/api/protocols'; % staging % jas_temp_hard_coded
url_protocols_str 	= 'http://jervis.genalyte.com/api/protocols';         % production % jas_temp_hard_coded
% For Protocols: Add a request header parameter with the following key and values
options =  weboptions('HeaderFields',{'Content-Type' 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token )}); % jas_debug_timeout 
options.Timeout         = 25; % jas_debug_timeout    
url_protocols_str_qc  = strcat(url_protocols_str,'/',a_protocol_id);
try
    error_message       = '';
    protocol_struct  = nan;
    protocol_struct	= webread(url_protocols_str_qc,options);
catch
    % COULD NOT READ PROTOCOL LISTS
    error_message       = fprintf('\nERROR_ WHILE READING Protocol details from API: WEBREAD error. Function: sa_rest_protocol_get_details for: %s',a_protocol_id);
    display(error_message);
    return;
end
if ( ((isfield(protocol_struct,'id')) && (isfield(protocol_struct,'name')) && (isfield(protocol_struct,'part_number')) ) && (~(isempty(protocol_struct.name))) )
    % OK RESPONSE FROM THE WEB SERVICE: API protocol details
else
    % FAILED: Report error and get out
    error_message = fprintf(...
        '\nERROR_ RETRIEVING THE PROTOCOL DETAILS FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: ');
end
end % fn sa_rest_protocol_get_details

% reci =  struct2table(protocol_struct.recipe.steps)
%     step        comment         row    column    flow_rate    duration    scan 
%     ____    ________________    ___    ______    _________    ________    _____
% 
%     '1'     'Running Buffer'    '2'    '1'       '40'         '60'        false
%     '2'     'Running Buffer'    '2'    '1'       '40'         '30'        true 
%     '3'     'Sample'            '1'    '1'       '30'         '180'       true 
%     '4'     'Running Buffer'    '3'    '1'       '40'         '120'       true 
%     '5'     'Detection'         '4'    '1'       '30'         '210'       true 
%     '6'     'Running Buffer'    '3'    '1'       '40'         '60'        true 
%     '7'     'Air'               '5'    '1'       '60'         '15'        false
    


% [ protocol_list, response, error_message ]  = get_protocol_list();
% protocol_list_cel =  struct2cell(protocol_list);
% protocol_name_list = protocol_list_cel(2,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_2 is name
% protocol_id_list   = protocol_list_cel(1,1:end)'; % cell:  28x1 list of names of 28 protocols.  field_1 is id
% qc_protocol 22: protocol_name_list(22)
% close to: {'85112PRT ANA PanelProtocol','c893151d-67a9-4112-a063-4bee164d40c2'} part_no 85112


% url_protocols_str_qc   = strcat(url_protocols_str,'/','c893151d-67a9-4112-a063-4bee164d40c2');
% url_protocols_str_qc  = strcat(url_protocols_str,'/',protocol_id_list(22));
% protocol_struct    = webread(url_protocols_str_qc{1},options);



