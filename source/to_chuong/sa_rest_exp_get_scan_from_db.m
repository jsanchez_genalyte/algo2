function [ error_message,scan_data_struct ] = sa_rest_exp_get_scan_from_db( local_hd_struct,sensor_cnt,rest_token,cur_expid,cur_proid,exp_sensor_struct,sensor_type) %  save_to_hd_flag,save_qc_rpt_to_excel_req_flag ) %) % jas_domingo
%SA_REST_EXP_GET_SCAN_FROM_DB retrieves from the DB  the scan data for a single experiment (all sensors)
%   Returns scan_data_struct: 3 integers and a matrix of doubles.
%   3 integers: sensor_count, scan_len_max, scan_col_max  (col_1 is the sensor number now is implicit)
%   col_1 to end is the real scan data.

% see:      sa_rest_exp_save_scan  (twin function) and sa_rest_exp_get_scan_from_hd
% pipeline: sa_rest_exp_get_scan --> sa_rest_exp_get_scan_from_db
% sample call: [ error_message, scan_data_struct ] = sa_rest_exp_get_scan_from_hd('01101ef0-47bd-49f9-af6b-a5cc8087e64a'); % 'cur_expid)

% NOTE: The code of this function will be commented out and replaced with the cpp call.
error_message           = '';
scan_data_struct        = {};
fake_cpp_call           = true;
[ url_struct ]          = sa_set_production_vs_stagging_url_flag(); % prod_stag_flag );
url_scan_str            = url_struct.url_scan_str;

if (fake_cpp_call)
    % FAKE CPP CALL: Do a API DB call:
    %tic                    % to record time to pull scan data for the 136 sensors per experiment ~ 1 min per exp.
    options                 = weboptions('HeaderFields',{'ContentType',  'text' ;'Authorization',sprintf(' Bearer %s', rest_token )}); % jas_debug_timeout
    options.Timeout         = 25; %jas_debug_timeout    
    exp_scan_cell           = cell(sensor_cnt,1);
    bogus_data_first_sensor = 0;                % 2018_05_02: fix. Some sensor data may be empty
    %bogus_data              = nan;
    
    % do first sensor by itself:
    try
        url_str                 = strcat(url_scan_str,sprintf('%-3d',exp_sensor_struct(1).id));  %jas_bug_fix: 05_23_2018 from: cur_sensor_ndx to 1 
        exp_scan_cell{1}        = nan;
        exp_scan_cell{1}        = webread(url_str,options);   % *** BOTTLENECK ****
    catch
        bogus_data_first_sensor      = 1;
        exp_scan_cell{1}             = struct(); 
        exp_scan_cell{1}.sensor_data = nan;
     	exp_scan_cell{1}.is_valid    = 0;
    end
    
    %url_str            = sprintf('http://jervis-staging.genalyte.com/api/sensors/%-3d',exp_sensor_struct(cur_sensor_ndx).id    );
    parfor cur_sensor_ndx   = 2:1:sensor_cnt        % for vs parfor
        %cur_sensor_ndx
        % orig: url_str   	= sprintf('http://jervis.genalyte.com/api/sensors/%-3d',exp_sensor_struct(cur_sensor_ndx).id    );
        url_str             = strcat(url_scan_str,sprintf('%-3d',exp_sensor_struct(cur_sensor_ndx).id));       
        try                   % without: try/catch: exp_scan_cell 	= webread(url_str,options);
            %error_message       = '';
            exp_scan_cell{cur_sensor_ndx} = nan;
            exp_scan_cell{cur_sensor_ndx} = webread(url_str,options);   % *** BOTTLENECK ****
        catch
            continue;
        end
        %2018_05_02: fixes to handle all kind of issues with sensor data: it may come empty even when it says it si valid:
    end % for each sensor  parfor or for
    
    % SECOND PASS: error checking detection and attempt to fix missing scan data:
    for cur_sensor_ndx   = 2:1:sensor_cnt        % for vs parfor
        bogus_data     	= 0;
        if (isempty(exp_scan_cell{cur_sensor_ndx})  )
            % FLAG IT: bad data
            bogus_data     	= 1;
        else
            %2018_05_02: fixes to handle all kind of issues with sensor data: it may come empty even when it says it si valid:
            if (~(isfield(exp_scan_cell{cur_sensor_ndx},'sensor_data')))
                % BOGUS DATA: set the flag:
                bogus_data = 1;
            else
                % FIELD EXIST: Check if there is any data or not:
                if (isempty(exp_scan_cell{cur_sensor_ndx}.sensor_data))
                    % NO DATA: Set the flag
                    bogus_data = 1;
                end
            end
        end
        if (bogus_data)
            % BAD DATA:
            % OK THE FIRST SENSOR: fix data by setting with nans the struct with the same size as the previous sensor
            exp_scan_cell{cur_sensor_ndx}             = struct();
            exp_scan_cell{cur_sensor_ndx}.sensor_data = nan(size(exp_scan_cell{cur_sensor_ndx-1}.sensor_data));            
            exp_scan_cell{cur_sensor_ndx}.is_valid    = 0; % it can  not be valid if there is no sensor data!

        end
    end % for each sensor SECOND PASS: error checking detection and attempt to fix missing scan data:
    
    if (bogus_data_first_sensor)
        % BAD FIRST SENSOR: Fill in with nans of size same as the second sensor (poor man's way to handle it)
        nans_cnt = 0;
        for cur_bad_sen_ndx=1:1:sensor_cnt
            if( isempty(exp_scan_cell{cur_bad_sen_ndx}))
                nans_cnt = nans_cnt+1;
            end
        end
        if (nans_cnt == sensor_cnt)
            % NO SCAN DATA AT ALL: Set flag and return error. There is nothing to save !!!
            error_message = 'Scan Data is Empty for this experiment';
            return;
        end
        %if (isfield(exp_scan_cell{1},'is_valid'))
        exp_scan_cell{1}.is_valid    = 0;
        exp_scan_cell{1}.sensor_data = nan(size(exp_scan_cell{2}.sensor_data));
        %end
    end
    
    %msg = toc;
    %lprintf_log('Time required to retrieve scan data for the 136 sensors per experiment = %5.1f secs\n',double(msg));
    sensor_scan_struct = scan_cvt_exp_scan_cell_to_sensor_scan_struct(exp_scan_cell);
    scan_data_struct   = scan_cvt_sensor_scan_struct_to_scan_data_struct(local_hd_struct,sensor_scan_struct); % fills SCAN_OPTICAL_EXCEL
    
    sensor_scan_struct.sensor_type = sensor_type;  %jas_performance.
    scan_data_struct.sensor_type   = sensor_type;  %jas_performance.
    sensor_scan_struct.proid       = cur_proid;    %jas_performance.
    scan_data_struct.proid         = cur_proid;    %jas_performance.
    if (local_hd_struct.local_hd_save_scan_excel_flag)
        % SCAN_OPTICAL_EXCEL:  CALLER REQUESTED TO SAVE LOCALLY: Save it
        %[ error_message,sensor_data_mat ] = sa_rest_exp_get_scan_from_hd( cur_expid );
        [ error_message ] = sa_rest_exp_save_scan(local_hd_struct,sensor_scan_struct,cur_expid);
    end
else
    % REAL CPP CALL: %jas_tbdf_debug the url for the cpp case (clone it from the fake case: tbd)
    exp_sensor_struct_table = struct2table(exp_sensor_struct);
    sensor_id_list          = exp_sensor_struct_table.id;  % vector of double.
    sensor_id_list_str      = num2str(sensor_id_list); % char 136 x 7      maybe niu
    sensor_id_struct        = cell(sensor_cnt,1);
    for cur_sensor_ndx = 1:1:sensor_cnt   % for vs parfor
        sensor_id_struct{cur_sensor_ndx}= sprintf('%-3d',exp_sensor_struct(cur_sensor_ndx).id    );
    end % for each sensor  parfor or for
    lprintf_log('\nexp_id:%s sen_id_1= %s',cur_expid, sensor_id_list_str(1));
    % here: make the cpp call: (see file: scan_get\source\sa_rest_exp_get_scan_from_db
end % real cpp call

% niu: ? tbv
% scan_data_plane_time       = 1;
% scan_data_plane_gru        = 2;
% scan_data_plane_dim         = 2;
% exp_scan_data               = nan(sensor_cnt,max_scan_esti_len,scan_data_plane_dim );
end % fn sa_rest_exp_get_scan_from_db

%     sensor_data_mat(cur_start_row:cur_end_row,2:size(cur_sensor_data,2)+1) = cur_sensor_data;
%     sensor_data_time(cur_start_row:cur_end_row,1:size(cur_sensor_data,2))  = cur_sensor_data(:,1;

% structs:
%  stored in files:  3 integers: sensor_cnt,max_scan_len,max_scan_col,
%                    1 matrix:   scan_data_matrix   dim(sensor_cnt*max_scan_len,max_scan_col)
%  recovered    sensor_data_time   scan_data_matrix(:,1)
%  recovered    sensor_data_grur   scan_data_matrix(:,2)
%  build        sensor_data_numb   1:1:sensor_cnt          replicated
%  build        sensor_data_len    max_scan_len            replicated