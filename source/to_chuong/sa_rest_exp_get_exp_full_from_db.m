function [ error_message,protocol_struct,exp_det_struct,exp_probe_struct,exp_sensor_struct] =  sa_rest_exp_get_exp_full_from_db( ...
    rest_token,expe_ned_table,cur_exp_ndx) %  save_to_hd_flag,save_qc_rpt_to_excel_req_flag ) %) % jas_domingo
%SA_REST_EXP_GET_EXP_FULL_FROM_DB retrieves from the DB  the full_exp_data for a single experiment (all protocol/probes/sensors)
%   Returns 4 data_structs.

% see:      sa_rest_exp_save_scan  (twin function) and sa_rest_exp_get_assay_from_hd
% pipeline: sa_rest_exp_get_scan -->  sa_rest_exp_get_exp_full_from_db
% sample call: [ error_message, scan_data_struct ] = sa_rest_exp_get_assay_from_hd('01101ef0-47bd-49f9-af6b-a5cc8087e64a'); % 'cur_expid)

% NOTE: The code of this function will be commented out and replaced with the cpp call.
%error_message     	= '';
%protocol_struct     = struct;
exp_det_struct      = struct;
exp_probe_struct    = struct;
exp_sensor_struct   = struct;
%scan_data_struct    = struct;

%jas_tbd: return these 3:  sensor_cnt,exp_sensor_struct,sensor_type

% S0. SET UP:
%cur_exper = char(expe_ned_table.exper(cur_exp_ndx));
% cur_instr = char(expe_ned_table.instr(cur_exp_ndx));
% cur_carri = char(expe_ned_table.carri(cur_exp_ndx));
% cur_spotl = char(expe_ned_table.spotl(cur_exp_ndx));
%cur_kitlo= char(expe_ned_table.expid(cur_exp_ndx));
% cur_proto = char(expe_ned_table.proto(cur_exp_ndx));
% date_str  = char(expe_ned_table.dates(cur_exp_ndx));
cur_expid = char(expe_ned_table.expid(cur_exp_ndx));
% date_str  = cellstr(date_str);
% date_str  = strrep(date_str,'(UTC)','');
% date_str  = strrep(date_str,' ','');
% %cur_datef = datenum(date_str);
%  % cellstr(int2str(datenum(date_str)));
cur_proid= char(expe_ned_table.proid(cur_exp_ndx));


% S1. PROTOCOLS PART: ______________________________________________________________________
[ error_message,protocol_struct ] = sa_rest_protocol_get_details(cur_proid,rest_token); %
if (~(isempty(error_message)))
    lprintf_log( '\n  skipping experiment due to protocol issue: %s',error_message);
    % jas_tbd: mark experiment as data_bad.
    return;
end
% FOUND DETAILS FOR THIS PROTOCOL: Update all proto names with the same category

% S2. EXP_DETAILS PART: ______________________________________________________________________
[ error_message,exp_det_struct ] = sa_rest_exp_get_details(cur_expid,rest_token );
if (~(isempty(error_message)))
    % jas_tbd: mark experiment as data_bad.
    return;
end
% S3. PROBE PART:       ______________________________________________________________________
[ error_message,exp_probe_struct ] = sa_rest_exp_get_probes(char(expe_ned_table.expid(cur_exp_ndx)), rest_token );
if (~(isempty(error_message)))
    % jas_tbd: mark experiment as data_bad.
    return;
end
% S4. SENSOR PART:      ______________________________________________________________________
%     Get cur experiment sensors:
%     returns: id(1876 to .. ?), number(1 to 136),   channel(1 or 2),  is_valid(0,1),  sensor_errors,    results( value(grue), normalized_value(norm_gru), errors etc.
[ error_message,exp_sensor_struct ] = sa_rest_exp_get_sensors(char(expe_ned_table.expid(cur_exp_ndx)), rest_token );
if (~(isempty(error_message)))
    % jas_tbd: mark experiment as data_bad.
    return; 
end
% S5. SCAN PART: ______________________________________________________________________ not done here (done separately)
% %     Get scan data from either source: The db (either cpp or Matlab using the API), the Hard Drive (Using previoulsly stored files if available)
% cur_sensor_cnt                              = size(exp_sensor_struct,1);
% [ sensor_table ]                            = probe_get_sensor_names_and_numbers( probe_map_table, cur_sensor_cnt );
% [ error_message,scan_data_struct] = sa_rest_exp_get_scan(local_hd_struct,cur_expid,cur_proid,exp_sensor_struct,rest_token,sensor_table); % SCAN_OPTICAL_EXCEL:
% if (~(isempty(error_message)))
%     % jas_tbd: mark experiment as data_bad.
%     return; % try with other experiments
% end

end % fn  sa_rest_exp_get_exp_full_from_db
