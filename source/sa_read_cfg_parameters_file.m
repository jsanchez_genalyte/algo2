function [ input_parameters_struct ] = sa_read_cfg_parameters_file( cfg_parameters_file_name )
%SA_READ_CFG_PARAMETERS_FILE reads a cfg parameter file and stores into a structure.
%   This is a one time initialization of input parameters for this SA app.
% Sample call:  sa_read_cfg_parameters_file('C:\Users\jsanchez\Documents\algo2\source\std_curve_cfg')
% see:         open 'C:\Users\jsanchez\Documents\algo2\source\sa_cfg'

if (isempty(cfg_parameters_file_name))
    % NO CONFIG PARAMETER TO READ: Set dummy defaults:
    % file_locations______________________________________________________________________________________________________________________________________
    %matlab_code_dir_01  = 'C:\Users\';
    excel_input_dir_01   = 'C:\Users\';                         % Now not using default input dir
    input_file_name_01   = 'local_data_set.mat';    
    local_input_dir       = excel_input_dir_01;  % 01 default dir to look for excel input files.
    input_file_name       = input_file_name_01;  % 02  % input file    
    % data related: ______________________________________________________________________________________________________________________________________
    % analyte_names_for_analysis:                              % enter: { 'all' }             to include everything on the excel file. or:
    probe_valid	= { 'All', 'HSA1',	'dsDNA',	'Sm',	'Scl-70',	'HSA5',	'Jo-1',	'SS-A60',	'HSA8',	'SS-B',	'RNP',	'HSA11',	'CenpB',	'HSA13',	'HumanIgG',	'HSA15',	'Anti-IgG'};
    dates_valid	= { 'Last_7_days', 'Today', 'Today', 'Yesterday', 'Last_30_days', 'This_month', 'Last_month',  'Custom_range', 'All' };
    proto_valid	= { 'All','ANA Multiplex (12 Chip) 1.1 QC-Prod' }; % jas_tbd_hardcode only known protocol. add more from jason objs.
    sampl_valid = { 'All','SAMPLE #1','SAMPLE #2','SAMPLE #3','SAMPLE #4','SAMPLE #5'}; % think jas_tbd jasd_here_Wednesday. 
    chipl_valid = { 'All','160505'}; % think jas_tbd
    exper_valid = { 'All','CL160505-12-Array-SP-01','CL160505-12-Array-SP-02','CL160505-12-Array-SP-03','CL160505-12-Array-SP-04','CL160505-12-Array-SP-05'}; % think jas_tbd
    instr_valid = { 'All','10', '11','12','13','14','15','16','17','18','19','20','21','22',}; % think jas_tbd
    carri_valid = { 'All'}; % think jas_tbd
    chann_valid = { 'Both','1','2'};
    clust_valid = { 'All','1','2','3','4'};
    datat_valid = { 'Raw','Normalized Jervis','Normalized Matlab'}; 
    ampli_valid = { 'All','Positives Only','Negative Only','Custom Range'}; % think jas_tbd    
    % Define Processing Input Arguments
    global_figure_start_no          = 1;      % starting number of figure: All figures will be number after this value.
    tr_delta_step_threshold         = 2.5;    % Maximum admisible difference between the 2 TR values in a channel for which TR values are consider sane (usable)
    wanted_inflexion_point_count    = 2;      % Want to find the 2 largest inflexion points: IE the ones followed by the largest going up values
    tr_enable_normalize_flag        = true;   % true; % == Do TR normalize; % false; == Do not normalize

    % G0_ large matrices_storage:  ________________________________________________________
    
    % additional cols for  g_db_set this is an original classificat
    db_set_db_ndx      ;%    this is the original order the data came from the db query.


else
    %run(cfg_parameters_file_name) % this is the configuration file for this app.
    % cfg_parameters_file_name %  deploy_ do_compile_qc  hardcoded 
    sa_cfg
    
end

% Pack all the input parameters into a struct to be returned.
% ANALYTE struct: _________________________________________________________________


field_01 = 'local_input_dir';               value_01 = { local_input_dir      };
field_02 = 'input_file_name';               value_02 = { input_file_name      };

% old algo: temporal:
field_03 = 'global_figure_start_no';      	value_03 = {  global_figure_start_no        };
field_04 = 'tr_delta_step_threshold';    	value_04 = { tr_delta_step_threshold        };
field_05 = 'wanted_inflexion_point_count'; 	value_05 = { wanted_inflexion_point_count   };
field_06 = 'tr_enable_normalize_flag';  	value_06 = { tr_enable_normalize_flag       };   % max number of blocks per excel sheet. combine: -REF- and -GNM- blocks.
field_07 = 'probe_valid';                   value_07 = { probe_valid  };
field_08 = 'dates_valid';                   value_08 = { dates_valid  };
field_09 = 'proto_valid';                   value_09 = { proto_valid  };
field_10 = 'sampl_valid';                   value_10 = { sampl_valid  };
field_11 = 'chipl_valid';                   value_11 = { chipl_valid  };
field_12 = 'exper_valid';                   value_12 = { exper_valid  };
field_13 = 'instr_valid';                   value_13 = { instr_valid  };
field_14 = 'carri_valid';                   value_14 = { carri_valid  };
field_15 = 'chann_valid';                   value_15 = { chann_valid  };
field_16 = 'clust_valid';                   value_16 = { clust_valid  };
field_17 = 'datat_valid';                   value_17 = { datat_valid  };
field_18 = 'ampli_valid';                   value_18 = { ampli_valid  };
% gui related:

field_19 = 'min_pos_delta_th_hor_pct';   	value_19 = { min_pos_delta_th_hor_pct  };
field_20 = 'min_pos_delta_th_amp_pct';   	value_20 = { min_pos_delta_th_amp_pct  };
field_21 = 'senso_curves_perp_cnt';   	    value_21 = { senso_curves_perp_cnt  };
field_22 = 'color_orange_warning';   	    value_22 = { color_orange_warning  };
field_23 = 'color_green_nice';              value_23 = { color_green_nice  };

input_parameters_struct = struct(...
    field_01 , value_01, field_02, value_02, field_03, value_03 ...
   ,field_04 , value_04, field_05, value_05, field_06, value_06 ...
   ,field_07, value_07, field_08, value_08, field_09, value_09 ...  
   ,field_10, value_10 ,field_11, value_11, field_12, value_12 ...  
   ,field_13, value_13 ,field_14, value_14, field_15, value_15 ...
   ,field_16, value_16 ,field_17, value_17, field_18, value_18 ...
   ,field_19, value_19, field_20, value_20, field_21, value_21 ...
   ,field_22, value_22, field_23, value_23);
end % fn: sa_read_cfg_parameters_file


%     ,field_04, value_04, field_05, value_05, field_06 ,value_06 ...
%     ,field_07, value_07 ,field_08, value_08, field_09, value_09 ...
%     ,field_10, value_10 ,field_11, value_11, field_12, value_12 ...
%     ,field_13, value_13 ,field_14, value_14, field_15, value_15 ...
%     ,field_16, value_16 ,field_17, value_17                   );

% Display the structure with all the parameters to be returned
% input_parameters_struct



% lefovers:

%     excel_tab_number      = 0;                   % 03  % Unknown until the file is being open and the CRP tab is located.
%     excel_sheet_name      = 'CRP';               % 04  % use tab 1 by default    
%     excel_file_version    = 2;                   % 05  % 1: Format used for demo. 2: Changes requested by Mus/Sasi.

% 
%     gru_range_for_output         = [ 100 800 ];      % 07      % niu currently: AN_SP? Range of GRU values to use (values outside: cliped)
%     con_range_for_output         = [ 100 800 ];      % 08      % niu currently: AN_SP? Range of concntration values to use (values outside: cliped)
%     synthetic_data_flag          = {  'false' };     % 09      % 'true' == synthetic data.	'false' == real instrument data. default==false
%     fit_type_valid               = { 'exp1-cte' 'exp2' 'linear' 'poly3'  'power2'  'fourier2'};  % similar to models_name_cell.
%      max_con_step                 = 0.002;
%     %max_gru_step                 = 50;
%     interpolation_length_factor   = 100; % ie: the data curves will be oversampled 100 times the number of points before taking the log to do SG.
%     do_interpolation_flag        = 0 ; 
%     sg_poly_order                =  3;                          % Order of polynomial fit
%     sg_win_length                = 101;	% Window length
%     [~,sg_coef]                  = sgolay(sg_poly_order,sg_win_length);   % Calculate S-G coefficients


    
% field_08 = 'gru_range_for_output';          value_08 = { gru_range_for_output       }; % AN_SP
% field_09 = 'con_range_for_output';          value_09 = { con_range_for_output       }; % AN_SP
% field_10 = 'synthetic_data_flag';        	value_10 = { synthetic_data_flag        }; % AN_SP
% field_11 = 'fit_type_valid';                value_11 = { fit_type_valid             };
% field_12 = 'max_con_step';                  value_12 = { max_con_step               };
% field_13 = 'interpolation_length_factor';	value_13 = { interpolation_length_factor};
% field_14 = 'do_interpolation_flag';       	value_14 = { do_interpolation_flag      };
% field_15 = 'sg_poly_order';              	value_15 = { sg_poly_order              };
% field_16 = 'sg_win_length';                 value_16 = { sg_win_length              };
% field_17 = 'sg_coef';                       value_17 = { sg_coef                    };


