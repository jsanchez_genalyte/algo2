
% Initialize web service
webservice_url = 'http://10.0.2.226/runcard_test/soap?wsdl';
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
methods(obj)

partnum = '';
partrev = '';
lotnum = '';
serial = 'B1501S6';
workorder = '';
opcode = '';
seqnum = '';
whereUsed = 0;

[response, error, msg] = fetchUnitGenealogy(obj,partnum, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);
if error ~= 0
    disp(['RunCard exception encountered for fetchUnitGenealogy(): ' msg]);
    return
else
    disp(msg);
end 