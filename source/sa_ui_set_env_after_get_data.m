function [ handles ] = sa_ui_set_env_after_get_data( handles )
%SA_SET_ENV_AFTER_GET_DATA sets the ui consistent with the data just loaded.
%   Goest thru all the loaded data and updates all the pulldown menu items to reflect the available data to work with:

% input_parameters_struct           = handles.input_parameters_struct;
ui_state_struct                     = handles.ui_state_struct;
input_parameters_struct             = handles.input_parameters_struct;

% get the number of loaded rows and unique values and counts for each pulldown
rows_db_cnt                         = handles.db_data_set_struct.rows_db_cnt{1};     % single float
ui_state_struct.db_cur_fit_net_ndx  = ones(rows_db_cnt,1);                           % start with all rows selected
if (rows_db_cnt)
    % SOMETHING GOT LOADED: From db or local file: Set the ui to be consistent:
    % E1.  Main Menus FILE/EDIT/VIEW/ANALYZE MENU:  _________	      set(handles.edit_menu,                             'Visible','on','Enable','on');
    set(handles.view_menu,                          'Visible','on','Enable','on');
    set(handles.file_save_db_local_menu_item,       'Visible','on','Enable','on');
    set(handles.file_append_sel_local_menu_item,    'Visible','on','Enable','on');
    set(handles.edit_copy_sel_menu_item,            'Visible','on','Enable','off');    % wait for selection
    set(handles.analysis_menu,                      'Visible','on','Enable','on');     % menu_4 analysis
    set(handles.analysis_norm_jervis_menu_item,   	'Visible','on','Enable','on');
    %E2. Popup Menus
    list_probe = categories(handles.db_data_set_struct.db_metad_set_struct.probe);
    list_dates = categories(handles.db_data_set_struct.db_metad_set_struct.dates);    
    list_proto = categories(handles.db_data_set_struct.db_metad_set_struct.proto);
    list_sampl = categories(handles.db_data_set_struct.db_metad_set_struct.sampl);
    list_chipl = categories(handles.db_data_set_struct.db_metad_set_struct.chipl);
    list_exper = categories(handles.db_data_set_struct.db_metad_set_struct.exper);
    list_instr = categories(handles.db_data_set_struct.db_metad_set_struct.instr);
    list_carri = categories(handles.db_data_set_struct.db_metad_set_struct.carri);
    list_chann = categories(handles.db_data_set_struct.db_metad_set_struct.chann);   % jas_tb_think: are these correct?
    list_clust = categories(handles.db_data_set_struct.db_metad_set_struct.clust);
    list_datat = categories(handles.db_data_set_struct.db_metad_set_struct.datat);
    list_valfg = categories(handles.db_data_set_struct.db_metad_set_struct.valfg);
    list_outfg = categories(handles.db_data_set_struct.db_metad_set_struct.outfg);
    list_error = categories(handles.db_data_set_struct.db_metad_set_struct.error);
    
    % These are the cnts of the available categories from the loaded db_data_set: i.e. the lenghth of each filter list.
    probe_len = size(list_probe,1);
    dates_len = size(list_dates,1);    
    proto_len = size(list_proto,1);
    sampl_len = size(list_sampl,1);
    chipl_len = size(list_chipl,1);
    exper_len = size(list_exper,1);
    instr_len = size(list_instr,1);
    carri_len = size(list_carri,1);
    chann_len = size(list_chann,1);
    clust_len = size(list_clust,1);
    %datat_len= size(list_datat,1);      % noiu it does not make sense
    %     valfg_len = size(list_valfg,1);    jas_tbd_future
    %     outfg_len = size(list_outfg,1);
    %     error_len = size(list_error,1);
    % make sure all are column vectors
    list_probe = util_transpose_to_col_vector(list_probe);
    list_dates = util_transpose_to_col_vector(list_dates);    
    list_proto = util_transpose_to_col_vector(list_proto);
    list_sampl = util_transpose_to_col_vector(list_sampl);
    list_chipl = util_transpose_to_col_vector(list_chipl);
    list_exper = util_transpose_to_col_vector(list_exper);
    list_instr = util_transpose_to_col_vector(list_instr);
    list_carri = util_transpose_to_col_vector(list_carri);
    list_chann = util_transpose_to_col_vector(list_chann);
    list_clust = util_transpose_to_col_vector(list_clust);
    list_datat = util_transpose_to_col_vector(list_datat);
    list_valfg = util_transpose_to_col_vector(list_valfg);
    list_outfg = util_transpose_to_col_vector(list_outfg);
    list_error = util_transpose_to_col_vector(list_error);
    
    % Modify text: Include found counts.
    set(handles.text_probe,'String',sprintf('PROBE    %-4d',probe_len));
    set(handles.text_dates,'String',sprintf('DATE     %-4d',dates_len));    
    set(handles.text_proto,'String',sprintf('PROTOCOL %-4d',proto_len));
    set(handles.text_sampl,'String',sprintf('SAMPLE   %-4d',sampl_len));
    set(handles.text_chipl,'String',sprintf('CHI PLOT %-4d',chipl_len));
    set(handles.text_exper,'String',sprintf('EXPERIM. %-4d',exper_len));
    set(handles.text_instr,'String',sprintf('INSTRUM. %-4d',instr_len));
    set(handles.text_carri,'String',sprintf('CARRIER  %-4d',carri_len));
    set(handles.text_chann,'String',sprintf('CHANNEL  %-4d',chann_len));
    set(handles.text_clust,'String',sprintf('CLUSTER  %-4d',clust_len));
    % set(handles.text_datat,'String',sprintf('DATA     %-4d',datat_len));
    
    % Update ui_state_struct indices with the new loaded data (to be consistent):
    if (~isempty(list_probe)) % 1
        ui_probe_state_struct                            = sa_ui_create_popup_struc( list_probe,'PROBE');
        %_tr_ ui_probe_state_struct.list_str_cel_cur_gui{end}= 'Tr';   % overwrite search with Tr.2017_10_11 fix  (future_spopupmenu_: end+1 tbd)
        ui_state_struct.probe                   	     = {ui_probe_state_struct};
        set(handles.popupmenu_probe,'String',ui_probe_state_struct.list_item_names,'Value',1);
        % spopupmenu:
        % handles.popupmenu_probe = mpopup_menu_set_items(handles.popupmenu_probe,ui_probe_state_struct.list_item_names,ui_probe_state_struct.list_item_slected,ui_probe_state_struct.list_item_enabled,1);
    end
    if (~isempty(list_dates)) % 2
        ui_dates_state_struct                       = sa_ui_create_popup_struc( list_dates,'DATES');
        ui_state_struct.dates                       = {ui_dates_state_struct};
        set(handles.popupmenu_dates,'String',ui_dates_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_proto)) % 3
        ui_proto_state_struct                       = sa_ui_create_popup_struc( list_proto,'PROTO');
        ui_state_struct.proto                       = {ui_proto_state_struct};
        set(handles.popupmenu_proto,'String',ui_proto_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_sampl)) % 4
        ui_sampl_state_struct                       = sa_ui_create_popup_struc( list_sampl,'SAMPL');
        ui_state_struct.sampl                   	= {ui_sampl_state_struct};
        set(handles.popupmenu_sampl,'String',ui_sampl_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_chipl)) % 5
        ui_chipl_state_struct                       = sa_ui_create_popup_struc( list_chipl,'CHAPL');
        ui_state_struct.chipl                       = {ui_chipl_state_struct};
        set(handles.popupmenu_chipl,'String',ui_chipl_state_struct.list_item_names,'Value',1);
    end
    
    if (~isempty(list_exper)) % 6
        ui_exper_state_struct                       = sa_ui_create_popup_struc( list_exper,'EXPER');
        ui_state_struct.exper                       = {ui_exper_state_struct};
        set(handles.popupmenu_exper,'String',ui_exper_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_instr)) % 7
        ui_instr_state_struct                       = sa_ui_create_popup_struc( list_instr,'INSTR');
        ui_state_struct.instr                       = {ui_instr_state_struct};
        set(handles.popupmenu_instr,'String',ui_instr_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_carri)) % 8
        ui_carri_state_struct                       = sa_ui_create_popup_struc( list_carri,'CARRI');
        ui_state_struct.carri                       = {ui_carri_state_struct};
        set(handles.popupmenu_carri,'String',ui_carri_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_chann)) % 9
        ui_chann_state_struct                       = sa_ui_create_popup_struc( list_chann,'CHANN');
        ui_state_struct.chann                       = {ui_chann_state_struct};
        set(handles.popupmenu_chann,'String',ui_chann_state_struct.list_item_names,'Value',1);
    end
    if (~isempty(list_clust)) % 10
        ui_clust_state_struct                       = sa_ui_create_popup_struc( list_clust,'CLUST');        
        ui_state_struct.clust                       = {ui_clust_state_struct};
        set(handles.popupmenu_clust,'String',ui_clust_state_struct.list_item_names,'Value',1);
    end
    %if (~isempty(list_datat)) % 11
        ui_datat_state_struct                       = sa_ui_create_popup_struc( {},'DATAT');
        % datat is special case: overwrite it       
        ui_datat_state_struct.list_item_names       = { 'Raw','Normalized By Jervis','Normalized by Matlab ...'};
        ui_datat_state_struct.list_item_slected    = [ 1 0 0 ];
        ui_datat_state_struct.list_item_enabled    = [ 1 1 1 ];        
        ui_state_struct.datat                       = {ui_datat_state_struct};
        set(handles.popupmenu_datat,'String',ui_datat_state_struct.list_item_names,'Value',1); % 1==raw
    %end
    % jas_tbd_amplification: not a discrete quantity. maybe discretize using thersholds? think about jas_tbd
    % %     if (~isempty(list_ampli)) % 12
    % %         ui_ampli_state_struct                       = sa_ui_create_popup_struc( list_ampli,'AMPLI');
    % %         ui_state_struct.ampli                       = {ui_carri_state_struct};
    % %         set(handles.popupmenu_ampli,'String',ui_ampli_state_struct.list_str_cel_ori_gui,'Value',1);
    % %     end
    
    if (~isempty(list_valfg)) % 13
        ui_valfg_state_struct                       = sa_ui_create_popup_struc( list_valfg,'DATES');
        ui_state_struct.valfg                       = {ui_valfg_state_struct};
        %set(handles.popupmenu_valfg,'String',ui_valfg_state_struct.list_str_cel_ori_gui,'Value',1);  %jas_tbd_future: no gui elem for this
    end
    
    if (~isempty(list_outfg)) % 14
        ui_outfg_state_struct                       = sa_ui_create_popup_struc( list_outfg,'OUTLI');
        ui_state_struct.outfg                       = {ui_outfg_state_struct};
        %set(handles.popupmenu_outfg,'String',ui_outfg)_state_struct.list_str_cel_ori_gui,'Value',1);  %jas_tbd_future: no gui elem for this
    end
    
    if (~isempty(list_error)) % 15
        ui_error_state_struct                       = sa_ui_create_popup_struc( list_error,'ERROR');
        ui_state_struct.error                       = {ui_error_state_struct};
        %set(handles.popupmenu_error,'String',ui_error_state_struct.list_str_cel_ori_gui,'Value',1);
    end
    
    % text_probe      	popupmenu_probe     1 Ana_1 ..Ana_16 , Tr, All (except  Tr)   no need for check marks
    % text_dates     	popupmenu_dates     2
    % text_proto      	popupmenu_proto     3
    % text_sampl       	popupmenu_sampl     4
    % text_chipl      	popupmenu_chipl     5
    % text_exper       	popupmenu_exper  	6
    % text_instr     	popupmenu_instr     7
    % text_carri        popupmenu_carri     8
    % text_chann        popupmenu_chann     9
    % text_clust        popupmenu_clust   	10
    % text_datat     	popupmenu_datat   	11
    % text_ampli        popupmenu_ampli     12
    
    % turn on and enable all the plot reltated stuff: We have data!.
    % but first set values from cfg_file that comes from xxx
    set(handles.edit_senso_per_plot,'String',int2str(input_parameters_struct.senso_curves_perp_cnt));
    %set(handles.edit_sens_anom_per_plot,'String',int2str(input_parameters_struct.senso_curves_perp_cnt));
    % jas_tbd:  set default_init here for envelo, median, mean, median check_boxes_default, and anything else user_wants_to_custom_initialize
    
    %set(handles.text_good_bline_region_yellow,   	'Visible','on'); % start invisible until a ref and gnm have a fit.
    %set(handles.text_good_ampli_region_yellow,   	'Visible','on'); % start invisible until a ref and gnm have a fit.
    %set(handles.text_anom_bline_region_yellow,   	'Visible','on'); % start invisible until a ref and gnm have a fit.
    %set(handles.text_anom_ampli_region_yellow,   	'Visible','on'); % start invisible until a ref and gnm have a fit.
    
    % good side
    set(handles.checkbox_good_sens_env,             'Visible','on','Enable','on','Value',0);
    set(handles.checkbox_good_sens_med,             'Visible','on','Enable','on','Value',0);
    set(handles.checkbox_good_sens_mea,             'Visible','on','Enable','on','Value',0);
    set(handles.checkbox_hide_good,                 'Visible','on','Enable','on','Value',0);
    % middle side
    set(handles.text_senso_per_plot,                'Visible','on');
    set(handles.edit_senso_per_plot,                'Visible','on','Enable','on');
    set(handles.pushbutton_senso_next,              'Visible','on','Enable','on');
    set(handles.checkbox_combine,                   'Visible','on','Enable','on');
    % anom side
    set(handles.checkbox_anom_sens_med,             'Visible','on','Enable','on','Value',0);
    set(handles.checkbox_anom_sens_mea,             'Visible','on','Enable','on','Value',0);
    set(handles.checkbox_hide_anom,                 'Visible','on','Enable','on','Value',0);
    
	cla(handles.axes_good_sens);
	cla(handles.axes_anom_sens);
    

   	ui_state_struct.checkbox_good_sens_mea  = 0;  % do not turn on anything until user request it
	ui_state_struct.checkbox_hide_good      = 0;
	ui_state_struct.checkbox_hide_anom      = 0;
	ui_state_struct.checkbox_anom_sens_mea  = 0;
	ui_state_struct.checkbox_anom_sens_med  = 0;    
	ui_state_struct.checkbox_good_sens_mea  = 0;
	ui_state_struct.checkbox_good_sens_med  = 0;     
	ui_state_struct.checkbox_good_sens_env  = 0;   % miss
	ui_state_struct.checkbox_combine        = 0;  %jas_temp niu jas_futre jas_tbd
    ui_state_struct.senso_start_ndx         = 1; 
    ui_state_struct.senso_start_ndx_save    = 1;    
    ui_state_struct.senso_start_ndx_next    = 1;
    ui_state_struct.senso_curves_perp_cnt   = min(input_parameters_struct.senso_curves_perp_cnt,rows_db_cnt); 
    ui_state_struct.color_orange_warning    = input_parameters_struct.color_orange_warning;
    ui_state_struct.color_green_nice        = input_parameters_struct.color_green_nice;    
    ui_state_struct.ui_request_flag         = 'g_n_a'; % consistent with: 0 hide_good and 0: hide_anom 
    % The Tos should be visible:
    % has_tbd: poulate tos with all the db_data: 
    set(handles.ui_table_tos,                       'Visible','on','Enable','on');
    set(handles.edit_select_all_menu_item,          'Visible','on','Enable','on');
    set(handles.edit_sel_inv_menu_item,             'Visible','on','Enable','on');
    
    handles.ui_state_struct = ui_state_struct;  % save it back.
        % MAKE SURE WE START WITH ALL SELECTED:
     [ handles ] = mpopup_menu_reset_all_to_all_all( handles );
     [ handles ]                     = sa_ui_set_popupmenus_after_popup_change( handles );

end % rows_db_cnt > 0
end % fn sa_set_env_after_get_data

