function [ file_loaded_count, raw_times_data, raw_shifts_data,  raw_col_names_data, raw_ld_data, raw_tr_data,meta_data_data  ] ...
    = load_test_data( input_data_type, input_file_names)
%LOAD SAMPLE DATA Loads in memory the raw sensor data for the specified file names or directories.
%   Returns 5 cells: raw_shifts_data, raw_times_data, raw_col_names_data, ld_data and tr_data
%                    for the files were found in the directory.
%                    The last row of the 5 cells has N (The count of rows from excel)

% This code will be obsolete once we can retrieve the data from the DB and parse the JSON object ...

declare_globals; 

raw_times_data      = {};% empty cells ..
raw_shifts_data     = {};
raw_col_names_data  = {};
meta_data_data      = {};
file_loaded_count   = 0;
if (strcmp(input_data_type,'excel_dir'))
    % USER SPECIFIED AN EXCEL DIRECTORY: load all the xlsx files sitting in this dir.
    
    excel_tab_number    = 2;                  % second tab is expected to be raw data per sensor  str2num(char(a{20}))
    file_count          = size(input_file_names);
    file_count          = file_count(1);
    raw_shifts_data     = cell(file_count,1);
    raw_times_data      = cell(file_count,1);
    raw_col_names_data  = cell(file_count,1);
    raw_ld_data         = cell(file_count,1);
    raw_tr_data         = cell(file_count,1);
    meta_data_data      = cell(file_count,1);    
    
    for cur_file = 1:1:file_count
        %         if (cur_file > 80)
        %             break;  % For debug/development process only a couple of files......
        %         end
        display (strcat(num2str(cur_file),' - Loaded Excel file  = _ ',input_file_names{cur_file}))
        
        % [ excel_true_range_data excel_true_row_last, excel_true_col_last 
        [ excel_true_range_data  ] = determine_excel_sensor_range_( input_file_names{cur_file} );
        raw_sensor_table     = readtable(input_file_names{cur_file},'Sheet',excel_tab_number, 'Range',excel_true_range_data','FileType','spreadsheet');
        raw_sensor_matrix    = table2array(raw_sensor_table); %  (1:true_row_range,:));
        raw_sensor_col_names = raw_sensor_table.Properties.VariableNames;
        
        raw_sensor_size      = size(raw_sensor_table);
        raw_sensor_col_count = raw_sensor_size(2);
        raw_sensor_row_count = raw_sensor_size(1);
        % Build indices: This deppends on the TSD excel file
        col_time_indices          = 2:2:raw_sensor_col_count-9; % Assumining: Last 8 are LD and TR, and first col is the time point(to be ignored!)
        col_shift_indices         = 3:2:raw_sensor_col_count-8; % Assumining: Last 8 are LD and TR, and first col is the time point(to be ignored!)
        col_ld_time_shift_indices = [ ( raw_sensor_col_count-7) ( raw_sensor_col_count-6) ( raw_sensor_col_count-3) ( raw_sensor_col_count-2)];
        col_tr_time_shift_indices = [ ( raw_sensor_col_count-5) ( raw_sensor_col_count-4) ( raw_sensor_col_count-1) ( raw_sensor_col_count-0)];
        % Clean up Analyte names:
        col_shift_col_names = raw_sensor_col_names(col_shift_indices);
        col_shift_col_names = strrep(col_shift_col_names(:), '_','');
        col_shift_col_names = strrep(col_shift_col_names(:), 'Shift','');
        col_shift_col_names = strrep(col_shift_col_names(:), '1','');
        
        % store N on all the rows so it's clear what the excel file original contents was.
        % Next line will increase the num of rows by one: This is done intentionally to store N in the last row.
        raw_sensor_matrix(end+1,:) = raw_sensor_row_count;
        
        % Store the 4 quantities for future processing.
        raw_times_data{cur_file} =raw_sensor_matrix(:,col_time_indices);
        %size(raw_times_data{cur_file})
        
        raw_shifts_data{cur_file}=raw_sensor_matrix(:,col_shift_indices);
        size(raw_shifts_data{cur_file})
        raw_col_names_data{cur_file} = col_shift_col_names;
        % extract ld data:
        raw_ld_data{cur_file} =raw_sensor_matrix(:,col_ld_time_shift_indices);
        % extract tr data:                                                     % contents will be:
        raw_tr_data{cur_file} =raw_sensor_matrix(:,col_tr_time_shift_indices); % C1=TR1_Time	C2=TR1_Shift	C3=TR2_Time	C4=TR2_Shift
        
        %feature_matrix(feature_excel_cnt_row,:,cur_file) = raw_sensor_size(1);
        
        % Quick and dirty addition to get meta-data: Tab:1 (all this code will be obsolete once I get the db connectivity)
        excel_range_data    = 'B1:B15'; % very large range down: Just one column
        meta_data_table     = readtable(input_file_names{cur_file},'Sheet',1, 'Range',excel_range_data','FileType','spreadsheet');
        meta_data_data{cur_file} =table2cell(meta_data_table);
        
        % ALL EXCEL FILES ARE LOADED __________________________________________________
    end
    file_loaded_count = cur_file;
end  % input_data_type == 'excel_dir'
