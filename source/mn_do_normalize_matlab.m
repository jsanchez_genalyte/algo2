function [ db_data_set_struct ] = mn_do_normalize_matlab( db_data_set_struct )
%MN_DO_NORMALIZE_MATLAB Performs tr normalization (jas_clone it from jervis normalization:             jas_tbd
%   do the normalization given raw data. populate all the 3 cols and the matlab norm columns.
% sample call: handles.db_data_set_struct =  do_normalize_matlab(handles.db_data_set_struct)


% fill in this column:  handles.db_data_set_struct.scan_nrm_gru_matlab
% given these columns:

%                   orndx: [1536x1 double]
%                   probe: {1x16 cell}
%     db_metad_set_struct: [1x1 struct]
%                smd_cols: [1x1 struct]
%                   datef: [1536x1 double]
%                   bling: [1536x1 double]    x3
%                   amplg: [1536x1 double]    x3
%                   shift: [1536x1 double]    x3
%           scan_raw_time: {[1536x63 double]}
%            scan_raw_gru: {[1536x63 double]} x1 swap on datat
%         scan_raw_tr_gru: {[1536x63 double]}
%            scan_nrm_gru: {[1536x63 double]} x1 swap on datat
%     scan_nrm_gru_matlab: {[1536x63 double]} x1 swap on datat
%             rows_db_cnt: {[1536]}
%             cols_db_cnt: NaN

end % fn mn_do_normalize_matlab

