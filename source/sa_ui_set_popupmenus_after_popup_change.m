function [ handles ] = sa_ui_set_popupmenus_after_popup_change( handles)
%SA_UI_SET_POPUPMENUS_AFTER_POPUP_CHANGE updates the popupmenus list to keep consistency
%   Input:
%       1. list_ndx_filt_ori_dat a new direct filter for "some" popupmenu.
%       2. db_metad_set_struct
%       3. ui_state_struct.*.    dir_ndx (ori),list_ndx_filt_ori_dat, list_str_cel_dat
%   Once a new direct filter has bee updated:
%   S1 - applies the new filter (i.e. and it) to the current direct filters.
%   S2 - calculates new updated categories, (ie categories...) applies the new net filters to the db_metadata
%   S3 - uses these categories to update each cur_dat and each cur_gui, the update on the screen the popuplists
%   S4 - Update text: 3 cases (single: orig_text,all-true: orig_text #, all-partial orig_text x / y
%   S5 - Save into structs the updated popups and net filters ready for the next user filtering action.
% pipeline: Sensogram_Analysis_App_10 --> sa_ui_set_popupmenus_after_popup_change -> mpopup_menu_set_items

ui_state_struct     = handles.ui_state_struct;
db_metad_set_struct = handles.db_data_set_struct.db_metad_set_struct;

% S1 - applies the new filter (i.e. and it) to the current direct filters
% think about: pass the name of 'probe' then use a if (xxx) or a clever way.

% S1.2 Calculate a new :db_cur_fit_net_ndx   This is he overall availabe data now: Whatever they have before anded with the new direct filter is:
% niu:  if (strcmpi('probe',updated_filter_name))  % 1

% the first 3 will never be included because there is never an single item called 'all', 'swap' or 'search...'   so .... no problem
db_cur_fit_net_ndx = ...  % note: it does not hurt to have extra categories in ori: like all or custom: they just get ignored bec there is not probe: called 'all'
    (  ismember(db_metad_set_struct.probe, ui_state_struct.probe{1}.list_item_names(ui_state_struct.probe{1}.list_item_slected))) ...    
    & (ismember(db_metad_set_struct.dates, ui_state_struct.dates{1}.list_item_names(ui_state_struct.dates{1}.list_item_slected))) ...                      ...
    & (ismember(db_metad_set_struct.proto, ui_state_struct.proto{1}.list_item_names(ui_state_struct.proto{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.sampl, ui_state_struct.sampl{1}.list_item_names(ui_state_struct.sampl{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.chipl, ui_state_struct.chipl{1}.list_item_names(ui_state_struct.chipl{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.exper, ui_state_struct.exper{1}.list_item_names(ui_state_struct.exper{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.instr, ui_state_struct.instr{1}.list_item_names(ui_state_struct.instr{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.carri, ui_state_struct.carri{1}.list_item_names(ui_state_struct.carri{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.chann, ui_state_struct.chann{1}.list_item_names(ui_state_struct.chann{1}.list_item_slected))) ...
    & (ismember(db_metad_set_struct.clust, ui_state_struct.clust{1}.list_item_names(ui_state_struct.clust{1}.list_item_slected)));

% HAVE THE FINAL: db_cur_fit_net_ndx
% S2 - calculates new updated enabled(available) categories, (ie categories...) applies the new net filter to the db_metadata
ui_state_struct.probe{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.probe(db_cur_fit_net_ndx))));
ui_state_struct.dates{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.dates(db_cur_fit_net_ndx))));
ui_state_struct.proto{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.proto(db_cur_fit_net_ndx))));
ui_state_struct.sampl{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.sampl(db_cur_fit_net_ndx))));
ui_state_struct.chipl{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.chipl(db_cur_fit_net_ndx))));
ui_state_struct.exper{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.exper(db_cur_fit_net_ndx))));
ui_state_struct.instr{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.instr(db_cur_fit_net_ndx))));
ui_state_struct.carri{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.carri(db_cur_fit_net_ndx))));
ui_state_struct.chann{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.chann(db_cur_fit_net_ndx))));
ui_state_struct.clust{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.clust(db_cur_fit_net_ndx))));
%ui_state_struct.datat{1}.list_item_enabled(4:end)  = logical (countcats((db_metad_set_struct.datat(db_cur_fit_net_ndx))));

% S3. Combine the list_item_slected and list_item_enabled to update the mpopupmenus in the screen:
%     At this point need to set the proper string and the proper value from: cur_gui_ndx
%     Then update the buttons with the proper counts: selected / available.
 
s_sruct               = ui_state_struct.probe{1};
mpopup_menu_set_items( handles.popupmenu_probe,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_probe,handles.text_probe,'PROBE   ');
ui_state_struct.probe = {s_sruct};

s_sruct               = ui_state_struct.dates{1};
mpopup_menu_set_items( handles.popupmenu_dates,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_dates,handles.text_dates,'DATES   ');
ui_state_struct.dates = {s_sruct};

s_sruct               = ui_state_struct.proto{1};
mpopup_menu_set_items( handles.popupmenu_proto,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_proto,handles.text_proto,'PRTOCOL ');
ui_state_struct.proto = {s_sruct};

s_sruct               = ui_state_struct.sampl{1};
mpopup_menu_set_items( handles.popupmenu_sampl,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_sampl,handles.text_sampl,'SAMPLE  ');
ui_state_struct.sampl = {s_sruct};

s_sruct               = ui_state_struct.chipl{1};
mpopup_menu_set_items( handles.popupmenu_chipl,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_chipl,handles.text_chipl,'CHP LOT ');
ui_state_struct.chipl = {s_sruct};

s_sruct               = ui_state_struct.exper{1};
mpopup_menu_set_items( handles.popupmenu_exper,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_exper,handles.text_exper,'EXPERI ');
ui_state_struct.exper = {s_sruct};

s_sruct               = ui_state_struct.instr{1};
mpopup_menu_set_items( handles.popupmenu_instr,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_instr,handles.text_instr,'INSTRUM ');
ui_state_struct.instr = {s_sruct};

s_sruct               = ui_state_struct.carri{1};
mpopup_menu_set_items( handles.popupmenu_carri,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_carri,handles.text_carri,'CARRIER ');
ui_state_struct.carri = {s_sruct};

s_sruct               = ui_state_struct.chann{1};
mpopup_menu_set_items( handles.popupmenu_chann,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_chann,handles.text_chann,'CHANNEL ');
ui_state_struct.chann = {s_sruct};

s_sruct               = ui_state_struct.clust{1};
mpopup_menu_set_items( handles.popupmenu_clust,s_sruct.list_item_names,s_sruct.list_item_slected,s_sruct.list_item_enabled,s_sruct.cur_gui_ndx) % [ a_popupmenu ]
s_sruct               = ui_update_popup_and_text(s_sruct,handles.popupmenu_clust,handles.text_clust,'CLUSTER ');
ui_state_struct.clust = {s_sruct};

% datat tobedone: not standard mpopupmenu 
% display(class(list_probe));
% display(class(list_proto));
% display(class(list_sampl));
% display(class(list_chipl));
% display(class(list_exper));
% display(class(list_instr));
% display(class(list_carri));

%if (~isempty(list_datat))                                                                           % 11 datat
    % THERE IS DATA for the given data_type: raw, normj normm ..
    % retrieve all indices that are currently on the screen: static pop up: list never canges: { 'Raw','Normalized By Jervis','Normalized by Matlab'};
    popupmenu_sel_index     = get(handles.popupmenu_datat,   'Value');
    if (popupmenu_sel_index ~= ui_state_struct.datat{1}.cur_gui_ndx )
        % USER CHANGED DATA Type:
        ui_state_struct.datat{1}.cur_gui_ndx = popupmenu_sel_index; % save the new datat
        if (popupmenu_sel_index  ==1 )
            % USER REQUESTED RAW DATA:
            % jas_tbd:  copy the 3 arrays and normalize if it has notbee done yet.
            
            %                   orndx: [1536x1 double]
            %                   probe: {1x16 cell}
            %     db_metad_set_struct: [1x1 struct]
            %                smd_cols: [1x1 struct]
            %                   datef: [1536x1 double]
            %                   bling: [1536x1 double]    x3
            %                   amplg: [1536x1 double]    x3
            %                   shift: [1536x1 double]    x3
            %           scan_raw_time: {[1536x63 double]}
            %            scan_raw_gru: {[1536x63 double]} x1 swap on datat
            %         scan_raw_tr_gru: {[1536x63 double]}
            %            scan_nrm_gru: {[1536x63 double]} x1 swap on datat
            %     scan_nrm_gru_matlab: {[1536x63 double]} x1 swap on datat
            %             rows_db_cnt: {[1536]}
            %             cols_db_cnt: NaN
            
            % use: scan_raw_gru         as the active column
            % use: scan_nrm_gru         as the active column
            % use: scan_nrm_gru_matlab  as the active column
            handles.db_data_set_struct.scan_cur_gru = handles.db_data_set_struct.scan_raw_gru;
            handles.db_data_set_struct.bligc       	= handles.db_data_set_struct.blinr;        %  make raw the new current
            handles.db_data_set_struct.ampgc      	= handles.db_data_set_struct.ampgr;        %
            handles.db_data_set_struct.shifc      	= handles.db_data_set_struct.ampgr - handles.db_data_set_struct.blinr;
        end
        if (popupmenu_sel_index  ==2 )
            % USER REQUESTED NORMALIZED BY JERVIS DATA:
            % jas_tbr:
            handles.db_data_set_struct.scan_cur_gru = handles.db_data_set_struct.scan_nrm_gru;    % jas_temp: may not be available
            handles.db_data_set_struct.bligc       	= handles.db_data_set_struct.blinj;     % make jervis normalized the new current
            handles.db_data_set_struct.ampgc      	= handles.db_data_set_struct.ampgj;
            handles.db_data_set_struct.shifc     	= handles.db_data_set_struct.ampgj - handles.db_data_set_struct.blinj;
        end
        if (popupmenu_sel_index  ==3 )
            % USER REQUESTED NORMALIZED BY MATLAB DATA:(may need dlg: normalizing data ....
            % jas_tbd:  use the raw data to do matlab normalization: output:
            % check if normalization was done:
            if (~(handles.db_data_set_struct.nomfg))
                handles.db_data_set_struct         = mn_do_normalize_matlab(handles.db_data_set_struct);   %jas_tbd: fake empty normalization
                handles.db_data_set_struct.nomfg   = true;
            end
            handles.db_data_set_struct.scan_cur_gru = handles.db_data_set_struct.scan_nrm_gru_matlab;
            
            handles.db_data_set_struct.bligc    	= handles.db_data_set_struct.blinm;        %   make matlab normalized the new current
            handles.db_data_set_struct.ampgc     	= handles.db_data_set_struct.ampgm;        %
            handles.db_data_set_struct.shifc        = handles.db_data_set_struct.ampgm - handles.db_data_set_struct.blinm;
        end
        %noiu ui_temp_struct         = sa_ui_update_popup_struc(ui_state_struct.datat{1}, list_datat,popupmenu_sel_index ); % special!!!.
        %noiu ui_temp_struct          = ui_state_struct.datat{1};
        %noiu ui_temp_struct.list_str_cel_cur_gui{end}  = 'Normalized By Jervis';   % special case: datat menu never changes!. (no search)
        %noiu ui_temp_struct.list_str_cel_cur_gui{end+1}= 'Normalized By Matlab';   % special case: datat menu never changes!.
        %set(handles.popupmenu_datat,'String',ui_temp_struct.list_str_cel_cur_gui,'Value',popupmenu_sel_index);
    end % user chnaged data)type
%end % not empty list_datat

% Build the ultimate ndx: The combination of all the filters:

% S4. save stuff back:
% Save net filters back to struct:  Ready for  more user filtering actions.
ui_state_struct.db_cur_fit_net_ndx              = db_cur_fit_net_ndx;   % This will be used to display the tos records.
available_senso_cnt                             = sum(db_cur_fit_net_ndx);
if (ui_state_struct.senso_curves_perp_cnt       >  available_senso_cnt)
    % CURENT REQUEST IS  MORE SENSO THAN AVAILABLE: Lower it)
    ui_state_struct.senso_curves_perp_cnt        = available_senso_cnt; % start with as many as found. good_ curves_per_plot_cnt
    set(handles.edit_senso_per_plot,'string',sprintf('%-4d',ui_state_struct.senso_curves_perp_cnt));
else
    % CURENT REQUEST IS  less or equal than SENSO THAN AVAILABLE:  do nothing: Keep the current senso_curves_perp_cnt
end

handles.ui_state_struct                         = ui_state_struct;      % save it back.
%handles.db_data_set_struct.db_metad_set_struct  = db_metad_set_struct;  % no need to save because it did not change.
end % fn sa_ui_set_popupmenus_after_popup_change


