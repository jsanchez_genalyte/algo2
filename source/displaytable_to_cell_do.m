


col_head = data_sum_stats_table.Properties.VariableNames;
wid     = [16,  16,  16,  16,  16  ];
fms     = {'4.0f','5.0f','6.1f','6.1f','6.2f'};
row_head= {'', '', '', '', '' };% , fid, colsep, rowending)
fid     = 1;
col_sep = '\t';
row_end = '\n';

data_sum_stats_table.QC_RACK_Number = double(data_sum_stats_table.QC_RACK_Number);

text_cell = displaytable_to_cell(data_sum_stats_table,col_head, wid,  fms,row_head,fid, col_sep, row_end);

