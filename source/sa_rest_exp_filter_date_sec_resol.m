function [  exper_req_table ] = sa_rest_exp_filter_date_sec_resol(req_date_struct,exper_req_table)
%SA_REST_EXP_FILTER_DATE_SEC_RESOL Filter the expe_req_table to the request based in second resolution
%   Returns only  the experiments that pass the date required when using the second resolution request dates

if (isempty(exper_req_table))
    return 
end

sec_format              = 'MM/dd/yyyy hh:mm:ss aa'; % for datetime  jas_bug_fix_ 05_14_2018 (swapped MM/dd as dd/MM)
date_str_to_sec_format  = 'mm/dd/yyyy HH:MM:SS PM';

if (       (isempty(req_date_struct.start_day_resol_date_str)) ...
        && (isempty(req_date_struct.end_day_resol_date_str  )) ...
        && (isempty(req_date_struct.start_sec_resol_date_str))  ...
        && (isempty(req_date_struct.end_sec_resol_date_str  )) )
    % ALL DATE REQUESTS ARE EMPTY: Request is for specific experiments: Nothing needs to be fiter out.
    return;
end


found_date_num          = datenum(exper_req_table.dates,date_str_to_sec_format);

if (       (~(strcmp(req_date_struct.start_day_resol_date_str,req_date_struct.start_sec_resol_date_str))) ...
        || (~(strcmp(req_date_struct.end_day_resol_date_str,  req_date_struct.end_sec_resol_date_str  ))))
    % DATE REQUEST DAY AND SS ARE NOT THE SAME: Need to do filtering to include only whatever is within the sec resolution
    if (isdatetime(datetime(req_date_struct.start_sec_resol_date_str,'Format',sec_format)) && isdatetime(datetime(req_date_struct.start_sec_resol_date_str,'Format',sec_format)))
        % DATER REQUEST IS A DATETIME
        start_req_num    = datenum( datestr(datetime(req_date_struct.start_sec_resol_date_str,'Format',sec_format),date_str_to_sec_format) );
        end_req_num      = datenum( datestr(datetime(req_date_struct.end_sec_resol_date_str,  'Format',sec_format),date_str_to_sec_format) );
        exp_date_sec_ndx = (found_date_num >= start_req_num) & (found_date_num < end_req_num) ;        
    else
        % DATER REQUEST IS A DATE
        exp_date_sec_ndx = ...
            found_date_num >= datenum(req_date_struct.start_sec_resol_date_str) & found_date_num <= datenum(req_date_struct.end_sec_resol_date_str);
    end
    
    if (~(isempty(exp_date_sec_ndx)))
        % apply filter
        exper_req_table = exper_req_table(exp_date_sec_ndx,:);
    end
else
    % SAME RESOLUTION: Return the all experiments
    return;
end

end % fn sa_rest_exp_filter_date_sec_resol

