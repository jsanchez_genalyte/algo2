function    [ start_to_day_date_str, end_to_day_date_str, start_to_sec_date_str, end_to_sec_date_str] = parse_date_keyword_3(wanted_keyword)
%DATE_KEYWORD_3 converts a wanted_keyword of 3 words into concrete start and end dates:
% sample call:   
% [ start_to_day_date_str, end_to_day_date_str, start_to_sec_date_str, end_to_sec_date_str ] = parse_date_keyword_3('LAST 7 DAYS');
% [ start_to_day_date_str, end_to_day_date_str, start_to_sec_date_str, end_to_sec_date_str ] = parse_date_keyword_3('PREVIOUS 2 HOURS')

% keywords_3 = { 'LAST 7 DAYS','LAST 30 DAYS' }; % start_to_day_date_str, end_to_day_date_str, start_to_sec_date_str, end_to_sec_date_str
end_to_day_date_str   = '';
start_to_day_date_str = '';
start_to_sec_date_str = '';
end_to_sec_date_str   = '';

%time_now = datetime('now');

date_str_to_day_format               = 'mm/dd/yyyy';
date_str_to_sec_format               = 'mm/dd/yyyy HH:MM PM';
substract_days = 0;
if (strcmp(wanted_keyword,'LAST 2 DAYS'))
    substract_days = -2;
end
if (strcmp(wanted_keyword,'LAST 3 DAYS'))
    substract_days = -3;
end
if (strcmp(wanted_keyword,'LAST 4 DAYS'))
    substract_days = -4;
end
if (strcmp(wanted_keyword,'LAST 5 DAYS'))
    substract_days = -5;
end
if (strcmp(wanted_keyword,'LAST 6 DAYS'))
    substract_days = -6;
end
if (strcmp(wanted_keyword,'LAST 7 DAYS'))
    substract_days = -7;
end
if (strcmp(wanted_keyword,'LAST 30 DAYS'))
    substract_days = -30;
end
if (substract_days ~= 0)
    end_to_day_date_str          = datestr(now,date_str_to_day_format);
    end_to_sec_date_str          = datestr(now,date_str_to_sec_format);
    start_date                   = addtodate(now, substract_days, 'd');
    start_to_day_date_str        = datestr(start_date,date_str_to_day_format);
    start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);
end
% % if (strcmp(wanted_keyword,'LAST 7 DAYS'))
% %     %
% %     % old way" 
% % %     start_date                      = time_now-days(7);
% % %     start_to_day_date_str        = datestr(start_date,date_str_to_day_format);
% % %     end_to_day_date_str          = datestr(time_now,date_str_to_day_format);
% % %     
% % %     start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);
% % %     start_to_sec_date_str(13:17) = '00:00';% start of the day    
% % %     end_to_sec_date_str          = datestr(time_now,date_str_to_sec_format); 
% % %     end_to_sec_date_str(13:17)   = '23:59';% end of the day     
% %     
% %     % This will be exactly 7 days:  from 7 days  ago to now:     
% %     end_to_day_date_str          = datestr(now,date_str_to_day_format);   
% %     end_to_sec_date_str          = datestr(now,date_str_to_sec_format);      
% %     start_date                   = addtodate(now, -7, 'd');    
% %     start_to_day_date_str        = datestr(start_date,date_str_to_day_format); 
% %     start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);      
% % end
% % if (strcmp(wanted_keyword,'LAST 30 DAYS'))
% %     end_to_day_date_str          = datestr(now,date_str_to_day_format);   
% %     end_to_sec_date_str          = datestr(now,date_str_to_sec_format);      
% %     start_date                   = addtodate(now, -30, 'd');    
% %     start_to_day_date_str        = datestr(start_date,date_str_to_day_format); 
% %     start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);      
% % end
% keywords_3=  {'PREVIOUS 1 HOUR', 'PREVIOUS 2 HOURS',
substract_hours = 0;
if (strcmp(wanted_keyword,'PREVIOUS 1 HOUR'))   % Exact
    substract_hours = -1;
end
if (strcmp(wanted_keyword,'PREVIOUS 2 HOURS'))
    substract_hours = -2;
end
if (strcmp(wanted_keyword,'LAST 1 HOUR'))
    substract_hours = -1;
end
if (strcmp(wanted_keyword,'LAST 2 HOURS'))
    substract_hours = -2;
end
if (strcmp(wanted_keyword,'LAST 3 HOURS'))
    substract_hours = -3;
end
if (strcmp(wanted_keyword,'LAST 4 HOURS'))
    substract_hours = -4;
end
if (strcmp(wanted_keyword,'LAST 5 HOURS'))
    substract_hours = -5;
end
if (strcmp(wanted_keyword,'LAST 6 HOURS'))
    substract_hours = -6;
end
if (strcmp(wanted_keyword,'LAST 7 HOURS'))
    substract_hours = -7;
end
if (strcmp(wanted_keyword,'LAST 8 HOURS'))
    substract_hours = -8;
end
if (strcmp(wanted_keyword,'LAST 9 HOURS'))
    substract_hours = -9;
end
if (strcmp(wanted_keyword,'LAST 10 HOURS'))
    substract_hours = -10;
end
if (strcmp(wanted_keyword,'LAST 11 HOURS'))
    substract_hours = -11;
end
if (strcmp(wanted_keyword,'LAST 12 HOURS'))
    substract_hours = -12;
end
if (strcmp(wanted_keyword,'LAST 24 HOURS'))
    substract_hours = -24;
end
if (substract_hours ~= 0)
    if (( strcmp(wanted_keyword,'PREVIOUS 1 HOUR'))  || ( strcmp(wanted_keyword,'PREVIOUS 2 HOURS')) )
        % RETURN EXACTLY ONE HOUR ON EVEN HOUR BOUNDARY FROM THE LAST HOUR BOUNDARY to THE LAST 2 HOUR BOOUNDARY: ie start: 2018  4:00:00 PM   end: 2018  5:00:00 PM
        % Approach: from now: trim the minutes: to 00: that is the end.  from the end: substract 1 hour.
        beg_hour_str                 = '00:00'; % ndx: 15:19
        time_now_dt                  = datetime('now');
        end_to_day_date_str          = datestr(time_now_dt,date_str_to_day_format);
        end_to_sec_date_str          = datestr(time_now_dt,date_str_to_sec_format);
        end_to_sec_date_str(15:19)   = beg_hour_str;  '00:00'; % start of the hour
        
        start_date                   = addtodate(datenum(end_to_sec_date_str), substract_hours, 'h');
        start_to_day_date_str        = datestr(start_date,date_str_to_day_format);
        start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);
        return;
    end
    
    % This will be exactly one hour:  from an hour ago to now:     
    end_to_day_date_str          = datestr(now,date_str_to_day_format);   
    end_to_sec_date_str          = datestr(now,date_str_to_sec_format);      
    start_date                   = addtodate(now, substract_hours, 'h');    
    start_to_day_date_str        = datestr(start_date,date_str_to_day_format); 
    start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);      
    return    
end

if (strcmp(wanted_keyword,'LAST 1 YEAR')) % % full year: today - 1_year to today  BEST AND SIMPLEST
    %                                   
    end_to_day_date_str          = datestr(now,date_str_to_day_format);   
    end_to_sec_date_str          = datestr(now,date_str_to_sec_format);      
    start_date                   = addtodate(now, -1, 'y');    
    start_to_day_date_str        = datestr(start_date,date_str_to_day_format); 
    start_to_sec_date_str        = datestr(start_date,date_str_to_sec_format);                
    return;
end 

end % fn parse_date_keyword_3

