bkgr_image_orig_jpeg    = imread('frame1.jpg');
foreg_image_orig_jpeg 	= imread('frame2.jpg');
subplot(3,3,1);
imshow(bkgr_image_orig_jpeg);
title('Background Frame 1');
subplot(3,3,2);
imshow(foreg_image_orig_jpeg);
title('Frame 2');
foregroundFrame2 = foreg_image_orig_jpeg > bkgr_image_orig_jpeg;
if ndims(foregroundFrame2) > 1
    % Convert from color to 2-D logical
    foregroundFrame2 = max(foregroundFrame2, [], 3);
end
subplot(3,3,3);
imshow(foregroundFrame2);
title('foreground Frame 2');