function [ ] = ui_create_plot_senso_block(     	...   % handles    %,material_type
    axes_handle                                 ...   % good or anoma
    ,x_data         ,y_data                     ...   % 2d matrices with times and grus to be plotted.
    ,y_data_enve    ,y_data_mean,y_data_median  ...
    ,x_label_str    ,y_label_str                ...
    ,title_str      ,legend_str                 ...
    ,a_title_color_str                          ...
    )
%UI_CREATE_PLOT_SENSO_BLOCK Drawns on the main window:  1 plot: good or anomalous (ie a block of sensogram curves.
% Plots as many curves as come on the x_data,y_data
% enve,median and mean gets plotted if they are not null.
% pipeline:  ui_create_plot_senso_block

bl_str_time  	= 4.4; % jas_temp_hardcoded  jas_hard_coded
bl_end_time   	= 4.9; % jas_temp_hardcoded  note: 5.4;
am_str_time    	= 7.7; % jas_temp_hardcoded
am_end_time    	= 8.2; % jas_temp_hardcoded
x_bl_str     = nan(2,1);
x_am_str     = nan(2,1);
x_bl_end     = nan(2,1);
x_am_end     = nan(2,1);

y_bl_str     = nan(2,1);
y_am_str     = nan(2,1);
y_am_end     = nan(2,1);
y_bl_end     = nan(2,1);

x_bl_str(1) = bl_str_time -0.0001;
x_bl_str(2) = bl_str_time +0.0001;
x_bl_end(1) = bl_end_time -0.0001;
x_bl_end(2) = bl_end_time +0.0001;

x_am_str(1) = am_str_time -0.0001;
x_am_str(2) = am_str_time +0.0001;
x_am_end(1) = am_end_time -0.0001;
x_am_end(2) = am_end_time +0.0001;

y_bl_str(1) = nanmin ( nanmin(nanmin(y_data)), nanmin(nanmin(y_data_enve))); % vert line start
y_bl_str(2) = nanmax ( nanmax(nanmax(y_data)), nanmax(nanmax(y_data_enve))); % vert line end
y_bl_end(1) = nanmin ( nanmin(nanmin(y_data)), nanmin(nanmin(y_data_enve))); % vert line start
y_bl_end(2) = nanmax ( nanmax(nanmax(y_data)), nanmax(nanmax(y_data_enve))); % vert line end

y_am_str(1) = nanmin ( nanmin(nanmin(y_data)), nanmin(nanmin(y_data_enve))); % vert line start
y_am_str(2) = nanmax ( nanmax(nanmax(y_data)), nanmax(nanmax(y_data_enve))); % vert line end
y_am_end(1) = nanmin ( nanmin(nanmin(y_data)), nanmin(nanmin(y_data_enve))); % vert line start
y_am_end(2) = nanmax ( nanmax(nanmax(y_data)), nanmax(nanmax(y_data_enve))); % vert line end
x_data          = x_data';
y_data          = y_data';
y_data_enve     = y_data_enve';
y_data_mean     = y_data_mean';
y_data_median   = y_data_median';

if ( (size(y_data,2) >2) && (sum(sum(~(isnan(y_data_enve))))) )
    x_data_enve = x_data(:,1:2);
else
    x_data_enve = nan; 
    y_data_enve = nan; 
end
if ( (size(y_data,2) >2) && (sum(~(isnan(y_data_mean)))))
    x_data_mean = x_data(:,1);
else
     x_data_mean = nan;
     y_data_mean = nan;     
end
if ( (size(y_data,2) >2) && (sum(~(isnan(y_data_median)))))
    x_data_median = x_data(:,1);  
else
    x_data_median = nan;
    y_data_median = nan;    
end

% do the actual _plotting_ draw as many curves as they come:
%[ fitted_curve, upper_bounds, lower_bounds ] = eval_model( fit_result, x_data_ext); % from: x_data to: x_data_ext
p = plot(axes_handle                            ...
    ,x_data,            y_data          ,'-'  	... % , '.b'    
    ,x_data_mean, 	    y_data_mean     ,'-.r' 	...
    ,x_data_median,     y_data_median   ,'-.b'  ...
    ,x_data_enve,       y_data_enve     ,'-.r'  ...     % '.r'  circles: 'or'
    ,x_bl_str,          y_bl_str        ,'-k'  	...    
    ,x_bl_end,          y_bl_end        ,'-k'  	...    
    ,x_am_str,          y_am_str        ,'-k'  	...    
    ,x_am_end,          y_am_end        ,'-k'  	...        
    );
%,x_data_excluded, y_data_excluded,'*g'  ...
%,x_data_linear, y_data_linear,'.g');  % show the  green  line:    linear
%,x_data_linear, y_data_linear,'-g'     % show the magenta starts: linear  again
hold all;
ref_lines_cnt = size(y_data,2);

ref_lines_cnt = ref_lines_cnt +1;
if ( sum(~(isnan(y_data_mean))) > 0)
    p(ref_lines_cnt).LineWidth = 2; 
end

ref_lines_cnt = ref_lines_cnt +1;
if ( sum(~(isnan(y_data_median))) > 0)
    p(ref_lines_cnt).LineWidth = 2;     
end

ref_lines_cnt = ref_lines_cnt +1;
if ( sum(sum(~(isnan(y_data_enve)) )) >0)
    p(ref_lines_cnt).LineWidth = 2; % enve_btm
    ref_lines_cnt = ref_lines_cnt +1;    
    p(ref_lines_cnt).LineWidth = 2; % enve_top
end


% for i=1:1:ref_lines_cnt
%     p(i).LineWidth = 20; 
% end
% p(1).LineWidth = 4; % enve_top
% p(2).LineWidth = 4; % enve_btm
% p(3).LineWidth = 4; % mean
% p(4).LineWidth = 4; % median
set(axes_handle,'FontSize',12);
title(axes_handle,title_str,'Color',a_title_color_str); % sprintf(' GOOD SENSOGRAMS: %-3d-%-3d of %-3d', ,   ))
%std_qc_resize_plots_flag_save = std_qc_resize_plots_flag; %jas_matlab_bug: global variable gets wiped_dout with legend_cmd
if (~(isnan(legend_str)))
    legend( axes_handle, legend_str); % South
end
% Label axes
% Leave out x-axis label to save real state: The residual plot on the btm has the same label.
xlabel(axes_handle, x_label_str );
% std_qc_resize_plots_flag = std_qc_resize_plots_flag_save;
ylabel(axes_handle, y_label_str );  %  strcat(y_label_str,' - GRU') );
set(axes_handle,'XGrid','on');
set(axes_handle,'YGrid','on');
% if (~(isnan(y_data_enve   ,y_data_mean             ...
%     ,y_data_median
%grid on
hold off;
end % ui_create_plot_senso_block

% left overs:
% %
% % if (~(do_interpolation_flag))
% % % 2. Plot residuals. ____________________________________________________________
% %     zero_line = zeros(size(fit_output.residuals));
% %     [ x_residual_plot_data, y_residual_plot_data ] = gen_array_to_plot_residuals( x_data,fit_output.residuals  );
% %     plot(handles_axes_residuals ...
% %         ,x_data,fit_output.residuals ,'*b'                    ... % to produce lollypops on top
% %         ,x_data,zero_line           ,'-r'  ...                ... % to produce a red line that matches the label
% %         ,x_residual_plot_data,y_residual_plot_data,'-b'       ... % to produce vertical lines   ( no label )
% %         ,x_data,zero_line           ,'-r'  );
% %     hold all;
% %     legend(handles_axes_residuals, strcat(legend_fit_str,'- Res.'), 'Zero Line', 'Location', 'NorthEast');
% %     hold all;
% %
% %     % Set title
% %     set(handles_axes_residuals,'FontSize',9);
% %     title(handles_axes_residuals,'RESIDUALS')
% %     % Label axes
% %     xlabel(handles_axes_residuals, x_label_str );
% %     ylabel(handles_axes_residuals, 'Residual - GRU'); % y_label_str );
% %     grid on
% %     hold off;
% %
% % else
% %     % DOING INTERPOLATION AND BEST LINEAR MODEL: Do lin scale using interpolated data:  (Use residuals axes)
% %
% %     %[ fitted_curve, upper_bounds, lower_bounds ] = eval_model( fit_result, x_data_inter);
% %     h = plot(handles_axes_residuals    ...
% %         ,x_data_inter, y_data_inter,   '.r'...
% %         ,x_data_best_linear, y_data_best_linear,'-g' ...  % show the black  line:    linear
% %         ,x_data_best_linear, y_data_best_linear,'.k' ...  % show the green starts: linear  again
% %         );
% %     hold all;
% %     set(handles_axes_residuals,'FontSize',9);
% %     title(handles_axes_residuals,' FIT INTERPOLATED AND BEST LINEAR MODEL')
% %     legend(handles_axes_residuals, legend_raw_str,strcat(legend_fit_str ,' Lin-Model'), 'Location','SouthEast'); % South
% %     % Label axes
% %     % Leave out x-axis label to save real state: The residual plot on the btm has the same label.
% %     xlabel(handles_axes_residuals, strcat(' ',x_label_str) );
% %     ylabel(handles_axes_residuals, strcat( y_label_str,' - GRU') );
% %     set(handles_axes_residuals,'XGrid','on');
% %     set(handles_axes_residuals,'YGrid','on');
% %     %grid on
% %     hold off;
% %
% %
% % end % NOT interp and lin scale with fit and best lin model.
% % std_qc_resize_plots_flag = std_qc_resize_plots_flag_save; % matlab_bug after legend global gets wipedout
% % % 2. Plot residuals. ____________________________________________________________
% %
% %
% % % orig:
% % % h = plot(handles_axes_residuals, x_data,fit_output.residuals ,'.b'   ...
% % %     ,                            x_data,zero_line            ,'-r'  );
% %
% % % 3. Plot params.  handles.axes_params ___________________________________________
% %
% % temp            = confint(fit_result);
% % num_coef        = size(temp,2);
% % params_mat      = zeros(3,num_coef);
% % params_mat(1,:) = temp(1,:);
% % params_mat(2,:) = coeffvalues(fit_result);
% % params_mat(3,:) = temp(2,:);
% % labels_str = coeffnames(fit_result);
% % boxplot(handles.axes_params,params_mat,'boxstyle', 'outline'  ,'labels',labels_str);
% % hold all;
% % set(handles.axes_params,'FontSize',9,'YGrid','on');
% % title(handles.axes_params,strcat('FIT PARAMS ',fit_params_title_str));
% % hold off;


%,figure_name_str,  title_str,    gof
%legend('boxoff')
%legend('FontSize','5.0')
%legend( h,'FontSize','5.0');    'Location',  'Location',

% % % jas_temp: Create a 25% extrapolation to show effect of extrapolating in the confidence_bounds (graphics ONLY)
% % x_data_ext = x_data;
% % x_data_ext(end+1) = x_data(end) + ( range(x_data) * 0.25 );
