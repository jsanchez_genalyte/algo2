function [ cur_ndx_ori ] = ui_cvt_popmenu_to_ori_cur_ndx(ui_popup_state_struct, popupmenu_sel_index )  % cur_ ndx popupmenu_ probe_sel_index
%UI_CVT_POPMENU_TO_ORI_CUR_NDX Summary of this function goes here
%   given a popup menu, converts an cur_ndx (i.e. popupmenu_sel_index to an original index:
%   i.e index on current list into a index of the original list. see: ui_cvt_popmenu_to_cur_cur_ndx

% convert popupmenu_sel_index from being an ndx of the current list into an ndx of the orig list:
cur_ndx_ori = 0;  % initialize with something invalid.
probe_name_sel_cur = ui_popup_state_struct.list_str_cel{popupmenu_sel_index};
% find the ndx of this name in the original list:
[ element_found_flag, element_ndx ] = is_element_in_cell(ui_popup_state_struct.list_str_cel_ori,probe_name_sel_cur);
if (element_found_flag)
    cur_ndx_ori  = element_ndx;
else
    % REPORT ERRROR:
    display(sprintf(' Invalid index in popup menu current: %4d for popup probe' ,popupmenu_sel_index));
    display(ui_popup_state_struct.list_str_cel_ori);
    display(ui_popup_state_struct.list_str_cel);
end

end % fn ui_cvt_popmenu_to_ori_cur_ndx

