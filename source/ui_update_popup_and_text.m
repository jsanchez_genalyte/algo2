function [ ui_state_struct_cat ] = ui_update_popup_and_text(ui_state_struct_cat,handle_popup_categ,handle_text_categ,new_cat_name   )
%UI_UPDATE_POPUP_AND_TEXT Updates the popup and text field on top of the popup name with the new determined lists and quantities.
%   This function is to be called for all the std popup after having updated the  ui_state_struct as product of a user popup action
%   sample call:
%   ui_state_struct.probe{1} = ui_update_popup_text(ui_state_struct.probe{1},popupmenu_probe,text_probe,'PROBE    ');
%   pipeline: Sensogram_Analysis_App_10>popupmenu_probe_Callback --> sa_ui_set_popupmenus_after_popup_change --> ui_update_popup_and_text
% list_str_cel_ori_dat= ui_state_struct_cat.list_str_cel_ori_dat; % pre_mpopup
% list_str_cel_cur_dat= ui_state_struct_cat.list_str_cel_cur_dat;
% list_str_cel_cur_gui= ui_state_struct_cat.list_str_cel_cur_gui;
cur_gui_ndx        	= ui_state_struct_cat.cur_gui_ndx;
%new_cur_dat_len    	= length(list_str_cel_cur_dat);             % pre_mpopup
%list_str_cel_ori_gui = ui_state_struct_cat.list_str_cel_ori_gui;
%list_str_cel_cur_gui = ui_state_struct_cat.list_str_cel_cur_gui;

list_item_names     = ui_state_struct_cat.list_item_names;
list_item_slected   = ui_state_struct_cat.list_item_slected;
list_item_enabled   = ui_state_struct_cat.list_item_enabled;

if (length(list_item_slected) == 1)
    display ('wrong size');
end

if( cur_gui_ndx ==1 )
    % ALL SELECTED:
    list_item_slected(4:end) = 1; % all single selected
end

if (length(list_item_slected) == 1)
    display ('wrong size');
end

mpopup_menu_set_items( handle_popup_categ,list_item_names,list_item_slected,list_item_enabled,cur_gui_ndx);

item_slected_cnt    = sum(list_item_slected(4:end));
item_slected_avai   = sum(list_item_enabled(4:end));   % length(list_item_slected) - 3;

if (item_slected_cnt < item_slected_avai )
    % SELECTED LESS THAN AVAILABLE
    text_str = sprintf('%s%-5d/ %-4d',new_cat_name,item_slected_cnt,item_slected_avai);
    set(handle_text_categ,'String',strrep(text_str,'  ',' '));
else
    % SELECTED ALL AVAILABLE
    text_str = sprintf('%s%-5d',new_cat_name,item_slected_cnt);
    set(handle_text_categ,'String',strrep(text_str,'  ',' '));
end

end % fn ui_update_popup_and_text