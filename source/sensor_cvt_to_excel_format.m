function [ exp_sensor_excel_final ] = sensor_cvt_to_excel_format( probe_map_table,sensor_all_maps_table,exp_sensor_table )
%SENSOR_CVT_TO_EXCEL_FORMAT Produces a 'exp_sensor_excel_final' matix with the same format as the excel rpt from jervis (
%   Input:  probe_map_table         from the protocol
%           sensor_all_maps_table   136x4(cols) sensors with all the maps to facilitate the conversion  
%           exp_sensor_table        data retrieved from the sensor jervis-API to be converted
% output:  exp_sensor_excel_final   matrix to be logged into the xml file. first 2 cols are channel and sensor_num within probe: nxt 16 cols is true analyte data
% S1. Create  list of analyte names
% S2. Create matrix  to store analyte gru.
% S3. Populate matrix
 
name_all                = probe_map_table.name;
analyte_all_cnt         = length(name_all);
analyte_all_cnt_2       = analyte_all_cnt/2; 

category                =  probe_map_table.category;
category_wanted_ndx     = find(category == 1 | category == 4);
category_wanted_2_ndx  =  category_wanted_ndx(1:analyte_all_cnt_2); % need only the first half.
name_analyte            = name_all(category_wanted_ndx);    
analyte_cnt             = length(name_analyte);
analyte_cnt_2           = analyte_cnt/2; 
analyte_wanted_2_ndx    = category_wanted_2_ndx(1:analyte_cnt_2);
exp_sensor_excel        = nan(8,analyte_all_cnt_2); % first 2 are channel (1 or 2) and sensor_numb (1 to 4)
exp_sensor_channel_excel= nan(8,2); % first 2 are channel (1 or 2) and sensor_numb (1 to 4)

for cur_sensor_ndx  =1:1:136 
    if( sensor_all_maps_table.analyte_flag(cur_sensor_ndx))
        % TRUE ANALYTE
        cur_channel             = exp_sensor_table.channel(cur_sensor_ndx);
        cur_col                 = sensor_all_maps_table.analyte_ndx(cur_sensor_ndx);
        if (cur_col >  analyte_all_cnt_2)
            cur_col = cur_col - analyte_all_cnt_2;
        end
        if (cur_col >  analyte_all_cnt_2)
            disp(cur_col)
        end        
        cur_probe_sensor_number  = sensor_all_maps_table.probe_sensor_number(cur_sensor_ndx);
        if (cur_channel == 1)
            % FIRST BLOCK:
            cur_row = cur_probe_sensor_number;
        end
        if (cur_channel == 2)
            % SECOND BLOCK:
            cur_row = cur_probe_sensor_number+4;
        end
        results =exp_sensor_table.results(cur_sensor_ndx);
        results = results{1,1};
        if (~(isempty(results)))
            exp_sensor_excel(cur_row,cur_col)                       = results.normalized_value;
            sensor_all_maps_table.normalized_value(cur_sensor_ndx)  = results.normalized_value; % to make easy the dbg/verification.
        end
    end
end % for each of the 136 sensors.

% HAVE THE EXCEL MATRIX WITH ALL THE DATA CONVERTED TO THE NEW FORMAT: take only the true analytes:

exp_sensor_excel_final = exp_sensor_excel(1:8,analyte_wanted_2_ndx);

channel_array           = [ 1; 1; 1; 1; 2; 2; 2; 2];
sensor_array            = [ 1:4  1:4];
channel_sensor_array    = [ channel_array sensor_array'];

exp_sensor_excel_final = [ channel_sensor_array  exp_sensor_excel_final ];


end % fn sensor_cvt_to_excel_format
