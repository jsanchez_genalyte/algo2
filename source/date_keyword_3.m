function    [ req_start_date_str, req_end_date_str ] = date_keyword_3(wanted_keyword)
%DATE_KEYWORD_3 converts a wanted_keyword of 3 words into concrete start and end dates:
% sample call:   [ req_start_date_str, req_end_date_str ] = date_keyword_3('LAST 7 DAYS');

% keywords_3 = { 'LAST 7 DAYS','LAST 30 DAYS' };
req_end_date_str   = '';
req_start_date_str = '';

time_now = datetime('now');

date_str_format = 'mm/dd/yyyy';
if (strcmp(wanted_keyword,'LAST 7 DAYS'))
    %
    start_date = time_now-days(7);
    req_start_date_str = datestr(start_date,date_str_format);
    req_end_date_str   = datestr(time_now,date_str_format);
end
if (strcmp(wanted_keyword,'LAST 30 DAYS'))
    %
    %start_date         = time_now-days(30);                % this is an approx. 
    start_date          = time_now- calmonths(1);           % this is more accurate calendar month.
    req_start_date_str = datestr(start_date,date_str_format);
    req_end_date_str   = datestr(time_now,date_str_format);
end
end % fn date_keyword_3

