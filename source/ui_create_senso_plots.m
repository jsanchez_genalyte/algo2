function [  ] = ui_create_senso_plots( input_args )
%UI_CREATE_SENSO_PLOTS do the acttual plots (on whichever axes are passed as params: 
%   Detailed explanation goes here
function [ ] = create_fits_plots_main(      ...
    handles                                 ...
    ,material_type                          ...
    ,x_data,            y_data              ...
    ,x_data_excluded   ,y_data_excluded     ...        
    ,x_data_inter,   	y_data_inter        ...
    ,x_data_linear,  	y_data_linear       ...    
    ,x_data_best_linear,y_data_best_linear  ...        
    ,x_label_str,   	y_label_str         ...
    ,legend_raw_str,    legend_fit_str    	...
    ,fit_result,        fit_output          ...
    ,do_interpolation_flag)
%CREATE_FITS_PLOTS_MAIN Drawns on the main window 3 plots:
% 1. Plot fit with data. 2. Plot residuals. 3. Plot parameters (pseudo box-plot)
% 1. Plot fit with data. ____________________________________________________________
% h = plot(handles_axes_main_fit, fit_result, x_data, y_data, 'predobs' );

declare_std_curve_globals;  

% Select proper axes to do the plotting:
fit_params_title_str='';
if (material_type == material_type_ref)   %jas_tbd: replace 1 with predefined cte. from globals or cfg parameters.
    % Plotting REF:
    handles_axes_main_fit =  handles.axes_main_fit;
    handles_axes_residuals = handles.axes_residuals;
    fit_params_title_str   = '- REFERENCE';
end
if (std_qc_flag == 1)
    % DOING QC: Overwrite tile for params:
    fit_params_title_str   = '- STANDARD CURVE';
end
if (material_type == material_type_gnm)
    % Plotting GNM:    
    handles_axes_main_fit =  handles.axes_main_fit_gnm;
    handles_axes_residuals = handles.axes_residuals_gnm;    
    fit_params_title_str   = '- GENALYTE';    
end
if (material_type == material_type_std)
    % Plotting GNM:    
    handles_axes_main_fit =  handles.axes_main_fit_std;
    handles_axes_residuals = handles.axes_residuals_std;   
    fit_params_title_str   = '- STANDARD CURVE';    
end
    
% jas_temp: Create a 25% extrapolation to show effect of extrapolating in the confidence_bounds (graphics ONLY)
x_data_ext = x_data;
x_data_ext(end+1) = x_data(end) + ( range(x_data) * 0.25 );

[ fitted_curve, upper_bounds, lower_bounds ] = eval_model( fit_result, x_data_ext); % from: x_data to: x_data_ext
	plot(handles_axes_main_fit        ...
    ,x_data,     y_data      ,       '.b'  ...
    ,x_data_ext, fitted_curve,       'r'   ...   % from: x_data to: x_data_ext
    ,x_data_ext,  upper_bounds,      '-.r' ...
    ,x_data_ext, lower_bounds,       '-.r' ...
    ,x_data_excluded, y_data_excluded,'*g'  ...    
    ,x_data_linear, y_data_linear,'.g');  % show the  green  line:    linear
    %,x_data_linear, y_data_linear,'-g'     % show the magenta starts: linear  again

hold all;
set(handles_axes_main_fit,'FontSize',9);
title(handles_axes_main_fit,' FIT RESULTS')
std_qc_resize_plots_flag_save = std_qc_resize_plots_flag; %jas_matlab_bug: global variable gets wiped_dout with legend_cmd
legend( handles_axes_main_fit, legend_raw_str,legend_fit_str ,'Bounds', 'Location','SouthEast'); % South
% Label axes
% Leave out x-axis label to save real state: The residual plot on the btm has the same label.
%xlabel(handles_axes_main_fit, x_label_str );
std_qc_resize_plots_flag = std_qc_resize_plots_flag_save;
ylabel(handles_axes_main_fit, strcat(y_label_str,' - GRU') );
set(handles_axes_main_fit,'XGrid','on');
set(handles_axes_main_fit,'YGrid','on');
%grid on
hold off;
if (~(do_interpolation_flag))
% 2. Plot residuals. ____________________________________________________________    
    zero_line = zeros(size(fit_output.residuals));
    [ x_residual_plot_data, y_residual_plot_data ] = gen_array_to_plot_residuals( x_data,fit_output.residuals  );
    plot(handles_axes_residuals ...
        ,x_data,fit_output.residuals ,'*b'                    ... % to produce lollypops on top
        ,x_data,zero_line           ,'-r'  ...                ... % to produce a red line that matches the label
        ,x_residual_plot_data,y_residual_plot_data,'-b'       ... % to produce vertical lines   ( no label )
        ,x_data,zero_line           ,'-r'  );
    hold all;
    legend(handles_axes_residuals, strcat(legend_fit_str,'- Res.'), 'Zero Line', 'Location', 'NorthEast');
    hold all;
    
    % Set title
    set(handles_axes_residuals,'FontSize',9);
    title(handles_axes_residuals,'RESIDUALS')
    % Label axes
    xlabel(handles_axes_residuals, x_label_str );
    ylabel(handles_axes_residuals, 'Residual - GRU'); % y_label_str );
    grid on
    hold off;    
    
else
    % DOING INTERPOLATION AND BEST LINEAR MODEL: Do lin scale using interpolated data:  (Use residuals axes)
    
    %[ fitted_curve, upper_bounds, lower_bounds ] = eval_model( fit_result, x_data_inter);
    h = plot(handles_axes_residuals    ...
        ,x_data_inter, y_data_inter,   '.r'...
        ,x_data_best_linear, y_data_best_linear,'-g' ...  % show the black  line:    linear
        ,x_data_best_linear, y_data_best_linear,'.k' ...  % show the green starts: linear  again
        );
    hold all;
    set(handles_axes_residuals,'FontSize',9);
    title(handles_axes_residuals,' FIT INTERPOLATED AND BEST LINEAR MODEL')
    legend(handles_axes_residuals, legend_raw_str,strcat(legend_fit_str ,' Lin-Model'), 'Location','SouthEast'); % South
    % Label axes
    % Leave out x-axis label to save real state: The residual plot on the btm has the same label.
    xlabel(handles_axes_residuals, strcat(' ',x_label_str) );
    ylabel(handles_axes_residuals, strcat( y_label_str,' - GRU') );
    set(handles_axes_residuals,'XGrid','on');
    set(handles_axes_residuals,'YGrid','on');
    %grid on
    hold off;
    
    
end % NOT interp and lin scale with fit and best lin model.
std_qc_resize_plots_flag = std_qc_resize_plots_flag_save; % matlab_bug after legend global gets wipedout
% 2. Plot residuals. ____________________________________________________________
 

% orig:
% h = plot(handles_axes_residuals, x_data,fit_output.residuals ,'.b'   ...
%     ,                            x_data,zero_line            ,'-r'  );

% 3. Plot params.  handles.axes_params ___________________________________________

temp            = confint(fit_result);
num_coef        = size(temp,2);
params_mat      = zeros(3,num_coef);
params_mat(1,:) = temp(1,:);
params_mat(2,:) = coeffvalues(fit_result);
params_mat(3,:) = temp(2,:);
labels_str = coeffnames(fit_result);
boxplot(handles.axes_params,params_mat,'boxstyle', 'outline'  ,'labels',labels_str);
hold all;
set(handles.axes_params,'FontSize',9,'YGrid','on');
title(handles.axes_params,strcat('FIT PARAMS ',fit_params_title_str));
hold off;
end % create_fits_plots_main

% left overs:
%,figure_name_str,  title_str,    gof
%legend('boxoff')
%legend('FontSize','5.0')
%legend( h,'FontSize','5.0');    'Location',  'Location',

end

