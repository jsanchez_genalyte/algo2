function [ excel_true_range_data, excel_true_row_last, excel_true_col_last ] = determine_excel_sensor_range_( an_excel_file_name )
%DETERMINE_EXCEL_SENSOR_RANGE returns the range with the true raw sensor data
%   It figures out where the data is on the sensor trace tab (The second on the file)   
% Returns the excel true range, and the last row and cols to be used to load the data.


% 1. Find row range: Read just the first column and determine the correct rows: _______________________________________

excel_range_data    = 'A18:A500'; % very large range down: Just one column
excel_tab_number    = 2;
raw_sensor_table     = readtable(an_excel_file_name,'Sheet',excel_tab_number, 'Range',excel_range_data','FileType','spreadsheet');

% Determine range of real row data:
raw_sensor_table_size      = size(raw_sensor_table);
raw_sensor_table_rows      = raw_sensor_table_size(1);
%temp                       = raw_sensor_table{:,1};


cur_row=1;
while cur_row <= raw_sensor_table_rows
    if (isnan(str2double(char(raw_sensor_table{cur_row,1}))))
        break;
    end
    cur_row=cur_row+1;
end
excel_true_row_last=cur_row-1; % go back one row from the first empty row
%
% 2. HAVE ROW RANGE. Find Column Range for real data __________________________________________________________________

excel_range_data     = 'A18:HZ19'; % very large range to the right: Just 1 row
raw_sensor_table     = readtable(an_excel_file_name,'Sheet',excel_tab_number, 'Range',excel_range_data','FileType','spreadsheet');
raw_sensor_col_names = raw_sensor_table.Properties.VariableNames;
raw_sensor_table_size= size(raw_sensor_table);
raw_sensor_table_cols= raw_sensor_table_size(2);

for cur_col=1:1:raw_sensor_table_cols
    if ~(isempty(strfind(char(raw_sensor_col_names{cur_col}), 'Var')))
        break;
    end
end
excel_true_col_last=cur_col-1;

% 3. HAVE RANGE OF ROW AND COLUMNS: Determine range string of real col data: __________________________________________

excel_col_names = {'A',	'B',	'C',	'D',	'E',	'F',	'G',	'H',	'I',	'J',	'K',	'L',	'M',	'N',	'O',	'P',	'Q',	'R',	'S',	'T',	'U',	'V',	'W',	'X',	'Y',	'Z',	'AA',	'AB',	'AC',	'AD',	'AE',	'AF',	'AG',	'AH',	'AI',	'AJ',	'AK',	'AL',	'AM',	'AN',	'AO',	'AP',	'AQ',	'AR',	'AS',	'AT',	'AU',	'AV',	'AW',	'AX',	'AY',	'AZ',	'BA',	'BB',	'BC',	'BD',	'BE',	'BF',	'BG',	'BH',	'BI',	'BJ',	'BK',	'BL',	'BM',	'BN',	'BO',	'BP',	'BQ',	'BR',	'BS',	'BT',	'BU',	'BV',	'BW',	'BX',	'BY',	'BZ',	'CA',	'CB',	'CC',	'CD',	'CE',	'CF',	'CG',	'CH',	'CI',	'CJ',	'CK',	'CL',	'CM',	'CN',	'CO',	'CP',	'CQ',	'CR',	'CS',	'CT',	'CU',	'CV',	'CW',	'CX',	'CY',	'CZ',	'DA',	'DB',	'DC',	'DD',	'DE',	'DF',	'DG',	'DH',	'DI',	'DJ',	'DK',	'DL',	'DM',	'DN',	'DO',	'DP',	'DQ',	'DR',	'DS',	'DT',	'DU',	'DV',	'DW',	'DX',	'DY',	'DZ',	'EA',	'EB',	'EC',	'ED',	'EE',	'EF',	'EG',	'EH',	'EI',	'EJ',	'EK',	'EL',	'EM',	'EN',	'EO',	'EP',	'EQ',	'ER',	'ES',	'ET',	'EU',	'EV',	'EW',	'EX',	'EY',	'EZ',	'FA',	'FB',	'FC',	'FD',	'FE',	'FF',	'FG',	'FH',	'FI',	'FJ',	'FK',	'FL',	'FM',	'FN',	'FO',	'FP',	'FQ',	'FR',	'FS',	'FT',	'FU',	'FV',	'FW',	'FX',	'FY',	'FZ',	'GA',	'GB',	'GC',	'GD',	'GE',	'GF',	'GG',	'GH',	'GI',	'GJ',	'GK',	'GL',	'GM',	'GN',	'GO',	'GP',	'GQ',	'GR',	'GS',	'GT',	'GU',	'GV',	'GW',	'GX',	'GY',	'GZ',	'HA',	'HB',	'HC',	'HD',	'HE',	'HF',	'HG',	'HH',	'HI',	'HJ',	'HK',	'HL',	'HM',	'HN',	'HO',	'HP',	'HQ',	'HR',	'HS',	'HT',	'HU',	'HV',	'HW',	'HX',	'HY',	'HZ'};
excel_true_col_last_name =  excel_col_names{excel_true_col_last};
excel_true_col_last_str  = strcat(excel_true_col_last_name,num2str(18+excel_true_row_last));

excel_true_range_data = strcat('A18:',excel_true_col_last_str);

end

