function [db_data_exp_struct  ] = fill_in_db_exp_from_exp_data(...
    expe_req_table,db_data_exp_struct )   % jas_here_thurs_ all to be reviewed with new arg: 
% jas: NIU maybe eliminate this function all together thurs
%FILL_IN_DB_EXP_FROM_EXP_DATA  fills in a db_exp_set from exp data retrieved from the db after a ui request_data push.
%   Wipes out the existing experiments in memory.
% pipeline:    Sensogram_Analysis_App_10 -> pushbutton_get_data_Callback --> sa_do_push_req_exp -> fill_in_db_exp_from_exp_data
%db_data_exp_struct  = nan;

data_exp_cnt = size(exp_list_cel,1);

% metadata:
emd_ndxdb           =  cell(data_exp_cnt,1); % 1  ; %
emd_exper           =  cell(data_exp_cnt,1); % 2  ; % run date as a string.
emd_instr           =  cell(data_exp_cnt,1); % 3  ; %
emd_carri         	=  cell(data_exp_cnt,1); % 4  ; %
emd_spotl        	=  cell(data_exp_cnt,1); % 5  ; %
emd_kitlo       	=  cell(data_exp_cnt,1); % 6  ; %
emd_proto           =  cell(data_exp_cnt,1); % 7  ; %
emd_dates          	=  cell(data_exp_cnt,1); % 8  ; %
emd_state          	=  cell(data_exp_cnt,1); % 9  ; %
emd_futur       	=  cell(data_exp_cnt,1); % 10 ; %
emd_eguid       	=  cell(data_exp_cnt,1); % 11 ; %

date_str    = cellstr(exp_list_cel{6});
date_str    = strrep(date_str,'(UTC)','');
date_str    = strrep(date_str,' ','');
% replicate the date as a number (for efficiency)
%datef       (row_start:row_start+shift_curve_cnt-1) = datenum(date_str);
ndxdb = 1:data_exp_cnt;
emd_ndxdb   (row_start:row_start+shift_curve_cnt-1) = num2cell(ndxdb);
emd_exper   (row_start:row_start+shift_curve_cnt-1) = cellstr(expe_req_table    );
emd_instr   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,5));
emd_carri   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));
emd_spotl   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));
emd_kitlo   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));
emd_proto   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));
emd_dates   (row_start:row_start+shift_curve_cnt-1) = cellstr(int2str(datenum(date_str)));  % date_str;
emd_state   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));
emd_futur   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));
emd_eguid   (row_start:row_start+shift_curve_cnt-1) = cellstr(exp_list_cel(:,1));

db_metad_exp_struct = struct;

db_metad_exp_struct.ndxdb           = categorical(emd_ndxdb	,'Ordinal',true);
db_metad_exp_struct.exper           = categorical(emd_exper	,'Ordinal',true);
db_metad_exp_struct.instr        	= categorical(emd_instr	,'Ordinal',true);
db_metad_exp_struct.carri        	= categorical(emd_carri	,'Ordinal',true);
db_metad_exp_struct.spotl        	= categorical(emd_spotl	,'Ordinal',true);
db_metad_exp_struct.kitlo        	= categorical(emd_kitlo	,'Ordinal',true);
db_metad_exp_struct.proto        	= categorical(emd_proto	,'Ordinal',true);
db_metad_exp_struct.dates        	= categorical(emd_dates	,'Ordinal',true);
db_metad_exp_struct.state        	= categorical(emd_state	,'Ordinal',true);
db_metad_exp_struct.futur        	= categorical(emd_futur	,'Ordinal',true);

db_data_exp_struct.db_metad_exp_struct = db_metad_exp_struct;


end % fn fill_in_db_exp_from_exp_data
