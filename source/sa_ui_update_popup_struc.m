function [ ui_pop_up_struct ] = sa_ui_update_popup_struc( ...
    ui_pop_up_struct,new_list_str_cel_cur_dat,prev_popupmenu_sel_index )
%SA_UI_UPDATE_POPUP_STRUC updates a standard struct for popup menus.
%   Input:  ui_pop_up_struct:           structure to be updated.
%           new_list_str_cel_cur_dat:   new list of names to be used (result of anding all the filters) 
%           prev_popupmenu_sel_index:  	current index (cur_gui_ndx)   (it may be oudated because the list_str_cel_cur_gui is dynamic!!! 
%   Output: ui_pop_up_struct withe these fields:
%           list_str_cel_cur_dat  	list of names passed or restored list with curr dat   names (in case of all)
%           new_list_str_cel_cur_gui	list_str_cel_cur_dat extended to all and Search
%           list_ndx_filt_ori_dat       filter to reflect what is currently selected. (relative to dat)
%           cur_gui_ndx                 ndx of cur selection updated to reflect a new new_list_str_cel_cur_gui.
%           cur_dat                     ndx of list_str_cel_cur_dat (zero for All)
% pipeline: sa_ui_set_popupmenus_after_popup_change -> sa_ui_update_popup_struc

% NIU convert prev_popupmenu_sel_index from being an ndx of the current list into an ndx of the orig list:
% NIU [ cur_ndx_ori ]              	= ui_cvt_popmenu_to_ori_cur_ndx(ui_pop_up_struct, prev_popupmenu_sel_index );

% make sure new_list_str_cel_cur_dat is a column vector
new_list_str_cel_cur_dat            = util_transpose_to_col_vector(new_list_str_cel_cur_dat);
list_str_cel_ori_dat                = ui_pop_up_struct.list_str_cel_ori_dat;
list_str_len_ori_dat                = length(list_str_cel_ori_dat);
pop_name_cur_sel                    = ui_pop_up_struct.list_str_cel_cur_gui{prev_popupmenu_sel_index};

pop_up_len_cur_gui                	= length(ui_pop_up_struct.list_str_cel_cur_gui);
new_list_str_cel_cur_gui         	= { 'All' new_list_str_cel_cur_dat{1:end} 'Search...'};  
% Handle special calses: first(all) or last  (custom,searh,tr etc). indices
if ( prev_popupmenu_sel_index == 1)
    % User had selected ALL: build ndx with all data fields:   % for case when going back to ALL 
    list_ndx_filt_ori_dat         	= ones(list_str_len_ori_dat,1);
    cur_gui_ndx                     = 1;  % maintain the 'All' selection' from the end user
    cur_dat_ndx                  	= 0;  % 0 means all (this is invalid ndx inentionally)    
end
if ( prev_popupmenu_sel_index == pop_up_len_cur_gui)
    if (strcmpi(ui_pop_up_struct.pop_up_name,'PROBE'))  
        % DEALING WITH SPECIAL CASE: Probes (the last field is Tr rather than search fix: 2017_10_11 
        cur_gui_ndx                	= 2; % now the only 2 options will be 1=all 2 = tr.  prev_popupmenu_sel_index;
        cur_dat_ndx                 = 1; % to indicate out of range: special case.
        new_list_str_cel_cur_dat    = { 'Tr' };
        new_list_str_cel_cur_gui    = { 'All' 'Tr' };
        new_list_str_cel_cur_gui    = new_list_str_cel_cur_gui';   
        list_ndx_filt_ori_dat       = zeros(list_str_len_ori_dat,1); % flag to indicate Tr:    empty    list_ndx_filt_ori_dat
        list_ndx_filt_ori_dat(1)    = 1; % As all the trs are the same: use the trs for the first probe. (need to add to filter: chan and clust)
    else
        % User selected CUSTOM: build ndx with all data fields: (jas_temp_tbd)
        list_ndx_filt_ori_dat     	= ones(list_str_len_ori_dat,1);
        cur_gui_ndx               	= list_str_len_ori_dat;
        cur_dat_ndx               	= 0;
    end
end
% Handle single cases:in the middle
if ( ( prev_popupmenu_sel_index > 1) && ( prev_popupmenu_sel_index < pop_up_len_cur_gui))
    % INDIVIDUAL SELECTION: find the ndx of this name in the current list:
     
    % find the position in cur dat: of pop_name_cur_sel  to update  cur_dat_ndx
    [dat_ele_found_flag,dat_ele_ndx]= is_element_in_cell(new_list_str_cel_cur_dat,pop_name_cur_sel);
    if (dat_ele_found_flag)
        cur_dat_ndx                 = dat_ele_ndx;  %
    else
        % ERROR:
        display(' Errror in sa_ui_update_propup_struct: using new_list_str_cel_cur_dat');
        display(new_list_str_cel_cur_dat);
    end
    
    list_ndx_filt_ori_dat        	= zeros(list_str_len_ori_dat,1);
    % find the position: of pop_name_cur_sel  to update  cur_gui_ndx
    [element_found_flag,element_ndx]= is_element_in_cell(list_str_cel_ori_dat,pop_name_cur_sel);
    if (element_found_flag)
        cur_gui_ndx                 = 2; % element_ndx+1;% move it down by one because the first one is alwaysL 'All'.
        list_ndx_filt_ori_dat(element_ndx)= 1;      % turn on the single selection
    else
        % ERROR:
        display(' Errror in sa_ui_update_propup_struct: using list_str_cel_ori_dat');
        display(list_str_cel_ori_dat);        
    end
end % single selection
 
%  Update standard struct:
ui_pop_up_struct.list_str_cel_cur_dat   = new_list_str_cel_cur_dat;   % changes according to the current selection (this popup or other popups)
ui_pop_up_struct.list_str_cel_cur_gui   = new_list_str_cel_cur_gui;
ui_pop_up_struct.list_ndx_filt_ori_dat 	= list_ndx_filt_ori_dat;   % last explicit user selected (goes with list_str_cel_ori_dat) ***jas_test_no_brackets_***
ui_pop_up_struct.cur_dat_ndx         	= cur_dat_ndx;             % in cur_dat coord
ui_pop_up_struct.cur_gui_ndx            = cur_gui_ndx;             % in cur gui coord
end % fn sa_ui_update_popup_struc
%     %,'list_ndx_filt_ind',{list_ndx_filt_ind     }...


% leftovers_to_restore_the_original list:
% % if (strcmpi(new_list_str_cel_cur_dat{cur_gui_ndx},'All Except Tr') || strcmpi(new_list_str_cel_cur_dat{cur_gui_ndx},'All') || strcmpi(new_list_str_cel_cur_dat{cur_gui_ndx},'Both'))
% %     % user had selected: all items on the list:    
% %     pop_up_len_ori              = size(ui_pop_up_struct.list_str_cel_ori,1);
% %     list_ndx_filt_ori_dat           = ones(pop_up_len_ori,1);
% %     list_ndx_filt_ori_dat(end)      = 0;  % zero out custom
% %     list_str_cel                = ui_pop_up_struct.list_str_cel_ori;    
% %     list_ndx_filt_net           = ones(new_popup_len_cur_dat,1);
% %     list_ndx_filt_net(end)      = 0;   % custom is not included in All crieria.
% % else
% %     list_str_cel                = new_list_str_cel_cur_gui;
% %     list_ndx_filt_ori_dat           = zeros(pop_up_len_ori,1);  
% %     list_ndx_filt_ori_dat(cur_gui_ndx)  = 1;    
% %     list_ndx_filt_net           = list_ndx_filt_ori_dat;
% % end

% % % % % handle special caseL All, or 'Both;
% % % % if (       strcmpi(ui_pop_up_struct.list_str_cel_ori{cur_ndx_ori},'All Except Tr') ...
% % % %         || strcmpi(ui_pop_up_struct.list_str_cel_ori{cur_ndx_ori},'All'          ) ...
% % % %         || strcmpi(ui_pop_up_struct.list_str_cel_ori{cur_ndx_ori},'Both'         ))
% % % %     % user had selected: ALL items on the list:    
% % % %     list_ndx_filt_ori_dat               = ones(pop_up_len_ori,1);
% % % %     list_ndx_filt_ori_dat(end)          = 0;    % zero out custom: It is not included in All crieria.
% % % % 
% % % % else
% % % %     % user had selected: an Individual item on the list:      
% % % %     list_ndx_filt_ori_dat               = zeros(pop_up_len_ori,1);  
% % % %     list_ndx_filt_ori_dat(cur_ndx_ori)  = 1;    
% % % % end

