function [ ] = lprintf_log( format_string,varargin )
%LPRINTF_LOG prints to command_window and/or to a log file
%   hard_coded:  log filename flag to decide where to log: file and/or comand_window: log_to_file_flag and log_to_cmdw_flag
%   USAGE: global change in all source files: except this one: replace: fprintf with lprintf_log
%   rev:   1.00 04_30_2018
log_to_file_flag = 1;
log_to_cmdw_flag = 1;
arg_cnt          = size(varargin,2);

if (isdeployed || log_to_file_flag)
    file_name_log = 'sa_log.txt';
    file_id = fopen(file_name_log,'a');  %'a' is for append. 
    if (strcmp(format_string,'do_close_log'))
        fprintf(file_id,'\nNORMAL_EXIT..bye\n');
        fclose(file_id);
        return;
    end
end

try
    if (isdeployed ||  log_to_file_flag)
        % WRITING TO A LOG FILE
        %  errors out:  fprintf(file_id,format_string,varargin);
        switch arg_cnt
            case 0
                fprintf(file_id,format_string);
            case 1
                fprintf(file_id,format_string,varargin{1});
            case 2
                fprintf(file_id,format_string,varargin{1},varargin{2});
            case 3
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3});
            case 4
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3},varargin{4});
            case 5
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3});
            case 6
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6});
            case 7
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7});
            case 8
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7},varargin{8});
            case 9
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7},varargin{8},varargin{9});
            case 10
                fprintf(file_id,format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7},varargin{8},varargin{9},varargin{10});
            otherwise
                fprintf('\nERROR: too many args: %-4d. Only 10 are implemented: see function lprintf_log = \n',arg_cnt);
        end
    end
    if (log_to_cmdw_flag)
        % WRITING TO CMD WINDOW
        % fprintf(format_string,varargin);
        switch arg_cnt
            case 0
                fprintf(format_string);
            case 1
                fprintf(format_string,varargin{1});
            case 2
                fprintf(format_string,varargin{1},varargin{2});
            case 3
                fprintf(format_string,varargin{1},varargin{2},varargin{3});
            case 4
                fprintf(format_string,varargin{1},varargin{2},varargin{3},varargin{4});
            case 5
                fprintf(format_string,varargin{1},varargin{2},varargin{3});
            case 6
                fprintf(format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6});
            case 7
                fprintf(format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7});
            case 8
                fprintf(format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7},varargin{8});
            case 9
                fprintf(format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7},varargin{8},varargin{9});
            case 10
                fprintf(format_string,varargin{1},varargin{2},varargin{3},varargin{4},varargin{5},varargin{6},varargin{7},varargin{8},varargin{9},varargin{10});
            otherwise
                fprintf('\nERROR: too many args: %-4d. Only 10 are implemented: see function lprintf_log = \n',arg_cnt);
        end
    end
catch
    if (isdeployed ||  log_to_file_flag)
        fprintf(file_id,'\n_error_: exception_thrown_function_ lprintf_log\n');
        fprintf(file_id,'\nERROR: Exception thrown while trying to print to log file %s\n',file_name_log);
        format_string
        varargin
    else
        fprintf('\n_error_: exception_thrown_function_ lprintf_log\n');
        fprintf('\nERROR: Exception thrown while trying to print to cmd window \n');
        format_string
        varargin
    end
end %fn lprintf_log
