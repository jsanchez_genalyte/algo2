function [ toe_exper_row ] = toe_create_row()
%TOE_CREATE_ROW creates a structure with blanks values for a new row of a toe: table of experiments.
% Output is the new empty row.
% Assumption: there are 10 columns on the toe. 
% Sample call:  [ toe_exper_row ] = toe_create_row()
% see:  toe_create, ui_do_fill_toe_expe_set  sa_ui_update_toe.m  sa_ui_update_tos

 % toe_col_struct = handles.toe_col_struct; ui_table_of_fits ui_table_exp
 
% Initial State values _______________________________________________________
 ndxdb = '';
 exper = '';
 instr = '';
 carri = '';
 spotl = '';
 kitlo = '';
 proto = '';
 dates = '';
 state = '';
 futur = '';

 field0  = 'ndxdb';     value0  = { ndxdb } ;
 field1  = 'exper';     value1  = { exper } ;
 field2  = 'instr';     value2  = { instr } ;
 field3  = 'carri';     value3  = { carri } ; 
 field4  = 'spotl';     value4  = { spotl } ;
 field5  = 'kitlo';     value5  = { kitlo } ;
 field6  = 'proto';     value6  = { proto } ;
 field7  = 'dates';     value7  = { dates } ;
 field8  = 'state';     value8  = { state } ;
 field9  = 'futur';     value9  = { futur } ;
 toe_exper_row = struct( field0  ,value0                                ...
     ,field1  ,value1 ,field2 ,value2 ,field3 ,value3 ,field4 ,value4   ...                
     ,field5 ,value5 ,field6 ,value6 ,field7 ,value7 ,field8 ,value8    ...              
     ,field9 ,value9 );
%  ,field10,value10,field11,value11,field12,value12                   ...
%      ,field13,value13,field14,value14,field15,value15,field16,value16,field17,value17   ...
%      ,field18,value18,field19,value19,field20,value20,field21,value21,field22,value22   ...
% 	 ,field23,value23,field24,value24,field25,value25,field26,value26,field27,value27,field28,value28);
end % fn table_of_fits_create_row

