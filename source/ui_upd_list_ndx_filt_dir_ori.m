function [ change_flag,list_ndx_filt_dir ] = ui_upd_list_ndx_filt_dir_ori( ui_state_struct,popupmenu_sel_index )  % ui_state_struct,db_metad_set_struct   )
%ui_upd_list_ndx_filt_dir updates the ori dir indices
%   Handles the 'All' cases. Future: handle the custom or search cases.   % ORI: version: niu. ~. 
%   Currently handling single selection: future: multiple selections using custom or multi checkmarks on pullups if possible from matlab GUI

% For the new current analyte update the rest of the popup  menu with the corresponding quantities: ie probe x/y
% first check that the selected analyte is different than the current one. If it is the same: get out of here (nothing needs to be done)

% convert popupmenu_sel_index from being an ndx of the current list into an ndx of the orig list:
[ cur_ndx_ori ] = ui_cvt_popmenu_to_ori_cur_ndx(ui_state_struct, popupmenu_sel_index );

if (popupmenu_sel_index  == ui_state_struct.cur_ndx) % cur_ndx refers to the current list.
    %  USER SELECTED same ANALYTE.
    change_flag = false;
    list_ndx_filt_dir = ui_state_struct.list_ndx_filt_dir;   % jas_test_ {1}
else
    % USER SELECTED A NEW ANALYTE.
    change_flag = true;
    probe_len_ori = size(ui_state_struct.list_str_cel_ori,1);
    % check for special case: All
    if (strcmpi(ui_state_struct.list_str_cel{popupmenu_sel_index} ,'All'))
        % END USER SELECTED ALL: reset ndx to all:
        list_ndx_filt_dir        = ones(probe_len_ori,1);
        % CHECK if len matches len_ori: if it is the case: reset custom. Otherwhise custom is not part of the pary: nothing to reset.
        % jas_tbd:
        list_ndx_filt_dir(end)   = 0;  % all means no custom.
    else
        % INDIVIDUAL ANALYTE
        list_ndx_filt_dir = zeros(probe_len_ori,1);             % disable the rest
        list_ndx_filt_dir(cur_ndx_ori) = 1;                     % turn on the selected in ori coordinates!.
    end
    % DONE UPDATING THE DIRECT FILTER.
    if isrow(list_ndx_filt_dir)
        list_ndx_filt_dir = list_ndx_filt_dir';
    end
end % fn ui_upd_list_ndx_filt_dir
% leftovers: 
% niu [ list_ndx_filt_dir_ori, popupmenu_probe_sel_index_ori ] = ui_upd_state_after_popupmenu_probe(ui_probe_state_struct,popupmenu_probe_sel_index); % ui_state_struct,popupmenu_probe_sel_index);
% compare what has changed on the direct filter and update every other analyte accordingly:

% need to convert the current:  cur_ndx and list_ndx_filt_dir    into the global:   cur_ndx and list_ndx_filt_dir

