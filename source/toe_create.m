function   [toe_cell, toe_col_struct] = toe_create(num_rows)
%TOE_CREATE creates an empty toe with as many rows as requested.
%   Input:  number of rows
% Sample call:  [toe,toe_row_struct] = toe_create(1)
% Initialization of tos cell array with the requested number of rows. 
% Returns the empty shell to be filled int: cell num_rows x 18  (assumption: toe figure has 18 cols

% see: pipeline: Sensogram_Analysis_App_10 -->  toe_create -->  tos_create_row          creates an empty tos.
% see: pipeline: Sensogram_Analysis_App_10 --> ui_do_fill_tos_sens_set tos_create_row   fills in tos with whole data set
% DEFINE ORDER OF COLUMNS FOR toe ______________________________________
 toe_col_struct.ndxdb   =  1  ; %
 toe_col_struct.exper   =  2  ; %
 toe_col_struct.instr	=  3  ; % device id
 toe_col_struct.carri  	=  4  ; % run date as a string.
 toe_col_struct.spotl  	=  5  ; % spot lot number
 toe_col_struct.kitlo   =  6  ; % kit  lot number
 toe_col_struct.proto  	=  7  ; % protocol
 toe_col_struct.dates   =  8  ; % run date. 
 toe_col_struct.state   =  9  ; % state id: completed etc.
 toe_col_struct.futur   =  10 ; % future_jas_tbd
 toe_col_struct.col_cnt =  10 ; % count of cols for TOE. cols:4-17 come from the smd cell matrix (all strings)

% Blank values _______________________________________________________

toe_cell = cell(num_rows, toe_col_struct.col_cnt);  % initial empty table is num_rows x 21

% jas_test: maybe I can get away just returning an empyt cell   !!!!! 
for row_cur=1:1:num_rows
    % Create a row:
     [ toe_exper_row ] = toe_create_row();   
    % insert the row:
    toe_cell(row_cur,:) =  struct2cell(toe_exper_row);    
end % for each row on the table

% Done with creation of blank table of sensors: tos
end % fn toe_create

% leftovers:
% obj_info(toe);
% display (toe);
% for row_cur=1:1:num_rows
%     toe{row_cur,1}= '';% fit_name 1
%     toe{row_cur,1}= 0 ;% fit_data 2
%     toe{row_cur,1}= 0 ;% fit_type 3
%     toe{row_cur,1}= 0 ;% sse      4
%     toe{row_cur,1}= 0 ;% r_sqr    5
%     toe{row_cur,1}= 0 ;% dfe      6
%     toe{row_cur,1}= 0 ;% adj_dfe  7
%     toe{row_cur,1}= 0 ;% rmse     8
%     toe{row_cur,1}= 0 ;% num_coef 9
%     toe{row_cur,1}= 0 ;% model_eq 10



