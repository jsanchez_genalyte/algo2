function [ error_message,exp_sensor_struct ] = sa_rest_exp_get_sensors( a_exp_id, rest_token ) % JOHN: Seems to work fine.  API_8: showTestSensorMetadata: 136 sensors
%SA_REST_EXP_GET_SENSORS retrieves the details: raw and normalized data  of a given a_exp_id  ??? for the  136 sensors
%   assumption: None. If no experiment exist with this id: exp_sensor_struct will be returned nan.
%   if it is  valid: the error_msg wil be empty
% sample call:  [ error_message,exp_sensor_struct ] = sa_rest_exp_get_sensors('b5d306cb-70a4-462a-a651-d3878bddadc4', handles.rest_token );
% returns   exp_sensor_struct = 136�1 struct array with fields: to access it: exp_sensor_struct(10).results
%               id
%               number
%               channel
%               is_valid
%               sensor_errors
%               results:   struct with fields:
%                         value: 331.5868
%                         normalized_value: 312.7029
%                         is_outlier: 0
%                         sensor_errors: []
% to reconnect: [ error_message,handles ] = sa_rest_do_connect( handles,true );
exp_sensor_struct  = struct;
error_message       = '';
% ASSUMED OK RESPONSE FROM THE WEB SERVICE
options                 = weboptions('HeaderFields',{'ContentType',  'text'             ;'Authorization',sprintf(' Bearer %s', rest_token )}); % jas_debug_timeout
options.Timeout         = 25; % jas_debug_timeout    
[ url_struct ]          = sa_set_production_vs_stagging_url_flag(); % prod_stag_flag );
url_sensor_str          = url_struct.url_sensor_str;

url_sensor_str_full     = strcat(url_sensor_str,sprintf('%s/sensors',a_exp_id));

try
    error_message       = '';
    exp_sensor_struct   = nan;
    exp_sensor_struct 	= webread(url_sensor_str_full,options);
catch
    % COULD NOT READ SENSORS FOR THIS EXPERIMENT
    error_message       = sprintf('\nERROR_ WHILE READING experiment sensors from API: \nWEBREAD error. Function: sa_rest_exp_get_sensors for Experiment: %s NOT FOUND',a_exp_id);
    display(error_message);
    return;
end
if ( (isfield(exp_sensor_struct,'id')) && (isfield(exp_sensor_struct,'number')) && (isfield(exp_sensor_struct,'channel')) && (isfield(exp_sensor_struct,'results')) )
    % OK RESPONSE FROM THE WEB SERVICE: API sensors
else
    error_message = sprintf(...
        '\nERROR_ RETRIEVING THE EXPERIMENT DETAILS FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.');
end
end % fn sa_rest_exp_get_sensors



