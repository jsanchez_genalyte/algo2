%%                            QC REPORT: CONTROL CHARS FOR EACH ANALYTE - SPOTTING LOT COMBINATION
%%  RACK TO RACK DETAIL comparison is performed for each lot and for analyte and each lot.
%  Green    scatter dots:  sensor 1
%  Blue     scatter dots:  sensor 2
%  Red      scatter dots:  sensor 3
%  Orange   scatter dots:  sensor 4
%  Square   Odd  channel
%  Triangle Even channel
%  Source: Assay Dev Detail Report (From Jervis API)

%  To run: sa_do_plot_ana_control_charts_det_url

% color_sensor:     1             2           3          4
color_code = { rgb('Green')  rgb('Blue') rgb('Purple') rgb('Orange') };
shape_code = {     's'           's'          's'          's' };


%function [ qc_lot_2_lot_det_struct,qc_rack_2_rack_det_struct,error_message ] = sa_do_plot_ana_ctl_charts( qc_lot_2_lot_det_struct,qc_rack_2_rack_det_struct,assay_ana_header )
%SA_DO_PLOT_ANA_CTL_CHARTS  plots the control chart for each combo: analyte/spot.  for the auto qc report.


fig_btm             = 50;
fig_left            = 50;
fig_width           = 1600;
fig_height          = 900;
height_delta        = 0.15; % .2 units will be taller the box than the tex
load('qc_rack_2_rack_det_struct.mat','qc_lot_2_lot_det_struct','qc_rack_2_rack_det_struct')
ana_cnt = size(qc_lot_2_lot_det_struct,2)-1;
plot_lots_per_win    = 2;
for cur_ana_ndx=1:1:ana_cnt % jas_temp_debug:  ana_cnt % jas_temp_debug_ ana_cnt
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both
    spotl_names_list    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    name_ana            = qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana ;
    col_values_cat_u    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    spotl_cnt           = length(col_values_cat_u); %
    sub_plot_hor_cnt    = 1;	        % left: spotl1 right: spotl2 left. Change it from  2 to 3 or 4 to see more subplots horizontally: jas_hard_coded
    sub_plot_ver_cnt    = 3;            % top: plot btm: text for summary stats table
    num_sub_plots       = sub_plot_hor_cnt*sub_plot_ver_cnt;
    
    hFig                = figure('Name',sprintf('  RACK TO RACK DETAIL variability. Analyte: %-s ',name_ana));
    set(hFig, 'Position', [fig_left fig_btm  fig_width fig_height]);
    fig_left            = fig_left+20;
    cur_sub_plot_num    = 0;
    col_chann_both = size(qc_rack_2_rack_det_struct,3);
    chann_cnt      = col_chann_both -1;
    for cur_spotl_ndx=1:1:spotl_cnt
        % PRODUCE BOX PLOTS FOR CURRENT ANALYTE AND CURRENT SPOTL:  by qc_rack_number
        cur_sub_plot_num= cur_sub_plot_num +1;
        if (cur_sub_plot_num > sub_plot_ver_cnt)
            % DONE WITH THIS WINDOW: (ALL SUBPLOTS DONE) need to create a new window and reset cur_sub_plot_num
            cur_sub_plot_num = 1;
            hFig    = figure('Name',sprintf('  RACK TO RACK variability. Analyte: %-s ',name_ana));
            set(hFig, 'Position', [fig_left fig_btm fig_width fig_height]);
            fig_left = fig_left+0;
        end
        
        subplot(sub_plot_ver_cnt,sub_plot_hor_cnt,cur_sub_plot_num); % top part: plot. Btm part: table.
        hold all;
        ana_lot_rack_struct     = qc_rack_2_rack_det_struct(cur_ana_ndx,cur_spotl_ndx,col_chann_both).ana_lot_rack_struct;
        data_aggr_mat           = ana_lot_rack_struct.data_aggr_mat;
        name_cur_spotl          = ana_lot_rack_struct.name_ana;
        col_values_cat_u        = ana_lot_rack_struct.col_values_cat_u;
        rack_cnt                = length(col_values_cat_u);
        
        clear data_aggr_single_mat;
        for cur_chan = 1:1:col_chann_both
            data_aggr_single_mat(cur_chan)    = qc_rack_2_rack_det_struct(cur_ana_ndx,cur_spotl_ndx,cur_chan).ana_lot_rack_struct;
        end
        
        % Produce Box Plots for all available lots for each analyte
        % display(name_cur_spotl) position: [left bottom width height]
        ax_box                  = gca;
        set(ax_box,'Units','normalized')
        ax_box_position_ori     = ax_box.Position;
        ax_box_position_new     = ax_box_position_ori;
        ax_box_position_new(2)  = ax_box_position_ori(2) - height_delta; % lower the btm of the box
        ax_box_position_new(4)  = ax_box_position_ori(4) + height_delta; % make taller      the box
        ax_box.Position         = ax_box_position_new;
        
        boxplot(ax_box,data_aggr_mat,'notch','on',...
            'labels',{ char(col_values_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))
        set(ax_box,'FontSize',9);
        str_title = strcat(strrep(name_cur_spotl,'_','-'),'.  Positive Cluster Mean  .','  ',qc_lot_2_lot_det_struct(cur_ana_ndx).name_ana);
        title(ax_box,strrep(str_title,'.',' '),'FontSize',11);
        ylabel(ax_box, strcat(strrep(name_cur_spotl,'_','-'),' - GRU') );
        
        % draw the individual dots: oddd and even
        % Do scatter plots to plot individual dots ___________________________________________
        x_axis_qc_plot  = get(ax_box,'XAxis');
        x_limits_qc_plot= x_axis_qc_plot.Limits;
        x_min           = x_limits_qc_plot(1);
        x_max           = x_limits_qc_plot(2);
        x_range         = x_max - x_min;
        x_inc           = x_range / ((2*rack_cnt));
        cur_start_x     = x_min + (x_inc/2);
        
        clear data_single_plot_x;
        for  cur_chan = 1:1:col_chann_both
            temp = zeros(size((data_aggr_single_mat(cur_chan).data_aggr_mat),1),rack_cnt);
            data_single_plot_x{cur_chan} = temp;
        end
        
        cur_start_x = cur_start_x - (x_inc/3.5);
        for cur_rack_ndx = 1:1:rack_cnt
            for cur_chan = 1:1:chann_cnt
                cur_x_plot = data_single_plot_x{cur_chan};
                cur_x_plot(:,cur_rack_ndx) = cur_start_x;
                temp_mat = data_aggr_single_mat(cur_chan).data_aggr_mat;
                scatter(ax_box,cur_x_plot(:,cur_rack_ndx),temp_mat(:,cur_rack_ndx)...
                    ,'MarkerFaceColor',color_code{cur_chan},'LineWidth',0.2,'Marker'...
                    ,shape_code{cur_chan},'SizeData',9); % ,'MarkerFaceColor',dark_green_color);
                cur_start_x                 = cur_start_x  + (x_inc/(2.75*2));
                if (cur_chan == 2)
                    % CRANK IT UP
                    cur_start_x                 = cur_start_x  + (x_inc/(0.49*2));
                end
            end
            cur_start_x = cur_start_x + (x_inc/3.9); % % move to the next boxplot
        end
        set(ax_box,'XGrid','on');
        set(ax_box,'YGrid','off');
        grid on;
        hold off;
    end % for each spotting lot
end % for each analyte: : plotting each on it's own window.
%end % fn sa_do_plot_ana_ctrl_charts_det_url

