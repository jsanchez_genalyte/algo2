function   [tos_cell, tos_col_struct] = tos_create(num_rows)
%TOS_CREATE creates an empty tos with as many rows as requested.
%   Input:  number of ros
% Sample call:  [[tos,tos_row_struct] = tos_create(1)
% Initialization of tos cell array with the requested number of rows. 
% Returns the empty shell to be filled int: cell num_rows x 18  (assumption: tos figure has 18 cols

% see: pipeline: Sensogram_Analysis_App_10 -->  tos_create -->  tos_create_row          creates an empty tos.
% see: pipeline: Sensogram_Analysis_App_10 --> ui_do_fill_tos_sens_set tos_create_row   fills in tos with whole data set
% DEFINE ORDER OF COLUMNS FOR tos ______________________________________
 tos_col_struct.ndxdb    =  1   ; %
 tos_col_struct.clasi    =  2  ; % xxxx__first_of_smd __xxxx	classification flags (all of them as a single string: G,A,.....
 tos_col_struct.probe	=  3  ; %                           jas_tbd_add_guide 1*************   
 tos_col_struct.dates  	=  4  ; % run date as a string.
 tos_col_struct.proto  	=  5  ; % protocol
 tos_col_struct.sampl  	=  6  ; % sample id
 tos_col_struct.chipl    =  7  ; % chip lot
 tos_col_struct.exper   	=  8  ; % experiment id
 tos_col_struct.instr   	=  9  ; % instrument id
 tos_col_struct.carri    =  10 ; % carrier id
 tos_col_struct.chann    =  11 ; % channel number 1 or 2
 tos_col_struct.clust    =  12 ; % cluster number 1 to 4 
 tos_col_struct.datat    =  13 ; % data type: raw/norm_m or norm_j
 tos_col_struct.valfg    =  14 ; % valid flag                          jas_tbd_add_guide  2************* valid
 tos_col_struct.outfg    =  15 ; % outier flag                          jas_tbd_add_guide  3************* oulier
 tos_col_struct.error    =  16 ; % xxxx__last_of_smd __xxxx  jas_tbd_add_guide  4************* error  
 tos_col_struct.bligc    =  17 ; % base line gru
 tos_col_struct.ampgc    =  18 ; % amplification gru
 tos_col_struct.shifc    =  19 ; % gru shift
 tos_col_struct.anfg1    =  20 ; % Analysis Flag 1
 tos_col_struct.anfg2    =  21 ; % Analysis Flag 2
 tos_col_struct.anfg3    =  22 ; % Analysis Flag 3 
 tos_col_struct.anfg4    =  23 ; %  tos_col_struct.error_string        =  17 ; %
 tos_col_struct.anfg5    =  24 ;
 tos_col_struct.anfg6    =  25 ;
 tos_col_struct.anfg7    =  26 ;
 tos_col_struct.anfg8    =  27 ;
 tos_col_struct.anfg9    =  28 ;
 tos_col_struct.anfg10   =  29 ;

 tos_col_struct.col_cnt  =  29 ; % count of cols for TOS. cols:4-17 come from the smd cell matrix (all strings)

% niu  tos_col_struct.sensor_probe_number =  3  ; % gui: cluster 
% niu  tos_col_struct.sensor_number       =  2  ; % gui: number

% Blank values _______________________________________________________

tos_cell = cell(num_rows, tos_col_struct.col_cnt);  % initial empty table is num_rows x 21

% jas_test: maybe I can get away just returning an empyt cell   !!!!! 
for row_cur=1:1:num_rows
    % Create a row:
     [ tos_senso_row ] = tos_create_row();   
    % insert the row:
    tos_cell(row_cur,:) =  struct2cell(tos_senso_row);    
end % for each row on the table

% Done with creation of blank table of sensors: tos
end % fn tos_create

% leftovers:
% obj_info(tos);
% display (tos);
% for row_cur=1:1:num_rows
%     tos{row_cur,1}= '';% fit_name 1
%     tos{row_cur,1}= 0 ;% fit_data 2
%     tos{row_cur,1}= 0 ;% fit_type 3
%     tos{row_cur,1}= 0 ;% sse      4
%     tos{row_cur,1}= 0 ;% r_sqr    5
%     tos{row_cur,1}= 0 ;% dfe      6
%     tos{row_cur,1}= 0 ;% adj_dfe  7
%     tos{row_cur,1}= 0 ;% rmse     8
%     tos{row_cur,1}= 0 ;% num_coef 9
%     tos{row_cur,1}= 0 ;% model_eq 10
%     tos{row_cur,1}= 0 ;% coef_1   11
%     tos{row_cur,1}= 0 ;% coef_2   12
%     tos{row_cur,1}= 0 ;% coef_3   13
%     tos{row_cur,1}= 0 ;% coef_4   14
%     tos{row_cur,1}= 0 ;% coef_5   15
%     tos{row_cur,1}= 0 ;% coef_6   16



