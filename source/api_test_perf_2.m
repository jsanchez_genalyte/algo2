

p = gcp();
% To request multiple evaluations, use a loop.
for idx = 1:10
  f(idx) = parfeval(p,@magic,1,idx); % Square size determined by idx
end
% Collect the results as they become available.
magicResults = cell(1,10);
for idx = 1:10
  % fetchNext blocks until next results are available.
  [completedIdx,value] = fetchNext(f);
  magicResults{completedIdx} = value;
  fprintf('Got result with index: %d.\n', completedIdx);
end


sensor_cnt_max= 20;
idx =0;
p = gcp();
for sensor_cnt = 1:1:sensor_cnt_max    
    tic        % start timing
    % To request multiple evaluations, use a loop.
    for idx_loc  = 1:sensor_cnt
        idx = idx+1;
        url_str = sprintf('http://jervis.genalyte.com/api/sensors/%-3d',exp_sensor_struct(cur_sensor_ndx).id);
        f(idx) = parfeval(p,@webread,1,url_str,options); % magic,1,idx); % Square size determined by idx
        %f(idx) = temp;
    end
    msg = toc; % end timing
    %     %fprintf('%% Time required to retrieve scan data for the %4d sensors per experiment = %5.2f secs\n',sensor_cnt,double(msg));
    fprintf('%4d , %5.2f ;\n',sensor_cnt,double(msg));
end
% Collect the results as they become available.

sensor_cnt_max= 20;
idx =0;
for sensor_cnt = 1:1:sensor_cnt_max    
    tic
    exp_scan_struct = cell(1,sensor_cnt);
    for idx_loc = 1:sensor_cnt_max
        idx = idx+1;
        % fetchNext blocks until next results are available.
        [completedIdx,value] = fetchNext(f);
        exp_scan_struct{completedIdx} = value;
        %fprintf('Got result with index: %d.\n', completedIdx);
    end
     msg = toc; % end timing
%     %fprintf('%% Time required to retrieve scan data for the %4d sensors per experiment = %5.2f secs\n',sensor_cnt,double(msg));
     fprintf('%4d , %5.2f ;\n',sensor_cnt,double(msg));
end

% Time required to retrieve scan data for the 136   sensors per experiment =  47.2 secs
% Time required to retrieve scan data for the 136   sensors per experiment =  46.7 secs
% Time required to retrieve scan data for the 136x2 sensors per experiment = 124.0 secs   3xtime for 2x sensors!!!
% Time required to retrieve scan data for the 136x2 sensors per experiment = 410.8 secs  10xtime for 2x sensors!!!
% Time required to retrieve scan data for the 136x2 sensors per experiment =  93.6 secs   2xtime for 2x sensors!!!
% Time required to retrieve scan data for the 136x2 sensors per experiment =  93.8 secs   2xtime for 2x sensors!!!

% Time required to retrieve scan data for the 136x3 sensors per experiment = 139.3 secs   3xtime for 3x sensors!!!
% Time required to retrieve scan data for the 136x3 sensors per experiment = 138.5 secs   3xtime for 3x sensors!!!
% Time required to retrieve scan data for the   24 sensors per experiment =   8.2 secs

% 
%    1 ,  0.03 ;
%    2 ,  0.04 ;
%    3 ,  0.03 ;
%    4 ,  2.70 ;
%    5 ,  2.71 ;
%    6 ,  2.06 ;
%    7 ,  2.74 ;
%    8 ,  2.83 ;
%    9 ,  3.39 ;
%   10 ,  3.43 ;
%   11 ,  4.10 ;
%   12 ,  4.10 ;
%   13 ,  4.76 ;
%   14 ,  4.83 ;
%   15 ,  5.39 ;
%   16 ,  5.47 ;
%   17 ,  6.13 ;
%   18 ,  6.47 ;
%   19 ,  6.83 ;
%   20 ,  7.02 ;
%   21 ,  7.45 ;
%   22 ,  7.49 ;
%   23 ,  8.14 ;
%   24 ,  8.16 ;
%   25 ,  8.80 ;
%   26 ,  8.83 ;
%   27 ,  9.51 ;
%   28 ,  9.49 ;
%   29 , 10.23 ;
%   30 , 10.40 ;
%   31 , 10.74 ;
%   32 , 10.98 ;
%   33 , 11.48 ;
%   34 , 11.80 ;
%   35 , 12.25 ;
%   36 , 12.27 ;
%   37 , 12.85 ;
%   38 , 13.01 ;
%   39 , 13.56 ;
%   40 , 13.83 ;
%   41 , 14.24 ;
%   42 , 14.35 ;
%   43 , 15.02 ;
%   44 , 15.02 ;
%   45 , 15.55 ;
%   46 , 15.64 ;
%   47 , 16.30 ;
%   48 , 16.27 ;
%   49 , 16.82 ;
%   50 , 17.29 ;
%   51 , 17.58 ;
%   52 , 17.71 ;
%   53 , 18.31 ;
%   54 , 19.08 ;
%   55 , 18.89 ;
%   56 , 19.77 ;
%   57 , 19.93 ;
%   58 , 19.89 ;
%   59 , 20.38 ;
%   60 , 20.55 ;
%   61 , 21.07 ;
%   62 , 21.70 ;
%   63 , 21.75 ;
%   64 , 21.72 ;
%   65 , 22.49 ;
%   66 , 22.52 ;
%   67 , 23.04 ;
%   68 , 23.15 ;
%   69 , 23.95 ;
%   70 , 23.99 ;
%   71 , 22.67 ;
%   72 , 22.47 ;
%   73 , 25.09 ;
%   74 , 25.17 ;
%   75 , 26.34 ;
%   76 , 26.49 ;
%   77 , 26.36 ;
%   78 , 26.55 ;
%   79 , 27.22 ;
%   80 , 27.34 ;
%   81 , 27.79 ;
%   82 , 27.91 ;
%   83 , 28.44 ;
%   84 , 28.83 ;
%   85 , 29.49 ;
%   86 , 29.37 ;
%   87 , 29.92 ;
%   88 , 29.80 ;
%   89 , 30.26 ;
%   90 , 30.51 ;

