function [ handles ] = do_select_all_rows_menu_item( handles )
%DO_SELECT_ALL_MENU_ITEM Allows for user to select all elements of the table_of_fits
%   Stores the row selected to it can be cut/copy/clear later on.

% jas_vac: code from std_curve to ve cloned:

% Retrieve the current table_of_fits from the ui:
tos                                                 = get(handles.ui_table_tos,'Data');
last_row                                            = size(tos,1);
handles.ui_state_struct.tos_cur_selection = [1 last_row];      % jas_tb initialized
% Once selected all: then turn on right click menu to: edit/copy/plot_all,
set(handles.ui_table_tos,                          'Selected','on');
set(handles.edit_copy_sel_menu_item,               'Visible','on','Enable','on');

% jas_tbd: Think if want to do thefollowing:
% set(handles.pushbutton_fit_genalyte,            'Visible','on','Enable','off');  % update_main_plot_button Will do first.
% set(handles.pushbutton_fit_reference,           'Visible','on','Enable','off');  % update_main_plot_button Will do first.
% set(handles.push_button_assignment,             'Visible','on','Enable','off');  % update_main_plot_button Will do first.
% set(handles.push_button_generate_standard_curve,'Visible','on','Enable','off');
end % fn do_select_all_rows_menu_item

