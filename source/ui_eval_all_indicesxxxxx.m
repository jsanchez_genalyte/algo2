function [ ui_state_struct ] = ui_eval_all_indicesxxxxx( db_cur_fit_net_ndx,clasi, ui_state_struct  )
%UI_EVAL_ALL_INDICES Evaluates current indices to produce an ui_cur_fit_net_ndx to be used to display the tos and/or plots
%   Uses the ui_request_flag to decide how to build the ui indices:
% input:
% 1_ db_cur_fit_net_ndx: index of whatever is selected (net of all filtering)
% 2_ clasi:              classification results: U==Unknown,G=good,A=Anomalous,etc.               % db_metad_set_struct.clasi
% 3 _ ui_state_struct:   struct with start/cnt for good and bad sensograms.
% 4_ ui_request_flag
%           'g_only' to produce indices of only good sensograms.
%           'a_only' to produce indices of only anom sensograms.
%           'g_n_a'  to produce indices of good and  anom sensograms.
%           'u_only' to produce indices of unknown sensograms: ie not_analyzed.
% output: ui_state_struct.ui_cur_fit_net_ndx ndx to be used to plot/and tos.  either logical of size 1536 or small with indices
% sample call:  [ ui_state_struct_temp ] = ui_eval_all_indices( ...
% handles.ui_state_struct.db_cur_fit_net_ndx, handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct ,'u_only'  )
ui_request_flag = ui_state_struct.ui_request_flag;
ui_cur_fit_net_ndx = [];
 
senso_start_ndx        	= ui_state_struct.senso_start_ndx;        	% this ndx is relative to whatever is currently selected
senso_curves_perp_cnt   = ui_state_struct.senso_curves_perp_cnt;  	%100; % sa_cfg_
senso_end_ndx          	= senso_start_ndx+ senso_curves_perp_cnt-1; % for g_only: this may increase if it does not find enough goods

if (  ( (strcmpi(ui_request_flag,'g_only')) || (strcmpi(ui_request_flag,'g_n_a')) ) && (senso_end_ndx >= senso_start_ndx) )
    % REQUESTED GOOD_ONLY or both  AND SENSO RANGE IS NOT EMPTY
    % determine ndx of the good ones:
    ui_cur_fit_net_ndx_good_all     = clasi(logical(db_cur_fit_net_ndx)) =='G';
    % from these see return as much as it can be retrieved:
    ui_cur_fit_net_ndx_good_all_temp= ui_cur_fit_net_ndx_good_all(senso_start_ndx:end);
    ui_cur_fit_net_ndx_good       	= find(ui_cur_fit_net_ndx_good_all_temp, senso_curves_perp_cnt, 'first'); % returns at most the first senso_curves_perp_cnt indices corresponding to the nonzero entries of X.
    ui_cur_fit_net_ndx_good       	= ui_cur_fit_net_ndx_good +  (senso_start_ndx-1); % shift all of them to reflect that we start at  senso_start_ndx
    %good_cnt_true                  = length(ui_cur_fit_net_ndx_good); % dbg_ this is the true amount of good found.
    senso_start_ndx_next_good      	= min((ui_cur_fit_net_ndx_good(end)+1),db_cur_fit_net_len);  % save for the next trip around.
	senso_start_ndx_next            = senso_start_ndx_next_good; 
end

if ( ((strcmpi(ui_request_flag,'a_only')) || (strcmpi(ui_request_flag,'g_n_a')) )  && (senso_end_ndx >= senso_start_ndx) )
    % REQUESTED ANOM_ONLY or both  AND SENSO RANGE IS NOT EMPTY
    % determine ndx of the anom ones:
    ui_cur_fit_net_ndx_anom_all     = clasi(logical(db_cur_fit_net_ndx)) =='A';
    % from these see return as much as it can be retrieved:
    ui_cur_fit_net_ndx_anom_all_temp= ui_cur_fit_net_ndx_anom_all(senso_start_ndx:end);
    ui_cur_fit_net_ndx_anom       	= find(ui_cur_fit_net_ndx_anom_all_temp, senso_curves_perp_cnt, 'first'); % returns at most the first senso_curves_perp_cnt indices corresponding to the nonzero entries of X.
    ui_cur_fit_net_ndx_anom       	= ui_cur_fit_net_ndx_anom +  (senso_start_ndx-1); % shift all of them to reflect that we start at  senso_start_ndx
    %anom_end_cnt_true              = length(ui_cur_fit_net_ndx_anom); % dbg_ this is the true amount of anom found.
    senso_start_ndx_next_anom    	= min((ui_cur_fit_net_ndx_anom(end)+1),db_cur_fit_net_len);  % save for the next trip around.
	senso_start_ndx_next            = senso_start_ndx_next_anom; 
end


if (  (strcmpi(ui_request_flag,'g_n_a')) && (senso_end_ndx >= senso_start_ndx) ) % and not combined
    % REQUESTED GOOD AND ANOM_   AND SENSO ANGE IS NOT EMPTY
    % Already have the determine ndx of the anom ones:    
    % grab: all the good and anom that have an ndx less than this:
    senso_end_ndx          	= senso_start_ndx+ senso_curves_perp_cnt-1;
    ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good(ui_cur_fit_net_ndx_good <= senso_end_ndx);
    ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_good(ui_cur_fit_net_ndx_anom <= senso_end_ndx);    
    senso_start_ndx_next    = senso_end_ndx+1;      
end

if (   (strcmpi(ui_request_flag,'u_only')) && (senso_end_ndx >= senso_start_ndx) )  % jas_tbd: temporal using good_start as good_unknown
    % REQUESTED UNKNOWN_ONLY or both  AND GOOD RANGE IS NOT EMPTY
    % determine ndx of the good ones:
    ui_cur_fit_net_ndx_good_all = find(clasi(db_cur_fit_net_ndx) =='U');     % 'U'
    % from these see return as much as it can be retrieved:
    good_end_ndx_true       = min(senso_end_ndx,length(ui_cur_fit_net_ndx_good_all));
    good_range              = senso_start_ndx:1:good_end_ndx_true;
    ui_cur_fit_net_ndx_unkn = ui_cur_fit_net_ndx_good_all(good_range);
end                                             
                                             

