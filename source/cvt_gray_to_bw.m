function [success_fg] = cvt_gray_to_bw( im_tiff_gray_file_path,im_tiff_bw_file_path,min_rect_side_px,min_contrast)
% CVT_GRAY_TO_BW Converts a TIFF gray scale file with sparse rectangles to Binary (Black/White)
% Cleans the image noise while preserving  contents: Rectangle(s) size
% INPUT ARGUMENTS
% 1 - im_tiff_gray_file_path: [ Required string ] Full path to input file: TIFF gray scale
% 2 - im_tiff_bw_file_path:   [ Required string ]Full path to input file: TIFF 
% 3 - min_rect_side_px:       { Optional positive integer ] Minimum rectangle side (default 10 px)
% 4 - min_contrast:           [ Optional positive integer ] Minimum contrast (default 50 px)
% OUTPUT ARGUMENTS:  
% 1 success_fg                Boolean: 1 == success. 
%                                      0 == Errors on arguments or
%                                           Errors during file open/write.
% OUTPUT FILE:               TIFF Binary with noise removed.


% Parse arguments _________________________________________________

err_msg                   ='';  % No error msg to begin with.
success_fg                = 1;  % Assume things go fine... until a problem is found.
default_rect_min_side_px = 10;  % if no minimum restangle side is specified, 10 is used.
default_min_contrast      = 50; % if no minimum contrast is specified,   50 is used.
th_level                  = double(1.0);  % Threshold value make image binary.
if nargin == 0
    % Missing input and output file
    % Use Default values f
    err_msg=strcat(err_msg,sprintf('\n ERROR: Unspecified Input and output TIFF file paths '));
    success_fg = 0;    
end;

if nargin == 1
    % First ARGUMENT SPECIFIED: Input file name
    % 
    if (~ischar(im_tiff_gray_file_path))
        err_msg=strcat(err_msg,sprintf(...
        '\n ERROR: Missing output TIFF file path. Only input file path specified %s:' ...
        ,im_tiff_gray_file_path));
        success_fg  = 0;
    end
end

if (nargin == 2  && success_fg)
    % ARGUMENTS SPECIFIED: input and output files
    % Verify areguments are of the valid type    
    % Use Default values for min rectangle side in pixels   
    if (~ischar(im_tiff_bw_file_path)  )
        err_msg=strcat(err_msg,'\n ERROR: Invalid Input TIFF file path: , It must be a string.');
        success_fg  = 0;        
    end          
    if (~ischar(im_tiff_bw_file_path)  )
        err_msg=strcat(err_msg,'\n ERROR: Invalid Output TIFF file path: , It must be a string.');
        success_fg  = 0;        
    end           
    min_rect_side_px = default_rect_min_side_px;   
end

if (nargin == 3 && success_fg)
    % ARGUMENTS SPECIFIED: input and output files and min rect side.
    % Verify areguments are of the valid type
    if (~ischar(im_tiff_gray_file_path)  )
        err_msg=strcat(err_msg,'\n ERROR: Invalid Input TIFF file path: , It must be a string.');
        success_fg  = 0;        
    end          
    if (~ischar(im_tiff_bw_file_path)  )
        err_msg=strcat(err_msg,'\n ERROR: Invalid Output TIFF file path: , It must be a string.');
        success_fg  = 0;        
    end    
    if (~ isnumeric(min_rect_side_px)  )
        min_rect_side_px = default_rect_min_side_px;
        err_msg= strcat(err_msg,sprintf(... 
        '\n WARNING: Minimum rectangle side must be a positive integer. Using default value= %6d '...
        ,default_rect_min_side_px));
    end
end

if (nargin == 4 && success_fg)
    % ARGUMENTS SPECIFIED: input,output files, min rect side and min contrast
    % Verify areguments are of the valid type
    if (~ischar(im_tiff_gray_file_path)  )
        err_msg=strcat(err_msg, ...
        '\n ERROR: Invalid Input TIFF file path: , It must be a string.');
        success_fg  = 0;        
    end          
    if (~ischar(im_tiff_bw_file_path)  )
        err_msg=strcat(err_msg, ...
        '\n ERROR: Invalid Output TIFF file path: , It must be a string.');
        success_fg  = 0;        
    end    
    if (~ isnumeric(min_rect_side_px)  )
        min_rect_side_px = default_rect_min_side_px;
        err_msg= strcat(err_msg,sprintf( ...
        '\n WARNING: Minimum rect side must be a positive int. Using default value= %6d ' ...
        ,default_rect_min_side_px));
    end

    if (~ isnumeric(min_contrast)  )
        min_contrast = default_min_contrast;
        err_msg= strcat(err_msg,sprintf(...
        '\n WARNING: Minimum contrast must be a positive int. Using default value= %6d ' ...
        ,default_min_contrast));
    end    
end

if ~strcmp(err_msg,'');
    % Dislay Error/Warning Message
    disp(err_msg)
end

% DONE VERIFING ARGUMENTS _________________________________________________

fprintf('\n Processing file: %s ...\n',im_tiff_gray_file_path)  

% MAIN CONVERT TO BINARY script.

if success_fg
    
    % ARGUMENTS ARE VALID: Proceed. _______________________________________
    
    % Open Input TIFF file and retrieve file details.

    im_tiff_gray   = imread(im_tiff_gray_file_path);      
    h_im_tiff_gray = imshow(im_tiff_gray_file_path);
    imgmodel       = imagemodel(h_im_tiff_gray);

    height        = getImageHeight     (imgmodel);  % Return number of rows
    width         = getImageWidth      (imgmodel);  % Return number of cols
    
    % Open Output TIFF file and set tags same as the input file. __________
    
    % Find First row with a rect.__________________________________________
    % Result: Estimated rect illumination level for this row.
    
    im_width_min_rect =  width- min_rect_side_px;
    row_first_rect = 0; % zero means no row has been found (row with a rect)
    bkgr_illum     = 0; % zero means no value has been set yet
    rect_illum     = 0; % zero means no value has been set yet
    for row = 1:height
        lineBlkSumInten = zeros(1,min_rect_side_px);
        for col=1:im_width_min_rect
            lineBlkSumInten(1,col) = ... 
                      sum(im_tiff_gray(row,(col:(col+min_rect_side_px))));
        end
        % Determine darkest and lightest block
        illum_min = min(lineBlkSumInten);
        illum_max = max(lineBlkSumInten);
        
        if (abs(illum_min-illum_max) > (min_contrast * min_rect_side_px))
            %  FOUND A LARGE CONTRAST: This row MUST have a rect.
            %  STORE Found levels for this row as representative of the 
            %  whole image: BIG BIG Assumption !!!
            %  Save the row number  for the bacrground and the rect            
            rect_illum     = illum_min;
            row_first_rect = row;
            
            % save column number where the rect was found 
            % ( For debugging purposes Only !)            
            col_first_rect = find(lineBlkSumInten(1,:)==rect_illum,1,'first');
            
            % Refinement: 
            % Use the 10x10 block to determine the avg. rect illumination.
            % Rather than just the avg. of the top row of of the rect!            
            % Conservative:use only a 8x8 in case there is noise on the
            % edge of the rectangle.
            rect_illum = double(mean(mean(im_tiff_gray( ...
                 (row_first_rect+1:(row_first_rect+min_rect_side_px-2)) ...
                ,(col_first_rect+1:(col_first_rect+min_rect_side_px-2))))));
            bkgr_illum = illum_max / min_rect_side_px;
            
            % DO NOT NEED TO KEEP PROCESSING MORE ROWS: 
            % Found a row with a rectangle !
            break;
        end
    end
    
    % Check to see what you found:    
    if ((bkgr_illum == 0) && (rect_illum == 0))
        % WARNING: The whole image got processed and no row with ... 
        % high contrast was found.
        % Give message, but this not necessarily an error!
        err_msg=strcat(err_msg,sprintf( ...
        '\n Contrast on the image is less than :%d, No rect(s) found...'...
        ,min_contrast));
    end
    
    fprintf('\n Found first rect on row: %d \n',row_first_rect)         
    
    % Set Illumination threshold to be used to convert image to BINARY. ___
    
    % Set the threshold just above the rect gray level: 
    % the goal is to Get rid of most of the background. 
    % without eliminating any of the rects
    % TEMPORAL: 
    % I am hardcoding a 3% factor above the rect level found.
    % It would be better to make this factor an algorithm paramteter.
    
    th_factor = 1.03; % NOTE: better 1.03  worst 1.2 
    
    th_level = th_factor * (1.00 *rect_illum) / 255.0 ;    
    
    if th_level > 1
        % THRESHOLD TOO HIGH: Make sure it causes no exceptions.
        th_level = 0.99;
    end    
    
    % Convert image to BINARY _____________________________________________
    
    % Convert image to binary image, based on estimated threshold level 
    % Complement image to meet requirements: Squares are ones.
    
    im_tiff_bw_temp =  im2bw( im_tiff_gray, th_level);   
    im_tiff_bw =  imcomplement(im_tiff_bw_temp);
    % create fake noise
    im_tiff_bw(50,66) = 1; % white pixel just abve rect.
    
    %imtool_handle_bw = 
    imtool(im_tiff_bw); % displays the noisy binary image
    %close(imtool_handle_bw);     
    
    % Clean up Image noise: Use Dilate and Erosion. _______________________
    
    % removes from a binary image all connected components (objects) 
    % that have fewer than P 100 pixels,
    % bwareaopen preserves rect size because rect area min is 100 pixels

    % Do an open followed by a close:  cross
    
    conn     = ones(3);   %   cross
    conn(1,1)= 0;
    conn(1,3)= 0;
    conn(3,1)= 0;    
    conn(3,3)= 0;      
    min_area = min_rect_side_px  * min_rect_side_px;
    im_tiff_bw_clean_1 = bwareaopen(im_tiff_bw, min_area, conn);
    imtool_handle_bw_clean_1 = imtool(im_tiff_bw_clean_1);    
    close(imtool_handle_bw_clean_1)    
    
    % Clean Noise adjacent to  the squares: On top and below
    % that have fewer than 8 pixels (rects can be min 10 pixels!)     
    conn     = zeros(3);  %  horizontal line.        
    conn(2,1)= 1;   
    conn(2,2)= 1;
    conn(2,3)= 1;        
    im_tiff_bw_clean_2 = bwareaopen(im_tiff_bw_clean_1,(min_rect_side_px-2), conn);
    imtool_handle_bw_clean_2 = imtool(im_tiff_bw_clean_2);  
    close(imtool_handle_bw_clean_2)
  
    % Clean Noise adjacent to the squares: On the left and on the right
    % that have fewer than 8 pixels (rects can be min 10 pixels!)  
    conn     = zeros(3); %  vertical line.
    conn(1,2)= 1;
    conn(2,2)= 1;
    conn(3,2)= 1;    
    im_tiff_bw_clean_3 = bwareaopen(im_tiff_bw_clean_2, (min_rect_side_px-2), conn);    
    %imtool_handle_bw_clean_3 = 
    imtool(im_tiff_bw_clean_3); % displays the "perfect" binary image
    %close(imtool_handle_bw_clean_3)
           
    % Write output image:  TiFF file with same tags as input file. ________
    
    imwrite(im_tiff_bw_clean_3,im_tiff_bw_file_path); % 
    
   % Write output image:  TiFF file with same tags as input file. ________
    
    if (success_fg )
            % imwrite(C,im_tiff_bw_clean_2);            
    end; % VALID Arguments        
    
    if ~strcmp(err_msg,'');
        % Dislay Error Message
        disp(err_msg)
    else
        fprintf('\n ... DONE processing: \n')        
    end
    
end %fn CVT_GRAY_TO_BW

%
%[counts,x] = imhist(im_tiff_gray, n)
