function [ local_hd_struct,error_message ] = excel_define_out_sheet(local_hd_struct,expe_ned_table) %   ,expe_ned_table,  handles_text_exp_status)
%EXCEL_DEFINE_OUT_SHEET defines which sheet to write based on the following rules:
% If out excel file does not exist: create a new empty one with a tab called exp_summary.
% if out excel file does     exist: Grab the last sheet.
% if the last seet looks like: tab_xxx  then: new last sheet name would be: tab_xxx+1

% Prep prior to call:
%  local_hd_struct          = struct; %jas_argument
%  local_hd_excel_scan_set_file  = 'C:\Users\jsanchez\Documents\algo2\scan_data_for_wafer_analysis\exp_scan_set_2.xlsx';
%  local_hd_struct.local_hd_excel_scan_set_file =local_hd_excel_scan_set_file;

% Sample call: [ local_hd_struct,error_message ] = excel_define_out_sheet(local_hd_struct,expe_ned_table)

expected_xls_format               = 'Microsoft Excel Spreadsheet';
error_message                     = '';

% Decide if first call or subsequent call based on the existence of the field: last_sheet
if (~(isfield(local_hd_struct,'last_sheet')))
    % FIRST TIME BEING CALL: Check that the file exist.
    try
        [status,~,found_format] = xlsfinfo(local_hd_struct.local_hd_excel_scan_set_file); %  returns status and the name of each spreadsheet in the file.
        %sheet_list
        % FILE EXISTS FOR SURE:
        if (  ((isempty(status))) && (~(strcmpi(status, expected_xls_format))))
            % FILE EXIST: but it is not an excel file: return error:
            error_message = fprintf('Error: An excel file is expected, and found: %s file. Format: %s',status,found_format);
            return;
        end
        % FILE EXIST AND IT IS AN EXCEL FILE
    catch
        % FILE DOES NOT EXIST: Make sure dir does. jas_tbd: if it does not return. errror.
        last_sheet                          = 'exp_summary';
        local_hd_struct.last_sheet          = last_sheet;
        % Define the dir part and check that it is a valid directory:
        [path_str,~,~] = fileparts(local_hd_struct.local_hd_excel_scan_set_file);
        if (isempty(path_str))
            % DIR DOES NOT EXIST:  return error:
            error_message = fprintf('Error: A valid full path excel file is expected, and you entered: %s file',local_hd_struct.local_hd_excel_scan_set_file);
            return;
        end
        
        % DIR DOES EXIST: create empty output file with a first sheet named exp_summary and dump the expe_ned_table with the list of experiments to be used on each tab
        writetable(expe_ned_table,local_hd_struct.local_hd_excel_scan_set_file,'Sheet',last_sheet);
        local_hd_struct.last_sheet          = last_sheet;
        return;
    end % try to get the status
end

% SUBSEQUENT CALL: FILE EXISTS FOR SURE: AND IT IS AN EXCEL FILE. Noo need to do exception handling or check for existance
[~,sheet_list,~] = xlsfinfo(local_hd_struct.local_hd_excel_scan_set_file); %  returns status and the name of each spreadsheet in the file.

% define the new last_sheet to write to:
last_sheet = sheet_list{1,end};
sheet_ndx = strfind(last_sheet,'tab_');
if isempty(sheet_ndx)
    last_sheet = 'tab_001';
else
    new_str = num2str(str2double(last_sheet(end-2:end))+1);
    new_len = size(new_str,2);
    switch new_len
        case 1
            new_tab = strcat('tab_00',new_str);
        case 2
            new_tab = strcat('tab_0',new_str);
        case 3
            new_tab = strcat('tab_',new_str);
        otherwise
            new_tab = strcat('tabx_',new_str);
    end
    last_sheet = new_tab;
end

% HAVE THE LAST SHET TO USE: save it.

local_hd_struct.last_sheet = last_sheet;

end % fn excel_define_out_sheet

% sa_rest_exp_save_all_to_excel:
%
%     ndxdb
%     exper
%     instr
%     carri
%     spotl
%     kitlo
%     proto
%     dates
%     state
%     futur
%     expid
%     proid
%     senso
%     scan
%
%
%     fprintf('\n%-s \n%-s \n%-4d \n%-4d \n%-4d\n' ...
% ,cur_expid...
% ,proid...
% ,sensor_cnt...
% ,scan_len_max...
% ,scan_col_max);