function [                          ...
     tr_matrix ,flag_matrix         ...
    ,time_normalized_matrix         ...
    ,shift_normalized_matrix        ...
    ] ...
    = tr_calc_and_evaluate(         ...
    algo_gb_struct                  ...
   ,tr_matrix,flag_matrix           ...
    ,time_normalized_matrix         ...
    ,shift_normalized_matrix        ...
    ,tr_delta_step_threshold        ...
    ,time_raw_matrix                ...
    ,shift_raw_matrix               ...
    ,feature_matrix,raw_shifts_data ...
    ...,curve_size_len_max,curve_size_cnt_max
    ,file_loaded_count )
%TR_CALC_AND_EVALUATE attempts to calculate the tr value to be used for tr normalization and evaluates the attempt.
%   Peforms the following checks on times and shifts each file:
%   1 - non-nan values for tr1 or tr2: expected length is curve length. (one of the 2 trs is enough to do tr norm)
%   2 - non-nan values for tr1 or tr2 on the other channel              (one of the 2 trs is enough to do tr norm other channel)
% Assumtions: tr_matrix has already being populated with the contents of the raw times and raw shift matrices.
% OUTPUT:
% tr_matrix:               cols with the proper flags set (to debug/evaluate tr normalization).
% flag_matrix:             col: global_flag_tr_valid_tr_row    set
% time_normalized_matrix:  clone of raw_matrix (per Muz request)
% shift_normalized_matrix: cols with normalized shift values or with nan values (when normalization fails).

% declare_globals;

% 1. Reset all tr flags before starting:
for cur_file = 1:1:file_loaded_count
    tr_curve_len         = feature_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file); % jas_tbr: use length of first curve on the file
    tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col, cur_file)  = 0;
    tr_matrix(1:tr_curve_len,algo_gb_struct.tr_other_channel_ok_flag_col,cur_file)  = 0;
    tr_matrix(1:tr_curve_len,algo_gb_struct.tr_both_channel_ok_flag_col, cur_file)  = 0;
end

% 2. evaluate tr matrix and set the propper flags to asses if normalization is possible or not.
for cur_file = 1:1:file_loaded_count
    file_ok_tr           = true; % assume ok until otherwise case is found
    tr_curve_len         = feature_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file); % jas_tbr: use length of first curve on the file
    
    % check for at least one of the 2 TRS times is not nan:
    if (file_ok_tr && sum(isnan(tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_time_col,cur_file))))
        % THERE IS AT LEAST ONE NAN IN COLUMN: tr_mean_12_time_col. NORMALIZATION CAN NOT BE DONE. 
        file_ok_tr=false;
        tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_time_nan_flag_col,cur_file) = isnan(tr_matrix(1:tr_curve_len,tr_mean_12_time_col,cur_file));        
    end
    % check for at least one of the 2 TRS shifts is not nan:
    if (file_ok_tr && sum(isnan(tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_shift_col,cur_file))))
        % THERE IS AT LEAST ONE NAN IN COLUMN: tr_mean_12_shift_col. NORMALIZATION CAN NOT BE DONE.         
        file_ok_tr=false;
        tr_matrix(1:tr_curve_len,tr_mean_12_shift_nan_flag_col,cur_file) = isnan(tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_shift_col,cur_file));
    end
    
    % check for mean tr is sane: No erratic behavior:  |TR1 - TR(i-1) | > delta_tr_trheshold (input parameter default = 5 GRU)
    % check up to one less than the last one because the last one is always is nan (can not do delta for that one!!)
    if (file_ok_tr)
        % SET THE FLAG FOR LARGE DELTAS: 1 == large 0 == small. 
        tr_matrix(1:tr_curve_len-1,algo_gb_struct.tr_delta_12_shift_large_flag_col,cur_file) = abs(tr_matrix(1:tr_curve_len-1,algo_gb_struct.tr_delta_prev_shift_col,cur_file)) > tr_delta_step_threshold;
        tr_matrix(1:tr_curve_len, algo_gb_struct.tr_delta_12_shift_large_flag_col,cur_file)  = 1; % last one is always 1: it can not be compared!
    end
    
    if (file_ok_tr && sum(abs(tr_matrix(1:tr_curve_len-1,algo_gb_struct.tr_delta_prev_shift_col,cur_file)) > tr_delta_step_threshold))
        % AT LEAST ONE DELTA IS LARGER THAN THERESHOLD: call this erratic behavior and label the file as bad for tr norm.
        file_ok_tr=false;
    end  
    
    % check for overall behavior: for this channel: ok when the previous 3 are ok.
    if (file_ok_tr)
        tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file)  = 1; %
    end
    
end; % for each file: odd and even!

% ALL CHECKS WITHIN ALL THE THE FILES AND WITHIN OWN CHANNEL HAD BEEN MADE

% check for both channels being ok for a given sample: For each sample check the "other" channel.
for cur_file = 1:2:file_loaded_count-1
    tr_curve_len         = feature_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file);
    % CUR_FILE is for the ODD channel: Check the even channel.
    
    %     if (   (sum (tr_matrix(1:tr_curve_len,tr_this_channel_ok_flag_col,cur_file)   == 1)) )
    %         % THIS CHANNEL IS ok this channel to true
    %         tr_matrix(1:tr_curve_len,tr_this_channel_ok_flag_col,cur_file)  = 1;
    %     end
    
    if (       (sum (tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file))   == tr_curve_len) ...
            && (sum (tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file+1)) == tr_curve_len) )
        % THIS CHANNEL IS OK AND THE OTHER IS OK: Set "other_channel" flag for this channel to true
        tr_matrix(1:tr_curve_len,algo_gb_struct.tr_other_channel_ok_flag_col,cur_file)  = 1;
    end
    
end; % for each odd file
for cur_file = 2:2:file_loaded_count
    tr_curve_len         = feature_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file);
    % CUR_FILE is for the EVEN channel: Check the odd channel.
    
    %     if (   (sum (tr_matrix(1:tr_curve_len,tr_this_channel_ok_flag_col,cur_file)   == 1)) )
    %         % THIS CHANNEL IS ok this channel to true
    %         tr_matrix(1:tr_curve_len,tr_this_channel_ok_flag_col,cur_file)  = 1;
    %     end
    if (       (sum (tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file))   == tr_curve_len) ...
            && (sum (tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file-1)) == tr_curve_len) )
        % THIS CHANNEL IS OK AND THE OTHER IS OK: Set "other_channel" flag for this channel to true
        tr_matrix(1:tr_curve_len,algo_gb_struct.tr_other_channel_ok_flag_col,cur_file)  = 1;
    end
end; % for each even file


% FINAL MASTER CHECK: both channels must be ok
for cur_file = 1:1:file_loaded_count
    tr_curve_len         = feature_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file);    
    raw_shifts_data_size = size(raw_shifts_data{cur_file});
    shift_curve_cnt      = raw_shifts_data_size(2);
    
    % Check both channels being ok for the current file
    if (       (sum (tr_matrix(1:tr_curve_len,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file))  == tr_curve_len) ...
            && (sum (tr_matrix(1:tr_curve_len,algo_gb_struct.tr_other_channel_ok_flag_col,cur_file)) == tr_curve_len) )
        % BOTH Channels are ok
        tr_matrix(1:tr_curve_len,algo_gb_struct.tr_both_channel_ok_flag_col,cur_file)  = 1;
        % Record Normalization Summary for all curves in this file
        % for cur_curve=1:1:shift_curve_cnt
        flag_matrix(algo_gb_struct.flag_tr_valid_tr_row  ,1:shift_curve_cnt,cur_file) = 1;
        % end;
    else
        % Record Normalization Summary for all curves in this file: set it asbad: ie zero
        % for cur_curve=1:1:shift_curve_cnt
        flag_matrix(algo_gb_struct.flag_tr_valid_tr_row  ,1:shift_curve_cnt,cur_file) = 0;
        % end;
    end;
end; % for each file

% ALL CHECKS ARE DONE AND ALL FLAGS ARE SET. _______________________________________________________________

% Normalize whatever can be normalized

for cur_file = 1:1:file_loaded_count
    shifts_size      = size(shift_raw_matrix(:,:,cur_file));
    curve_count      = shifts_size(2);
    for cur_curve = 1:1:curve_count
        % to be deletd:
        %         if( cur_curve == 64 && cur_file ==2)
        %             display(sprintf(' complete column on feature_matrix with nans: file=%3d curve=%3d',cur_file));
        %         end
        tr_curve_len      = feature_matrix(algo_gb_struct.feature_excel_cnt_row,cur_curve,cur_file); %
        
        if ( sum(isfinite( tr_matrix(:,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file))) == 0 )
            % THE WHOLE CURVE CAN NOT BE NORMALIZED: wipe out the normalized shift data.
            display(sprintf(' complete column on feature_matrix with nans: file=%3d curve=%3d',cur_file,cur_curve));
            shift_normalized_matrix(1:tr_curve_len,cur_curve,           cur_file) = nan;
            break; % get out of the current curve.
        end
        
        
        if ( tr_matrix(1,algo_gb_struct.tr_this_channel_ok_flag_col,cur_file)) % just use the tr flag for the first point of the curve
            % NORMALIZATION FLAGS ARE OK: Do TR normalization  curve_size_len_max
            
            % Next line is questionable: Do we really want to normalize the times like this?
            % Normalized time is the  the mean of the TR time and the time of the sample
            
            % Removed per Muz request(4/5/2017) we do want to keep the original raw times.
            %             time_normalized_matrix(1:tr_curve_len,cur_curve,          cur_file) = mean (...
            %                 [ time_raw_matrix( 1:tr_curve_len,cur_curve,          cur_file) ...
            %                    tr_matrix(      1:tr_curve_len,tr_mean_12_time_col,cur_file)],2 );
            
            time_normalized_matrix(1:tr_curve_len,cur_curve,          cur_file) =  ...
                time_raw_matrix(   1:tr_curve_len,cur_curve,          cur_file);
            
            % Substract the mean of the TR shift to the Sample Shift: All the entries on the row get the same correction: ie substract the avg tr shift.
            % ***** THIS IS THE KEY PART OF NORMALIZATION.... Adjusting the shifts  ....
            shift_normalized_matrix(1:tr_curve_len,cur_curve,           cur_file) ...
                = shift_raw_matrix( 1:tr_curve_len,cur_curve,           cur_file) ...
                -        tr_matrix( 1:tr_curve_len,algo_gb_struct.tr_mean_12_shift_col,cur_file);
            
        else
            % NORMALIZATION FLAGS ARE not OK: skip TR normalization  % jas_tbd: check means done as expected
            % set to nan this curve: so it does not get displayed as a good curve!!!
            shift_normalized_matrix(1:tr_curve_len,cur_curve,           cur_file)  = nan;
        end
    end
end; % for each file
return; % fn tr_calc_and_evaluate   curve_size_len_max,curve_size_cnt_max




% leftovers to be reviewed:

% tr_matrix_extra_row_1_tr_final    = 5;  % no-nan-mean of tr1 and tr2 and no-nan tr abs < tr_abs_max_threshold and no nan-
% tr_matrix_extra_row_2_tr_any_flag = 6;  % >0 when either of the criterias is not met for at least one of the trs.
% %                                         0== both   trs are good and usable: meet all criteria.
% %                                         1== tr1    is nan
% %                                         2== tr2    is nan
% %                                         3== tr1    is too large: | tr1 | > tr_max_threshold
% %                                         4== tr2    is too large: | tr2 | > tr_max_threshold
% %                                         5== dif    is too large: | tr1 - tr2 |  > tr_max_delta_threshold
% %                                         6== change is too small: | tr1(i) - tr2(i) < tr_min_change_threshold
% %                                         7== tr2 is too large | tr2 | > tr_max_threshold
% %                                         8== tr2 is too large | tr2 | > tr_max_threshold
%
%
% tr_matrix_extra_row_3_tr_all_flag = 7;  % 0 both ok.

% extra 3 cols for tr_matrix: +1=mean +2=sigma^2


%                                         1== when no normalization was possible (neither one of the trs is usable).
% tr_extra_cols = 3;
%
% if (tr_normalize_flag)
%     tr_matrix    =  nan(curve_size_len_max,4+tr_extra_cols,file_loaded_count);
% end



