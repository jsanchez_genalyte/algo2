function [ assay_sum_table_qc, error_message ] = get_qc_rpt_data_from_runcard_wrapper( assay_sum_table )
%SA_QC_CALC_RACK_2_RACK  calculates the assay_sum_table_qc from runcard given a assay_sum_table
%   output:
%   1. assay_sum_table_qc: an extended data from the original assay_sum_table with all the relevant info from runcard.
%   2. error message:        blank means no errors.
% Note:   This is just a wrapper for the runcard function:   get_qc_rpt_data_from_runcard_sp_lot_list
%   sample call:
%  [ assay_sum_table_qc,error_message ] = get_qc_rpt_data_from_runcard_wrapper( assay_sum_table);

% S1_A: Get data from runcard for the requested spotl unique list.

input_params.webservice_url     = 'http://10.0.2.226/runcard/soap?wsdl'; % 'http://10.0.2.226/runcard_test/soap?wsdl';
input_params.sp_chip_part_number= '00445';
assay_spotl_u                   = unique(strrep(unique(assay_sum_table.spotl),' ','')); % { 'AB1602' , 'AB160J' }; % {'AB150I'  }; %   { 'AB1602' };             % spot lot number list
input_params.qc_sp_lot_list     = assay_spotl_u';
input_params.qc_part_number     = 'QCP0023-QC'; % spotted lot part number.
input_params.display_flag       = 1;           	% 1 == display as process goes on. 0 == be quiet
% call: get_qc_rpt_data_from_runcard: This table is the one needed to automate the qc_reports.
[ qc_rpt_table,error_message ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params );

% S1_B: Handle special runcard case: lot:  180118 _______________________________________________________________
% From Alex:
% The  only lot with traceability is 180118.
% The  MB PN: 00046 and LN:   AB1705.  The WO has a total of 174, but only the first 100 are the QC carriers.
% Chip PN 00445 LN  AB150I (180118), was issued to PN 00446  LN:  AB1705. (mb_ln)
% Special case: % manual fix: take the first 100 records and replace: AB150I with: 180118
% Special steps
% 1. Check if 180118 is in the list of the requested spotting lots: in assay_spotl_u.
% 2. if it is: check if the file exist: if it does load it. If it does not: save it.
%    [ qc_rpt_table,error_message ] = special_add_from_runcard_180118( input_params, );
[dat_ele_found_flag,~]= is_element_in_cell(assay_spotl_u,'180118');
if (dat_ele_found_flag)
    % USER REQUESTED SPOTTING LOT: 180118. (have to do a special trick because this lot is "specil case in runcard")
    % check if file exists:
    if exist('qc_rpt_table_180118.mat', 'file') == 2
        % FILE EXISTS: Load the matlab file: It has 180118 ready to be appended to the qc_rpt_table
        load('qc_rpt_table_180118.mat','qc_rpt_table_180118')
    else
        % FILE DOES NOT EXIST: generate it:
        input_params_180118                 = input_params;
        input_params_180118.qc_sp_lot_list	= { 'AB150I' };     % **SPECIAL**
        input_params_180118.qc_part_number 	= '00446';          % **SPECIAL** spotted lot part number.
        % call: get_qc_rpt_data_from_runcard: for the special case
        [ qc_rpt_table_180118,error_message ] = get_qc_rpt_data_from_runcard_sp_lot_list( input_params_180118 );
        if (~(isempty(error_message)))
            return;
        end
        % trim the data: first 100 elements according to alex:
        qc_rpt_table_180118 = qc_rpt_table_180118(1:100,:);
        % rename fields:
        for cur_ndx=1:1:100
            qc_rpt_table_180118.splot_ln(cur_ndx)   = {'180118'};
            qc_rpt_table_180118.mb_pn(cur_ndx)      = {'QCP0023-QC'};
        end
        % NOW TABLE LOOKS IDENTICAL AS THE REST OF THE spotl in runcard: Save it.
        save('qc_rpt_table_180118.mat','qc_rpt_table_180118');
        % Done saving file: From now on: Just load it.
    end
    % HAVE THE TABLE: qc_rpt_table_180118. Append it to the qc_rpt_table
    qc_rpt_table =  [ qc_rpt_table ; qc_rpt_table_180118 ];
    % %     a1          = table2struct(qc_rpt_table);
    % %     a2          = table2struct(qc_rpt_table_180118);
    % %     al_table    = struct2table(a1);
    % %     a2_table    = struct2table(a2);
    % %     a3_table    = [al_table ; a2_table];
    % %     qc_rpt_table= a3_table;
end % LOT: 180118 was requested by end user.

% END special case....

% S1+C: reconciliate the 2 tables: Add to assay_sum_table the rack info.

qc_rpt_table.Properties.VariableNames{'mb_id'} = 'carri'; % from:

% NOW WE HAVE A COLUMN TO DO THE OUTER JOIN: carri
assay_sum_table.carri               = categorical(assay_sum_table.carri);
qc_rpt_table.carri                  = categorical(qc_rpt_table.carri);
% remove missing data: ie experiments with bad data: (for the time being use: nan for dsdna
assay_sum_table_qc                  = outerjoin(assay_sum_table,qc_rpt_table,'MergeKeys',true);             % ***** REAL THING ***
blank_ndx                           = isnan(assay_sum_table_qc.dsdna);
assay_sum_table_qc                  = assay_sum_table_qc(~blank_ndx,:);

if (sum(iscell(assay_sum_table_qc.qc_rack_number)))
    assay_sum_table_qc.qc_rack_number   = cell2mat(assay_sum_table_qc.qc_rack_number);
end
end % fn get_qc_rpt_data_from_runcard_wrapper


















