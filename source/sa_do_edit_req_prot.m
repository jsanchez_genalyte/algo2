function [ handles ] = sa_do_edit_req_prot( hObject, eventdata, handles )
%sa_do_edit_req_prot  
%   Detailed explanation goes here

% Hints: get(hObject,'String') returns contents of sa_do_edit_req_prot as text
%        str2double(get(hObject,'String')) returns contents of edit_req_prot as a double


[a_request_cell_str] = strsplit(get(handles.edit_req_prot,'string'),{','},'CollapseDelimiters',true);
a_request_cell_str = a_request_cell_str';

% [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla_regex( get(handles.edit_req_prot,'string'));
% HAVE THE LIST OF PROTOCOL LOTS REQUESTED: Call the db
% request data
% append to toe exper from db
% update status: requesting experiments
% toe_status:  count:  xxx selected: yyy 
% 

if ( (isempty(a_request_cell_str)) || ((~(isempty(a_request_cell_str))) && ( isempty(a_request_cell_str{1}))))
    handles.ui_state_struct.req_prot_list   = {};
    status_message = sprintf('Not using Protocol Name filter ' );
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
    
else
    handles.ui_state_struct.req_prot_list   = a_request_cell_str;
    a_request_str = char(a_request_cell_str);
    status_message = '';
    for cur_req_ndx = 1:1:size(a_request_str,1)
        status_message = strcat(status_message,sprintf('  %s , ',a_request_str(cur_req_ndx,:)));
    end
    status_message = strcat('Protocol Filter:    ',status_message(1:end-1));
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
    
end

end

