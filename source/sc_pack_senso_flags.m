function [ senso_flag_str ] = sc_pack_senso_flags( senso_flag_array )
%SC_PACK_SENSO_FLAGS Packs an array of sensogram flags(resulting from classification) into a single string
%   handles up to 10 flags,
len_sf = length(senso_flag_array);
final_result_char = 'A'; % assume anoma until proven otherwise
switch len_sf
    case 1
        senso_flag_str = sprintf('%-2d ',senso_flag_array(1));
    case 2
        senso_flag_str = sprintf('%-2d %-2d ',senso_flag_array(1:2));
        final_result_char = 'G'; %  found a good one   jas_temp need to refine the results ... jas_tbd
    case 3
        senso_flag_str = sprintf('%-2d %-2d %-2d ',senso_flag_array(1:3));
    case 4
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d ',senso_flag_array(1:4));
    case 5
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:5));
    case 6
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:6));
    case 7
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d  %-2d %-2d %-2d ',senso_flag_array(1:end));
    case 8
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:end));
    case 9
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:end));
    case 10
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:end));
    otherwise
        senso_flag_str = sprintf('%-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:end));
        display(sprintf('\n Warning_: too_many_senso_flagsL num_flags = %-2d',len_sf));
end
senso_flag_str = strcat(final_result_char,senso_flag_str); % preappend the final character: either G for good or A for anom
display(sprintf('senso_flag_str_ %s',senso_flag_str));
debug_on_fg = 0;
if (debug_on_fg)
    switch len_sf
        case 1
            display(sprintf('Sensogram_Flags: %-2d ',senso_flag_array(1)));
        case 2
            display(sprintf('Sensogram_Flags: %-2d %-2d ',senso_flag_array(1:2)));
        case 3
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d ',senso_flag_array(1:3)));
        case 4
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d ',senso_flag_array(1:4)));
        case 5
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:5)));
        case 6
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:6)));
        case 7
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d  %-2d %-2d %-2d ',senso_flag_array(1:end)));
        case 8
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d ',senso_flag_array(1:end)))
        case 9
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d',senso_flag_array(1:end)));
        otherwise
            display(sprintf('Sensogram_Flags: %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d %-2d',senso_flag_array(1:end)));            
            display(sprintf('\nnum_flags = %-2d',len_sf));
            display(senso_flag_array);
            display('\n');
    end
end % debug_on
end % fn sc_pack_senso_flags

