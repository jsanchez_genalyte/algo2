function [error,msg] = pickUnitToWorkOrder(obj,serial,partnum,partrev,workorder,warehousebin,warehouseloc,operator)
%pickUnitToWorkOrder(obj,serial,partnum,partrev,workorder,warehousebin,warehouseloc,operator)
%
%     Input:
%       serial = (string)
%       partnum = (string)
%       partrev = (string)
%       workorder = (string)
%       warehousebin = (string)
%       warehouseloc = (string)
%       operator = (string)
%   
%     Output:
%       error = (int)
%       msg = (string)

% Build up the argument lists.
values = { ...
   serial, ...
   partnum, ...
   partrev, ...
   workorder, ...
   warehousebin, ...
   warehouseloc, ...
   operator, ...
   };
names = { ...
   'serial', ...
   'partnum', ...
   'partrev', ...
   'workorder', ...
   'warehousebin', ...
   'warehouseloc', ...
   'operator', ...
   };
types = { ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   '{http://www.w3.org/2001/XMLSchema}string', ...
   };

% Create the message, make the call, and convert the response into a variable.
soapMessage = createSoapMessage( ...
    'urn:runcard_wsdl', ...
    'pickUnitToWorkOrder', ...
    values,names,types,'rpc');
response = callSoapService( ...
    obj.endpoint, ...
    'urn:runcard_wsdl#pickUnitToWorkOrder', ...
    soapMessage);
[error,msg] = parseSoapResponse(response);
