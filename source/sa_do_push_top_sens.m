function [ handles ] = sa_do_push_top_sens( hObject, ~, handles )  % eventdata
%sa_do_push_top_sens handles switching to the top_sens tab (the real thing)  EXPERIMETNT --> SENSOGRAM
% freeze:alexa   B19RHWH B19R5TK B19RABQ B19RS8K B19RHWH
% freeze:alexa   Salt 1 Step - 5 Pt. Curve Protocol
%  to_test_francis_300 and runcard: exp: ANA-QC  spotl: AB1602 AB160J  180118    total: 143  runcard: 300
%  to_test_francis_100:          
%  exp:         ANA-QC-AB1602   
%  spot_lot:    AB1602 AB160J    

%  to_test_francis_300:  (see above)
% ANA-QC-180
% 180018 180023 180118 not found!!! 

% 
% alexa_125_2:   Salt 1 Step - 5 Pt. Curve Protocol
% alexa_125_2:   05/09/2018
% alexa_125_2:   B19ROEH  B19R9TR  B19RAEB  B19RUB5  B19RTAO

% Air Only Protocol - 125 runs
% Salt 1 Step - 5 Pt. Curve Protocol - 125 Runs
% 
% Barcodes:
% 
% B19ROEH
% B19R9TR
% B19RAEB
% B19RUB5
% B19RTAO

% B19ROEH  B19R9TR  B19RAEB  B19RUB5  B19RTAO

% 3 filters / 4 exp:
% exp_name:     M58 Air Only Run 4        
% spot_lot:     B19RTAO
% protocol:     Air Only Protocol



% TOP ENTRY FUNCTION TO PROCESS List of Requested Experiments: either gen_excel_rpts or  gen_qc_rpt_data (Auto version)
% to_test:   for 100_exp:  ANA-QC-AB1602     spot AB1602    francis
% to_test:   C:\Users\jsanchez\Documents\gen_qc_rpt\test\francis excel sample qc rpt excel output file
% to_test:   FROM 04/26/2018 02:06:03 PM TO 04/27/2018 07:00:00 AM
% to_test:   A05R1 carrier:  brings 11 experiments.  Experiment: Stability 4C filters to only 2 
% to_test:   B19RTAO  recent carrier. Alexa errors out on scan data.  exp name:    M60 Air Only Run 1   50-->1
% pipeline:  sa_do_push_top_sens --> sa_rest_db_set_get_all_exp_full_data
% pipeline:  sa_do_push_top_sens --> gen_qc_rpt_data

% to_test: Alexa 05_14_2018 part 2: 125   B19ROEH  B19R9TR  B19RAEB  B19RUB5  B19RTAO  
% to_test: Alexa 05_14_2018 part 2: 125   Salt 1 Step - 5 Pt. Curve Protocol



% policy for handling of bad data (from web_services):  (05_04_2018) 
% 1 - exper table must have a data column called: data: blank: ok data (initalized all with ones)  (maybe a future is already there). add exp_id to the table too.
% 2 - senso table must have a data column called: data: blank: ok data (initalized all with ones)  (maybe a future is already there). add exp_id to the table too.
% 3 - when retrieving protocols:   if bad_webread: exp_data does_not get reset to 0. exper.data=1 else exper.data =0; init struct before call blank-null.
% 4 - when retrieving exp-details: if bad_webread: exp_data does_not get reset to 0. exper.data=1 else exper.data =0; init struct before call
% 5 - when retrieving probes:      if bad_webread: exp_data does_not get reset to 0. senso.data=1 else senso.data =0; (all 4 sensors) init struct before call.
% 6 - when retrieving sensors:     if bad_webread: exp_data does_not get reset to 0. senso.data=1 else senso.data =0; (blank/null all fields) init struct before call.
% 7 - when retrieving sensors:     if bad_webread: exp_data does_not get reset to 0. senso.data=1 else senso.data =0; (scan: null block) (init block before call)

% Test Request: 02/07/2018
%    EXPERIMENT                     DEB CARRIER SPOTL   KIL PROTOCOL
% 1	 ANA-RA-1 channel run testing	M29	A03RGXS	180016	[]	Long Wash ANA Panel Protocol 85112PRT	02/07/2018	complete
% 2	 ANA-RA-1 channel run testing	M26	A03RL1A	180016	[]	Long Wash ANA Panel Protocol  85112PRT	02/07/2018	complete
% 3	 RR-LF-EqualDet-170352          M27	A41YP1V	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
% 4	 RR-LF-EqualDet-170352          M62	A41DSQW	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
% 5	 ANA-RA-1 channel run testing	M29	A03RGXS	180016	[]	Long Wash ANA Panel Protocol 85112PRT	02/07/2018	complete
% 6	 RR-LF-EqualDet-170352          M27	A41G657	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
% 7	 ANA-RA-1 channel run testing	M26	A03RL1A	180016	[]	Long Wash ANA Panel Protocol 85112PRT	02/07/2018	complete
% 8	 RR-LF-EqualDet-170352          M62	A4192ZG	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
% 9	 RR-LF-EqualDet-170352          M66	A41EX5W	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
% 10 RR-LF-EqualDet-170352          M64	A41WY30	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
% 11 RR-LF-EqualDet-170352          M55	A412H8S	170352	[]	RH panel 2 channel-3shift               02/07/2018	complete
%
% Test_2 Request: last 30 days      K1A-1  0.01M EPPS:                             1 experiment   
%                                   RH panel 1-3shift-HSA
% test3_ Request: last week 3       RH panel 1-3shift-HSA       carrier  A05R1     3  experiments:   

%   Retrieves the sensor and scan level data for the requested experiments.  
% Do ui first:   sa_do_push_top_sens
% __________________________________________________________________________________________________
% 2018_04_03: Carrier_alexa_4_  Barcodes: A14RZEW A14RK0C A14RJSR   First ones correct

% jas_temp_generation of gen_auto_qc_rpt QC reports:
%        Turn on 2 flags: gen_qc_auto_rpts and local_hd_db_exp_full_save_flag
% notes: To generate QC reports, these are the assumptions about the data:
%        1 - Have QC runs, i.e. the analytes run on QC runs, i.e. the ones listed below.
%            if the analytes are not there: the code will error out.




% jas_temp_generation of exp_scan_optical_data:

local_hd_struct                                     = struct;
local_hd_struct.local_hd_try_first_flag             = true;    %  jas_temp_hard_coded;  jas_tbd: configuration_parameter: true: try to find a matlab file with the scan data first. If not found: use the API         
local_hd_struct.local_hd_save_scan_flag             = true;    %  jas_temp_hard_coded;  jas_tbd: configuration_parameter: true: save the scan data to a matlab file (to avoid future quering on the same data)          
local_hd_struct.local_hd_save_scan_excel_flag       = true;   % *jas_temp_hard_coded;  jas_tbd: configuration_parameter: true: save scan/optical/FSR to an excel file. 
local_hd_struct.local_hd_save_qc_rpt_excel_flag     = true;   % *jas_temp_hard_coded;  jas_tbd: configuration_parameter: true: save qc_rpt       to an excel file.

if (isdeployed)
    % DEPLOYED APP: HARDCODED DIR:  C:\temp\sa_files
    local_hd_struct.local_hd_db_data_expr_scan_mat_dir  = 'C:\sa_files\db_data_scan_files\'; %jas_tbd: configuration_parameter: local dir to store .mat files with scan data
    local_hd_struct.local_hd_excel_scan_set_file        = 'C:\sa_files\db_data_qc_scan_output\exp_scan_set_fsr.xlsx'; % same for both.
    
    local_hd_struct.gen_qc_auto_rpts                   = false; % to generate: gen_auto_qc_rpt     
    local_hd_struct.local_hd_db_exp_full_save_flag     = false; % to generate/save/load all needed assay rpts (for selected expeiments)
    local_hd_struct.local_hd_db_exp_full_mat_dir       = 'C:\sa_files\db_data_exp_full_files\'; %jas_tbd: configuration_parameter: local dir to store .mat files with scan data
    
else
    % MATLAB CMD_PROMPT: jas dirs (local development)
    local_hd_struct.local_hd_db_data_expr_scan_mat_dir  = 'C:\Users\jsanchez\Documents\algo2\db_data_scan_files\'; %jas_tbd: configuration_parameter: local dir to store .mat files with scan data
    local_hd_struct.local_hd_excel_scan_set_file        = 'C:\Users\jsanchez\Documents\algo2\db_data_qc_scan_output\exp_scan_set_fsr_chuong.xlsx'; % same for both.
    
    local_hd_struct.gen_qc_auto_rpts                   = false; % to generate: gen_auto_qc_rpt   
    local_hd_struct.local_hd_db_exp_full_save_flag     = true; %* to generate/save/load all needed assay rpts (for selected expeiments) for gen_auto_qc_rpt
    local_hd_struct.local_hd_db_exp_full_mat_dir       = 'C:\Users\jsanchez\Documents\algo2\db_data_exp_full_files\'; %jas_tbd: configuration_parameter: local dir to store .mat files with scan data
end

% to_test: FROM 04/26/2018 02:06:03 PM TO 04/27/2018 07:00:00 AM

tic            % to record time to pull scan data for the 136 sensors per experiment
% niu:ui_state_struct             = handles.ui_state_struct;

% S0. Experiment: Make sure structs are well formed: If not: Create them ___________________________
if (~(isfield(handles,'db_data_exp_struct')))
    handles.db_data_exp_struct              = struct;
    handles.db_data_set_struct.rows_db_cnt  = {0};
end
db_data_exp_struct                          = handles.db_data_exp_struct; % data from last: sa_do_push_req_expe

if (~(isfield(db_data_exp_struct,'expe_req_table')))
    db_data_exp_struct.expe_req_table = table();
    db_data_exp_struct.expe_found_cnt = 0;
end
expe_req_table                  = db_data_exp_struct.expe_req_table;
exp_cnt                         = db_data_exp_struct.expe_found_cnt; % same as:    height(expe_req_table);

status_msg = sprintf('Processing Request for %-4d Experiments  ...',height(db_data_exp_struct.expe_req_table));
set(handles.text_exp_status,'string',status_msg,'ForegroundColor','blue','Fontsize',12); %'blue'    % clear whatever we had before.
drawnow;
assay_ana_header = {'hsa','dsdna','hsa3','jo_1','sm','hsa6','scl_70','hsa8','cenp_b','hsa10','ss_a_60','hsa12','ss_b','rnp','anti_igg','human_igg'};
% new_code: 05_08_18 qc_rpt: load/save/query all assay_rpts needed for qc_rpt.
handles_text_exp_status         = handles.text_exp_status;


if (local_hd_struct.gen_qc_auto_rpts)   % gen_auto_qc_rpt
    % PRODUCING AUTOMATED QC RPT: WEEKS 1 AND 2 gen_auto_qc_rpt
    [exp_full_struct,error_message] = sa_rest_db_set_get_all_exp_full_data(local_hd_struct,expe_req_table,handles.rest_token,handles_text_exp_status);
    use_norm_flag = true;  % jas_temp_hard_coded_parameters
    exc_outl_flag = true;  % jas_temp_hard_coded_parameters
    % Genererate Assay Dev rpt: Summary and Detail for all  the included Experiments.
    [ assay_sum_table, assay_det_table,error_message ] = gen_qc_rpt_data( expe_req_table,exp_full_struct,handles_text_exp_status,use_norm_flag,exc_outl_flag );
    if (~(strcmp(error_message,'')))
        return;
    end
    % new way: First calculations for qc_lot_2_lot with stats summary. (vs: old way: sa_qc_calc_lot_2_lot)
    %          Second plotting: one window per plot (need to add summary table to the plot. (lower part of the window.
    % WEEK_1 QC_RPT_1:  lot_2_lot
    [ qc_lot_2_lot_struct,error_message] = sa_qc_calc_lot_2_lot( assay_sum_table,assay_ana_header );
    if (~(strcmp(error_message,'')))
        return;
    end
    
    % WEEK_2 QC_RPT_2:  rack_2_rack sum _______________________________________________
    
    [ assay_sum_table_qc, error_message ] = get_qc_rpt_data_from_runcard_wrapper( assay_sum_table );
    if (~(isempty(error_message)))
        return;
    end            
    [ qc_lot_2_lot_struct,qc_rack_2_rack_struct,error_message] = sa_qc_calc_rack_2_rack( assay_sum_table_qc,assay_ana_header );
    if (~(isempty(error_message)))
        return;
    end
    
    % WEEK_2.5 QC_RPT_2.5:  rack_to_rack_det box_plots _______________________________________________
        [ assay_det_table_qc, error_message ] = get_qc_rpt_data_from_runcard_wrapper( assay_det_table );
    if (~(isempty(error_message)))
        return;
    end
    [ qc_lot_2_lot_det_struct,qc_rack_2_rack_det_struct,error_message] = sa_qc_calc_rack_2_rack_det( assay_det_table_qc,assay_ana_header );
    if (~(isempty(error_message)))
        return;
    end
    close_all_true_figures;
    
    % WEEK_3 QC_RPT_3:  rack_2_rack rack_to_rack_det charts  _______________________________________________
    [ qc_control_charts_det_struct,error_message] = sa_qc_calc_ana_control_charts_det( assay_det_table,assay_ana_header );
    if (~(strcmp(error_message,'')))
        return;
    end
    % Generate an HTML view of the saved MATLAB file.
    % Published figure window appearance 'entireGUIWindow' (default) | 'print' | 'getframe' | 'entireFigureW
    
    publish('sa_do_plot_ana_ctl_charts_url.m'...
        , 'figureSnapMethod','getframe' ... %  'entireFigureWindow'...
        ,'useNewFigure',true...
        ,'showCode' ,false ...
        );
    % The publish command executes the code for each cell in sa_do_plot_ana_rack_2_rack_url.m, and saves the file to /html/sa_do_plot_ana_lot_2_lot_url.html.
    % View the HTML file.
    web('html/sa_do_plot_ana_rack_2_rack_url.html');
end % gen_qc_auto_rpts: producing gen_auto_qc_rpt: weeks 1 and 2

test_flag = 0;  % 0 or 1 to force a return without loading exp nor sensor: Just SWITCHING. 
% jas_test_only: ntx_3_lines
if (test_flag)
    set(handles.ui_panel_expe,'Visible','off');
    set(handles.ui_panel_sens,'Visible','on');
    return;
end

if (exp_cnt == 0) % future:  jas_temp: for development == 0
    % NO DATA TO WORK WITH: Do not let user go to sensograms: Return
    warning_message = sprintf('You need to have at least 1 experiment in the TABLE OF EXPERIMENTS to be able to analze sensograms' );
    set(handles.text_exp_status,'string',warning_message,'ForegroundColor','red','Fontsize',12); %'blue'
    guidata(hObject, handles);
    return;
end

% S1. Sensogram: Make sure structs are well formed: If not: Create them ___________________________
list_tos_expid = {};
if (isfield(handles,'db_data_set_struct'))
    if (isfield(handles.db_data_set_struct,'db_metad_set_struct'))
        % HAVE SOMETHING ALREADY:
        list_tos_expid          = categories(categorical(handles.db_data_set_struct.db_metad_set_struct.expid)); % jas_tbd: add expid to     db_metad_set_struct.expid
    else
        % NO METADATA YET: Create it
        handles.db_data_set_struct.db_metad_set_struct          = struct;
        handles.db_data_set_struct.db_metad_set_struct.expid    = {};
    end
else
    % NO DB_DATA_SET_STRUCT YET: Create it
    handles.db_data_set_struct                                  = struct;
    handles.db_data_set_struct.db_metad_set_struct              = struct;
    handles.db_data_set_struct.db_metad_set_struct.expid        = {};     % for sensors!
end
db_data_set_struct = handles.db_data_set_struct;

% S1. Determine what data from exp is already in the sensogram side: tos ___________________________

list_toe_expid          = expe_req_table.expid;  % It may need a loop doing one at a time: or do an intersert! or not in cur_exp_ndx);
db_cur_toe_nim_ndx      = ismember(list_toe_expid, list_tos_expid);
db_cur_toe_nim_ndx      = ~(db_cur_toe_nim_ndx);   %This is the ndx in toe of what needs to be requested to the api.

% HAVE THE : db_cur_toe_nim_ndx experiments not load in memory i.e. not in the      db_data_set_struct
% S2 - calculates new exp to be requested to the db: one of the following 2 ways:
exp_request_cnt = sum(db_cur_toe_nim_ndx);

% % info_message  = sprintf('Retrieving sensor and scan data for %4d   experiments. Data retrieval rate: ~1 min per experiment. Enjoy your coffee  .....',exp_request_cnt );
% % set(handles.text_exp_status,'string',info_message,'ForegroundColor','blue','Fontsize',12); %'blue'
% % drawnow;
% % guidata(hObject, handles);


% niu: list_toe_expid = list_toe_expid(db_cur_toe_nim_ndx);
expe_ned_table = expe_req_table(db_cur_toe_nim_ndx,:);
% next lines do  not belong here: 2018_05_15 (what was I thingking!)
% % dark_green_color = [0.1  0.5  0.2];  % nice dark green.
% % info_message  = sprintf('DONE Retrieving sensor and scan data for %4d   experiments. ',exp_cnt );
% % set(handles.text_exp_status,'string',info_message,'ForegroundColor',dark_green_color,'Fontsize',12); %'blue'
% % drawnow;

% Next function call replaces: fill_in_db_set_from_fake_data
% Inside call the save excel file with scan and FSR data if the flat is set to true.
[ db_data_set_struct,db_metad_set_table,error_message] = sa_rest_db_set_get_all_data(local_hd_struct...
    ,expe_ned_table,handles.rest_token,db_data_set_struct,handles.text_exp_status);
elapsed_time = toc;
if (elapsed_time < 60)
    msg = sprintf(' %4.1f  sec.',round(elapsed_time,1));
else
    msg = sprintf(' %7.1f  min.',round((elapsed_time/60),1));
end
if (isempty(error_message))
    info_message = sprintf('Experiment count = %4d   Time required to retieve scan data from the DB = %s',exp_cnt,msg);
    fprintf('\n%s\n ',info_message);
    set(handles.text_exp_status,'string',info_message,'ForegroundColor','blue','Fontsize',12); %'blue'
    set(handles.text_status,    'string',info_message,'ForegroundColor','blue','Fontsize',12); %'blue'  
    handles.db_data_set_struct    = db_data_set_struct;        
    % save experiment stuff. What is incremental wrt sensograms:
    db_data_exp_struct.expe_ned_table = expe_ned_table;
    db_data_exp_struct.expe_ned_cnt   = height(expe_ned_table);
    handles.db_data_exp_struct        = db_data_exp_struct;
        
    % Finally .. switch to sensor side:
    handles.ui_state_struct.db_session_opened   = 1;     % flag to tell that have a loaded/opened session.
    handles.ui_state_struct.cur_session_name    = {''};
    handles.ui_state_struct.local_input_dir     = {handles.input_parameters_struct.local_input_dir}; % use the excel dir to start with (poor man's option)
    guidata(hObject, handles);
    [handles]                                   = sa_ui_set_env_after_get_data( handles );
    % DO THE SWITCH
    set(handles.ui_panel_expe,'Visible','off');
    set(handles.ui_panel_sens,'Visible','on');
    info_message = sprintf('Data Retrieval: DONE. Experiment count = %4d   Time required to retieve scan data from the DB = %s',exp_cnt,msg);
    fprintf('\n%s\n ',info_message);
    set(handles.text_status,'string',info_message,'ForegroundColor','blue','Fontsize',12); %'blue'
    
    guidata(hObject, handles); % Update handles structure  FDBK to end-user: cnts of loaded data.
    % do dlg: if  ( max(senso_curves_perp_cnt+anomn_curves_per_plot_cnt) > 20 ) do dlg: Classiff Senso ... Wait ..
    
    % Classify sensograms to display on the screen according to which plots are up: good/anom or both
    [handles ]                  = sc_classify_sensograms(handles); % classify only what is needed to display in screen
    % create TOS
    handles.ui_state_struct.ui_request_flag = 'g_n_a'; % by default start with good and anomalous.
    [tos_cell,tos_col_struct]	= tos_create(handles.db_data_set_struct.rows_db_cnt{1}); % future.   sum(handles.ui_state_struct.db_cur_fit_net_ndx);
    handles.tos_col_struct      = tos_col_struct;
    handles.tos_cell            = tos_cell; % tos_cell has the full_db_data_set
    
    
    % Save the virigin full tos_cell (untochable!)
    [handles]                   = ui_do_fill_tos_sens_set(handles);
    
    % build indices to be used by plot and tos
    % 3 cases: G only A only or G&A visible on the screen.
    if (handles.ui_state_struct.checkbox_hide_anom) % hide anom is on
        handles.ui_state_struct.ui_request_flag = 'g_only';
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_full_pos );
        set(handles.axes_anom_sens,'Visible','off' );
        cla(handles.axes_anom_sens);
        guidata(hObject, handles);
        set(handles.axes_good_sens,'Visible','on' );        
    end
    if (handles.ui_state_struct.checkbox_hide_good) % hide good is on
        handles.ui_state_struct.ui_request_flag = 'a_only';
        set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_full_pos );
        set(handles.axes_good_sens,'Visible','off' );
        cla(handles.axes_good_sens);
        guidata(hObject, handles);
        set(handles.axes_anom_sens,'Visible','on' );
    end
    if ( (~(handles.ui_state_struct.checkbox_hide_good)) && (~(handles.ui_state_struct.checkbox_hide_anom) )) % hide anom is off and  hide good is off
        handles.ui_state_struct.ui_request_flag = 'g_n_a';
        % jas_2018_04_13_: 
        if (~(isfield(handles.ui_state_struct,'axes_good_half_pos')))
            sprintf('\nLogic error: as_2018_04_13 axes_good_half_pos is not member of handles.ui_state_struct. jas_tbdebug')            
        end
        set(handles.axes_good_sens,'Position',handles.ui_state_struct.axes_good_half_pos );
        set(handles.axes_anom_sens,'Position',handles.ui_state_struct.axes_anom_half_pos );
        set(handles.axes_good_sens,'Visible','on' );
        guidata(hObject, handles);
        set(handles.axes_anom_sens,'Visible','on' );
    end
    handles.ui_state_struct	= ui_eval_all_indices( handles.ui_state_struct.db_cur_fit_net_ndx...
        , handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct,handles.text_status);
    [handles]              	= ui_do_plot_sens_cur(hObject,handles);
    % Populate TOS on the UI just with what is selected:
    [handles ] = sa_ui_update_tos(handles);
    % move the ndx to the next bock
    handles.ui_state_struct.senso_start_ndx_save = handles.ui_state_struct.senso_start_ndx;
    if (isnan(handles.ui_state_struct.senso_start_ndx_next))
        % GOT TO THE END: roll over to the first one
        handles.ui_state_struct.senso_start_ndx = 1;
    else
        handles.ui_state_struct.senso_start_ndx = handles.ui_state_struct.senso_start_ndx_next; % get ready for next call
    end
    guidata(hObject, handles); % Update handles structure
    
    
else
    % DATA RETRIEVAL FROM DB FAILED: Rpt error. Do not save bogus data.
    fprintf('\nERROR: %s\n ',error_message);
    error_message_user = sprintf('DATABASE CONNECTION ERROR: %s Try again ...',error_message);    
    set(handles.text_exp_status,'string',error_message_user,'ForegroundColor','red','Fontsize',12); %'blue'    
    % force a reconnect ... poor mans 
    [ error_message, handles ]  = sa_rest_do_connect( handles,true );    
    guidata(hObject, handles);
end

end % fn sa_do_push_top_sens



% GOAL: To fill db_data_set_struct with data from toe and API rather than using the fake_input_data function (from excel)

% db_data_set_struct =
%
%   struct with fields:
%
%                    orndx: [1536�1 double]
%                    probe: {1�16 cell}        from  API: get probe data
%      db_metad_set_struct: [1�1 struct]
%                 smd_cols: [1�1 struct]
%                    datef: [1536�1 double]
%                    bligc: [1536�1 double]
%                    ampgc: [1536�1 double]
%                    shifc: [1536�1 double]
%            scan_raw_time: {[1536�63 double]}
%             scan_raw_gru: {[1536�63 double]}
%          scan_raw_tr_gru: {[1536�63 double]}
%             scan_nrm_gru: {[1536�63 double]}
%      scan_nrm_gru_matlab: {[1536�63 double]}
%              rows_db_cnt: {[1536]}
%              cols_db_cnt: NaN
%                    bligr: [1536�1 double]
%                    ampgr: [1536�1 double]
%                    shifr: [1536�1 double]
%                    bligj: [1536�1 double]
%                    ampgj: [1536�1 double]
%                    shifj: [1536�1 double]
%                    bligm: [1536�1 double]
%                    ampgm: [1536�1 double]
%                    shifm: [1536�1 double]
%                    nomfg: 0
%     sc_clasi_flag_struct: [1�1 struct]
%     sc_clasi_vals_struct: [1�1 struct]
%             scan_raw_len: {[1536�62 double]}
%            sc_clasi_flag: [1536�10 double]
%            sc_clasi_vals: [1536�13 double]

% db_metad_set_struct =
%
%   struct with fields:
%
%     clasi: [1536�1 categorical]
%     probe: [1536�1 categorical]
%     dates: [1536�1 categorical]
%     proto: [1536�1 categorical]
%     sampl: [1536�1 categorical]
%     chipl: [1536�1 categorical]
%     exper: [1536�1 categorical]
%     instr: [1536�1 categorical]
%     carri: [1536�1 categorical]
%     chann: [1536�1 categorical]
%     clust: [1536�1 categorical]
%     datat: [1536�1 categorical]
%     valfg: [1536�1 categorical]
%     outfg: [1536�1 categorical]
%     error: [1536�1 categorical]

% probe_get_channel_number.m
% probe_get_channel_number_for_sensor_number.m
% probe_get_map.m
% probe_get_sensor_names_and_numbers.m
