function [db_data_set_struct,db_metad_set_table,error_message  ] = sa_rest_db_set_get_all_data(...
    local_hd_struct,expe_ned_table,rest_token,db_data_set_struct,handles_text_exp_status) % algo_gb_struct, % SCAN_OPTICAL_EXCEL:
%SA_REST_DB_SET_GET_ALL_DATA fills in a db_set from real api db data . Replaces: fill_in_db_set_from_fake_data and fake_input_data
%   returns: db_data_set_struct  with the data loaded corresponding to the experiments listed in: expe_ned_table
% replaces: good old fake_input_data and fill_in_db_set_from_fake_data  raw_times_data,raw_shifts_data, raw_col_names_data,raw_tr_data,meta_data_data
% pipeline_new:  Sensogram_Analysis_App_10 --> pushbutton_top_sens_Callback  --> sa_do_push_top_sens --> sa_rest_db_set_get_all_data --> sa_rest_exp_get_sensors
% pipeline_old:  Sensogram_Analysis_App_10 --> pushbutton_get_data_Callback  --> fake_input_data     --> fill_in_db_set_from_fake_data --> sa_rest_exp_get_scan
% sa_rest_db_set_get_all_data --> excel_define_out_sheet

%local_hd_try_first_flag         = local_hd_struct.local_hd_try_first_flag;
%local_hd_save_scan_flag         = local_hd_struct.local_hd_save_scan_flag;
local_hd_save_scan_excel_flag    = local_hd_struct.local_hd_save_scan_excel_flag;    % to save scan/optical to an excel file.
local_hd_save_qc_rpt_excel_flag  = local_hd_struct.local_hd_save_qc_rpt_excel_flag;  % to save qc_rpt       to an excel file.

max_sensor_cnt_esti         = 136;  % jas_tbd_config_parameter    _max_sensor_cnt_
% this is the upper bound of how long I can manage scans: More than that it will show as lines to zero: From the end back to zero....
max_scan_len_esti           = 140;  % % jas_tbd_config_parameter  _max_scan_len_   to pre_allocate a reasonable size)  % tipically 63 for cilica 102
%                                   % jas_tbd_make it a configurable parameter: if zero lines at end of sensogram: means this value is too short.
error_message = '';

% columns to fill so we have everything to populate the tos
exp_request_cnt = height(expe_ned_table);
cur_sen_esti_cnt= exp_request_cnt * max_sensor_cnt_esti;

if (local_hd_save_qc_rpt_excel_flag)
    % QC_RPT_EXCEL:
    % one table/excel_tab for ALL experiments together: all 128 sensors of all experiments.
    % add one col at beginning: 'sensor_type' to easily trim at the writting time and keep only analysis_sensors:
    % ndx =  (qc_rpt_detail_table.sensor_type == 2) || (qc_rpt_detail_table.sensor_type == 3)
    % write:  qc_rpt_detail_header(ndx,2:end);
    qc_rpt_detail_header                        = {'sensor_type','experiment','run_date','chip','sample_id','analyte','channel','sensor_cluster_number'...
        ,'sensor_number','is_valid','is_outlier','gru_shift_raw','gru_shift_normalized','instrument','carrier_serial_number','spotting_lot_number','protocol','tracking_id'...
        ,'baseline_start','baseline_duration','amplified_start','amplified_duration','guid'};
    qc_rpt_detail_cell                            = cell(cur_sen_esti_cnt,size(qc_rpt_detail_header,2));
    qc_rpt_detail_table                         = cell2table(qc_rpt_detail_cell);
    qc_rpt_detail_table.Properties.VariableNames= qc_rpt_detail_header;
end

clasi           = cell(cur_sen_esti_cnt,1);
probe      	    = cell(cur_sen_esti_cnt,1);
datef      	    = nan(cur_sen_esti_cnt,1);
proto      	    = cell(cur_sen_esti_cnt,1);
sampl      	    = cell(cur_sen_esti_cnt,1);
chipl      	    = cell(cur_sen_esti_cnt,1);
exper      	    = cell(cur_sen_esti_cnt,1);
instr      	    = cell(cur_sen_esti_cnt,1);
carri      	    = cell(cur_sen_esti_cnt,1);
chann      	    = nan(cur_sen_esti_cnt,1);
clust      	    = nan(cur_sen_esti_cnt,1);
datat      	    = cell(cur_sen_esti_cnt,1);
valfg      	    = nan(cur_sen_esti_cnt,1);
outfg      	    = nan(cur_sen_esti_cnt,1); % outlier_flag
error      	    = cell(cur_sen_esti_cnt,1);
spotl           = cell(cur_sen_esti_cnt,1);
expid           = cell(cur_sen_esti_cnt,1); % jas_dbg_only
dates           = cell(cur_sen_esti_cnt,1);

bligr     	    = nan(cur_sen_esti_cnt,1); % raw base line gru          --scan--
ampgr     	    = nan(cur_sen_esti_cnt,1); % raw amplif    gru          --scan--
shifr     	    = nan(cur_sen_esti_cnt,1); % raw           gru shift

bligj     	    = nan(cur_sen_esti_cnt,1); % nor base line gru          --scan--? or calc?
ampgj     	    = nan(cur_sen_esti_cnt,1); % nor amplif    gru          --scan--? or calc?
shifj     	    = nan(cur_sen_esti_cnt,1); % nor           gru shift

bligm     	    = nan(cur_sen_esti_cnt,1); % matlab norm  base line gru  --ml norm calculation fn
ampgm     	    = nan(cur_sen_esti_cnt,1); % matlab norm  amplif    gru  --ml norm calculation fn
shifm     	    = nan(cur_sen_esti_cnt,1); % matlab norm            gru shift
proid           = cell(cur_sen_esti_cnt,1);

% ALLOCATE ESTIMATE ARRAYS TO STORE SCAN DATA:

scan_raw_time       = nan(cur_sen_esti_cnt,max_scan_len_esti); %[1536�63 double]}
scan_raw_gru        = nan(cur_sen_esti_cnt,max_scan_len_esti); %[1536�63 double]}
%nouscan_raw_tr_gru     = nan(cur_sen_esti_cnt,max_scan_len_esti); %{[1536�63 double]}  old_way_tr_jas_tbd_sensor_type_2
scan_nrm_gru        = nan(cur_sen_esti_cnt,max_scan_len_esti); %{[1536�63 double]}
scan_nrm_gru_matlab = nan(cur_sen_esti_cnt,max_scan_len_esti); %[1536�63 double]}
%                                                               % rows_db_cnt: {[1536]}
scan_raw_len      	= nan(cur_sen_esti_cnt,1);                  % length of raw data
sensor_type      	= nan(cur_sen_esti_cnt,1);

error(1:end)    = {''}; % BLANK ERROR IS: No error. Empty is not good to calculate UNIQE, for the categories!
% Get the data for real from the API:
cur_clasi      = {'U'};
cur_sen_ndx_use= 0;

for cur_exp_ndx= 1:1:exp_request_cnt % ________________________________________________________________________________________________
    status_msg = sprintf('Retrieving: experiment  %5d  of  %5d ... Please wait .....',cur_exp_ndx,exp_request_cnt);
    set(handles_text_exp_status,'string',status_msg,'ForegroundColor','blue','Fontsize',12); %'blue'
    drawnow;
    %lprintf_log( '\n%s\n',status_msg);
    cur_exper = char(expe_ned_table.exper(cur_exp_ndx));
    cur_instr = char(expe_ned_table.instr(cur_exp_ndx));
    cur_carri = char(expe_ned_table.carri(cur_exp_ndx));
    cur_spotl = char(expe_ned_table.spotl(cur_exp_ndx));
    %cur_kitlo= char(expe_ned_table.expid(cur_exp_ndx));
    cur_proto = char(expe_ned_table.proto(cur_exp_ndx));
    date_str  = char(expe_ned_table.dates(cur_exp_ndx));
    cur_expid = char(expe_ned_table.expid(cur_exp_ndx));
    date_str  = cellstr(date_str);
    date_str  = strrep(date_str,'(UTC)','');
    date_str  = strrep(date_str,' ','');
    cur_datef = datenum(date_str);
    cur_dates = date_str;  % cellstr(int2str(datenum(date_str)));
    cur_proid= char(expe_ned_table.proid(cur_exp_ndx));
    lprintf_log( '\n%s \t\t%-s \t\t%-s \t\t%-s \t\t%-s \n'...
        ,status_msg,char(cur_dates),cur_carri,cur_expid,cur_exper);
    
    [ error_message,protocol_struct ] = sa_rest_protocol_get_details(cur_proid,rest_token); %
    if (~(isempty(error_message)))
        lprintf_log( '\n  skipping experiment due to protocol issue: %s',error_message);
        continue;
    else
        % FOUND DETAILS FOR THIS PROTOCOL: Update all proto names with the same category
        probe_map                                   = protocol_struct.probe_map;
        probe_map_table                             = struct2table(probe_map);
        %probe_well_plate                            = protocol_struct.well_plate;
        % update the protocol names for all exp that have this protocol_id
        cur_baseline_start  = {''};
        if ( strcmp(protocol_struct.parameters(3).name, 'Baseline Interval Start'))
            cur_baseline_start  = {protocol_struct.parameters(3).value};
        end
        cur_baseline_duration  = {''};
        if ( strcmp(protocol_struct.parameters(4).name, 'Baseline Interval Duration'))
            cur_baseline_duration   = {protocol_struct.parameters(4).value};
        end
        cur_amplified_start  = {''};
        if ( strcmp(protocol_struct.parameters(5).name, 'Amplified Interval Start'))
            cur_amplified_start    = {protocol_struct.parameters(5).value};
        end
        cur_amplified_duration  = {''};
        if ( strcmp(protocol_struct.parameters(6).name, 'Amplified Interval Duration'))
            cur_amplified_duration = {protocol_struct.parameters(6).value};
        end
    end

    % S1. Get 1 experiment details:     API_6: showTestMetadata
    [ error_message,exp_detail_struct ] = sa_rest_exp_get_details(cur_expid,rest_token );
    if (~(isempty(error_message)))
        break;
    end
    if (isfield(exp_detail_struct,'consumable'))
        if (~(isempty(exp_detail_struct.consumable)))
            %cur_carri = exp_detail_struct.consumable.carrier_serial_number;
            %cur_spotl = exp_detail_struct.consumable.spotting_lot_number;
            cur_chipl = exp_detail_struct.consumable.lot_number;      %jas_tbd_is this correct? chip or kit lot number? or something else
            if  (isempty(cur_chipl))
                cur_chipl = cur_spotl;  %jas_tbd: to avoid empty chiplots: Use spot lot.
            end
        end
    end
    if (isfield(exp_detail_struct,'specimen_id'))
        if (~(isempty(exp_detail_struct.specimen_id)))
            cur_sampl = exp_detail_struct.specimen_id{1};  % jas_tbd define where to store specimen
            if (size(exp_detail_struct.specimen_id,1))
                % temp1 = exp_detail_struct.specimen_id{2};% jas_tbd define what to do
            end
        end
    end
    
    % S2.1 Get cur experiment sensors:     API_8: showTestSensorMetadata: 136 sensors  input: Experiment GUID
    %     returns: id(1876 to .. ?), number(1 to 136),   channel(1 or 2),  is_valid(0,1),  sensor_errors,    results( value(grue), normalized_value(norm_gru), errors etc.
    [ error_message,exp_sensor_struct ] = sa_rest_exp_get_sensors(char(expe_ned_table.expid(cur_exp_ndx)), rest_token );
    if (~(isempty(error_message)))
        break;
    end
    
    cur_sensor_cnt                              = size(exp_sensor_struct,1);
    [ sensor_table ]                            = probe_get_sensor_names_and_numbers( probe_map_table, cur_sensor_cnt );
    % S2.2 store experiment sensors:
    %max_scan_len_all_exp                        = 0;  % just to tell how long are the scans for all the experiments.
    start_ndx                                   = cur_sen_ndx_use+1;
    
    if (local_hd_save_scan_excel_flag)
        % SCAN_OPTICAL_EXCEL:  user requested saving this exp: All data (even scan_optical to Excel)
        % create blank sensor table to save it into each excel tab: SCAN_OPTICAL first
        % one table per excel_tab: ie per ONE experiment.
        sensor_excel_header                          = { 'exper_id','exper_name', 'carrier_serial_number','sensor_number','sensor_type','channel','probe_name','probe_sensor'};
        sensor_excel_cel                            = cell(cur_sensor_cnt,size(sensor_excel_header,2));
        sensor_excel_table                          = cell2table(sensor_excel_cel);
        sensor_excel_table.Properties.VariableNames = sensor_excel_header;
        [ local_hd_struct,error_message ]           = excel_define_out_sheet(local_hd_struct,expe_ned_table);
        if (~(isempty(error_message)))
            return;
        end
    end % local_hd_save_scan_excel_flag 
    
    for cur_sensor_ndx = 1:1:cur_sensor_cnt % ___________________________________________________________ sensor_loop  jas_test: start with a real sensor: 7
        % STORE EACH SENSOR: If it is not a leak detector     e124 = dserror(124)  isempty(e124{1})
        % sensor_type_key:  _bad_  1 == leak detector, 2 == Temp Detect       3 == Analyte,      4== control)
        % sensor_type_key:  _okk_  1 == Analyte,       2 == leak detector,    3 == Temp Detect   4== control
        cur_sensor_struct             = exp_sensor_struct(       cur_sensor_ndx);
        
        if (local_hd_save_scan_excel_flag)
            % SCAN_OPTICAL_EXCEL
            sensor_excel_table(cur_sensor_ndx,:) = cell2table({ ... % table( cur_expid,cur_exper,cur_sensor_ndx,sensor_table.sensor_type(cur_sensor_ndx), chann(cur_sen_ndx_use), probe(cur_sen_ndx_use), clust(cur_sen_ndx_use));
                {cur_expid}                                     ...
                , {cur_exper}                                   ...
                , {cur_carri}                                   ...   % addition: 2018_04_04 extra col to scan tab: carrier                
                , {cur_sensor_ndx}                              ...
                , {sensor_table.sensor_type(cur_sensor_ndx)}    ...
                , {cur_sensor_struct.channel}                   ... % {chann(cur_sen_ndx_use)}...
                ,  sensor_table.sensor_name(cur_sensor_ndx)     ... % {probe(cur_sen_ndx_use)}...
                , {sensor_table.cluster(    cur_sensor_ndx)}});     % {clust(cur_sen_ndx_use)}});
        end
        
        if ( (sensor_table.sensor_type(cur_sensor_ndx) == 2)  || (sensor_table.sensor_type(cur_sensor_ndx) == 3))  %  %jas_temp_tr_norm: tbd:
            % LD OR TR: jas_temp: for the time being: do not process _tr_jas_tbd_tr_
            % scond_cange to align the data: DO NOT       continue;  % keep 136 and not 128
        end
        cur_sen_ndx_use               = cur_sen_ndx_use + 1;
        probe(cur_sen_ndx_use)        = sensor_table.sensor_name(cur_sensor_ndx);
        clust(cur_sen_ndx_use)        = sensor_table.cluster(    cur_sensor_ndx);
        
        chann(cur_sen_ndx_use)        = cur_sensor_struct.channel;
        if (~(isempty(cur_sensor_struct.sensor_errors)))
            error(cur_sen_ndx_use) 	  = cur_sensor_struct.sensor_errors(1,:); % jas_temp_grab_only first error
        end
        if (~( isempty(cur_sensor_struct.results)))
            %bligr(cur_sen_ndx_use)   = nan; % 2 cuantities to be calculated for raw data
            %ampgr(cur_sen_ndx_use)   = nan;
            %jas_here_fri_02_22_2018: calculate here the bligr for all the sensograms: have the protocol_steps_duration:
            %average out: bl and average out ampl. for raw at least. (clone from norm code).
            % later: will calc for norm (this is a way to slow down user...)
            %jas_here_fri_02_22_2018: sa_rest_db_set_get_all_data and sa_ui_update_tos 
            
            shifr(cur_sen_ndx_use) 	  = cur_sensor_struct.results.value;            % raw           gru shift
            shifj(cur_sen_ndx_use) 	  = cur_sensor_struct.results.normalized_value; % jervis norm   gru shift
            shifm(cur_sen_ndx_use)    = cur_sensor_struct.results.normalized_value; % matlab norm   gru shift jas_temp: fill matlab norm with jervis norm
            outfg(cur_sen_ndx_use)    = cur_sensor_struct.results.is_outlier;
        end
        
        %qc_rpt:
        if (local_hd_save_qc_rpt_excel_flag)
            % QC_RPT_EXCEL: one for all experiments and all 128 sensors. ( Need to trim out the non-analysis sensors before writting to excel
            %cur_exp_ndx
            %cur_sen_ndx_use
            qc_rpt_detail_table.sensor_type(cur_sen_ndx_use)             = {sensor_table.sensor_type(cur_sensor_ndx)}; %jas: if issues: do a continue.
            qc_rpt_detail_table.experiment(cur_sen_ndx_use)              = {cur_exper};
            qc_rpt_detail_table.run_date(cur_sen_ndx_use)                = {date_str{1}(1:10)}; %jas_date printed to day resolution but available to second.
            qc_rpt_detail_table.chip(cur_sen_ndx_use)                    = {'future_runcard'};    %jas_tbd_pull_it from run_card runcard_tbd            
            qc_rpt_detail_table.sample_id(cur_sen_ndx_use)               = {cur_sampl};
            qc_rpt_detail_table.analyte(cur_sen_ndx_use)                 = sensor_table.sensor_name(cur_sensor_ndx); %
            qc_rpt_detail_table.channel(cur_sen_ndx_use)                 = {sensor_table.channel(cur_sensor_ndx)};   % {cur_sensor_struct.channel;
            qc_rpt_detail_table.sensor_cluster_number(cur_sen_ndx_use)   = {sensor_table.cluster(cur_sensor_ndx)};
            qc_rpt_detail_table.sensor_number(cur_sen_ndx_use)           = {cur_sensor_ndx};            
            qc_rpt_detail_table.is_valid(cur_sen_ndx_use)                = {cur_sensor_struct.is_valid};
            qc_rpt_detail_table.is_outlier(cur_sen_ndx_use)              = {outfg(cur_sen_ndx_use)};
            qc_rpt_detail_table.gru_shift_raw(cur_sen_ndx_use)           = {shifr(cur_sen_ndx_use)};  % raw  gru shift
            qc_rpt_detail_table.gru_shift_normalized(cur_sen_ndx_use)    = {shifj(cur_sen_ndx_use)};  % norm gru shift jervis         
            qc_rpt_detail_table.instrument(cur_sen_ndx_use)              = {cur_instr};
            qc_rpt_detail_table.carrier_serial_number(cur_sen_ndx_use)   = {cur_carri};
            qc_rpt_detail_table.spotting_lot_number(cur_sen_ndx_use)     = {cur_spotl};
            qc_rpt_detail_table.protocol(cur_sen_ndx_use)                = {cur_proto};
            qc_rpt_detail_table.tracking_id(cur_sen_ndx_use)             = {'niu'}; % jas_tbd: %jas_qc_3 future use: currently: niu. 
            
            qc_rpt_detail_table.baseline_start(cur_sen_ndx_use)      = {protocol_struct.parameters(3).value};
            qc_rpt_detail_table.baseline_duration(cur_sen_ndx_use)   = {protocol_struct.parameters(4).value};
            qc_rpt_detail_table.amplified_start(cur_sen_ndx_use)     = {protocol_struct.parameters(5).value};
            qc_rpt_detail_table.amplified_duration(cur_sen_ndx_use)  = {protocol_struct.parameters(6).value};
            
            qc_rpt_detail_table.baseline_start(cur_sen_ndx_use)      = cur_baseline_start;
            qc_rpt_detail_table.baseline_duration(cur_sen_ndx_use)   = cur_baseline_duration;
            qc_rpt_detail_table.amplified_start(cur_sen_ndx_use)     = cur_amplified_start;
            qc_rpt_detail_table.amplified_duration(cur_sen_ndx_use)  = cur_amplified_duration;
            
            qc_rpt_detail_table.guid(cur_sen_ndx_use)                    = {cur_expid};  
        end % local_hd_save_qc_rpt_excel_flag: building sensor table for the qc_rpt_excel: all sensors of all experiments.
        
    end % for each sensor ________________________________________________________________________________________________
    
    %Retrieve scan data for all analysis  sensors: i.e. exclude the leak detectors
    % S7. Get 1 experiment SCAN data: 	API_3: showSensor: 136 sensors. (eliminate non_analytes but keep trs)
    %exp_scan_sensors:           sensor_cnt x 3        col_1 number, col_2 is_valid, col_3_scan_cnt
    %scan_col: exp_scan_data:    sensor_cnt x 70 x 2   scan_plane_time = 1  scan_plane_gru = 2    row is sensor and col is scan sequence. assumption: scan_cnt < 70.
    
    % Get scan data from either source: The db (either cpp or Matlab using the API), the Hard Drive (Using previoulsly stored files if available)
    
    
    [ error_message,scan_data_struct] = sa_rest_exp_get_scan(local_hd_struct,cur_expid,cur_proid,exp_sensor_struct,rest_token,sensor_table); % SCAN_OPTICAL_EXCEL:
    if (~(isempty(error_message)))
        continue; % try with other experiments   jas_here_flag_bad_data_experiment
    end
    
    % Check for any garbage at the end: ie zeros etc)  k = find(X,1,'last') % locate the last zero
    % % NIU: Unless I get garbage in the plots: jas_dbg tool.
    % %     last_non_zero_ndx= find(scan_data_struct.scan_data_time,1,'last'); %  finds the last  nonzero elements in the time array.
    % %     if ( (~(isempty(last_non_zero_ndx))) && (last_non_zero_ndx < (length(scan_data_struct.scan_data_time))))
    % %         % FOUND BAD DATA WITH ZEROS AT THE END: Clean it up for the plots: Make it nan
    % %         scan_data_struct.scan_data_time(last_non_zero_ndx:end) = nan;
    % %         scan_data_struct.scan_data_rgru(last_non_zero_ndx:end) = nan;
    % %     end
    
    if (size(scan_data_struct.scan_data_time,2) >  max_scan_len_esti)
        lprintf_log('\nERROR: Scan way too long: %4d   Current Matrices are dimensioned for max scan length of %4d. \nChange configuration parameter max_scan_length to at least %4d ',...
            size(scan_data_struct.scan_data_time,2),max_scan_len_esti,size(scan_data_struct.scan_data_time,2));
    end
    
    if (size(scan_data_struct.scan_data_time,1) >  max_sensor_cnt_esti)
        lprintf_log('\nERROR: Scan way too long: %4d   Current Matrices are dimensioned for max scan length of %4d. \nChange configuration parameter max_sensor_cnt to at least %4d ',...
            size(scan_data_struct.scan_data_time,1),max_sensor_cnt_esti,size(scan_data_struct.scan_data_time,1));
    end
    
    
    % HAVE FETCHED ALL EXPERIMENT DATA: exp, sensor, and scan level. Store it for sensogram analysis
    % S2.3 store experiment details for current experiment
    end_ndx                                                         = cur_sen_ndx_use;
    max_scan_len_cur_exp                                            = scan_data_struct.scan_len_max; % nanmax(exp_scan_sensors(:,3));
    % maybe niu??? max_scan_len_all_exp                                            = nanmax(max_scan_len_all_exp,max_scan_len_cur_exp);
    scan_raw_len(       start_ndx:end_ndx)                          = scan_data_struct.scan_len_max; % exp_scan_sensors(:,3);
    scan_raw_time(      start_ndx:end_ndx,1:max_scan_len_cur_exp)   = scan_data_struct.scan_data_time; %(:,1:max_scan_len_cur_exp,1); % time
    scan_raw_gru(       start_ndx:end_ndx,1:max_scan_len_cur_exp)   = scan_data_struct.scan_data_rgru; %(:,1:max_scan_len_cur_exp,2); % raw gru
    scan_nrm_gru(       start_ndx:end_ndx,1:max_scan_len_cur_exp)   = scan_data_struct.scan_data_rgru; %(:,1:max_scan_len_cur_exp,2); % nor gru fake
    scan_nrm_gru_matlab(start_ndx:end_ndx,1:max_scan_len_cur_exp)   = scan_data_struct.scan_data_rgru; %(:,1:max_scan_len_cur_exp,2); % nor gru fake
    sensor_type(        start_ndx:end_ndx)                          = scan_data_struct.sensor_type; % sensor_type
    
    %probe(start_ndx:end_ndx)      	            = ok
    clasi(start_ndx:end_ndx)      	            = cur_clasi;
    datef(start_ndx:end_ndx)      	            = cur_datef;
    proto(start_ndx:end_ndx)      	            = {cur_proto};
    sampl(start_ndx:end_ndx)                    = {cur_sampl}; % jas_tbreviewed: cases with 2 samples.
    chipl(start_ndx:end_ndx)      	            = {cur_chipl}; % jas_tbreviewed: correct or not?
    exper(start_ndx:end_ndx)      	            = {cur_exper};
    instr(start_ndx:end_ndx)      	            = {cur_instr};
    carri(start_ndx:end_ndx)      	            = {cur_carri};
    %chann(start_ndx:end_ndx)      	            = ok
    %clust(start_ndx:end_ndx)      	            = ok
    datat(start_ndx:end_ndx)      	            = {'raw'};
    %valfg(start_ndx:end_ndx)      	            = ok
    %outfg(start_ndx:end_ndx)      	            = ok
    %error(start_ndx:end_ndx)      	            = ok
    spotl(start_ndx:end_ndx)      	            = {cur_spotl};
    expid(start_ndx:end_ndx)      	            = {cur_expid};  % only for dbg. later it will be removed. jas_temp jas_dbg
    dates(start_ndx:end_ndx)      	            =  cur_dates ;
    proid(start_ndx:end_ndx)      	            = {cur_proid};  % only for dbg. later it will be removed. jas_temp jas_dbg
    
    if (local_hd_save_qc_rpt_excel_flag)
    % SCAN_OPTICAL_EXCEL:  USER REQUESTED TO GENERATE SCAN FILE EXCEL: Write sensor and scan data on latest tab: one experiemnt at a time.
        % jas: to avoid writing each tab of qc data use: (remove comment) continue; % jas_temp
        [ local_hd_struct,error_message ]    = excel_exp_save_scan(local_hd_struct,expe_ned_table,sensor_excel_table,scan_data_struct);
        if (error_message)
            % jas_tbd: decide how to handle write errors: show in status line in red?
            continue;
        end
    end
end % for each expid not already in memory: % _________________________________________________________________________________________

if (local_hd_save_qc_rpt_excel_flag)
    % SCAN_OPTICAL_EXCEL:  USER REQUESTED QC_RPT FILE EXCEL: Write sensor and scan data on latest tab: ALL experiemnts at once.
    [ ~,error_message ] = excel_save_qc_rpt_detail( local_hd_struct,qc_rpt_detail_table );
    if (error_message)
        % jas_tbd: decide how to handle write errors: show in status line in red? local_hd_struct
        return;
    end
end

rows_db_cnt = cur_sen_ndx_use;              % this is the real cnt of sensors loaded from the db. (includes tr).
% % DONE ACCUMULATING ALL DATA FOR ALL NEEDED EXPERIMENTS



% BRAND NEW DATA: Just set it: one row per sensor for all the experiments in this data-set

db_data_set_struct.clasi                = clasi(1:rows_db_cnt);
db_data_set_struct.probe                = probe(1:rows_db_cnt);
db_data_set_struct.datef                = datef(1:rows_db_cnt);
db_data_set_struct.proto                = proto(1:rows_db_cnt);
db_data_set_struct.sampl                = sampl(1:rows_db_cnt);
db_data_set_struct.chipl                = chipl(1:rows_db_cnt);
db_data_set_struct.exper                = exper(1:rows_db_cnt);
db_data_set_struct.instr                = instr(1:rows_db_cnt);
db_data_set_struct.carri                = carri(1:rows_db_cnt);
db_data_set_struct.chann                = chann(1:rows_db_cnt);
db_data_set_struct.clust                = clust(1:rows_db_cnt);
db_data_set_struct.datat                = datat(1:rows_db_cnt);
db_data_set_struct.valfg                = valfg(1:rows_db_cnt);
db_data_set_struct.outfg                = outfg(1:rows_db_cnt);
db_data_set_struct.error                = error(1:rows_db_cnt);
db_data_set_struct.spotl                = spotl(1:rows_db_cnt);

% 4 sets of data:   c==current      r==raw  j==jerv_norm    m=mlab_norm

db_data_set_struct.bligc                = bligr(1:rows_db_cnt); % current is raw: bl,am,and shift for raw, jervis norm and matlab norm:
db_data_set_struct.ampgc                = ampgr(1:rows_db_cnt);
db_data_set_struct.shifc                = shifr(1:rows_db_cnt);
db_data_set_struct.bligr                = bligr(1:rows_db_cnt); % bl,am,and shift for raw, jervis norm and matlab norm:
db_data_set_struct.ampgr                = ampgr(1:rows_db_cnt); % sensor level ampli gru     raw
db_data_set_struct.shifr                = shifr(1:rows_db_cnt); % sensor level shift gru     raw
db_data_set_struct.bligj                = bligj(1:rows_db_cnt); % sensor level bline gru     norm jervis
db_data_set_struct.ampgj                = ampgj(1:rows_db_cnt); % sensor level ampli gru     norm jervis
db_data_set_struct.shifj                = shifj(1:rows_db_cnt); % sensor level shift         norm jervis
db_data_set_struct.bligm                = bligm(1:rows_db_cnt); % same for matlab calculated quantities  ...
db_data_set_struct.ampgm                = ampgm(1:rows_db_cnt);
db_data_set_struct.shifm                = shifm(1:rows_db_cnt);
db_data_set_struct.expid                = expid(1:rows_db_cnt);
db_data_set_struct.proid                = proid(1:rows_db_cnt); % 1D matrix  char: protocol id.


% store scan data to be used for sensogram analysis: raw,normalized,tr etc 2D matrices: The big data.
orndx                                   = 1:1:rows_db_cnt;
orndx                                   = orndx'; % col vector
db_data_set_struct.orndx                = { orndx                                   };
db_data_set_struct.scan_raw_time        = { scan_raw_time(1:rows_db_cnt,:)         	};     % 2D matrix with scan data: time
db_data_set_struct.scan_raw_gru     	= { scan_raw_gru( 1:rows_db_cnt,:)        	};     % 2D matrix with scan data: raw gru
db_data_set_struct.scan_raw_len     	= { scan_raw_len( 1:rows_db_cnt)         	};     % 1D matrix real len of the scan.
db_data_set_struct.scan_raw_tr_gru     	= { scan_raw_len( 1:rows_db_cnt,:)        	};     % to be eliminated because trs are now handled as all analytes: use analisys type column.
db_data_set_struct.scan_nrm_gru     	= { scan_raw_len( 1:rows_db_cnt,:)       	};     % future or maybe never because jervis does not send normalized data
db_data_set_struct.scan_nrm_gru_matlab  = { scan_raw_len( 1:rows_db_cnt,:)       	};     % raw temporarily unit I implement matlab normalization: jas_tbd
db_data_set_struct.rows_db_cnt          = { rows_db_cnt                             };
db_data_set_struct.sensor_type       	= { sensor_type( 1:rows_db_cnt)         	};     % 1D matrix  int: type of the sensor

%jas_here_wed_41_col_ more than that: _normalized_jas

% % jas_tbd the following fields:
%     sc_clasi_flag_struct: [1�1 struct]
%     sc_clasi_vals_struct: [1�1 struct]
%             scan_raw_len: {[1536�62 double]}
%            sc_clasi_flag: [1536�10 double]
%            sc_clasi_vals: [1536�13 double]
%


% Initialize empty lists: to determine metadata

% metadata:
% data_set_sensogram_cnt = db_data_set_struct.rows_db_cnt{1};
% smd_clasi             =  cell(data_set_sensogram_cnt,1); % 1  ; %
% smd_probe             =  cell(data_set_sensogram_cnt,1); % 1  ; %
% smd_dates             =  cell(data_set_sensogram_cnt,1); % 2  ; % run date as a string.
% smd_proto             =  cell(data_set_sensogram_cnt,1); % 3  ; %
% smd_sampl         	=  cell(data_set_sensogram_cnt,1); % 4  ; %
% smd_chipl             =  cell(data_set_sensogram_cnt,1); % 5  ; %
% smd_exper             =  cell(data_set_sensogram_cnt,1); % 6  ; %
% smd_instr             =  cell(data_set_sensogram_cnt,1); % 7  ; %
% smd_carri          	=  cell(data_set_sensogram_cnt,1); % 8  ; %
% smd_chann          	=  cell(data_set_sensogram_cnt,1); % 9  ; %
% smd_clust             =  cell(data_set_sensogram_cnt,1); % 10 ; %
% smd_datat             =  cell(data_set_sensogram_cnt,1); % 11 ; %
% smd_valfg             =  cell(data_set_sensogram_cnt,1); % 13 ; %
% smd_outfg             =  cell(data_set_sensogram_cnt,1); % 14 ; %
% smd_error             =  cell(data_set_sensogram_cnt,1); % 15 ; %
% smd_spotl             =  cell(data_set_sensogram_cnt,1); % 16 ; % to join_with_toe

db_metad_set_struct                 = struct;
db_metad_set_struct.clasi           = categorical(util_transpose_to_col_vector(categorical(clasi(1:rows_db_cnt),'Ordinal',true)));  %jas_temp fake
db_metad_set_struct.clasi           = addcats(db_metad_set_struct.clasi, { 'A','G','U' });
db_metad_set_struct.clasi           = reordercats(db_metad_set_struct.clasi); % to come up with: A G U order for the categories.
db_metad_set_struct.probe           = categorical(db_data_set_struct.probe,'Ordinal',true)    ;
db_metad_set_struct.dates           = categorical(dates(1:rows_db_cnt),'Ordinal',true) ; %   cellstr(int2str(datenum(date_str)));
db_metad_set_struct.proto           = categorical(db_data_set_struct.proto,'Ordinal',false)   ;
db_metad_set_struct.sampl           = categorical(db_data_set_struct.sampl,'Ordinal',true)    ;
db_metad_set_struct.chipl           = categorical(db_data_set_struct.chipl,'Ordinal',true)    ;
db_metad_set_struct.exper           = categorical(db_data_set_struct.exper,'Ordinal',false)   ;
db_metad_set_struct.instr           = categorical(db_data_set_struct.instr,'Ordinal',true)    ;
db_metad_set_struct.carri           = categorical(db_data_set_struct.carri,'Ordinal',true)    ;
db_metad_set_struct.chann           = categorical(db_data_set_struct.chann,'Ordinal',true)    ;
db_metad_set_struct.clust           = categorical(db_data_set_struct.clust,'Ordinal',true)    ;
db_metad_set_struct.datat           = categorical(db_data_set_struct.datat,'Ordinal',false)   ;
db_metad_set_struct.valfg           = categorical(db_data_set_struct.valfg,'Ordinal',false)   ;
db_metad_set_struct.outfg           = categorical(db_data_set_struct.outfg,'Ordinal',false)   ;
db_metad_set_struct.error           = categorical(db_data_set_struct.error,'Ordinal',false)   ;
db_metad_set_struct.spotl           = categorical(db_data_set_struct.spotl,'Ordinal',true)    ;
db_metad_set_struct.expid           = categorical(db_data_set_struct.expid,'Ordinal',true)    ; % to join with toe

db_data_set_struct.sc_clasi_flag 	= zeros(rows_db_cnt,db_data_set_struct.sc_clasi_flag_struct.clasi_flag_col_cnt);  %jas_temp fake rows_db_cnt
db_data_set_struct.sc_clasi_vals	= zeros(rows_db_cnt,db_data_set_struct.sc_clasi_vals_struct.clasi_vals_col_cnt);  %jas_temp fake

% save back. Ready to do sensor classification:
db_data_set_struct.db_metad_set_struct = db_metad_set_struct;

db_metad_set_table = struct2table(db_metad_set_struct);

end % fn sa_rest_db_set_get_all_data

% db_data_set_struct =  struct with fields:   from fake_input_data. This is the target for this function
%
%                    orndx: [1536�1 double]
%                    probe: {1�16 cell}
%      db_metad_set_struct: [1�1 struct]
%                 smd_cols: [1�1 struct]
%                    datef: [1536�1 double]
%                    bligc: [1536�1 double]
%                    ampgc: [1536�1 double]
%                    shifc: [1536�1 double]
%            scan_raw_time: {[1536�63 double]}
%             scan_raw_gru: {[1536�63 double]}
%          scan_raw_tr_gru: {[1536�63 double]}
%             scan_nrm_gru: {[1536�63 double]}
%      scan_nrm_gru_matlab: {[1536�63 double]}
%              rows_db_cnt: {[1536]}
%              cols_db_cnt: NaN
%                    bligr: [1536�1 double]
%                    ampgr: [1536�1 double]
%                    shifr: [1536�1 double]
%                    bligj: [1536�1 double]
%                    ampgj: [1536�1 double]
%                    shifj: [1536�1 double]
%                    bligm: [1536�1 double]
%                    ampgm: [1536�1 double]
%                    shifm: [1536�1 double]
%                    nomfg: 0
%     sc_clasi_flag_struct: [1�1 struct]
%     sc_clasi_vals_struct: [1�1 struct]
%             scan_raw_len: {[1536�62 double]}
%            sc_clasi_flag: [1536�10 double]
%            sc_clasi_vals: [1536�13 double]
%
% %db_metad_set_struct.clasi           = categorical(util_transpose_to_col_vector(categorical(clasi(1:rows_db_cnt),'Ordinal',true)));  %jas_temp fake


%
% rows_db_cnt_cur                     = 0;
% if (iscell(db_data_set_struct.rows_db_cnt)  )
%     rows_db_cnt_cur                 = db_data_set_struct.rows_db_cnt{1};
%     rows_db_cnt_cur                 = 0 ; % FORCE OVERWRITE  jas_temp  jas_dbg
% end
%
% rows_db_cnt_new                     = rows_db_cnt_cur + cur_sen_ndx_use;
% db_data_set_struct.rows_db_cnt      = {rows_db_cnt_new};
% if (rows_db_cnt_cur == 0)


% else
%     % EXISTING DATA IN THE SENSOR FROM THE DB:
%     % promt the user if he/she wants to: (do it before the exp_need loop)
%     % 1 - Overwrite existing data set. (mark it as not saved)
%     % 2 - Append to existing data set.
%     % Temporarily: Append to existing data set (mark it as not saved)
%
%     % jas tbd: something like this:
%     %         db_data_set_struct.probe                = { db_data_set_struct.probe ; probe };
%     %         db_data_set_struct.datef                = { db_data_set_struct.datef ; datef };
%     %         db_data_set_struct.clasi                = { db_data_set_struct.clasi ; clasi };
%
%     db_data_set_struct.clasi                = clasi(1:rows_db_cnt);
%     db_data_set_struct.probe                = probe(1:rows_db_cnt);
%     db_data_set_struct.datef                = datef(1:rows_db_cnt);
%     db_data_set_struct.proto                = proto(1:rows_db_cnt);
%     db_data_set_struct.sampl                = sampl(1:rows_db_cnt);
%     db_data_set_struct.chipl                = chipl(1:rows_db_cnt);
%     db_data_set_struct.exper                = exper(1:rows_db_cnt);
%     db_data_set_struct.instr                = instr(1:rows_db_cnt);
%     db_data_set_struct.carri                = carri(1:rows_db_cnt);
%     db_data_set_struct.chann                = chann(1:rows_db_cnt);
%     db_data_set_struct.clust                = clust(1:rows_db_cnt);
%     db_data_set_struct.datat                = datat(1:rows_db_cnt);
%     db_data_set_struct.valfg                = valfg(1:rows_db_cnt);
%     db_data_set_struct.outfg                = outfg(1:rows_db_cnt);
%     db_data_set_struct.error                = error(1:rows_db_cnt);
%     db_data_set_struct.spotl                = spotl(1:rows_db_cnt);
%
%     db_data_set_struct.bligc                = bligr(1:rows_db_cnt); % current is raw: bl,am,and shift for raw, jervis norm and matlab norm:
%     db_data_set_struct.ampgc                = ampgr(1:rows_db_cnt);
%     db_data_set_struct.shifc                = shifr(1:rows_db_cnt);
%     db_data_set_struct.bligr                = bligr(1:rows_db_cnt); % bl,am,and shift for raw, jervis norm and matlab norm:
%     db_data_set_struct.ampgr                = ampgr(1:rows_db_cnt);
%     db_data_set_struct.shifr                = shifr(1:rows_db_cnt);
%     db_data_set_struct.bligj                = bligj(1:rows_db_cnt);
%     db_data_set_struct.ampgj                = ampgj(1:rows_db_cnt);
%     db_data_set_struct.shifj                = shifj(1:rows_db_cnt);
%     db_data_set_struct.bligm                = bligm(1:rows_db_cnt);
%     db_data_set_struct.ampgm                = ampgm(1:rows_db_cnt);
%     db_data_set_struct.shifm                = shifm(1:rows_db_cnt);
%     db_data_set_struct.expid                = expid(1:rows_db_cnt);
% end
%
%
%
%
%         % jas_here: tuesday eve ..volleyball pickup
%         total_files                 = size(time_raw_matrix,3); % loaded files               = 24
%         total_sensograms_per_file   = size(time_raw_matrix,2); % total_sensograms_per_file  = 64
%         max_len_sensograms_per_file = size(time_raw_matrix,1); % max_len_sensograms_per_file= 63 just the data.
%         data_set_sensogram_cnt      = total_files * total_sensograms_per_file; % (includes trs and references)
%         rows_db_cnt                 = cur_sen_esti_cnt;
%         % 2D matrices: vector of scan data per sensogram (the real data  ...)
%         scan_raw_time               = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
%         scan_raw_gru                = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
%         scan_raw_len                = nan(data_set_sensogram_cnt,1); % length of raw data
%         scan_raw_tr_gru             = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
%         scan_nrm_gru                = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
%         scan_nrm_gru_matlab         = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);

%         % ALLOCATE ESTIMATE ARRAYS TO STORE SCAN DATA
%         max_scan_len_esti  = 70; % tipically 63
%         scan_raw_time       = nan(cur_sen_esti_cnt,max_scan_len_esti); %[1536�63 double]}
%         scan_raw_gru        = nan(cur_sen_esti_cnt,max_scan_len_esti); %[1536�63 double]}
%         scan_raw_tr_gru     = nan(cur_sen_esti_cnt,max_scan_len_esti); %{[1536�63 double]}
%         scan_nrm_gru        = nan(cur_sen_esti_cnt,max_scan_len_esti); %{[1536�63 double]}
%         scan_nrm_gru_matlab = nan(cur_sen_esti_cnt,max_scan_len_esti); %[1536�63 double]}
%         %                  cur_sen_ndx_use                                             % rows_db_cnt: {[1536]}




