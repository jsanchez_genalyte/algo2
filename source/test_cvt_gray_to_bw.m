% CVT_GRAY_TO_BW Test script:
% CVT_GRAY_TO_BW Converts a TIFF gray scale file with sparse rectangles to Binary (Black/White)
% Cleans the image noise while preserving  contents: Rectangle(s) size
% INPUT ARGUMENTS
% 1 - im_tiff_gray_file_path: [ Required string ] Full path to input file: TIFF gray scale
% 2 - im_tiff_bw_file_path:   [ Required string ]Full path to input file: TIFF 
% 3 - min_rect_side_px:       { Optional positive integer ] Minimum rectangle side (default 10 px)
% 4 - min_contrast:           [ Optional positive integer ] Minimum contrast (default 50 px)
% OUTPUT ARGUMENTS: 
% 1 success_fg                Boolean: 1 == success. 
%                                      0 == Errors on arguments or
%                                           Errors during file open/write.
% OUTPUT FILE:               TIFF Binary with noise removed.


% TEST_ 1: Basic test with nice contrast and 1 10x10 rectange;
%clear all
close all 
im_tiff_gray_file_path = 'C:\Users\jsanchez\Documents\algo2\ml_test_algo\cvt_gray_to_bw_test_files\sample_image_gray_1.tif'; % input  file path
im_tiff_bw_file_path   = 'C:\Users\jsanchez\Documents\algo2\ml_test_algo\cvt_gray_to_bw_test_files\sample_image_bw_1.tif';   % output file path
min_rect_side_px = 10;                                                                  % min rect size (optional)
min_contrast     = 50;                                                                  % min contrast  (optional)
[success_fg] = cvt_gray_to_bw( im_tiff_gray_file_path,im_tiff_bw_file_path,min_rect_side_px,min_contrast); 
% TEST_ 2
% more to come to test corner cases: Contrast, higher noise, etc.

