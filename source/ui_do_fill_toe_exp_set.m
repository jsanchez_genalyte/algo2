function [ handles ] = ui_do_fill_toe_exp_set( handles )
%UI_DO_FILL_TOE_EXP_SET fills in the tos NOT: using the current ndx:   db_cur_exp_net_ndx twin: db_cur_fit_net_ndx
%   Returns handles with the whole toe filled in.                      db_data_exp_struct twin: db_data_exp_struct
%   see: toe_create_row                                                ui_cur_exp_net_ndx twin: ui_cur_fit_net_ndx
%                                                                      ui_table_tos             twin: ui_table_toe
% ros_rows = handles.db_data_exp_struct.rows_db_cnt{1};                ojo: no ui_table_exp but ui_table_toe
% tos_col_struct = handles.tos_col_struct;                             db_metad_exp_struct      twin:  db_metad_set_struct

% ui_request_flag                         = handles.ui_state_struct.ui_request_flag;
% setup for the 3 cases:
% senso_start_ndx                         = handles.ui_state_struct.senso_start_ndx;        	% this ndx is relative to whatever is currently selected: db_cur_fit_net_ndx
% senso_curves_perp_cnt                  	= handles.ui_state_struct.senso_curves_perp_cnt;  	%100; % sa_cfg_
% included_indices                        = find(db_cur_fit_net_ndx);
% included_len                            = length(included_indices);

handles.toe_cell(:,handles.toe_col_struct.ndxdb)    = cellstr(handles.db_data_exp_struct.db_metad_exp_struct.ndxdb); %  xxxx__first_of_smd __xxxx
handles.toe_cell(:,handles.toe_col_struct.exper)    = cellstr(handles.db_data_exp_struct.db_metad_exp_struct.exper); %
handles.toe_cell(:,handles.toe_col_struct.instr)	= cellstr(handles.db_data_exp_struct.db_metad_exp_struct.instr); 
                                                       
handles.toe_cell(:,handles.toe_col_struct.carri)	= cellstr(handles.db_data_exp_struct.db_metad_exp_struct.carri); %
handles.toe_cell(:,handles.toe_col_struct.kitlo)	= cellstr(handles.db_data_exp_struct.db_metad_exp_struct.kitlo); %
handles.toe_cell(:,handles.toe_col_struct.proto)	= cellstr(handles.db_data_exp_struct.db_metad_exp_struct.proto); %
handles.toe_cell(:,handles.toe_col_struct.dates)	= cellstr(datestr(str2num(char(handles.db_data_exp_struct.db_metad_exp_struct.dates)),'mm/dd/yyyy')); %  run date as a string.
handles.toe_cell(:,handles.toe_col_struct.state)	= cellstr(handles.db_data_exp_struct.db_metad_exp_struct.state); %
handles.toe_cell(:,handles.toe_col_struct.futur)	= cellstr(handles.db_data_exp_struct.db_metad_exp_struct.futur); %

% if (strcmpi(ui_request_flag,'g_n_a')) % str2num
%     % DOING BOTH:
%     handles.toe_cell(:,handles.toe_col_struct.clasi)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %  xxxx__first_of_smd __xxxx
%     handles.toe_cell(:,handles.toe_col_struct.probe)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.probe(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.dates)	= cellstr(datestr(str2double(char(handles.db_data_exp_struct.db_metad_set_struct.dates(handles.ui_state_struct.ui_cur_fit_net_ndx))),'dd-mmm-yy HH:MM')) ; % run date as a string.
%     handles.toe_cell(:,handles.toe_col_struct.proto)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.proto(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.sampl)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.sampl(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.chipl)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.chipl(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.exper)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.exper(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.instr)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.instr(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.carri)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.carri(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.chann)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.chann(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.clust)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.clust(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.datat)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.datat(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.valfg)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.valfg(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.outfg)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.outfg(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.error)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.error(handles.ui_state_struct.ui_cur_fit_net_ndx)) ; % xxxx__last_of_smd __xxxx
% end
% 
% if (strcmpi(ui_request_flag,'g_only'))
%     % DOING GOOD_ONLY
%     handles.toe_cell(:,handles.toe_col_struct.clasi)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %  xxxx__first_of_smd __xxxx
%     handles.toe_cell(:,handles.toe_col_struct.probe)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.probe(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.dates)	= cellstr(datestr(str2double(char(handles.db_data_exp_struct.db_metad_set_struct.dates(handles.ui_state_struct.ui_cur_fit_net_ndx_good))),'dd-mmm-yy HH:MM')) ; % run date as a string.
%     handles.toe_cell(:,handles.toe_col_struct.proto)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.proto(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.sampl)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.sampl(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.chipl)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.chipl(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.exper)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.exper(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.instr)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.instr(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.carri)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.carri(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.chann)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.chann(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.clust)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.clust(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.datat)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.datat(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.valfg)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.valfg(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.outfg)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.outfg(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.error)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.error(handles.ui_state_struct.ui_cur_fit_net_ndx_good)) ; % xxxx__last_of_smd __xxxx
% end
% 
% if (strcmpi(ui_request_flag,'s_only'))
%     % DOING ANOM ONLY:
%     handles.toe_cell(:,handles.toe_col_struct.clasi)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.clasi(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %  xxxx__first_of_smd __xxxx
%     handles.toe_cell(:,handles.toe_col_struct.probe)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.probe(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.dates)	= cellstr(datestr(str2double(char(handles.db_data_exp_struct.db_metad_set_struct.dates(handles.ui_state_struct.ui_cur_fit_net_ndx_anom))),'dd-mmm-yy HH:MM')) ; % run date as a string.
%     handles.toe_cell(:,handles.toe_col_struct.proto)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.proto(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.sampl)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.sampl(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.chipl)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.chipl(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.exper)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.exper(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.instr)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.instr(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.carri)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.carri(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.chann)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.chann(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.clust)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.clust(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.datat)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.datat(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.valfg)	= cellstr(handles.db_data_exp_struct.db_metad_set_struct.valfg(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.outfg)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.outfg(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; %
%     handles.toe_cell(:,handles.toe_col_struct.error)    = cellstr(handles.db_data_exp_struct.db_metad_set_struct.error(handles.ui_state_struct.ui_cur_fit_net_ndx_anom)) ; % xxxx__last_of_smd __xxxx
% end

end % fn ui_do_fill_toe_exp_set

