CLASSES:
	protocol
		create
		set_probe_map	
		set_well_plate	
		set_recipe
		get_probe_map 								OK	
		get_well_plate
		get_sensor_names_and_numbers_for_probe.m  	OK	
		ver_jervis_cmp_probe_shift.m				OK
		/JSON/get_protocol_structure.m              OK

	experiment
		create          input device_id,specimen_id call before: createTest

		set_scan_data
		set_raw_data
		set_norm_data call before: showTestProbes
		attributes:
			consumable:	
					{
					Identifier for Consumable
					experiment_name:	
					string
					carrier_serial_number:	string
					spotting_lot_number:	string
					kit_name:				string
					part_number:			string
					lot_number:				string
					serial_number:			string
					expiration_date:		string
					}
			device_id:						string (guid) Identifier for Device
			protocol_id:					string (guid) Identifier of the Protocol to be used
			specimen_id:	
					[
					Identifier           	string Identifier of the Specimen or Sample, 
											if only 1 string is passed it is used for both channels. 
					]
			control:   						boolean Indicates if this test should be treated as a control test

'_'
    'EXPERIMENT'
    'DEVICE'
    'CARRIER'
    'SPOTTING LOT'
    'KIT LOT'
    'PROTOCOL'
    'DATE'
    'STATE'
    ''
	
expe_list(5).consumable.experiment_name, 
expe_list(5).consumable.carrier_serial_number, 
expe_list(5).consumable.spotting_lot_number, 
expe_list(5).consumable.lot_number
 


handles =   struct with fields:
              figure_sens_Anal_app_10: [1×1 Figure]
                  pushbutton_top_algo: [1×1 UIControl]
                  pushbutton_top_sens: [1×1 UIControl]
                  pushbutton_top_expe: [1×1 UIControl]
                        ui_panel_expe: [1×1 Panel]
                        ui_panel_sens: [1×1 Panel]
                           uitoolbar1: [1×1 Toolbar]
                            help_menu: [1×1 Menu]
                      thresholds_menu: [1×1 Menu]
                        analysis_menu: [1×1 Menu]
                            view_menu: [1×1 Menu]
                            edit_menu: [1×1 Menu]
                            file_menu: [1×1 Menu]
                  pushbutton_req_date: [1×1 UIControl]
                        edit_req_date: [1×1 UIControl]
                  pushbutton_req_prot: [1×1 UIControl]
                        edit_req_prot: [1×1 UIControl]
                  pushbutton_req_expe: [1×1 UIControl]
                        edit_req_expe: [1×1 UIControl]
                  pushbutton_req_kitl: [1×1 UIControl]
                        edit_req_kitl: [1×1 UIControl]
                  pushbutton_req_spot: [1×1 UIControl]
                        edit_req_spot: [1×1 UIControl]
                  pushbutton_req_carr: [1×1 UIControl]
                        edit_req_carr: [1×1 UIControl]
                               text39: [1×1 UIControl]
                         ui_table_exp: [1×1 Table]
                   uipanel_anom_senso: [1×1 Panel]
                            uipanel13: [1×1 Panel]
                          uipanel_tos: [1×1 Panel]
                 uipanel_data_filters: [1×1 Panel]
                    uipanel_good_sens: [1×1 Panel]
                             uipanel5: [1×1 Panel]
                             uipanel7: [1×1 Panel]
                             uipanel3: [1×1 Panel]
                            uipanel15: [1×1 Panel]
      ui_toggle_tool_auto_manual_axis: [1×1 ToggleTool]
                ui_toggle_tool_legend: [1×1 ToggleTool]
           ui_toggle_tool_data_cursor: [1×1 ToggleTool]
                   ui_toggle_tool_pan: [1×1 ToggleTool]
              ui_toggle_tool_zoom_out: [1×1 ToggleTool]
               ui_toggle_tool_zoom_in: [1×1 ToggleTool]
      ui_push_tool_save_db_local_file: [1×1 PushTool]
      ui_push_tool_open_db_local_file: [1×1 PushTool]
       ui_push_tool_new_db_local_file: [1×1 PushTool]
                  help_help_menu_item: [1×1 Menu]
              help_about_sc_menu_item: [1×1 Menu]
                threshold_3_menu_item: [1×1 Menu]
                threshold_2_menu_item: [1×1 Menu]
                threshold_1_menu_item: [1×1 Menu]
      analysis_classi_senso_menu_item: [1×1 Menu]
       analysis_norm_matlab_menu_item: [1×1 Menu]
       analysis_norm_jervis_menu_item: [1×1 Menu]
    view_plot_ampli_circles_menu_item: [1×1 Menu]
    view_plot_bline_circles_menu_item: [1×1 Menu]
             view_plot_grid_menu_item: [1×1 Menu]
         view_plot_meas_reg_menu_item: [1×1 Menu]
          view_plot_tr_anom_menu_item: [1×1 Menu]
          view_plot_tr_good_menu_item: [1×1 Menu]
     view_manual_scale_anom_menu_item: [1×1 Menu]
       view_auto_scale_anom_menu_item: [1×1 Menu]
     view_manual_scale_good_menu_item: [1×1 Menu]
       view_auto_scale_good_menu_item: [1×1 Menu]
           edit_copy_window_menu_item: [1×1 Menu]
              edit_copy_sel_menu_item: [1×1 Menu]
               edit_cut_sel_menu_item: [1×1 Menu]
               edit_sel_inv_menu_item: [1×1 Menu]
            edit_select_all_menu_item: [1×1 Menu]
                  file_exit_menu_item: [1×1 Menu]
      file_append_sel_local_menu_item: [1×1 Menu]
         file_save_db_local_menu_item: [1×1 Menu]
            file_open_local_menu_item: [1×1 Menu]
             file_new_local_menu_item: [1×1 Menu]
                   checkbox_hide_anom: [1×1 UIControl]
               checkbox_anom_sens_mea: [1×1 UIControl]
               checkbox_anom_sens_med: [1×1 UIControl]
                  edit_senso_per_plot: [1×1 UIControl]
                pushbutton_senso_next: [1×1 UIControl]
                     checkbox_combine: [1×1 UIControl]
                  text_senso_per_plot: [1×1 UIControl]
                         ui_table_tos: [1×1 Table]
                          text_status: [1×1 UIControl]
                               text26: [1×1 UIControl]
                           text_proto: [1×1 UIControl]
                           text_chipl: [1×1 UIControl]
                           text_instr: [1×1 UIControl]
                           text_exper: [1×1 UIControl]
                           text_carri: [1×1 UIControl]
                           text_datat: [1×1 UIControl]
                           text_chann: [1×1 UIControl]
                           text_ampli: [1×1 UIControl]
                           text_clust: [1×1 UIControl]
                      popupmenu_chipl: [1×1 UIControl]
                      popupmenu_exper: [1×1 UIControl]
                      popupmenu_instr: [1×1 UIControl]
                      popupmenu_carri: [1×1 UIControl]
                      popupmenu_chann: [1×1 UIControl]
                      popupmenu_clust: [1×1 UIControl]
                      popupmenu_datat: [1×1 UIControl]
                      popupmenu_ampli: [1×1 UIControl]
                           text_probe: [1×1 UIControl]
                           text_dates: [1×1 UIControl]
                           text_sampl: [1×1 UIControl]
                      popupmenu_dates: [1×1 UIControl]
                      popupmenu_proto: [1×1 UIControl]
                      popupmenu_sampl: [1×1 UIControl]
                      popupmenu_probe: [1×1 UIControl]
                       axes_good_sens: [1×1 Axes]
                       axes_anom_sens: [1×1 Axes]
                       radiobutton_db: [1×1 UIControl]
                  pushbutton_get_data: [1×1 UIControl]
               radiobutton_local_file: [1×1 UIControl]
                           popupmenu9: [1×1 UIControl]
                          popupmenu10: [1×1 UIControl]
                          popupmenu11: [1×1 UIControl]
                               text15: [1×1 UIControl]
                               text14: [1×1 UIControl]
                               text13: [1×1 UIControl]
                               text12: [1×1 UIControl]
                               text25: [1×1 UIControl]
                               text31: [1×1 UIControl]
                                edit4: [1×1 UIControl]
                          popupmenu13: [1×1 UIControl]
                               text32: [1×1 UIControl]
                          popupmenu14: [1×1 UIControl]
                        radiobutton11: [1×1 UIControl]
                       radiobutton_10: [1×1 UIControl]
                          axes_pareto: [1×1 Axes]
               checkbox_good_sens_mea: [1×1 UIControl]
               checkbox_good_sens_med: [1×1 UIControl]
               checkbox_good_sens_env: [1×1 UIControl]
                   checkbox_hide_good: [1×1 UIControl]
                      ui_state_struct: [1×1 struct]
              input_parameters_struct: [1×1 struct]
                   db_data_set_struct: [1×1 struct]
                               output: [1×1 Figure]
                       tos_col_struct: [1×1 struct]
                             tos_cell: {1536×29 cell}

K>> 