function [ handles ] = sa_do_save_session( handles,a_matlab_file_name )
%SA_DO_SAVE_SESSION saves everything associated with a session.
%   Saves the following: input_args, analyte_struct and table_of_fits.
% Check that the file does not exist: If it does: Prompt the user for overwrite:
%
%sa_declare_std_curve_globals;

% This DOES     work: Save one variable at a time: but for the time being:  save the whole thing
if ( (length(a_matlab_file_name) >4) && (strcmpi(a_matlab_file_name(end-3:end),'.mat')) )
    a_matlab_file_name_display = a_matlab_file_name(1:end-4);
else
    a_matlab_file_name_display = a_matlab_file_name;
end
% Save your own name,figure name, cwd etc
set(handles.figure_sens_Anal_app_10,'name',sprintf('SENSOGRAM ANALYSIS   SESSION:     %s   ',a_matlab_file_name_display));
handles.ui_state_struct.cur_session_name    = { a_matlab_file_name };
cur_dir                                     = pwd;
handles.ui_state_struct.local_input_dir     = { cur_dir };
save(a_matlab_file_name          ); %, 'handles'                  **** __SAVE__   __save__  ****
% save(a_matlab_file_name,          'env_struct_list');                          % 1 save environment  '-struct',
% save(a_matlab_file_name,         'analyte_struct_list'            ,'-append'); % 2 save real data models loaded from excel
% save(a_matlab_file_name,         'table_of_fits'                  ,'-append'); % 3 save TOF spare_row_status
% jas_tbd: Figure out how to get the output of the save function.
% display(return_status);

end % fn sa_do_save_session   save('jas_session_handles.m', 'handles');