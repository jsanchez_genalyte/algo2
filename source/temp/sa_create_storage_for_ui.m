function [ ui_state_struct ] = sa_create_storage_for_ui( handles,input_parameters_struct )
 %SA_CREATE_STORAGE_FOR_UI  creates storage for all ui elements using as input the input_parameters_struct
 %  these are used as default to start the app and as the list of available items to select from before requesting data to the db.
 %  whatever is wanted and not in this struct, the user will need to request it as custom (each time the app starts from zero (before loading a file)
 %  see: sa_cfg file
 %  create struct to return

%  each structure has:
%  1 - list_str_cell:      the names to show on the ui (includes 'All' etc.
%  2 - list_ndx_filt_net:  direct and filter: i.e. net (to be used for analysis and display)
%  3 - list_ndx_filt_dir:  direct filter from end user 0/1
%  4 - list_ndx_filt_ind:  indirect filter from end user by filtering other categories. 0/1
%  5 - def_ndx: the ndx of whatever is set at start up (to overwrite it: edit the sa_cfg file) shown in ui.
%  6 - cur_ndx: the ndx of whatever is selected in the ui: set by end-user (or set at start up by default)

%text_probe 
new_probes_cell         =  input_parameters_struct.probe_valid;
def_ndx                 = 1; % default is 'All' probes
ui_probe_state_struct   = sa_ui_create_popup_struc( new_probes_cell,def_ndx );
set(handles.popupmenu_probe,'String',new_probes_cell,'Value',def_ndx);
% set all and the rest to: checked.  

%text_dates 
new_dates_cell         =  input_parameters_struct.run_dates_valid;
def_ndx                 = 1; % default is 'last 7 days' 
ui_dates_state_struct   = sa_ui_create_popup_struc( new_dates_cell,def_ndx );
set(handles.popupmenu_dates,'String',new_dates_cell,'Value',def_ndx);

%text_proto 
new_proto_cell         =  input_parameters_struct.proto_valid;
def_ndx                 = 1; % default is 'last 7 days' 
ui_proto_state_struct   = sa_ui_create_popup_struc( new_proto_cell,def_ndx );
set(handles.popupmenu_proto,'String',new_proto_cell,'Value',def_ndx);

%text_sampl 
new_sampl_cell         =  input_parameters_struct.sampl_valid;
def_ndx                 = 1; % default is 'last 7 days' 
ui_sampl_state_struct   = sa_ui_create_popup_struc( new_sampl_cell,def_ndx );
set(handles.popupmenu_sampl,'String',new_sampl_cell,'Value',def_ndx);

%text_chipl 
new_chipl_cell         =  input_parameters_struct.chipl_valid;
def_ndx                 = 1; % default is 'xxxxx' % jas_tbd
ui_chipl_state_struct   = sa_ui_create_popup_struc( new_chipl_cell,def_ndx );
set(handles.popupmenu_chipl,'String',new_chipl_cell,'Value',def_ndx);

%text_exper 
new_exper_cell         =  input_parameters_struct.exper_valid;
def_ndx                 = 1; % default is 'xxxxx' % jas_tbd
ui_exper_state_struct   = sa_ui_create_popup_struc( new_exper_cell,def_ndx );
set(handles.popupmenu_exper,'String',new_exper_cell,'Value',def_ndx);

%text_instr 
new_instr_cell         =  input_parameters_struct.instr_valid;
def_ndx                 = 1; % default is 'xxxxx' % jas_tbd
ui_instr_state_struct   = sa_ui_create_popup_struc( new_instr_cell,def_ndx );
set(handles.popupmenu_exper,'String',new_instr_cell,'Value',def_ndx);

%text_carri 
new_carri_cell         =  input_parameters_struct.carri_valid;
def_ndx                 = 1; % default is 'xxxxx' % jas_tbd
ui_carri_state_struct   = sa_ui_create_popup_struc( new_carri_cell,def_ndx );
set(handles.popupmenu_carri,'String',new_carri_cell,'Value',def_ndx);

%text_chann 
new_chann_cell         =  input_parameters_struct.chann_valid;

def_ndx                 = 1; % default is 'xxxxx' % jas_tbd
ui_chann_state_struct   = sa_ui_create_popup_struc( new_chann_cell,def_ndx );
set(handles.popupmenu_chann,'String',new_chann_cell,'Value',def_ndx);


%text_clust
new_clust_cell         =  input_parameters_struct.clust_valid;
def_ndx                 = 1; % default is all 4 sensors of the cluster.
ui_clust_state_struct   = sa_ui_create_popup_struc( new_clust_cell,def_ndx );
set(handles.popupmenu_clust,'String',new_clust_cell,'Value',def_ndx);

%text_datat
new_datat_cell         =  input_parameters_struct.datat_valid;
def_ndx                 = 1; % default is raw.
ui_datat_state_struct   = sa_ui_create_popup_struc( new_datat_cell,def_ndx );
        % datat is special case: overwrite it
        ui_datat_state_struct.list_str_cel_dat      = { 'Raw','Normalized By Jervis','Normalized by Matlab ...'};
        ui_datat_state_struct.list_str_cel_cur      = { 'Raw','Normalized By Jervis','Normalized by Matlab ...'};
        ui_datat_state_struct.list_ndx_filt_dat     = [ 1 0 0 ];      
        ui_datat_state_struct.dat_ndx               = 1;
        ui_datat_state_struct.cur_ndx               = 1;   
        ui_datat_state_struct.def_ndx               = 1;  
set(handles.popupmenu_clust,'String',new_datat_cell,'Value',ui_datat_state_struct.def_ndx);

%text_datat
new_ampli_cell         =  input_parameters_struct.ampli_valid;
def_ndx                 = 1; % default is all 4 sensors of the cluster.
ui_ampli_state_struct   = sa_ui_create_popup_struc( new_ampli_cell,def_ndx );
set(handles.popupmenu_clust,'String',new_ampli_cell,'Value',def_ndx);

% set all and the rest to: checked.  

ui_state_struct = struct(...
     'probe',ui_probe_state_struct ...
    ,'dates',ui_dates_state_struct ...
    ,'proto',ui_proto_state_struct ...
    ,'sampl',ui_sampl_state_struct ...  
    ,'chipl',ui_chipl_state_struct ...    
    ,'exper',ui_exper_state_struct ...
    ,'instr',ui_instr_state_struct ...    
    ,'carri',ui_carri_state_struct ...
    ,'chann',ui_chann_state_struct ...    
    ,'clust',ui_clust_state_struct ...
    ,'datat',ui_datat_state_struct ... 
    ,'ampli',ui_ampli_state_struct ...    
    ,'db_cur_fit_net_ndx',nan      ...      
    );
end % fn 