
%
% Generate HTML View of MATLAB Script
% Generate an HTML view of the code, MATLAB results, and comments in a MATLAB script.

% Copy the example file, sa_do_plot_ana_lot_2_lot_url.m, to your current folder.

% copyfile(fullfile(matlabroot,'help','techdoc',...
% 'matlab_env','examples','sa_do_plot_ana_lot_2_lot_url.m'),'.','f')
% Generate an HTML view of the MATLAB file.

 publish('sa_do_plot_ana_lot_2_lot_url.m'...
     , 'figureSnapMethod', 'entireFigureWindow'...
     ,'useNewFigure',false...
     ,'showCode' ,false ...
);
% The publish command executes the code for each cell in sa_do_plot_ana_lot_2_lot_url.m, and saves the file to /html/sa_do_plot_ana_lot_2_lot_url.html.

% View the HTML file.

web('html/sa_do_plot_ana_lot_2_lot_url.html')