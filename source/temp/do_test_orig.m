function [ data_box_plot,data_odd_plot,data_even_plot ] = do_test( assay_sum_table,assay_sum_ana_mat )
%gen_ Summary of this function goes here
%   sample call:
% [ data_box_plot,data_odd_plot,data_even_plot ] = do_test( assay_sum_table,assay_sum_ana_mat );
data_offset = 9; % data starts in col 10
spotl_cat = categorical(assay_sum_table.spotl);
spotl_cat_u = unique(categorical(assay_sum_table.spotl));
spotl_cnt   = length(spotl_cat_u);

odd_ch_ndx  = 1:2:size(assay_sum_ana_mat,1);
even_ch_ndx = 2:2:size(assay_sum_ana_mat,1);

% Produce indices to travers odd and even data.
ndx_all                 = false(size(assay_sum_ana_mat,1),1);
ndx_odd                 = ndx_all;
ndx_even                = ndx_all;
ndx_odd(odd_ch_ndx)     = 1;
ndx_even(even_ch_ndx)   = 1;

% build index of lot_numbers
spot_mat_ndx = nan(height(assay_sum_table),spotl_cnt);

for cur_lot_ndx = 1:1:spotl_cnt
    cur_lot     = spotl_cat_u(cur_lot_ndx);
    spot_mat_ndx(:,cur_lot_ndx)= spotl_cat ==  cur_lot;
    spot_cnt_list =   sum(spot_mat_ndx);
end

% fill in the matrix: data_box_plot with data arranged this way:
%                     each cil in one lot. 


% jas: all to be reviewed tomorrow.
largest_lot_data_len = max(sum(spot_mat_ndx));

figure('Name','  Lot to lot variability  ( Cluster mean )'); 
fig_qc_lot_2_lot = gcf;
for cur_ana_ndx=1:1:16 % 6
    % PRODUCE PLOTS FOR CURRENT ANALYTE:  both    
    data_box_plot  	= nan(largest_lot_data_len,spotl_cnt);
    data_odd_plot  	= nan(largest_lot_data_len,spotl_cnt);    
    data_even_plot  = nan(largest_lot_data_len,spotl_cnt);      
    for cur_lot_ndx = 1:1:spotl_cnt
        ndx_cur     = logical(spot_mat_ndx(:,cur_lot_ndx));   % spot_mat_ndx(1:spot_cnt_list(cur_lot_ndx))';
        ndx_cur_odd = ndx_cur;
        ndx_cur_odd(ndx_even) =0; % for the odd: reset the even
        
        ndx_cur_even= ndx_cur;
        ndx_cur_even(ndx_odd) =0; % for the even: reset the odd
        temp_rows = 1:spot_cnt_list(cur_lot_ndx);
        temp_rows = temp_rows';
        temp_data = assay_sum_ana_mat(:,cur_ana_ndx);
        temp_ndx  = spot_mat_ndx(:,     cur_lot_ndx);
        
        temp_all_ndx                = logical(temp_ndx);    
        temp_all_ndx (~ndx_cur)     =0;  % exclude outisders.         
        data_box_plot( temp_rows,                     cur_lot_ndx) = temp_data(logical(temp_all_ndx));
        
        temp_odd_ndx                = temp_ndx;          
        temp_odd_ndx(~ndx_cur_odd)  =0;  % exclude outisders.         
        data_odd_plot( 1:sum(ndx_cur_odd),           cur_lot_ndx) = temp_data(logical(temp_odd_ndx));
        
        temp_even_ndx                = temp_ndx;         
        temp_even_ndx(~ndx_cur_even)  =0;  % exclude outisders.            
        data_even_plot(1:sum(ndx_cur_even),           cur_lot_ndx) = temp_data(logical(temp_even_ndx));
    end
    dark_green_color = [0.1  0.5  0.2];  % nice dark green.
    dark_blue_color  = [0     0    1];    % nice dark blue
    %data_ana = assay_sum_ana_mat(cur_ana_ndx+data_offset);
    name_cur_ana = strrep(assay_sum_table.Properties.VariableNames{cur_ana_ndx+data_offset},'_','-') ;
    % produce box plots for all lots and given analyte
    subplot(4,4,cur_ana_ndx)
    hold all;
    boxplot(data_box_plot,'notch','on',...
        'labels',{ char(spotl_cat_u) } ,'ExtremeMode','compress','Whisker',1.5,'LabelOrientation','inline');  % assay_ana_h_pret(1:2))    
    axes_qc_lot_2_lot = gca;
    set(axes_qc_lot_2_lot,'FontSize',9);
    title(axes_qc_lot_2_lot, strcat(strrep(name_cur_ana,'_','-'),'   Positive Cluster Mean') );
    ylabel(axes_qc_lot_2_lot, strcat(strrep(name_cur_ana,'_','-'),' - GRU') );
    % draw the individual dots: oddd and even
    hold all;
    % PLOT INDIVIDUAL DOTS
    x_axis_qc_plot  = get(axes_qc_lot_2_lot,'XAxis');
    x_limits_qc_plot= x_axis_qc_plot.Limits;
    x_min           = x_limits_qc_plot(1);
    x_max           = x_limits_qc_plot(2);
    x_range         = x_max - x_min;
    x_inc           = x_range / ((2*spotl_cnt));
    cur_start_x     = x_min + (x_inc/2);
    data_odd_plot_x = zeros(size(data_odd_plot,1), spotl_cnt);
    data_even_plot_x= zeros(size(data_even_plot,1),spotl_cnt);
    for cur_lot_ndx = 1:1:spotl_cnt
        data_odd_plot_x( :,cur_lot_ndx)     = cur_start_x - (x_inc/3);
        data_even_plot_x(:,cur_lot_ndx)     = cur_start_x; % + (x_inc/3);
        data_odd_plot_x(1:end,  cur_lot_ndx)= cur_start_x + x_inc;
        cur_start_x                         = cur_start_x  + (2*x_inc);
        
        scatter(axes_qc_lot_2_lot,data_odd_plot_x(:,cur_lot_ndx),data_odd_plot(:,cur_lot_ndx),'*g','LineWidth',0.2); % ,'MarkerFaceColor',dark_green_color);
        scatter(axes_qc_lot_2_lot,data_even_plot_x(:,cur_lot_ndx),data_even_plot(:,cur_lot_ndx),'*b','LineWidth',0.2); % ,'MarkerFaceColor',dark_blue_color);
    end  
    set(axes_qc_lot_2_lot,'XGrid','on');
    set(axes_qc_lot_2_lot,'YGrid','off');
    grid on;
    hold off;
end % for each analyte
set(fig_qc_lot_2_lot,'Units','normalized')
%fig_qc_lot_2_lot.Position %  [left bottom width height]
fig_qc_lot_2_lot.Position =   [ 0.1   0.03   0.9    0.9];

end % fn do_test

