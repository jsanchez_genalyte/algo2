function [ qc_lot_2_lot_struct,error_message ] = sa_qc_calc_lot_2_lot_old( assay_sum_table )
%SA_QC_CALC_LOT_2_LOT  calculates the qc_lot_2_lot_struct for the auto qc report.
% obsolete: this was the first implementation: before implementing: aggreate_column_for_table
%   outputs:  (note: here lot refers to spotting lot: ie spotl)
%   1. the data to produce the qc_lot_to_lot plot for each analyte. Array with 16 entries:
%      1 to 16: each analyte. 
%          data_aggr_mat     aggregated (by spotl) data matrix with odd and even sensors
%          data_aggr_o_mat   same but only odd sensors
%          data_aggr_e_mat   same but only even 
%          name_ana          name of analyte
%          col_values_cat_u  unique list of spotl for the given analyte
%          data_sum_stats    statistical summary for the given analyte (by spotl)
%   2. error message:        blank means no errrors.
%   sample call:
%  [ qc_lot_2_lot_struct,error_message ] = sa_qc_calc_lot_2_lot( assay_sum_table,assay_sum_ana_mat );
% pipeline:   sa_do_push_top_sens.m --> sa_qc_calc_lot_2_lot.m -->

qc_lot_2_lot_struct    = struct;
error_message           = '';
data_offset             = 9; % data starts in col 10
spotl_cat               = categorical(assay_sum_table.spotl);
spotl_cat_u             = unique(categorical(assay_sum_table.spotl));
spotl_cnt               = length(spotl_cat_u);
odd_ch_ndx              = 1:2:size(assay_sum_ana_mat,1);
even_ch_ndx             = 2:2:size(assay_sum_ana_mat,1);

% Produce indices to traverse odd and even data.
ndx_all                 = false(size(assay_sum_ana_mat,1),1);
ndx_odd                 = ndx_all;
ndx_even                = ndx_all;
ndx_odd(odd_ch_ndx)     = 1;
ndx_even(even_ch_ndx)   = 1;

% build index of lot_numbers
spot_mat_ndx            = nan(height(assay_sum_table),spotl_cnt);
for cur_lot_ndx = 1:1:spotl_cnt
    cur_lot                     = spotl_cat_u(cur_lot_ndx);
    spot_mat_ndx(:,cur_lot_ndx) = spotl_cat ==  cur_lot;
    spot_cnt_list               = sum(spot_mat_ndx);
end

% fill in the matrix: data_box_plot with data arranged this way:
%                     each column:  one lot.
largest_lot_data_len = max(sum(spot_mat_ndx));

for cur_ana_ndx=1:1:16 % 6
    % CALCULATE DATA FOR LOT_2_LOT PLOT FOR CURRENT ANALYTE:
    data_box_plot  	= nan(largest_lot_data_len,spotl_cnt);
    data_odd_plot  	= nan(largest_lot_data_len,spotl_cnt);
    data_even_plot  = nan(largest_lot_data_len,spotl_cnt);
    data_sum_stats  = nan(4,spotl_cnt);
    for cur_lot_ndx = 1:1:spotl_cnt
        ndx_cur                     = logical(spot_mat_ndx(:,cur_lot_ndx));   % spot_mat_ndx(1:spot_cnt_list(cur_lot_ndx))';
        ndx_cur_odd                 = ndx_cur;
        ndx_cur_odd(ndx_even)       = 0; % for the odd: reset the even
        ndx_cur_even                = ndx_cur;
        ndx_cur_even(ndx_odd)       = 0; % for the even: reset the odd
        temp_rows                   = 1:spot_cnt_list(cur_lot_ndx);
        temp_rows                   = temp_rows';
        temp_data                   = assay_sum_ana_mat(:,cur_ana_ndx);
        temp_ndx                    = spot_mat_ndx(:,     cur_lot_ndx);
        temp_all_ndx                = logical(temp_ndx);
        temp_all_ndx (~ndx_cur)     = 0;  % exclude outisders.
        data_box_plot( temp_rows,           cur_lot_ndx) = temp_data(logical(temp_all_ndx));
        temp_odd_ndx                = temp_ndx;
        temp_odd_ndx(~ndx_cur_odd)  = 0;  % exclude outisders.
        data_odd_plot( 1:sum(ndx_cur_odd),  cur_lot_ndx) = temp_data(logical(temp_odd_ndx));
        temp_even_ndx               = temp_ndx;
        temp_even_ndx(~ndx_cur_even)= 0;  % exclude outisders.
        data_even_plot(1:sum(ndx_cur_even), cur_lot_ndx)= temp_data(logical(temp_even_ndx));
        % store results:
        qc_lot_2_lot_struct(cur_ana_ndx).data_box_plot  = data_box_plot;
        qc_lot_2_lot_struct(cur_ana_ndx).data_odd_plot  = data_odd_plot;     % scatter odd
        qc_lot_2_lot_struct(cur_ana_ndx).data_even_plot = data_even_plot;    % scatter even
        qc_lot_2_lot_struct(cur_ana_ndx).name_ana       = strrep(assay_sum_table.Properties.VariableNames{cur_ana_ndx+data_offset},'_','-') ;
        qc_lot_2_lot_struct(cur_ana_ndx).spotl_cat_u    = spotl_cat_u;
        
        % calculate summary stats for current analyte: data_sum_stats  = nan(4,spotl_cnt);
        data_sum_stats(1,cur_lot_ndx) 	= sum(isfinite((data_box_plot(temp_rows,cur_lot_ndx))));                % N
        data_sum_stats(2,cur_lot_ndx)  	= nanmean(data_box_plot(temp_rows,cur_lot_ndx));                        % mean
        data_sum_stats(3,cur_lot_ndx)   = nanstd(data_box_plot(temp_rows,cur_lot_ndx));                         % std  
        data_sum_stats(4,cur_lot_ndx)   = 100 * data_sum_stats(3,cur_lot_ndx) /data_sum_stats(2,cur_lot_ndx);   % CV
        qc_lot_2_lot_struct(cur_ana_ndx).data_sum_stats =data_sum_stats';
    end
end % for each analyte: calc data to plot and

end % fn sa_qc_calc_lot_2_lot 

