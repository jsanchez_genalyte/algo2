function [ handles ] = sa_do_edit_req_date( hObject, ~, handles )   % eventdata
%sa_do_edit_req_date   JAS_TBD: get rid of all the calls to: parse_date_keyword_1 _2 and _3 
%                      JAS_TBD (not needed anymore and remove these files)
%   Parses the date edit request string, if valid: stores start and end date.
%   sample:  from 11/21/2017  to  11/22/2017
% Hints: get(hObject,'String') returns contents of edit_req_spot as text
%        str2double(get(hObject,'String')) returns contents of edit_req_spot as a double

% RULES: VALID STRINGS: ( 7 conditions)
% 1 token:  It must  be:  a    date    --> start and end date will get this date: Interpretation: one day of running.
% 2 tokens: It could be:  2    dates   --> start and end date will get these date: Interpretation: specific range
%           It could be:  TO   date    --> end date will get this date:   Interpretation: open range from the early left range (start_date: will be blank)
%           It could be:  FROM date    --> start date will get this date: Interpretation: open range from the late range range (end_date:   will be blank)
% 3 tokens: It could be:  FROM date date-> start and end date will get these date: Interpretation: specific range
%           It could be:  date TO   date-> start and end date will get these date:  Interpretation: specific range
%           It could be:  date date xxx--> start and end date will get these date:  Interpretation: specific range

% 4 tokens: It must  be:  date FROM date-> start and end date will get these date: Interpretation: specific range

% jas_temp: from dcrc_gen_xml_files   to be done: figure out where to stick this 
% Parse dates to accomodate for API - jervis day resolution


%jas blind_shot
edit_req_date_str = get(hObject,'String');

 

[ req_date_struct,error_message  ] = parse_date_request( edit_req_date_str); % 'FROM 01/25/2018 03:00:10 PM TO 01/25/2018 06:17:12 PM')
if (~(isempty(error_message)))
    % COULD NOT PARSE DATE REQUEST: Get out
    return;
end


keywords_1 = { 'TODAY', 'YESTERDAY' };  % TODAY == 'LAST DAY'   'YESTERDAY' =='LAST 2 DAYS' (partial days)
keywords_2 = { 'LAST HOUR', 'THIS WEEK', 'LAST WEEK', 'THIS MONTH', 'LAST MONTH',  'THIS YEAR' ,  'LAST YEAR' };
keywords_3=  { 'LAST 2 DAYS','LAST 3 DAYS','LAST 4 DAYS','LAST 5 DAYS','LAST 6 DAYS','LAST 7 DAYS','LAST 30 DAYS','LAST 1 HOUR' ,'LAST 2 HOURS','LAST 3 HOURS','LAST 4 HOURS','LAST 5 HOURS','LAST 6 HOURS','LAST 7 HOURS','LAST 8 HOURS','LAST 9 HOURS','LAST 10 HOURS','LAST 11 HOURS','LAST 12 HOURS','LAST 24 HOURS','LAST 1 YEAR'}; 

% 
% keywords_1_cat = categorical(keywords_1); % { 'TODAY', 'YESTERDAY' };
% keywords_2_cat = categorical(keywords_2); % { 'THIS WEEK', 'LAST WEEK', 'THIS MONTH', 'LAST MONTH',  'THIS YEAR' ,  'LAST YEAR' };
% keywords_3_cat = categorical(keywords_3); % { 'LAST 7 DAYS','LAST 30 DAYS' };

% Assumptions: 2 date-strings separated by either comma or blank or 'to'
% or:          1 date-string: in this case it is taken as a given day.
date_str_format = 'mm/dd/yyyy'; % jas_hard_coded from Dinakar: no datetime resolution.
% clean up the sepparator: replace it with blank.
edit_req_date_str = get(hObject,'String');
% HAVE ONE OR 2 CONTIGUOS VALID STRINGS.

edit_req_date_str_ori = upper(edit_req_date_str);
edit_req_date_str_ori = strrep(edit_req_date_str_ori,'''','');  % get rid of quotes: if any
edit_req_date_str_ori = strrep(edit_req_date_str_ori,'  ',' '); % get rid of multiple spaces: if any

token_delimiter = {' ','\f','\n','\r','\t','\v'};
token_cel = strsplit(edit_req_date_str_ori, token_delimiter) ;
token_cnt = size(token_cel,2);

if ( (token_cnt > 1) && (isempty(token_cel{1})) )
    % EMPTY TOKEN AT THE BEGINNING: Get rid of it.
    token_cel = token_cel(2:end);
end

if ( (token_cnt > 1) && (isempty(token_cel{end})) )
    % EMPTY TOKEN AT THE END: Get rid of it.
    token_cel = token_cel(1:end-1);
end

token_cnt = size(token_cel,2);

req_start_date_str = '';
req_end_date_str   = '';

date_format_ok_flag =0;
switch token_cnt
    case 1
        if (isempty(token_cel{1}))
            % BLANKS INPUT: Reset
            date_format_ok_flag = 1;
            handles.ui_state_struct.req_start_date_str = '';
            handles.ui_state_struct.req_end_date_str   = '';
            status_message = sprintf('Not using Date filter ' );
            set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
            return;
        else
            % SOM INPUT: Parse it.            
            if (is_date_time(token_cel{1},date_str_format ))
                % SINGLE TOKEN AND IS A DATE
                req_start_date_str = char(token_cel{1});
                req_end_date_str   = char(token_cel{1});
                date_format_ok_flag =1;
            end
            if (any(ismember(keywords_1,token_cel{1})))
                [ req_start_date_str, req_end_date_str ] = date_keyword_1(token_cel{1});
                date_format_ok_flag =1;
            end
        end
    case 2  %
        if ( (strcmp(token_cel{1},'FROM')) && (is_date_time(token_cel{2},date_str_format  )))
            % 2 tokens: FROM date
            req_start_date_str = char(token_cel{2});
            req_end_date_str   = ''; % means open end from the right
            date_format_ok_flag =1;
        end
        if ( (is_date_time(token_cel{1},date_str_format  ))&& (is_date_time(token_cel{2},date_str_format  )) )
            % "2 DATEs:  the start_date and end_date
            req_start_date_str = char(token_cel{1});
            req_end_date_str   = char(token_cel{2});
            date_format_ok_flag =1;
        end % 2 tokens and 2 dates
        if ( (strcmp(token_cel{1},'TO'))&& (is_date_time(token_cel{2},date_str_format  )) )
            % 2 tokens: TO date
            req_start_date_str = '';
            req_end_date_str   = char(token_cel{2});
            date_format_ok_flag =1;
        end
        wanted_keyword = sprintf('%s %s',token_cel{1},token_cel{2});
        if (any(ismember(keywords_2,wanted_keyword)))
            [ req_start_date_str, req_end_date_str ] = date_keyword_2(wanted_keyword);
            date_format_ok_flag =1;
        end
        
    case 3  % 3 TOKENS
        if ( (strcmp(token_cel{1},'FROM')) && (is_date_time(token_cel{2},date_str_format  )) && (is_date_time(token_cel{3},date_str_format  )) )
            % 3 tokens: FROM date date
            req_start_date_str = char(token_cel{2});
            req_end_date_str   = char(token_cel{3});
            date_format_ok_flag =1;
        end
        if (  (is_date_time(token_cel{1},date_str_format  )) && (strcmp(token_cel{2},'TO')) && (is_date_time(token_cel{3},date_str_format  )) )
            % 3 tokens: date to date
            req_start_date_str = char(token_cel{1});
            req_end_date_str   = char(token_cel{3});
            date_format_ok_flag =1;
        end
        
        if (  (is_date_time(token_cel{1},date_str_format  )) && (is_date_time(token_cel{2},date_str_format  )) )  % ignore third: garbabe likely
            % 3 tokens: date date xxx
            req_start_date_str = char(token_cel{1});
            req_end_date_str   = char(token_cel{2});
            date_format_ok_flag =1;
        end
        wanted_keyword = sprintf('%s %s %s',token_cel{1},token_cel{2},token_cel{3}); 
        if (any(ismember(keywords_3,wanted_keyword)))
            [ req_start_date_str, req_end_date_str ] = date_keyword_3(wanted_keyword);
            date_format_ok_flag =1;
        end        
    case 4  % 4 TOKENS
        if (  ( strcmp(token_cel{1},'FROM') ) &&  (is_date_time(token_cel{2},date_str_format ))  && ( strcmp(token_cel{3},'TO')) &&   (is_date_time(token_cel{4},date_str_format )) )
            req_start_date_str = char(token_cel{2});
            req_end_date_str   = char(token_cel{4});
            date_format_ok_flag =1;
        end
%                       1    2          3        4  5  6          7        8        
    case 8  %  8 TOKENS FROM 04/26/2018 02:06:03 PM TO 04/27/2018 07:00:00 AM
        start_candidate     = [ char(token_cel{2}),' ',char(token_cel{3}),' ',char(token_cel{4}) ];
        end_candidate       = [ char(token_cel{6}),' ',char(token_cel{7}),' ',char(token_cel{8}) ];
        date_time_str_format =  'mm/dd/yyyy HH:MM:SS';
        if (        (strcmp(token_cel{1},'FROM') ) ...
                &&  (is_date_time(start_candidate,date_time_str_format ))  ...
                &&  (strcmp(token_cel{5},'TO')) ...
                &&  (is_date_time(end_candidate,date_time_str_format )) )
            req_start_date_str = start_candidate;
            req_end_date_str   = end_candidate;
            date_format_ok_flag= 1;
        end        
    otherwise  % more than 4 TOKENS
end % case token_cnt

% DONE PARSING TOKENS AND REQUEST DATE STRING: Save results.

if (date_format_ok_flag)
    req_start_date_str                          = req_date_struct.start_sec_resol_date_str;
    req_end_date_str                            = req_date_struct.end_sec_resol_date_str;
    handles.ui_state_struct.req_start_date_str  = req_start_date_str; % strrep(req_start_date_str,' ','');
    handles.ui_state_struct.req_end_date_str    = req_end_date_str;   % strrep(req_end_date_str,  ' ','');
    handles.ui_state_struct.req_date_struct     = req_date_struct;    %jas_to_be_used everywere mar-27-2018
    status_message = sprintf('Request date range:   FROM  %-s    TO    %-s',req_date_struct.start_sec_resol_date_str ,req_date_struct.end_sec_resol_date_str );
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
else
    error_message = sprintf(' Invalid dates. %3d words: Use this format: FROM 11/22/2017 TO 11/23/2017   Ignoring Invalid dates: %s ',token_cnt,edit_req_date_str_ori);
    set(handles.text_exp_status,'string',error_message,'ForegroundColor','red','Fontsize',12); %'blue'
    fprintf(error_message);
end
end % fn sa_do_edit_req_date

% cases:
% 1 - YES FROM and YES TO:  expect 2 dates
% 2 - YES FROM and NO  TO:  expect 1 date=from  req_end_date_str='';
% 3 - NO  FROM and YES TO:  expect 1 date=to    req_start_date_str='';
% 4 - NO  FROM and NOT TO:
%     FOUND coma:
%         3_str:
%         clear_cut                            req_start_date_str=first_str    req_end_date_str=second_str
%         2_str:
% 5 -     if comma second: first is from_date  req_end_date_str=''              req_start_date_str=first_str
% 6 -     if comma first:  second is to_date   req_start_date_str=''            req_end_date_str=second_str
%         1_str:
%         error: blank dates with just a comma.
%     FOUND NO coma:
%         1_str:
%           req_start_date_str=first_str    req_end_date_str=first_str
%         2_str:
%           req_start_date_str=first_str    req_end_date_str=second_str



%
% OLD VERSION:
%
%
% edit_req_date_str = split(edit_req_date_str_ori,delimiter);
% if ( isdatetime(datetime(edit_req_date_str)))
%     % VALID 1 or 2 dates)
%     req_start_date_str = edit_req_date_str(1,:);
%     %req_end_date_str   = '';
%     if (size(edit_req_date_str,1) == 2)
%         % VALID 2 dates
%         req_end_date_str = edit_req_date_str(2,:);
%     else
%         % ONE VALID DATE: Determine resolution
%         [start_hour,start_minute] = hms(datetime(req_start_date_str));
%         if (start_hour == 0 && start_minute == 0)
%             % ONLY DAY SPEC (missing hour): Set it to 24 hours
%             req_end_date_str = strcat(req_start_date_str,' 23 59 59');
%         else
%             % ONLY DAY-HOUR SPEC (missing minute: Set it to 1 hour
%             req_end_date_str = strcat(req_start_date_str,sprintf(' %2d:%-2d ',start_hour+1,start_minute));
%         end
%     end

% try
%    D = datenum(token_cel{2},'dd/mm/yyyy')
% catch
%    error('Incorrect Format')
% end

