function [db_data_set_struct  ] = fill_in_db_set_from_fake_data(...
    input_parameters_struct,algo_gb_struct  ...
    ,file_loaded_count,raw_times_data,raw_shifts_data, raw_col_names_data,raw_tr_data,meta_data_data, ...
    db_data_set_struct )
%FILL_IN_DB_SET_FROM_FAKE_DATA temporary function to fill in a db_set from fake data (old QC from Luis: 5 dirs)
%   returns: db_data_set_struct  with the data loaded from the excel files: 2 itemts:
%            1: raw_times_data,raw_shifts_data, raw_col_names_data,raw_tr_data,
%            2: meta_data_data:

%                 'ANA Multiplex (12 Chip) 1.1 QC-Prod'     % 1  smd_proto
%                 '160505'                                  % 2  smd_chipl
%                 '0100009601'                              % 3
%                 '21'                                      % 4  smd_instr
%                 '2.2.79.0'                                % 5
%                 '9/6/2016 (UTC)'                          % 6
%                 '7:31 PM (UTC)'                           % 7
%                 'MAV-011615-0827 system'                  % 8
%                 'CL160505-12-Array-SP-01'                 % 9  smd_exper
%                 '1'                                       % 10
%                 '1'                                     	% 11 smd_carri
%                 'd08ee7d7-523c-4256-a4c5-a43e2c9be42c'    % 12
%                 'SAMPLE #1'                           	% 13 smd_sampl
%                 '1'                                       % 15
%
% 
%     smd_proto   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{1 });
%     smd_sampl   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{13});
%     smd_chipl   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{2 });
%     smd_exper   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{9 });
%     smd_instr   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{4 });
%     smd_carri   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{11}); % Chip #  saved as carri.

% pipeline:    Sensogram_Analysis_App_10 -> pushbutton_get_data_Callback --> fake_input_data -> fill_in_db_set_from_fake_data
%db_data_set_struct  = nan;

return;  %jas_debug_jas_debug % *********************  jas_debug *****


% 1. determine dimentions for the matrix: ____________________________________________
curve_size_len_max =0;
curve_size_cnt_max =0;
for cur_file = 1:1:file_loaded_count
    temp_size = size(raw_shifts_data{cur_file,1});
    if (temp_size(1) > curve_size_len_max)
        curve_size_len_max = temp_size(1);
    end
    if (temp_size(2) > curve_size_cnt_max)
        curve_size_cnt_max = temp_size(2);
    end
end

% Allocate memory for: raw_shift,time,envelope,flags and features matrices
time_raw_matrix           = nan(curve_size_len_max,             curve_size_cnt_max,file_loaded_count);
shift_raw_matrix          = nan(curve_size_len_max,             curve_size_cnt_max,file_loaded_count);
feature_raw_matrix        = nan(algo_gb_struct.feature_row_max, curve_size_cnt_max,file_loaded_count);

tr_matrix                 = nan(curve_size_len_max,          algo_gb_struct.tr_last_col,file_loaded_count);
%flag_matrix              = nan(algo_gb_struct.flag_row_max, curve_size_cnt_max,        file_loaded_count);
flag_raw_matrix           = nan(algo_gb_struct.flag_row_max, curve_size_cnt_max,        file_loaded_count);
envelope_raw_matrix       = nan(curve_size_len_max,          curve_size_cnt_max,        algo_gb_struct.envelope_col_max);

% Normalized matrices have the same dimentions as the raw matrices. The only exception is the tr_matrix.

time_normalized_matrix    = time_raw_matrix;
shift_normalized_matrix   = shift_raw_matrix;
feature_normalized_matrix = feature_raw_matrix;
envelope_normalized_matrix= envelope_raw_matrix;
flag_normalized_matrix    = flag_raw_matrix;

% 3. populate times and shifts matrices: bridge: from algo to algo2
for cur_file = 1:1:file_loaded_count
    cur_time            = raw_times_data{cur_file};
    curve_times_len     = cur_time(end,:);         % get excel curve length list for times
    cur_time            = cur_time(1:end-1,:);     % Do not include last row: i.e. N
    raw_times_size      = size(cur_time);
    time_curve_len       =raw_times_size(1);
    time_curve_cnt      = raw_times_size(2);
    time_raw_matrix(1:time_curve_len,1:time_curve_cnt,cur_file)=cur_time;
    
    cur_shift            = raw_shifts_data{cur_file};
    curve_shifts_len     = cur_shift(end,:);         % get excel curve length list for shifts
    cur_shift            = cur_shift(1:end-1,:);     % Do not include last row: i.e. N
    raw_shifts_data_size = size(cur_shift);
    shift_curve_len      = raw_shifts_data_size(1);
    shift_curve_cnt      = raw_shifts_data_size(2);
    shift_raw_matrix( 1:shift_curve_len,1:shift_curve_cnt,cur_file)=cur_shift;
    
    % perform sanity check: Times and shifts sizes MUST match
    if (size(cur_time) ~= size(cur_shift))
        msg = sprintf(' EXCEL FILE FORMAT ISSUE: Sizes of times and shifts DO NOT MATCH. File Number = %d',cur_file);
        display(msg)
    end
    if (curve_times_len ~= curve_shifts_len)
        msg = sprintf(' DATA MISTMATCH ISSUE: Sizes of times and shifts DO NOT MATCH. File Number = %d',cur_file);
        display(msg)
    end
    % Store excel curve length for each curve and true curve length:
    % i.e. finite value count on each curve: non nan/non-blank values    
    feature_raw_matrix(algo_gb_struct.feature_excel_cnt_row,:,cur_file) = curve_shifts_len; % save excel N for each curve    
    for cur_curve=1:1:shift_curve_cnt
        curve_len        = feature_raw_matrix(algo_gb_struct.feature_excel_cnt_row,cur_curve,cur_file);
        curve_len_finite = sum(isfinite(cur_shift(1:curve_len,cur_curve)));
        feature_raw_matrix(algo_gb_struct.feature_cnt_row,:,cur_file) = curve_len_finite;     % Save non-nan N for each curve:
        if (curve_len_finite ~= curve_len)
            msg = sprintf(' nan data  ISSUE: File Number = %d Curve Number = %d Expected data points = finite_data_points =%d'...
                ,cur_file,curve_len,curve_len_finite);
            display(msg)
        end
    end
end  % for cur_file 

% 4. populate tr matrix wiht raw
[ tr_matrix  ] = tr_build2(algo_gb_struct,tr_matrix, raw_tr_data,file_loaded_count,feature_raw_matrix  );

% 5. calculate tr, evaluate tr to decide: Normalization is possible or not. Then Normalize
% old code ................
[analyte_names ]           = build_list_of_analyte_names(input_parameters_struct.input_file_names,raw_col_names_data{1}); % build list of unique analyte names
%analyte_dbg_ndx            = 9; %    9 % CenpB index = 36 to 39---   % one of the worst
analyte_dbg_ndx            = 2; %    2 % Sm    index = 36 to 39---   % one of the best
display(sprintf('debugging %s  ... ',cell2mat(analyte_names{analyte_dbg_ndx})));
%[a1,a2,a3,a4,a5,a6,a7] = 
report_stat_matrix(algo_gb_struct,'1. before_normalizing. Raw', time_raw_matrix,shift_raw_matrix,tr_matrix...
    ,analyte_names,analyte_dbg_ndx,flag_raw_matrix,feature_raw_matrix  );
display 'before_normalizing'

[ tr_matrix ,flag_raw_matrix,time_normalized_matrix,shift_normalized_matrix]  ...
    = tr_calc_and_evaluate(algo_gb_struct         ...
    ,tr_matrix,flag_raw_matrix          ...
    ,time_normalized_matrix         ...
    ,shift_normalized_matrix        ...
    ,input_parameters_struct.tr_delta_step_threshold        ...
    ,time_raw_matrix                ...
    ,shift_raw_matrix               ...
    ,feature_raw_matrix,raw_shifts_data ...
    ,file_loaded_count );

% jas_tbd: use tr_matrix to populate the tr columns


%[a1,a2,a3,a4,a5,a6,a7] =
report_stat_matrix(algo_gb_struct,'2. after_normalizing. Raw       ', time_raw_matrix,shift_raw_matrix,tr_matrix,analyte_names,analyte_dbg_ndx,flag_raw_matrix,feature_raw_matrix  );
%[a1,a2,a3,a4,a5,a6,a7] = report_stat_matrix('3. after_normalizing. Normalized', time_normalized_matrix,shift_normalized_matrix,tr_matrix,analyte_names,analyte_dbg_ndx,flag_normalized_matrix,feature_normalized_matrix  );
display 'done_normalizing'

% Pack algo matrices into algo2 matices: bridge  Arrays and Matrices to pack:
% curve_size_len_max, curve_size_cnt_max,file_loaded_count ...
%     ,time_raw_matrix        ,shift_raw_matrix                        ...
%     ,time_normalized_matrix ,shift_normalized_matrix                 ...
%     ,tr_matrix                                                       ...
%     ,flag_raw_matrix         ,flag_normalized_matrix                 ...
%     ,feature_raw_matrix      ,feature_normalized_matrix              ...
%     ,envelope_raw_matrix     ,envelope_normalized_matrix             ...
total_files                 = size(time_raw_matrix,3); % loaded files               = 24
total_sensograms_per_file   = size(time_raw_matrix,2); % total_sensograms_per_file  = 64
max_len_sensograms_per_file = size(time_raw_matrix,1); % max_len_sensograms_per_file= 63 just the data.
data_set_sensogram_cnt      = total_files * total_sensograms_per_file; % (includes trs and references)
rows_db_cnt                 = data_set_sensogram_cnt;   
% 2D matrices: vector of scan data per sensogram (the real data  ...)
scan_raw_time       = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
scan_raw_gru        = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
scan_raw_len        = nan(data_set_sensogram_cnt,1);
scan_raw_tr_gru     = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
scan_nrm_gru        = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
scan_nrm_gru_matlab = nan(data_set_sensogram_cnt,max_len_sensograms_per_file);
orndx       = 1:1:data_set_sensogram_cnt;
orndx       = orndx'; % col vector
% metadata:
smd_probe           =  cell(data_set_sensogram_cnt,1); % 1  ; %
smd_dates           =  cell(data_set_sensogram_cnt,1); % 2  ; % run date as a string.
smd_proto           =  cell(data_set_sensogram_cnt,1); % 3  ; %
smd_sampl         	=  cell(data_set_sensogram_cnt,1); % 4  ; %
smd_chipl        	=  cell(data_set_sensogram_cnt,1); % 5  ; %
smd_exper       	=  cell(data_set_sensogram_cnt,1); % 6  ; %
smd_instr           =  cell(data_set_sensogram_cnt,1); % 7  ; %
smd_carri          	=  cell(data_set_sensogram_cnt,1); % 8  ; %
smd_chann          	=  cell(data_set_sensogram_cnt,1); % 9  ; %
smd_clust       	=  cell(data_set_sensogram_cnt,1); % 10 ; %
smd_datat           =  cell(data_set_sensogram_cnt,1); % 11 ; %
smd_valfg           =  cell(data_set_sensogram_cnt,1); % 13 ; %
smd_outfg        	=  cell(data_set_sensogram_cnt,1); % 14 ; %
smd_error        	=  cell(data_set_sensogram_cnt,1); % 15 ; %
expid               =  cell(data_set_sensogram_cnt,1); % 16 ; % to join_with_toe

datef      	=  nan(data_set_sensogram_cnt,1);
bligc     	=  nan(data_set_sensogram_cnt,1); % 3 cuantities to be calculated for raw data
ampgc     	=  nan(data_set_sensogram_cnt,1);
shifc     	=  nan(data_set_sensogram_cnt,1);

% sensogram classification:  rows_db_cnt  jas_sc

    

%tbd wed: complete and dbg next loop.
%         for each sensogram: add proper fields: strings etc. 
%                             build proper indices
%                             fill in tos (maybe after the loop in one shot per column). 

% jas_hard_coded_from_excel file: first 128 cols. (not including tr) and including 2 refs)
analyte_names_qc = { ...
'dsDNA'              ...
,'Sm'                ...
,'Scl-70'            ...
,'Jo-1'              ...
,'SS-A 60'           ...
,'SS-A 52'           ...
,'SS-B'              ...
,'RNP'               ...
,'Cenp B'            ...
,'PCNA'              ...
,'Nucleosome'        ...
,'Ribo-P P0'         ...
,'Ku'                ...
,'Anti-IgG'          ...
,'Ref 1'             ...
,'Ref 2'             ...
};


row_start=1;
temp_chann  = repmat({'1';'2'},32,1);
temp_chann  =  temp_chann';

temp_clust  = repmat({'1';'2';'3';'4'},16,1);
 

analyte_names_qc_temp  = analyte_names_qc';
temp_probe	= repmat(analyte_names_qc_temp,4,1);
temp_probe  = temp_probe'; % now temp_probe is a 1x64 cell array 
qct = analyte_names_qc';
a  = repmat(qct,1,4)';
temp_probe_fixed = reshape(a,1,64);

  smd_probe(row_start:row_start+shift_curve_cnt-1) = { cellstr(analyte_names_qc) }; 
  
for cur_file = 1:1:file_loaded_count
    % copy all the data from a given file in one shot to build the algo2 matrices: 2D matrices.
    % 1_ get scan_raw_time:
    cur_time            = raw_times_data{cur_file};
    curve_times_len     = cur_time(end,:);         % get excel curve length list for times
    cur_time            = cur_time(1:end-1,:);     % Do not include last row: i.e. N
    raw_times_size      = size(cur_time);
    time_curve_len      = raw_times_size(1);
    time_curve_cnt      = raw_times_size(2);
    cur_time            = cur_time';               % transpose it: points are now in the same row.    
    %time_raw_matrix(1:time_curve_len,1:time_curve_cnt,cur_file)=cur_time';    
    scan_raw_time(row_start:row_start+time_curve_cnt-1,1:time_curve_len)=cur_time;
    % 2_ get scan_raw_gru    
    cur_shift            = raw_shifts_data{cur_file};
    curve_shifts_len     = cur_shift(end,:);         % get excel curve length list for shifts
    cur_shift            = cur_shift(1:end-1,:);     % Do not include last row: i.e. N
    raw_shifts_data_size = size(cur_shift);
    shift_curve_len      = raw_shifts_data_size(1);
    shift_curve_cnt      = raw_shifts_data_size(2);
    cur_shift            = cur_shift';               % transpose it: points are now in the same row.     
    scan_raw_gru(row_start:row_start+shift_curve_cnt-1,1:shift_curve_len)   = cur_shift;  
    scan_raw_len(row_start:row_start+shift_curve_cnt-1,1:shift_curve_len,1) = shift_curve_len;
    % 3_ get scan_raw_tr        
    tr_curve_len  	= feature_raw_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file); % jas_tbr: use length of first curve on the file
    cur_tr        	= tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_shift_col,cur_file);
    % repeat the same tr_matrix for each analyte (even for tr!). (that would be sanity check!).
    cur_tr       	= cur_tr';
    scan_raw_tr_gru(row_start:row_start+shift_curve_cnt-1,1:shift_curve_len)=  repmat(cur_tr,shift_curve_cnt,1);   
    % 4_ get: scan_nrm_gru_matlab from above    
    %         note: normlization calculation need to be updated using: normalization code  from jervis verification. jas_tbd.
    cur_shift_norm  = shift_normalized_matrix(1:tr_curve_len,1:shift_curve_cnt,cur_file); 
    cur_shift_norm  =cur_shift_norm';
    scan_nrm_gru_matlab(row_start:row_start+shift_curve_cnt-1,1:shift_curve_len)=cur_shift_norm; % until available from the db or from jervis.     	
    % 5_ get: scan_nrm_gru: not available here (it will be available from the db or from Jervis)   
    scan_nrm_gru(       row_start:row_start+shift_curve_cnt-1,1:shift_curve_len)=cur_shift_norm; %jas_temp: clone of ml temporarily
    
    % replicate metadata: identical info for whole file, from tab_1 excel into each sensogram:
    cur_meta_data= meta_data_data{cur_file};
    smd_probe(row_start:row_start+shift_curve_cnt-1) = temp_probe_fixed;  % {'yyy'}; % list of probe
    date_str    = cellstr(cur_meta_data{6});
    date_str    = strrep(date_str,'(UTC)','');
    date_str    = strrep(date_str,' ','');
    % replicate the date as a number (for efficiency)
    datef       (row_start:row_start+shift_curve_cnt-1) = datenum(date_str);       
    smd_dates   (row_start:row_start+shift_curve_cnt-1) = cellstr(int2str(datenum(date_str)));  % date_str;
    smd_proto   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{1 });
    smd_sampl   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{13});
    smd_chipl   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{2 });
    smd_exper   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{9 });
    smd_instr   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{4 });
    smd_carri   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{11}); % Chip #  saved as carri.

    smd_clasi   (row_start:row_start+shift_curve_cnt-1) = {'U'};	% U == Unknown  
    smd_chann   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{14});	% fake  jas_ttbd this is the old way. future: from protocol jas_tbd
    smd_clust   (row_start:row_start+shift_curve_cnt-1) = cellstr(temp_clust);	        % fake  jas_ttbd
    smd_datat   (row_start:row_start+shift_curve_cnt-1) = {'raw'};  % fake  jas_ttbd
    smd_valfg   (row_start:row_start+shift_curve_cnt-1) = {'nan'};	% fake  jas_ttbd   
    smd_outfg   (row_start:row_start+shift_curve_cnt-1) = {'nan'};	% fake  jas_ttbd  
    smd_error   (row_start:row_start+shift_curve_cnt-1) = {'nan'};	% fake  jas_ttbd
    expid   (row_start:row_start+shift_curve_cnt-1) = cellstr(cur_meta_data{1 });  % to join_with_toe  empty for the time being jas_test_only
    
%     smd_chann   (row_start:row_start+shift_curve_cnt-1) = temp_chann;
%   % jas fake_clust: need to be replaced for real     
%     smd_clust  (row_start:row_start+shift_curve_cnt-1) = temp_probe;
    % move to the next file.
    row_start = row_start+time_curve_cnt; % get ready for next iter.
end % for cur_file

% store everything into db_data_set_struct: start with single values:
db_data_set_struct.rows_db_cnt          = { rows_db_cnt             };     % single float
db_data_set_struct.probe                = analyte_names_qc;                 % single cell: tbu: probe pull down

% scan data: raw,normalized,tr etc 2D matrices: The big data.

db_data_set_struct.scan_raw_time        = { scan_raw_time         	};     % 2D matrix with scan data: 
db_data_set_struct.scan_raw_gru     	= { scan_raw_gru         	};     % 2D matrix
db_data_set_struct.scan_raw_len     	= { scan_raw_len         	};     % 1D matrix
db_data_set_struct.scan_raw_tr_gru      = { scan_raw_tr_gru         };     % 2D matrix
db_data_set_struct.scan_nrm_gru     	= { scan_nrm_gru         	};     % 2D matrix
db_data_set_struct.scan_nrm_gru_matlab	= { scan_nrm_gru_matlab   	};     % 2D matrix

% jas_tbd_ call here or find out where is calculated: blinc and amplc  for raw data.

% store float arrays:
db_data_set_struct.orndx            = orndx;  
db_data_set_struct.datef            = datef;
db_data_set_struct.bligc        	= bligc;        %  current (default is raw)   save_current
db_data_set_struct.ampgc        	= ampgc;        %  current 
db_data_set_struct.shifc          	= ampgc - bligc;

db_data_set_struct.bligr        	= bligc;        %  raw     (default is raw)   save raw from current
db_data_set_struct.ampgr        	= ampgc;        %  raw 
db_data_set_struct.shifr          	= ampgc - bligc;

db_data_set_struct.bligj        	= bligc;        %  norm jervis     (save fake norm jervis from current because it is not avai from luis files)
db_data_set_struct.ampgj        	= ampgc;        %  norm jervis
db_data_set_struct.shifj          	= ampgc - bligc;

% jas_tbd_ call here the matlab normalization to populate: blinc and ampgc
%  [ blinc and ampgc ] do_matlab_normalization(...)

db_data_set_struct.bligm        	= bligc;        %  norm matlab     (save fake norm matlab from current  because I have not done 2 things: 1 norm, 2 calc 3quant for norm)
db_data_set_struct.ampgm        	= ampgc;        %  norm matlab  
db_data_set_struct.shifm          	= ampgc - bligc;

% cvt regular arrays into categorical arrays:  real: (missing: date)
% and store them into the db_metad_set_struct to be returned.

db_metad_set_struct = struct;

db_metad_set_struct.clasi           = util_transpose_to_col_vector(categorical(smd_clasi,'Ordinal',true));  %jas_temp fake
db_metad_set_struct.clasi           = addcats(db_metad_set_struct.clasi, { 'A','G' } ,'Before','U'); 
% jas_clasi_jas_categories_jas_add_cat_here 
probe_temp                          = categorical(smd_probe,'Ordinal',false);  
db_metad_set_struct.probe           = reordercats(probe_temp,analyte_names_qc);
db_metad_set_struct.dates           = categorical(smd_dates	,'Ordinal',true); 
db_metad_set_struct.proto        	= categorical(smd_proto	,'Ordinal',true);  %jas_tbd: test_ordinal false.        
db_metad_set_struct.sampl           = categorical(smd_sampl	,'Ordinal',true);  %jas_tbd: test_ordinal false.        
db_metad_set_struct.chipl        	= categorical(smd_chipl	,'Ordinal',true);  %jas_tbd: test_ordinal false.        
db_metad_set_struct.exper         	= categorical(smd_exper	,'Ordinal',true);  %jas_tbd: test_ordinal false.        
db_metad_set_struct.instr        	= categorical(smd_instr	,'Ordinal',true);        
db_metad_set_struct.carri        	= categorical(smd_carri	,'Ordinal',true);        
db_metad_set_struct.chann         	= categorical(smd_chann	,'Ordinal',true);        
db_metad_set_struct.clust       	= categorical(smd_clust	,'Ordinal',true);        
db_metad_set_struct.datat           = categorical(smd_datat	,'Ordinal',true);        
db_metad_set_struct.valfg           = categorical(smd_valfg	,'Ordinal',true);  % not_avail jas_temp   ; categorical(smd_valfg    ,'Ordinal',true);        
db_metad_set_struct.outfg           = categorical(smd_outfg	,'Ordinal',true);  % not_avail jas_temp   ;categorical(smd_outfg  ,'Ordinal',true);        
db_metad_set_struct.error           = categorical(smd_error	,'Ordinal',true);  % not_avail jas_temp   ;categorical(smd_error,'Ordinal',true);   
db_metad_set_struct.expid        	= categorical(expid	,'Ordinal',false);  

db_data_set_struct.nomfg            = false; % jas_tbd_change to true when done. normalization matlab flag done.

db_data_set_struct.sc_clasi_flag 	= zeros(rows_db_cnt,db_data_set_struct.sc_clasi_flag_struct.clasi_flag_col_cnt);  %jas_temp fake rows_db_cnt
db_data_set_struct.sc_clasi_vals	= zeros(rows_db_cnt,db_data_set_struct.sc_clasi_vals_struct.clasi_vals_col_cnt); %jas_temp fake


db_data_set_struct.db_metad_set_struct = db_metad_set_struct;



end % fn fill_in_db_set_from_fake_data

%_debug_ jas_debug
% display(sprintf('category count for probe = %4d',size(categories(db_metad_set_struct.probe),1)));                  
% display(sprintf('category count for proto = %4d',size(categories(db_metad_set_struct.proto),1)));         
% display(sprintf('category count for sampl = %4d',size(categories(db_metad_set_struct.sampl),1)));                 
% display(sprintf('category count for chipl = %4d',size(categories(db_metad_set_struct.chipl),1)));                 
% display(sprintf('category count for exper = %4d',size(categories(db_metad_set_struct.exper),1)));                 
% display(sprintf('category count for instr = %4d',size(categories(db_metad_set_struct.instr),1)));               
% display(sprintf('category count for carri = %4d',size(categories(db_metad_set_struct.carri),1)));               
% display(sprintf('category count for chann = %4d',size(categories(db_metad_set_struct.chann),1)));               
% display(sprintf('category count for clust = %4d',size(categories(db_metad_set_struct.clust),1)));        	  
% display(sprintf('category count for datat = %4d',size(categories(db_metad_set_struct.datat),1)));   

