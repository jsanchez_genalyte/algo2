function [ ok_file_flag ] = init_exp_data_set(an_excel_file_name) %
%INIT_EXP_DATA_SET generates the output excel file  

% clean_data: Empty structure to accumulate data after being cleaned.
exp_data_set                    = struct;        
exp_data_set.sensor_excel_table = [];
exp_data_set.scan_data_struct   = [];

A13RZEW last from alexa


qc_rpt_detail_header = { 'experiment',	'run_date',	'sample id',	'analyte',	'channel',	'chip',	'sensor number',	'is valid',	'is outlier',	'gru shift',	'instrument',	'carrier serial number'	'spotting lot number'	'protocol'	'tracking id'	'baseline start'	'baseline duration'	'amplified start',	'amplified duration',	'guid'};


