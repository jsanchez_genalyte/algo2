function [ handles ] = sa_init_env_app( handles,input_parameters_struct )
%INIT_ENV_APP One time initialization of all UI elements and it's state.
%   see:  sa_ui_set_env_after_get_data 
%   Elements initialized:
%   E1. Main Menus   
%   E2. Popup Menus
%   E3. Pull Down Menus
%   E4. Radio Buttons
%   E5. Table of Fits 
%   E6. Plots: 

%sa_declare_globals; 
if (isfield(handles,'ui_state_struct'))
    ui_state_struct = handles.ui_state_struct;
else
    ui_state_struct = struct;   % empty struct that will be filled mostly in: sa_ui_set_env_after_get_data
end
%                                           % sa_ui_set_popupmenus_after_popup_change and ui_do_plot_sens_set
% set(handles.listbox_excel_tab_selection,  'Visible','off','Enable','off');
% set(handles.excel_sheet_selection_text,   'Visible','off'); 

% Initialize/Show/Enable/ All data filters.
% jas_tbd_all initializations 
% probe_names, dates, etc.

% E1.  get axes positions:
% Save size of both anom and good axes: From the figure: half position. Then guest-timate the full positions.
ui_state_struct.axes_good_half_pos      = get(handles.axes_good_sens,'Position'); %  [0.5466 0.1254 0.4521 0.7977]
ui_state_struct.axes_anom_half_pos      = get(handles.axes_good_sens,'Position');
temp_pos = get(handles.axes_anom_sens,'Position'); %
ui_state_struct.axes_anom_half_pos(1)   = temp_pos(1); % overwrite just the x-position: grab it from the figure.
%  do: full positions: 
ui_state_struct.axes_good_full_pos      = ui_state_struct.axes_good_half_pos;
ui_state_struct.axes_good_full_pos(3)   = ui_state_struct.axes_good_half_pos(3)* 2.10; %jas_hardcoded: approx: from .45 to ~.95
ui_state_struct.axes_anom_full_pos      = ui_state_struct.axes_good_full_pos; % anom is identical to full in the full position.

% E1.  Main Menus FILE/EDIT/VIEW/ANALYZE MENU: Disable all but the open excel and open session menu items from the file menu: ____________
set(handles.file_menu,                      'Visible','on','Enable','on');           % menu_1 file
set(handles.file_new_local_menu_item,       'Visible','on','Enable','on');
set(handles.file_open_local_menu_item,      'Visible','on','Enable','on');
set(handles.file_save_db_local_menu_item, 	'Visible','on','Enable','off');
set(handles.file_append_sel_local_menu_item,'Visible','on','Enable','off');
set(handles.file_exit_menu_item,            'Visible','on','Enable','on');

set(handles.edit_menu,                      'Visible','on','Enable','on');           % menu_2 edit
set(handles.edit_sel_inv_menu_item,         'Visible','on','Enable','off');
set(handles.edit_copy_sel_menu_item,        'Visible','on','Enable','off');
set(handles.edit_copy_window_menu_item, 	'Visible','on','Enable','on');

set(handles.view_menu,                          'Visible','on','Enable','on');    	% menu_3 view
set(handles.view_manual_scale_good_menu_item, 	'Visible','on','Enable','on');
set(handles.view_auto_scale_anom_menu_item,    	'Visible','on','Enable','on');
set(handles.view_manual_scale_anom_menu_item, 	'Visible','on','Enable','on');
set(handles.view_plot_tr_anom_menu_item,        'Visible','on','Enable','on');
set(handles.view_plot_meas_reg_menu_item,     	'Visible','on','Enable','on');
set(handles.view_plot_grid_menu_item,           'Visible','on','Enable','on');
set(handles.view_plot_bline_circles_menu_item,  'Visible','on','Enable','on');
set(handles.view_plot_ampli_circles_menu_item, 	'Visible','on','Enable','on');

set(handles.analysis_menu,                      'Visible','on','Enable','off');     % menu_4 analysis
set(handles.analysis_norm_jervis_menu_item,   	'Visible','on','Enable','off');
set(handles.analysis_norm_matlab_menu_item,   	'Visible','on','Enable','on');
set(handles.analysis_classi_senso_menu_item,	'Visible','on','Enable','on');      % Re- classify selected sensograms.

% thresholds_menu                                                                   % menu_5 thersholds jas_tbd

% help _menu                                                                        % menu_6 help
set(handles.help_menu,                          'Visible','on','Enable','on');    	
set(handles.help_about_sc_menu_item,            'Visible','on','Enable','on');      % displays version
set(handles.help_help_menu_item,                'Visible','on','Enable','on');      % help dlg

%E2. Pull Down Menus
%text_probe  1
new_probe_cell = input_parameters_struct.probe_valid; %    not needed nymore. all is part of the cell :%{'all' , cellstr(input_parameters_struct.probe_valid) };
set(handles.popupmenu_probe,'String',new_probe_cell,'Value',1);
%text_date  2
new_dates_cell = input_parameters_struct.dates_valid; % 
set(handles.popupmenu_dates,'String',new_dates_cell,'Value',1);
%text_proto 3
new_proto_cell = input_parameters_struct.proto_valid; % 
set(handles.popupmenu_proto,'String',new_proto_cell,'Value',1);
%text_sampl 4
new_sampl_cell = input_parameters_struct.sampl_valid; % 
set(handles.popupmenu_sampl,'String',new_sampl_cell,'Value',1);
%text_chipl 5
new_chipl_cell = input_parameters_struct.chipl_valid; % 
set(handles.popupmenu_chipl,'String',new_chipl_cell,'Value',1);
%text_exper 6
new_exper_cell = input_parameters_struct.exper_valid; % 
set(handles.popupmenu_exper,'String',new_exper_cell,'Value',1);
%text_chipl 7
new_instr_cell = input_parameters_struct.instr_valid; % 
set(handles.popupmenu_instr,'String',new_instr_cell,'Value',1);
%text_carri 8
new_carri_cell = input_parameters_struct.carri_valid; % 
set(handles.popupmenu_carri,'String',new_carri_cell,'Value',1);
%text_chann 9
new_chann_cell = input_parameters_struct.chann_valid; % 
set(handles.popupmenu_chann,'String',new_chann_cell,'Value',1);
%text_clust  10
new_clust_cell = input_parameters_struct.clust_valid; % 
set(handles.popupmenu_clust,'String',new_clust_cell,'Value',1);
%text_datat  11
new_datat_cell = input_parameters_struct.datat_valid; % 
set(handles.popupmenu_datat,'String',new_datat_cell,'Value',1);
%text_ampli  12
new_ampli_cell = input_parameters_struct.ampli_valid; % 
set(handles.popupmenu_ampli,'String',new_ampli_cell,'Value',1);
% text_probe      	popupmenu_probe     1
% text_dates     	popupmenu_dates     2
% text_proto      	popupmenu_proto     3
% text_sampl       	popupmenu_sampl     4
% text_chipl      	popupmenu_chipl     5
% text_exper       	popupmenu_exper  	6
% text_instr     	popupmenu_instr     7
% text_carri        popupmenu_carri     8
% text_chann        popupmenu_chann     9
% text_clust        popupmenu_clust   	10
% text_datat     	popupmenu_datat   	11
% text_ampli        popupmenu_ampli     12

%E3. Pull Down Menus ____________________
% jas_tbd. 
%E4. Radio Buttons
%E5. Table of Fits 
%E6. Plots:

% handle tabs: Always start with experiments tab ___________________
%a = get(handles.uipanel16,'Position');
full_position = [0    0.0    1    0.97 ];  % leave room only for the top buttons (ie fake tabs)
% resize both pannels to the full window
set(handles.ui_panel_expe,'Position',full_position);
set(handles.ui_panel_sens,'Position',full_position);
% % Move to the back ui_panel_senso and to front ui_panel_exper
% % make invisible   ui_panel_senso and visible  ui_panel_exper           

set(handles.ui_panel_expe,'Visible','on');
set(handles.ui_panel_sens,'Visible','off');
set(handles.pushbutton_top_expe,'Enable','on');
set(handles.pushbutton_top_sens,'Enable','on');  % jas_temp: fake data.. so set it to on. but it should be ','off');
set(handles.pushbutton_top_algo,'Enable','on');      % jas_temp: fake algo: does nothing so far....
% lefovers:  to be delted _jas_tbd
% pmp         = get(handles.popupmenu_probe); % old name:    popup_menu_analyte);
% pmp.String  = probe_name_list_cel;
% pmp.Visible = 'on';
% set(handles.popup_menu_reference_material, 'Value', probe_name_cur_ui_ndx);
% set(handles.popup_menu_reference_material,'String', probe_name_list_cel);

% fig_children = get(handles.figure_sens_Anal_app_10,'Children');
%   8�1 graphics array:
%   Panel      (uipanel16)
%   Toolbar    (uitoolbar1)
%   Menu       (help_menu)
%   Menu       (thresholds_menu)
%   Menu       (analysis_menu)
%   Menu       (view_menu)
%   Menu       (edit_menu)
%   Menu       (file_menu)

% pushbutton_req_expe   pushbutton_req_carri    pushbutton_req_spot pushbutton_req_kilo     pushbutton_req_prot pushbutton_req_date
% edit_req_expe         edit_req_carri          edit_req_spot       edit_req_kilo           edit_req_prot       edit_req_date

% pushbutton_top_expe   pushbutton_top_sens    pushbutton_top_algo

    


% store resutls in handles:

handles.input_parameters_struct = input_parameters_struct;
handles.ui_state_struct        	= ui_state_struct;
% lefovers:  to be delted _jas_tbd
% pmp         = get(handles.popupmenu_probe); % old name:    popup_menu_analyte);
% pmp.String  = probe_name_list_cel;
% pmp.Visible = 'on';
% set(handles.popup_menu_reference_material, 'Value', probe_name_cur_ui_ndx);
% set(handles.popup_menu_reference_material,'String', probe_name_list_cel);
 
% Show/Enable all Data Source: 2 radio/1 getData button.

% Hide all plots: good/anom/resuls.

% Hide TOS

% menus: Enable File.  Disable: edit/Analysis/ Enable view.
