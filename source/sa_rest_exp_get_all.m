function  [expe_req_table, error_message ]  = sa_rest_exp_get_all(start_date,end_date,exp_name,rest_token ) % v2_
%sa_rest_exp_get_all retrieves the list of ALL available experiments from Jervis within the date range and a given exp_name Data Service Johns:  API_5: indexTests
% Input: start_date: if blank: means from first exp. Format: mm/dd/yyyy  DAY RESOLUTION and NO PROTOCOL NAMES!
%        end_date:   if blank: means up o current.   Format: mm/dd/yyyy
%        exp_name:   if blank: all experiments. In reallity exp_name could be a spotting lot number, or anything else....
%        rest_token: token from previous connection
%  returns:
%   1 expe_req_table:    a table with a list of experiments availabe that meet the date range and exp name. If none found: exp_list is returned: nan.
%                       expe_req_table  with  fileds:
%                       jas_tbd: id,name and maintenance flag
%                       done: Handles up to 6 concatenations: ie 600 experiments and filters out non-complete jobs
%                       exp_list_cel(1,:)         % =   1�9 cell array
%     '28ec02fe-7bd4-4f54-be32-5f751c1287ef'      % column 1 exp_id
%     'complete'                                  % column 2 exp_status
%     [1]                                         % column 3 exp_xxx
%     '4cdbc37c-3325-443c-a903-b55d70cbdbdd'      % column 4 exp_protocol_id: errors out in stagging
%     'M53'                                       % column 5 exp_device_id
%     {2�1 cell}                                  % column 6 sample_id: Usually 2 tbd.
%     [1�1 struct]                                % column 7 exp name, chip_spot_lot etc... (the good stuff)
%     '2017-07-21T05:57:45.994-07:00'             % column 8 exp_run_date:   11/21/2017 12:00:31pm
%     '2017-07-21T06:00:27.163-07:00'             % Column 9 exp_run_date_2: modified date?
%
%   2 error_message:    either an empty string error message if everything ok, or some kind of diagnostic.
% sample call:          [ expe_req_table,  error_message ]  = sa_rest_exp_get_all('2017-07-20 5:00','2017-07-22 7:00','Jervis ANA Validation', handles.rest_token);
% pipeline:             sa_do_push_req_expe --> sa_rest_exp_get_all --> exp_list_concatenate

% clone:                C:\Users\jsanchez\Documents\dcrc_gen_xml\source
expe_req_table          = table();
%url_experiments_str    = 'h ttp://jervis-staging.genalyte.com/api/tests/';
%url_experiments_str    = 'h ttp://jervis.genalyte.com/api/tests/';

[ url_struct ]          = sa_set_production_vs_stagging_url_flag(); % prod_stag_flag );
url_exp_det_str       	= url_struct.url_exp_det_str;


options                 = weboptions('HeaderFields',{'Content-Type', 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token ); 'RequestMethod', 'post';'ArrayFormat','json'}); % jas_debug_timeout
options.Timeout         = 25; %jas_debug_timeout
sa_concat_size          = 10; % jas_hardcoded: Handling Up to 10x100 = 1K experiments.
api_page_size           = 100;% jas_hardcoded_rest_api_limit_from_john: each page brings 100 experiments at a time.
request_done_ok_flag    = 0; % flag to tell that no exceptions where trown inadvertedly
% creates a weboptions object that contains two header fields: Content-Length with value 78 and Content-Type with value application/json.
%options = weboptions('Authorization', strcat('Bearer', ' ', rest_token ));   %  ,'ContentType','text'
try
    error_message       = '';
    exp_list            = {};  % '2017-07-20 5:00','end_date','2017-07-22 7:00
    exp_set_cnt         = 0;
    exp_list_acc_cel    = {500,1}; %preallocate a large size: wild guess.
    if ((~(isempty(start_date))) &&  (~(isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_1: PASSED THE 3 ARGUMENTS: Full query
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                         	= webread(url_exp_det_str,'start_date',start_date,'end_date',end_date,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag                    = 1;
    end  % case 1
    
    if ((~(isempty(start_date))) &&  ((isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_2: PASSED ONLY 2 ARGUMENTS: 1 0 1
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_exp_det_str,'start_date',start_date,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag = 1;
    end  % case 2
    
    if (((isempty(start_date))) &&  (~(isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_3: PASSED ONLY 2 ARGUMENTS: 0 1 1
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_exp_det_str,'end_date',end_date,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag                    = 1;
    end  % case 3
    
    if ((~(isempty(start_date))) &&  ((isempty(end_date))) &&  ((isempty(exp_name)))  )
        % CASE_4: PASSED ONLY 1 ARGUMENTS: 1 0 0
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_exp_det_str,'start_date',start_date,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag                    = 1;
    end  % case 4
    
    
    if (((isempty(start_date))) &&  (~(isempty(end_date))) &&  ((isempty(exp_name)))  )
        % CASE_5: PASSED ONLY 1 ARGUMENTS: 0 1 0
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_exp_det_str,'end_date',end_date,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag                    = 1;
    end  % case 5
    
    
    % jas_fix_2018_03_28 exp_list            = nan;  % '2017-07-20 5:00','end_date','2017-07-22 7:00
    %jas_here_wed_eve: replicate this fix for each of the cases in a clean way.
    if (((isempty(start_date))) &&  ((isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % CASE_6: PASSED ONLY 1 ARGUMENTS: 0 0 1 date and non-blank experiment
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_exp_det_str,'q',exp_name,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag                    = 1;
    end % case 6: empty start and end dates but not empty experiment name
    
    if (((isempty(start_date))) &&  ((isempty(end_date))) &&  ((isempty(exp_name)))  )
        % CASE_7: PASSED NO ARGUMENTS: set a warning: nothing will be returned
        error_message ='Missing filter. You need at least one. No filters would imply to request ALL experiments. Try again';
        sprintf('\n%s\n',error_message);
        return; % it will return an empty table
    end  % case 7
    
    if ((~(isempty(start_date))) &&  (~(isempty(end_date))) &&  ((isempty(exp_name)))  )
        %  CASE_8: PASSED ONLY 2 ARGUMENTS: 1 1 0 date and blank experiment
        for exp_page_cnt=1:1:sa_concat_size
            exp_list                            = webread(url_exp_det_str,'start_date',start_date,'end_date',end_date,options,'page',num2str(exp_page_cnt));
            exp_list_len                        = size(exp_list,1);
            for cur_exp_ndx = 1:1:size(exp_list,1)
                exp_set_cnt                     = exp_set_cnt+1;
                exp_list(cur_exp_ndx).device_id = { exp_list(cur_exp_ndx).device_id };
                exp_list(cur_exp_ndx).state     = { exp_list(cur_exp_ndx).state     };
                exp_list(cur_exp_ndx).control   = { exp_list(cur_exp_ndx).control   };
                exp_list_acc_cel{exp_set_cnt}   = exp_list(cur_exp_ndx);
            end
            if (exp_list_len < api_page_size)
                % FOUND LESS EXP THAN THE api_page_size: WE ARE DONE: get out.
                break;
            end
        end; % for/parfor while not done
        % concatenate and filter out incomplete jobs:
        [exp_list, error_message ]              = exp_list_concatenate(exp_list_acc_cel(1:exp_set_cnt) );
        request_done_ok_flag                    = 1;
    end % case 8 Last case
    
catch
    % COULD NOT READ EXPERIMENTS: Failure of webread.
    lprintf_log('\n_error_: exception_thrown_function_ sa_rest_exp_get_all.\n');
    error_message_1	= sprintf('\nERROR_ RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE: during webread.  \nFunction: sa_rest_exp_get_all for start_date=%s, end_date=%s and exp_name=%s'...
        ,start_date,end_date,char(exp_name));
    error_message_2	= sprintf('\nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.\n');
    error_message	= strcat(error_message_1,error_message_2);
    lprintf_log(error_message);
    return;
end

if (~(isempty(error_message)))
    % PROBABLY: Missing filters: I.E. all empty
    if (request_done_ok_flag == 0)
        error_message = strcat(error_message,' Some Exception happened. flag: request_done_ok_flag is zero: i.e. no_completion');
    end
    lprintf_log(error_message);
    return;
end

if ( (isempty(exp_list) ) || (size(exp_list,1) ==0) || (strcmp(class(exp_list),'double')) )
    % NO EXPERIMENTS FOUND: No error but nothing found
    error_message       = 'warning_:no_experiments_found';
    if (request_done_ok_flag == 0)
        error_message = strcat(error_message,' Some Exception happened. flag: request_done_ok_flag is zero: i.e. no_completion');
    end
    lprintf_log(error_message);
    return;
end

if ( (isfield(exp_list,'id')) && (isfield(exp_list,'state')) && (isfield(exp_list,'control')) )
    % OK RESPONSE FROM THE WEB SERVICE: API exp list
else
    error_message = lprintf_log('\nERROR_ RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE: webread. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.');
    return;
end

% TRANSFORM DATA SO IT CAN BE CONSUMED AS A TABLE WITH THE FIELDS THAT WE CARE FOR.

exp_list_cel   = struct2cell(exp_list);
exp_list_cel   = exp_list_cel';

% build list of experiment names:
exp_cnt         = size(exp_list_cel,1);
exper           = cell(exp_cnt,1);
%instr          = cell(exp_cnt,1);
carri           = cell(exp_cnt,1);
spotl           = cell(exp_cnt,1);
kitlo           = cell(exp_cnt,1);
proto           = cell(exp_cnt,1);
%dates          = cell(exp_cnt,1);
%state          = cell(exp_cnt,1);
%futur           = cell(exp_cnt,1);
%expid          = cell(exp_cnt,1);

senso           = cell(exp_cnt,1); % to hold sensor level data
scan            = cell(exp_cnt,1); % to hold scan   level data

% hardcoded_columns: From api_hard_coded_columns_returned in the output cell.

ndxdb           = 1:1:exp_cnt;
ndxdb           = ndxdb';
instr           = exp_list_cel(:,5);
%                                                           10     19
%                                                 1234567890 23456789
dates_temp_1 	= cell2mat(exp_list_cel(:,8));  % 2018-01-25T17:05:16.006-08:00  % change: 2018_02_22 jas_temp jas_dbg ask Dinakar about -8:00
dates_temp_1    = dates_temp_1(:,1:19);
dates_temp_2    = replace(string(dates_temp_1),'T',' ');

formatOut      	= 'mm/dd/yyyy HH:MM:SS pm';                 % API requies this format: from Dinakar 2018_jan_28
dates           = cellstr(datestr(datenum(char(dates_temp_2)),formatOut)); % bug_fix add: cellstr so table creation ok 2018_01_31
state           = exp_list_cel(:,2);
expid           = exp_list_cel(:,1);
proid           = exp_list_cel(:,4);
futur           = ndxdb;                       % just put something, so it is not empty


% jas_here_thurs : to get protocol name: call protocol details for each unique prodid and update all the protocol names.
% steps: categorical / unique / for each unique: get details. then update names

% fake a prodid for staging testing (because it errors out:
% if (~(isempty(strfind(url_exp_det_str,'staging'))))
%     % CONNECTED TO STAGING: Fake the protocol id: Bug in the db to have a bad protocol.
%     for cur_prot_ndx = 1:1:exp_cnt
%         if strcmp(proid{cur_prot_ndx},'4cdbc37c-3325-443c-a903-b55d70cbdbdd')
%             proid{cur_prot_ndx} = '40d23e81-a796-4f10-9c89-f3a6d02c04ad';
%         end
%     end
% end

for cur_exp_ndx=1:1:exp_cnt
    exper{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.experiment_name;
    carri{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.carrier_serial_number;
    spotl{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.spotting_lot_number;
    kitlo{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.lot_number;
end
expe_req_table      = table(ndxdb,exper,instr,carri,spotl,kitlo,proto,dates,state,futur,expid,proid,senso,scan);
lprintf_log('\nExperiments found count = %-4d DAY_RESOLUTION\n',height(expe_req_table));
end    % fn sa_rest_exp_get_all

% REMOVE ROWS WITH EMPTY FIELDS: jas_temp: I do not remeber how I was getting rid or why I was doing it: removing empty rows ! so I commented out:
% exper_empty_ndx is not defined before using it !!!!! big_mistery !!!!!
% if sum(isempty(exper))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(exper),:);
% end
% if sum(isempty(instr))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(instr),:);
% end
% if sum(isempty(carri))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(carri),:);
% end
% if sum(isempty(spotl))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(spotl),:);
% end
% if sum(isempty(kitlo))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(kitlo),:);
% end



%