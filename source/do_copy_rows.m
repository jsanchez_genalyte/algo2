function [ handles ] = do_copy_rows( handles)
%DO_COPY_ROWS copies one or more rows from the TOF
%   Accounts from the dummy empty row at the beginning.

get(handles.ui_table_tos,'Selected');           % returns on or off.    Selection status of uitable
get(handles.ui_table_tos,'SelectionHighlight'); % returns on or off.  Ability to highlight selection handles

% Retrieve the current table_of_fits from the ui:
table_of_fits               = get(handles.ui_table_tos,'Data');
%table_of_fits_spare_row_cnt = handles.table_of_fits_spare_row_cnt;

row_to_copy_start            = handles.ui_state_struct.tos_cur_selection(1);
row_to_copy_end              = handles.ui_state_struct.tos_cur_selection(2);

if ( get(handles.ui_table_tos,'Selected') )
    % SOMETHING IS SELECTED: Copy it
    
%     if (row_to_copy_start == table_of_fits_spare_row_cnt)
%         % User selected the spare one: Return without doing anything.
%         return;
%     end
    
    % USER SELECTED A REAL ROW: Copy  it:
    % First un-select the tof
    %set(handles.ui_table_tos,'Selected','off');
    table_of_fits_to_clipboard = table_of_fits(row_to_copy_start:row_to_copy_end,:);
    % It does not work:  fancyClipboard(table_of_fits_to_clipboard);
    mat2clip(table_of_fits_to_clipboard);
    %old_way:  set(handles.pushbutton_plot,       'Visible','on','Enable','on');  % update_main_plot_button
    % new_way: jas_tbdone what neds to be done here
end



end % fn do_copy_rows

%leftovers to be deleted JAS

%     tof_part_1 =  table_of_fits_to_clipboard(:,4:9 );
%     tof_part_2 =  table_of_fits_to_clipboard(:,11:16 );
%     % Replace nans with zeros:
%     for i=1:1:size(tof_part_2,1)
%         for j=1:1:size(tof_part_2,2)
%             if (isnan(tof_part_2{i,j}))
%                 tof_part_2{i,j} = 0;
%             end
%         end
%     end
