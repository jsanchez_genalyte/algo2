function [ local_hd_struct,error_message ] = excel_save_qc_rpt_detail( local_hd_struct,qc_rpt_detail_table )
%EXCEL_SAVE_QC_RPT_DETAIL saves to an excel tab a QC detail rpt with the contents of the qc_detail_table
%   The contents of the table may be raw or normalized 
%   The name of the excel file is whatever comes in local_hd_struct (with optical)
%   Input:
%   local_hd_struct        file_names and tab_names. 
%   qc_rpt_detail_table    table identical to the qc rpt detail from jervis with data for all requested reports.
error_message = '';
% qc_rpt_detail_header  = {'sensor_type','experiment','run_date','sample_id','analyte','channel','chip','sensor_number','is_valid','is_outlier','gru_shift','instrument','carrier_serial_number','spotting_lot_number','protocol','tracking_id','baseline_start','baseline_duration','amplified_start','amplified_duration','guid'};

local_hd_struct.qc_rpt_detail_sheet = 'QC_REPORT_DETAIL';  % set the last one with a user friendly name: Always overwrite.

% HAVE THE TABLE TO WRITE TO THE EXCEL TAB: set_excel_table
    if (~(isempty(local_hd_struct.qc_rpt_detail_sheet)))
        % HAVE A SHEET TO WRITE TO: Go for it
        % ndx =  (qc_rpt_detail_table.sensor_type == 2) || (qc_rpt_detail_table.sensor_type == 3)
        % write:  qc_rpt_detail_header(ndx,2:end);        
        writetable(qc_rpt_detail_table,local_hd_struct.local_hd_excel_scan_set_file,'Sheet',local_hd_struct.qc_rpt_detail_sheet);
        fprintf('\n _writing_file: %-s \n _sheet_:          %-s \nDONE\n',local_hd_struct.local_hd_excel_scan_set_file,local_hd_struct.qc_rpt_detail_sheet); 
    end   
end % excel_save_qc_rpt_detail

