function [ handles ] = ui_do_plot_sens_cur(hObject, handles )
%UI_DO_PLOT_SENS_CUR plots a set of sensograms in either or both of the good/anoma axes. 
%   uses ui_state_struct.senso_start_ndx.
%input_parameters_struct	= handles.input_parameters_struct;
env_std_cnt                 = 1.5; % jas_temp_hardcoded

ui_state_struct           	= handles.ui_state_struct;
ui_cur_fit_net_ndx       	= ui_state_struct.ui_cur_fit_net_ndx;
ui_request_flag             = ui_state_struct.ui_request_flag;
db_cur_fit_net_len          = sum(ui_state_struct.db_cur_fit_net_ndx); % this is the amout of available total plots for the current selection of the db_data_set
senso_curves_perp_cnt   	= ui_state_struct.senso_curves_perp_cnt; %  4; % rows_to_plot_cnt ; %
% determine total_plot_cnt:
plot_total_cnt              = idivide( int32(db_cur_fit_net_len)            ,int32(senso_curves_perp_cnt),'floor'); %  % 96/ 4 = 24
% determine current plot_number:
plot_cur_num                = idivide(int32(ui_state_struct.senso_start_ndx),int32(senso_curves_perp_cnt),'ceil');

%popup_analyte_sel_ndx       = get(handles.popup_menu_reference_material,   'Value');
%popup_candidate_sel_ndx     = get(handles.popup_menu_reference_candidate,  'Value');
%popup_fit_type_sel_ndx      = get(handles.popup_menu_reference_fit,        'Value');

% determine which sensograms need to be: classified and then plotted: build index of:
%   ui_state_struct.cur_good_plot_ndx, and  ui_state_struct.cur_anom_plot_ndx.
% Start from the current db_data_set, use exiting filters( from the pulldown menus), and buttons etc.

senso_actually_ploted_good	= 0;
senso_actually_ploted_anom	= 0;
% green_nice                  = [ 27 79 53 ]; % rgb lite [ 59 113 86 ];  rgb med:  [  193 221 198 ]   rgb dark: [ 27 79 53  ]
% green_nice                  = green_nice /255;  % normalized rgb 

% Build the ndx to select data to plot: a chunk at a time. i.e. a block.
% plot current block of curves.
if strcmpi(ui_request_flag,'g_only')                                                                        % CASE_1  g_only
    % PLOTTING GOOD SENSOGRAMS ONLY
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx_good))
        % THERE IS no GOOD SENSOGRAMS TO PLOT: Clear the good axes
        ui_create_plot_senso_block(handles.axes_good_sens,nan,nan...
            ,nan,nan,nan                                    ...  % y_data_enve,y_data_mean,y_data_median
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,' GOOD SENSOGRAMS:   0'                        ...
            ,nan                                            ...
            ,ui_state_struct.color_green_nice);
    else
        % THERE ARE good SENSOGRAMS TO PLOT: Plot them
        senso_start_ndx                 = ui_state_struct.ui_cur_fit_net_ndx_good(1);
        senso_end_ndx                   = ui_state_struct.ui_cur_fit_net_ndx_good(end);
        senso_actually_ploted_good      = length(ui_state_struct.ui_cur_fit_net_ndx_good); %   senso_end_ndx - senso_start_ndx +1;
        x_data                          = handles.db_data_set_struct.scan_raw_time{1}(ui_state_struct.ui_cur_fit_net_ndx_good,:);  % _anom
        if (strcmpi('Tr',ui_state_struct.probe{1}.list_item_names{1}))
            % PLOTTING TRS:
            y_data                          = handles.db_data_set_struct.scan_raw_tr_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_good,:);            
        else
            y_data                          = handles.db_data_set_struct.scan_raw_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_good,:);
        end
        y_data_mean         = nan;
        y_data_median    	= nan;
        y_data_enve         = nan;
        if ( handles.checkbox_good_sens_mea.Value  )
            y_data_mean     = nanmean(y_data,1);
        end
        if ( (handles.checkbox_good_sens_med.Value)   )
            y_data_median 	= nanmedian(y_data,1);
        end  
        if (handles.checkbox_good_sens_env.Value)
            y_data_enve_top     = nanmean(y_data,1) +  (env_std_cnt * std(y_data,0,1,'omitnan')); 
            y_data_enve_btm     = nanmean(y_data,1) -  (env_std_cnt * std(y_data,0,1,'omitnan'));
            y_data_enve         = [ y_data_enve_top ; y_data_enve_btm];
        end        
        title_str                       = strcat(' GOOD SENSOGRAMS:  ',(strrep(sprintf(' %-4d  Plot: ( %-4d / %-4d )  ( %-4d - %-4d / %-4d )'...
            ,senso_actually_ploted_good, plot_cur_num,plot_total_cnt,senso_start_ndx,senso_end_ndx,db_cur_fit_net_len),'  ',' ')));
        title_str                       = strrep(title_str,'  ',' ');
        legend_str                      = nan;
        ui_create_plot_senso_block(handles.axes_good_sens,x_data,y_data...
            ,y_data_enve,y_data_mean,y_data_median          ...
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,title_str                                      ...
            ,legend_str                                     ...
            ,ui_state_struct.color_green_nice);
        % Update the Sensograms per plot edit text on the screen only: for UI consistency:
        % if (senso_actually_ploted_good ~= senso_curves_perp_cnt)
        %	set(handles.edit_senso_per_plot,'String',int2str(senso_actually_ploted_good));
        % end
    end % something to plot: good
end % 'g_only'

if strcmpi(ui_request_flag,'a_only')                                                                        % CASE_2  a_only
    % PLOTTING ANOM SENSOGRAMS ONLY
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx_anom))
        % THERE IS no ANOM SENSOGRAMS TO PLOT: Clear the anom axes
        ui_create_plot_senso_block(handles.axes_anom_sens,nan,nan...
            ,nan,nan,nan                                    ...  % no_y_data_enve no_y_data_mean,y_data_median
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,' ANOM SENSOGRAMS:   0'                        ...
            ,nan                                            ...
            ,'red');
    else
        % THERE ARE some ANOM SENSOGRAMS TO PLOT: Plot them
        senso_start_ndx                 = ui_state_struct.ui_cur_fit_net_ndx_anom(1);
        senso_end_ndx                   = ui_state_struct.ui_cur_fit_net_ndx_anom(end);
        senso_actually_ploted_anom      = length(ui_state_struct.ui_cur_fit_net_ndx_anom); %
        x_data                          = handles.db_data_set_struct.scan_raw_time{1}(ui_state_struct.ui_cur_fit_net_ndx_anom,:); % _anom
        if (strcmpi('Tr',ui_state_struct.probe{1}.list_item_names{1}))
            % PLOTTING TRS:
            y_data                          = handles.db_data_set_struct.scan_raw_tr_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_anom,:);            
        else
            y_data                          = handles.db_data_set_struct.scan_raw_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_anom,:);
        end
        y_data_mean         = nan;
        y_data_median    	  = nan;
        %y_data_enve         = nan;
        if ( handles.checkbox_good_sens_mea.Value )
            y_data_mean     = nanmean(y_data,1);
        end
        if ( (handles.checkbox_good_sens_med.Value)   )
            y_data_median 	= nanmedian(y_data,1);
        end  
        if (handles.checkbox_good_sens_env.Value)
            % y_data_enve_top     = nanmean(y_data,1) +  (env_std_cnt * std(y_data,0,1,'omitnan')); 
            % y_data_enve_btm     = nanmean(y_data,1) -  (env_std_cnt * std(y_data,0,1,'omitnan'));
            % y_data_enve         = [ y_data_enve_top ; y_data_enve_btm];
        end        
        title_str                       = strcat(' ANOM SENSOGRAMS:  ',(strrep(sprintf(' %-4d  Plot: ( %-4d / %-4d )  ( %-4d - %-4d / %-4d )'...
            ,senso_actually_ploted_anom, plot_cur_num,plot_total_cnt,senso_start_ndx,senso_end_ndx,db_cur_fit_net_len),'  ',' ')));
        title_str                       = strrep(title_str,'  ',' ');        
        legend_str                      = nan;
        ui_create_plot_senso_block(handles.axes_anom_sens,x_data,y_data...
            ,nan,y_data_mean,y_data_median                  ...  % no_enve
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,title_str                                      ...
            ,legend_str                                     ...
            ,'red');
        % Update the Sensograms per plot edit text on the screen only: for UI consistency:
        if (senso_actually_ploted_anom ~= senso_curves_perp_cnt)
            set(handles.edit_senso_per_plot,'String',int2str(senso_actually_ploted_anom));
        end
    end % something to plot
end % 'a_only'

if strcmpi(ui_request_flag,'g_n_a')                                                                        % CASE_3  g_n_a
    % PLOTTING GOOD AND ANOM SENSOGRAMS:
    % S3_1: Plot the good senso on the good side:
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx_good))
        % THERE IS no GOOD SENSOGRAMS TO PLOT: Clear the good axes
        ui_create_plot_senso_block(handles.axes_good_sens,nan,nan...
            ,nan,nan,nan                                    ...  % y_data_enve,y_data_mean,y_data_median
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,' GOOD SENSOGRAMS:   0'                        ...
            ,nan                                            ...
            ,ui_state_struct.color_green_nice);
    else
        % THERE ARE good SENSOGRAMS TO PLOT: Plot them      
        senso_start_ndx                 = ui_state_struct.ui_cur_fit_net_ndx_good(1);
        senso_end_ndx                   = ui_state_struct.ui_cur_fit_net_ndx_good(end);
        senso_actually_ploted_good      = length(ui_state_struct.ui_cur_fit_net_ndx_good);
        x_data                          = handles.db_data_set_struct.scan_raw_time{1}(ui_state_struct.ui_cur_fit_net_ndx_good,:);
        
        if (strcmpi('Tr',ui_state_struct.probe{1}.list_item_names{1}))
            % PLOTTING TRS:
            y_data       	= handles.db_data_set_struct.scan_raw_tr_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_good,:);
        else
            y_data         	= handles.db_data_set_struct.scan_raw_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_good,:);
        end
        y_data_mean         = nan;
        y_data_median    	= nan;
        y_data_enve         = nan;
        if ( handles.checkbox_good_sens_mea.Value  )
            y_data_mean     = nanmean(y_data,1);
        end
        if ( (handles.checkbox_good_sens_med.Value)   )
            y_data_median 	= nanmedian(y_data,1);
        end  
        if (handles.checkbox_good_sens_env.Value)
            y_data_enve_top     = nanmean(y_data,1) +  (env_std_cnt * std(y_data,0,1,'omitnan')); 
            y_data_enve_btm     = nanmean(y_data,1) -  (env_std_cnt * std(y_data,0,1,'omitnan'));
            y_data_enve         = [ y_data_enve_top ; y_data_enve_btm];
        end
        %y_data                         = y_data( senso_start_ndx:senso_end_ndx,:);
        title_str                       = strcat(' GOOD SENSOGRAMS:  ',(strrep(sprintf(' %-4d  Plot: ( %-4d / %-4d )  ( %-4d - %-4d / %-4d )'...
            ,senso_actually_ploted_good, plot_cur_num,plot_total_cnt,senso_start_ndx,senso_end_ndx,db_cur_fit_net_len),'  ',' ')));
        title_str                       = strrep(title_str,'  ',' ');
        legend_str                      = nan;
        ui_create_plot_senso_block(handles.axes_good_sens,x_data,y_data...
            ,y_data_enve,y_data_mean,y_data_median          ...  
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,title_str                                      ...
            ,legend_str                                     ...
            ,ui_state_struct.color_green_nice);
        
        % Update the Sensograms per plot edit text on the screen only: for UI consistency:
        % if (senso_actually_ploted_good ~= senso_curves_perp_cnt)
        %	set(handles.edit_senso_per_plot,'String',int2str(senso_actually_ploted_good));
        % end
    end % something to plot: good
    % S3_2: find out the ndx of the anom and plot them.
    if (isempty(ui_state_struct.ui_cur_fit_net_ndx_anom))
        % THERE IS no ANOM SENSOGRAMS TO PLOT: Clear the anom axes
        ui_create_plot_senso_block(handles.axes_anom_sens,nan,nan...
            ,nan,y_data_mean,y_data_median                  ...  % no_y_data_enve,y_data_mean,y_data_median   no envelope for anom
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,' ANOM SENSOGRAMS:   0'                        ...
            ,nan                                            ...
            ,'red');
    else
        % THERE ARE some ANOM SENSOGRAMS TO PLOT: Plot them
        senso_start_ndx                 = ui_state_struct.ui_cur_fit_net_ndx_anom(1);
        senso_end_ndx                   = ui_state_struct.ui_cur_fit_net_ndx_anom(end);
        senso_actually_ploted_anom   	= length(ui_state_struct.ui_cur_fit_net_ndx_anom);        
        x_data                        = handles.db_data_set_struct.scan_raw_time{1}(ui_state_struct.ui_cur_fit_net_ndx_anom,:);
        if (strcmpi('Tr',ui_state_struct.probe{1}.list_item_names{1}))
            % PLOTTING TRS:
            y_data                          = handles.db_data_set_struct.scan_raw_tr_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_anom,:);            
        else
            y_data                          = handles.db_data_set_struct.scan_raw_gru{1}(ui_state_struct.ui_cur_fit_net_ndx_anom,:);
        end        
        title_str                       = strcat(' ANOM SENSOGRAMS:  ',(strrep(sprintf(' %-4d  Plot: ( %-4d / %-4d )  ( %-4d - %-4d / %-4d )'...
            ,senso_actually_ploted_anom, plot_cur_num,plot_total_cnt,senso_start_ndx,senso_end_ndx,db_cur_fit_net_len),'  ',' ')));    
        title_str           = strrep(title_str,'  ',' ');
        legend_str        	= nan;
        
        y_data_mean         = nan;
        y_data_median    	= nan;
        %y_data_enve         = nan;
        if ( handles.checkbox_good_sens_mea.Value )
            y_data_mean     = nanmean(y_data,1);
        end
        if ( (handles.checkbox_good_sens_med.Value)   )
            y_data_median 	= nanmedian(y_data,1);
        end  
        if (handles.checkbox_good_sens_env.Value)
            y_data_enve_top     = nanmean(y_data,1) +  (env_std_cnt * std(y_data,0,1,'omitnan')); 
            y_data_enve_btm     = nanmean(y_data,1) -  (env_std_cnt * std(y_data,0,1,'omitnan'));
            %y_data_enve         = [ y_data_enve_top ; y_data_enve_btm];
        end             
        
        ui_create_plot_senso_block(handles.axes_anom_sens,x_data,y_data...
            ,nan,y_data_mean,y_data_median                           ...  % no envelope for anom 
            ,'Time '                                        ...  % x_label_str
            ,' GRU '                                        ...  % y_label_str
            ,title_str                                      ...
            ,legend_str                                     ...
            ,'red');
        % Update the Sensograms per plot edit text on the screen only: for UI consistency:
        if ((senso_actually_ploted_good + senso_actually_ploted_anom) ~= senso_curves_perp_cnt)
            set(handles.edit_senso_per_plot,'String',int2str(senso_actually_ploted_good + senso_actually_ploted_anom));
        end
    end % There is something to plot: anom
    
end % 'g_and_a'
guidata(hObject, handles); % Update handles structure
% save results in handles.
handles.ui_state_struct =ui_state_struct;
end % fn ui_do_plot_sens_cur

