function [handles ] = sc_classify_sensograms(handles)
%sc_classify_sensograms Classifies raw sensograms (future: add argument: raw, normj normm
%     Classifes sensograms located between senso_start_ndx and (senso_start_ndx+senso_curves_perp_cnt - 1)
%     Input:
%       input_parameters_struct: senso_clasi thesholds               to decide how to classify
%       senso_start_ndx                                              to decide where to start classi: relative to: db_cur_fit_net_ndx
%       senso_curves_perp_cnt                                        to decide when to stop clasi
%       db_cur_fit_net_ndx                                           to decide which ones to classify
%       ui_state_struct.ui_request_flag: 'g_n_a','g_only','a_only' } to decide how many senso need to be classified
%
%     Output:
%       1_  db_data_set_struct.db_metad_set_struct.clasi gets updated on the corresp classif results.
%       2_  db_data_set_struct.sc_clasi_flag
%       3_  db_data_set_struct.sc_clasi_vals
% 3 cases handled:
%   case_1: (good visi and anom visi): For loop: exact amount (split in goo and bad)
%   case_2: (good visi and anom hide): While loop: until find enough good, save classi of good, but ignore anom cnts
%   case_3: (good hide and anom visi): While loop: until find enough anom, save classi of anom, but ignore good cnts
%
%   common to the 3 cases:
%           all use:     senso_start_ndx ands enso_curves_perp_cnt
%                        all do classification only for  cases where the senso row is 'U' For unknown
%           all set:     part or all of the clasi array 

% see: sa_init_cells_and_structs for definition of sc_clasi_flag and sc_clasi_vals
fake_clasi_debug_fg   	= 1;    % 1   == fake classification all results are 'G'  0: DO NOT FAKE: Run clasi algo FOR REAL. 
fake_clasi_res          = 'G';  % 'G' == all clasi as good. 'A' == Alll clasi as Anom
ui_request_flag       	= handles.ui_state_struct.ui_request_flag;
db_cur_fit_net_ndx      = handles.ui_state_struct.db_cur_fit_net_ndx;
prot_req_table          = handles.db_data_exp_struct.prot_req_table;
proid_exper_list        = handles.db_data_exp_struct.expe_req_table.proid; % list of protocol ids for all experiment
recipie_stept_new       = prot_req_table.stept;        

%display(sprintf(' Start time_clasi = %s ',datestr(now)));
set(handles.text_status,'string','Classifying Sensograms       ....','ForegroundColor','blue','Fontsize',12); %'blue'
wanted_ip_cnt         	= handles.input_parameters_struct.wanted_inflexion_point_count;
min_pos_delta_th_hor_pct= handles.input_parameters_struct.min_pos_delta_th_hor_pct; % 0.02;
min_pos_delta_th_amp_pct= handles.input_parameters_struct.min_pos_delta_th_amp_pct; %  0.1;

db_data_set_struct      = handles.db_data_set_struct;
% setup for the 3 cases:
senso_start_ndx       	= handles.ui_state_struct.senso_start_ndx;
senso_curves_perp_cnt 	= handles.ui_state_struct.senso_curves_perp_cnt;
included_indices        = find(db_cur_fit_net_ndx);
included_len        	= length(included_indices);
senso_end_ndx         	= min( (senso_start_ndx + senso_curves_perp_cnt - 1),included_len);

clasi                   = db_data_set_struct.db_metad_set_struct.clasi; % on top
sc_clasi_flag           = db_data_set_struct.sc_clasi_flag; % (db_cur_fit_net_ndx,:);
sc_clasi_vals         	= db_data_set_struct.sc_clasi_vals; % (db_cur_fit_net_ndx,:);
sc_clasi_flag_struct    = db_data_set_struct.sc_clasi_flag_struct;
sc_clasi_vals_struct    = db_data_set_struct.sc_clasi_vals_struct;

ui_cur_fit_net_ndx_good = [];
ui_cur_fit_net_ndx_anom = []; % to plot them simultaneously or separatedly:  to save indices of the found anom

% create arrays of the proper size:
ui_cur_fit_net_ndx      = zeros(senso_end_ndx-senso_start_ndx+1,1); % to plot them simultaneously                  to save indices of the found all
ui_cur_fit_net_ndx_good = zeros(senso_end_ndx-senso_start_ndx+1,1); % to plot them simultaneously or separatedly:  to save indices of the found good
ui_cur_fit_net_ndx_anom = zeros(senso_end_ndx-senso_start_ndx+1,1); % to plot them simultaneously or separatedly:  to save indices of the found anom


% cur_cats_cnt            = countcats (unique(clasi)); % vs countcats
% if (cur_cats_cnt == 1)
%     % ALL CLASSIFICATIONS ARE 'U': IE UNKNOWN
%     % add a few known classification categories
%     clasi_new = addcats(clasi,              ...
%         {                                   ...
%         'A'                                 ...
%         'G'                                 ...
%         },'Before','U');
%     clasi  = clasi_new;
% end
% Grab only what db_cur_fit_net_ndx allows:
scan_raw_time           = db_data_set_struct.scan_raw_time{1};       % 2D matrix with scan data:
scan_raw_gru            = db_data_set_struct.scan_raw_gru{1};     	 % 2D matrix
scan_raw_len            = db_data_set_struct.scan_raw_len{1};     	 % 1D matrix
% scan_raw_time           = scan_raw_time(logical(db_cur_fit_net_ndx),:); no need to copy the scan data: use the correct ndx
% scan_raw_gru           	= scan_raw_gru( logical(db_cur_fit_net_ndx),:);
% scan_raw_len            = scan_raw_len( logical(db_cur_fit_net_ndx),:);

% scan_raw_tr_gru         = db_data_set_struct.scan_raw_tr_gru{1};     % 2D matrix
% scan_nrm_gru         	= db_data_set_struct.scan_nrm_gru{1};          % 2D matrix
% scan_nrm_gru_matlab     = db_data_set_struct.scan_nrm_gru_matlab{1}; % 2D matrix

% % %jas_2018: new way to get protocol info: from API we get:  complete these steps using code from sc_fill_in_recipe_and_analysis_time
% % recipe       =  struct2table(protocol_struct.recipe.steps);
% % duration_sec = (cell2matdouble(recipe.duration) ) / 60; %     60    30   180   120   210    60    15 in  seconds: % to convert sec to minutes

%2018_jas_tbd: gra recipie duration from:
% stept = db_data_set_struct.prot_req_table.stept;   proti. see: sa_do_push_req_expe.m   stept.dura_sec

old_qc_protocol = 1;  %JAS_HARD_CODED TBD: MAKE_A_CFG_PARAMETER.
if (old_qc_protocol)
    % OLD QC PROTOCOL_TEMPORAL_JAS_TEMP_JAS_HARDCODED
    %
    %                     ACC    0      0.5      3       4.5        8.0    9(made up)
    %                         Iwsh    Swsh    BL      AmpS        AmP
    %                            1   2      3       4       5           6(madeup)
    recipe.step.duration  = [  0.5    2.5     1.5     3.5         1.0];
else
    % NEW QC PROTOCOL_TEMPORAL_JAS_TEMP_JAS_HARDCODED
    %
    %                     ACC    0      0.5     2.5  	4.5     8.0    9.0
    %                         Iwsh    Swsh    BL      AmpS    AmP     DonCare
    %                            1   2      3       4       5       6       7
    %recipe.step.duration  = [  0.5	3       2       3.5   	1       1 ];
end

% senso_end_ndx_sel         = included_indices(senso_end_ndx);                              % NIU
clasi_senso_cnt     = 0;
clasi_good_cnt      = 0;
clasi_anom_cnt      = 0;
if (strcmpi(ui_request_flag,'g_n_a'))
    % CASE_1: (GOOD VISI AND ANOM VISI): For loop: exact amount, but split in good and bad. _____________
    for cur_senso_ndx =senso_start_ndx:1:senso_end_ndx                                      % __________ 1_CLASI FOR LOOP __________
        cur_senso_ndx_sel = included_indices(cur_senso_ndx);
        clasi_senso_cnt = clasi_senso_cnt+1;
        if (~(mod(clasi_senso_cnt,10)))
            set(handles.text_status,'string',sprintf('Classifying Sensograms  %-3d  ....',clasi_senso_cnt),'ForegroundColor','blue','Fontsize',12); %'blue'            
        end
        clasi_res = clasi(cur_senso_ndx_sel);
        if (clasi_res(1) =='U')
            % SENSOGRAM HAD NOT BEEN CLASSIFIED:
            if (fake_clasi_debug_fg)
                % FAKING CLASSIFICATION
                clasi_res     = fake_clasi_res; % 'G'; 
                sc_clasi_flag = nan;
                sc_clasi_vals = nan;
            else
                % REAL CLASSIFICATION
                % ret the protocolid of the sensor and the get the steps for this protocol jas_tbd (note the tos need to ve a protocolid column)
                recipie_stept_new = handles.db_data_exp_struct.prot_req_table; % here I need to extract only the one for the current experiment being classified
                
                [ ~,~,~                                                             ...
                    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt,senso_flag_array_adrsq,sc_clasi_flag] =	...
                    sc_classify_sensogram_process( recipie_stept_new,recipe...
                    ,scan_raw_time(cur_senso_ndx_sel,1:scan_raw_len(cur_senso_ndx_sel))'  ... % time_raw_matrix(1:curve_length,cur_curve,cur_senso_ndx_sel) ... %     time_array,
                    ,scan_raw_gru( cur_senso_ndx_sel,1:scan_raw_len(cur_senso_ndx_sel))'  ... % shift_matrix(1:curve_length,cur_curve,cur_senso_ndx_sel)    ... %     shift_array ...
                    ,wanted_ip_cnt,min_pos_delta_th_hor_pct,min_pos_delta_th_amp_pct ...
                    ,cur_senso_ndx_sel,sc_clasi_flag_struct,sc_clasi_flag ); % jas_tbd pass as parameter: ,sc_clasi_vals
                
                [clasi_res,sc_clasi_flag,sc_clasi_vals  ] = sc_eval_flags(cur_senso_ndx_sel,senso_flag_array_adrsq,clasi_res,sc_clasi_flag,sc_clasi_vals ...
                    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt ...
                    ,sc_clasi_flag_struct,sc_clasi_vals_struct);
            end % real classification
            handles.db_data_set_struct.db_metad_set_struct.clasi(cur_senso_ndx_sel) = {clasi_res };
        end % senso had not been classified
        % Save the indices of the locations of the good and anom:
        ui_cur_fit_net_ndx(clasi_senso_cnt) = cur_senso_ndx_sel;
        if (clasi_res(1) == 'G')
            clasi_good_cnt= clasi_good_cnt +1;
            % Save the indices of the locations of the good:
            ui_cur_fit_net_ndx_good(clasi_good_cnt) = cur_senso_ndx_sel;
        end
        if (clasi_res(1) == 'A')
            clasi_anom_cnt= clasi_anom_cnt +1;
            % Save the indices of the locations of the good:
            ui_cur_fit_net_ndx_anom(clasi_anom_cnt) = cur_senso_ndx_sel;
        end        
    end %  for loop: cur_senso_ndx for case_1
end  % case 1 'g_n_a'
if (strcmpi(ui_request_flag,'g_only'))
    % CASE_2: (GOOD VISI AND ANOM HIDE): While loop: until find enough good, save classi of good, but ignore anom cnts _____________
    clasi_good_cnt      = 0;
    cur_senso_ndx       = senso_start_ndx;
    cur_senso_ndx_sel   = included_indices(senso_start_ndx);
    while cur_senso_ndx_sel <= included_indices(end)                                % __________ 2_CLASI WHIE LOOP __________
        clasi_res       = clasi(cur_senso_ndx_sel);
        if (clasi_res(1) =='U')
            % SENSOGRAM HAD NOT BEEN CLASSIFIED:
            if (fake_clasi_debug_fg)
                % FAKING CLASSIFICATION
                clasi_res     = fake_clasi_res;
                sc_clasi_flag = nan;
                sc_clasi_vals = nan;
            else
                [ ~,~,~                                                             ...
                    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt,senso_flag_array_adrsq] =	...
                    sc_classify_sensogram_process( recipe                          ...
                    ,scan_raw_time(cur_senso_ndx_sel,1:scan_raw_len(cur_senso_ndx_sel))'    ... % time_raw_matrix(1:curve_length,cur_curve,cur_senso_ndx_sel) ... %     time_array,
                    ,scan_raw_gru( cur_senso_ndx_sel,1:scan_raw_len(cur_senso_ndx_sel))'    ... % shift_matrix(1:curve_length,cur_curve,cur_senso_ndx_sel)    ... %     shift_array ...
                    ,wanted_ip_cnt,min_pos_delta_th_hor_pct,min_pos_delta_th_amp_pct ...
                    ,cur_senso_ndx_sel,sc_clasi_flag_struct,sc_clasi_flag ); % jas_tbd pass as parameter: ,sc_clasi_vals
                
                [clasi_res,sc_clasi_flag,sc_clasi_vals] = sc_eval_flags(cur_senso_ndx_sel,senso_flag_array_adrsq,clasi_res,sc_clasi_flag,sc_clasi_vals ...
                    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt...
                    ,sc_clasi_flag_struct,sc_clasi_vals_struct);                                                                                                      
            end % real classification
            handles.db_data_set_struct.db_metad_set_struct.clasi(cur_senso_ndx_sel ) ={ clasi_res};            
            if (clasi_res(1) == 'G')
                clasi_good_cnt= clasi_good_cnt +1;
                % Save the indices of the locations of the good:
                ui_cur_fit_net_ndx_good(clasi_good_cnt) = cur_senso_ndx_sel;
                if (~(mod(clasi_good_cnt,10)))
                    set(handles.text_status,'string',sprintf('Classifying GOOD Sensograms  %-3d  ....',clasi_good_cnt),'ForegroundColor','blue','Fontsize',12); %'blue'
                end
            end
            if (clasi_good_cnt >= senso_curves_perp_cnt)
                % FOUND AS MANY AS WANTED: get out
                break;
            end
        else
            % SENSO HAS BEEN CLASIFIED
            if (clasi_res(1) == 'G')
                clasi_good_cnt= clasi_good_cnt +1;
                % Save the indices of the locations of the good:
                ui_cur_fit_net_ndx_good(clasi_good_cnt) = cur_senso_ndx_sel;
                if (~(mod(clasi_good_cnt,10)))
                    set(handles.text_status,'string',sprintf('Classifying GOOD Sensograms  %-3d  ....',clasi_good_cnt),'ForegroundColor','blue','Fontsize',12); %'blue'
                end
            end
        end % senso has not been clasified
        
        if ( cur_senso_ndx_sel == included_indices(end) )
            % AT THE END: Get out to avoid ndx out of bounds.
            break;
        end
        cur_senso_ndx       = cur_senso_ndx+1; % go for the next senso
        cur_senso_ndx_sel   = included_indices(cur_senso_ndx);
    end %  while loop: cur_senso_ndx
end % case 2

if (strcmpi(ui_request_flag,'a_only'))
    % CASE_3: (GOOD HIDE AND ANOM VISI): While loop: until find enough anom, save classi of anom, but ignore good cnts
    clasi_anom_cnt          = 0;
    cur_senso_ndx     = senso_start_ndx;
    cur_senso_ndx_sel = included_indices(senso_start_ndx);
    while cur_senso_ndx_sel <= included_indices(end)                                % __________ 3_CLASI WHIE LOOP __________
        clasi_res = clasi(cur_senso_ndx_sel);
        if (clasi_res(1) =='U')
            % SENSOGRAM HAD NOT BEEN CLASSIFIED:
            if (fake_clasi_debug_fg)
                % FAKING CLASSIFICATION
                clasi_res     = fake_clasi_res;
                sc_clasi_flag = nan;
                sc_clasi_vals = nan;
            else
                [ ~,~,~                                                             ...
                    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt,senso_flag_array_adrsq] =	...
                    sc_classify_sensogram_process( recipe                           ...
                    ,scan_raw_time(cur_senso_ndx_sel,1:scan_raw_len(cur_senso_ndx_sel))'    ... % time_raw_matrix(1:curve_length,cur_curve,cur_senso_ndx_sel) ... %     time_array,
                    ,scan_raw_gru( cur_senso_ndx_sel,1:scan_raw_len(cur_senso_ndx_sel))'    ... % shift_matrix(1:curve_length,cur_curve,cur_senso_ndx_sel)    ... %     shift_array ...
                    ,wanted_ip_cnt,min_pos_delta_th_hor_pct,min_pos_delta_th_amp_pct ...
                    ,cur_senso_ndx_sel,sc_clasi_flag_struct,sc_clasi_flag ); % jas_tbd pass as parameter: ,sc_clasi_vals                
                
                [clasi_res,sc_clasi_flag,sc_clasi_vals  ] = sc_eval_flags(cur_senso_ndx_sel,senso_flag_array_adrsq,clasi_res,sc_clasi_flag,sc_clasi_vals ...
                    ,ip_sec_ndx_swash,found_swash_ip_cnt,ip_sec_ndx_amp,found_amp_ip_cnt...
                    ,sc_clasi_flag_struct,sc_clasi_vals_struct);
            end % real classification
            handles.db_data_set_struct.db_metad_set_struct.clasi(cur_senso_ndx_sel) = { clasi_res };
            
            if (clasi_res(1) == 'A')
                clasi_anom_cnt= clasi_anom_cnt +1;
                % Save the indices of the locations of the good:
                ui_cur_fit_net_ndx_anom(clasi_anom_cnt) = cur_senso_ndx_sel;                
                if (~(mod(clasi_anom_cnt,10)))
                    set(handles.text_status,'string',sprintf('Classifying GOOD Sensograms  %-3d  ....',clasi_anom_cnt),'ForegroundColor','blue','Fontsize',12); %'blue'
                end                
            end
            if (clasi_anom_cnt >= senso_curves_perp_cnt)
                % FOUND AS MANY AS WANTED: get out
                break;
            end
        else
            % SENSO HAS BEEN CLASIFIED
            if (clasi_res(1) == 'A')
                clasi_anom_cnt= clasi_anom_cnt +1;
                % Save the indices of the locations of the good:
                ui_cur_fit_net_ndx_anom(clasi_anom_cnt) = cur_senso_ndx_sel;
                if (~(mod(clasi_anom_cnt,10)))
                    set(handles.text_status,'string',sprintf('Classifying GOOD Sensograms  %-3d  ....',clasi_anom_cnt),'ForegroundColor','blue','Fontsize',12); %'blue'
                end
            end
        end % senso has not been clasified
        
        if ( cur_senso_ndx_sel == included_indices(end) )
            % AT THE END: Get out to avoid ndx out of bounds.
            break;
        end
        cur_senso_ndx       = cur_senso_ndx+1; % go for the next senso
        cur_senso_ndx_sel   = included_indices(cur_senso_ndx);
    end %  while loop: cur_senso_ndx
end  % case 3
set(handles.text_status,'string','Classifying Sensograms       DONE','ForegroundColor',handles.ui_state_struct.color_green_nice,'Fontsize',12); %'blue'
% Save back results:
% niu handles.db_data_set_struct.db_metad_set_struct.clasi(logical(db_cur_fit_net_ndx))   = clasi;
handles.db_data_set_struct.sc_clasi_flag = sc_clasi_flag; % (db_cur_fit_net_ndx,:)
handles.db_data_set_struct.sc_clasi_vals = sc_clasi_vals; % (db_cur_fit_net_ndx,:)

if( clasi_senso_cnt     > 0)
    % FOUND SOME SENSO:
    handles.ui_state_struct.ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx(1:clasi_senso_cnt); %  save indices of the found senso
else
    handles.ui_state_struct.ui_cur_fit_net_ndx      = [];
end
if( clasi_good_cnt     > 0)
    % FOUND SOME GOOD:
    handles.ui_state_struct.ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good(1:clasi_good_cnt); %  save indices of the found good
else
    handles.ui_state_struct.ui_cur_fit_net_ndx_good = [];
end
if( clasi_anom_cnt     > 0)
    % FOUND SOME ANOM:
    handles.ui_state_struct.ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom(1:clasi_anom_cnt); %  save indices of the found anom
else
    handles.ui_state_struct.ui_cur_fit_net_ndx_anom = [];
end

% display(sprintf(' End   time_clasi = %s ',datestr(now)));
end % % fn sc_classify_sensograms

%         'A1  2  3  41  42 43 44'            ...
%         'A1  2  3  41  42 43 44'            ...
%         'A1  2  3  41  42 43 -44'           ...
%         'A-1 1  2  3  41 42 43 44'          ...
%         'A-1 -2 1  2  3  41 42 43 44'       ...
%         'A-1 -2 2  3  41 42 43 44'          ...
%         'A-2 1  2  3  41 42 43 44'          ...