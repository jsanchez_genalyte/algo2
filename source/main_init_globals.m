function [algo_gb_struct ] = main_algo_init_gb(input_parameters_struct)
% MAIN_ALGO_INIT_GB  is a script to do a one time inizialization of all the globals in this app. % old: MAIN_INIT_GLOBALS
%declare_globals;

% Initialize Globals: ______________________________________________________________________________________________________           
% Define QC recipie steps: RS _____________________________________________________________________________________________
% RS   Step_name          Step Description
% ____ _________________  ______________________________________________
% RS_1_initial_wash       Running Water+salt or ANA buffer. Purpose: To remove debri. and condition the surface of rings & surrounding areas  From RT=0. 
% RS_2_sample_binding     Running Biomarkers or Analytes (like SSB or DNA or IGG or RecombProtein) This is to start attaching sample to ring
% RS_3_sample_wash        Same as RS_1.           Purpose: To remove loosely bound sample to ring. Goal is to leave only material hard bound to top of ring.  
% RS_5_amp_binding        Running Human Anti-IGG  Purpose: To produce binding of Ani-IGG and IGG or other biomarker. THis produces the shift of light. 
% RS_6_amp_binding_end    End running   Anti-IGG  Purpose: To run long enough to allow amplification to complete: Goal slope at end should be ~flat

%       <AnaBuffer>|<-----Analyte------------>|<------AnaBuffer-->|<--------------ANTI-IGG----------------------------->|
% REC:  RS_1       RS_2                       RS_3                RS_4                                                  RS_5
%       |----------|--------------------------|-------------------|-----------------------------------------------------|
% QC:   0          0.5                        3.0                 4.5                                                   8.0
%                                                                4.4   5.0                                        7.7   8.2                      
%       |--------------------------------------------------------|-----|------------------------------------------|------|
% PSA:                                                           PS_1_S PS1_E                                     PS_2_S PS_2_E
%
% Define QC process steps for analysis: PSA ____________________________________________________________________________________
% RS   Step_name          Step Description
% ____ _________________  __________________
% PSA_1_sample_baseline   Part_1 of running Anti-IGG. Region to measure baseline    
% PSA_2_amp_binding       Part_2 of running Anti-IGG. Region to measure amp peak.
%                               rs_1  rs_2   rs_3  rs_4  rs_5            rs==RecipieStep

% G1_recipie structure  rs_ == Recipie Structure. 
%rs_time_minutes               = [0.0   0.4    2.0   4.2   8.0]; % FAKE FOR DEUBG jas_temp JAS_TEMP ***
%rs_time_minutes               = [0.0   0.3    3.0   4.5   8.0]; % fake twick second value only .5 to .3
algo_gb_struct = struct;
algo_gb_struct.rs_time_minutes               = [0.0   0.5    3.0   4.5   8.0]; % REAL source: product: ANA 66060 QC. Lot: 160505


algo_gb_struct.rs_1_initial_wash_ndx         = 1; % 
algo_gb_struct.rs_2_sample_binding_ndx       = 2; % 
algo_gb_struct.rs_3_sample_wash_ndx          = 3; % 
algo_gb_struct.rs_4_sample_binding_start_ndx = 4; % 
algo_gb_struct.rs_5_sample_binfing_end_ndx   = 5; % 
algo_gb_struct.rs_curve_length               = 61;% % checking for length and nan values.  

% G2 process_analysis structure _______________________________________________

% ps == ProcessSteps                        ps_1 ps_2 ps_3 ps_4               
algo_gb_struct.ps_analysis_minutes                     = [ 4.4  5.0  7.7  8.2 ]; % Source: Nick. 
algo_gb_struct.ps_1_nalysis_sample_baseline_start_ndx  = 1;
algo_gb_struct.ps_2_analysis_sample_baseline_end_ndx   = 2;
algo_gb_struct.ps_3_analysis_amp_binding_start_ndx     = 3;
algo_gb_struct.ps_4_analysis_amp_binding_end_ndx       = 4;

% G3 flag_matrix rows: inflexion points related ____________________________________
% All flags initalized with nan and set to 1 when true:

algo_gb_struct.flag_inflexion_times_ok              = 1;  % a good curve must have them and at the proper regions. 
algo_gb_struct.flag_inflexion_time_1_not_found_row  = 2;  % 
algo_gb_struct.flag_inflexion_time_2_not_found_row  = 3;  %
algo_gb_struct.flag_inflexion_time_1_too_early_row  = 4;  % 
algo_gb_struct.flag_inflexion_time_2_too_early_row  = 5;  % 
algo_gb_struct.flag_inflexion_time_2_too_late_row   = 6;  % 
algo_gb_struct.flag_tr_valid_tr_row                 = 7;  % 
algo_gb_struct.flag_row_max                         = 7;  % number of rows for the flag_matrix ... add here as needed ...


% G4  features_matrix rows globals ____________________________________________

% G4.1 feature_matrix: inflexion points related _______________________________
wanted_inflexion_point_count = input_parameters_struct.wanted_inflexion_point_count;
algo_gb_struct.inflexion_point_expected_count   = wanted_inflexion_point_count;
algo_gb_struct.feature_inflexion_time_1_ndx_row = 1; 
algo_gb_struct.feature_inflexion_time_2_ndx_row = 2; 
algo_gb_struct.feature_inflexion_time_3_ndx_row = 3; 
algo_gb_struct.feature_inflexion_time_4_ndx_row = 4; 

% G4.2 feature_matrix: excel amount of curve data per file ____________________
algo_gb_struct.feature_excel_cnt_row = wanted_inflexion_point_count+1; % N from excel: count of the range of found rows
algo_gb_struct.feature_cnt_row       = wanted_inflexion_point_count+2; % N true with floating values (exclude blanks and "invalid" labels

% G4.3 feature_matrix: chi2 related ___________________________________________
algo_gb_struct.feature_chi2_df_row   = wanted_inflexion_point_count+3; % df == cnt - file_cnt
algo_gb_struct.feature_chi2_data_row = wanted_inflexion_point_count+4; %
algo_gb_struct.feature_chi2_dist_row = wanted_inflexion_point_count+5; %

% G4.4 feature_matrix: estimated GRU shift deltas using found inflexion points. 
algo_gb_struct.feature_shift_base_row = wanted_inflexion_point_count+6; %
algo_gb_struct.feature_shift_amp_row  = wanted_inflexion_point_count+7; %
algo_gb_struct.feature_shift_delta_row= wanted_inflexion_point_count+8; %

algo_gb_struct.feature_row_max = feature_shift_delta_row;        % LAST: number of rows for the feature _matrix ... add here as needed ...

% G5 envelope matrix cols _____________________________________________________

algo_gb_struct.envelope_cnt_col       = 1; % M
algo_gb_struct.envelope_mean_col      = 2; % mean
algo_gb_struct.envelope_median_col    = 3; % median
algo_gb_struct.envelope_std_col       = 4; % std
algo_gb_struct.envelope_sigma2_col    = 5; % sigma ^ 2 i.e. var  The variance is the square of the standard deviation (STD).
algo_gb_struct.envelope_col_max       = 6;

% G6. tr_matrix  cols _________________________________________________________
% 	 Derived tr values and flags: All flags here are internal to the TR normalization process
%    Final result flag is tr_both_channel_ok_flag_col

algo_gb_struct.tr_raw_1_time_minutes_col       = 1; % 1 1:4 raw: C1=TR1_Time	C2=TR1_Shift C3=TR2_Time C4=TR2_Shift
algo_gb_struct.tr_raw_1_shift_pm_col           = 2; % 2
algo_gb_struct.tr_raw_2_time_minutes_col       = 3; % 3
algo_gb_struct.tr_raw_2_shift_pm_col           = 4; % 4

algo_gb_struct.tr_mean_12_time_col             = 5; % 6 value to use
algo_gb_struct.tr_mean_12_shift_col            = 6; % 5 value to use if it is sane: not_erratic (small delta),& ok other channel.
algo_gb_struct.tr_delta_prev_shift_col         = 7; % 7 value to use during evaluation: erratic or not the shifts?

algo_gb_struct.tr_mean_12_time_nan_flag_col    = 8; % 9
algo_gb_struct.tr_mean_12_shift_nan_flag_col   = 9; % 9
algo_gb_struct.tr_delta_12_shift_large_flag_col=10; % 10

algo_gb_struct.tr_this_channel_ok_flag_col     = 11;  %11   ok when 7 and 8 are ok.
algo_gb_struct.tr_other_channel_ok_flag_col    = 12; % 12  Grab it from the other channel's col: tr_this_channel_ok_flag_col
algo_gb_struct.tr_both_channel_ok_flag_col     = 13; % 13  ok only when both channels are ok. (This is the final result: to export to flag_normalization_matrix)
algo_gb_struct.tr_last_col                     = 13; % 13

% G8 Define QC specific globals __________________________________________________
% None so far ...

% GFINAL end of global matrix definitions __________________________________________________________
% return; % fn main_init_globals: Now it is a script!. 
