function [ handles ] = sa_do_push_req_expe(hObject, handles ) % hObject, eventdata, handles
%SA_DO_PUSH_REQ_EXPE handles requesting to the db by a combination of all what is currently in the UI edit boxes
%   See: sa_do_edit_req_date ui_state_struct
% Input: It handles 7 types of filters (all optional except one) (if no filters at all: requesting all data in the db!)
%   1 - req_start_date: start data range	
%   2 - req_end_date:   end data range	
%   3 - req_expe:       experiment name	
%   4 - req_spot:       spotting lot number	
%   5 - req_prot:       protocol name	
%   6 - req_kitl:       kit lot number	
%   7 - req_carr:       carrier_serial_number (match box carrier)
% OUTPUT:
%   The list of experiments that meet the 7 filters following these reules:
%   1 - The dates are anded with everything else. Dates have speciall rules (defined in parse_dates)
%   2 - filters 3 to 7 are anded with the dates and then the results are anded 
%   3 - 
% output fields:
%   handles.db_data_exp_struct.expe_req_table   = expe_req_table;           % final table with experiments
%   handles.db_data_exp_struct.expe_found_cnt   = height(expe_req_table);
%   handles.db_data_exp_struct.prot_req_table   = prot_req_table;           % final table with protocols used
%   handles.toe_cell                            = toe_cell;                 % 
%   handles.toe_col_struct                      = toe_col_struct;
%   handles.ui_state_struct                     = ui_state_struct;

% Steps:
% S1 gets start and end dates
% S2 gets list of experiments
% S3 gets any other of the parameters to apply filters.
% S4 get all experiments from : API indexTests using dates for each of the exp names.
% S5 from the exp returned by the API: removes whatever filters are set from the S3 step.
% S3 Update the toe
% sample exp name:          Jervis ANA Validation     OR   AA18R3        test: spot: AA16R1 carri: A2622J6 last year
% sample date range:        06/01/2017 to 12/01/2017
prot_req_table = table();
expe_req_table = table();
set(handles.text_exp_status,'enable','inactive');
guidata(hObject, handles);
status_msg = 'Requesting Experiments to the Cloud...  Please wait .....';

set(handles.text_exp_status,'string',status_msg,'ForegroundColor','blue','Fontsize',12); %'blue'
guidata(hObject, handles);
%set(handles.text_exp_status,'enable','inactive');
drawnow;

%lprintf_log('\nToo late to wait for the Request to the cloud\n')
ui_state_struct             = handles.ui_state_struct;
db_data_exp_struct          = handles.db_data_exp_struct;
%S1: Connect once
[ error_message, handles ]  = sa_rest_do_connect( handles,false );
if (~(isempty(error_message)))
    % CONNECTION PROBLEMS: No chance of doing anythingelse: Return
    status_msg = sprintf('ERROR_: Connecting to the API service: %s',error_message);
    set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'
    
    lprintf_log('\nERROR_ %s connecting to the API service \n',error_message);
    disp(error_message);            
    return;
end

if (~(isfield(ui_state_struct,'req_date_struct')))
    req_date_struct                         = struct;
    req_date_struct.start_day_resol_date_str='';
    req_date_struct.end_day_resol_date_str  ='';
    req_date_struct.start_sec_resol_date_str='';
    req_date_struct.end_sec_resol_date_str  ='';
    ui_state_struct.req_date_struct         = req_date_struct;
else
    req_date_struct                         = handles.ui_state_struct.req_date_struct;
end


if (~(isfield(ui_state_struct,'req_start_date_str')))
    ui_state_struct.req_start_date_str = '';
end
if (~(isfield(ui_state_struct,'req_end_date_str')))
    ui_state_struct.req_end_date_str = '';
end
if (~(isfield(ui_state_struct,'req_expe_list')))
    ui_state_struct.req_expe_list = '';
end
if (~(isfield(ui_state_struct,'req_spot_list')))
    ui_state_struct.req_spot_list = '';
end
if (~(isfield(ui_state_struct,'req_prot_list')))
    ui_state_struct.req_prot_list = '';
end
if (~(isfield(ui_state_struct,'req_kitl_list')))
    ui_state_struct.req_kitl_list = '';
end
if (~(isfield(ui_state_struct,'req_carr_list')))
    ui_state_struct.req_carr_list = '';
end

req_start_date_str  = ui_state_struct.req_start_date_str;
req_end_date_str    = ui_state_struct.req_end_date_str;
req_expe_list       = ui_state_struct.req_expe_list;
req_spot_list       = ui_state_struct.req_spot_list;
req_prot_list       = ui_state_struct.req_prot_list;
req_kitl_list       = ui_state_struct.req_kitl_list;
req_carr_list       = ui_state_struct.req_carr_list;

% % jas_temp: from dcrc_gen_xml_files   to be done: figure out where to stick this 
% % Parse dates to accomodate for API - jervis day resolution
% [ req_date_struct,error_message  ] = parse_date_request( request_date_str); % 'FROM 01/25/2018 03:00:10 PM TO 01/25/2018 06:17:12 PM')
% if (~(isempty(error_message)))
%     % COULD NOT PARSE DATE REQUEST: Get out
%     return;
% end

if ( (isempty(req_date_struct.start_sec_resol_date_str)) && (isempty(req_date_struct.start_sec_resol_date_str)))
    lprintf_log('Request date range:   NONE ' );
else
    % old_way: lprintf_log('\nRequest date range:   FROM  %s  TO  %s\n',req_start_date_str ,req_end_date_str);
    lprintf_log('Request date range:   FROM  %-s    TO    %-s',req_date_struct.start_sec_resol_date_str ,req_date_struct.end_sec_resol_date_str );
end

if (isempty(req_spot_list))
    lprintf_log('\nRequest spot:              None\n');
else
    lprintf_log('\nRequest spot:              %s\n',char(req_spot_list{1}));
end
if (isempty(req_carr_list))
    lprintf_log('\nRequest carri:             None\n');
else
    lprintf_log('\nRequest carri:             %s\n',char(req_carr_list{1}));
end
if (isempty(req_prot_list))
    lprintf_log('\nRequest protocol:          None\n');
else
    lprintf_log('\nRequest protocol:          %s\n',char(req_prot_list{1}));
end
if (isempty(req_kitl_list))
    lprintf_log('\nRequest kit lot number :   None\n');
else
    lprintf_log('\nRequest kit lot number:    %s\n',char(req_kitl_list{1}));
end

if (isempty(req_expe_list))
    lprintf_log('\nRequest Experiment:        None\n');
else
    lprintf_log('\nRequest Experiment:    %s\n',char(req_expe_list{1}));
end

if (isempty(req_expe_list))
    exp_req_cnt_1	= 0;
else
    exp_req_cnt_1 	=  size(req_expe_list,1);
end
if (isempty(req_spot_list))
    exp_req_cnt_2 	= 0;
else
    exp_req_cnt_2 	=  size(req_spot_list,1);
end
if (isempty(req_prot_list))
    exp_req_cnt_3 	= 0;
else
    exp_req_cnt_3 	=  size(req_prot_list,1);
end

if (isempty(req_kitl_list))
    exp_req_cnt_4 	= 0;
else
    exp_req_cnt_4 	= size(req_kitl_list,1);
end

if (isempty(req_carr_list))
    exp_req_cnt_5 	= 0;
else
    exp_req_cnt_5 	=  size(req_carr_list,1);
end

expe_req_cnt           = exp_req_cnt_1 + exp_req_cnt_2 + exp_req_cnt_3+ exp_req_cnt_4 + exp_req_cnt_5;
if (expe_req_cnt)
    expe_req_table_cel     = cell(expe_req_cnt,1);
else
    expe_req_table_cel     = cell(1,1); % probably just a date request.
end


cur_use_ndx = 1;
expe_req_table_cel{cur_use_ndx} = table();
for cur_expe_ndx = 1:1:exp_req_cnt_1
    cur_req_expe_str = req_expe_list(cur_expe_ndx,:);
    % Call API for cur experiment from the edit list: get list of experiments: for the given window and the given name.
    [ expe_req_table_cel_temp,   error_message ]  = ...
        sa_rest_exp_get_all(req_start_date_str,req_end_date_str,char(cur_req_expe_str), handles.rest_token);
    if ( (~(isempty(error_message))) && (~(strcmp(error_message,'warning_:no_experiments_found'))))
        % PROBLEMS: rpt and continue
        lprintf_log('\nERROR_ %s retrieving experiment %s for dates:FROM %s TO %s',error_message,cur_req_expe_str,req_start_date_str,req_end_date_str);
        continue;
    end
    % NO ERRORS AND MAYBE EXP FOUND: store the table (maybe empty)
    expe_req_table_cel{cur_use_ndx} = [ expe_req_table_cel{cur_use_ndx} ; expe_req_table_cel_temp ]; % concatenate vertically all the horiz requests: comma sepparated.
end % for each experiment for exp_name
if (~(isempty(expe_req_table_cel{cur_use_ndx}))) 
    % FOUND SOMETHING: increment the use index
    cur_use_ndx = cur_use_ndx + 1;
end

expe_req_table_cel{cur_use_ndx} = table();
for cur_expe_ndx = 1:1:exp_req_cnt_2
    cur_req_expe_str = req_spot_list(cur_expe_ndx,:);
    % Call API for cur experiment from the edit list: get list of experiments: for the given window and the given name.
    [ expe_req_table_cel_temp,   error_message ]  = ...
        sa_rest_exp_get_all(req_start_date_str,req_end_date_str,cur_req_expe_str, handles.rest_token);
    if ( (~(isempty(error_message))) && (~(strcmp(error_message,'warning_:no_experiments_found'))))
        % PROBLEMS: rpt and continue
        lprintf_log('\nERROR_ %s retrieving spot %s for dates:FROM %s TO %s',error_message,cur_req_expe_str,req_start_date_str,req_end_date_str);
        continue;
    end
    % NO ERRORS AND MAYBE EXP FOUND: store the table (maybe empty)
    expe_req_table_cel{cur_use_ndx} = [ expe_req_table_cel{cur_use_ndx} ; expe_req_table_cel_temp ]; % concatenate vertically all the horiz requests: comma sepparated.
end % for each experiment for spot
if (~(isempty(expe_req_table_cel{cur_use_ndx}))) 
    % FOUND SOMETHING: increment the use index
    cur_use_ndx = cur_use_ndx + 1;
end

expe_req_table_cel{cur_use_ndx} = table();
for cur_expe_ndx = 1:1:exp_req_cnt_3
    cur_req_expe_str = req_prot_list(cur_expe_ndx,:);
    % Call API for cur experiment from the edit list: get list of experiments: for the given window and the given name.
    [ expe_req_table_cel_temp,   error_message ]  = ...
        sa_rest_exp_get_all(req_start_date_str,req_end_date_str,cur_req_expe_str, handles.rest_token);
    if ( (~(isempty(error_message))) && (~(strcmp(error_message,'warning_:no_experiments_found'))))
        % PROBLEMS: rpt and continue
        lprintf_log('\nERROR_ %s retrieving protocol %s for dates:FROM %s TO %s',error_message,cur_req_expe_str,req_start_date_str,req_end_date_str);
        continue;
    end
    % NO ERRORS AND MAYBE EXP FOUND: store the table (maybe empty)
    expe_req_table_cel{cur_use_ndx} = [ expe_req_table_cel{cur_use_ndx} ; expe_req_table_cel_temp ]; % concatenate vertically all the horiz requests: comma sepparated.
end % for each experiment for proto
if (~(isempty(expe_req_table_cel{cur_use_ndx}))) 
    % FOUND SOMETHING: increment the use index
    cur_use_ndx = cur_use_ndx + 1;
end

expe_req_table_cel{cur_use_ndx} = table();    % wed_here:   input: protocol:  Salt 1 Step - 5 Point Curve Protocol (Fixed)
for cur_expe_ndx = 1:1:exp_req_cnt_4          % wed_here:   input  guiL kitl AA16R1 AA17R2
    cur_req_expe_str = req_kitl_list(cur_expe_ndx,:);
    % Call API for cur experiment from the edit list: get list of experiments: for the given window and the given name.
    [ expe_req_table_cel_temp,   error_message ]  = ...
        sa_rest_exp_get_all(req_start_date_str,req_end_date_str,cur_req_expe_str, handles.rest_token);
    if ( (~(isempty(error_message))) && (~(strcmp(error_message,'warning_:no_experiments_found'))))
        % PROBLEMS: rpt and continue
        lprintf_log('\nERROR_ %s retrieving kitl %s for dates:FROM %s TO %s',error_message,cur_req_expe_str,req_start_date_str,req_end_date_str);
        continue;
    end
    % NO ERRORS AND MAYBE EXP FOUND: store the table (maybe empty)
    expe_req_table_cel{cur_use_ndx} = [ expe_req_table_cel{cur_use_ndx} ; expe_req_table_cel_temp ]; % concatenate vertically all the horiz requests: comma sepparated.
end % for each experiment for kitl
if (~(isempty(expe_req_table_cel{cur_use_ndx}))) 
    % FOUND SOMETHING: increment the use index
    cur_use_ndx = cur_use_ndx + 1;
end

% if ( exp_req_cnt_4 >= 1) do simple
% for 2::1:exp_req_cnt_4  and do  [ xxx ; cur_table ]   test: spot: AA16R1 carri: A2622J6 last year

par_expe_req_table_cel = cell(100,1); 
%tic;
for cur_expe_ndx = 1:1:exp_req_cnt_5   % for_parfor 
    cur_req_expe_str = req_carr_list(cur_expe_ndx,:);
    % Call API for cur experiment from the edit list: get list of experiments: for the given window and the given name.
    [ expe_req_table_cel_temp,   error_message ]  = ...
        sa_rest_exp_get_all(req_start_date_str,req_end_date_str,char(cur_req_expe_str), handles.rest_token);                   %**BOTLENECKK****
    if ( (~(isempty(error_message))) && (~(strcmp(error_message,'warning_:no_experiments_found'))))
        % PROBLEMS: rpt and continue
        lprintf_log('\nERROR_ %s retrieving carrier %s for dates:FROM %s TO %s',error_message,char(cur_req_expe_str),char(req_start_date_str),char(req_end_date_str));
        continue;
    end
    % NO ERRORS AND MAYBE EXP FOUND: store the table (maybe empty)
    par_expe_req_table_cel{cur_expe_ndx} =  expe_req_table_cel_temp ; % concatenate vertically all the horiz requests: comma sepparated.
end % for each experiment for carr
%api_response_time_msg         = toc;
%lprintf_log('Time required retrieve %-5d exp = %s',exp_req_cnt_5,api_response_time_msg);

for cur_expe_ndx = 1:1:exp_req_cnt_5
    expe_req_table_cel{cur_use_ndx} = [ expe_req_table_cel{cur_use_ndx} ; par_expe_req_table_cel{cur_expe_ndx} ];
end

if (~(isempty(expe_req_table_cel{cur_use_ndx}))) 
    % FOUND SOMETHING: increment the use index
    cur_use_ndx = cur_use_ndx + 1;
end


if (expe_req_cnt == 0)
    % NO REQUEST BY ANY OF THE CRITERIA: Attempt just the dates as a filter:
    cur_req_expe_str = '';
    % Call API for cur experiment from the edit list: get list of experiments: for the given window and the given name.
    
    % BUT FIRST CHECK FOR EMPTY DATES: This would imply an empty request with no filters: the whole DB. prevent it!
    if ((isempty(req_start_date_str)) && (isempty(req_end_date_str)))
        % ZERO FILTERS: give warning and do nothing:
        warn_smg = 'WARNING: No filters. You need to have at least one. Otherwhise you will atempt to request every experiment in the database';
        set(handles.text_exp_status,'string',warn_smg,'ForegroundColor','red','Fontsize',12); %'blue'
        guidata(hObject, handles);
        return;
    end
    [ expe_req_table_cel{1}, error_message ]  = ...
        sa_rest_exp_get_all(req_start_date_str,req_end_date_str,cur_req_expe_str, handles.rest_token);

    if ( (~(isempty(error_message))) && (~(strcmp(error_message,'warning_:no_experiments_found'))))
        % PROBLEMS: rpt and continue   
        err_msg = sprintf('\nERROR_ %s retrieving ALL experiments for dates: FROM %s TO %s',error_message,req_start_date_str,req_end_date_str);
        set(handles.text_exp_status,'string',err_msg,'ForegroundColor','red','Fontsize',12); %'blue'
        guidata(hObject, handles);
        if (~(strcmp(error_message,'More than 1000 experiments found. Try a smaller window')))
            return;
        end
    end
    if ( (size(expe_req_table_cel{1},1) > 0) && (cur_use_ndx == 1 )   )
        cur_use_ndx = 2;
    end

    if ((~(strcmp(error_message,'warning_:no_experiments_found'))))
        cur_use_ndx = 2; % FINAL REQUEST: one: by date
    end
end
% OTHER REQUESTS BESIDES DATE
cur_use_ndx =  cur_use_ndx - 1; % index was ahead by 1 position. Set it right.
expe_req_cnt = cur_use_ndx;

% Concat the tables for up to 8 individual experiment names coming from a combination of the UI user selected filters.
if (expe_req_cnt == 0)
        expe_req_table =   table();
        status_msg = sprintf('NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'
end
if (expe_req_cnt >= 1)
        expe_req_table =   expe_req_table_cel{1};
end
if (expe_req_cnt >= 2)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{2}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I2 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end
end
if (expe_req_cnt >= 3)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{3}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I3 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end    
end
if (expe_req_cnt >= 4)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{4}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I4 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end       
end
if (expe_req_cnt >= 5)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{5}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I5 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end       
end
if (expe_req_cnt >= 6)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{6}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I6 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end       
end
if (expe_req_cnt >= 7)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{7}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I7 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end       
end
if (expe_req_cnt >= 8)
    [~,i1]          = intersect(expe_req_table.expid,expe_req_table_cel{8}.expid );
    expe_req_table  = expe_req_table(i1,:);
    if (isempty(i1))
        status_msg = sprintf('INTERSECTION_I8 NO EXPERIMENTS were found in the database. Try another filter.');
        set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'        
    end       
end
if (expe_req_cnt >= 9)
    status_msg = sprintf('warning: processing only the first 8 out of %d  requested experiments',expe_req_cnt);
    set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'
end

% 2018_04_19 filter out requests that are posibly outside the hh:mm:ss resolution for the request start and end datetimes.

if ((~(isempty(req_date_struct))) && (isfield(req_date_struct,'start_day_resol_date_str')))
    % NON-EMPTY DATE REQUEST: Make sure we filter for second resolution bug_fix 2018_05_27
    if (      (~ (strcmp(req_date_struct.start_day_resol_date_str,''))) ...
            ||(~ (strcmp(req_date_struct.start_day_resol_date_str,''))) ...
            ||(~ (strcmp(req_date_struct.start_day_resol_date_str,''))) ...
            ||(~ (strcmp(req_date_struct.start_day_resol_date_str,''))) ...
            )
        % DATE FILTER IS NOT EMPTY: IE there is a real request to filter by date:
        [ expe_req_table]  = sa_rest_exp_filter_date_sec_resol(req_date_struct,expe_req_table);
    end
end
expe_found_cnt     = height(expe_req_table);
lprintf_log('\nExperiments found count = %-4d SECOND_RESOLUTION AND COMPLETE STATUS\n...Processing... (Rate: ~20 exp per minute)....',expe_found_cnt);

if ((cur_use_ndx) && (~(isempty(expe_req_table))))
    % FOUND SOMETHING
    % HERE IS THE PLACE TO GET THE PROTOCOLS:
    % Once it is known the unique experiments: jas_tbd: remove protocol stuff from sa_rest_exp_get_all
    proto_all       = cell(expe_found_cnt,1); % this holds all the protocol names     
    proto           = cell(expe_found_cnt,1); % this holds all the protocol names. worst case: each exp has a different protocol (unique)
    prots           = cell(expe_found_cnt,1); % this holds all the protocol data structures.
    stept           = cell(expe_found_cnt,1);
    proti           = cell(expe_found_cnt,1); % this holds all the protocol ids (unique list)
    
    proid           = expe_req_table.proid;
    proid_cat       = categorical(expe_req_table.proid);
    proid_cat_u     = unique(proid_cat);
    proto_u_cnt     = size(proid_cat_u,1);
    if (proto_u_cnt)
        cur_cat_ndx = 1;
        cur_cat_bad = 0;        
        while cur_cat_ndx  <= proto_u_cnt % bug_fix: 2018_05_27            
            cur_id = char(proid_cat_u(cur_cat_ndx));
            [ error_message,protocol_struct ] = sa_rest_protocol_get_details(cur_id,handles.rest_token); %
            if (~(isempty(error_message)))
                % IGNORE BAD PROTOCOLS: do not store them.
                cur_cat_bad = cur_cat_bad +1;
                if (( cur_cat_ndx + cur_cat_bad -1) >= proto_u_cnt)
                    % WE ARE DONE: OK + BAD ADD UP TO TOTAL: get out
                    break;
                else
                    continue;
                end
            else
                % FOUND DETAILS FOR THIS PROTOCOL: Update all proto names with the same category
                proto(cur_cat_ndx)                            = {protocol_struct.name};
                prots(cur_cat_ndx)                            = { protocol_struct }; % to build table with needed protocols
                protocol_step_table                           = struct2table(protocol_struct.recipe.steps);
                dura_secs                                     = cell2matdouble(protocol_step_table.duration);
                scan_flag                                     = protocol_step_table.scan;
                step_table                                    = table(dura_secs,scan_flag);
                stept(cur_cat_ndx)                            = {step_table};
                proti(cur_cat_ndx)                            = {protocol_struct.id };
                proto_all(proid == proid_cat_u(cur_cat_ndx))  = {protocol_struct.name}; % fix_2018_04_19  proid_cat_u vs  proid_cat   update the protocol names for all exp that have this protocol_id
            end
            cur_cat_ndx =  cur_cat_ndx +1;
        end % end while
        ok_proto_cnt = cur_cat_ndx-1;
        if (ok_proto_cnt)
            proto                = proto(1:ok_proto_cnt);
            proti               = proti(1:ok_proto_cnt);
            prots               = prots(1:ok_proto_cnt); % protocol struct: The whole thing.
            stept               = stept(1:ok_proto_cnt); % step_table: dura_secs and scan_flag ready to be used for analysis
            % recipe            = struct2table(protocol_struct.recipe.steps)
            % recipe            = struct2table(protocol_struct.recipe.steps);
            % duration_sec      = (cell2matdouble(recipe.duration) ) / 60; %     60    30   180   120   210    60    15 in  seconds: % to convert sec to minutes
            prot_req_table      = table(proti,proto,prots,stept); %2018_03_18 addition: add the recipe.step.duration so it can be used for analysis easily
            expe_req_table.proto= proto_all;  % update the protocol names in the table of experiments.
                        
            % HAVE THE REQUESTED EXPERIMENTS AND THE PROTOCOL NAMES AND DETAILS: Get all the associated data
            % BUT BEFORE: Get rid of records that do not have an experiment name:
            
            empty_ndx       = zeros(expe_found_cnt,1);
            for cur_ndx=1:1:size(proto_all,1)
                if (isnumeric(proto_all{cur_ndx}))
                    % EMPTY PROTOCOL NAME: isnumeric returns true for an empty array. Get rid of it.
                    empty_ndx(cur_ndx) = 1;
                    %lprintf_log('\nWARNING: removing bad protocol: %-s\n',char(proid{cur_ndx}));
                end
            end
            if ( sum(empty_ndx))
                % THERE WERE SOME EXPERIMENTS WITH BAD PROTOCOLS: Get rid of them
                lprintf_log('\nWARNING: removed: %-4s experiments with bad protocols\n', sum(empty_ndx));
                % filter out empty protocol names
                empty_ndx       =~(empty_ndx);
                expe_req_table  = expe_req_table(empty_ndx,:);
            end
        else
            % THERE ARE NO GOOD PROTOCOLS: Nothing to process:
            prot_req_table      = table();
            status_message = sprintf('Found %-4d experiments but all their procols are bad. ',expe_found_cnt);
            lprintf_log('\n%s',status_message);
            expe_req_table = table();
        end
    end % at least one protocol
    
    %expe_found_cnt  = height(expe_req_table);
    
    % Check if filtering by protocol Name: If this is the case:
    % remove experiments that do not pass the protocol filter criteria:
    % jas_here: monday_jas
    
    if ((exp_req_cnt_3) && (height(prot_req_table) > 1) )
        % FLTERING BY PROTOCOL NAME:
        expe_req_table.proid = categorical(expe_req_table.proid);
        % MORE THAN ONE PROTOCOL: Need to get rid of exp that do not pass the protocol filter:
        for cur_prot_ndx = 1:1:height(prot_req_table)
            if (strcmpi(char(prot_req_table.name(cur_prot_ndx)),char(cur_req_expe_str)))
                wanted_proid  = prot_req_table.id(cur_prot_ndx);
                % THIS IS THE WANTED PROTOCOL: Grab the experiments with this protocol ONLY.
                expe_req_table(expe_req_table.proid ~= wanted_proid,:) = [];
                break;
            end
            
        end % for each unique protocol
    end

    expe_found_cnt  = height(expe_req_table);            
    status_msg = sprintf('Found: %4d Experiments in the database. You may Click on SENSOGRAMS to analyze sensograms.',expe_found_cnt);
    lprintf_log('\n%s\n',status_msg)
    
    dark_green_color = [0.1  0.5  0.2];  % nice dark green.    
    set(handles.text_exp_status,'string',status_msg,'ForegroundColor',dark_green_color,'Fontsize',12); %'blue'
    drawnow;
    

  lprintf_log('\nDONE WITH SA_DO_PUSH_REQ_EXPE\n')   
end % at least one exp found

% NOW: The real work:  file: sa_do_push_req_expe
   % jas_here_th_before_pickup_to_be_cloned_in_algo2 from here down
   % idea: use the future column to show the "bad_data" experiments: clone of exper_bad_data_ndx in get_jervis_data_for_dcrc
   
% Create toe: Start with the found experiments for the toe rows and adjust as needed:      % sample exp name: Jervis ANA Validation

ui_state_struct.db_cur_exp_net_ndx  = true(expe_found_cnt,1); % start with all rows selected
[toe_cell, toe_col_struct]          = toe_create(expe_found_cnt);
db_data_exp_struct.expe_req_table   = expe_req_table;
db_data_exp_struct.expe_found_cnt   = height(expe_req_table);
db_data_exp_struct.prot_req_table   = prot_req_table;  
handles.toe_cell                    = toe_cell;
handles.toe_col_struct              = toe_col_struct;
handles.ui_state_struct             = ui_state_struct;
handles.db_data_exp_struct          = db_data_exp_struct;

% NIU Not needed: db_data_exp_struct  ] = fill_in_db_exp_from_exp_data(expe_req_table,db_data_exp_struct );
% Save the virigin full toe_cell (untochable! until a new ui request is done!)
% [handles]                   = ui_do_fill_toe_exp_set(handles);

%if ( expe_found_cnt >0 )
% AT LEAST ONE RECORD TO BE DISPLAYED:
[handles ] = sa_ui_update_toe(handles);
% end



end % fn sa_do_push_req_expe


% THIS IS THE "or" OF ALL CONDITIONS. Need to implement the "and": find common elements in all the tabbles
% 
% switch expe_req_cnt
%     case 0
%         expe_req_table =   table();
%         status_msg = sprintf('NO EXPERIMENTS were found in the database. Try another filter.');
%         set(handles.text_exp_status,'string',status_msg,'ForegroundColor','red','Fontsize',12); %'blue'
%         %return;
%     case 1
%         expe_req_table =   expe_req_table_cel{1};
%     case 2
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2} ];
%         
%     case 3
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}  ; expe_req_table_cel{3} ];
%     case 4
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}   ; expe_req_table_cel{3} ; expe_req_table_cel{4}] ;
%     case 5
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}   ; expe_req_table_cel{3} ; expe_req_table_cel{4}  ; expe_req_table_cel{5} ];
%     case 6
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}   ; expe_req_table_cel{3} ; expe_req_table_cel{4}  ; expe_req_table_cel{5}  ; expe_req_table_cel{6} ];
%     case 7
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}   ; expe_req_table_cel{3} ; expe_req_table_cel{4}  ; expe_req_table_cel{5}  ; expe_req_table_cel{6}  ; expe_req_table_cel{7} ];
%     case 8
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}   ; expe_req_table_cel{3} ; expe_req_table_cel{4}  ; expe_req_table_cel{5}  ; expe_req_table_cel{6}  ; expe_req_table_cel{7}  ; expe_req_table_cel{8} ];
%     otherwise
%         status_msg = sprintf('warning: processing only the first 8 out of %d  requested experiments',expe_req_cnt);
%         set(handles.text_exp_status,'string',status_msg,'ForegroundColor','green','Fontsize',12); %'blue'
%         
%         expe_req_table = [ expe_req_table_cel{1} ; expe_req_table_cel{2}   ; expe_req_table_cel{3} ; expe_req_table_cel{4}  ; expe_req_table_cel{5}  ; expe_req_table_cel{6}  ; expe_req_table_cel{7}  ; expe_req_table_cel{8} ];
% end
