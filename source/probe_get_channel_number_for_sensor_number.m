function [ channel_number ] = probe_get_channel_number_for_sensor_number( probe, sensor_number )  %old get_channel_number_for_sensor_number
%PROBE_GET_CHANNEL_NUMBER_FOR_SENSOR_NUMBER for a given sensor number, retrieves the associated channel number 
%   Detailed explanation goes here   UNFINISHED CODE DO NOT USE IT YET!.  NIU (old style: data from the instrument)
% sample call: [ channel_number ] = probe_get_channel_number_for_sensor_number( probe, 40);
channel_number = 0;
probe_table  = struct2table(probe);  
if (channel_number == 1)
    name_list   = probe_table(1:18,1);
    sensor_list = probe_table(1:18,2);
end
if (channel_number == 2)
    name_list   = probe_table(19:36,1);
    sensor_list = probe_table(19:36,2);
end

% 'Temperature Reference'

[ element_found_flag, element_ndx ] = is_element_in_cell(table2cell(name_list),sensor_name );
if (element_found_flag)
    sensor_numbers_array = table2array(sensor_list(element_ndx,1));
    sensor_numbers_array = sensor_numbers_array{1};
else
    sensor_numbers_array = [];
end

end % fn probe_get_channel_number_for_sensor_number

