function [ tr_matrix  ] = tr_build2(algo_gb_struct,tr_matrix, raw_tr_data,file_loaded_count,feature_matrix )
%TR_BUILD builds the tr values that may get used during normalizaton. 
%  The evaluation of the tr_matrix is performed by the tr_calc_and_evaluate function
%  The tr_matrix passed as argument is usually nan. It gets overwritten with the contents of the raw_tr_data
% declare_globals;

for cur_file = 1:1:file_loaded_count
    %raw_tr_data_size = size(raw_tr_data{cur_file});
    tr_curve_len      = feature_matrix(algo_gb_struct.feature_excel_cnt_row,1,cur_file); % jas_tbr: use length of first curve on the file
    % fill in tr_matrix
    % cols 1 to 4: raw
    tr_matrix(1:tr_curve_len,algo_gb_struct.tr_raw_1_time_minutes_col:algo_gb_struct.tr_raw_2_shift_pm_col,cur_file) = raw_tr_data{cur_file}(1:tr_curve_len,:) ;
    %a = tr_matrix(1:tr_curve_len,2:2:4,cur_file);
    % cols 5 and 6 mean time and mean shifts
    tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_time_col,cur_file)      =  nanmean(tr_matrix(1:tr_curve_len,algo_gb_struct.tr_raw_1_time_minutes_col:2:algo_gb_struct.tr_raw_2_time_minutes_col,cur_file),2);
    tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_shift_col,cur_file)     =  nanmean(tr_matrix(1:tr_curve_len,algo_gb_struct.tr_raw_1_shift_pm_col    :2:algo_gb_struct.tr_raw_2_shift_pm_col    ,cur_file),2);
    % col 7         % Calculate delta shifts for the mean tr
    tr_matrix(1:tr_curve_len-1,algo_gb_struct.tr_delta_prev_shift_col,cur_file)= abs(diff(tr_matrix(1:tr_curve_len,algo_gb_struct.tr_mean_12_shift_col                                                           ,cur_file)));
end
return;

