% file: algo2_wrk.m (file_1)
% Thurs: 1 - generate main DONE
%      2 - generate storage_app_deppendent: no arguments: 
%          return: fitter_cells_minimal,ui_state_minimal, 
%      3 - generate storage_dbs_deppendent: see matrices_storage: rows_db_cnt,max_scan_cnt,db_cols_cnt,tos_rows,tos_cols
%          return: fitter_cells_real,ui_state_real, 
%      4 - get_data_code: fake it with data from algo1. 5 sensograms.
%      5 - sc_classify_sensograms_code: 
%      6 - plot good_data_code
%      7 - plot anom_data_code
%      8 - implement code for 2 filters and generate REST request string.
%      8 - update_tos_code 

% list of filters:
%
% text_probe        : 1 
% popupmenu_probe   : populate with list of probes (from cfg file) about 16  default: first one on the list. 
%                   :   sel change: dlg_msg: Do you want to save the current data_set? y/n.
%                       1: y: save_dglg 
%                       2: reset ui. + reset memory ready to open/get.  + dlg_msg: either open local file or Select filters before requesting data from db.                    
% text_date         : 2
% popupmenu_date    : options: all, today, yesterday, last_7_days, last_30_days, this_month, last_month,  custom_range
%                   :   % std_update_for all filters:
%                           1 - no custom: calc new date_range. 
%                           2 - custom:  dlg to enter data_range
%                       	3 - if data_set_is_opened:  apply_filter: g_db_set_ui_filter and recal all quantities to upd filter_cnts.
% text_proto        : 3
% popupmenu_proto: default all. / add_protocol / add result from a query / select unique from a query (multiple) enter custom (may have wild) 
%                   :   % std_update_for all filters:
%                           1 - add_protocol(s): dlg to type protocol names  (you may use * as wild character)
%                           2 - if single pick with no check mark: add    checkmark. (multiple)
%                           3 - if single pick with    check mark: remove checkmark. 
%                       	4 - if data_set_is_opened:  apply_filter: g_db_set_ui_filter and recal all quantities to upd filter_cnts.
%                       	5 - if data_set_is_closed:  turn_on_flag: need_to_get_data_from_db=1

% text_probe        : 1  % popupmenu_probe    :
% text_dates        : 2  % popupmenu_dates    : 
% text_proto        : 3  % popupmenu_proto    :
% text_clust        : 4  % popupmenu_clust    :
% text_chann        : 5  % popupmenu_chann    : default all. / add result from a query / select unique from a query / enter custom (may have wild) 
% text_exper        : 6  % popupmenu_exper    : default all. / add result from a query / select unique from a query / enter custom (may have wild) 
% text_chipl        : 7  % popupmenu_chipl    : default all. / add result from a query / select unique from a query / enter custom (may have wild) 
% text_carri        : 8  % popupmenu_carri    : default all. / add result from a query / select unique from a query / enter custom (may have wild) 
% text_inst         : 9  % popupmenu_instr    : default all. / add result from a query / select unique from a query / enter custom (may have wild) 
% text_sampl        : 10 % popupmenu_sampl    : default all. / add result from a query / select unique from a query / enter custom (may have wild) 
%_________________________________________
% text_datat        : 11 % popupmenu_datat   : default Normalized_only / raw_only  / raw and Normalized  /ml_norm only (future)
%                                              special: if probe==tr: disable Normalized_only,  raw and Normalized and set it to raw.  
% test_ampli        : 12 % popupmenu_ampli   : default all. /  gru_raw_range /  gru_norm_range /  shift_raw_range /  shift_norm_range  
%                                              (gru or shift enable/disabled according to popupmenu_data menu state: 
%                                              special: if probe==tr: disable shift*range    
% text_kitlo        : future
% popupmenu_kitlo   : default all. / add result from a query / select unique from a query / enter custom (may have wild) 

%_________________________________________
tbd: Thurs:
1 - Handle Consistent check marks on each pulldown menu: Future multiple selections: then need checkmarks on pulldown 
    or may need item:  Exlcude: custom list or 
% text_probe      	popupmenu_probe     1 Ana_1 ..Ana_16 , Tr, All (except  Tr)   no need for check marks
% text_dates     	popupmenu_dates     2
% text_proto      	popupmenu_proto     3
% text_sampl       	popupmenu_sampl     4
% text_chipl      	popupmenu_chipl     5
% text_exper       	popupmenu_exper  	6
% text_instr     	popupmenu_instr     7
% text_carri        popupmenu_carri     8
% text_chann        popupmenu_chann     9
% text_clust        popupmenu_clust   	10
% text_datat     	popupmenu_datat   	11
% text_ampli        popupmenu_ampli     12
2 - 

text_sens_good_per_plot new start
text_sens_anom_per_plot
text_sens_good_show_ref
text_sens_anom_show_ref new end 
text
edit_sens_good_per_plot
edit_sens_anom_per_plot
pushbutton_sens_good_next
pushbutton_sens_anom_next
% good plots _________________________
checkbox_good_sens_env
checkbox_good_sens_med
checkbox_good_sens_avg
%anom plots _________________________
checkbox_anom_sens_env
checkbox_anom_sens_med
checkbox_anom_sens_avg
checkbox_update_good_and_anom_sens   % to upd simultaneously

radiobutton_local_file
radiobutton_db
pushbutton_get_data
axes_good_sens
axes_anom_sens
uitable_tos

uipanel_data_filters
uipanel_anom_sens
uipanel_good_sens
uipanel_anom_criteria
uipanel_anom_criteria

text_good_bline_region_yellow
text_good_ampli_region_yellow

text_anom_bline_region_yellow
text_anom_ampli_region_yellow

1 - ui_push_tool_new_db_local_file	New Local Data Set File
1 - ui_push_tool_open_db_local_file	Open Local Data Set File
1 - ui_push_tool_save_db_local_file	Save Local Data Set File
2 - ui_toggle_tool_maximize_plot    Maximize Plot                tbd
3 - ui_toggle_tool_residuals_plot   Show Residuals Plot          tbd
4 - ui_toggle_tool_env              Show Envelope                tbd
5 - ui_toggle_tool_zoom_in          Zoom In    def
6 - ui_toggle_tool_zoom_out         Zoom Out   def 
7 - ui_toggle_tool_pan              Pan Selected Plot    
8 - ui_toggle_tool_data_cursor      Data Cursor 
9 - ui_toggle_tool_exclude_outliers Exclude Outliers             tbd 
10- ui_toggle_tool_legend           Legend                       toggle legends
11 -ui_toggle_tool_auto_manual_axis Adjust Axes Limits           

figure_sens_Anal_app  
figure_name tag: sc_figure:   sensogram_classification.
tbd_ui:
 get data radio:  group it to make it mutually exlclusive. 
        

% 1_File menu:  
%   1_Open Local data .. open mat file from current dir. 
%   2_Save Data Whole data Set ... probe_cur_date_yyy_mm_dd_mm_file_save.mat
%      Saves: data_set_db  (to recreate the session)
%             cur_filters_state
%             cur_classif_state 
%             cur_db_query_request: for jas_dbg. 
%   3_Save Selected TOS records data Set ... file_save.mat
%      Saves: data_db(tos_ui_sel_ndx,*) (to recreate smaller session: tos_ui_sel_ndx)
%             cur_filters_state
%             cur_classif_state 

% 2_Edit menu:
%   select all                   	ctr-a   (selects all rows from tos (implicit uses cur  filters)
%   cut: not needed(?)                      ?maybe get rid of sensograms you do not want for watever reason? delete from db?
%   copy cur-sel rows rows.      	ctrl-c (selects cur sel rows from tos (implicit uses curr filters)
%   copy whole window.           	ctrl-w (to paste window img into power point)
%   copy cur plot.                  ctrl-p (to paste plot   img into power point)
%   paste: not needed   

% 4_Analysis menu:
%     Classify all sensograms      ctrl-s   (batch like: or when changing any of the thresholds/or norm algorithm).
%     Normalize using Jervis algo           single check-mark default. 
%     Normalize using Matlab algo           single check-mark
    

% Get_data btn
%     cases:  is_opened_data_set = false:  Starting from zero.
%             is_opened_data_set = true:   if need_to_get_data_from_db 
%                   dlg: ask user if wants to: start new data_set or append_to existing_data_set
%                        if start new: ask if want to save existing data set to a local file.
%                        if append: find what data needs to be retrieved to bring only non-retrieved data.
%     from db: 1 - builds_query using cur_filters_state
%              2 - sends_query
%              3 - waits_for_data_from_db: dlg (getting data. ...
%              4 - when received: dlg (classifing data ...)
%              5 - Classifying sensograms (only enough to fill the good and bad).
%              5 - Display sensogram plots using plot_state
%              6 - Update Table of sensors. 
%              7 - Update gui state and ...listen for clicks.
%
% matrices_storage: rows_db_cnt,max_scan_cnt,db_cols_cnt,tos_rows,tos_cols
% 1 - g_db_set      2d rows_db_cnt x (db_cols_cnt + ui_total_fg_cnt)  how does it relate to tos ?
% 2 - g_db_set_cat  2d rows_db_cnt x (db_cols_cat_cnt)                      cat  ordinal proto+samp+lotx+expr+inst+carr+chan+clust+datat
% 3 - g_db_dates    2d rows_db_cnt x 1                                      dates 
% 4 - g_scan_raw    2d rows_db_cnt x max_scan_cnt from db                   float
% 3 - g_scan_nrm    2d rows_db_cnt x max_scan_cnt from db                   float
% 4 - g_scan_tr     2d rows_db_cnt x max_scan_cnt from db                   float
% 5 - g_scan_nrm_ma 2d rows_db_cnt x max_scan_cnt  calculated by matlab    	float
% 6 - sc_flags_cat  2d rows_db_cnt x 1                                      cat   ordinal

% additional cols for  g_db_set this is an original classificat
%   g_db_set_db_ndx     this is the original order the data came from the db query.
%   g_tos_sort_ndx      this is the order to display data in the tos. (default: same as g_db_set_db_ndx)
%   g_db_set_ui_filter  0/1 1:if selected by filters. (ie the real data to work with)
%   g_db_set_sc_done    0/1 1:if classification done
%   g_db_set_rc_done    0/1 1:if re-classification done by end-user).

%   g_db_set_ui_plot_g  currently ploted on the good side 
%   g_db_set_ui_plot_a  currently ploted on the anom side  
%   g_db_set_ui_ptof    currently displayed on tos tbl.  

% cells:
% 1 - filter_state: 
%       1_ probe_filter: {ndx_ui_cur, probe_1, .... probe_n }
%       2_ dates_filter: {ndx_ui_cur,from_cur-to_cur, all, today, yesterday, last_7_days, last_30_days, this_month, last_month,  custom_range }
%       3_ proto_filter: {ndx_ui_cur_list, all, proto_1, .... proto_n }                             muliple
%       4_ clust_filter: {ndx_ui_cur, all, 1,2,3,4 }   23 means 2 and 3   234 means 2,3 and 4 etc.  muliple
%       5_ chann_filter: {ndx_ui_cur, all, 1, 2 }                                                   single
%       6_ exper_filter: {ndx_ui_cur_list, all, exper_1, .... exper_n }                             muliple
%       7_ chipl_filter: {ndx_ui_cur_list, all, chipl_1, .... chipl_n }                             muliple
%       8_ carri_filter: {ndx_ui_cur_list, all, carri_1, .... carri_n }                             muliple
%       9_ instr_filter: {ndx_ui_cur_list, all, instr_1, .... instr_n }                             muliple
%      10_ sampl_filter: {ndx_ui_cur_list, all, sampl_1, .... sampl_n }                             muliple
%      11_ datat_filter: {ndx_ui_cur, both, raw, norm}                                              single
%      12_ ampli_filter: {ndx_ui_cur, all, raw_in_range..., raw_ex_range...,norm_in_range... ,norm_ex_range... } single
%                        {change to , all, raw_in_xx_to_yy, raw_ex_xx_to_yy,norm_in_xx_to_yy ,norm_ex_xx_to_yy }
     
% idea:  after get/load data or any filtering: update some ui static text with unique_select/uniq_avail.
%        only when the ratio is one then:  not x/x but just x. 
% idea:  probe:     359/834                         
% idea:  protocol:     4/13 
% idea:  sample:        1/1  
% idea:  lot:           1/1 
% idea:  exp:           1/1 
% idea:  inst:          1/1 
% idea:  carr:          2/9 
% Good Sensograms:   210/359 (358 is the selected after all filters).
% Anom Sensograms:   149/359 (358 is the selected after all filters).

% tos_cols:
% db_sen_ndx:        == indep of what is being displayed/filtered etc.
% tos_ui_sel_ndx:    == ndx of currently tos rows selected (for potential save/plot)
% sc_flags_cat 	U_	 == unknown. SV not done yet 
%             	G_	 == good 
%             	A_	 == anomalous
%             	G_O_ == Good       Overwerite 
%            	A_O_ == Overwerite Anomalous 
%            	F_   == unknown algorithm Failure 
%            	BLNF_== bl_not_found  (ip)
%            	AMNF_== am_not_found  (ip)
%            	TMIP_== too_many_op_found  (ip)
%            	BLSL_== meas_region_base_line_slope_too_high  
%            	AMSL_== meas_region_ampl_area_slope_too_high 
%            	BLRQ_== meas_region_base_line_R Square too low  
%            	AMRQ_== meas_region_ampl_area_R Square too low  
% tos pulldown menu:
%   Display good_rows_only          (implicit: uses current filters) (checked/unchecked)
%   Display anom_rows_only          (implicit: uses current filters) (checked/unchecked)
%   ________________
%   plot_cur_select_rows          	(other implications: reset both sides and sens/plot: 1 or x
%   ________________
%   recla_cur_select_rows_as_good 	algo will be ignored            for this/these sensors
%   recla_cur_select_rows_as_anom  	algo will be ignored          	for this/these sensors
%   recla_cur_select_rows_as_algo   	user reclass will be ignored    for this/these sensors
%   ________________
%   Filter_out cur_select_rows      (implicit: uses current filters) must go and upd all filter cnts and indices.
%   Reset all Filters               (resets: current filters to all) must go and upd all filter cnts and indices.
% sort by: 20 cols + all flags (use excel dlg: first by x then by y then by z then by w.
%   ________________
% copy rows (to paste into Excel)
% 

% tr analysis: when tr is the probe selected: all rules change:
% 1 - sc_classify_analyte vs   sc_classify_tr (threshold_max_delta_consec_pnts).

% plot_pull_downs:
%   recla_cur_select_sens_as_good 	algo will be ignored            for this sensor(s)(sensor will disapear if cur_plot_is_anom) if upd_both: it will reapear on other side
%   recla_cur_select_sens_as_anom  	algo will be ignored          	for this sensor(s)(sensor will disapear if  cur_plot_is_good) same
%   recla_cur_select_sens_as_algo  	user reclass will be ignored    for this sensor(s)(sensor may  disapear deppending on algo ) same

% plot_btns:  display tr 
% 

% tbd_from simmon: publishing, (generate pdf rpt / generate web rpt).
% tbd: sel/zoom/pan ie plot_edit icons. 

handles.pushbutton_get_data


handles.file_menu                       % menu_1
handles.file_new_local_menu_item
handles.file_open_local_menu_item
handles.file_save_db_local_menu_item
handles.file_append_sel_local_menu_item    (w: dupplicates will be removed!). 
handles.file_exit_menu_item                

% handles.file_append_sel_local_menu_item(future: Append: check with end users).
% better option: save radio: all/selected   radio: new file/append.


handles.edit_menu                       % menu_2
handles.edit_sel_all_menu_item
handles.edit_sel_inv_menu_item
handles.edit_copy_sel_menu_item
handles.edit_copy_window_menu_item

handles.view_menu                       % menu_3 view
handles.view_manual_scale_good_menu_item
handles.view_auto_scale_anom_menu_item
handles.view_manual_scale_anom_menu_item
handles.view_plot_tr_good_menu_item
handles.view_plot_tr_anom_menu_item
handles.view_plot_meas_reg_menu_item
handles.view_plot_grid_menu_item
handles.view_plot_bline_circles_menu_item
handles.view_plot_ampli_circles_menu_item

handles.analysis_menu                    % menu_4 analysis
handles.analysis_norm_jervis_menu_item
handles.analysis_norm_matlab_menu_item
handles.analysis_class_senso_menu_item

thresholds_menu                         % menu_5 thersholds

handles.help_menu                       % menu_6 help
handles.help_about_sc_menu_item         % displays version
handles.help_help_menu_item             % displays version



