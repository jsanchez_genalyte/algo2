function [ probe_map,probe_name,sensor_name,sensor_channel ] = probe_get_map( probe) % old: get_probe_map
%PROBE_GET_MAP  retrieves the probe_map: a 18 x 2 x 4 array with sensor numbers. NIU (old style: data from the instrument)
%  the returned map can be used like this:  cur_sensor = probe_map(cur_probe_ndx,cur_channel_ndx,cur_sensor_probe_ndx)
% dim_1 is the probe:       1 to 18   144/8=18 2 trs and 2 leak detectors: 16 analyes. 
% dim_2 is the channel and  1 to 2
% dim_3 is the probe_sensor 1 to 4  (for all of them except 4 so total is 144-8 = 136 I.e. there are 8 nans)
%   If the name requested is not found in the config struct, it returns null.
%   Sample call:  [ probe_map,probe_name,sensor_name,sensor_channel ] = probe_get_map( probe)

probe_map       = nan(18,2,4);
probe_name      = cell(18,1);
sensor_name     = cell(136,1);  % jas_hardcoded jas_temp 
sensor_channel  = nan(136,1);
for cur_probe_ndx = 1:1:36 %  ch_1_start_analyte_pos_ndx:1:ch_1_final_analyte_pos_ndx
    % get the scan_data for the current sensor
    %cur_scan_data = table2array(scan_table(cur_sensor_ndx,2));cur_scan_data = cur_scan_data{1};
    %gru = scan(1,cur_sensor_ndx).sensor_data(:,2);
    %a = = table2array(scan_table(cur_sensor_ndx,1));cur_scan_data = cur_scan_data{1};
    sensor_list     = probe(1,cur_probe_ndx).sensors(:,:);
    sensor_list_len = length(sensor_list);
    cur_channel     = probe(1,cur_probe_ndx).channel;
    for cur_sensor_ndx  =1:1:sensor_list_len
        
        cur_sensor  = sensor_list(cur_sensor_ndx);
        if (cur_probe_ndx> 18)
            cur_probe_store = cur_probe_ndx -18;
            
        else
            cur_probe_store = cur_probe_ndx;
        end
        
        probe_map(cur_probe_store,cur_channel,cur_sensor_ndx) = sensor_list(cur_sensor_ndx);
        probe_name{cur_probe_store} = probe(1,cur_probe_ndx).name;
        sensor_name{cur_sensor}     = probe(1,cur_probe_ndx).name;
        sensor_channel(cur_sensor)  = probe(1,cur_probe_ndx).channel;
    end
    
end
end % fn probe_get_map