function [ url_struct] = sa_set_production_vs_stagging_url_flag(  )
%SA_SET_PRODUCTION_STAGGING_FLAG sets the proper url_login_str for e
%   This function defines ALL the needed urls to connect to the jervis API. 
%
% Production:   'GL33D0C2AB50' given     by Azadeh (2018_02_01)
% staging:      'GL86C3700A12' confirmed by Azadeh
% Production:   url_login_str = 'http://jervis.genalyte.com/api/auth/token';           % staging url. For % production   https://jervis.genalyte.com
% staging:      url_login_str = 'http://jervis-staging.genalyte.com/api/auth/token';   % staging url. For % production   https://jervis.genalyte.com

%jas_temp_hard_coded: (Single place in the whole sa app.)
prod_stag_flag          = 1; %  1 == connect to production API       0 == connect to staging API

token_str_prod          = 'GL33D0C2AB50';
token_str_stag          = 'GL86C3700A12';

url_login_str_prod      = 'http://jervis.genalyte.com/api/auth/token';
url_login_str_stag      = 'https://jervis-staging.genalyte.com/api/auth/token';
url_protocols_str_prod 	= 'http://jervis.genalyte.com/api/protocols';
url_protocols_str_stag  = 'https://jervis-staging.genalyte.com/api/protocols';

url_sensor_str_prod   	= 'http://jervis.genalyte.com/api/tests/';                  % NOTE: caller will append: sensor number/sensors/exp_id
url_sensor_str_stag  	= 'https://jervis-staging.genalyte.com/api/tests/';

url_scan_str_prod     	= 'http://jervis.genalyte.com/api/sensors/';                % NOTE: caller will append: -3d',exp_sensor_struct(cur_sensor_ndx).id    );
url_scan_str_stag   	= 'https://jervis-staging.genalyte.com/api/sensors/';        % NOTE: caller will append: -3d',exp_sensor_struct(cur_sensor_ndx).id    );

url_probes_str_prod    	= 'http://jervis.genalyte.com/api/tests/';                  % NOTE: caller will append:  %s/probes',a_exp_id);
url_probes_str_stag    	= 'https://jervis-staging.genalyte.com/api/tests/';          % NOTE: caller will append: %s/probes',a_exp_id);

url_exp_det_str_prod  	= 'http://jervis.genalyte.com/api/tests/';                  % NOTE: caller will append:%s/metadata',char(a_exp_id));
url_exp_det_str_stag  	= 'https://jervis-staging.genalyte.com/api/tests/';         % NOTE: caller will append:%s/metadata',char(a_exp_id));


if (prod_stag_flag)
    % PRODUCTION
    token_str           = token_str_prod;
    url_login_str       = url_login_str_prod;
    url_protocols_str   = url_protocols_str_prod;
    url_sensor_str      = url_sensor_str_prod;
    url_scan_str        = url_scan_str_prod;
    url_probes_str      = url_probes_str_prod;
    url_exp_det_str     = url_exp_det_str_prod;
else
    % STAGING
    token_str           = token_str_stag;
    url_login_str       = url_login_str_stag;
    url_protocols_str   = url_protocols_str_stag;
    url_sensor_str      = url_sensor_str_stag;
    url_scan_str        = url_scan_str_stag;
    url_probes_str      = url_probes_str_stag;
    url_exp_det_str     = url_exp_det_str_stag;
end

url_struct                  = {};
url_struct.token_str        = token_str;
url_struct.url_login_str    = url_login_str;
url_struct.url_protocols_str= url_protocols_str;
url_struct.url_sensor_str   = url_sensor_str;
url_struct.url_scan_str     = url_scan_str;
url_struct.url_probes_str   = url_probes_str;
url_struct.url_exp_det_str 	= url_exp_det_str;

end % fn sa_set_production_vs_stagging_url_flag

