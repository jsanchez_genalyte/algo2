function [ list_ndx_filt_dir ] = ui_upd_state_after_popupmenu_probe( ui_state_struct,popupmenu_probe_sel_index )  % ui_state_struct,db_metad_set_struct   )
%UI_UPD_STATE_AFTER_POPUPMENU_PROBE updates the ori dir indices   **************NOI***************
%   Makes sure we have consistency between all the popup menus and what it gets processed: plotted/tos/analyzed.
%   Currently handling single selection: future: multiple selections using custom or multi checkmarks on pullups if possible from matlab GUI

% For the new current analyte update the rest of the popup  menu with the corresponding quantities: ie probe x/y
% first check that the selected analyte is different than the current one. If it is the same: get out of here (nothing needs to be done)
ui_probe_state_struct           = ui_state_struct.probe;
ui_probe_state_struct           = ui_probe_state_struct{1};

% convert popupmenu_probe_sel_index from being an ndx of the current list into an ndx of the orig list:
[ cur_ndx_ori ] = ui_cvt_popmenu_to_ori_cur_ndx(ui_probe_state_struct, popupmenu_probe_sel_index ); 

if (popupmenu_probe_sel_index  ~= ui_probe_state_struct.cur_ndx) % cur_ndx refers to the current list. 
    % USER SELECTED A NEW ANALYTE.
    probe_len_ori = size(ui_probe_state_struct.list_str_cel_ori,1);
    % check for special case: All
    if (strcmpi(ui_probe_state_struct.list_str_cel{popupmenu_probe_sel_index} ,'All'))
        % END USER SELECTED ALL: reset ndx to all:
        list_ndx_filt_dir        = ones(probe_len_ori,1);
        % CHECK if len matches len_ori: if it is the case: reset custom. Otherwhise custom is not part of the pary: nothing to reset.
        % jas_tbd:
        list_ndx_filt_dir(end)   = 0;  % all means no custom.
    else
        % INDIVIDUAL ANALYTE
        list_ndx_filt_dir = zeros(probe_len_ori,1);             % disable the rest
        list_ndx_filt_dir(cur_ndx_ori) = 1;                     % turn on the selected in ori coordinates!.
    end
    % DONE UPDATING THE DIRECT FILTER.
    
    
end % fn ui_upd_state_after_popupmenu_probe

% lefover code niu:
% for cur_ndx = 1:1:probe_len_ori
%     if ( ui_state_struct.probe.list_str_cel(cur_ndx) == list_ndx_filt_dir(cur_ndx) == )
%         % NO CHANGE: skip it
%         continue;
%     end
%
%     if ( ( ui_state_struct.probe.list_str_cel(cur_ndx) == 0 ) && (list_ndx_filt_dir(cur_ndx) == 1))
%         % CHANGE: Check if from 0 to 1
%         % find if any of the other need to be updated: how?
%
%     end
%     if ( ( ui_state_struct.probe.list_str_cel(cur_ndx) == 1 ) && (list_ndx_filt_dir(cur_ndx) == 0))
%         % CHANGE: Check if from 1 to 0
%         % find if any of the other need to be updated: how?
%
%     end
% end

