function [ probe_date_struct ] = ...
    create_date_struct_for_date_str_list( date_str_list )
%CREATE_STORAGE_FOR_EXCEL_SHEET   creates a struct with 3 arrays: Y,M,D from a list of dates
% the list of strings represents dates with resolution of at least y to day.
% sample call: date_str_list  = {'01/02/2003' ; '04/05/2006' ; '07/08/2009' ; '10/11/2012'};
% sample call: probe_date_struct = create_date_struct_for_date_str_list( date_str_list );
% sample result: probe_date_struct =  date_month: [4x1 double] = [	1    4       7       10 ]
%                                   % date_day:   [4x1 double] = [	2	 3       8       11 ]
%                                   % date_year: [4x1 double]  = [  2003 2006    2009    2012 ]        
probe_sens_cnt  = size(date_str_list,1);

date_month      = nan(probe_sens_cnt,1);
date_day        = nan(probe_sens_cnt,1);
date_year       = nan(probe_sens_cnt,1);
for cur_sens =1:1:probe_sens_cnt
    [Y, M, D ] = datevec(date_str_list{cur_sens});
    date_month(cur_sens)    = M;
    date_day(cur_sens)      = D;
    date_year(cur_sens)     = Y;    
end
field_1                 = 'date_month';	value_1 = { date_month  };
field_2                 = 'date_day';  	value_2 = { date_day 	};
field_3                 = 'date_year'; 	value_3 = { date_year	};
probe_date_struct = struct(field_1,value_1,field_2,value_2,field_3,value_3);
dbg_flag = true;
if (dbg_flag)
    max_display_cnt = min(10,probe_sens_cnt);
    for cur_sens =1:1:max_display_cnt
        temp = [   ...
            probe_date_struct.date_year(cur_sens) ...
            ,probe_date_struct.date_month(cur_sens) ...
            ,probe_date_struct.date_day(cur_sens) ...
            ,0 ...
            ,0 ...
            ,0 ];
        datestr(temp,'mm/dd/yyyy' )
    end
end
end % fn create_date_struct_for_date_str_list
% leftovers
%                                   % sample code: datestr(now,'mm/dd/yyyy' )
%                                   % sample code:  now_num = datenum(now);
%                                   % sample code:  datenum( datestr(now,'mm/dd/yyyy' ))
%                                   % sample code:  probe_datenum =nan(probe_sens_cnt,1);
%                                   % sample code:  probe_datenum(cur_sens) = now_num;

