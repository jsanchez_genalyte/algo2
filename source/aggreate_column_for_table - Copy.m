function [ aggr_struct,errror_message ] = aggreate_column_for_table( a_table,a_col_aggr_name,a_col_data_name_list )
%AGGREATE_COLUMN_FOR_TABLE  agregates several data columns of a table give one column used as aggregation criteria.
%   Generic function that uses any table with any column names and produces the aggreation, the indices and the stats.
%   Input:
%   1. a_table              data table
%   2. a_col_aggr_name      name of the column of the table to build the aggreations    (i.e. spotl) (i.e. the pivot)
%   2.a_col_data_name_list  name of the data columns of the table to do the aggreations (i.e. a list of the 16 analytes).
%   Output:
%   1. aggr_struct: Array of structures (one element per name in the a_col_aggr_name_list) with:
%                   data_aggr_mat:       data for the given column  used to build the aggregations.
%                   data_aggr_o_mat:     data for: scatter odd
%                   data_aggr_e_mat:     data for: scatter  even
%                   name_ana:            clean name of the given analyte (clean enough to be able to be used in plot labels/titles)
%                   col_values_cat_u:    unique list of names for the given a_col_data_name
%                   data_sum_stats:      marix with 4 cols and as many rows as unique values: n,mean,std and cv ussing the corresponding data.
%   2. a index_set_table with:
%           1 - as many columns as unique values for the given column of the original table
%           2 - each element is a logical: 0/1 on whether the column value row matches the corresponding value for this column.
%   2. data_sum_stats

% assay_ana_header = {'hsa','dsdna','hsa3','jo_1','sm','hsa6','scl_70','hsa8','cenp_b','hsa10','ss_a_60','hsa12','ss_b','rnp','anti_igg','human_igg'};
% assay_ana_header = {'hsa','dsdna' } % small test
%   sample call:
% [ aggr_struct,errror_message ] = aggreate_column_for_table( assay_sum_table,'spotl',assay_ana_header )
% [ aggr_struct,errror_message ] = aggreate_column_for_table( assay_sum_table,'instr',assay_ana_header )

aggr_struct                     = struct;
errror_message                  = '';

a_table_header = a_table.Properties.VariableNames;

% find the column indexes  on the table for the wanted columns

wanted_data_size    = length(a_col_data_name_list);
ndx_data_col_list   = nan(wanted_data_size,1);
all_data_size       = height(a_table);

for cur_data_name_ndx = 1:1:wanted_data_size
    a_col_data_name = a_col_data_name_list{cur_data_name_ndx};
    [column_data_found_flag,a_column_data_ndx]= is_element_in_cell(a_table_header,a_col_data_name);
    % CHECK THAT THE COLUMN IS ONE THE DATA COLUMNS FOR THE TABLE
    if (~(column_data_found_flag))
        % COLUMN DATA IS NOT ON THE TABLE: get out
        errror_message =sprintf('Invalid data column name %s not found in data table',a_col_data_name);
        return;
    end
    ndx_data_col_list(cur_data_name_ndx) = a_column_data_ndx;
    
end

% Do the same for the aggregation colum: i.e. the spotl

[column_aggr_found_flag,a_column_aggr_ndx]= is_element_in_cell(a_table_header,a_col_aggr_name);
if (~(column_aggr_found_flag))
    % COLUMN IS NOT ON THE TABLE: get out
    errror_message =sprintf('Invalid aggregation column name %s not found in data table',a_col_aggr_name);
    return;
end

% FOUND BOTH COLUMNS ON THE TABLE: the aggregation and all the data columns.


col_values_table    = a_table(:,a_column_aggr_ndx);
if (strcmp(char(col_values_table.Properties.VariableNames),'qc_rack_number'))
    % SPECIAL CASE: qc_rack numbers comes as cells.
    col_values_cat      = categorical(col_values_table.qc_rack_number);
else
    % REGULAR CASE: analyte names: strings.
    col_values_cat    	= categorical(table2cell(((a_table(:,a_column_aggr_ndx)))));
end

col_values_cat_u   	= unique(col_values_cat);
a_col_values_cnt   	= length(col_values_cat_u);

% Produce indices to traverse odd and even data.
odd_ch_ndx              = 1:2:all_data_size;
even_ch_ndx             = 2:2:all_data_size;
ndx_all                 = false(all_data_size,1);
ndx_odd                 = ndx_all;
ndx_even                = ndx_all;
ndx_odd(odd_ch_ndx)     = 1;
ndx_even(even_ch_ndx)   = 1;

% build index for each unique value of the aggregation column (i.e. spotl)
col_aggr_mat_ndx                        = nan(all_data_size,a_col_values_cnt);
for cur_value_ndx = 1:1:a_col_values_cnt
    cur_value                           = col_values_cat_u(cur_value_ndx);
    col_aggr_mat_ndx(:,cur_value_ndx)   = col_values_cat ==  cur_value;
    col_cnt_list                        = sum(col_aggr_mat_ndx);
end

% fill in the matrix: data_aggr_mat with data arranged this way:
%                     each column:  one unique value for this column.
largest_col_aggr_len    = max(sum(col_aggr_mat_ndx));

%aggr_struct             =   cell(wanted_data_size);

for cur_ana_ndx=1:1:wanted_data_size % 6
    % CALCULATE DATA FOR LOT_2_LOT PLOT FOR CURRENT ANALYTE:
    data_aggr_mat       = nan(largest_col_aggr_len,a_col_values_cnt);
    data_aggr_o_mat  	= nan(largest_col_aggr_len,a_col_values_cnt);
    data_aggr_e_mat     = nan(largest_col_aggr_len,a_col_values_cnt);
    data_sum_stats      = nan(4,a_col_values_cnt);
    for cur_value_ndx = 1:1:a_col_values_cnt
        ndx_cur                     = logical(col_aggr_mat_ndx(:,cur_value_ndx));   % col_aggr_mat_ndx(1:col_cnt_list(cur_value_ndx))';
        ndx_cur_odd                 = ndx_cur;
        ndx_cur_odd(ndx_even)       = 0; % for the odd: reset the even
        ndx_cur_even                = ndx_cur;
        ndx_cur_even(ndx_odd)       = 0; % for the even: reset the odd
        temp_rows                   = 1:col_cnt_list(cur_value_ndx);
        temp_rows                   = temp_rows';
        temp_data                   = table2array(a_table(:,ndx_data_col_list(cur_ana_ndx)));
        temp_ndx                    = col_aggr_mat_ndx(:,     cur_value_ndx);
        
        temp_all_ndx                = logical(temp_ndx);
        temp_all_ndx (~ndx_cur)     = 0;  % exclude outisders.
        if (sum(iscell(temp_data(logical(temp_all_ndx)))))
            fprintf('found a cell\n')
        end
        data_aggr_mat( temp_rows,           cur_value_ndx) = temp_data(logical(temp_all_ndx));
        temp_odd_ndx                                = temp_ndx;
        temp_odd_ndx(~ndx_cur_odd)                  = 0;  % exclude outisders.
        data_aggr_o_mat(1:sum(ndx_cur_odd), cur_value_ndx) = temp_data(logical(temp_odd_ndx));
        temp_even_ndx                               = temp_ndx;
        temp_even_ndx(~ndx_cur_even)                = 0;  % exclude outisders.
        data_aggr_e_mat(1:sum(ndx_cur_even),cur_value_ndx)= temp_data(logical(temp_even_ndx));
        % store results:
        aggr_struct(cur_ana_ndx).data_aggr_mat      = data_aggr_mat;
        aggr_struct(cur_ana_ndx).data_aggr_o_mat    = data_aggr_o_mat;    % scatter odd
        aggr_struct(cur_ana_ndx).data_aggr_e_mat    = data_aggr_e_mat;    % scatter even
        aggr_struct(cur_ana_ndx).name_ana           = strrep(a_col_data_name_list{cur_ana_ndx},'_','-') ;
        aggr_struct(cur_ana_ndx).col_values_cat_u   = col_values_cat_u;
        
        % calculate summary stats for current analyte: data_sum_stats  = nan(4,spotl_cnt);
        data_sum_stats(1,cur_value_ndx)             = sum(isfinite((data_aggr_mat(temp_rows,cur_value_ndx))));               % N
        data_sum_stats(2,cur_value_ndx)             = nanmean(data_aggr_mat(temp_rows,cur_value_ndx));                       % mean
        data_sum_stats(3,cur_value_ndx)             = nanstd(data_aggr_mat(temp_rows,cur_value_ndx));                        % std
        data_sum_stats(4,cur_value_ndx)             = 100 * data_sum_stats(3,cur_value_ndx) /data_sum_stats(2,cur_value_ndx);% CV
        aggr_struct(cur_ana_ndx).data_sum_stats     = data_sum_stats';
    end
end

end % aggreate_column_for_table

