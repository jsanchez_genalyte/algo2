clc;
clear;
bkgr_image_orig_jpeg    = imread('frame1.jpg'); %'C:\Users\jsanchez\Documents\algo2\ml_test_algo\2018_test_files\test_1.jpg');  % 'frame1.jpg');
foreg_image_orig_jpeg 	= imread('frame2.jpg'); %'C:\Users\jsanchez\Documents\algo2\ml_test_algo\2018_test_files\test_1.jpg');            % 'frame2.jpg');
subplot(3,3,1);
imshow(bkgr_image_orig_jpeg);
title('BackGr Frame 1');

subplot(3,3,2);
imshow(foreg_image_orig_jpeg);
title('ForgeGr Frame 2');

gray_foreGr_img = rgb2gray(foreg_image_orig_jpeg);
BW = im2bw(gray_foreGr_img);
subplot(3,3,3);
imshow(BW)
title('ForeGr im2bw');

gray_bkgr_image     = rgb2gray(bkgr_image_orig_jpeg);
foregroundDetector  = vision.ForegroundDetector('InitialVariance',(30/255)^2);
foreground          = step(foregroundDetector, gray_bkgr_image);
subplot(3,3,4);
imshow(foreground);
title('foreground frame 1');
foreground1         = step(foregroundDetector, gray_foreGr_img);
subplot(3,3,5);
imshow(foreground1);
title('foreground frame 2');
BlobAnalysis        = vision.BlobAnalysis('MinimumBlobArea',100,'MaximumBlobArea',50000);
[area,centroid,bbox]= step(BlobAnalysis,foreground1);
Ishape              = insertShape(foreg_image_orig_jpeg,'rectangle',bbox,'Color', 'green','Linewidth',6);
subplot(3,3,6);
imshow(Ishape);
title('Detect');
subplot(3,3,7);
title('Histogram');
[row , col ]    = size (bbox);
for i =1 : row
    x = bbox(i,1);
    y = bbox(i,2);
    w = bbox(i,3);
    h = bbox(i,4);
    TestImage   = foreg_image_orig_jpeg(y :(y+h),x:(x+w), :);
    r           = TestImage(:,:,1);
    g           = TestImage(:,:,2);
    b           = TestImage(:,:,3);
    histogram2(r,g,'DisplayStyle','tile','ShowEmptyBins','on', ...
        'XBinLimits',[0 255],'YBinLimits',[0 255]);
    histogram(r,'BinMethod','integers','FaceColor','r','EdgeAlpha',0,'FaceAlpha',1)
    hold on
    histogram(g,'BinMethod','integers','FaceColor','g','EdgeAlpha',0,'FaceAlpha',0.7)
    histogram(b,'BinMethod','integers','FaceColor','b','EdgeAlpha',0,'FaceAlpha',0.7)
    xlabel('RGB value')
    ylabel('Frequency')
    title('Color Histogram')
    xlim([0 257])
    thresholding =128;
    rth     = graythresh(TestImage(:,:,1))*255
    gth     = graythresh(TestImage(:,:,2))*255
    bth     = graythresh(TestImage(:,:,3))*255
    if ( rth*gth*bth >= thresholding )
        msgbox('Alarm it is white !');
        load gong.mat;
        while ( rth*gth*bth >= thresholding)%endless loop
            sound(y);
            lastTime = clock;
            while etime(clock, lastTime) < 5
                pause(0.03);
            end
            break;
        end
    else
        msgbox('ok');
    end
    clear TestImage;
end