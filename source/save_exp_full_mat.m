function [ local_hd_struct,error_message ] = save_exp_full_mat(local_hd_struct,cur_expid,exp_full_struct)
%SAVE_EXP_FULL_MAT saves the full exp data for 1 whole experiment into a mat file (exp,sensor,probe, and scan(?))
%   Aligns (replicates) 2 matrices: the sensor_excel_table (one record per sensor) and tghe scan_data: ~ 110 records per sensor
%   The name of the mat file is whatever comes in local_hd_struct
%   Input:
%   local_hd_struct        file_names and tab_names.
%   expe_ned_table:        table with one row per experiment requested.
%   sensor_excel_table:    table with experiment,probe and sensor level info: as many rows as sensors.
%                          each row will be repeated for each scan of the given sensor
%   scan_data_struct:      scan matrix to be dump in excel next to the sensor_excel_table

% see:          local_hd_struct deined in sa_do_push_top_sens
% pipeline:     Sensogram_Analysis_App_10 --> pushbutton_top_sens_Callback  --> sa_do_push_top_sens --> sa_rest_db_set_get_all_exp_full_data --> save_exp_full_mat
% sample call:  [ local_hd_struct,error_message ] = sa_rest_exp_save_scan(local_hd_struct,expe_ned_table,sensor_excel_table,scan_data_struct);
error_message = '';
local_hd_db_data_exp_full_mat_dir   = local_hd_struct.local_hd_db_data_expr_scan_mat_dir;   % 'C:\Users\jsanchez\Documents\algo2\db_data_scan\';
db_data_exp_full_file_name          = strcat(local_hd_db_data_exp_full_mat_dir,cur_expid,'_exp.mat');

protocol_struct   = exp_full_struct.protocol_struct;
exp_det_struct    = exp_full_struct.exp_det_struct;
exp_probe_struct  = exp_full_struct.exp_probe_struct;
exp_sensor_struct = exp_full_struct.exp_sensor_struct;
%probe_cnt                          = probe_scan_struct.probe_cnt;
% HAVE exp_full data_mat save it into a matlab binary file:

save                                 (db_data_exp_full_file_name...
    ,'protocol_struct'                  ...
    ,'exp_det_struct'                   ...
    ,'exp_probe_struct'     ...
    ,'exp_sensor_struct'   );

% HAVE scan data_mat save it into a matlab binary file:
%jas_tbd: Once debug the rest of the code: then add scan:   not here!. 
%        [ error_message ] = sa_rest_exp_save_scan(local_hd_struct,sensor_scan_struct,cur_expid)
end % fn save_exp_full_mat
%

