function [ req_date_clean_str ] = clean_up_date_range( edit_req_date_str )
%CLEAN_UP_DATE_RANGE performs the following clean up
%   Eliminates blanks at beginning and end
%   Eliminates quotes (single or double)
%   Converts multiple blanks into single blank
%   Handles one or more than one date-time: one per row in the input.
%   

req_date_clean_str = edit_req_date_str;
num_dates = size(edit_req_date_str,1);
cur_date_use_ndx=0;
for cur_date_ndx=1:1:num_dates
    if (size(edit_req_date_str(cur_date_ndx,:),2) > 0)
        cur_date_use_ndx=cur_date_use_ndx+1;  
        names_clean = strrep(edit_req_date_str(cur_date_ndx,:),',','');   % commas into blanks 
        names_clean = strrep(names_clean,'  ',' ');
        names_clean = strrep(names_clean,'  ',' ');   % up to 4 blanks into 1 blank
        %names_clean = strrep(names_clean,' ', spaces_to_single_char);
        % get rid of blanks at the beginning:  
        for cur_ndx =1:1:length(names_clean)
            if (isspace(names_clean(1)))
                names_clean=names_clean(2:end);
            else
                % FOUND THE FIRST NON BLANK: Get out.
                break;
            end
        end
        for cur_ndx =length(names_clean):-1:2  %jas_len = 1
            if (isspace(names_clean(end)))
                names_clean=names_clean(1:end-1);
            else
                % FOUND THE FIRST NON BLANK AT THE END: Get out.
                break;
            end
        end
        req_date_clean_str(cur_date_use_ndx,:)=names_clean;
    end    
end

end %fn clean_up_date_range

