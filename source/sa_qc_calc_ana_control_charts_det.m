function [ qc_control_charts_det_struct,error_message ] = sa_qc_calc_ana_control_charts_det( assay_det_table,assay_ana_header )
%SA_QC_CALC_LOT_2_LOT  calculates the qc_control_charts_det_struct for the auto qc report: ana_control_charts_det


qc_control_charts_det_struct            = struct;
error_message                           = '';
blank_ndx                               = isnan(assay_det_table.dsdna);                                      % jas: temporary way to eliminate missing data records
assay_det_table                         = assay_det_table(~blank_ndx,:);
assay_ana_header_ext                   	= assay_ana_header;
assay_ana_header_ext{end+1}             = 'chann';
if (sum(iscell(assay_det_table.chann)))
    assay_det_table.chann   = cell2mat(assay_det_table.chann);
end
[qc_lot_2_lot_det_struct,error_message] = aggreate_column_for_table( assay_det_table,'spotl',assay_ana_header_ext,'sensor' ); % *** 1 aggreagte ****
if (~(isempty(error_message)))
    return;
end
col_chann_both                          = size(qc_lot_2_lot_det_struct(1).data_aggr_mat,3); % third dim is for both channels.
ana_cnt                                 = size(qc_lot_2_lot_det_struct,2)-1;  % substract the last one: the chann
for cur_ana_ndx=1:1:ana_cnt % 16
    % PROCESS CURRENT ANALYTE:  both
    spotl_names_list    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    data_aggr_mat       = qc_lot_2_lot_det_struct(cur_ana_ndx).data_aggr_mat(:,:,col_chann_both);
    col_values_cat_u    = qc_lot_2_lot_det_struct(cur_ana_ndx).col_values_cat_u;
    ana_lot_aggr_table  = array2table(data_aggr_mat);
    spotl_cnt           = size(col_values_cat_u,1)  ;            % cnt of unique spotl categories
    for cur_spotl_ndx=1:1:spotl_cnt
        % set the column names: name of the spotl: WARNING: when a lot number starts with a number: it is not a valid variable name
        % so: preapend the lot number with LOT_  so they become valid
        lot_numbers_list_ori_str                                = char(spotl_names_list);  %   cellstr(char(spotl_names_list));
        lot_numbers_list_ext_cel                                = cell(size(spotl_names_list)); % (lot_numbers_list_ori_str;
        for cur_lot_name_ndx = 1:1:size(lot_numbers_list_ori_str,1)
            lot_numbers_list_ext_cel{cur_lot_name_ndx}          = strcat('lot_',lot_numbers_list_ori_str(cur_lot_name_ndx,:));
        end
        ana_lot_aggr_table.Properties.VariableNames             = lot_numbers_list_ext_cel;
        chan_cnt                                                = size(qc_lot_2_lot_det_struct(cur_ana_ndx).data_aggr_mat,3); % third dim is for both channels.
        for cur_chan_ndx =1:1:chan_cnt
            % RETRIEVE THE DATA FOR EACH CHANNEL: break it down into each sensor
            temp_rack_tabl                                      = array2table(qc_lot_2_lot_det_struct(end).data_aggr_mat(:,:,cur_chan_ndx));
            if (cur_chan_ndx > chan_cnt)
                % DO IT FOR BOTH CHANNELS
                ana_lot_aggr_table_ext                          = [ ana_lot_aggr_table(:,cur_spotl_ndx)  temp_rack_tabl(:,cur_spotl_ndx) ];
            else
                % INDIVIDUAL CHANNES:
                data_aggr_mat                                   = qc_lot_2_lot_det_struct(cur_ana_ndx).data_aggr_mat(:,:,cur_chan_ndx);
                ana_lot_aggr_table                              = array2table(data_aggr_mat);
                ana_lot_aggr_table.Properties.VariableNames     = lot_numbers_list_ext_cel;
                ana_lot_aggr_table_ext                          = [ ana_lot_aggr_table(:,cur_spotl_ndx)  temp_rack_tabl(:,cur_spotl_ndx) ];              
            end
            ana_lot_aggr_table_ext.Properties.VariableNames{end}= 'chann';
            % remove nans
            remove_ndx                                          = isnan(ana_lot_aggr_table_ext.chann);
            if ( sum(remove_ndx))
                % SOME NANS: remove them.
                ana_lot_aggr_table_ext                          = ana_lot_aggr_table_ext(~remove_ndx,:);
            end
            cur_spotl_ext_name                                  = strcat('lot_',char(qc_lot_2_lot_det_struct(end).col_values_cat_u(cur_spotl_ndx)));
            %                                                                                                                    % *** 2 aggreagte ****
            [ ana_lot_rack_struct,error_message ]               = aggreate_column_for_table( ana_lot_aggr_table_ext,'chann',{ cur_spotl_ext_name},'');
            if (~(isempty(error_message)))
                return;
            end
            % store results:
            qc_control_charts_det_struct(cur_ana_ndx,cur_spotl_ndx,cur_chan_ndx).ana_lot_rack_struct  = ana_lot_rack_struct;
        end % cur_chan_ndx
    end % for each spotln
end % for each analye: calc data to plot and summary
save('qc_control_charts_det_struct.mat','qc_lot_2_lot_det_struct','qc_control_charts_det_struct');


publish('sa_do_plot_ana_control_charts_det_url.m'...
    , 'figureSnapMethod','getframe' ... %  'entireFigureWindow'...
    ,'useNewFigure',true...
    ,'showCode' ,false ...
    );
% The publish command executes the code for each cell in sa_do_plot_ana_rack_2_rack_url.m, and saves the file to /html/sa_do_plot_ana_lot_2_lot_url.html.
% View the HTML file.
web('html/sa_do_plot_ana_control_charts_det_url.html');

end % fn sa_qc_calc_rack_2_rack