function [ handles ] = mpopup_menu_reset_all_to_all( handles )
%MPOPUP_MENU_RESET_ALL_TO_ALL resets to all all the mpopup menust
%   THis is a fake: does nothing. see mpopup_menu_reset_all_to_all_ALL 

% TURN ON ALL: Means turn on all for all other menus (it is like start all over
% handles.ui_state_struct.probe{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.dates{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.proto{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.sampl{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.chipl{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.exper{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.instr{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.carri{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.chann{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.clust{1}.list_item_slected(1:end) = 1;
% handles.ui_state_struct.probe{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.dates{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.proto{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.sampl{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.chipl{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.exper{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.instr{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.carri{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.chann{1}.list_item_slected(2:3)   = 0;
% handles.ui_state_struct.clust{1}.list_item_slected(2:3)   = 0;
% 
% handles.ui_state_struct.probe{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.dates{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.proto{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.sampl{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.chipl{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.exper{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.instr{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.carri{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.chann{1}.cur_gui_ndx = 1;
% handles.ui_state_struct.clust{1}.cur_gui_ndx = 1;

end

