function [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla( a_request_vanila_str )
%SA_DO_PARSE_STR_REQUEST_VANILLA handles parsing a vanilla request
%   a vanilla request is either:
%   1 - a single string (with no wilde chars)
%   2 - a single string (with wilde chars)
%   3 - a comma separated list of single strings
% returns a cell array of strings
% sample call  [ a_request_cell_str,wild_array_flag ] = sa_do_parse_str_request_vanilla( 'exp_1, exp_2, exp  3333')
comma_pattern       = ','; % comma is the sepparator in a list of experiments,lots etc.
semic_pattern       = ';'; %
wildc_pattern       = '*'; %
%a_request_vanila_str= 'exp_1, exp_2, exp_3333';
temp_str            = replace(a_request_vanila_str,comma_pattern,semic_pattern);
str_ndx             = strfind(temp_str,semic_pattern);
wild_array_flag     = ones(1,1);
if (isempty(str_ndx))
    % NO COMMAS IN STRING: single token
    a_request_cell_str = cellstr(temp_str);
    % check for wild chars:
    str_wild_ndx  = strfind(temp_str,wildc_pattern);
    if (isempty(str_wild_ndx))
        wild_array_flag(1,1) = 0;
    end
else
    % COMMAS IN STRING: multiple token: break them into a cellstr
    a_request_cell_str = cell(length(str_ndx)+1,1);
    wild_array_flag    = ones(length(str_ndx)+1,1);
    pre_pos = 1;
    cur_pos = 1;
    for cur_ndx =1:length(str_ndx)
        % PROCESS CURRENT TOKEN:
        cur_pos                     = str_ndx(cur_ndx)-1;
        cur_str                     = temp_str(pre_pos:cur_pos);
        a_request_cell_str{cur_ndx} = cur_str;
        % check for wild chars:
        str_wild_ndx  = strfind(cur_str,wildc_pattern);
        if (isempty(str_wild_ndx))
            wild_array_flag(cur_ndx,1) = 0;
        end
        pre_pos = cur_pos+2; %move the nxt but skip the comma. 
    end
    % process the last token:
    cur_ndx                         = cur_ndx+1;
    cur_str                         = temp_str(pre_pos:end); % get last token
    a_request_cell_str{cur_ndx}     = cur_str;
    % check for wild chars:
    str_wild_ndx  = strfind(cur_str,wildc_pattern);
    if isempty (str_wild_ndx )
        wild_array_flag(cur_ndx,1)    = 0;
    end
end
final_len = length(wild_array_flag);
% cleanup empty tokens:
for cur_ndx=1:length(wild_array_flag)
    if (isempty(a_request_cell_str{cur_ndx}))
        % FOUND EMPTY ONE: MOVE UP from nxt to the end
        final_len = final_len-1;
        if (cur_ndx == length(wild_array_flag))
            % AT THE END: get out
            break
        end
        % EMPTY ONE AND NOT AT THE END: move up:
        for cur_move_ndx=cur_ndx:length(wild_array_flag)-1
            wild_array_flag( cur_move_ndx)     = wild_array_flag(cur_move_ndx+1);
            a_request_cell_str( cur_move_ndx)  = a_request_cell_str(cur_move_ndx+1);
        end
    end
end
if (final_len < length(wild_array_flag) )
    % NEED TO TRIM: Cut the empty ones
    wild_array_flag      = wild_array_flag(1:final_len);
    a_request_cell_str = a_request_cell_str(1:final_len);
end


end % fn sa_do_parse_str_request_vanilla