% Initialize web service
% query: select * from wo_consumption where inv_lotnum
%        

webservice_url = 'http://10.0.2.226/runcard_test/soap?wsdl';
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
methods(obj)
partnum = '';
partrev = '';
lotnum = 'AB2010';
serial = '';
workorder = '';
opcode = '';
seqnum = '';
whereUsed = 0;
[response, error, msg] = fetchUnitGenealogy(obj,partnum, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);
if error ~= 0
    disp(['RunCard exception encountered for fetchUnitGenealogy(): ' msg]);
    return
else
    disp(msg);

end 
% try_1
partnum = '';
partrev = '';
lotnum = '';
serial = '';
workorder = 'MW0-0092';
opcode = '';
seqnum = '';
whereUsed = 0;
[spotting_lot_list, error, msg] = fetchUnitGenealogy(obj,partnum, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);
whereUsed = 0;
workorder = 'MWO-0083';
lotnum = '';
[spotting_lot_parent, error, msg] = fetchUnitGenealogy(obj,partnum, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);
spotting_lot_parent_table = struct2table(spotting_lot_parent);

% get unique set of workorder
% for each workorder do query_2:

% try_2
partnum = '00446';
partrev = '';
lotnum = '';
serial = '';
workorder = 'MWO-0058';
opcode = '';
seqnum = '';
whereUsed = 0;
[mb_lot_parent, error, msg] = fetchUnitGenealogy(obj,partnum, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);
mb_lot_parent_table = struct2table(mb_lot_parent);
% join the 2 tables by lotnum column.

% runcard BOM consumption history: run_card_test:
% filter: Work Order ='MWO-0092' and  BOM Part Number:  = '00445'

'B1500BQ'               % MB serial
'00446'                 % MB part number
'01'                    % MB part rev 
'AB2010'                % MB lot number 
'MWO-0092'              % MB work order
'30'                    % step
'A320'                  % opcode
'aosorio'               % operator
'00445'                 % BOM part number  spotted chip part number  ****
'01'                    % BOM part rev 
'P162142.24.0370'       % BOM serial       spotted chip serial_number				
'AB150I'                % BOM lot id       spotted lot id            ****
'1'                  	% BOM quantity
'2018-05-17 12:20:04' 	% date


webservice_url = 'http://10.0.2.226/runcard_test/soap?wsdl';
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
%methods(obj)
partnum = '00445'; % spotted lot chips _part_number
partrev = '';
lotnum = '';
serial = '';
workorder = '';
opcode = '';
seqnum = '';
whereUsed = 1; % parent records: ie: all mb 
[mb_all_list_response, error, msg] = fetchUnitGenealogy(obj,partnum, partrev,lotnum,serial,workorder,opcode,seqnum,whereUsed);

mb_all_list_table = struct2table(mb_all_list_response);
mb_partnum_cat    = categorical(mb_all_list_table.partnum);

a_chips_ndx_list = mb_partnum_cat == 'QCP0023-QC'; % '00446'; % ie spoted chips part of the mb. 
 sum(a_chips_ndx_list)  % records 200   989
mb_sp_list_table= mb_all_list_table(a_chips_ndx_list,:);

spotl_cat    = categorical(mb_sp_list_table.bom_lotnum);
a_chips_sl_lot_ndx_list = spotl_cat == 'AB1602';  % % chips belong to sp lot number 
sum(a_chips_sl_lot_ndx_list) %  100 records %579
mb_sp_table  =  mb_sp_list_table(a_chips_sl_lot_ndx_list,:);        

unique(spotl_cat)
