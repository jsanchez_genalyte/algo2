function  [expe_req_table, error_message ]  = v1_sa_rest_exp_get_all(start_date,end_date,exp_name,rest_token ) % FOLLOW UP: Debug it with John. It errors out.
%sa_rest_exp_get_all retrieves the list of ALL available experiments from Jervis within the date range and a given exp_name Data Service Johns:  API_5: indexTests
% Input: start_date: if blank: means from first exp. Format: mm/dd/yyyy
%        end_date:   if blank: means up o current.   Format: mm/dd/yyyy
%        exp_name:   if blank: all experiments. In reallity exp_name could be a spotting lot number, or anything else....
%        rest_token: token from previous connection
%  returns:
%   1 expe_req_table:    a table with a list of experiments availabe that meet the date range and exp name. If none found: exp_list is returned: nan.
%                       expe_req_table  with  fileds:
%                       jas_tbd: id,name and maintenance flag
%                       exp_list_cel(1,:)         % =   1�9 cell array
%     '28ec02fe-7bd4-4f54-be32-5f751c1287ef'      % column 1 exp_id
%     'complete'                                  % column 2 exp_status
%     [1]                                         % column 3 exp_xxx
%     '4cdbc37c-3325-443c-a903-b55d70cbdbdd'      % column 4 exp_protocol_id: errors out in stagging
%     'M53'                                       % column 5 exp_device_id
%     {2�1 cell}                                  % column 6 sample_id: Usually 2 tbd.
%     [1�1 struct]                                % column 7 exp name, chip_spot_lot etc... (the good stuff)
%     '2017-07-21T05:57:45.994-07:00'             % column 8 exp_run_date:   11/21/2017 12:00:31pm
%     '2017-07-21T06:00:27.163-07:00'             % Column 9 exp_run_date_2: modified date?
%
%   2 error_message:    either an empty string error message if everything ok, or some kind of diagnostic.
% sample call:    [ expe_req_table,  error_message ]  = sa_rest_exp_get_all('2017-07-20 5:00','2017-07-22 7:00','Jervis ANA Validation', handles.rest_token);

expe_req_table       = table();
%url_experiments_str = 'http://jervis-staging.genalyte.com/api/tests/';
url_experiments_str = 'http://jervis.genalyte.com/api/tests/';
options             = weboptions('HeaderFields',{'Content-Type', 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token ); 'RequestMethod', 'post';'ArrayFormat','json'});

% creates a weboptions object that contains two header fields: Content-Length with value 78 and Content-Type with value application/json.
%options = weboptions('Authorization', strcat('Bearer', ' ', rest_token ));   %  ,'ContentType','text'
try
    error_message       = '';
    exp_list            = nan;  % '2017-07-20 5:00','end_date','2017-07-22 7:00
    if ((~(isempty(start_date))) &&  (~(isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % PASSED THE 3 ARGUMENTS: Full query
        exp_list                = webread(url_experiments_str,'start_date',start_date,'end_date',end_date,'q',exp_name,options);
        if (size(exp_list,1) == 100)
            exp_list_2      	= webread(url_experiments_str,'start_date',start_date,'end_date',end_date,'q',exp_name,options,'page','2');
            if (~(isempty(exp_list_2)))
                exp_list_2_size                 =  size(exp_list_2,1);
                exp_list(101:100+exp_list_2_size)   = exp_list_2;
                % jas_temp: handling only up to 200 experiments ... jas_tbd.
            end
        end % > 100 exp_list size
    end
    if ((~(isempty(start_date))) &&  ((isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % PASSED ONLY 2 ARGUMENTS: 1 0 1
        exp_list               = webread(url_experiments_str,'start_date',start_date,'q',exp_name,options);
        if (size(exp_list,1) == 100)
            exp_list_2      	= webread(url_experiments_str,'start_date',start_date,'q',exp_name,options,'page','2');
            if (~(isempty(exp_list_2)))
                exp_list_2_size                 =  size(exp_list_2,1);
                exp_list(101:100+exp_list_2_size)   = exp_list_2;
                % jas_temp: handling only up to 200 experiments ... jas_tbd.
            end
        end % > 100 exp_list size
        
    end
    
    if (((isempty(start_date))) &&  (~(isempty(end_date))) &&  (~(isempty(exp_name)))  )
        % PASSED ONLY 2 ARGUMENTS: 0 1 1
        exp_list                = webread(url_experiments_str,'end_date',end_date,'q',exp_name,options);
        if (size(exp_list,1) == 100)
            exp_list_2      	= webread(url_experiments_str,'end_date',end_date,'q',exp_name,options,'page','2');
            if (~(isempty(exp_list_2)))
                exp_list_2_size                 =  size(exp_list_2,1);
                exp_list(101:100+exp_list_2_size)   = exp_list_2;
                % jas_temp: handling only up to 200 experiments ... jas_tbd.
            end
        end % > 100 exp_list size
    end
    
    if ((~(isempty(start_date))) &&  ((isempty(end_date))) &&  ((isempty(exp_name)))  )
        % PASSED ONLY 1 ARGUMENTS: 1 0 0
        exp_list               = webread(url_experiments_str,'start_date',start_date,options);
        if (size(exp_list,1) == 100)
            exp_list_2      	= webread(url_experiments_str,'start_date',start_date,options,'page','2');
            if (~(isempty(exp_list_2)))
                exp_list_2_size                 =  size(exp_list_2,1);
                exp_list(101:100+exp_list_2_size)   = exp_list_2;
                % jas_temp: handling only up to 200 experiments ... jas_tbd.
            end
        end % > 100 exp_list size
    end
    
    if (((isempty(start_date))) &&  (~(isempty(end_date))) &&  ((isempty(exp_name)))  )
        % PASSED ONLY 1 ARGUMENTS: 0 1 0
        exp_list                = webread(url_experiments_str,'end_date',end_date,options);
        if (size(exp_list,1) == 100)
            exp_list_2      	= webread(url_experiments_str,'end_date',end_date,options,'page','2');
            if (~(isempty(exp_list_2)))
                exp_list_2_size                 =  size(exp_list_2,1);
                exp_list(101:100+exp_list_2_size)   = exp_list_2;
                % jas_temp: handling only up to 200 experiments ... jas_tbd.
            end
        end % > 100 exp_list size
    end
    
    if (((isempty(start_date))) &&  ((isempty(end_date))) &&  ((isempty(exp_name)))  )
        % PASSED NO ARGUMENTS: set a warning: nothing will be returned
        fprintf('warning_:missing_filter. Nothing will be returned');
        return;
    end
    
    if ((~(isempty(start_date))) &&  (~(isempty(end_date))) &&  ((isempty(exp_name)))  )
        % PASSED ONLY 2 ARGUMENTS: 1 1 0 date and blank experiment
        exp_list               = webread(url_experiments_str,'start_date',start_date,'end_date',end_date,options);
        if (size(exp_list,1) == 100)
            exp_list_2      	= webread(url_experiments_str,'start_date',start_date,'end_date',end_date,options,'page','2');
            if (~(isempty(exp_list_2)))
                exp_list_2_size                 =  size(exp_list_2,1);
                exp_list(101:100+exp_list_2_size)   = exp_list_2;
                % jas_temp: handling only up to 200 experiments ... jas_tbd.
            end
        end % > 100 exp_list size
    end
    
catch
    % COULD NOT READ EXPERIMENTS: Failure of webread.
    error_message_1	= sprintf('\nERROR_ RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE: during webread.  \nFunction: sa_rest_exp_get_all for start_date=%s, end_date=%s and exp_name=%s'...
        ,start_date,end_date,char(exp_name));
    error_message_2	= sprintf('\nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.\n');
    error_message	= strcat(error_message_1,error_message_2);
    disp(error_message);
    return;
end

if (~(isempty(error_message)))
    % PROBABLY: Missing filters: I.E. all empty
    disp(error_message);
    return;
end

if (isempty(exp_list))
    % NO EXPERIMENTS FOUND: No error but nothing found
    error_message       = 'warning_:no_experiments_found';
    disp(error_message);
    return;
end

if ( (isfield(exp_list,'id')) && (isfield(exp_list,'state')) && (isfield(exp_list,'control')) )
    % OK RESPONSE FROM THE WEB SERVICE: API exp list
else
    error_message = fprintf('\nERROR_ RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE: webread. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support.');
    return;
end

% TRANSFORM DATA SO IT CAN BE CONSUMED AS A TABLE WITH THE FIELDS THAT WE CARE FOR.

exp_list_cel   = struct2cell(exp_list);
exp_list_cel   = exp_list_cel';

% build list of experiment names:
exp_cnt         = size(exp_list_cel,1);
exper           = cell(exp_cnt,1);
%instr          = cell(exp_cnt,1);
carri           = cell(exp_cnt,1);
spotl           = cell(exp_cnt,1);
kitlo           = cell(exp_cnt,1);
proto           = cell(exp_cnt,1);
%dates          = cell(exp_cnt,1);
%state          = cell(exp_cnt,1);
%futur           = cell(exp_cnt,1);
%expid          = cell(exp_cnt,1);

senso           = cell(exp_cnt,1); % to hold sensor level data
scan            = cell(exp_cnt,1); % to hold scan   level data

% hardcoded_columns: From api_hard_coded_columns_returned in the output cell.

ndxdb           = 1:1:exp_cnt;
ndxdb           = ndxdb';
instr           = exp_list_cel(:,5);
%                                                           10     19
%                                                 1234567890 23456789
dates_temp_1 	= cell2mat(exp_list_cel(:,8));  % 2018-01-25T17:05:16.006-08:00  % change: 2018_02_22 jas_temp jas_dbg ask Dinakar about -8:00
dates_temp_1    = dates_temp_1(:,1:19);
dates_temp_2    = replace(string(dates_temp_1),'T',' ');

formatOut      	= 'mm/dd/yyyy HH:MM:SS pm';                 % API requies this format: from Dinakar 2018_jan_28
dates           = cellstr(datestr(datenum(char(dates_temp_2)),formatOut)); % bug_fix add: cellstr so table creation ok 2018_01_31
state           = exp_list_cel(:,2);
expid           = exp_list_cel(:,1);
proid           = exp_list_cel(:,4);
futur           = ndxdb;                       % just put something, so it is not empty


% jas_here_thurs : to get protocol name: call protocol details for each unique prodid and update all the protocol names.
% steps: categorical / unique / for each unique: get details. then update names

% fake a prodid for staging testing (because it errors out:
if (~(isempty(strfind(url_experiments_str,'staging'))))
    % CONNECTED TO STAGING: Fake the protocol id: Bug in the db to have a bad protocol.
    for cur_prot_ndx = 1:1:exp_cnt
        if strcmp(proid{cur_prot_ndx},'4cdbc37c-3325-443c-a903-b55d70cbdbdd')
            proid{cur_prot_ndx} = '40d23e81-a796-4f10-9c89-f3a6d02c04ad';
        end
    end
end

for cur_exp_ndx=1:1:exp_cnt
    exper{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.experiment_name;
    carri{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.carrier_serial_number;
    spotl{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.spotting_lot_number;
    kitlo{cur_exp_ndx,1} = exp_list_cel{cur_exp_ndx,7}.lot_number;
end
expe_req_table      = table(ndxdb,exper,instr,carri,spotl,kitlo,proto,dates,state,futur,expid,proid,senso,scan);
% REMOVE ROWS WITH EMPTY FIELDS: jas_temp: I do not remeber how I was getting rid or why I was doing it: removing empty rows ! so I commented out:
% exper_empty_ndx is not defined before using it !!!!! big_mistery !!!!!
% if sum(isempty(exper))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(exper),:);
% end
% if sum(isempty(instr))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(instr),:);
% end
% if sum(isempty(carri))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(carri),:);
% end
% if sum(isempty(spotl))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(spotl),:);
% end
% if sum(isempty(kitlo))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(kitlo),:);
% end

end    % fn sa_rest_exp_get_all

% deal with empty proto names:
% does not work:
% if sum(isempty(cell2str(proto)))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(proto),:);
% end

% now the protocol names are returned empty
%
% exp_found_cnt   = size(proto,1);
% empty_ndx       = zeros(exp_found_cnt,1);
% for cur_ndx=1:1:size(proto,1)
%     if(isnumeric(proto{cur_ndx}))
%         empty_ndx(cur_ndx) =1;
%     end
% end
%
% sum(empty_ndx)
% % filter out empty protocol names
% empty_ndx =~(empty_ndx);
% expe_req_table   = expe_req_table(empty_ndx,:);
% exp_found_cnt_u = height(expe_req_table);
%
%
% if (exp_found_cnt_u < exp_found_cnt)
%     % THERE WERE DUPS IN THE ORIGINAL TABLE:
%     ndxdb           = 1:1:exp_found_cnt_u;
%     ndxdb = ndxdb';
%     expe_req_table = [ table(ndxdb) expe_req_table(:,2:end)];
% end
%
% if sum(isempty(dates))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(dates),:);
% end
% if sum(isempty(state))
%     expe_req_table = expe_req_table(exper_empty_ndx == isempty(state),:);
% end
%



% cd /c/Users/jsanchez/Documents/algo2/source
% cd C:/Users/jsanchez/Documents/16b/algo2r/source     dont use: cd C:\Users\jsanchez\Documents\16b\algo2r\source
% qc_protocol 22 : close to: {'85112PRT ANA Panel Protocol','c893151d-67a9-4112-a063-4bee164d40c2'}

%     % get list of experiments:
%     url_expe_str        = 'http://jervis-staging.genalyte.com/api/tests';   % try and failed: /api/tests/indexTests';
%     id                  = 'Jervis ANA Validation';
%     id                  = 'b5d306cb-70a4-462a-a651-d3878bddadc4';  %control: '28ec02fe-7bd4-4f54-be32-5f751c1287ef'; %   'Jervis ANA Validation' ; %  '3013345c-0f44-4469-82f2-ad4217fc1cc1';
%     req_start_date_str  = '07/21/2017 5:00';
%     req_end_date_str    = '07/21/2017 7:00';
%
%     id                  = strcat('''','Jervis ANA Validation',''''); %  '3013345c-0f44-4469-82f2-ad4217fc1cc1';
% 	  id                  = strcat('''','b5d306cb-70a4-462a-a651-d3878bddadc4','''');
%     req_start_date_str  = strcat('''','07/21/2017 5:00','''');
%     req_end_date_str    = strcat('''','07/21/2017 7:00','''');
%
%     url_expe_str2       = strcat(url_expe_str,',',req_start_date_str,',',req_end_date_str,',',id);  % 'q',',',id , ,')'
%     url_expe_str3       = strcat(',''start_date'',',req_start_date_str,',''end_date'',',req_end_date_str,',''q'',',id);
%     options             = weboptions('HeaderFields',{'Content-Type' 'application/json' ;'Authorization' sprintf(' Bearer %s', rest_token )});
%     expe_list           = webread(url_expe_str,url_expe_str3,options);
%
%
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','start_date','2017-07-20 5:00','end_date','2017-07-22 7:00','q','b5d306cb-70a4-462a-a651-d3878bddadc4',options);
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','start_date','2017-07-20 5:00','end_date','2017-07-22 7:00','q','Jervis ANA Validation',options);
%
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','q','b5d306cb-70a4-462a-a651-d3878bddadc4',options);
%     expe_list           = webread('http://jervis-staging.genalyte.com/api/tests','q','Jervis ANA Validation',options);
%
%     if ( (isfield(expe_list,'id')) && (isfield(expe_list,'state'))  )
%         % OK RESPONSE FROM THE WEB SERVICE: API test list
%     else
%         error_message = strcat(' ERROR RETRIEVING THE EXPERIMENT LIST FROM THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: %s'...
%             ,char(expe_list));
%         return;
%     end
%
%
% else
%     error_message = strcat(' ERROR LOGGING INTO THE THE WEB SERVICE. \nOpen a Jira Ticket and Contact John Stallings <john@mobeezio.com> for Support: %s'...
%         ,char(response));
%     return;
% end;


% expe_list_cel       =  struct2cell(expe_list);
% expe_list_cel       = expe_list_cel';





% protocol_id_list   = exp_list_cel(1,1:end)'; % cell:  28x1 list of names of 28 experiments.  field_1 is id
% protocol_name_list = exp_list_cel(2,1:end)'; % cell:  28x1 list of names of 28 experiments.  field_2 is name
% protocol_main_list = exp_list_cel(2,1:end)'; % cell:  28x1 list of names of 28 experiments.  field_3 is maintenenance flag



% sample call: [ exp_list_cel, error_message ]  = sa_rest_exp_get_all( )

% exp_list_cel =   30�3 cell array
%     '86ba7f1a-e393-4e55-bcd0-1a558a30c869'    'Maverick Maintenance Kit'                 [1]
%     '6bd9420c-3cab-4c1d-b298-1005cd0fb858'    '00083 ANA Training'                       [0]
%     'eb0986b4-07ce-4282-96b4-2489a7905838'    'Celiac 2-Step 5_2'                        [0]
%     '666ad442-bb35-4a37-bb5a-c5b6d5c00220'    'LF Panel 2-Step Dilution B'               [0]
%     '00cc717a-ae5f-4f5a-83df-9bb92b2faf65'    'Celiac 2-Step 2'                          [0]
%     '52df9a69-e774-49b7-a27b-fe0d60cddc22'    'Celiac 2-Step 6'                          [0]
%     '40d23e81-a796-4f10-9c89-f3a6d02c04ad'    'Salt 1 Step - 5 Point Curve Protoco�'    [0]
%     '5ea2163c-9e06-422a-bca8-634e0e6d8742'    'Celiac Looks Like Lupus Imported'         [0]
%     'd8f2959e-4c71-4bfa-b200-1ad70932bb37'    'LF Panel SampleA'                         [0]
%     '697701c0-1156-4144-9bf5-d38d095fbdf0'    'The Protocol formerly known as LF P�'    [0]
%     '6bb73d13-d208-48db-be34-65c9eabbc620'    'LF Panel 85113PRT'                        [0]
%     '03da0d05-3f5a-4925-8fc3-88787556cf85'    'Cylance_Test'                             [1]
%     '00c80636-799c-41d4-87ca-70119a7ab639'    'Celiac Test KR'                           [0]
%     '2319cddf-261f-4f62-aa6e-581052fbf0d7'    'Test test'                                [0]
%     '54e183a3-9062-4fe3-b0c8-d48fab599fb7'    'Celiac 2 Test KR'                         [0]
%     'f3a26634-6fb0-41b8-8d53-b6d42bec3b5f'    'Celiac Test KR 2'                         [0]
%     'd34c8f81-f1dd-48c7-ba29-554e41a53cdf'    'RA Panel 14-2 detects'                    [0]
%     '5f641b15-ae68-48b8-82b4-c761c7283cbe'    'RA Panel 14-2 detects.1'                  [0]
%     'c4fe0628-7870-4715-ba37-4b2583195bae'    'Celiac 2-Step 3-AB'                       [0]
%     '446933a6-d80a-40ef-afaa-a700ff4d4687'    'Salt Steps'                               [0]
%     'b0594274-24bd-486c-bca0-01abdc9ffba9'    'Maverick Maintenance Kit_TEST4'           [1]
%     'c893151d-67a9-4112-a063-4bee164d40c2'    '85112PRT ANA Panel Protocol'              [0]
%     '4a7ee136-fdab-4463-ab87-6be8a6ba9152'    '00083 ANA Training - John Test'           [0]
%     '69e6f69f-2c15-4c76-8105-42a09695716f'    'Celiac Panel protocol'                    [0]
%     'cdb0ce4f-d4b7-4d9f-8ef9-b7000bfc4381'    'Celiac 2-Step 3-AB1'                      [0]
%     'e4dd14f3-f81b-4511-b886-fb38fd2c44c7'    'Celiac 2-Step-170397-3 Racks-2'           [0]
%     '248253ba-14f4-42a2-8bb9-3e776b53f4bb'    'RA Panel 14_long RB1'                     [0]
%     'ce3b0bcf-8fc1-4ffe-9cee-8fc843677dda'    'Celiac Test KR3'                          [0]
%     'b4925e84-9a63-4f6a-b26e-c0500222ee9a'    'Debug Test'                               [0]
%     '852244d0-1a0b-4c12-983b-4ef1d40fd816'    '00185PRT Celiac Panel protocol'           [0]
