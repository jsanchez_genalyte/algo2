function [ a_row_or_col_vector ] = util_transpose_to_col_vector( a_row_or_col_vector )
%UTIL_TRANSPOSE_TO_COL_VECTOR takes a vector and returns itself as a column vector
%   if it is a column vector, returns itsef. If it is a row vector, returns the transposed version.

    if (isrow( a_row_or_col_vector))
        a_row_or_col_vector = a_row_or_col_vector';
    end

end

