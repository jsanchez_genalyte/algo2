function    [ req_start_date_str, req_end_date_str ] = date_keyword_2(wanted_keyword)
%DATE_KEYWORD_1 converts a wanted_keyword into concrete start and end dates:
% handles all cases of 2 word keywords. If none found, returns blank: req_start_date_str and req_end_date_str
% sample call:   [ req_start_date_str, req_end_date_str ] = date_keyword_2('THIS MONTH');

% keywords_2 = { 'THIS WEEK', 'LAST WEEK', 'THIS MONTH', 'LAST MONTH',  'THIS YEAR' ,  'LAST YEAR' };
% keywords_2_cat = categorical(keywords_2);
date_str_format         = 'mm/dd/yyyy';
req_end_date_str        = '';
req_start_date_str      = '';
time_now_dt             = datetime('now');
cur_year                = year(time_now_dt);
cur_month               = month(time_now_dt);

% dsbug: datestr(sprintf('%s/%s/%s',num2str(cur_month), num2str(cur_day), num2str(cur_year)),date_str_format);

if (strcmp(wanted_keyword,'THIS WEEK'))
    % Find the day of the week and go back to monday:                                   % OK
    end_day_of_week_num = weekday(datenum(time_now_dt));
    start_date          = time_now_dt-days(end_day_of_week_num-1);
    req_start_date_str  = datestr(start_date,date_str_format);
    req_end_date_str    = datestr(now,date_str_format);
    return;
end

if (strcmp(wanted_keyword,'LAST WEEK'))
    % Find the day of the week and go back to monday: then substract 7 days.         	% OK
    end_day_of_week_num = weekday(datenum(time_now_dt));
    end_date            = time_now_dt-days(end_day_of_week_num-1);
    req_end_date_str    = datestr(end_date,date_str_format);
    start_date          = end_date-days(7);
    req_start_date_str  = datestr(start_date,date_str_format);
    return;
end

if (strcmp(wanted_keyword,'THIS MONTH'))
    % Find the day of the month and go back to the first:                               % OK
    start_day           = '01';
    req_start_date_str  = datestr(sprintf('%s/%s/%s',num2str(cur_month), num2str(start_day), num2str(cur_year)),date_str_format);
    req_end_date_str    = datestr(today,date_str_format);
    return;
end

if (strcmp(wanted_keyword,'LAST MONTH'))   % clean                                      % OK
    date_str_format  	= 'MM/dd/yyyy'; % SPECIAL for this case: so it is not ambiguous mm month or mm minute:
    % Fist: substract one month from now. Then get start month and year. then use day 1 for start.
    start_date          = time_now_dt - calmonths(1);
    start_year          = year(start_date);
    start_month         = month(start_date);
    start_day           = 1;
    date_start_dt       = datetime(start_year,start_month,start_day,'format',date_str_format);
    req_start_date_str  = datestr(date_start_dt,'mm/dd/yyyy');  % IT DOES NOT WORK: ,'format','mm/dd/yyy'); % date_str_format); % date_start_num      = datenum(date_start_dt);
    % HAVE START DATE: Add 1 calendar month minus 1 da because jervis is inclusive in the end date.
    % date_start_num    = datetime(start_year,start_month,1,0,0,0);
    req_end_date_dt     = date_start_dt   +  calmonths(1);
    req_end_date_dt 	= req_end_date_dt - caldays(1);
    req_end_date_str    = datestr(req_end_date_dt,'mm/dd/yyyy'); % IT DOES NOT WORK: , date_str_format);
    return;
end

if (strcmp(wanted_keyword,'THIS YEAR'))                                                  % OK
    %
    start_year          = int2str(year(time_now_dt));
    start_month         = '01';
    start_day           = '01';
    req_start_date_str  = strcat(start_month,'/',start_day,'/',start_year);
    req_end_date_str    = datestr(today,date_str_format);
    return;
end

if (strcmp(wanted_keyword,'LAST YEAR'))                                                  % OK
    % Like better: older implementation: for last year:
    start_year          = int2str(year(time_now_dt) -1 );
    start_month         = '01';
    start_day           = '01';
    req_start_date_str = strcat(start_month,'/',start_day,'/',start_year);
    end_year            = start_year;
    end_month           = '12';
    end_day             = '31';
    req_end_date_str    = strcat(end_month,'/',end_day,'/',end_year);
    return;
end
end % fn date_keyword_1

%
% % left overs
%
%     start_day           = '01';
%     end_day             = '01';     % approximation: it should be 28,29,30 or 31
%     start_year          = int2str(year(time_now_dt));
%     end_year            = start_year;
%
%
%     datestr(sprintf('%s/%s/%s',num2str(cur_month), num2str(cur_day), num2str(cur_year)),date_str_format);
%
%
%     cur_month           = month(time_now_dt);
%     req_start_date_str  =
%
%     if (month(time_now_dt)==1)
%         % JANUARY
%         start_year      = int2str(year(time_now_dt)-1);
%         start_month     = '12';
%         end_month       = '01';
%     else
%         % NO NEED TO ADJUST MONTH/YEAR
%         if (month((time_now_dt))<=9)
%             % ONE DIGIT
%             start_month = sprintf('0%1d',month((time_now_dt-1)));
%         else
%             % TWO DIGIT
%             start_month = sprintf('%2d',month((time_now_dt-1)));
%         end
%         if (month((time_now_dt))<=9)
%             % ONE DIGIT
%             end_month = sprintf('0%1d',month((time_now_dt))); % approx
%         else
%             % TWO DIGIT
%             end_month = sprintf('%2d',month((time_now_dt)));  % approx
%         end
%     end
%     req_start_date_str  = strcat(start_month,'/',start_day,'/',start_year);
%     req_end_date_str    = strcat(end_month,'/',end_day,'/',end_year);
%

%
%     start_year          = int2str(year(time_now_dt));
%     if (month((time_now_dt))<=9)
%         % ONE DIGIT
%         start_month     = sprintf('0%1d',month((time_now_dt)));
%     else
%         % TWO DIGIT
%         start_month     = sprintf('%2d',month((time_now_dt)));
%     end
%     start_day           = '01';
%     req_start_date_str = strcat(start_month,'/',start_day,'/',start_year);



%     % last year: Fist: substract one month from now. Then get start month and year. then use day 1 for start.
%     start_date          = time_now_dt- calyears(1);
%     start_year          = year(start_date);
%     start_month         = '01';
%     start_day           = '01';
%     req_start_date_str  = datestr(sprintf('%s/%s/%s',num2str(start_month), num2str(start_day), num2str(start_year)),date_str_format);
%     % HAVE START DATE: Add 1 calendar month minus 1 da because jervis is inclusive in the end date.
%     end_month           = '12';
%     end_day             = '31';
%     req_end_date_str    = datestr(sprintf('%s/%s/%s',num2str(end_month), num2str(end_day), num2str(start_year)),date_str_format);
