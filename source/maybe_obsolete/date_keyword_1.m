function    [ req_start_date_str, req_end_date_str ] = date_keyword_1(wanted_keyword)
%DATE_KEYWORD_1 converts a wanted_keyword into concrete start and end dates:
%
% sample call:   [ req_start_date_str, req_end_date_str ] = date_keyword_1(keywords_1,token_cel{1});

req_end_date_str   = '';
req_start_date_str = '';

date_str_format = 'mm/dd/yyyy';
if (strcmp(wanted_keyword,'TODAY'))
    %
    req_start_date_str = datestr(now,date_str_format);
    req_end_date_str   = req_start_date_str;
end

if (strcmp(wanted_keyword,'YESTERDAY'))
    %
    req_start_date_str = datestr((today - days(1)),date_str_format);
    req_end_date_str   = req_start_date_str;
end
end % fn date_keyword_1

