function [ ui_state_struct ] = ui_eval_all_indices_ori_bad( db_cur_fit_net_ndx,clasi, ui_state_struct,handle_text_status  )
%UI_EVAL_ALL_INDICES Evaluates current indices to produce an , to be used to display the tos and/or plots
%   Produces 3 indices: ui_cur_fit_net_ndx (union or single), ui_cur_fit_net_ndx_good (good only),ui_cur_fit_net_ndx_anom (anom only)
%   Uses the ui_request_flag to decide how to build the ui indices:
% input:
% 1_ db_cur_fit_net_ndx: index of whatever data is selected (net of all filtering)
% 2_ clasi:              classification results: U==Unknown,G=good,A=Anomalous,etc.       	% db_metad_set_struct.clasi
% 3 _ ui_state_struct:   senso_start_ndx and senso_curves_perp_cnt: start & curves_perp_cnt for good and bad sensograms:
% 4_ ui_request_flag
%           'g_only' to produce indices of only good sensograms.
%           'a_only' to produce indices of only anom sensograms.
%           'g_n_a'  to produce indices of good and  anom sensograms.
%           'u_only' to produce indices of unknown sensograms: ie not_analyzed.
% output: ui_state_struct.
%               ui_cur_fit_net_ndx      ndx to be used to plot/and tos. logical of size 1536 +,ui_cur_fit_net_ndx_good +,ui_cur_fit_net_ndx_anom
%               senso_start_ndx_next  	ndx of the next start (for the next call.).
% sample call:  [ ui_state_struct_temp ] = ui_eval_all_indices( ...
%               handles.ui_state_struct.db_cur_fit_net_ndx, handles.db_data_set_struct.db_metad_set_struct.clasi, handles.ui_state_struct ,'u_only'  )

% ASSUMPTION:  sc_clasify_sensograms was called BEFORE evaluating all indexes, becuase it uses the latest classification results.
%              future: make a classy and eval_indices wrapper to pack the 2 of them in one and guarantee the proper equence.
ui_request_flag                         = ui_state_struct.ui_request_flag;
ui_state_struct.ui_cur_fit_net_ndx      = [];
ui_state_struct.ui_cur_fit_net_ndx_good = []; % to plot them simultaneously or separatedly
ui_state_struct.ui_cur_fit_net_ndx_anom = []; % to plot them simultaneously or separatedly
ui_cur_fit_net_ndx                      = [];
ui_cur_fit_net_ndx_good                 = []; % to plot them simultaneously or separatedly
ui_cur_fit_net_ndx_anom                 = []; % to plot them simultaneously or separatedly
senso_start_ndx_next_good               = nan;
senso_start_ndx_next_anom               = nan;

db_cur_fit_net_len                      = sum(db_cur_fit_net_ndx);
% setup for the 3 cases:
senso_start_ndx                         = ui_state_struct.senso_start_ndx;        	% this ndx is relative to whatever is currently selected: db_cur_fit_net_ndx
senso_curves_perp_cnt                  	= ui_state_struct.senso_curves_perp_cnt;  	%100; % sa_cfg_
included_indices                        = find(db_cur_fit_net_ndx);
senso_end_ndx                           = min( (senso_start_ndx + senso_curves_perp_cnt - 1),length(included_indices));
%warning_smg = '';
if (  (senso_end_ndx - senso_start_ndx + 1) < senso_curves_perp_cnt)
    warning_msg = sprintf('Reached the end. Displaying only:  %-4d sensogram(s) of the %4d  requested  '...
        , (  senso_end_ndx - senso_start_ndx + 1),senso_curves_perp_cnt);
    set(handle_text_status,'string',warning_msg,'ForegroundColor','blue','Fontsize',12);
end

if (senso_end_ndx < senso_start_ndx)
    % EMPTY RANGE
    ui_state_struct.ui_cur_fit_net_ndx      = []; % save the result to return.
    ui_state_struct.senso_start_ndx_next    = nan;
else
    % NON EMPTY RANGE
    if (  ( (strcmpi(ui_request_flag,'g_only')) || (strcmpi(ui_request_flag,'g_n_a')) ) && (senso_end_ndx >= senso_start_ndx) )
        % REQUESTED GOOD_ONLY or both  AND SENSO RANGE IS NOT EMPTY
        % determine ndx of the good ones:
        ui_cur_fit_net_ndx_good_all     = clasi(logical(db_cur_fit_net_ndx)) =='G';
        % from these see return as much as it can be retrieved:
        ui_cur_fit_net_ndx_good_all_temp= ui_cur_fit_net_ndx_good_all(senso_start_ndx:end);
        ui_cur_fit_net_ndx_good       	= find(ui_cur_fit_net_ndx_good_all_temp, senso_curves_perp_cnt, 'first'); % returns at most the first senso_curves_perp_cnt indices corresponding to the nonzero entries of X.
        if (isempty(ui_cur_fit_net_ndx_good))
            % FOUND no GOOD FOR THE SELECTED RANGE
            senso_start_ndx_next            = nan;
        else
            % FOUND some GOOD FOR THE SELECTED RANGE
            ui_cur_fit_net_ndx_good       	= ui_cur_fit_net_ndx_good +  (senso_start_ndx-1); % shift all of them to reflect that we start at  senso_start_ndx
            %good_end_cnt_true              = length(ui_cur_fit_net_ndx_good); % dbg_ this is the true amount of good found.
            senso_start_ndx_next_good       = min((ui_cur_fit_net_ndx_good(end)+1),db_cur_fit_net_len);  % save for the next trip around.
        end
    end
    
    if ( ((strcmpi(ui_request_flag,'a_only')) || (strcmpi(ui_request_flag,'g_n_a')) )  && (senso_end_ndx >= senso_start_ndx) )
        % REQUESTED ANOM_ONLY or both  AND SENSO RANGE IS NOT EMPTY
        % determine ndx of the anom ones:
        ui_cur_fit_net_ndx_anom_all     = clasi(logical(db_cur_fit_net_ndx)) =='A';
        % from these see return as much as it can be retrieved:
        ui_cur_fit_net_ndx_anom_all_temp= ui_cur_fit_net_ndx_anom_all(senso_start_ndx:end);
        ui_cur_fit_net_ndx_anom       	= find(ui_cur_fit_net_ndx_anom_all_temp, senso_curves_perp_cnt, 'first'); % returns at most the first senso_curves_perp_cnt indices corresponding to  nonzero entries of X.
        if (isempty(ui_cur_fit_net_ndx_anom))
            % FOUND NO ANOMALOUS FOR THE SELECTED RANGE
            senso_start_ndx_next            = nan;
        else
            % FOUND NO ANOMALOUS FOR THE SELECTED RANGE
            ui_cur_fit_net_ndx_anom    	= ui_cur_fit_net_ndx_anom +  (senso_start_ndx-1); % shift all of them to reflect that we start at  senso_start_ndx
            %anom_end_cnt_true       	= length(ui_cur_fit_net_ndx_anom); % dbg_ this is the true amount of anom found.
            senso_start_ndx_next_anom	= min((ui_cur_fit_net_ndx_anom(end)+1),db_cur_fit_net_len);  % save for the next trip around.
        end
    end
    
    if (  (strcmpi(ui_request_flag,'g_n_a')) && (senso_end_ndx >= senso_start_ndx) ) % and not combined
        % REQUESTED GOOD AND ANOM_   AND SENSO ANGE IS NOT EMPTY
        % Already have the determine ndx of the anom ones:
        % grab: all the good and anom that have an ndx less than this: In other words trim the 2 arrays (the union should match)
        senso_end_ndx          	= senso_start_ndx+ senso_curves_perp_cnt-1;
        ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good(ui_cur_fit_net_ndx_good <= senso_end_ndx);
        ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom(ui_cur_fit_net_ndx_anom <= senso_end_ndx);
        senso_start_ndx_next    = senso_end_ndx+1;
        % SANITY CHECK:
        if ( (  length(ui_cur_fit_net_ndx_good) + length(ui_cur_fit_net_ndx_anom )) ~= senso_end_ndx)
            err_msg = sprintf('LOGIC ERROR IN UI_EVAL_INDICES %4d + %d ~= %4d '...
                , length(ui_cur_fit_net_ndx_good),length(ui_cur_fit_net_ndx_anom), senso_end_ndx);
            display(err_msg);
            set(handle_text_status,'string',err_msg,'ForegroundColor','red','Fontsize',12);
            pause(3);
        end
    end
    
    if (   (strcmpi(ui_request_flag,'u_only')) && (senso_end_ndx >= senso_start_ndx) )  % jas_tbd: temporal using good_start as good_unknown
        % REQUESTED UNKNOWN_ONLY or both  AND GOOD RANGE IS NOT EMPTY
        % determine ndx of the good ones:
        ui_cur_fit_net_ndx_unkn_all = find(clasi(logical(db_cur_fit_net_ndx)) =='U');     % 'U'
        
        if (isempty(ui_cur_fit_net_ndx_unkn_all))
            % FOUND NO UNKNOWNS FOR THE SELECTED RANGE
            senso_start_ndx_next            = nan;
        else
            % FOUND SOME UNKNOWNS FOR THE SELECTED RANGE
            % from these see return as much as it can be retrieved:
            unkn_end_ndx_true       = min(senso_end_ndx,length(ui_cur_fit_net_ndx_unkn_all));
            unkn_range              = senso_start_ndx:1:unkn_end_ndx_true;
            ui_cur_fit_net_ndx_unkn = ui_cur_fit_net_ndx_unkn_all(unkn_range);
        end
    end
    
    
    % Evaluate what you've got:
    ui_state_struct.ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good; % to plot them simultaneously or separatedly
    ui_state_struct.ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom; % to plot them simultaneously or separatedly
    if  (   strcmpi(ui_request_flag,'g_only'))
        % REQUESTED GOOD_ONLY
        ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx_good;
        senso_start_ndx_next    = senso_start_ndx_next_good;
    elseif (strcmpi(ui_request_flag,'a_only'))
        % REQUESTED ANOM_ONLY
        ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx_anom;
        senso_start_ndx_next    = senso_start_ndx_next_anom;
        %     elseif (strcmpi(ui_request_flag,'u_only'))
        %         % REQUESTED ANOM_ONLY
        %         ui_cur_fit_net_ndx = ui_cur_fit_net_ndx_unkn;
        
    elseif (strcmpi(ui_request_flag,'g_n_a'))
        %     REQUESTED GOOD_AND ANOM
        ui_cur_fit_net_ndx = union(ui_cur_fit_net_ndx_good, ui_cur_fit_net_ndx_anom);
    end
    
    %     % REQUESTED GOOD_AND ANOM % this is the combined way... or something like this.
    %     ui_cur_fit_net_ndx = union(ui_cur_fit_net_ndx_good, ui_cur_fit_net_ndx_anom);
    %     ui_state_struct.ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good; % to plot them simultaneously or separatedly
    %     ui_state_struct.ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom; % to plot them simultaneously or separatedly
    %end
    
    ui_state_struct.ui_cur_fit_net_ndx      = ui_cur_fit_net_ndx; % save the result to return.
    ui_state_struct.senso_start_ndx_next    = min(db_cur_fit_net_len,senso_start_ndx_next);
end % non-empty sel range
end % fn ui_eval_all_indices

% leftovers: old way:
% if (  ( (strcmpi(ui_request_flag,'g_only')) || (strcmpi(ui_request_flag,'g_n_a')) ) && (senso_end_ndx >= senso_start_ndx) )
%     % REQUESTED GOOD_ONLY or both  AND GOOD RANGE IS NOT EMPTY
%     % determine ndx of the good ones:
%     ui_cur_fit_net_ndx_good_all = clasi(logical(db_cur_fit_net_ndx)) =='G';
%     % from these see return as much as it can be retrieved:
%     good_end_ndx_true       = min(senso_end_ndx,sum(ui_cur_fit_net_ndx_good_all));
%     good_range              = senso_start_ndx:1:good_end_ndx_true;
%     ui_cur_fit_net_ndx_good = ui_cur_fit_net_ndx_good_all(good_range);
% end
%
% if ( ((strcmpi(ui_request_flag,'a_only')) || (strcmpi(ui_request_flag,'g_n_a')) )  && (senso_end_ndx >= senso_start_ndx) )
%     % REQUESTED ANOM_ONLY or both AND ANOM RANGE IS NOT EMPTY
%     % determine ndx of the anom ones:
%     ui_cur_fit_net_ndx_anom_all = find(clasi(db_cur_fit_net_ndx) =='A' &    clasi(db_cur_fit_net_ndx) ~='U'); %      =='A');
%     % from these see return as much as it can be retrieved:
%     anom_end_ndx_true       = min(senso_end_ndx,length(ui_cur_fit_net_ndx_anom_all));
%     anom_range              = senso_start_ndx:1:anom_end_ndx_true;
%     ui_cur_fit_net_ndx_anom = ui_cur_fit_net_ndx_anom_all(anom_range);
% end
%
%

