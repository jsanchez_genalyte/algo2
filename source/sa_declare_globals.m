% % SA_DECLARE_GLOBALS declares EVERY global file used in this sensogram analysis app. (sa == sensogram_analysis)
% % This file ONLY declares globals. It does NOT defines globals !!!!! Never and assigment in this file !!!!
% % TO use it: at the top of any function that uses globals do:  sa_declare_globals;
% %see: sa_init_globals
% 
% declare_globals;    % from algo (not from algo2: jas_temp)
% 
% global g_db_sensor_name;
% global g_rows_db_cnt;
% global g_cols_db_cnt;
% 
% % G0_ large matrices_storage:  ________________________________________________________
% 
% % additional cols for  g_db_set this is an original classificat
% global g_db_set_db_ndx      ;%    this is the original order the data came from the db query.
% global g_tos_sort_ndx       ;%     this is the order to display data in the tos. (default: same as g_db_set_db_ndx)
% global g_db_set_ui_filter   ;%     0/1 1:if selected by filters. (ie the real data to work with)
% global g_db_set_sc_done     ;%     0/1 1:if classification done
% global g_db_set_rc_done     ;%      0/1 1:if re-classification done by end-user).
% 
% global g_db_set_ui_plot_g   ;%     currently ploted on the good side 
% global g_db_set_ui_plot_a   ;%     currently ploted on the anom side  
% global g_db_set_ui_ptof     ;%     currently displayed on tos tbl.  
% 
% % G0_ xxxx   ________________________________________________________
% 
% % G7  input_parameters:  __________________________________________________________
% %global global_figure_start_no           ;  
% 
% % file: declare_std_curve_globals
% 
% %______________________________________________________________________________________________________________________
% % declaration of all the data deppendent:
% % call: sa_init_globals_data_dep: once the size of the input data is known and the global variables with the real sizes is known
% % I.E. after having requested data from the db or openning a local data set file.
% 
% % G0_ large matrices_storage: data_dep: ________________________________________________________
% % matrices_storage: rows_db_cnt,max_scan_cnt,db_cols_cnt,tos_rows,tos_cols
% global g_db_set      ;%2d rows_db_cnt x (db_cols_cnt + ui_total_fg_cnt)  how does it relate to tos ?
% global g_db_set_cat  ;%2d rows_db_cnt x (db_cols_cat_cnt)                      cat  ordinal proto+samp+lotx+expr+inst+carr+chan+clust+datat
% global g_db_dates    ;%2d rows_db_cnt x 1                                      dates 
% global g_scan_raw    ;%2d rows_db_cnt x max_scan_cnt from db                   float
% global g_scan_nrm    ;%2d rows_db_cnt x max_scan_cnt from db                   float
% global g_scan_tr     ;%2d rows_db_cnt x max_scan_cnt from db                   float
% global g_scan_nrm_ma ;%2d rows_db_cnt x max_scan_cnt  calculated by matlab    	float
% global sc_flags_cat  ;%2d rows_db_cnt x 1                                      cat   ordinal
% %display(sprintf('DONE: sa_declare_globals'));