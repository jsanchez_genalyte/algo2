function [ an_output_double_array ] = cell2mat_nan( an_input_cell,an_output_double_array,fill_value )
%CELL2MAT_NAN converts an input one dimentional cell with possible empty values into a double array. 
%   Empty values get filled with whatever is passed as the fill_value argument  
%   This is my "robust" version of matlab cell to mat, that handles empy cells.

expected_values_cnt = length(an_output_double_array);
for cur_senso_out_ndx = 1:1:expected_values_cnt
    if  ( (isempty(an_input_cell{cur_senso_out_ndx}))  )
        % *** TAKE MISSING OUTLIER FLAG AS OUTLIER:
        an_output_double_array(cur_senso_out_ndx) = fill_value;
    else
        an_output_double_array(cur_senso_out_ndx) = cell2mat(an_input_cell(cur_senso_out_ndx));
    end
end
end

