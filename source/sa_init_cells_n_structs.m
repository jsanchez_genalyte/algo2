function [ db_data_set_struct,db_data_exp_struct,ui_state_struct] = sa_init_cells_n_structs(  ) %  input_parameters_struct,handles
%SA_INIT_CELLS_AND_STRUCTS creates cells and structs (one time) when app starts  and hangs them to handles.
% cells created:  
% pipeline:  Sensogram_Analysis_App_10_OpeningFcn --> sa_init_cells_and_structs
%__________________________________________________________________________________________________
% S1_ define the cols for: sens_meta_data_cell: all strings - only strings ________________________
smd_cols=struct;
smd_cols.ndxdb              =  1  ; %
smd_cols.clasi              =  2  ; % classification flags (all of them as a single string: G,A,.....
smd_cols.probe              =  3  ; %
smd_cols.dates              =  4  ; % run date as a string.
smd_cols.proto          	=  5  ; %
smd_cols.sampl              =  6  ; %
smd_cols.chipl              =  7  ; %
smd_cols.exper             	=  8  ; %
smd_cols.instr            	=  9  ; %
smd_cols.carri              =  10 ; %
smd_cols.chann              =  11 ; %
smd_cols.clust              =  12 ; %
smd_cols.datat              =  13 ; %
smd_cols.valfg              =  14 ; %
smd_cols.outfg              =  15 ; %
smd_cols.error              =  16 ; %
smd_cols.col_cnt            =  16 ; % count of cols for matrix. These are the first 16 cols of the tos.      

db_metad_set_struct=struct;
db_metad_set_struct.smd_ndxdb     	= {}; %   1  ; % db index from retrieved data (just an id) smd == ? sensogram_meta_data
db_metad_set_struct.smd_clasi     	= {}; %   2  ; % classification flags (all of them as a single string: G,A,.....
db_metad_set_struct.smd_probe      	= {}; %   3  ; %
db_metad_set_struct.smd_dates      	= {}; %   4  ; % run date as a string.
db_metad_set_struct.smd_proto    	= {}; %   5  ; %
db_metad_set_struct.smd_sampl      	= {}; %   6  ; %
db_metad_set_struct.smd_chipl    	= {}; %   7  ; %
db_metad_set_struct.smd_exper     	= {}; %   8  ; %
db_metad_set_struct.smd_instr       = {}; %   9  ; %
db_metad_set_struct.smd_carri       = {}; %   10 ; %
db_metad_set_struct.smd_chann       = {}; %   11 ; %
db_metad_set_struct.smd_clust       = {}; %   12 ; %
db_metad_set_struct.smd_datat       = {}; %   13 ; %
db_metad_set_struct.smd_valfg       = {}; %   14 ; %
db_metad_set_struct.smd_outfg       = {}; %   15 ; %
db_metad_set_struct.smd_error       = {}; %   16 ; % 
db_metad_set_struct.expid           = {}; %           % added 2018_02_08
% sens_meta_data_cell = cell(smd_col_cnt,1);                            % niu
% sens_meta_data = {'','','','','','','','','','','','','','',''};      % niu

% classification flags and values 
sc_clasi_flag_struct=struct;
sc_clasi_flag_struct.allre_ip_found_fg   =  1;%
sc_clasi_flag_struct.swash_ip_found_fg   =  2;% { 0,1             ) good = 1
sc_clasi_flag_struct.ampli_ip_found_fg   =  3;% { 0,1             ) good = 1
sc_clasi_flag_struct.swash_ok_adrsq_fg   =  4;% { 0,1             ) good = 1
sc_clasi_flag_struct.bline_ok_adrsq_fg   =  5;% { 0,1             ) good = 1
sc_clasi_flag_struct.ampli_ok_adrsq_fg   =  6;% { 0,1             ) good = 1
sc_clasi_flag_struct.bline_ok_mea_ra_fg  =  7;   % bline measurement range flag -41
sc_clasi_flag_struct.bline_ok_mea_sl_fg  =  8;   % bline measurement slope flag -42 
sc_clasi_flag_struct.ampli_ok_mea_ra_fg  =  9;   % bline measurement range flag -43
sc_clasi_flag_struct.ampli_ok_mea_sl_fg  = 10;   % bline measurement slope flag -44 
	 


sc_clasi_flag_struct.clasi_flag_col_cnt   = 10;% num cols




sc_clasi_vals_struct=struct;
sc_clasi_vals_struct.swash_ip_found_cnt =  1 ;%   { 0,1,2,3 >3     ) good = 1
sc_clasi_vals_struct.swash_ip_found_dx1 =  2 ;%   loc
sc_clasi_vals_struct.swash_ip_found_dx2 =  3 ;%
sc_clasi_vals_struct.swash_ip_found_dx3 =  4 ;%
sc_clasi_vals_struct.swash_mx_found_ndx =  5 ;%   ndx of max found
sc_clasi_vals_struct.ampli_ip_found_cnt =  6 ;%   { 0,1,2,3 or more) good = 1
sc_clasi_vals_struct.ampli_ip_found_dx1 =  7 ;%
sc_clasi_vals_struct.ampli_ip_found_dx2 =  8 ;%
sc_clasi_vals_struct.ampli_ip_found_dx3 =  9 ;%
sc_clasi_vals_struct.ampli_mx_found_ndx =  10;%   ndx of max found
sc_clasi_vals_struct.swash_ok_adrsq_val =  11;%   { 0 to 1              )
sc_clasi_vals_struct.bline_ok_adrsq_val =  12;%   { 0 to 1              )
sc_clasi_vals_struct.ampli_ok_adrsq_val =  13;%   { 0 to 1              )
sc_clasi_vals_struct.clasi_vals_col_cnt =  13;%   num analog cols

% Vectors: single value per sensogram
datef               = nan; % vector of run dates. tos:3. S1_ define run data as a float
% cols times 3: 
% blin g               = nan; % vector of float tos:13 %ref
% ampl g               = nan; % vector of float tos:14
% shif t               = nan; % vector of float tos:15

bligc               = nan; % vector of float tos:13 %current
ampgc               = nan; % vector of float tos:14
shifc               = nan; % vector of float tos:15

bligr               = nan; % vector of float tos:13  raw ..
ampgr               = nan; % vector of float tos:14
shifr               = nan; % vector of float tos:15

bligj               = nan; % vector of float tos:13  jervis norm
ampgj               = nan; % vector of float tos:14
shifj               = nan; % vector of float tos:15

bligm               = nan; % vector of float tos:13  matlab norm
ampgm               = nan; % vector of float tos:14
shifm               = nan; % vector of float tos:15

% 2D matrices: vector of scan data per sensogram (the real data plotted) 
scan_raw_time       = nan;
scan_raw_gru        = nan;
scan_raw_tr_gru     = nan;
scan_nrm_gru        = nan;
scan_nrm_gru_matlab = nan;
orndx               = nan;

rows_db_cnt         = nan; 	% number of rows returned by the rest_service after querying the db.
cols_db_cnt         = nan; 	% number of cols returned by the rest_service after querying the db.
probe = { 'gotIt' }; % jas_test 
% probe = { ...  % from qc: Luis files. 16 analytes
% 'dsDNA'              ...
% ,'Sm'                ...
% ,'Scl-70'            ...
% ,'Jo-1'              ...
% ,'SS-A 60'           ...
% ,'SS-A 52'           ...
% ,'SS-B'              ...
% ,'RNP'               ...
% ,'Cenp B'            ...
% ,'PCNA'              ...
% ,'Nucleosome'        ...
% ,'Ribo-P P0'         ...
% ,'Ku'                ...
% ,'Anti-IgG'          ...
% ,'Ref 1'             ...
% ,'Ref 2'             ...
% };

field_01 = 'orndx';                 value_01 = { orndx               	};     % single vector with 1 value per sensogram: original ndx from db (as retrieved)
field_02 = 'probe';                 value_02 = { probe 	                };     % single vector with nominal (to start with) list of qc analytes
field_03 = 'db_metad_set_struct';   value_03 = { db_metad_set_struct    };     % struct with cell arrays with 1 row per sensogram and a many cols as metadata strs to describe a senso
field_04 = 'smd_cols';          	value_04 = { smd_cols             	};     % struct with  cols for sens_meta_data_cell matrix
field_05 = 'datef';                 value_05 = { datef                  };     % vector with 1 value per sensogram
field_06 = 'bligc';                 value_06 = { bligc                  };     % vector with 1 value per sensogram
field_07 = 'ampgc';              	value_07 = { ampgc                  };     % vector with 1 value per sensogram
field_08 = 'shifc';                 value_08 = { shifc                  };     % vector with 1 value per sensogram
field_09 = 'scan_raw_time';         value_09 = { scan_raw_time         	};     % 2D matrix with scan data: 
field_10 = 'scan_raw_gru';          value_10 = { scan_raw_gru           };     % 2D matrix
field_11 = 'scan_raw_tr_gru';    	value_11 = { scan_raw_tr_gru        };     % 2D matrix
field_12 = 'scan_nrm_gru';          value_12 = { scan_nrm_gru         	};     % 2D matrix
field_13 = 'scan_nrm_gru_matlab'; 	value_13 = { scan_nrm_gru_matlab   	};     % 2D matrix
field_14 = 'rows_db_cnt';           value_14 = { rows_db_cnt            };     % single value 
field_15 = 'cols_db_cnt';           value_15 = { cols_db_cnt            };     % single value

field_16 = 'bligr';                 value_16 = { bligr                  };     % vector with 1 value per sensogram
field_17 = 'ampgr';              	value_17 = { ampgr                  };     % vector with 1 value per sensogram
field_18 = 'shifr';                 value_18 = { shifr                  };     % vector with 1 value per sensogram
field_19 = 'bligj';                 value_19 = { bligj                  };     % vector with 1 value per sensogram
field_20 = 'ampgj';              	value_20 = { ampgj                  };     % vector with 1 value per sensogram
field_21 = 'shifj';                 value_21 = { shifj                  };     % vector with 1 value per sensogram
field_22 = 'bligm';                 value_22 = { bligm                  };     % vector with 1 value per sensogram
field_23 = 'ampgm';              	value_23 = { ampgm                  };     % vector with 1 value per sensogram
field_24 = 'shifm';                 value_24 = { shifm                  };     % vector with 1 value per sensogram
nomfg    = 'false'; % .. until done
field_25 = 'nomfg';                 value_25 = { nomfg                  };     % single value: norm_matlab_flag done
field_26 = 'sc_clasi_flag_struct';	value_26 = { sc_clasi_flag_struct	}; 
field_27 = 'sc_clasi_vals_struct';	value_27 = { sc_clasi_vals_struct	}; 

% Pack all the db_data_set fields into a struct to be returned.
% db_data_set will hold ALL data comming from the db or loaded from a local file ________________
db_data_set_struct = struct(...
    field_01 , value_01, field_02, value_02, field_03, value_03                     ...
   ,field_04 , value_04, field_05, value_05, field_06, value_06, field_07, value_07	...
   ,field_08 , value_08, field_09, value_09, field_10, value_10, field_11, value_11 ...
   ,field_12,  value_12, field_13, value_13, field_14, value_14, field_15, value_15 ...
   ,field_16,  value_16, field_17, value_17, field_18, value_18, field_19, value_19 ...
   ,field_20,  value_20, field_21, value_21, field_22, value_22, field_23, value_23 ...   
   ,field_24,  value_24 ,field_25, value_25, field_26, value_26 ,field_27,  value_27);

% S1_ define the cols for: sens_meta_data_cell: all strings ________________________

% S2_ define cols for: tos ________________________
% see function: tos_create

% % % % handles.tos_struct                              = ;
% % % % tos_data = nan(1,tos_col_struct.col_cnt);
% % % % tos_struct = struct(...
% % % %     'tos_col_struct' , {tos_col_struct}, 'tos_data', {tos_data} );
% % % % 
% % % % % jas_tb_reviewed



ui_tos_sort_ndx   	=nan;   % this is the order to display data in the tos. (default: same as g_db_set_db_ndx)
ui_db_set_ui_filter	=nan;   % 0/1 1:if selected by filters. (ie the real data to work with)
ui_db_set_sc_done  	=nan;   % 0/1 1:if classification done
ui_db_set_rc_done  	=nan;   % 0/1 1:if re-classification done by end-user).

ui_db_set_plot_g    =nan;   % currently ploted on the good side
ui_db_set_plot_a    =nan;   % currently ploted on the anom side
ui_db_set_pto_tos   =nan;   % currently displayed on tos tbl.
cur_session_name    ='';    % current name of session
db_session_opened   =0;     % 0 until a fake or real data gets loaded from file or from db
local_input_dir    = '';


% START: toe_additions: 2018_01_26 ____________________________________________________________________________________________________________

% NOTE: all the emd and db_metad_exp_struct maybe: NOI jas_tbreviewed
% S1_ define the cols for: exp_meta_data_cell: all strings - only strings 
emd_cols=struct;
emd_cols.ndxdb              =  1  ; %
emd_cols.exper              =  2  ; % classification flags (all of them as a single string: G,A,.....
emd_cols.instr              =  3  ; %
emd_cols.carri              =  4  ; % run date as a string.
emd_cols.spotl          	=  5  ; %
emd_cols.kitlo              =  6  ; %
emd_cols.proto              =  7  ; %
emd_cols.dates             	=  8  ; %
emd_cols.state            	=  9  ; %
emd_cols.futur              =  10 ; %
emd_cols.col_cnt            =  10 ; % count of cols for matrix. These are the first 10 cols of the toe.      

% Experiment toe meta data:
db_metad_exp_struct=struct;
db_metad_exp_struct.emd_ndxdb     	= {}; %   1  ; % db index from retrieved data (just an id) smd == ? sensogram_meta_data
db_metad_exp_struct.emd_exper     	= {}; %   2  ; % classification flags (all of them as a single string: G,A,.....
db_metad_exp_struct.emd_instr      	= {}; %   3  ; %
db_metad_exp_struct.emd_carri      	= {}; %   4  ; % run date as a string.
db_metad_exp_struct.emd_spotl    	= {}; %   5  ; %
db_metad_exp_struct.emd_chipl      	= {}; %   6  ; %
db_metad_exp_struct.emd_proto    	= {}; %   7  ; %
db_metad_exp_struct.emd_dates     	= {}; %   8  ; %
db_metad_exp_struct.emd_state       = {}; %   9  ; %
db_metad_exp_struct.emd_futur       = {}; %   10 ; %
db_metad_set_struct.expid           = {}; %   11 ; % this is not for display but to link with tos expi

field_01 = 'orndx';                 value_01 = { orndx               	};     % single vector with 1 value per sensogram: original ndx from db (as retrieved)
field_02 = 'db_metad_exp_struct';   value_03 = { db_metad_exp_struct    };     % struct with cell arrays with 1 row per exp and a many cols as metadata strs to describe a exp
field_03 = 'sme_cols';          	value_04 = { emd_cols             	};     % struct with  cols for exp_meta_data_cell matrix
field_04 = 'datef';                 value_05 = { datef                  };     % vector with 1 value per exp (may not be needed!!!)

% Pack all the db_data_set fields into a struct to be returned.  jas_tbreviewed all fields of db_data_exp_struct  _jas_tbd
% db_data_set will hold ALL data comming from the db or loaded from a local file ________________
db_data_exp_struct = struct(...
    field_01 , value_01, field_02, value_02, field_03, value_03                     ...
   ,field_04 , value_04);


% END: toe_additions: 2018_01_26 ____________________________________________________________________________________________________________

ui_toe_sort_ndx   	=nan;   % this is the order to display data in the toe. (default: same as g_db_set_db_ndx)
ui_db_exp_ui_filter	=nan;   % 0/1 1:if selected by filters. (ie the real data to work with)

ui_state_struct = struct(...
    'ui_tos_sort_ndx',      {ui_tos_sort_ndx},  'ui_db_set_ui_filter', {ui_db_set_ui_filter}    ...
    ,'ui_db_set_sc_done',   {ui_db_set_sc_done},'ui_db_set_rc_done',   {ui_db_set_rc_done}      ...
    ,'ui_db_set_plot_g',    {ui_db_set_plot_g}, 'ui_db_set_plot_a',    {ui_db_set_plot_a}       ...
    ,'ui_db_set_pto_tos',   {ui_db_set_pto_tos} ...
    ,'cur_session_name',    {cur_session_name}  ...
    ,'db_session_opened',   {db_session_opened}  ...    
    ,'local_input_dir',     {local_input_dir}   ...    
    ,'tos_cur_selection',   [1 1]               ... 	% dummy init to frist row jas_plane
    ,'ui_toe_sort_ndx',      {ui_toe_sort_ndx},  'ui_db_exp_ui_filter', {ui_db_exp_ui_filter}    ...
    ,'toe_cur_selection',   [1 1]               ... 	% dummy init to frist row jas_plane
    );

end % fn: sa_init_cells_and_structs
% leftovers: _________________________________________________________________________________________________________