sa_ui_set_env_after_get_data    sa_ui_set_env_after_get_exp

db_cur_fit_net_ndx              db_cur_toe_net_ndx
db_data_set_struct              db_toe_set_struct
ui_table_tos                    ui_table_exp

api_exp_names   = expe_list_cel(:,7);
exp_len         = size(expe_list_cel,1);
exp_names_cel   = cell(exp_len,1);
for cur_exp_ndx=1:1:exp_len
    temp        = expe_list_cel{cur_exp_ndx,7};
    if isfield(temp,'experiment_name')
        exp_names_cel{cur_exp_ndx} =   temp.experiment_name;            
    end
end