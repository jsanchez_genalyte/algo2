function [ local_hd_struct,error_message ] = excel_exp_save_scan(local_hd_struct,expe_ned_table,sensor_excel_table,scan_data_struct)
%EXCEL_EXP_SAVE_SCAN saves the scan data for 1 whole experiment per tab into a excel file (exp,sensor,probe, and scan and optics level)
%   Aligns (replicates) 2 matrices: the sensor_excel_table (one record per sensor) and tghe scan_data: ~ 110 records per sensor
%   The name of the excel file is whatever comes in local_hd_struct (with optical)
%   Input:
%   local_hd_struct        file_names and tab_names.
%   expe_ned_table:        table with one row per experiment requested.
%   sensor_excel_table:    table with experiment,probe and sensor level info: as many rows as sensors.
%                          each row will be repeated for each scan of the given sensor
%   scan_data_struct:      scan matrix to be dump in excel next to the sensor_excel_table

% see:          local_hd_struct deined in sa_do_push_top_sens
% pipeline:     Sensogram_Analysis_App_10 --> pushbutton_top_sens_Callback  --> sa_do_push_top_sens --> sa_rest_db_set_get_all_data --> sa_rest_exp_save_scan
% sample call:  [ local_hd_struct,error_message ] = sa_rest_exp_save_scan(local_hd_struct,expe_ned_table,sensor_excel_table,scan_data_struct);

[ local_hd_struct,error_message ] = excel_define_out_sheet(local_hd_struct,expe_ned_table); %   ,expe_ned_table,  handles_text_exp_status)

sensor_excel_header = { 'exper_id','exper_name', 'carrier_serial_number', 'sensor_number','sensor_type','channel','probe_name','probe_sensor'};
scan_excel_header   = { 'time_shitf_min','raw_shift_rel','scan_index','abs_wave_len_1','abs_wave_len_2','abs_wave_len_3','abs_wave_len_4','abs_wave_len_5','abs_wave_len_6','pk_wav_nm','pbr','fsr_nm','er_1','er_2','er_3','er_4','er_5','er_6','qual_factor_1','qual_factor_2','qual_factor_3','qual_factor_4','qual_factor_5','qual_factor_6','lin_ratio_1','lin_ratio_2','lin_ratio_3','lin_ratio_4','lin_ratio_5','lin_ratio_6','peak_type_1','peak_type_2','peak_type_3','peak_type_4','peak_type_5','peak_type_6','power_adc_1','power_adc_2','power_adc_3','power_adc_4','power_adc_5','power_adc_6'};
%header_excel_set   = [ sensor_excel_header scan_excel_header ];

% HAVE sensor_data_mat with exact dimentions: Save it:
sensor_cnt          = scan_data_struct.sensor_cnt;
scan_len_max        = scan_data_struct.scan_len_max;
%scan_col_max       = scan_data_struct.scan_col_max;
sensor_data_mat     = scan_data_struct.sensor_data_mat;
%sensor_type        = scan_data_struct.sensor_type;

exper_data_cell     = cell(size(sensor_data_mat,1),size(sensor_excel_header,2));
exper_excel_table   = cell2table(exper_data_cell);
exper_excel_table.Properties.VariableNames = sensor_excel_header;

% fill in matrix to write to the excel tab:
exper_width         = size(sensor_excel_header,2);
cur_row_start       = 1;
for cur_sensor_ndx  = 1:1:sensor_cnt % ___________________________________________________________ sensor_loop  jas_test: start with a real sensor: 7
    % STORE EACH SENSOR: Include tr and leak detector 
    % replicate the current sensor record into as many scan rows:
    cur_row_end     = cur_row_start+scan_len_max-1;              % *** MAGIC ****
    exper_excel_table(cur_row_start:cur_row_end,1:exper_width) = repmat(sensor_excel_table(cur_sensor_ndx,:),scan_len_max,1);
    cur_row_start   = cur_row_end+1; 
end
%exper_excel_table(:,exper_width+1:end) = table(sensor_data_mat);
scan_excel_table    = array2table(sensor_data_mat,'VariableNames',scan_excel_header);
set_excel_table     = [exper_excel_table scan_excel_table];

% HAVE THE TABLE TO WRITE TO THE EXCEL TAB: set_excel_table
    if (~(isempty(local_hd_struct.last_sheet)))
        % HAVE A SHETT TO WRITE TO: Go for it
        writetable(set_excel_table,local_hd_struct.local_hd_excel_scan_set_file,'Sheet',local_hd_struct.last_sheet);
        fprintf('\n _writing_ file: %-s \n _sheet_:          %-s \nDONE\n',local_hd_struct.local_hd_excel_scan_set_file,local_hd_struct.last_sheet); 
    end   
end % fn sa_rest_exp_save_scan
% 

