function [assay_sens_struct,assay_sens_table,error_message  ] = get_assay_sensor_data_from_sensor(exp_sensor_struct,assay_cnt)
%GET_ASSAY_SENSOR_DATA_FROM_SENSORS fills in the assay_sens_table with info from the exp_sensor_struct.
%   nothing special: just repackaging the data. No calculations. returns 32 probes x 7 x 4
% assay_cnt = 16; % jas_hardcoded
error_message           = '';
assay_sens_struct       = struct; %   * empty table 

% assay_sens_header = {'probe','clust','chann','shifr','shifj','outfg','errfg'};
% sample call:
%  [assay_sens_struct,assay_sens_table,error_message  ] = get_assay_sensor_data_from_sensor(exp_sensor_struct,16)

probe = cell(assay_cnt,4);
clust = cell(assay_cnt,4);
chann = cell(assay_cnt,4);
shifr = cell(assay_cnt,4); 	% raw           gru shift
shifj = cell(assay_cnt,4); 	% jervis norm   gru shift
outfg = cell(assay_cnt,4);   % outlier flag
errfg = cell(assay_cnt,4);   % jas_tbd initialize an array of cells. 

exp_sensor_table        = struct2table	( exp_sensor_struct);
exp_sensor_assay_table  = exp_sensor_table(5:132,2:end);
cur_probe=1;
cur_ch   =1;
for cur_sensor=1:4:128  %  included on the rpt: hsa1: is 5,6,7,8,9 ..
    %cur_sensor
    cur_sensor_ndx= cur_sensor-1;
    for cur_sensor_prb=1:1:4
        %cur_sensor_prb
        cur_sensor_ndx= cur_sensor_ndx+1;
        chann(cur_probe,cur_sensor_prb) = {exp_sensor_assay_table.channel(cur_sensor_ndx)}; %cur_ch;
        if (~(isempty(exp_sensor_assay_table.results{cur_sensor})))
            % HAVE DATA:  % value: 518.4443  normalized_value: 522.9580 is_outlier: 0   sensor_errors: []
            temp = exp_sensor_assay_table.results{cur_sensor_ndx};
            if (isfield(temp,'value'))
                shifr{cur_probe,cur_sensor_prb} =exp_sensor_assay_table.results{cur_sensor_ndx}.value;         	% raw           gru shift
                shifj{cur_probe,cur_sensor_prb} =exp_sensor_assay_table.results{cur_sensor_ndx}.normalized_value;	% nor           gru shift
                outfg{cur_probe,cur_sensor_prb} =exp_sensor_assay_table.results{cur_sensor_ndx}.is_outlier;		% outfg         outlier flag
                errfg{cur_probe,cur_sensor_prb} =exp_sensor_assay_table.results{cur_sensor_ndx}.sensor_errors;	    % err           desc (string
                probe{cur_probe,cur_sensor_prb} ='';	    %  tb_filled
                clust{cur_probe,cur_sensor_prb} = cur_sensor_prb;
            end
        end
    end
    if (cur_ch ==1)
        cur_ch =2;
    else
        cur_ch =1;
    end
    if (cur_ch ==1)
    end
	cur_probe = cur_probe+1;    
end



assay_sens_struct.probe = probe;
assay_sens_struct.clust = clust;
assay_sens_struct.chann = chann;
assay_sens_struct.shifr = shifr;
assay_sens_struct.shifj = shifj;
assay_sens_struct.outfg = outfg;
assay_sens_struct.errfg = errfg;
assay_sens_table= struct2table(assay_sens_struct);

%  assay_sens_table = table(probe,clust,chann,shifr,shifj,outfg,errfg);

   % probe_instr_raw_gru_value(cur_probe,cur_ch)	= nanmean([gru_instr_s1,gru_instr_s2,gru_instr_s3,gru_instr_s4]);    

end % fn get_assay_sensor_data_from_sensors


