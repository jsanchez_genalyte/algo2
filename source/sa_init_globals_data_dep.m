% % SA_INIT_GLOBALS_DATA_DEP  is a script to do a one time inizialization of all the data deppendent globals in this app. 
% see: sa_declare_globals;
% this script is to be called once the size of the input data is known and the variables with the real sizes is known
% I.E. after having requested data from the db or openning a local data set file.



% G0_ large matrices_storage: data_dep: ________________________________________________________
% matrices_storage: rows_db_cnt,max_scan_cnt,db_cols_cnt,tos_rows,tos_cols
g_db_set      =  nan(g_rows_db_cnt,(db_cols_cnt + ui_total_fg_cnt));%   how does it relate to tos ?
g_db_set_cat  =  nan(g_rows_db_cnt, db_cols_cat_cnt );              %   cat  ordinal proto+samp+lotx+expr+inst+carr+chan+clust+datat
g_db_dates    =  nan(g_rows_db_cnt,1);                              % dates 
g_scan_raw    =  nan(g_rows_db_cnt,g_max_scan_rows_db_cnt);         % float
g_scan_nrm    =  nan(g_rows_db_cnt,g_max_scan_rows_db_cnt);         % float
g_scan_tr     =  nan(g_rows_db_cnt,g_max_scan_rows_db_cnt);         % float
g_scan_nrm_ma =  nan(g_rows_db_cnt,g_max_scan_rows_db_cnt);         % float calc by matlab    	
sc_flags_cat  =  nan(g_rows_db_cnt,1);                              % cat   ordinal

%
% additional cols for  g_db_set this is an original classificat
g_db_set_db_ndx    =  nan(g_rows_db_cnt,1);%  this is the original order the data came from the db query.
g_tos_sort_ndx     =  nan(g_rows_db_cnt,1);%  this is the order to display data in the tos. (default: same as g_db_set_db_ndx)
g_db_set_ui_filter =  nan(g_rows_db_cnt,1);%  0/1 1:if selected by filters. (ie the real data to work with)
g_db_set_sc_done   =  nan(g_rows_db_cnt,1);%  0/1 1:if classification done
g_db_set_rc_done   =  nan(g_rows_db_cnt,1);%  0/1 1:if re-classification done by end-user).

g_db_set_ui_plot_g =  nan(g_rows_db_cnt,1);%  currently ploted on the good side 
g_db_set_ui_plot_a =  nan(g_rows_db_cnt,1);%  currently ploted on the anom side  
g_db_set_ui_ptof   =  nan(g_rows_db_cnt,1);%  currently displayed on tos tbl.  