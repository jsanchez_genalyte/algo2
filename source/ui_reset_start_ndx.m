function [ ui_state_struct ] = ui_reset_start_ndx( ui_state_struct )
%UNTITLED3 Resets the start ndx to re-start a new display/next/display loop.
%   Accounts for the size of the available data
%   sample call:  handles.ui_state_struct = ui_reset_start_ndx(handles.ui_state_struct);
%   when to call: after any of the 12 data filters get modified by the end-user:  safest thing for the time being is to reset for all of them. 
%                 and after the db_cur_fit_net_ndx filter gets updated with the new selection.
%   what gets reset:  all the: view related indices:
            
ui_state_struct.senso_start_ndx         = 1;
ui_state_struct.senso_curves_perp_cnt	= min(ui_state_struct.senso_curves_perp_cnt,sum(ui_state_struct.db_cur_fit_net_ndx));
% jas_tbd: Think of other items that may need to be reset here.


end % fn ui_state_struct

