function [ handles ] = sa_do_push_top_expe( hObject, eventdata, handles )
%sa_do_push_top_sens handles switching to the top_exp tab (the real thing)
%   Detailed explanation goes here

% STEPS:
% Rules: Always allow to go back to experiments.
% Prompt for Saving rules: AND of:               SAVE: save and go to exp. cancel: do nothing
%  1 - if data_set_dirty_flag AND
%  2 - senso size > 300 && exp_size > 2 
% 

%  1 - set_data_set_dirty_flag:
%      Just after loading scan data.         (>300)
%      Just after classi sensograms          (>300)
%      Just after doing matlab normalization (>300)
%      
%  1 - set_data_set_not_dirty_flag:
%      Just after saving      
%      Just after confirming not saving data_set


% Do ui first:
set(handles.ui_panel_expe,'Visible','on');
set(handles.ui_panel_sens,'Visible','off');
% set(handles.ui_panel_algo,'Visible','off');  % it does not exist yet in guide !!!!
% S1. Do not need to do anything else. Let the user change the toe selection or place new requests.
 

%set(handles.pushbutton_top_expe,'Enable','on');
%set(handles.pushbutton_top_sens,'Enable','on');  % jas_temp: fake data.. so set it to on. but it should be ','off');
%set(handles.pushbutton_algo,'Enable','off');

%handles = sa_do_push_top_expe((hObject, eventdata, handles);
end % fn sa_do_push_top_expe
