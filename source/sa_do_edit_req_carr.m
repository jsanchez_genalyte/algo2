function [ handles ] = sa_do_edit_req_carr( ~, ~, handles )
%SA_DO_EDIT_REQ_CARI   handles request of carr editing box.
%   Detailed explanation goes here

% Hints: get(hObject,'String') returns contents of edit_req_spot as text
%        str2double(get(hObject,'String')) returns contents of edit_req_spot as a double

[ a_request_cell_str,~ ] = sa_do_parse_str_request_vanilla_regex( get(handles.edit_req_carr,'string'));
% HAVE THE LIST OF CARRIERS REQUESTED: Call the db
% request data
% append to toe exper from db
% update status: requesting experiments
% toe_status:  count:  xxx selected: yyy
%
if ( (isempty(a_request_cell_str)) || ((~(isempty(a_request_cell_str))) && ( isempty(a_request_cell_str{1}))))
    handles.ui_state_struct.req_carr_list   = {};
    status_message = sprintf('Not using Carrirer filter ' );
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'    
else
    handles.ui_state_struct.req_carr_list   = a_request_cell_str;
    a_request_str = char(a_request_cell_str);
    status_message = '';
    for cur_req_ndx = 1:1:size(a_request_str,1)
        status_message = strcat(status_message,sprintf(' %s , ',a_request_str(cur_req_ndx,:)));
    end
    status_message = strcat('Carrier Filter:  ',status_message(1:end-1));
    set(handles.text_exp_status,'string',status_message,'ForegroundColor','blue','Fontsize',12); %'blue'
    
end
end % fn sa_do_edit_req_carr

