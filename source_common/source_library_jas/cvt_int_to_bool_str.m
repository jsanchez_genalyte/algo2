function [ true_false_str ] = cvt_int_to_bool_str( a_value )
%cvt_int_to_bool_str converts a zero into a FALSE string and anything else into a TRUE string
%   Input is expected to be numeric.  Sample call:   cvt_int_to_bool_str( 0 )   returns string: FALSE

if  (a_value == 0)
    true_false_str  =  'FALSE';
else
    true_false_str  = 'TRUE';
end;
end

