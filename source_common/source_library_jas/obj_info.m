function [  ] = obj_info( an_object )
%obj_info displays info for a given object
%   This function is to speed up retrieving info about any object. to be used from the cmd line:
%   Sample call:   obj_info(any_variable_name) 

class(an_object)
size(an_object)

if isstruct(an_object)
    fieldnames(an_object)'
    an_object
end; % oi function

