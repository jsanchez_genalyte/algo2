function [ data_matrix_double ] = cell2matdouble( cell_data_cell )
%CELL2MATDOUBLE converts a cell array with strings of numeric data into a 2D array of doubles.
%   Returns a 2D array of doubles.
%   Assumptions:
%   Input: cell_data_cell contains cells whose elements are strings of numeric values.
%   Output details: nans stay as nans and blanks become nans.

data_matrix_double = zeros(size(cell_data_cell));
for i=1:1:size(cell_data_cell,1)   
    for j=1:1:size(cell_data_cell,2)
        data_matrix_double(i,j) = str2double(cell_data_cell{i,j});
    end
end

return

