function [ range_array_int ] = excel_map_column_letter_to_int( first_column_letter, second_column_letter )
%EXCEL_MAP_COLUMN_LETTER_TO_INT  maps a letter representing an column number to an integer value
%   First column A maps to 1 and so on.
%   This is excel specific code
% sample call:  [ range_array_int ] = excel_map_column_letter_to_int( 'a' , 'd' )
debug_flag = true;
range_array_int = nan;
excel_col_names_let = {'A',     'B',	'C',	'D',	'E',	'F',	'G',	'H',	'I',	'J',	'K',	'L',	'M',	'N',	'O',	'P',	'Q',	'R',	'S',	'T',	'U',	'V',	'W',	'X',	'Y',	'Z',	'AA',	'AB',	'AC',	'AD',	'AE',	'AF',	'AG',	'AH',	'AI',	'AJ',	'AK',	'AL',	'AM',	'AN',	'AO',	'AP',	'AQ',	'AR',	'AS',	'AT',	'AU',	'AV',	'AW',	'AX',	'AY',	'AZ',	'BA',	'BB',	'BC',	'BD',	'BE',	'BF',	'BG',	'BH',	'BI',	'BJ',	'BK',	'BL',	'BM',	'BN',	'BO',	'BP',	'BQ',	'BR',	'BS',	'BT',	'BU',	'BV',	'BW',	'BX',	'BY',	'BZ',	'CA',	'CB',	'CC',	'CD',	'CE',	'CF',	'CG',	'CH',	'CI',	'CJ',	'CK',	'CL',	'CM',	'CN',	'CO',	'CP',	'CQ',	'CR',	'CS',	'CT',	'CU',	'CV',	'CW',	'CX',	'CY',	'CZ',	'DA',	'DB',	'DC',	'DD',	'DE',	'DF',	'DG',	'DH',	'DI',	'DJ',	'DK',	'DL',	'DM',	'DN',	'DO',	'DP',	'DQ',	'DR',	'DS',	'DT',	'DU',	'DV',	'DW',	'DX',	'DY',	'DZ',	'EA',	'EB',	'EC',	'ED',	'EE',	'EF',	'EG',	'EH',	'EI',	'EJ',	'EK',	'EL',	'EM',	'EN',	'EO',	'EP',	'EQ',	'ER',	'ES',	'ET',	'EU',	'EV',	'EW',	'EX',	'EY',	'EZ',	'FA',	'FB',	'FC',	'FD',	'FE',	'FF',	'FG',	'FH',	'FI',	'FJ',	'FK',	'FL',	'FM',	'FN',	'FO',	'FP',	'FQ',	'FR',	'FS',	'FT',	'FU',	'FV',	'FW',	'FX',	'FY',	'FZ',	'GA',	'GB',	'GC',	'GD',	'GE',	'GF',	'GG',	'GH',	'GI',	'GJ',	'GK',	'GL',	'GM',	'GN',	'GO',	'GP',	'GQ',	'GR',	'GS',	'GT',	'GU',	'GV',	'GW',	'GX',	'GY',	'GZ',	'HA',	'HB',	'HC',	'HD',	'HE',	'HF',	'HG',	'HH',	'HI',	'HJ',	'HK',	'HL',	'HM',	'HN',	'HO',	'HP',	'HQ',	'HR',	'HS',	'HT',	'HU',	'HV',	'HW',	'HX',	'HY',	'HZ'};

first_column_letter  = upper(first_column_letter);

switch nargin
    case 1
        % USER REQUESTED SINGLE VALUE
        [ first_element_found_flag, first_element_ndx ] = is_element_in_cell( excel_col_names_let,first_column_letter );
        if (first_element_found_flag)
            range_array_int = first_element_ndx;
        end
    case 2
        % USER REQUESTED MULTIPLE VALUES: IE A RANGE
        second_column_letter = upper(second_column_letter);
        [ first_element_found_flag, first_element_ndx ] = is_element_in_cell( excel_col_names_let,first_column_letter );
        if (~(first_element_found_flag))
            return;
        end
        [ second_element_found_flag, second_element_ndx ] = is_element_in_cell( excel_col_names_let,second_column_letter );
        if (~(second_element_found_flag))
            return;
        end
        % HAVE THE 2 INT VALUES: Check the range
        if (first_element_ndx > second_element_ndx)
            display (sprintf('invalid Excel range requested: from %s %s',first_column_letter,second_column_letter));
        end
        % HAVE THE 2 INT VALUES: Define the range
        range_array_int = first_element_ndx:second_element_ndx;
        if debug_flag
            display(sprintf('VALID Excel range requested: from %s %s maps to columns %3d : %3d',first_column_letter,second_column_letter,first_element_ndx:second_element_ndx));
        end
    otherwise
        range_array_int =nan;
end


end % fn excel_map_column_letter_to_int

