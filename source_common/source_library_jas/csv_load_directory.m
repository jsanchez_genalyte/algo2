function [  dir_data_cell ] = csv_load_directory( directory_name )
%CSV_LOAD_DIRECTORY  loads a whole dir with csv file excel file into a cell of tables.
% returns a table with whatever csv files are found in directory
% sample call: [  dir_data_cell ] = csv_load_directory('C:\Users\jsanchez\Documents\gca_ver\ver_norm\data\Test_John\chung_exported_csv\0100009196\\GPA1944-6400\' )

% Get files In Directory

file_list = dir(strcat(directory_name,'*.csv'));

file_count =  size(file_list,1);
% sort file names as numbers

file_list_ndx  = zeros(file_count,1);
for cur_file=1:1:file_count
     cur_file_name = char(file_list(cur_file).name);
     cur_file_name = cur_file_name(1:end-4);
     cur_file_number = str2double(cur_file_name);
     file_list_ndx(cur_file,1) =   cur_file_number; 
end

% Create a cell: Each entry on the cell will be a matrix with each of the csv file contents.
dir_data_cell = cell(file_count,1);

% Load each file
for cur_file=1:1:file_count
    cur_ndx = file_list_ndx(cur_file);
    dir_data_cell{cur_ndx}= csv_load_single(strcat(directory_name,file_list(cur_file).name));
end

end % load_single_excel

