function [ element_found_flag, element_ndx ] = is_element_in_cell( a_cell_array,a_wanted_element )
%IS_ELEMENT_IN_CELL returns true if the element is in the cell array. False otherwhise.
%   return false for non_cell elements, or empty or nan  cell_arrays.
% Assumptions: a_cell_array      is an array of cells with chars as elements.
% Assumptions: a_wanted_element  is an array of cells with chars as elements or  an array of chars
%   only case when true is returned, is when the element is founf on the cell array (assuming string elements).
% SAMPE CALL:  [ element_found_flag, element_ndx ] = is_element_in_cell( {'analyte_1', 'analyte_2','analyte_3' },'analyte_2' )
element_found_flag = false;
element_ndx   = 0;
if (isempty(a_cell_array))   % (isnan(a_cell_array)
    return;
end

if ( ( ~(iscell(a_cell_array))) )
    % NON CELL ARRAY
    return;
end

for cur_ndx=1:1:length(a_cell_array)
    test_element = a_cell_array{cur_ndx};
    if iscell(test_element)
        % NESTED CELL ELEMENT: Handle one level down
        test_element = test_element{1};
    end
    if sum(isnan(test_element))
        continue
    end
    if iscell(a_wanted_element)
        % ONE OR MORE ELEMENTS TO LOOK FOR: Loop over
        for j=1:1:length(a_wanted_element)
            if (strcmp(test_element,a_wanted_element{j} ))
                % FOUND A MATCH: get out.
                element_found_flag = true;
                element_ndx        = cur_ndx;
                return
            end
        end
    else
        % SINGLE ELEMENT TO LOOK FOR: single comparison.
        if (isnan(a_wanted_element))
            % NAN wanted_element: return false
            return;
        end
        if (strcmp(test_element,a_wanted_element ))
            % FOUND A MATCH: get out.
            element_found_flag = true;
            element_ndx        = cur_ndx;
            return;
        end
    end
end % for each element of the cell
return;
