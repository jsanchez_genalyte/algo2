function [ an_int_arr ] = cvt_bool_str_to_int( true_false_cell_str )
%cvt_int_to_bool_str converts sting(s) of TRUE/ FALSE  and anything else into an (array) of 0/1 (s)
%   Input is expected to be either string: 'TRUE'/ 'FALSE'  string.               Sample call:  cvt_bool_str_to_int( 'FALSE' )         returns int: 0
%   Input is expected to be or     cell:  { 'TRUE'/'FALSE' , 'TRUE'/'FALSE' ..}.  Sample call:  cvt_bool_str_to_int( 'true'  'FALSE' ) returns int:   [ 1 0 ]


if ( ischar(true_false_cell_str) )
    true_false_str = true_false_cell_str;
    an_int_arr = nan;
    if  strcmpi(true_false_str, 'FALSE')
        an_int_arr  =  0;
    end
    if  strcmpi(true_false_str, 'TRUE')
        an_int_arr  =  1;
    end
    
end % ischar

if ( iscell(true_false_cell_str) )
    % INPUT IS A CELL: An array  OF TRUE / FALSE values.  Return an int array of zeros and ones.
    true_false_cell = true_false_cell_str;
    an_int_arr = nan(size(true_false_cell));
    for cur_cel_ndx = 1:1:length(true_false_cell);
        an_int_arr(cur_cel_ndx)  =  nan;
        if  strcmpi(true_false_cell{cur_cel_ndx}, 'FALSE')
            an_int_arr(cur_cel_ndx)  =  0;
        end
        if  strcmpi(true_false_cell{cur_cel_ndx}, 'TRUE')
            an_int_arr(cur_cel_ndx)  =  1;
        end
    end
    
end % ischar

end % fn cvt_bool_str_to_int


