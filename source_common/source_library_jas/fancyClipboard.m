function copy_to_clipboard(a_cell)
% FANCY_CLIPBOARD_COPY  copies the contents of a_cell into the clipboard.
% cell could be mix of chars and numeric.
% JAS_TBD: It works only partially. The integer columns get scrambled.  JAS_TBDebug. 
% source:  http://stackoverflow.com/questions/13922929/pasting-a-cell-array-of-strings-into-the-clipboard

if ~iscell(a_cell), error('Input must be a cell'); end
if ndims(a_cell) > 2, error('Only 1D & 2D cells supported.'); end

% JAS_TBD: The 2 integer columns 6 and 8 get scrambled when pasting into excel!!!.
%          The a_str_to_clipboard is corrupted!. 
% for i=1:1:size(a_cell,1)
%     for j=1:1:size(a_cell,2)
%         if (isnumeric(a_cell{i,j}))
%             a_cell{i,j} = double(a_cell{i,j}) * 1.5;   % a_cell{1,6}  is 28 !!!!! 
%         end
%     end
% end


a_str_to_clipboard = [];
newline = sprintf('\n');
for i = 1:size(a_cell,1);
    row = sprintf('%s\t', a_cell{i,:});
    row(end) = newline;
    a_str_to_clipboard = [a_str_to_clipboard row]; %#ok<AGROW>
end
clipboard('copy',a_str_to_clipboard);
end % fn fancyClipboard(a_cell)