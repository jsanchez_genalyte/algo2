function [  sheet_table]  = ...
    excel_get_range( an_excel_file_name,excel_tab_number )
%EXCEL_GET_RANGE returns a whole sheet from a given excel file and tabl
%   Everything goes here: The whole sheet

% GENERIC EXCEL CODE:

debug_fn = false;  %dbg_

sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
if ( debug_fn)
    obj_info(sheet_table)
    display sheet_table)
end

end % fn excel_get_range

% , ranges_cell, col_names_cell