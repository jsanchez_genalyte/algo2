function [  data_table ] = csv_load_single( csv_file_name )
%CSV_LOAD_SINGLE  loads a single excel file into a table.
% returns a table with whatever is found on the table for the given tab number.
% sample call: [  data_table ] = csv_load_single('C:\Users\jsanchez\Documents\gca_ver\ver_norm\data\Test_John\chung_exported_csv\0100009196\\GPA1944-6400\1.csv' )

[data_table]              = readtable(csv_file_name,'ReadVariableNames',false);  % excel_get_range( csv_file_name,excel_tab_number); %
data_table_rows           = size(data_table,1) ;
data_table_cols           = size(data_table,2) ;
display(sprintf('Rows = %6d Cols = %6d File: %s',data_table_rows,data_table_cols,csv_file_name));

% end load_single_excel